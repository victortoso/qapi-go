/*
 * Copyright 2025 Red Hat, Inc.
 * SPDX-License-Identifier: (MIT-0 and GPL-2.0-or-later)
 */

/****************************************************************************
 * THIS CODE HAS BEEN GENERATED. DO NOT CHANGE IT DIRECTLY                  *
 ****************************************************************************/
package qapi

// Synchronous interface of all available commands
type Commands interface {
	AddFd(AddFdCommand) (AddfdInfo, error)
	AddClient(AddClientCommand) error
	AnnounceSelf(AnnounceSelfCommand) error
	Balloon(BalloonCommand) error
	BlockCommit(BlockCommitCommand) error
	BlockDirtyBitmapAdd(BlockDirtyBitmapAddCommand) error
	BlockDirtyBitmapClear(BlockDirtyBitmapClearCommand) error
	BlockDirtyBitmapDisable(BlockDirtyBitmapDisableCommand) error
	BlockDirtyBitmapEnable(BlockDirtyBitmapEnableCommand) error
	BlockDirtyBitmapMerge(BlockDirtyBitmapMergeCommand) error
	BlockDirtyBitmapRemove(BlockDirtyBitmapRemoveCommand) error
	BlockExportAdd(BlockExportAddCommand) error
	BlockExportDel(BlockExportDelCommand) error
	BlockJobCancel(BlockJobCancelCommand) error
	BlockJobChange(BlockJobChangeCommand) error
	BlockJobComplete(BlockJobCompleteCommand) error
	BlockJobDismiss(BlockJobDismissCommand) error
	BlockJobFinalize(BlockJobFinalizeCommand) error
	BlockJobPause(BlockJobPauseCommand) error
	BlockJobResume(BlockJobResumeCommand) error
	BlockJobSetSpeed(BlockJobSetSpeedCommand) error
	BlockLatencyHistogramSet(BlockLatencyHistogramSetCommand) error
	BlockSetWriteThreshold(BlockSetWriteThresholdCommand) error
	BlockStream(BlockStreamCommand) error
	BlockResize(BlockResizeCommand) error
	BlockSetIoThrottle(BlockSetIoThrottleCommand) error
	BlockdevAdd(BlockdevAddCommand) error
	BlockdevBackup(BlockdevBackupCommand) error
	BlockdevChangeMedium(BlockdevChangeMediumCommand) error
	BlockdevCloseTray(BlockdevCloseTrayCommand) error
	BlockdevCreate(BlockdevCreateCommand) error
	BlockdevDel(BlockdevDelCommand) error
	BlockdevInsertMedium(BlockdevInsertMediumCommand) error
	BlockdevMirror(BlockdevMirrorCommand) error
	BlockdevOpenTray(BlockdevOpenTrayCommand) error
	BlockdevRemoveMedium(BlockdevRemoveMediumCommand) error
	BlockdevReopen(BlockdevReopenCommand) error
	BlockdevSetActive(BlockdevSetActiveCommand) error
	BlockdevSnapshot(BlockdevSnapshotCommand) error
	BlockdevSnapshotDeleteInternalSync(BlockdevSnapshotDeleteInternalSyncCommand) (SnapshotInfo, error)
	BlockdevSnapshotInternalSync(BlockdevSnapshotInternalSyncCommand) error
	BlockdevSnapshotSync(BlockdevSnapshotSyncCommand) error
	CalcDirtyRate(CalcDirtyRateCommand) error
	CancelVcpuDirtyLimit(CancelVcpuDirtyLimitCommand) error
	ChangeBackingFile(ChangeBackingFileCommand) error
	ChangeVncPassword(ChangeVncPasswordCommand) error
	ChardevAdd(ChardevAddCommand) (ChardevReturn, error)
	ChardevChange(ChardevChangeCommand) (ChardevReturn, error)
	ChardevRemove(ChardevRemoveCommand) error
	ChardevSendBreak(ChardevSendBreakCommand) error
	ClientMigrateInfo(ClientMigrateInfoCommand) error
	Closefd(ClosefdCommand) error
	Cont(ContCommand) error
	CxlAddDynamicCapacity(CxlAddDynamicCapacityCommand) error
	CxlInjectCorrectableError(CxlInjectCorrectableErrorCommand) error
	CxlInjectDramEvent(CxlInjectDramEventCommand) error
	CxlInjectGeneralMediaEvent(CxlInjectGeneralMediaEventCommand) error
	CxlInjectMemoryModuleEvent(CxlInjectMemoryModuleEventCommand) error
	CxlInjectPoison(CxlInjectPoisonCommand) error
	CxlInjectUncorrectableErrors(CxlInjectUncorrectableErrorsCommand) error
	CxlReleaseDynamicCapacity(CxlReleaseDynamicCapacityCommand) error
	DeviceListProperties(DeviceListPropertiesCommand) ([]ObjectPropertyInfo, error)
	DeviceSyncConfig(DeviceSyncConfigCommand) error
	DeviceAdd(DeviceAddCommand) error
	DeviceDel(DeviceDelCommand) error
	DisplayReload(DisplayReloadCommand) error
	DisplayUpdate(DisplayUpdateCommand) error
	DriveBackup(DriveBackupCommand) error
	DriveMirror(DriveMirrorCommand) error
	DumpGuestMemory(DumpGuestMemoryCommand) error
	DumpSkeys(DumpSkeysCommand) error
	Dumpdtb(DumpdtbCommand) error
	Eject(EjectCommand) error
	ExpirePassword(ExpirePasswordCommand) error
	GetWin32Socket(GetWin32SocketCommand) error
	Getfd(GetfdCommand) error
	HumanMonitorCommand(HumanMonitorCommandCommand) (string, error)
	InjectNmi(InjectNmiCommand) error
	InputSendEvent(InputSendEventCommand) error
	JobCancel(JobCancelCommand) error
	JobComplete(JobCompleteCommand) error
	JobDismiss(JobDismissCommand) error
	JobFinalize(JobFinalizeCommand) error
	JobPause(JobPauseCommand) error
	JobResume(JobResumeCommand) error
	Memsave(MemsaveCommand) error
	Migrate(MigrateCommand) error
	MigrateContinue(MigrateContinueCommand) error
	MigrateIncoming(MigrateIncomingCommand) error
	MigratePause(MigratePauseCommand) error
	MigrateRecover(MigrateRecoverCommand) error
	MigrateSetCapabilities(MigrateSetCapabilitiesCommand) error
	MigrateSetParameters(MigrateSetParametersCommand) error
	MigrateStartPostcopy(MigrateStartPostcopyCommand) error
	MigrateCancel(MigrateCancelCommand) error
	NbdServerAdd(NbdServerAddCommand) error
	NbdServerRemove(NbdServerRemoveCommand) error
	NbdServerStart(NbdServerStartCommand) error
	NbdServerStop(NbdServerStopCommand) error
	NetdevAdd(NetdevAddCommand) error
	NetdevDel(NetdevDelCommand) error
	ObjectAdd(ObjectAddCommand) error
	ObjectDel(ObjectDelCommand) error
	Pmemsave(PmemsaveCommand) error
	QmpCapabilities(QmpCapabilitiesCommand) error
	QomGet(QomGetCommand) (any, error)
	QomList(QomListCommand) ([]ObjectPropertyInfo, error)
	QomListProperties(QomListPropertiesCommand) ([]ObjectPropertyInfo, error)
	QomListTypes(QomListTypesCommand) ([]ObjectTypeInfo, error)
	QomSet(QomSetCommand) error
	QueryAcpiOspmStatus(QueryAcpiOspmStatusCommand) ([]ACPIOSTInfo, error)
	QueryAudiodevs(QueryAudiodevsCommand) ([]Audiodev, error)
	QueryBalloon(QueryBalloonCommand) (BalloonInfo, error)
	QueryBlock(QueryBlockCommand) ([]BlockInfo, error)
	QueryBlockExports(QueryBlockExportsCommand) ([]BlockExportInfo, error)
	QueryBlockJobs(QueryBlockJobsCommand) ([]BlockJobInfo, error)
	QueryBlockstats(QueryBlockstatsCommand) ([]BlockStats, error)
	QueryChardev(QueryChardevCommand) ([]ChardevInfo, error)
	QueryChardevBackends(QueryChardevBackendsCommand) ([]ChardevBackendInfo, error)
	QueryColoStatus(QueryColoStatusCommand) (COLOStatus, error)
	QueryCommandLineOptions(QueryCommandLineOptionsCommand) ([]CommandLineOptionInfo, error)
	QueryCommands(QueryCommandsCommand) ([]CommandInfo, error)
	QueryCpuDefinitions(QueryCpuDefinitionsCommand) ([]CpuDefinitionInfo, error)
	QueryCpuModelBaseline(QueryCpuModelBaselineCommand) (CpuModelBaselineInfo, error)
	QueryCpuModelComparison(QueryCpuModelComparisonCommand) (CpuModelCompareInfo, error)
	QueryCpuModelExpansion(QueryCpuModelExpansionCommand) (CpuModelExpansionInfo, error)
	QueryCpusFast(QueryCpusFastCommand) ([]CpuInfoFast, error)
	QueryCryptodev(QueryCryptodevCommand) ([]QCryptodevInfo, error)
	QueryCurrentMachine(QueryCurrentMachineCommand) (CurrentMachineParams, error)
	QueryDirtyRate(QueryDirtyRateCommand) (DirtyRateInfo, error)
	QueryDisplayOptions(QueryDisplayOptionsCommand) (DisplayOptions, error)
	QueryDump(QueryDumpCommand) (DumpQueryResult, error)
	QueryDumpGuestMemoryCapability(QueryDumpGuestMemoryCapabilityCommand) (DumpGuestMemoryCapability, error)
	QueryFdsets(QueryFdsetsCommand) ([]FdsetInfo, error)
	QueryGicCapabilities(QueryGicCapabilitiesCommand) ([]GICCapability, error)
	QueryHotpluggableCpus(QueryHotpluggableCpusCommand) ([]HotpluggableCPU, error)
	QueryHvBalloonStatusReport(QueryHvBalloonStatusReportCommand) (HvBalloonInfo, error)
	QueryIothreads(QueryIothreadsCommand) ([]IOThreadInfo, error)
	QueryJobs(QueryJobsCommand) ([]JobInfo, error)
	QueryKvm(QueryKvmCommand) (KvmInfo, error)
	QueryMachines(QueryMachinesCommand) ([]MachineInfo, error)
	QueryMemdev(QueryMemdevCommand) ([]Memdev, error)
	QueryMemoryDevices(QueryMemoryDevicesCommand) ([]MemoryDeviceInfo, error)
	QueryMemorySizeSummary(QueryMemorySizeSummaryCommand) (MemoryInfo, error)
	QueryMice(QueryMiceCommand) ([]MouseInfo, error)
	QueryMigrate(QueryMigrateCommand) (MigrationInfo, error)
	QueryMigrateCapabilities(QueryMigrateCapabilitiesCommand) ([]MigrationCapabilityStatus, error)
	QueryMigrateParameters(QueryMigrateParametersCommand) (MigrationParameters, error)
	QueryMigrationthreads(QueryMigrationthreadsCommand) ([]MigrationThreadInfo, error)
	QueryName(QueryNameCommand) (NameInfo, error)
	QueryNamedBlockNodes(QueryNamedBlockNodesCommand) ([]BlockDeviceInfo, error)
	QueryPci(QueryPciCommand) ([]PciInfo, error)
	QueryPrManagers(QueryPrManagersCommand) ([]PRManagerInfo, error)
	QueryQmpSchema(QueryQmpSchemaCommand) ([]SchemaInfo, error)
	QueryReplay(QueryReplayCommand) (ReplayInfo, error)
	QueryRocker(QueryRockerCommand) (RockerSwitch, error)
	QueryRockerOfDpaFlows(QueryRockerOfDpaFlowsCommand) ([]RockerOfDpaFlow, error)
	QueryRockerOfDpaGroups(QueryRockerOfDpaGroupsCommand) ([]RockerOfDpaGroup, error)
	QueryRockerPorts(QueryRockerPortsCommand) ([]RockerPort, error)
	QueryRxFilter(QueryRxFilterCommand) ([]RxFilterInfo, error)
	QueryS390XCpuPolarization(QueryS390XCpuPolarizationCommand) (CpuPolarizationInfo, error)
	QuerySev(QuerySevCommand) (SevInfo, error)
	QuerySevAttestationReport(QuerySevAttestationReportCommand) (SevAttestationReport, error)
	QuerySevCapabilities(QuerySevCapabilitiesCommand) (SevCapability, error)
	QuerySevLaunchMeasure(QuerySevLaunchMeasureCommand) (SevLaunchMeasureInfo, error)
	QuerySgx(QuerySgxCommand) (SGXInfo, error)
	QuerySgxCapabilities(QuerySgxCapabilitiesCommand) (SGXInfo, error)
	QuerySpice(QuerySpiceCommand) (SpiceInfo, error)
	QueryStats(QueryStatsCommand) ([]StatsResult, error)
	QueryStatsSchemas(QueryStatsSchemasCommand) ([]StatsSchema, error)
	QueryStatus(QueryStatusCommand) (StatusInfo, error)
	QueryTarget(QueryTargetCommand) (TargetInfo, error)
	QueryTpm(QueryTpmCommand) ([]TPMInfo, error)
	QueryTpmModels(QueryTpmModelsCommand) ([]TpmModel, error)
	QueryTpmTypes(QueryTpmTypesCommand) ([]TpmType, error)
	QueryUuid(QueryUuidCommand) (UuidInfo, error)
	QueryVcpuDirtyLimit(QueryVcpuDirtyLimitCommand) ([]DirtyLimitInfo, error)
	QueryVersion(QueryVersionCommand) (VersionInfo, error)
	QueryVmGenerationId(QueryVmGenerationIdCommand) (GuidInfo, error)
	QueryVnc(QueryVncCommand) (VncInfo, error)
	QueryVncServers(QueryVncServersCommand) ([]VncInfo2, error)
	QueryXenReplicationStatus(QueryXenReplicationStatusCommand) (ReplicationStatus, error)
	QueryYank(QueryYankCommand) ([]YankInstance, error)
	Quit(QuitCommand) error
	RemoveFd(RemoveFdCommand) error
	ReplayBreak(ReplayBreakCommand) error
	ReplayDeleteBreak(ReplayDeleteBreakCommand) error
	ReplaySeek(ReplaySeekCommand) error
	RequestEbpf(RequestEbpfCommand) (EbpfObject, error)
	RingbufRead(RingbufReadCommand) (string, error)
	RingbufWrite(RingbufWriteCommand) error
	RtcResetReinjection(RtcResetReinjectionCommand) error
	Screendump(ScreendumpCommand) error
	SendKey(SendKeyCommand) error
	SetAction(SetActionCommand) error
	SetCpuTopology(SetCpuTopologyCommand) error
	SetNumaNode(SetNumaNodeCommand) error
	SetVcpuDirtyLimit(SetVcpuDirtyLimitCommand) error
	SetLink(SetLinkCommand) error
	SetPassword(SetPasswordCommand) error
	SevInjectLaunchSecret(SevInjectLaunchSecretCommand) error
	SnapshotDelete(SnapshotDeleteCommand) error
	SnapshotLoad(SnapshotLoadCommand) error
	SnapshotSave(SnapshotSaveCommand) error
	Stop(StopCommand) error
	SystemPowerdown(SystemPowerdownCommand) error
	SystemReset(SystemResetCommand) error
	SystemWakeup(SystemWakeupCommand) error
	TraceEventGetState(TraceEventGetStateCommand) ([]TraceEventInfo, error)
	TraceEventSetState(TraceEventSetStateCommand) error
	Transaction(TransactionCommand) error
	WatchdogSetAction(WatchdogSetActionCommand) error
	XBlockdevAmend(XBlockdevAmendCommand) error
	XBlockdevChange(XBlockdevChangeCommand) error
	XBlockdevSetIothread(XBlockdevSetIothreadCommand) error
	XColoLostHeartbeat(XColoLostHeartbeatCommand) error
	XDebugBlockDirtyBitmapSha256(XDebugBlockDirtyBitmapSha256Command) (BlockDirtyBitmapSha256, error)
	XDebugQueryBlockGraph(XDebugQueryBlockGraphCommand) (XDbgBlockGraph, error)
	XExitPreconfig(XExitPreconfigCommand) error
	XQueryInterruptControllers(XQueryInterruptControllersCommand) (HumanReadableText, error)
	XQueryIrq(XQueryIrqCommand) (HumanReadableText, error)
	XQueryJit(XQueryJitCommand) (HumanReadableText, error)
	XQueryNuma(XQueryNumaCommand) (HumanReadableText, error)
	XQueryOpcount(XQueryOpcountCommand) (HumanReadableText, error)
	XQueryRamblock(XQueryRamblockCommand) (HumanReadableText, error)
	XQueryRoms(XQueryRomsCommand) (HumanReadableText, error)
	XQueryUsb(XQueryUsbCommand) (HumanReadableText, error)
	XQueryVirtio(XQueryVirtioCommand) ([]VirtioInfo, error)
	XQueryVirtioQueueElement(XQueryVirtioQueueElementCommand) (VirtioQueueElement, error)
	XQueryVirtioQueueStatus(XQueryVirtioQueueStatusCommand) (VirtQueueStatus, error)
	XQueryVirtioStatus(XQueryVirtioStatusCommand) (VirtioStatus, error)
	XQueryVirtioVhostQueueStatus(XQueryVirtioVhostQueueStatusCommand) (VirtVhostQueueStatus, error)
	XenColoDoCheckpoint(XenColoDoCheckpointCommand) error
	XenEventInject(XenEventInjectCommand) error
	XenEventList(XenEventListCommand) ([]EvtchnInfo, error)
	XenLoadDevicesState(XenLoadDevicesStateCommand) error
	XenSaveDevicesState(XenSaveDevicesStateCommand) error
	XenSetGlobalDirtyLog(XenSetGlobalDirtyLogCommand) error
	XenSetReplication(XenSetReplicationCommand) error
	Yank(YankCommand) error
}

type AddFdComplete func(AddfdInfo, error)
type AddClientComplete func(error)
type AnnounceSelfComplete func(error)
type BalloonComplete func(error)
type BlockCommitComplete func(error)
type BlockDirtyBitmapAddComplete func(error)
type BlockDirtyBitmapClearComplete func(error)
type BlockDirtyBitmapDisableComplete func(error)
type BlockDirtyBitmapEnableComplete func(error)
type BlockDirtyBitmapMergeComplete func(error)
type BlockDirtyBitmapRemoveComplete func(error)
type BlockExportAddComplete func(error)
type BlockExportDelComplete func(error)
type BlockJobCancelComplete func(error)
type BlockJobChangeComplete func(error)
type BlockJobCompleteComplete func(error)
type BlockJobDismissComplete func(error)
type BlockJobFinalizeComplete func(error)
type BlockJobPauseComplete func(error)
type BlockJobResumeComplete func(error)
type BlockJobSetSpeedComplete func(error)
type BlockLatencyHistogramSetComplete func(error)
type BlockSetWriteThresholdComplete func(error)
type BlockStreamComplete func(error)
type BlockResizeComplete func(error)
type BlockSetIoThrottleComplete func(error)
type BlockdevAddComplete func(error)
type BlockdevBackupComplete func(error)
type BlockdevChangeMediumComplete func(error)
type BlockdevCloseTrayComplete func(error)
type BlockdevCreateComplete func(error)
type BlockdevDelComplete func(error)
type BlockdevInsertMediumComplete func(error)
type BlockdevMirrorComplete func(error)
type BlockdevOpenTrayComplete func(error)
type BlockdevRemoveMediumComplete func(error)
type BlockdevReopenComplete func(error)
type BlockdevSetActiveComplete func(error)
type BlockdevSnapshotComplete func(error)
type BlockdevSnapshotDeleteInternalSyncComplete func(SnapshotInfo, error)
type BlockdevSnapshotInternalSyncComplete func(error)
type BlockdevSnapshotSyncComplete func(error)
type CalcDirtyRateComplete func(error)
type CancelVcpuDirtyLimitComplete func(error)
type ChangeBackingFileComplete func(error)
type ChangeVncPasswordComplete func(error)
type ChardevAddComplete func(ChardevReturn, error)
type ChardevChangeComplete func(ChardevReturn, error)
type ChardevRemoveComplete func(error)
type ChardevSendBreakComplete func(error)
type ClientMigrateInfoComplete func(error)
type ClosefdComplete func(error)
type ContComplete func(error)
type CxlAddDynamicCapacityComplete func(error)
type CxlInjectCorrectableErrorComplete func(error)
type CxlInjectDramEventComplete func(error)
type CxlInjectGeneralMediaEventComplete func(error)
type CxlInjectMemoryModuleEventComplete func(error)
type CxlInjectPoisonComplete func(error)
type CxlInjectUncorrectableErrorsComplete func(error)
type CxlReleaseDynamicCapacityComplete func(error)
type DeviceListPropertiesComplete func([]ObjectPropertyInfo, error)
type DeviceSyncConfigComplete func(error)
type DeviceAddComplete func(error)
type DeviceDelComplete func(error)
type DisplayReloadComplete func(error)
type DisplayUpdateComplete func(error)
type DriveBackupComplete func(error)
type DriveMirrorComplete func(error)
type DumpGuestMemoryComplete func(error)
type DumpSkeysComplete func(error)
type DumpdtbComplete func(error)
type EjectComplete func(error)
type ExpirePasswordComplete func(error)
type GetWin32SocketComplete func(error)
type GetfdComplete func(error)
type HumanMonitorCommandComplete func(string, error)
type InjectNmiComplete func(error)
type InputSendEventComplete func(error)
type JobCancelComplete func(error)
type JobCompleteComplete func(error)
type JobDismissComplete func(error)
type JobFinalizeComplete func(error)
type JobPauseComplete func(error)
type JobResumeComplete func(error)
type MemsaveComplete func(error)
type MigrateComplete func(error)
type MigrateContinueComplete func(error)
type MigrateIncomingComplete func(error)
type MigratePauseComplete func(error)
type MigrateRecoverComplete func(error)
type MigrateSetCapabilitiesComplete func(error)
type MigrateSetParametersComplete func(error)
type MigrateStartPostcopyComplete func(error)
type MigrateCancelComplete func(error)
type NbdServerAddComplete func(error)
type NbdServerRemoveComplete func(error)
type NbdServerStartComplete func(error)
type NbdServerStopComplete func(error)
type NetdevAddComplete func(error)
type NetdevDelComplete func(error)
type ObjectAddComplete func(error)
type ObjectDelComplete func(error)
type PmemsaveComplete func(error)
type QmpCapabilitiesComplete func(error)
type QomGetComplete func(any, error)
type QomListComplete func([]ObjectPropertyInfo, error)
type QomListPropertiesComplete func([]ObjectPropertyInfo, error)
type QomListTypesComplete func([]ObjectTypeInfo, error)
type QomSetComplete func(error)
type QueryAcpiOspmStatusComplete func([]ACPIOSTInfo, error)
type QueryAudiodevsComplete func([]Audiodev, error)
type QueryBalloonComplete func(BalloonInfo, error)
type QueryBlockComplete func([]BlockInfo, error)
type QueryBlockExportsComplete func([]BlockExportInfo, error)
type QueryBlockJobsComplete func([]BlockJobInfo, error)
type QueryBlockstatsComplete func([]BlockStats, error)
type QueryChardevComplete func([]ChardevInfo, error)
type QueryChardevBackendsComplete func([]ChardevBackendInfo, error)
type QueryColoStatusComplete func(COLOStatus, error)
type QueryCommandLineOptionsComplete func([]CommandLineOptionInfo, error)
type QueryCommandsComplete func([]CommandInfo, error)
type QueryCpuDefinitionsComplete func([]CpuDefinitionInfo, error)
type QueryCpuModelBaselineComplete func(CpuModelBaselineInfo, error)
type QueryCpuModelComparisonComplete func(CpuModelCompareInfo, error)
type QueryCpuModelExpansionComplete func(CpuModelExpansionInfo, error)
type QueryCpusFastComplete func([]CpuInfoFast, error)
type QueryCryptodevComplete func([]QCryptodevInfo, error)
type QueryCurrentMachineComplete func(CurrentMachineParams, error)
type QueryDirtyRateComplete func(DirtyRateInfo, error)
type QueryDisplayOptionsComplete func(DisplayOptions, error)
type QueryDumpComplete func(DumpQueryResult, error)
type QueryDumpGuestMemoryCapabilityComplete func(DumpGuestMemoryCapability, error)
type QueryFdsetsComplete func([]FdsetInfo, error)
type QueryGicCapabilitiesComplete func([]GICCapability, error)
type QueryHotpluggableCpusComplete func([]HotpluggableCPU, error)
type QueryHvBalloonStatusReportComplete func(HvBalloonInfo, error)
type QueryIothreadsComplete func([]IOThreadInfo, error)
type QueryJobsComplete func([]JobInfo, error)
type QueryKvmComplete func(KvmInfo, error)
type QueryMachinesComplete func([]MachineInfo, error)
type QueryMemdevComplete func([]Memdev, error)
type QueryMemoryDevicesComplete func([]MemoryDeviceInfo, error)
type QueryMemorySizeSummaryComplete func(MemoryInfo, error)
type QueryMiceComplete func([]MouseInfo, error)
type QueryMigrateComplete func(MigrationInfo, error)
type QueryMigrateCapabilitiesComplete func([]MigrationCapabilityStatus, error)
type QueryMigrateParametersComplete func(MigrationParameters, error)
type QueryMigrationthreadsComplete func([]MigrationThreadInfo, error)
type QueryNameComplete func(NameInfo, error)
type QueryNamedBlockNodesComplete func([]BlockDeviceInfo, error)
type QueryPciComplete func([]PciInfo, error)
type QueryPrManagersComplete func([]PRManagerInfo, error)
type QueryQmpSchemaComplete func([]SchemaInfo, error)
type QueryReplayComplete func(ReplayInfo, error)
type QueryRockerComplete func(RockerSwitch, error)
type QueryRockerOfDpaFlowsComplete func([]RockerOfDpaFlow, error)
type QueryRockerOfDpaGroupsComplete func([]RockerOfDpaGroup, error)
type QueryRockerPortsComplete func([]RockerPort, error)
type QueryRxFilterComplete func([]RxFilterInfo, error)
type QueryS390XCpuPolarizationComplete func(CpuPolarizationInfo, error)
type QuerySevComplete func(SevInfo, error)
type QuerySevAttestationReportComplete func(SevAttestationReport, error)
type QuerySevCapabilitiesComplete func(SevCapability, error)
type QuerySevLaunchMeasureComplete func(SevLaunchMeasureInfo, error)
type QuerySgxComplete func(SGXInfo, error)
type QuerySgxCapabilitiesComplete func(SGXInfo, error)
type QuerySpiceComplete func(SpiceInfo, error)
type QueryStatsComplete func([]StatsResult, error)
type QueryStatsSchemasComplete func([]StatsSchema, error)
type QueryStatusComplete func(StatusInfo, error)
type QueryTargetComplete func(TargetInfo, error)
type QueryTpmComplete func([]TPMInfo, error)
type QueryTpmModelsComplete func([]TpmModel, error)
type QueryTpmTypesComplete func([]TpmType, error)
type QueryUuidComplete func(UuidInfo, error)
type QueryVcpuDirtyLimitComplete func([]DirtyLimitInfo, error)
type QueryVersionComplete func(VersionInfo, error)
type QueryVmGenerationIdComplete func(GuidInfo, error)
type QueryVncComplete func(VncInfo, error)
type QueryVncServersComplete func([]VncInfo2, error)
type QueryXenReplicationStatusComplete func(ReplicationStatus, error)
type QueryYankComplete func([]YankInstance, error)
type QuitComplete func(error)
type RemoveFdComplete func(error)
type ReplayBreakComplete func(error)
type ReplayDeleteBreakComplete func(error)
type ReplaySeekComplete func(error)
type RequestEbpfComplete func(EbpfObject, error)
type RingbufReadComplete func(string, error)
type RingbufWriteComplete func(error)
type RtcResetReinjectionComplete func(error)
type ScreendumpComplete func(error)
type SendKeyComplete func(error)
type SetActionComplete func(error)
type SetCpuTopologyComplete func(error)
type SetNumaNodeComplete func(error)
type SetVcpuDirtyLimitComplete func(error)
type SetLinkComplete func(error)
type SetPasswordComplete func(error)
type SevInjectLaunchSecretComplete func(error)
type SnapshotDeleteComplete func(error)
type SnapshotLoadComplete func(error)
type SnapshotSaveComplete func(error)
type StopComplete func(error)
type SystemPowerdownComplete func(error)
type SystemResetComplete func(error)
type SystemWakeupComplete func(error)
type TraceEventGetStateComplete func([]TraceEventInfo, error)
type TraceEventSetStateComplete func(error)
type TransactionComplete func(error)
type WatchdogSetActionComplete func(error)
type XBlockdevAmendComplete func(error)
type XBlockdevChangeComplete func(error)
type XBlockdevSetIothreadComplete func(error)
type XColoLostHeartbeatComplete func(error)
type XDebugBlockDirtyBitmapSha256Complete func(BlockDirtyBitmapSha256, error)
type XDebugQueryBlockGraphComplete func(XDbgBlockGraph, error)
type XExitPreconfigComplete func(error)
type XQueryInterruptControllersComplete func(HumanReadableText, error)
type XQueryIrqComplete func(HumanReadableText, error)
type XQueryJitComplete func(HumanReadableText, error)
type XQueryNumaComplete func(HumanReadableText, error)
type XQueryOpcountComplete func(HumanReadableText, error)
type XQueryRamblockComplete func(HumanReadableText, error)
type XQueryRomsComplete func(HumanReadableText, error)
type XQueryUsbComplete func(HumanReadableText, error)
type XQueryVirtioComplete func([]VirtioInfo, error)
type XQueryVirtioQueueElementComplete func(VirtioQueueElement, error)
type XQueryVirtioQueueStatusComplete func(VirtQueueStatus, error)
type XQueryVirtioStatusComplete func(VirtioStatus, error)
type XQueryVirtioVhostQueueStatusComplete func(VirtVhostQueueStatus, error)
type XenColoDoCheckpointComplete func(error)
type XenEventInjectComplete func(error)
type XenEventListComplete func([]EvtchnInfo, error)
type XenLoadDevicesStateComplete func(error)
type XenSaveDevicesStateComplete func(error)
type XenSetGlobalDirtyLogComplete func(error)
type XenSetReplicationComplete func(error)
type YankComplete func(error)

// Asynchronous interface of all available commands
type CommandsAsync interface {
	AddFd(AddFdCommand, AddFdComplete) error
	AddClient(AddClientCommand, AddClientComplete) error
	AnnounceSelf(AnnounceSelfCommand, AnnounceSelfComplete) error
	Balloon(BalloonCommand, BalloonComplete) error
	BlockCommit(BlockCommitCommand, BlockCommitComplete) error
	BlockDirtyBitmapAdd(BlockDirtyBitmapAddCommand, BlockDirtyBitmapAddComplete) error
	BlockDirtyBitmapClear(BlockDirtyBitmapClearCommand, BlockDirtyBitmapClearComplete) error
	BlockDirtyBitmapDisable(BlockDirtyBitmapDisableCommand, BlockDirtyBitmapDisableComplete) error
	BlockDirtyBitmapEnable(BlockDirtyBitmapEnableCommand, BlockDirtyBitmapEnableComplete) error
	BlockDirtyBitmapMerge(BlockDirtyBitmapMergeCommand, BlockDirtyBitmapMergeComplete) error
	BlockDirtyBitmapRemove(BlockDirtyBitmapRemoveCommand, BlockDirtyBitmapRemoveComplete) error
	BlockExportAdd(BlockExportAddCommand, BlockExportAddComplete) error
	BlockExportDel(BlockExportDelCommand, BlockExportDelComplete) error
	BlockJobCancel(BlockJobCancelCommand, BlockJobCancelComplete) error
	BlockJobChange(BlockJobChangeCommand, BlockJobChangeComplete) error
	BlockJobComplete(BlockJobCompleteCommand, BlockJobCompleteComplete) error
	BlockJobDismiss(BlockJobDismissCommand, BlockJobDismissComplete) error
	BlockJobFinalize(BlockJobFinalizeCommand, BlockJobFinalizeComplete) error
	BlockJobPause(BlockJobPauseCommand, BlockJobPauseComplete) error
	BlockJobResume(BlockJobResumeCommand, BlockJobResumeComplete) error
	BlockJobSetSpeed(BlockJobSetSpeedCommand, BlockJobSetSpeedComplete) error
	BlockLatencyHistogramSet(BlockLatencyHistogramSetCommand, BlockLatencyHistogramSetComplete) error
	BlockSetWriteThreshold(BlockSetWriteThresholdCommand, BlockSetWriteThresholdComplete) error
	BlockStream(BlockStreamCommand, BlockStreamComplete) error
	BlockResize(BlockResizeCommand, BlockResizeComplete) error
	BlockSetIoThrottle(BlockSetIoThrottleCommand, BlockSetIoThrottleComplete) error
	BlockdevAdd(BlockdevAddCommand, BlockdevAddComplete) error
	BlockdevBackup(BlockdevBackupCommand, BlockdevBackupComplete) error
	BlockdevChangeMedium(BlockdevChangeMediumCommand, BlockdevChangeMediumComplete) error
	BlockdevCloseTray(BlockdevCloseTrayCommand, BlockdevCloseTrayComplete) error
	BlockdevCreate(BlockdevCreateCommand, BlockdevCreateComplete) error
	BlockdevDel(BlockdevDelCommand, BlockdevDelComplete) error
	BlockdevInsertMedium(BlockdevInsertMediumCommand, BlockdevInsertMediumComplete) error
	BlockdevMirror(BlockdevMirrorCommand, BlockdevMirrorComplete) error
	BlockdevOpenTray(BlockdevOpenTrayCommand, BlockdevOpenTrayComplete) error
	BlockdevRemoveMedium(BlockdevRemoveMediumCommand, BlockdevRemoveMediumComplete) error
	BlockdevReopen(BlockdevReopenCommand, BlockdevReopenComplete) error
	BlockdevSetActive(BlockdevSetActiveCommand, BlockdevSetActiveComplete) error
	BlockdevSnapshot(BlockdevSnapshotCommand, BlockdevSnapshotComplete) error
	BlockdevSnapshotDeleteInternalSync(BlockdevSnapshotDeleteInternalSyncCommand, BlockdevSnapshotDeleteInternalSyncComplete) error
	BlockdevSnapshotInternalSync(BlockdevSnapshotInternalSyncCommand, BlockdevSnapshotInternalSyncComplete) error
	BlockdevSnapshotSync(BlockdevSnapshotSyncCommand, BlockdevSnapshotSyncComplete) error
	CalcDirtyRate(CalcDirtyRateCommand, CalcDirtyRateComplete) error
	CancelVcpuDirtyLimit(CancelVcpuDirtyLimitCommand, CancelVcpuDirtyLimitComplete) error
	ChangeBackingFile(ChangeBackingFileCommand, ChangeBackingFileComplete) error
	ChangeVncPassword(ChangeVncPasswordCommand, ChangeVncPasswordComplete) error
	ChardevAdd(ChardevAddCommand, ChardevAddComplete) error
	ChardevChange(ChardevChangeCommand, ChardevChangeComplete) error
	ChardevRemove(ChardevRemoveCommand, ChardevRemoveComplete) error
	ChardevSendBreak(ChardevSendBreakCommand, ChardevSendBreakComplete) error
	ClientMigrateInfo(ClientMigrateInfoCommand, ClientMigrateInfoComplete) error
	Closefd(ClosefdCommand, ClosefdComplete) error
	Cont(ContCommand, ContComplete) error
	CxlAddDynamicCapacity(CxlAddDynamicCapacityCommand, CxlAddDynamicCapacityComplete) error
	CxlInjectCorrectableError(CxlInjectCorrectableErrorCommand, CxlInjectCorrectableErrorComplete) error
	CxlInjectDramEvent(CxlInjectDramEventCommand, CxlInjectDramEventComplete) error
	CxlInjectGeneralMediaEvent(CxlInjectGeneralMediaEventCommand, CxlInjectGeneralMediaEventComplete) error
	CxlInjectMemoryModuleEvent(CxlInjectMemoryModuleEventCommand, CxlInjectMemoryModuleEventComplete) error
	CxlInjectPoison(CxlInjectPoisonCommand, CxlInjectPoisonComplete) error
	CxlInjectUncorrectableErrors(CxlInjectUncorrectableErrorsCommand, CxlInjectUncorrectableErrorsComplete) error
	CxlReleaseDynamicCapacity(CxlReleaseDynamicCapacityCommand, CxlReleaseDynamicCapacityComplete) error
	DeviceListProperties(DeviceListPropertiesCommand, DeviceListPropertiesComplete) error
	DeviceSyncConfig(DeviceSyncConfigCommand, DeviceSyncConfigComplete) error
	DeviceAdd(DeviceAddCommand, DeviceAddComplete) error
	DeviceDel(DeviceDelCommand, DeviceDelComplete) error
	DisplayReload(DisplayReloadCommand, DisplayReloadComplete) error
	DisplayUpdate(DisplayUpdateCommand, DisplayUpdateComplete) error
	DriveBackup(DriveBackupCommand, DriveBackupComplete) error
	DriveMirror(DriveMirrorCommand, DriveMirrorComplete) error
	DumpGuestMemory(DumpGuestMemoryCommand, DumpGuestMemoryComplete) error
	DumpSkeys(DumpSkeysCommand, DumpSkeysComplete) error
	Dumpdtb(DumpdtbCommand, DumpdtbComplete) error
	Eject(EjectCommand, EjectComplete) error
	ExpirePassword(ExpirePasswordCommand, ExpirePasswordComplete) error
	GetWin32Socket(GetWin32SocketCommand, GetWin32SocketComplete) error
	Getfd(GetfdCommand, GetfdComplete) error
	HumanMonitorCommand(HumanMonitorCommandCommand, HumanMonitorCommandComplete) error
	InjectNmi(InjectNmiCommand, InjectNmiComplete) error
	InputSendEvent(InputSendEventCommand, InputSendEventComplete) error
	JobCancel(JobCancelCommand, JobCancelComplete) error
	JobComplete(JobCompleteCommand, JobCompleteComplete) error
	JobDismiss(JobDismissCommand, JobDismissComplete) error
	JobFinalize(JobFinalizeCommand, JobFinalizeComplete) error
	JobPause(JobPauseCommand, JobPauseComplete) error
	JobResume(JobResumeCommand, JobResumeComplete) error
	Memsave(MemsaveCommand, MemsaveComplete) error
	Migrate(MigrateCommand, MigrateComplete) error
	MigrateContinue(MigrateContinueCommand, MigrateContinueComplete) error
	MigrateIncoming(MigrateIncomingCommand, MigrateIncomingComplete) error
	MigratePause(MigratePauseCommand, MigratePauseComplete) error
	MigrateRecover(MigrateRecoverCommand, MigrateRecoverComplete) error
	MigrateSetCapabilities(MigrateSetCapabilitiesCommand, MigrateSetCapabilitiesComplete) error
	MigrateSetParameters(MigrateSetParametersCommand, MigrateSetParametersComplete) error
	MigrateStartPostcopy(MigrateStartPostcopyCommand, MigrateStartPostcopyComplete) error
	MigrateCancel(MigrateCancelCommand, MigrateCancelComplete) error
	NbdServerAdd(NbdServerAddCommand, NbdServerAddComplete) error
	NbdServerRemove(NbdServerRemoveCommand, NbdServerRemoveComplete) error
	NbdServerStart(NbdServerStartCommand, NbdServerStartComplete) error
	NbdServerStop(NbdServerStopCommand, NbdServerStopComplete) error
	NetdevAdd(NetdevAddCommand, NetdevAddComplete) error
	NetdevDel(NetdevDelCommand, NetdevDelComplete) error
	ObjectAdd(ObjectAddCommand, ObjectAddComplete) error
	ObjectDel(ObjectDelCommand, ObjectDelComplete) error
	Pmemsave(PmemsaveCommand, PmemsaveComplete) error
	QmpCapabilities(QmpCapabilitiesCommand, QmpCapabilitiesComplete) error
	QomGet(QomGetCommand, QomGetComplete) error
	QomList(QomListCommand, QomListComplete) error
	QomListProperties(QomListPropertiesCommand, QomListPropertiesComplete) error
	QomListTypes(QomListTypesCommand, QomListTypesComplete) error
	QomSet(QomSetCommand, QomSetComplete) error
	QueryAcpiOspmStatus(QueryAcpiOspmStatusCommand, QueryAcpiOspmStatusComplete) error
	QueryAudiodevs(QueryAudiodevsCommand, QueryAudiodevsComplete) error
	QueryBalloon(QueryBalloonCommand, QueryBalloonComplete) error
	QueryBlock(QueryBlockCommand, QueryBlockComplete) error
	QueryBlockExports(QueryBlockExportsCommand, QueryBlockExportsComplete) error
	QueryBlockJobs(QueryBlockJobsCommand, QueryBlockJobsComplete) error
	QueryBlockstats(QueryBlockstatsCommand, QueryBlockstatsComplete) error
	QueryChardev(QueryChardevCommand, QueryChardevComplete) error
	QueryChardevBackends(QueryChardevBackendsCommand, QueryChardevBackendsComplete) error
	QueryColoStatus(QueryColoStatusCommand, QueryColoStatusComplete) error
	QueryCommandLineOptions(QueryCommandLineOptionsCommand, QueryCommandLineOptionsComplete) error
	QueryCommands(QueryCommandsCommand, QueryCommandsComplete) error
	QueryCpuDefinitions(QueryCpuDefinitionsCommand, QueryCpuDefinitionsComplete) error
	QueryCpuModelBaseline(QueryCpuModelBaselineCommand, QueryCpuModelBaselineComplete) error
	QueryCpuModelComparison(QueryCpuModelComparisonCommand, QueryCpuModelComparisonComplete) error
	QueryCpuModelExpansion(QueryCpuModelExpansionCommand, QueryCpuModelExpansionComplete) error
	QueryCpusFast(QueryCpusFastCommand, QueryCpusFastComplete) error
	QueryCryptodev(QueryCryptodevCommand, QueryCryptodevComplete) error
	QueryCurrentMachine(QueryCurrentMachineCommand, QueryCurrentMachineComplete) error
	QueryDirtyRate(QueryDirtyRateCommand, QueryDirtyRateComplete) error
	QueryDisplayOptions(QueryDisplayOptionsCommand, QueryDisplayOptionsComplete) error
	QueryDump(QueryDumpCommand, QueryDumpComplete) error
	QueryDumpGuestMemoryCapability(QueryDumpGuestMemoryCapabilityCommand, QueryDumpGuestMemoryCapabilityComplete) error
	QueryFdsets(QueryFdsetsCommand, QueryFdsetsComplete) error
	QueryGicCapabilities(QueryGicCapabilitiesCommand, QueryGicCapabilitiesComplete) error
	QueryHotpluggableCpus(QueryHotpluggableCpusCommand, QueryHotpluggableCpusComplete) error
	QueryHvBalloonStatusReport(QueryHvBalloonStatusReportCommand, QueryHvBalloonStatusReportComplete) error
	QueryIothreads(QueryIothreadsCommand, QueryIothreadsComplete) error
	QueryJobs(QueryJobsCommand, QueryJobsComplete) error
	QueryKvm(QueryKvmCommand, QueryKvmComplete) error
	QueryMachines(QueryMachinesCommand, QueryMachinesComplete) error
	QueryMemdev(QueryMemdevCommand, QueryMemdevComplete) error
	QueryMemoryDevices(QueryMemoryDevicesCommand, QueryMemoryDevicesComplete) error
	QueryMemorySizeSummary(QueryMemorySizeSummaryCommand, QueryMemorySizeSummaryComplete) error
	QueryMice(QueryMiceCommand, QueryMiceComplete) error
	QueryMigrate(QueryMigrateCommand, QueryMigrateComplete) error
	QueryMigrateCapabilities(QueryMigrateCapabilitiesCommand, QueryMigrateCapabilitiesComplete) error
	QueryMigrateParameters(QueryMigrateParametersCommand, QueryMigrateParametersComplete) error
	QueryMigrationthreads(QueryMigrationthreadsCommand, QueryMigrationthreadsComplete) error
	QueryName(QueryNameCommand, QueryNameComplete) error
	QueryNamedBlockNodes(QueryNamedBlockNodesCommand, QueryNamedBlockNodesComplete) error
	QueryPci(QueryPciCommand, QueryPciComplete) error
	QueryPrManagers(QueryPrManagersCommand, QueryPrManagersComplete) error
	QueryQmpSchema(QueryQmpSchemaCommand, QueryQmpSchemaComplete) error
	QueryReplay(QueryReplayCommand, QueryReplayComplete) error
	QueryRocker(QueryRockerCommand, QueryRockerComplete) error
	QueryRockerOfDpaFlows(QueryRockerOfDpaFlowsCommand, QueryRockerOfDpaFlowsComplete) error
	QueryRockerOfDpaGroups(QueryRockerOfDpaGroupsCommand, QueryRockerOfDpaGroupsComplete) error
	QueryRockerPorts(QueryRockerPortsCommand, QueryRockerPortsComplete) error
	QueryRxFilter(QueryRxFilterCommand, QueryRxFilterComplete) error
	QueryS390XCpuPolarization(QueryS390XCpuPolarizationCommand, QueryS390XCpuPolarizationComplete) error
	QuerySev(QuerySevCommand, QuerySevComplete) error
	QuerySevAttestationReport(QuerySevAttestationReportCommand, QuerySevAttestationReportComplete) error
	QuerySevCapabilities(QuerySevCapabilitiesCommand, QuerySevCapabilitiesComplete) error
	QuerySevLaunchMeasure(QuerySevLaunchMeasureCommand, QuerySevLaunchMeasureComplete) error
	QuerySgx(QuerySgxCommand, QuerySgxComplete) error
	QuerySgxCapabilities(QuerySgxCapabilitiesCommand, QuerySgxCapabilitiesComplete) error
	QuerySpice(QuerySpiceCommand, QuerySpiceComplete) error
	QueryStats(QueryStatsCommand, QueryStatsComplete) error
	QueryStatsSchemas(QueryStatsSchemasCommand, QueryStatsSchemasComplete) error
	QueryStatus(QueryStatusCommand, QueryStatusComplete) error
	QueryTarget(QueryTargetCommand, QueryTargetComplete) error
	QueryTpm(QueryTpmCommand, QueryTpmComplete) error
	QueryTpmModels(QueryTpmModelsCommand, QueryTpmModelsComplete) error
	QueryTpmTypes(QueryTpmTypesCommand, QueryTpmTypesComplete) error
	QueryUuid(QueryUuidCommand, QueryUuidComplete) error
	QueryVcpuDirtyLimit(QueryVcpuDirtyLimitCommand, QueryVcpuDirtyLimitComplete) error
	QueryVersion(QueryVersionCommand, QueryVersionComplete) error
	QueryVmGenerationId(QueryVmGenerationIdCommand, QueryVmGenerationIdComplete) error
	QueryVnc(QueryVncCommand, QueryVncComplete) error
	QueryVncServers(QueryVncServersCommand, QueryVncServersComplete) error
	QueryXenReplicationStatus(QueryXenReplicationStatusCommand, QueryXenReplicationStatusComplete) error
	QueryYank(QueryYankCommand, QueryYankComplete) error
	Quit(QuitCommand, QuitComplete) error
	RemoveFd(RemoveFdCommand, RemoveFdComplete) error
	ReplayBreak(ReplayBreakCommand, ReplayBreakComplete) error
	ReplayDeleteBreak(ReplayDeleteBreakCommand, ReplayDeleteBreakComplete) error
	ReplaySeek(ReplaySeekCommand, ReplaySeekComplete) error
	RequestEbpf(RequestEbpfCommand, RequestEbpfComplete) error
	RingbufRead(RingbufReadCommand, RingbufReadComplete) error
	RingbufWrite(RingbufWriteCommand, RingbufWriteComplete) error
	RtcResetReinjection(RtcResetReinjectionCommand, RtcResetReinjectionComplete) error
	Screendump(ScreendumpCommand, ScreendumpComplete) error
	SendKey(SendKeyCommand, SendKeyComplete) error
	SetAction(SetActionCommand, SetActionComplete) error
	SetCpuTopology(SetCpuTopologyCommand, SetCpuTopologyComplete) error
	SetNumaNode(SetNumaNodeCommand, SetNumaNodeComplete) error
	SetVcpuDirtyLimit(SetVcpuDirtyLimitCommand, SetVcpuDirtyLimitComplete) error
	SetLink(SetLinkCommand, SetLinkComplete) error
	SetPassword(SetPasswordCommand, SetPasswordComplete) error
	SevInjectLaunchSecret(SevInjectLaunchSecretCommand, SevInjectLaunchSecretComplete) error
	SnapshotDelete(SnapshotDeleteCommand, SnapshotDeleteComplete) error
	SnapshotLoad(SnapshotLoadCommand, SnapshotLoadComplete) error
	SnapshotSave(SnapshotSaveCommand, SnapshotSaveComplete) error
	Stop(StopCommand, StopComplete) error
	SystemPowerdown(SystemPowerdownCommand, SystemPowerdownComplete) error
	SystemReset(SystemResetCommand, SystemResetComplete) error
	SystemWakeup(SystemWakeupCommand, SystemWakeupComplete) error
	TraceEventGetState(TraceEventGetStateCommand, TraceEventGetStateComplete) error
	TraceEventSetState(TraceEventSetStateCommand, TraceEventSetStateComplete) error
	Transaction(TransactionCommand, TransactionComplete) error
	WatchdogSetAction(WatchdogSetActionCommand, WatchdogSetActionComplete) error
	XBlockdevAmend(XBlockdevAmendCommand, XBlockdevAmendComplete) error
	XBlockdevChange(XBlockdevChangeCommand, XBlockdevChangeComplete) error
	XBlockdevSetIothread(XBlockdevSetIothreadCommand, XBlockdevSetIothreadComplete) error
	XColoLostHeartbeat(XColoLostHeartbeatCommand, XColoLostHeartbeatComplete) error
	XDebugBlockDirtyBitmapSha256(XDebugBlockDirtyBitmapSha256Command, XDebugBlockDirtyBitmapSha256Complete) error
	XDebugQueryBlockGraph(XDebugQueryBlockGraphCommand, XDebugQueryBlockGraphComplete) error
	XExitPreconfig(XExitPreconfigCommand, XExitPreconfigComplete) error
	XQueryInterruptControllers(XQueryInterruptControllersCommand, XQueryInterruptControllersComplete) error
	XQueryIrq(XQueryIrqCommand, XQueryIrqComplete) error
	XQueryJit(XQueryJitCommand, XQueryJitComplete) error
	XQueryNuma(XQueryNumaCommand, XQueryNumaComplete) error
	XQueryOpcount(XQueryOpcountCommand, XQueryOpcountComplete) error
	XQueryRamblock(XQueryRamblockCommand, XQueryRamblockComplete) error
	XQueryRoms(XQueryRomsCommand, XQueryRomsComplete) error
	XQueryUsb(XQueryUsbCommand, XQueryUsbComplete) error
	XQueryVirtio(XQueryVirtioCommand, XQueryVirtioComplete) error
	XQueryVirtioQueueElement(XQueryVirtioQueueElementCommand, XQueryVirtioQueueElementComplete) error
	XQueryVirtioQueueStatus(XQueryVirtioQueueStatusCommand, XQueryVirtioQueueStatusComplete) error
	XQueryVirtioStatus(XQueryVirtioStatusCommand, XQueryVirtioStatusComplete) error
	XQueryVirtioVhostQueueStatus(XQueryVirtioVhostQueueStatusCommand, XQueryVirtioVhostQueueStatusComplete) error
	XenColoDoCheckpoint(XenColoDoCheckpointCommand, XenColoDoCheckpointComplete) error
	XenEventInject(XenEventInjectCommand, XenEventInjectComplete) error
	XenEventList(XenEventListCommand, XenEventListComplete) error
	XenLoadDevicesState(XenLoadDevicesStateCommand, XenLoadDevicesStateComplete) error
	XenSaveDevicesState(XenSaveDevicesStateCommand, XenSaveDevicesStateComplete) error
	XenSetGlobalDirtyLog(XenSetGlobalDirtyLogCommand, XenSetGlobalDirtyLogComplete) error
	XenSetReplication(XenSetReplicationCommand, XenSetReplicationComplete) error
	Yank(YankCommand, YankComplete) error
}
