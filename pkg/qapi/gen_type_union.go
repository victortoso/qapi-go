/*
 * Copyright 2025 Red Hat, Inc.
 * SPDX-License-Identifier: (MIT-0 and GPL-2.0-or-later)
 */

/****************************************************************************
 * THIS CODE HAS BEEN GENERATED. DO NOT CHANGE IT DIRECTLY                  *
 ****************************************************************************/
package qapi

import (
	"encoding/json"
	"errors"
	"fmt"
)

// Options of an audio backend.
//
// Since: 4.0
type Audiodev struct {
	// identifier of the backend
	Id string `json:"id"`
	// timer period (in microseconds, 0: use lowest possible)
	TimerPeriod *uint32 `json:"timer-period,omitempty"`
	// Variants fields
	None      *AudiodevGenericOptions   `json:"-"`
	Alsa      *AudiodevAlsaOptions      `json:"-"`
	Coreaudio *AudiodevCoreaudioOptions `json:"-"`
	Dbus      *AudiodevDBusOptions      `json:"-"`
	Dsound    *AudiodevDsoundOptions    `json:"-"`
	Jack      *AudiodevJackOptions      `json:"-"`
	Oss       *AudiodevOssOptions       `json:"-"`
	Pa        *AudiodevPaOptions        `json:"-"`
	Pipewire  *AudiodevPipewireOptions  `json:"-"`
	Sdl       *AudiodevSdlOptions       `json:"-"`
	Sndio     *AudiodevSndioOptions     `json:"-"`
	Spice     *AudiodevGenericOptions   `json:"-"`
	Wav       *AudiodevWavOptions       `json:"-"`
}

func (s Audiodev) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias Audiodev
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.None != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.None); err == nil {
			m["driver"] = AudiodevDriverNone
			bytes, err = json.Marshal(m)
		}
	}

	if s.Alsa != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Alsa); err == nil {
			m["driver"] = AudiodevDriverAlsa
			bytes, err = json.Marshal(m)
		}
	}

	if s.Coreaudio != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Coreaudio); err == nil {
			m["driver"] = AudiodevDriverCoreaudio
			bytes, err = json.Marshal(m)
		}
	}

	if s.Dbus != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Dbus); err == nil {
			m["driver"] = AudiodevDriverDbus
			bytes, err = json.Marshal(m)
		}
	}

	if s.Dsound != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Dsound); err == nil {
			m["driver"] = AudiodevDriverDsound
			bytes, err = json.Marshal(m)
		}
	}

	if s.Jack != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Jack); err == nil {
			m["driver"] = AudiodevDriverJack
			bytes, err = json.Marshal(m)
		}
	}

	if s.Oss != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Oss); err == nil {
			m["driver"] = AudiodevDriverOss
			bytes, err = json.Marshal(m)
		}
	}

	if s.Pa != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Pa); err == nil {
			m["driver"] = AudiodevDriverPa
			bytes, err = json.Marshal(m)
		}
	}

	if s.Pipewire != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Pipewire); err == nil {
			m["driver"] = AudiodevDriverPipewire
			bytes, err = json.Marshal(m)
		}
	}

	if s.Sdl != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Sdl); err == nil {
			m["driver"] = AudiodevDriverSdl
			bytes, err = json.Marshal(m)
		}
	}

	if s.Sndio != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Sndio); err == nil {
			m["driver"] = AudiodevDriverSndio
			bytes, err = json.Marshal(m)
		}
	}

	if s.Spice != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Spice); err == nil {
			m["driver"] = AudiodevDriverSpice
			bytes, err = json.Marshal(m)
		}
	}

	if s.Wav != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Wav); err == nil {
			m["driver"] = AudiodevDriverWav
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal Audiodev due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal Audiodev unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *Audiodev) UnmarshalJSON(data []byte) error {
	type AudiodevBase struct {
		Id          string         `json:"id"`
		Driver      AudiodevDriver `json:"driver"`
		TimerPeriod *uint32        `json:"timer-period,omitempty"`
	}

	tmp := struct {
		AudiodevBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.Id = tmp.Id
	s.TimerPeriod = tmp.TimerPeriod
	switch tmp.Driver {
	case AudiodevDriverNone:
		s.None = new(AudiodevGenericOptions)
		if err := json.Unmarshal(data, s.None); err != nil {
			s.None = nil
			return err
		}
	case AudiodevDriverAlsa:
		s.Alsa = new(AudiodevAlsaOptions)
		if err := json.Unmarshal(data, s.Alsa); err != nil {
			s.Alsa = nil
			return err
		}
	case AudiodevDriverCoreaudio:
		s.Coreaudio = new(AudiodevCoreaudioOptions)
		if err := json.Unmarshal(data, s.Coreaudio); err != nil {
			s.Coreaudio = nil
			return err
		}
	case AudiodevDriverDbus:
		s.Dbus = new(AudiodevDBusOptions)
		if err := json.Unmarshal(data, s.Dbus); err != nil {
			s.Dbus = nil
			return err
		}
	case AudiodevDriverDsound:
		s.Dsound = new(AudiodevDsoundOptions)
		if err := json.Unmarshal(data, s.Dsound); err != nil {
			s.Dsound = nil
			return err
		}
	case AudiodevDriverJack:
		s.Jack = new(AudiodevJackOptions)
		if err := json.Unmarshal(data, s.Jack); err != nil {
			s.Jack = nil
			return err
		}
	case AudiodevDriverOss:
		s.Oss = new(AudiodevOssOptions)
		if err := json.Unmarshal(data, s.Oss); err != nil {
			s.Oss = nil
			return err
		}
	case AudiodevDriverPa:
		s.Pa = new(AudiodevPaOptions)
		if err := json.Unmarshal(data, s.Pa); err != nil {
			s.Pa = nil
			return err
		}
	case AudiodevDriverPipewire:
		s.Pipewire = new(AudiodevPipewireOptions)
		if err := json.Unmarshal(data, s.Pipewire); err != nil {
			s.Pipewire = nil
			return err
		}
	case AudiodevDriverSdl:
		s.Sdl = new(AudiodevSdlOptions)
		if err := json.Unmarshal(data, s.Sdl); err != nil {
			s.Sdl = nil
			return err
		}
	case AudiodevDriverSndio:
		s.Sndio = new(AudiodevSndioOptions)
		if err := json.Unmarshal(data, s.Sndio); err != nil {
			s.Sndio = nil
			return err
		}
	case AudiodevDriverSpice:
		s.Spice = new(AudiodevGenericOptions)
		if err := json.Unmarshal(data, s.Spice); err != nil {
			s.Spice = nil
			return err
		}
	case AudiodevDriverWav:
		s.Wav = new(AudiodevWavOptions)
		if err := json.Unmarshal(data, s.Wav); err != nil {
			s.Wav = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal Audiodev received unrecognized value '%s'",
			tmp.Driver)
	}
	return nil
}

// Describes a block export, i.e. how single node should be exported
// on an external interface.
//
// Since: 4.2
type BlockExportOptions struct {
	// A unique identifier for the block export (across all export
	// types)
	Id string `json:"id"`
	// True prevents the block node from being moved to another thread
	// while the export is active. If true and @iothread is given,
	// export creation fails if the block node cannot be moved to the
	// iothread. The default is false. (since: 5.2)
	FixedIothread *bool `json:"fixed-iothread,omitempty"`
	// The name of the iothread object where the export will run. The
	// default is to use the thread currently associated with the
	// block node. (since: 5.2)
	Iothread *string `json:"iothread,omitempty"`
	// The node name of the block node to be exported (since: 5.2)
	NodeName string `json:"node-name"`
	// True if clients should be able to write to the export (default
	// false)
	Writable *bool `json:"writable,omitempty"`
	// If true, caches are flushed after every write request to the
	// export before completion is signalled. (since: 5.2; default:
	// false)
	Writethrough *bool `json:"writethrough,omitempty"`
	// If true, the export allows the exported node to be inactive. If
	// it is created for an inactive block node, the node remains
	// inactive. If the export type doesn't support running on an
	// inactive node, an error is returned. If false, inactive block
	// nodes are automatically activated before creating the export
	// and trying to inactivate them later fails. (since: 10.0;
	// default: false)
	AllowInactive *bool `json:"allow-inactive,omitempty"`
	// Variants fields
	Nbd          *BlockExportOptionsNbd          `json:"-"`
	VhostUserBlk *BlockExportOptionsVhostUserBlk `json:"-"`
	Fuse         *BlockExportOptionsFuse         `json:"-"`
	VduseBlk     *BlockExportOptionsVduseBlk     `json:"-"`
}

func (s BlockExportOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias BlockExportOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Nbd != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Nbd); err == nil {
			m["type"] = BlockExportTypeNbd
			bytes, err = json.Marshal(m)
		}
	}

	if s.VhostUserBlk != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.VhostUserBlk); err == nil {
			m["type"] = BlockExportTypeVhostUserBlk
			bytes, err = json.Marshal(m)
		}
	}

	if s.Fuse != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Fuse); err == nil {
			m["type"] = BlockExportTypeFuse
			bytes, err = json.Marshal(m)
		}
	}

	if s.VduseBlk != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.VduseBlk); err == nil {
			m["type"] = BlockExportTypeVduseBlk
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal BlockExportOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal BlockExportOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *BlockExportOptions) UnmarshalJSON(data []byte) error {
	type BlockExportOptionsBase struct {
		Type          BlockExportType `json:"type"`
		Id            string          `json:"id"`
		FixedIothread *bool           `json:"fixed-iothread,omitempty"`
		Iothread      *string         `json:"iothread,omitempty"`
		NodeName      string          `json:"node-name"`
		Writable      *bool           `json:"writable,omitempty"`
		Writethrough  *bool           `json:"writethrough,omitempty"`
		AllowInactive *bool           `json:"allow-inactive,omitempty"`
	}

	tmp := struct {
		BlockExportOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.Id = tmp.Id
	s.FixedIothread = tmp.FixedIothread
	s.Iothread = tmp.Iothread
	s.NodeName = tmp.NodeName
	s.Writable = tmp.Writable
	s.Writethrough = tmp.Writethrough
	s.AllowInactive = tmp.AllowInactive
	switch tmp.Type {
	case BlockExportTypeNbd:
		s.Nbd = new(BlockExportOptionsNbd)
		if err := json.Unmarshal(data, s.Nbd); err != nil {
			s.Nbd = nil
			return err
		}
	case BlockExportTypeVhostUserBlk:
		s.VhostUserBlk = new(BlockExportOptionsVhostUserBlk)
		if err := json.Unmarshal(data, s.VhostUserBlk); err != nil {
			s.VhostUserBlk = nil
			return err
		}
	case BlockExportTypeFuse:
		s.Fuse = new(BlockExportOptionsFuse)
		if err := json.Unmarshal(data, s.Fuse); err != nil {
			s.Fuse = nil
			return err
		}
	case BlockExportTypeVduseBlk:
		s.VduseBlk = new(BlockExportOptionsVduseBlk)
		if err := json.Unmarshal(data, s.VduseBlk); err != nil {
			s.VduseBlk = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal BlockExportOptions received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// Block job options that can be changed after job creation.
//
// Since: 8.2
type BlockJobChangeOptions struct {
	// The job identifier
	Id string `json:"id"`
	// Variants fields
	Mirror *BlockJobChangeOptionsMirror `json:"-"`
	// Unbranched enum fields
	Commit         bool `json:"-"`
	Stream         bool `json:"-"`
	Backup         bool `json:"-"`
	Create         bool `json:"-"`
	Amend          bool `json:"-"`
	SnapshotLoad   bool `json:"-"`
	SnapshotSave   bool `json:"-"`
	SnapshotDelete bool `json:"-"`
}

func (s BlockJobChangeOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias BlockJobChangeOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Mirror != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Mirror); err == nil {
			m["type"] = JobTypeMirror
			bytes, err = json.Marshal(m)
		}
	}

	if s.Commit && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeCommit
			bytes, err = json.Marshal(m)
		}
	}

	if s.Stream && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeStream
			bytes, err = json.Marshal(m)
		}
	}

	if s.Backup && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeBackup
			bytes, err = json.Marshal(m)
		}
	}

	if s.Create && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeCreate
			bytes, err = json.Marshal(m)
		}
	}

	if s.Amend && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeAmend
			bytes, err = json.Marshal(m)
		}
	}

	if s.SnapshotLoad && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeSnapshotLoad
			bytes, err = json.Marshal(m)
		}
	}

	if s.SnapshotSave && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeSnapshotSave
			bytes, err = json.Marshal(m)
		}
	}

	if s.SnapshotDelete && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeSnapshotDelete
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal BlockJobChangeOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal BlockJobChangeOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *BlockJobChangeOptions) UnmarshalJSON(data []byte) error {
	type BlockJobChangeOptionsBase struct {
		Id   string  `json:"id"`
		Type JobType `json:"type"`
	}

	tmp := struct {
		BlockJobChangeOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.Id = tmp.Id
	switch tmp.Type {
	case JobTypeMirror:
		s.Mirror = new(BlockJobChangeOptionsMirror)
		if err := json.Unmarshal(data, s.Mirror); err != nil {
			s.Mirror = nil
			return err
		}
	case JobTypeCommit:
		s.Commit = true

	case JobTypeStream:
		s.Stream = true

	case JobTypeBackup:
		s.Backup = true

	case JobTypeCreate:
		s.Create = true

	case JobTypeAmend:
		s.Amend = true

	case JobTypeSnapshotLoad:
		s.SnapshotLoad = true

	case JobTypeSnapshotSave:
		s.SnapshotSave = true

	case JobTypeSnapshotDelete:
		s.SnapshotDelete = true

	default:
		return fmt.Errorf("unmarshal BlockJobChangeOptions received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// Information about a long-running block device operation.
//
// Since: 1.1
type BlockJobInfo struct {
	// The job identifier. Originally the device name but other values
	// are allowed since QEMU 2.7
	Device string `json:"device"`
	// Estimated @offset value at the completion of the job. This
	// value can arbitrarily change while the job is running, in both
	// directions.
	Len int64 `json:"len"`
	// Progress made until now. The unit is arbitrary and the value
	// can only meaningfully be used for the ratio of @offset to @len.
	// The value is monotonically increasing.
	Offset int64 `json:"offset"`
	// false if the job is known to be in a quiescent state, with no
	// pending I/O. (Since 1.3)
	Busy bool `json:"busy"`
	// whether the job is paused or, if @busy is true, will pause
	// itself as soon as possible. (Since 1.3)
	Paused bool `json:"paused"`
	// the rate limit, bytes per second
	Speed int64 `json:"speed"`
	// the status of the job (since 1.3)
	IoStatus BlockDeviceIoStatus `json:"io-status"`
	// true if the job may be completed (since 2.2)
	Ready bool `json:"ready"`
	// Current job state/status (since 2.12)
	Status JobStatus `json:"status"`
	// Job will finalize itself when PENDING, moving to the CONCLUDED
	// state. (since 2.12)
	AutoFinalize bool `json:"auto-finalize"`
	// Job will dismiss itself when CONCLUDED, moving to the NULL
	// state and disappearing from the query list. (since 2.12)
	AutoDismiss bool `json:"auto-dismiss"`
	// Error information if the job did not complete successfully. Not
	// set if the job completed successfully. (since 2.12.1)
	Error *string `json:"error,omitempty"`
	// Variants fields
	Mirror *BlockJobInfoMirror `json:"-"`
	// Unbranched enum fields
	Commit         bool `json:"-"`
	Stream         bool `json:"-"`
	Backup         bool `json:"-"`
	Create         bool `json:"-"`
	Amend          bool `json:"-"`
	SnapshotLoad   bool `json:"-"`
	SnapshotSave   bool `json:"-"`
	SnapshotDelete bool `json:"-"`
}

func (s BlockJobInfo) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias BlockJobInfo
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Mirror != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Mirror); err == nil {
			m["type"] = JobTypeMirror
			bytes, err = json.Marshal(m)
		}
	}

	if s.Commit && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeCommit
			bytes, err = json.Marshal(m)
		}
	}

	if s.Stream && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeStream
			bytes, err = json.Marshal(m)
		}
	}

	if s.Backup && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeBackup
			bytes, err = json.Marshal(m)
		}
	}

	if s.Create && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeCreate
			bytes, err = json.Marshal(m)
		}
	}

	if s.Amend && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeAmend
			bytes, err = json.Marshal(m)
		}
	}

	if s.SnapshotLoad && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeSnapshotLoad
			bytes, err = json.Marshal(m)
		}
	}

	if s.SnapshotSave && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeSnapshotSave
			bytes, err = json.Marshal(m)
		}
	}

	if s.SnapshotDelete && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = JobTypeSnapshotDelete
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal BlockJobInfo due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal BlockJobInfo unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *BlockJobInfo) UnmarshalJSON(data []byte) error {
	type BlockJobInfoBase struct {
		Type         JobType             `json:"type"`
		Device       string              `json:"device"`
		Len          int64               `json:"len"`
		Offset       int64               `json:"offset"`
		Busy         bool                `json:"busy"`
		Paused       bool                `json:"paused"`
		Speed        int64               `json:"speed"`
		IoStatus     BlockDeviceIoStatus `json:"io-status"`
		Ready        bool                `json:"ready"`
		Status       JobStatus           `json:"status"`
		AutoFinalize bool                `json:"auto-finalize"`
		AutoDismiss  bool                `json:"auto-dismiss"`
		Error        *string             `json:"error,omitempty"`
	}

	tmp := struct {
		BlockJobInfoBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.Device = tmp.Device
	s.Len = tmp.Len
	s.Offset = tmp.Offset
	s.Busy = tmp.Busy
	s.Paused = tmp.Paused
	s.Speed = tmp.Speed
	s.IoStatus = tmp.IoStatus
	s.Ready = tmp.Ready
	s.Status = tmp.Status
	s.AutoFinalize = tmp.AutoFinalize
	s.AutoDismiss = tmp.AutoDismiss
	s.Error = tmp.Error
	switch tmp.Type {
	case JobTypeMirror:
		s.Mirror = new(BlockJobInfoMirror)
		if err := json.Unmarshal(data, s.Mirror); err != nil {
			s.Mirror = nil
			return err
		}
	case JobTypeCommit:
		s.Commit = true

	case JobTypeStream:
		s.Stream = true

	case JobTypeBackup:
		s.Backup = true

	case JobTypeCreate:
		s.Create = true

	case JobTypeAmend:
		s.Amend = true

	case JobTypeSnapshotLoad:
		s.SnapshotLoad = true

	case JobTypeSnapshotSave:
		s.SnapshotSave = true

	case JobTypeSnapshotDelete:
		s.SnapshotDelete = true

	default:
		return fmt.Errorf("unmarshal BlockJobInfo received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// Block driver specific statistics
//
// Since: 4.2
type BlockStatsSpecific struct {
	// Variants fields
	File       *BlockStatsSpecificFile `json:"-"`
	HostDevice *BlockStatsSpecificFile `json:"-"`
	Nvme       *BlockStatsSpecificNvme `json:"-"`
	// Unbranched enum fields
	Blkdebug           bool `json:"-"`
	Blklogwrites       bool `json:"-"`
	Blkreplay          bool `json:"-"`
	Blkverify          bool `json:"-"`
	Bochs              bool `json:"-"`
	Cloop              bool `json:"-"`
	Compress           bool `json:"-"`
	CopyBeforeWrite    bool `json:"-"`
	CopyOnRead         bool `json:"-"`
	Dmg                bool `json:"-"`
	SnapshotAccess     bool `json:"-"`
	Ftp                bool `json:"-"`
	Ftps               bool `json:"-"`
	Gluster            bool `json:"-"`
	HostCdrom          bool `json:"-"`
	Http               bool `json:"-"`
	Https              bool `json:"-"`
	IoUring            bool `json:"-"`
	Iscsi              bool `json:"-"`
	Luks               bool `json:"-"`
	Nbd                bool `json:"-"`
	Nfs                bool `json:"-"`
	NullAio            bool `json:"-"`
	NullCo             bool `json:"-"`
	NvmeIoUring        bool `json:"-"`
	Parallels          bool `json:"-"`
	Preallocate        bool `json:"-"`
	Qcow               bool `json:"-"`
	Qcow2              bool `json:"-"`
	Qed                bool `json:"-"`
	Quorum             bool `json:"-"`
	Raw                bool `json:"-"`
	Rbd                bool `json:"-"`
	Replication        bool `json:"-"`
	Ssh                bool `json:"-"`
	Throttle           bool `json:"-"`
	Vdi                bool `json:"-"`
	Vhdx               bool `json:"-"`
	VirtioBlkVfioPci   bool `json:"-"`
	VirtioBlkVhostUser bool `json:"-"`
	VirtioBlkVhostVdpa bool `json:"-"`
	Vmdk               bool `json:"-"`
	Vpc                bool `json:"-"`
	Vvfat              bool `json:"-"`
}

func (s BlockStatsSpecific) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias BlockStatsSpecific
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.File != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.File); err == nil {
			m["driver"] = BlockdevDriverFile
			bytes, err = json.Marshal(m)
		}
	}

	if s.HostDevice != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.HostDevice); err == nil {
			m["driver"] = BlockdevDriverHost_Device
			bytes, err = json.Marshal(m)
		}
	}

	if s.Nvme != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Nvme); err == nil {
			m["driver"] = BlockdevDriverNvme
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blkdebug && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBlkdebug
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blklogwrites && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBlklogwrites
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blkreplay && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBlkreplay
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blkverify && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBlkverify
			bytes, err = json.Marshal(m)
		}
	}

	if s.Bochs && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBochs
			bytes, err = json.Marshal(m)
		}
	}

	if s.Cloop && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverCloop
			bytes, err = json.Marshal(m)
		}
	}

	if s.Compress && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverCompress
			bytes, err = json.Marshal(m)
		}
	}

	if s.CopyBeforeWrite && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverCopyBeforeWrite
			bytes, err = json.Marshal(m)
		}
	}

	if s.CopyOnRead && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverCopyOnRead
			bytes, err = json.Marshal(m)
		}
	}

	if s.Dmg && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverDmg
			bytes, err = json.Marshal(m)
		}
	}

	if s.SnapshotAccess && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverSnapshotAccess
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ftp && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverFtp
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ftps && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverFtps
			bytes, err = json.Marshal(m)
		}
	}

	if s.Gluster && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverGluster
			bytes, err = json.Marshal(m)
		}
	}

	if s.HostCdrom && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverHost_Cdrom
			bytes, err = json.Marshal(m)
		}
	}

	if s.Http && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverHttp
			bytes, err = json.Marshal(m)
		}
	}

	if s.Https && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverHttps
			bytes, err = json.Marshal(m)
		}
	}

	if s.IoUring && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverIo_Uring
			bytes, err = json.Marshal(m)
		}
	}

	if s.Iscsi && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverIscsi
			bytes, err = json.Marshal(m)
		}
	}

	if s.Luks && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverLuks
			bytes, err = json.Marshal(m)
		}
	}

	if s.Nbd && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNbd
			bytes, err = json.Marshal(m)
		}
	}

	if s.Nfs && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNfs
			bytes, err = json.Marshal(m)
		}
	}

	if s.NullAio && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNullAio
			bytes, err = json.Marshal(m)
		}
	}

	if s.NullCo && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNullCo
			bytes, err = json.Marshal(m)
		}
	}

	if s.NvmeIoUring && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNvmeIo_Uring
			bytes, err = json.Marshal(m)
		}
	}

	if s.Parallels && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverParallels
			bytes, err = json.Marshal(m)
		}
	}

	if s.Preallocate && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverPreallocate
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qcow && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverQcow
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qcow2 && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverQcow2
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qed && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverQed
			bytes, err = json.Marshal(m)
		}
	}

	if s.Quorum && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverQuorum
			bytes, err = json.Marshal(m)
		}
	}

	if s.Raw && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverRaw
			bytes, err = json.Marshal(m)
		}
	}

	if s.Rbd && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverRbd
			bytes, err = json.Marshal(m)
		}
	}

	if s.Replication && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverReplication
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ssh && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverSsh
			bytes, err = json.Marshal(m)
		}
	}

	if s.Throttle && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverThrottle
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vdi && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVdi
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vhdx && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVhdx
			bytes, err = json.Marshal(m)
		}
	}

	if s.VirtioBlkVfioPci && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVirtioBlkVfioPci
			bytes, err = json.Marshal(m)
		}
	}

	if s.VirtioBlkVhostUser && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVirtioBlkVhostUser
			bytes, err = json.Marshal(m)
		}
	}

	if s.VirtioBlkVhostVdpa && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVirtioBlkVhostVdpa
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vmdk && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVmdk
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vpc && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVpc
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vvfat && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVvfat
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal BlockStatsSpecific due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal BlockStatsSpecific unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *BlockStatsSpecific) UnmarshalJSON(data []byte) error {
	type BlockStatsSpecificBase struct {
		Driver BlockdevDriver `json:"driver"`
	}

	tmp := struct {
		BlockStatsSpecificBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Driver {
	case BlockdevDriverFile:
		s.File = new(BlockStatsSpecificFile)
		if err := json.Unmarshal(data, s.File); err != nil {
			s.File = nil
			return err
		}
	case BlockdevDriverHost_Device:
		s.HostDevice = new(BlockStatsSpecificFile)
		if err := json.Unmarshal(data, s.HostDevice); err != nil {
			s.HostDevice = nil
			return err
		}
	case BlockdevDriverNvme:
		s.Nvme = new(BlockStatsSpecificNvme)
		if err := json.Unmarshal(data, s.Nvme); err != nil {
			s.Nvme = nil
			return err
		}
	case BlockdevDriverBlkdebug:
		s.Blkdebug = true

	case BlockdevDriverBlklogwrites:
		s.Blklogwrites = true

	case BlockdevDriverBlkreplay:
		s.Blkreplay = true

	case BlockdevDriverBlkverify:
		s.Blkverify = true

	case BlockdevDriverBochs:
		s.Bochs = true

	case BlockdevDriverCloop:
		s.Cloop = true

	case BlockdevDriverCompress:
		s.Compress = true

	case BlockdevDriverCopyBeforeWrite:
		s.CopyBeforeWrite = true

	case BlockdevDriverCopyOnRead:
		s.CopyOnRead = true

	case BlockdevDriverDmg:
		s.Dmg = true

	case BlockdevDriverSnapshotAccess:
		s.SnapshotAccess = true

	case BlockdevDriverFtp:
		s.Ftp = true

	case BlockdevDriverFtps:
		s.Ftps = true

	case BlockdevDriverGluster:
		s.Gluster = true

	case BlockdevDriverHost_Cdrom:
		s.HostCdrom = true

	case BlockdevDriverHttp:
		s.Http = true

	case BlockdevDriverHttps:
		s.Https = true

	case BlockdevDriverIo_Uring:
		s.IoUring = true

	case BlockdevDriverIscsi:
		s.Iscsi = true

	case BlockdevDriverLuks:
		s.Luks = true

	case BlockdevDriverNbd:
		s.Nbd = true

	case BlockdevDriverNfs:
		s.Nfs = true

	case BlockdevDriverNullAio:
		s.NullAio = true

	case BlockdevDriverNullCo:
		s.NullCo = true

	case BlockdevDriverNvmeIo_Uring:
		s.NvmeIoUring = true

	case BlockdevDriverParallels:
		s.Parallels = true

	case BlockdevDriverPreallocate:
		s.Preallocate = true

	case BlockdevDriverQcow:
		s.Qcow = true

	case BlockdevDriverQcow2:
		s.Qcow2 = true

	case BlockdevDriverQed:
		s.Qed = true

	case BlockdevDriverQuorum:
		s.Quorum = true

	case BlockdevDriverRaw:
		s.Raw = true

	case BlockdevDriverRbd:
		s.Rbd = true

	case BlockdevDriverReplication:
		s.Replication = true

	case BlockdevDriverSsh:
		s.Ssh = true

	case BlockdevDriverThrottle:
		s.Throttle = true

	case BlockdevDriverVdi:
		s.Vdi = true

	case BlockdevDriverVhdx:
		s.Vhdx = true

	case BlockdevDriverVirtioBlkVfioPci:
		s.VirtioBlkVfioPci = true

	case BlockdevDriverVirtioBlkVhostUser:
		s.VirtioBlkVhostUser = true

	case BlockdevDriverVirtioBlkVhostVdpa:
		s.VirtioBlkVhostVdpa = true

	case BlockdevDriverVmdk:
		s.Vmdk = true

	case BlockdevDriverVpc:
		s.Vpc = true

	case BlockdevDriverVvfat:
		s.Vvfat = true

	default:
		return fmt.Errorf("unmarshal BlockStatsSpecific received unrecognized value '%s'",
			tmp.Driver)
	}
	return nil
}

// Options for amending an image format
//
// Since: 5.1
type BlockdevAmendOptions struct {
	// Variants fields
	Luks  *BlockdevAmendOptionsLUKS  `json:"-"`
	Qcow2 *BlockdevAmendOptionsQcow2 `json:"-"`
	// Unbranched enum fields
	Blkdebug           bool `json:"-"`
	Blklogwrites       bool `json:"-"`
	Blkreplay          bool `json:"-"`
	Blkverify          bool `json:"-"`
	Bochs              bool `json:"-"`
	Cloop              bool `json:"-"`
	Compress           bool `json:"-"`
	CopyBeforeWrite    bool `json:"-"`
	CopyOnRead         bool `json:"-"`
	Dmg                bool `json:"-"`
	File               bool `json:"-"`
	SnapshotAccess     bool `json:"-"`
	Ftp                bool `json:"-"`
	Ftps               bool `json:"-"`
	Gluster            bool `json:"-"`
	HostCdrom          bool `json:"-"`
	HostDevice         bool `json:"-"`
	Http               bool `json:"-"`
	Https              bool `json:"-"`
	IoUring            bool `json:"-"`
	Iscsi              bool `json:"-"`
	Nbd                bool `json:"-"`
	Nfs                bool `json:"-"`
	NullAio            bool `json:"-"`
	NullCo             bool `json:"-"`
	Nvme               bool `json:"-"`
	NvmeIoUring        bool `json:"-"`
	Parallels          bool `json:"-"`
	Preallocate        bool `json:"-"`
	Qcow               bool `json:"-"`
	Qed                bool `json:"-"`
	Quorum             bool `json:"-"`
	Raw                bool `json:"-"`
	Rbd                bool `json:"-"`
	Replication        bool `json:"-"`
	Ssh                bool `json:"-"`
	Throttle           bool `json:"-"`
	Vdi                bool `json:"-"`
	Vhdx               bool `json:"-"`
	VirtioBlkVfioPci   bool `json:"-"`
	VirtioBlkVhostUser bool `json:"-"`
	VirtioBlkVhostVdpa bool `json:"-"`
	Vmdk               bool `json:"-"`
	Vpc                bool `json:"-"`
	Vvfat              bool `json:"-"`
}

func (s BlockdevAmendOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias BlockdevAmendOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Luks != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Luks); err == nil {
			m["driver"] = BlockdevDriverLuks
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qcow2 != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Qcow2); err == nil {
			m["driver"] = BlockdevDriverQcow2
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blkdebug && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBlkdebug
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blklogwrites && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBlklogwrites
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blkreplay && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBlkreplay
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blkverify && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBlkverify
			bytes, err = json.Marshal(m)
		}
	}

	if s.Bochs && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBochs
			bytes, err = json.Marshal(m)
		}
	}

	if s.Cloop && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverCloop
			bytes, err = json.Marshal(m)
		}
	}

	if s.Compress && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverCompress
			bytes, err = json.Marshal(m)
		}
	}

	if s.CopyBeforeWrite && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverCopyBeforeWrite
			bytes, err = json.Marshal(m)
		}
	}

	if s.CopyOnRead && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverCopyOnRead
			bytes, err = json.Marshal(m)
		}
	}

	if s.Dmg && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverDmg
			bytes, err = json.Marshal(m)
		}
	}

	if s.File && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverFile
			bytes, err = json.Marshal(m)
		}
	}

	if s.SnapshotAccess && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverSnapshotAccess
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ftp && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverFtp
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ftps && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverFtps
			bytes, err = json.Marshal(m)
		}
	}

	if s.Gluster && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverGluster
			bytes, err = json.Marshal(m)
		}
	}

	if s.HostCdrom && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverHost_Cdrom
			bytes, err = json.Marshal(m)
		}
	}

	if s.HostDevice && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverHost_Device
			bytes, err = json.Marshal(m)
		}
	}

	if s.Http && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverHttp
			bytes, err = json.Marshal(m)
		}
	}

	if s.Https && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverHttps
			bytes, err = json.Marshal(m)
		}
	}

	if s.IoUring && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverIo_Uring
			bytes, err = json.Marshal(m)
		}
	}

	if s.Iscsi && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverIscsi
			bytes, err = json.Marshal(m)
		}
	}

	if s.Nbd && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNbd
			bytes, err = json.Marshal(m)
		}
	}

	if s.Nfs && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNfs
			bytes, err = json.Marshal(m)
		}
	}

	if s.NullAio && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNullAio
			bytes, err = json.Marshal(m)
		}
	}

	if s.NullCo && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNullCo
			bytes, err = json.Marshal(m)
		}
	}

	if s.Nvme && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNvme
			bytes, err = json.Marshal(m)
		}
	}

	if s.NvmeIoUring && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNvmeIo_Uring
			bytes, err = json.Marshal(m)
		}
	}

	if s.Parallels && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverParallels
			bytes, err = json.Marshal(m)
		}
	}

	if s.Preallocate && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverPreallocate
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qcow && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverQcow
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qed && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverQed
			bytes, err = json.Marshal(m)
		}
	}

	if s.Quorum && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverQuorum
			bytes, err = json.Marshal(m)
		}
	}

	if s.Raw && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverRaw
			bytes, err = json.Marshal(m)
		}
	}

	if s.Rbd && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverRbd
			bytes, err = json.Marshal(m)
		}
	}

	if s.Replication && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverReplication
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ssh && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverSsh
			bytes, err = json.Marshal(m)
		}
	}

	if s.Throttle && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverThrottle
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vdi && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVdi
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vhdx && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVhdx
			bytes, err = json.Marshal(m)
		}
	}

	if s.VirtioBlkVfioPci && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVirtioBlkVfioPci
			bytes, err = json.Marshal(m)
		}
	}

	if s.VirtioBlkVhostUser && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVirtioBlkVhostUser
			bytes, err = json.Marshal(m)
		}
	}

	if s.VirtioBlkVhostVdpa && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVirtioBlkVhostVdpa
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vmdk && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVmdk
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vpc && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVpc
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vvfat && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVvfat
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal BlockdevAmendOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal BlockdevAmendOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *BlockdevAmendOptions) UnmarshalJSON(data []byte) error {
	type BlockdevAmendOptionsBase struct {
		Driver BlockdevDriver `json:"driver"`
	}

	tmp := struct {
		BlockdevAmendOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Driver {
	case BlockdevDriverLuks:
		s.Luks = new(BlockdevAmendOptionsLUKS)
		if err := json.Unmarshal(data, s.Luks); err != nil {
			s.Luks = nil
			return err
		}
	case BlockdevDriverQcow2:
		s.Qcow2 = new(BlockdevAmendOptionsQcow2)
		if err := json.Unmarshal(data, s.Qcow2); err != nil {
			s.Qcow2 = nil
			return err
		}
	case BlockdevDriverBlkdebug:
		s.Blkdebug = true

	case BlockdevDriverBlklogwrites:
		s.Blklogwrites = true

	case BlockdevDriverBlkreplay:
		s.Blkreplay = true

	case BlockdevDriverBlkverify:
		s.Blkverify = true

	case BlockdevDriverBochs:
		s.Bochs = true

	case BlockdevDriverCloop:
		s.Cloop = true

	case BlockdevDriverCompress:
		s.Compress = true

	case BlockdevDriverCopyBeforeWrite:
		s.CopyBeforeWrite = true

	case BlockdevDriverCopyOnRead:
		s.CopyOnRead = true

	case BlockdevDriverDmg:
		s.Dmg = true

	case BlockdevDriverFile:
		s.File = true

	case BlockdevDriverSnapshotAccess:
		s.SnapshotAccess = true

	case BlockdevDriverFtp:
		s.Ftp = true

	case BlockdevDriverFtps:
		s.Ftps = true

	case BlockdevDriverGluster:
		s.Gluster = true

	case BlockdevDriverHost_Cdrom:
		s.HostCdrom = true

	case BlockdevDriverHost_Device:
		s.HostDevice = true

	case BlockdevDriverHttp:
		s.Http = true

	case BlockdevDriverHttps:
		s.Https = true

	case BlockdevDriverIo_Uring:
		s.IoUring = true

	case BlockdevDriverIscsi:
		s.Iscsi = true

	case BlockdevDriverNbd:
		s.Nbd = true

	case BlockdevDriverNfs:
		s.Nfs = true

	case BlockdevDriverNullAio:
		s.NullAio = true

	case BlockdevDriverNullCo:
		s.NullCo = true

	case BlockdevDriverNvme:
		s.Nvme = true

	case BlockdevDriverNvmeIo_Uring:
		s.NvmeIoUring = true

	case BlockdevDriverParallels:
		s.Parallels = true

	case BlockdevDriverPreallocate:
		s.Preallocate = true

	case BlockdevDriverQcow:
		s.Qcow = true

	case BlockdevDriverQed:
		s.Qed = true

	case BlockdevDriverQuorum:
		s.Quorum = true

	case BlockdevDriverRaw:
		s.Raw = true

	case BlockdevDriverRbd:
		s.Rbd = true

	case BlockdevDriverReplication:
		s.Replication = true

	case BlockdevDriverSsh:
		s.Ssh = true

	case BlockdevDriverThrottle:
		s.Throttle = true

	case BlockdevDriverVdi:
		s.Vdi = true

	case BlockdevDriverVhdx:
		s.Vhdx = true

	case BlockdevDriverVirtioBlkVfioPci:
		s.VirtioBlkVfioPci = true

	case BlockdevDriverVirtioBlkVhostUser:
		s.VirtioBlkVhostUser = true

	case BlockdevDriverVirtioBlkVhostVdpa:
		s.VirtioBlkVhostVdpa = true

	case BlockdevDriverVmdk:
		s.Vmdk = true

	case BlockdevDriverVpc:
		s.Vpc = true

	case BlockdevDriverVvfat:
		s.Vvfat = true

	default:
		return fmt.Errorf("unmarshal BlockdevAmendOptions received unrecognized value '%s'",
			tmp.Driver)
	}
	return nil
}

// Options for creating an image format on a given node.
//
// Since: 2.12
type BlockdevCreateOptions struct {
	// Variants fields
	File      *BlockdevCreateOptionsFile      `json:"-"`
	Gluster   *BlockdevCreateOptionsGluster   `json:"-"`
	Luks      *BlockdevCreateOptionsLUKS      `json:"-"`
	Nfs       *BlockdevCreateOptionsNfs       `json:"-"`
	Parallels *BlockdevCreateOptionsParallels `json:"-"`
	Qcow      *BlockdevCreateOptionsQcow      `json:"-"`
	Qcow2     *BlockdevCreateOptionsQcow2     `json:"-"`
	Qed       *BlockdevCreateOptionsQed       `json:"-"`
	Rbd       *BlockdevCreateOptionsRbd       `json:"-"`
	Ssh       *BlockdevCreateOptionsSsh       `json:"-"`
	Vdi       *BlockdevCreateOptionsVdi       `json:"-"`
	Vhdx      *BlockdevCreateOptionsVhdx      `json:"-"`
	Vmdk      *BlockdevCreateOptionsVmdk      `json:"-"`
	Vpc       *BlockdevCreateOptionsVpc       `json:"-"`
	// Unbranched enum fields
	Blkdebug           bool `json:"-"`
	Blklogwrites       bool `json:"-"`
	Blkreplay          bool `json:"-"`
	Blkverify          bool `json:"-"`
	Bochs              bool `json:"-"`
	Cloop              bool `json:"-"`
	Compress           bool `json:"-"`
	CopyBeforeWrite    bool `json:"-"`
	CopyOnRead         bool `json:"-"`
	Dmg                bool `json:"-"`
	SnapshotAccess     bool `json:"-"`
	Ftp                bool `json:"-"`
	Ftps               bool `json:"-"`
	HostCdrom          bool `json:"-"`
	HostDevice         bool `json:"-"`
	Http               bool `json:"-"`
	Https              bool `json:"-"`
	IoUring            bool `json:"-"`
	Iscsi              bool `json:"-"`
	Nbd                bool `json:"-"`
	NullAio            bool `json:"-"`
	NullCo             bool `json:"-"`
	Nvme               bool `json:"-"`
	NvmeIoUring        bool `json:"-"`
	Preallocate        bool `json:"-"`
	Quorum             bool `json:"-"`
	Raw                bool `json:"-"`
	Replication        bool `json:"-"`
	Throttle           bool `json:"-"`
	VirtioBlkVfioPci   bool `json:"-"`
	VirtioBlkVhostUser bool `json:"-"`
	VirtioBlkVhostVdpa bool `json:"-"`
	Vvfat              bool `json:"-"`
}

func (s BlockdevCreateOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias BlockdevCreateOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.File != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.File); err == nil {
			m["driver"] = BlockdevDriverFile
			bytes, err = json.Marshal(m)
		}
	}

	if s.Gluster != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Gluster); err == nil {
			m["driver"] = BlockdevDriverGluster
			bytes, err = json.Marshal(m)
		}
	}

	if s.Luks != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Luks); err == nil {
			m["driver"] = BlockdevDriverLuks
			bytes, err = json.Marshal(m)
		}
	}

	if s.Nfs != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Nfs); err == nil {
			m["driver"] = BlockdevDriverNfs
			bytes, err = json.Marshal(m)
		}
	}

	if s.Parallels != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Parallels); err == nil {
			m["driver"] = BlockdevDriverParallels
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qcow != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Qcow); err == nil {
			m["driver"] = BlockdevDriverQcow
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qcow2 != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Qcow2); err == nil {
			m["driver"] = BlockdevDriverQcow2
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qed != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Qed); err == nil {
			m["driver"] = BlockdevDriverQed
			bytes, err = json.Marshal(m)
		}
	}

	if s.Rbd != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Rbd); err == nil {
			m["driver"] = BlockdevDriverRbd
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ssh != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Ssh); err == nil {
			m["driver"] = BlockdevDriverSsh
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vdi != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vdi); err == nil {
			m["driver"] = BlockdevDriverVdi
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vhdx != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vhdx); err == nil {
			m["driver"] = BlockdevDriverVhdx
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vmdk != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vmdk); err == nil {
			m["driver"] = BlockdevDriverVmdk
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vpc != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vpc); err == nil {
			m["driver"] = BlockdevDriverVpc
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blkdebug && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBlkdebug
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blklogwrites && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBlklogwrites
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blkreplay && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBlkreplay
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blkverify && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBlkverify
			bytes, err = json.Marshal(m)
		}
	}

	if s.Bochs && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverBochs
			bytes, err = json.Marshal(m)
		}
	}

	if s.Cloop && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverCloop
			bytes, err = json.Marshal(m)
		}
	}

	if s.Compress && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverCompress
			bytes, err = json.Marshal(m)
		}
	}

	if s.CopyBeforeWrite && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverCopyBeforeWrite
			bytes, err = json.Marshal(m)
		}
	}

	if s.CopyOnRead && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverCopyOnRead
			bytes, err = json.Marshal(m)
		}
	}

	if s.Dmg && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverDmg
			bytes, err = json.Marshal(m)
		}
	}

	if s.SnapshotAccess && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverSnapshotAccess
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ftp && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverFtp
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ftps && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverFtps
			bytes, err = json.Marshal(m)
		}
	}

	if s.HostCdrom && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverHost_Cdrom
			bytes, err = json.Marshal(m)
		}
	}

	if s.HostDevice && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverHost_Device
			bytes, err = json.Marshal(m)
		}
	}

	if s.Http && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverHttp
			bytes, err = json.Marshal(m)
		}
	}

	if s.Https && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverHttps
			bytes, err = json.Marshal(m)
		}
	}

	if s.IoUring && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverIo_Uring
			bytes, err = json.Marshal(m)
		}
	}

	if s.Iscsi && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverIscsi
			bytes, err = json.Marshal(m)
		}
	}

	if s.Nbd && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNbd
			bytes, err = json.Marshal(m)
		}
	}

	if s.NullAio && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNullAio
			bytes, err = json.Marshal(m)
		}
	}

	if s.NullCo && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNullCo
			bytes, err = json.Marshal(m)
		}
	}

	if s.Nvme && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNvme
			bytes, err = json.Marshal(m)
		}
	}

	if s.NvmeIoUring && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverNvmeIo_Uring
			bytes, err = json.Marshal(m)
		}
	}

	if s.Preallocate && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverPreallocate
			bytes, err = json.Marshal(m)
		}
	}

	if s.Quorum && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverQuorum
			bytes, err = json.Marshal(m)
		}
	}

	if s.Raw && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverRaw
			bytes, err = json.Marshal(m)
		}
	}

	if s.Replication && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverReplication
			bytes, err = json.Marshal(m)
		}
	}

	if s.Throttle && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverThrottle
			bytes, err = json.Marshal(m)
		}
	}

	if s.VirtioBlkVfioPci && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVirtioBlkVfioPci
			bytes, err = json.Marshal(m)
		}
	}

	if s.VirtioBlkVhostUser && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVirtioBlkVhostUser
			bytes, err = json.Marshal(m)
		}
	}

	if s.VirtioBlkVhostVdpa && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVirtioBlkVhostVdpa
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vvfat && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["driver"] = BlockdevDriverVvfat
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal BlockdevCreateOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal BlockdevCreateOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *BlockdevCreateOptions) UnmarshalJSON(data []byte) error {
	type BlockdevCreateOptionsBase struct {
		Driver BlockdevDriver `json:"driver"`
	}

	tmp := struct {
		BlockdevCreateOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Driver {
	case BlockdevDriverFile:
		s.File = new(BlockdevCreateOptionsFile)
		if err := json.Unmarshal(data, s.File); err != nil {
			s.File = nil
			return err
		}
	case BlockdevDriverGluster:
		s.Gluster = new(BlockdevCreateOptionsGluster)
		if err := json.Unmarshal(data, s.Gluster); err != nil {
			s.Gluster = nil
			return err
		}
	case BlockdevDriverLuks:
		s.Luks = new(BlockdevCreateOptionsLUKS)
		if err := json.Unmarshal(data, s.Luks); err != nil {
			s.Luks = nil
			return err
		}
	case BlockdevDriverNfs:
		s.Nfs = new(BlockdevCreateOptionsNfs)
		if err := json.Unmarshal(data, s.Nfs); err != nil {
			s.Nfs = nil
			return err
		}
	case BlockdevDriverParallels:
		s.Parallels = new(BlockdevCreateOptionsParallels)
		if err := json.Unmarshal(data, s.Parallels); err != nil {
			s.Parallels = nil
			return err
		}
	case BlockdevDriverQcow:
		s.Qcow = new(BlockdevCreateOptionsQcow)
		if err := json.Unmarshal(data, s.Qcow); err != nil {
			s.Qcow = nil
			return err
		}
	case BlockdevDriverQcow2:
		s.Qcow2 = new(BlockdevCreateOptionsQcow2)
		if err := json.Unmarshal(data, s.Qcow2); err != nil {
			s.Qcow2 = nil
			return err
		}
	case BlockdevDriverQed:
		s.Qed = new(BlockdevCreateOptionsQed)
		if err := json.Unmarshal(data, s.Qed); err != nil {
			s.Qed = nil
			return err
		}
	case BlockdevDriverRbd:
		s.Rbd = new(BlockdevCreateOptionsRbd)
		if err := json.Unmarshal(data, s.Rbd); err != nil {
			s.Rbd = nil
			return err
		}
	case BlockdevDriverSsh:
		s.Ssh = new(BlockdevCreateOptionsSsh)
		if err := json.Unmarshal(data, s.Ssh); err != nil {
			s.Ssh = nil
			return err
		}
	case BlockdevDriverVdi:
		s.Vdi = new(BlockdevCreateOptionsVdi)
		if err := json.Unmarshal(data, s.Vdi); err != nil {
			s.Vdi = nil
			return err
		}
	case BlockdevDriverVhdx:
		s.Vhdx = new(BlockdevCreateOptionsVhdx)
		if err := json.Unmarshal(data, s.Vhdx); err != nil {
			s.Vhdx = nil
			return err
		}
	case BlockdevDriverVmdk:
		s.Vmdk = new(BlockdevCreateOptionsVmdk)
		if err := json.Unmarshal(data, s.Vmdk); err != nil {
			s.Vmdk = nil
			return err
		}
	case BlockdevDriverVpc:
		s.Vpc = new(BlockdevCreateOptionsVpc)
		if err := json.Unmarshal(data, s.Vpc); err != nil {
			s.Vpc = nil
			return err
		}
	case BlockdevDriverBlkdebug:
		s.Blkdebug = true

	case BlockdevDriverBlklogwrites:
		s.Blklogwrites = true

	case BlockdevDriverBlkreplay:
		s.Blkreplay = true

	case BlockdevDriverBlkverify:
		s.Blkverify = true

	case BlockdevDriverBochs:
		s.Bochs = true

	case BlockdevDriverCloop:
		s.Cloop = true

	case BlockdevDriverCompress:
		s.Compress = true

	case BlockdevDriverCopyBeforeWrite:
		s.CopyBeforeWrite = true

	case BlockdevDriverCopyOnRead:
		s.CopyOnRead = true

	case BlockdevDriverDmg:
		s.Dmg = true

	case BlockdevDriverSnapshotAccess:
		s.SnapshotAccess = true

	case BlockdevDriverFtp:
		s.Ftp = true

	case BlockdevDriverFtps:
		s.Ftps = true

	case BlockdevDriverHost_Cdrom:
		s.HostCdrom = true

	case BlockdevDriverHost_Device:
		s.HostDevice = true

	case BlockdevDriverHttp:
		s.Http = true

	case BlockdevDriverHttps:
		s.Https = true

	case BlockdevDriverIo_Uring:
		s.IoUring = true

	case BlockdevDriverIscsi:
		s.Iscsi = true

	case BlockdevDriverNbd:
		s.Nbd = true

	case BlockdevDriverNullAio:
		s.NullAio = true

	case BlockdevDriverNullCo:
		s.NullCo = true

	case BlockdevDriverNvme:
		s.Nvme = true

	case BlockdevDriverNvmeIo_Uring:
		s.NvmeIoUring = true

	case BlockdevDriverPreallocate:
		s.Preallocate = true

	case BlockdevDriverQuorum:
		s.Quorum = true

	case BlockdevDriverRaw:
		s.Raw = true

	case BlockdevDriverReplication:
		s.Replication = true

	case BlockdevDriverThrottle:
		s.Throttle = true

	case BlockdevDriverVirtioBlkVfioPci:
		s.VirtioBlkVfioPci = true

	case BlockdevDriverVirtioBlkVhostUser:
		s.VirtioBlkVhostUser = true

	case BlockdevDriverVirtioBlkVhostVdpa:
		s.VirtioBlkVhostVdpa = true

	case BlockdevDriverVvfat:
		s.Vvfat = true

	default:
		return fmt.Errorf("unmarshal BlockdevCreateOptions received unrecognized value '%s'",
			tmp.Driver)
	}
	return nil
}

// Options for creating a block device.  Many options are available
// for all block devices, independent of the block driver:
//
// Since: 2.9
type BlockdevOptions struct {
	// the node name of the new node (Since 2.0). This option is
	// required on the top level of blockdev-add. Valid node names
	// start with an alphabetic character and may contain only
	// alphanumeric characters, '-', '.' and '_'. Their maximum length
	// is 31 characters.
	NodeName *string `json:"node-name,omitempty"`
	// discard-related options (default: ignore)
	Discard *BlockdevDiscardOptions `json:"discard,omitempty"`
	// cache-related options
	Cache *BlockdevCacheOptions `json:"cache,omitempty"`
	// whether the block node should be activated (default: true).
	// Having inactive block nodes is useful primarily for migration
	// because it allows opening an image on the destination while the
	// source is still holding locks for it. (Since 10.0)
	Active *bool `json:"active,omitempty"`
	// whether the block device should be read-only (default: false).
	// Note that some block drivers support only read-only access,
	// either generally or in certain configurations. In this case,
	// the default value does not work and the option must be
	// specified explicitly.
	ReadOnly *bool `json:"read-only,omitempty"`
	// if true and @read-only is false, QEMU may automatically decide
	// not to open the image read-write as requested, but fall back to
	// read-only instead (and switch between the modes later), e.g.
	// depending on whether the image file is writable or whether a
	// writing user is attached to the node (default: false, since
	// 3.1)
	AutoReadOnly *bool `json:"auto-read-only,omitempty"`
	// force share all permission on added nodes. Requires read-
	// only=true. (Since 2.10)
	ForceShare *bool `json:"force-share,omitempty"`
	// detect and optimize zero writes (Since 2.1) (default: off)
	DetectZeroes *BlockdevDetectZeroesOptions `json:"detect-zeroes,omitempty"`
	// Variants fields
	Blkdebug           *BlockdevOptionsBlkdebug           `json:"-"`
	Blklogwrites       *BlockdevOptionsBlklogwrites       `json:"-"`
	Blkverify          *BlockdevOptionsBlkverify          `json:"-"`
	Blkreplay          *BlockdevOptionsBlkreplay          `json:"-"`
	Bochs              *BlockdevOptionsGenericFormat      `json:"-"`
	Cloop              *BlockdevOptionsGenericFormat      `json:"-"`
	Compress           *BlockdevOptionsGenericFormat      `json:"-"`
	CopyBeforeWrite    *BlockdevOptionsCbw                `json:"-"`
	CopyOnRead         *BlockdevOptionsCor                `json:"-"`
	Dmg                *BlockdevOptionsGenericFormat      `json:"-"`
	File               *BlockdevOptionsFile               `json:"-"`
	Ftp                *BlockdevOptionsCurlFtp            `json:"-"`
	Ftps               *BlockdevOptionsCurlFtps           `json:"-"`
	Gluster            *BlockdevOptionsGluster            `json:"-"`
	HostCdrom          *BlockdevOptionsFile               `json:"-"`
	HostDevice         *BlockdevOptionsFile               `json:"-"`
	Http               *BlockdevOptionsCurlHttp           `json:"-"`
	Https              *BlockdevOptionsCurlHttps          `json:"-"`
	IoUring            *BlockdevOptionsIoUring            `json:"-"`
	Iscsi              *BlockdevOptionsIscsi              `json:"-"`
	Luks               *BlockdevOptionsLUKS               `json:"-"`
	Nbd                *BlockdevOptionsNbd                `json:"-"`
	Nfs                *BlockdevOptionsNfs                `json:"-"`
	NullAio            *BlockdevOptionsNull               `json:"-"`
	NullCo             *BlockdevOptionsNull               `json:"-"`
	Nvme               *BlockdevOptionsNVMe               `json:"-"`
	NvmeIoUring        *BlockdevOptionsNvmeIoUring        `json:"-"`
	Parallels          *BlockdevOptionsGenericFormat      `json:"-"`
	Preallocate        *BlockdevOptionsPreallocate        `json:"-"`
	Qcow2              *BlockdevOptionsQcow2              `json:"-"`
	Qcow               *BlockdevOptionsQcow               `json:"-"`
	Qed                *BlockdevOptionsGenericCOWFormat   `json:"-"`
	Quorum             *BlockdevOptionsQuorum             `json:"-"`
	Raw                *BlockdevOptionsRaw                `json:"-"`
	Rbd                *BlockdevOptionsRbd                `json:"-"`
	Replication        *BlockdevOptionsReplication        `json:"-"`
	SnapshotAccess     *BlockdevOptionsGenericFormat      `json:"-"`
	Ssh                *BlockdevOptionsSsh                `json:"-"`
	Throttle           *BlockdevOptionsThrottle           `json:"-"`
	Vdi                *BlockdevOptionsGenericFormat      `json:"-"`
	Vhdx               *BlockdevOptionsGenericFormat      `json:"-"`
	VirtioBlkVfioPci   *BlockdevOptionsVirtioBlkVfioPci   `json:"-"`
	VirtioBlkVhostUser *BlockdevOptionsVirtioBlkVhostUser `json:"-"`
	VirtioBlkVhostVdpa *BlockdevOptionsVirtioBlkVhostVdpa `json:"-"`
	Vmdk               *BlockdevOptionsGenericCOWFormat   `json:"-"`
	Vpc                *BlockdevOptionsGenericFormat      `json:"-"`
	Vvfat              *BlockdevOptionsVVFAT              `json:"-"`
}

func (s BlockdevOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias BlockdevOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Blkdebug != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Blkdebug); err == nil {
			m["driver"] = BlockdevDriverBlkdebug
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blklogwrites != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Blklogwrites); err == nil {
			m["driver"] = BlockdevDriverBlklogwrites
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blkverify != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Blkverify); err == nil {
			m["driver"] = BlockdevDriverBlkverify
			bytes, err = json.Marshal(m)
		}
	}

	if s.Blkreplay != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Blkreplay); err == nil {
			m["driver"] = BlockdevDriverBlkreplay
			bytes, err = json.Marshal(m)
		}
	}

	if s.Bochs != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Bochs); err == nil {
			m["driver"] = BlockdevDriverBochs
			bytes, err = json.Marshal(m)
		}
	}

	if s.Cloop != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Cloop); err == nil {
			m["driver"] = BlockdevDriverCloop
			bytes, err = json.Marshal(m)
		}
	}

	if s.Compress != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Compress); err == nil {
			m["driver"] = BlockdevDriverCompress
			bytes, err = json.Marshal(m)
		}
	}

	if s.CopyBeforeWrite != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.CopyBeforeWrite); err == nil {
			m["driver"] = BlockdevDriverCopyBeforeWrite
			bytes, err = json.Marshal(m)
		}
	}

	if s.CopyOnRead != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.CopyOnRead); err == nil {
			m["driver"] = BlockdevDriverCopyOnRead
			bytes, err = json.Marshal(m)
		}
	}

	if s.Dmg != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Dmg); err == nil {
			m["driver"] = BlockdevDriverDmg
			bytes, err = json.Marshal(m)
		}
	}

	if s.File != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.File); err == nil {
			m["driver"] = BlockdevDriverFile
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ftp != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Ftp); err == nil {
			m["driver"] = BlockdevDriverFtp
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ftps != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Ftps); err == nil {
			m["driver"] = BlockdevDriverFtps
			bytes, err = json.Marshal(m)
		}
	}

	if s.Gluster != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Gluster); err == nil {
			m["driver"] = BlockdevDriverGluster
			bytes, err = json.Marshal(m)
		}
	}

	if s.HostCdrom != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.HostCdrom); err == nil {
			m["driver"] = BlockdevDriverHost_Cdrom
			bytes, err = json.Marshal(m)
		}
	}

	if s.HostDevice != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.HostDevice); err == nil {
			m["driver"] = BlockdevDriverHost_Device
			bytes, err = json.Marshal(m)
		}
	}

	if s.Http != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Http); err == nil {
			m["driver"] = BlockdevDriverHttp
			bytes, err = json.Marshal(m)
		}
	}

	if s.Https != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Https); err == nil {
			m["driver"] = BlockdevDriverHttps
			bytes, err = json.Marshal(m)
		}
	}

	if s.IoUring != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.IoUring); err == nil {
			m["driver"] = BlockdevDriverIo_Uring
			bytes, err = json.Marshal(m)
		}
	}

	if s.Iscsi != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Iscsi); err == nil {
			m["driver"] = BlockdevDriverIscsi
			bytes, err = json.Marshal(m)
		}
	}

	if s.Luks != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Luks); err == nil {
			m["driver"] = BlockdevDriverLuks
			bytes, err = json.Marshal(m)
		}
	}

	if s.Nbd != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Nbd); err == nil {
			m["driver"] = BlockdevDriverNbd
			bytes, err = json.Marshal(m)
		}
	}

	if s.Nfs != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Nfs); err == nil {
			m["driver"] = BlockdevDriverNfs
			bytes, err = json.Marshal(m)
		}
	}

	if s.NullAio != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.NullAio); err == nil {
			m["driver"] = BlockdevDriverNullAio
			bytes, err = json.Marshal(m)
		}
	}

	if s.NullCo != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.NullCo); err == nil {
			m["driver"] = BlockdevDriverNullCo
			bytes, err = json.Marshal(m)
		}
	}

	if s.Nvme != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Nvme); err == nil {
			m["driver"] = BlockdevDriverNvme
			bytes, err = json.Marshal(m)
		}
	}

	if s.NvmeIoUring != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.NvmeIoUring); err == nil {
			m["driver"] = BlockdevDriverNvmeIo_Uring
			bytes, err = json.Marshal(m)
		}
	}

	if s.Parallels != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Parallels); err == nil {
			m["driver"] = BlockdevDriverParallels
			bytes, err = json.Marshal(m)
		}
	}

	if s.Preallocate != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Preallocate); err == nil {
			m["driver"] = BlockdevDriverPreallocate
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qcow2 != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Qcow2); err == nil {
			m["driver"] = BlockdevDriverQcow2
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qcow != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Qcow); err == nil {
			m["driver"] = BlockdevDriverQcow
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qed != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Qed); err == nil {
			m["driver"] = BlockdevDriverQed
			bytes, err = json.Marshal(m)
		}
	}

	if s.Quorum != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Quorum); err == nil {
			m["driver"] = BlockdevDriverQuorum
			bytes, err = json.Marshal(m)
		}
	}

	if s.Raw != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Raw); err == nil {
			m["driver"] = BlockdevDriverRaw
			bytes, err = json.Marshal(m)
		}
	}

	if s.Rbd != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Rbd); err == nil {
			m["driver"] = BlockdevDriverRbd
			bytes, err = json.Marshal(m)
		}
	}

	if s.Replication != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Replication); err == nil {
			m["driver"] = BlockdevDriverReplication
			bytes, err = json.Marshal(m)
		}
	}

	if s.SnapshotAccess != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.SnapshotAccess); err == nil {
			m["driver"] = BlockdevDriverSnapshotAccess
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ssh != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Ssh); err == nil {
			m["driver"] = BlockdevDriverSsh
			bytes, err = json.Marshal(m)
		}
	}

	if s.Throttle != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Throttle); err == nil {
			m["driver"] = BlockdevDriverThrottle
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vdi != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vdi); err == nil {
			m["driver"] = BlockdevDriverVdi
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vhdx != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vhdx); err == nil {
			m["driver"] = BlockdevDriverVhdx
			bytes, err = json.Marshal(m)
		}
	}

	if s.VirtioBlkVfioPci != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.VirtioBlkVfioPci); err == nil {
			m["driver"] = BlockdevDriverVirtioBlkVfioPci
			bytes, err = json.Marshal(m)
		}
	}

	if s.VirtioBlkVhostUser != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.VirtioBlkVhostUser); err == nil {
			m["driver"] = BlockdevDriverVirtioBlkVhostUser
			bytes, err = json.Marshal(m)
		}
	}

	if s.VirtioBlkVhostVdpa != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.VirtioBlkVhostVdpa); err == nil {
			m["driver"] = BlockdevDriverVirtioBlkVhostVdpa
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vmdk != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vmdk); err == nil {
			m["driver"] = BlockdevDriverVmdk
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vpc != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vpc); err == nil {
			m["driver"] = BlockdevDriverVpc
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vvfat != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vvfat); err == nil {
			m["driver"] = BlockdevDriverVvfat
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal BlockdevOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal BlockdevOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *BlockdevOptions) UnmarshalJSON(data []byte) error {
	type BlockdevOptionsBase struct {
		Driver       BlockdevDriver               `json:"driver"`
		NodeName     *string                      `json:"node-name,omitempty"`
		Discard      *BlockdevDiscardOptions      `json:"discard,omitempty"`
		Cache        *BlockdevCacheOptions        `json:"cache,omitempty"`
		Active       *bool                        `json:"active,omitempty"`
		ReadOnly     *bool                        `json:"read-only,omitempty"`
		AutoReadOnly *bool                        `json:"auto-read-only,omitempty"`
		ForceShare   *bool                        `json:"force-share,omitempty"`
		DetectZeroes *BlockdevDetectZeroesOptions `json:"detect-zeroes,omitempty"`
	}

	tmp := struct {
		BlockdevOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.NodeName = tmp.NodeName
	s.Discard = tmp.Discard
	s.Cache = tmp.Cache
	s.Active = tmp.Active
	s.ReadOnly = tmp.ReadOnly
	s.AutoReadOnly = tmp.AutoReadOnly
	s.ForceShare = tmp.ForceShare
	s.DetectZeroes = tmp.DetectZeroes
	switch tmp.Driver {
	case BlockdevDriverBlkdebug:
		s.Blkdebug = new(BlockdevOptionsBlkdebug)
		if err := json.Unmarshal(data, s.Blkdebug); err != nil {
			s.Blkdebug = nil
			return err
		}
	case BlockdevDriverBlklogwrites:
		s.Blklogwrites = new(BlockdevOptionsBlklogwrites)
		if err := json.Unmarshal(data, s.Blklogwrites); err != nil {
			s.Blklogwrites = nil
			return err
		}
	case BlockdevDriverBlkverify:
		s.Blkverify = new(BlockdevOptionsBlkverify)
		if err := json.Unmarshal(data, s.Blkverify); err != nil {
			s.Blkverify = nil
			return err
		}
	case BlockdevDriverBlkreplay:
		s.Blkreplay = new(BlockdevOptionsBlkreplay)
		if err := json.Unmarshal(data, s.Blkreplay); err != nil {
			s.Blkreplay = nil
			return err
		}
	case BlockdevDriverBochs:
		s.Bochs = new(BlockdevOptionsGenericFormat)
		if err := json.Unmarshal(data, s.Bochs); err != nil {
			s.Bochs = nil
			return err
		}
	case BlockdevDriverCloop:
		s.Cloop = new(BlockdevOptionsGenericFormat)
		if err := json.Unmarshal(data, s.Cloop); err != nil {
			s.Cloop = nil
			return err
		}
	case BlockdevDriverCompress:
		s.Compress = new(BlockdevOptionsGenericFormat)
		if err := json.Unmarshal(data, s.Compress); err != nil {
			s.Compress = nil
			return err
		}
	case BlockdevDriverCopyBeforeWrite:
		s.CopyBeforeWrite = new(BlockdevOptionsCbw)
		if err := json.Unmarshal(data, s.CopyBeforeWrite); err != nil {
			s.CopyBeforeWrite = nil
			return err
		}
	case BlockdevDriverCopyOnRead:
		s.CopyOnRead = new(BlockdevOptionsCor)
		if err := json.Unmarshal(data, s.CopyOnRead); err != nil {
			s.CopyOnRead = nil
			return err
		}
	case BlockdevDriverDmg:
		s.Dmg = new(BlockdevOptionsGenericFormat)
		if err := json.Unmarshal(data, s.Dmg); err != nil {
			s.Dmg = nil
			return err
		}
	case BlockdevDriverFile:
		s.File = new(BlockdevOptionsFile)
		if err := json.Unmarshal(data, s.File); err != nil {
			s.File = nil
			return err
		}
	case BlockdevDriverFtp:
		s.Ftp = new(BlockdevOptionsCurlFtp)
		if err := json.Unmarshal(data, s.Ftp); err != nil {
			s.Ftp = nil
			return err
		}
	case BlockdevDriverFtps:
		s.Ftps = new(BlockdevOptionsCurlFtps)
		if err := json.Unmarshal(data, s.Ftps); err != nil {
			s.Ftps = nil
			return err
		}
	case BlockdevDriverGluster:
		s.Gluster = new(BlockdevOptionsGluster)
		if err := json.Unmarshal(data, s.Gluster); err != nil {
			s.Gluster = nil
			return err
		}
	case BlockdevDriverHost_Cdrom:
		s.HostCdrom = new(BlockdevOptionsFile)
		if err := json.Unmarshal(data, s.HostCdrom); err != nil {
			s.HostCdrom = nil
			return err
		}
	case BlockdevDriverHost_Device:
		s.HostDevice = new(BlockdevOptionsFile)
		if err := json.Unmarshal(data, s.HostDevice); err != nil {
			s.HostDevice = nil
			return err
		}
	case BlockdevDriverHttp:
		s.Http = new(BlockdevOptionsCurlHttp)
		if err := json.Unmarshal(data, s.Http); err != nil {
			s.Http = nil
			return err
		}
	case BlockdevDriverHttps:
		s.Https = new(BlockdevOptionsCurlHttps)
		if err := json.Unmarshal(data, s.Https); err != nil {
			s.Https = nil
			return err
		}
	case BlockdevDriverIo_Uring:
		s.IoUring = new(BlockdevOptionsIoUring)
		if err := json.Unmarshal(data, s.IoUring); err != nil {
			s.IoUring = nil
			return err
		}
	case BlockdevDriverIscsi:
		s.Iscsi = new(BlockdevOptionsIscsi)
		if err := json.Unmarshal(data, s.Iscsi); err != nil {
			s.Iscsi = nil
			return err
		}
	case BlockdevDriverLuks:
		s.Luks = new(BlockdevOptionsLUKS)
		if err := json.Unmarshal(data, s.Luks); err != nil {
			s.Luks = nil
			return err
		}
	case BlockdevDriverNbd:
		s.Nbd = new(BlockdevOptionsNbd)
		if err := json.Unmarshal(data, s.Nbd); err != nil {
			s.Nbd = nil
			return err
		}
	case BlockdevDriverNfs:
		s.Nfs = new(BlockdevOptionsNfs)
		if err := json.Unmarshal(data, s.Nfs); err != nil {
			s.Nfs = nil
			return err
		}
	case BlockdevDriverNullAio:
		s.NullAio = new(BlockdevOptionsNull)
		if err := json.Unmarshal(data, s.NullAio); err != nil {
			s.NullAio = nil
			return err
		}
	case BlockdevDriverNullCo:
		s.NullCo = new(BlockdevOptionsNull)
		if err := json.Unmarshal(data, s.NullCo); err != nil {
			s.NullCo = nil
			return err
		}
	case BlockdevDriverNvme:
		s.Nvme = new(BlockdevOptionsNVMe)
		if err := json.Unmarshal(data, s.Nvme); err != nil {
			s.Nvme = nil
			return err
		}
	case BlockdevDriverNvmeIo_Uring:
		s.NvmeIoUring = new(BlockdevOptionsNvmeIoUring)
		if err := json.Unmarshal(data, s.NvmeIoUring); err != nil {
			s.NvmeIoUring = nil
			return err
		}
	case BlockdevDriverParallels:
		s.Parallels = new(BlockdevOptionsGenericFormat)
		if err := json.Unmarshal(data, s.Parallels); err != nil {
			s.Parallels = nil
			return err
		}
	case BlockdevDriverPreallocate:
		s.Preallocate = new(BlockdevOptionsPreallocate)
		if err := json.Unmarshal(data, s.Preallocate); err != nil {
			s.Preallocate = nil
			return err
		}
	case BlockdevDriverQcow2:
		s.Qcow2 = new(BlockdevOptionsQcow2)
		if err := json.Unmarshal(data, s.Qcow2); err != nil {
			s.Qcow2 = nil
			return err
		}
	case BlockdevDriverQcow:
		s.Qcow = new(BlockdevOptionsQcow)
		if err := json.Unmarshal(data, s.Qcow); err != nil {
			s.Qcow = nil
			return err
		}
	case BlockdevDriverQed:
		s.Qed = new(BlockdevOptionsGenericCOWFormat)
		if err := json.Unmarshal(data, s.Qed); err != nil {
			s.Qed = nil
			return err
		}
	case BlockdevDriverQuorum:
		s.Quorum = new(BlockdevOptionsQuorum)
		if err := json.Unmarshal(data, s.Quorum); err != nil {
			s.Quorum = nil
			return err
		}
	case BlockdevDriverRaw:
		s.Raw = new(BlockdevOptionsRaw)
		if err := json.Unmarshal(data, s.Raw); err != nil {
			s.Raw = nil
			return err
		}
	case BlockdevDriverRbd:
		s.Rbd = new(BlockdevOptionsRbd)
		if err := json.Unmarshal(data, s.Rbd); err != nil {
			s.Rbd = nil
			return err
		}
	case BlockdevDriverReplication:
		s.Replication = new(BlockdevOptionsReplication)
		if err := json.Unmarshal(data, s.Replication); err != nil {
			s.Replication = nil
			return err
		}
	case BlockdevDriverSnapshotAccess:
		s.SnapshotAccess = new(BlockdevOptionsGenericFormat)
		if err := json.Unmarshal(data, s.SnapshotAccess); err != nil {
			s.SnapshotAccess = nil
			return err
		}
	case BlockdevDriverSsh:
		s.Ssh = new(BlockdevOptionsSsh)
		if err := json.Unmarshal(data, s.Ssh); err != nil {
			s.Ssh = nil
			return err
		}
	case BlockdevDriverThrottle:
		s.Throttle = new(BlockdevOptionsThrottle)
		if err := json.Unmarshal(data, s.Throttle); err != nil {
			s.Throttle = nil
			return err
		}
	case BlockdevDriverVdi:
		s.Vdi = new(BlockdevOptionsGenericFormat)
		if err := json.Unmarshal(data, s.Vdi); err != nil {
			s.Vdi = nil
			return err
		}
	case BlockdevDriverVhdx:
		s.Vhdx = new(BlockdevOptionsGenericFormat)
		if err := json.Unmarshal(data, s.Vhdx); err != nil {
			s.Vhdx = nil
			return err
		}
	case BlockdevDriverVirtioBlkVfioPci:
		s.VirtioBlkVfioPci = new(BlockdevOptionsVirtioBlkVfioPci)
		if err := json.Unmarshal(data, s.VirtioBlkVfioPci); err != nil {
			s.VirtioBlkVfioPci = nil
			return err
		}
	case BlockdevDriverVirtioBlkVhostUser:
		s.VirtioBlkVhostUser = new(BlockdevOptionsVirtioBlkVhostUser)
		if err := json.Unmarshal(data, s.VirtioBlkVhostUser); err != nil {
			s.VirtioBlkVhostUser = nil
			return err
		}
	case BlockdevDriverVirtioBlkVhostVdpa:
		s.VirtioBlkVhostVdpa = new(BlockdevOptionsVirtioBlkVhostVdpa)
		if err := json.Unmarshal(data, s.VirtioBlkVhostVdpa); err != nil {
			s.VirtioBlkVhostVdpa = nil
			return err
		}
	case BlockdevDriverVmdk:
		s.Vmdk = new(BlockdevOptionsGenericCOWFormat)
		if err := json.Unmarshal(data, s.Vmdk); err != nil {
			s.Vmdk = nil
			return err
		}
	case BlockdevDriverVpc:
		s.Vpc = new(BlockdevOptionsGenericFormat)
		if err := json.Unmarshal(data, s.Vpc); err != nil {
			s.Vpc = nil
			return err
		}
	case BlockdevDriverVvfat:
		s.Vvfat = new(BlockdevOptionsVVFAT)
		if err := json.Unmarshal(data, s.Vvfat); err != nil {
			s.Vvfat = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal BlockdevOptions received unrecognized value '%s'",
			tmp.Driver)
	}
	return nil
}

// Since: 2.10
type BlockdevQcow2Encryption struct {
	// Variants fields
	Aes  *QCryptoBlockOptionsQCow `json:"-"`
	Luks *QCryptoBlockOptionsLUKS `json:"-"`
}

func (s BlockdevQcow2Encryption) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias BlockdevQcow2Encryption
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Aes != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Aes); err == nil {
			m["format"] = BlockdevQcow2EncryptionFormatAes
			bytes, err = json.Marshal(m)
		}
	}

	if s.Luks != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Luks); err == nil {
			m["format"] = BlockdevQcow2EncryptionFormatLuks
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal BlockdevQcow2Encryption due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal BlockdevQcow2Encryption unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *BlockdevQcow2Encryption) UnmarshalJSON(data []byte) error {
	type BlockdevQcow2EncryptionBase struct {
		Format BlockdevQcow2EncryptionFormat `json:"format"`
	}

	tmp := struct {
		BlockdevQcow2EncryptionBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Format {
	case BlockdevQcow2EncryptionFormatAes:
		s.Aes = new(QCryptoBlockOptionsQCow)
		if err := json.Unmarshal(data, s.Aes); err != nil {
			s.Aes = nil
			return err
		}
	case BlockdevQcow2EncryptionFormatLuks:
		s.Luks = new(QCryptoBlockOptionsLUKS)
		if err := json.Unmarshal(data, s.Luks); err != nil {
			s.Luks = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal BlockdevQcow2Encryption received unrecognized value '%s'",
			tmp.Format)
	}
	return nil
}

// Since: 2.10
type BlockdevQcowEncryption struct {
	// Variants fields
	Aes *QCryptoBlockOptionsQCow `json:"-"`
}

func (s BlockdevQcowEncryption) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias BlockdevQcowEncryption
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Aes != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Aes); err == nil {
			m["format"] = BlockdevQcowEncryptionFormatAes
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal BlockdevQcowEncryption due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal BlockdevQcowEncryption unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *BlockdevQcowEncryption) UnmarshalJSON(data []byte) error {
	type BlockdevQcowEncryptionBase struct {
		Format BlockdevQcowEncryptionFormat `json:"format"`
	}

	tmp := struct {
		BlockdevQcowEncryptionBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Format {
	case BlockdevQcowEncryptionFormatAes:
		s.Aes = new(QCryptoBlockOptionsQCow)
		if err := json.Unmarshal(data, s.Aes); err != nil {
			s.Aes = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal BlockdevQcowEncryption received unrecognized value '%s'",
			tmp.Format)
	}
	return nil
}

// Configuration info for the new chardev backend.
//
// Since: 1.4
type ChardevBackend struct {
	// Variants fields
	File        *ChardevFileWrapper         `json:"-"`
	Serial      *ChardevHostdevWrapper      `json:"-"`
	Parallel    *ChardevHostdevWrapper      `json:"-"`
	Pipe        *ChardevHostdevWrapper      `json:"-"`
	Socket      *ChardevSocketWrapper       `json:"-"`
	Udp         *ChardevUdpWrapper          `json:"-"`
	Pty         *ChardevPtyWrapper          `json:"-"`
	Null        *ChardevCommonWrapper       `json:"-"`
	Mux         *ChardevMuxWrapper          `json:"-"`
	Hub         *ChardevHubWrapper          `json:"-"`
	Msmouse     *ChardevCommonWrapper       `json:"-"`
	Wctablet    *ChardevCommonWrapper       `json:"-"`
	Braille     *ChardevCommonWrapper       `json:"-"`
	Testdev     *ChardevCommonWrapper       `json:"-"`
	Stdio       *ChardevStdioWrapper        `json:"-"`
	Console     *ChardevCommonWrapper       `json:"-"`
	Spicevmc    *ChardevSpiceChannelWrapper `json:"-"`
	Spiceport   *ChardevSpicePortWrapper    `json:"-"`
	QemuVdagent *ChardevQemuVDAgentWrapper  `json:"-"`
	Dbus        *ChardevDBusWrapper         `json:"-"`
	Vc          *ChardevVCWrapper           `json:"-"`
	Ringbuf     *ChardevRingbufWrapper      `json:"-"`
	Memory      *ChardevRingbufWrapper      `json:"-"`
}

func (s ChardevBackend) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias ChardevBackend
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.File != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.File); err == nil {
			m["type"] = ChardevBackendKindFile
			bytes, err = json.Marshal(m)
		}
	}

	if s.Serial != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Serial); err == nil {
			m["type"] = ChardevBackendKindSerial
			bytes, err = json.Marshal(m)
		}
	}

	if s.Parallel != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Parallel); err == nil {
			m["type"] = ChardevBackendKindParallel
			bytes, err = json.Marshal(m)
		}
	}

	if s.Pipe != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Pipe); err == nil {
			m["type"] = ChardevBackendKindPipe
			bytes, err = json.Marshal(m)
		}
	}

	if s.Socket != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Socket); err == nil {
			m["type"] = ChardevBackendKindSocket
			bytes, err = json.Marshal(m)
		}
	}

	if s.Udp != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Udp); err == nil {
			m["type"] = ChardevBackendKindUdp
			bytes, err = json.Marshal(m)
		}
	}

	if s.Pty != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Pty); err == nil {
			m["type"] = ChardevBackendKindPty
			bytes, err = json.Marshal(m)
		}
	}

	if s.Null != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Null); err == nil {
			m["type"] = ChardevBackendKindNull
			bytes, err = json.Marshal(m)
		}
	}

	if s.Mux != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Mux); err == nil {
			m["type"] = ChardevBackendKindMux
			bytes, err = json.Marshal(m)
		}
	}

	if s.Hub != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Hub); err == nil {
			m["type"] = ChardevBackendKindHub
			bytes, err = json.Marshal(m)
		}
	}

	if s.Msmouse != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Msmouse); err == nil {
			m["type"] = ChardevBackendKindMsmouse
			bytes, err = json.Marshal(m)
		}
	}

	if s.Wctablet != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Wctablet); err == nil {
			m["type"] = ChardevBackendKindWctablet
			bytes, err = json.Marshal(m)
		}
	}

	if s.Braille != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Braille); err == nil {
			m["type"] = ChardevBackendKindBraille
			bytes, err = json.Marshal(m)
		}
	}

	if s.Testdev != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Testdev); err == nil {
			m["type"] = ChardevBackendKindTestdev
			bytes, err = json.Marshal(m)
		}
	}

	if s.Stdio != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Stdio); err == nil {
			m["type"] = ChardevBackendKindStdio
			bytes, err = json.Marshal(m)
		}
	}

	if s.Console != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Console); err == nil {
			m["type"] = ChardevBackendKindConsole
			bytes, err = json.Marshal(m)
		}
	}

	if s.Spicevmc != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Spicevmc); err == nil {
			m["type"] = ChardevBackendKindSpicevmc
			bytes, err = json.Marshal(m)
		}
	}

	if s.Spiceport != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Spiceport); err == nil {
			m["type"] = ChardevBackendKindSpiceport
			bytes, err = json.Marshal(m)
		}
	}

	if s.QemuVdagent != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.QemuVdagent); err == nil {
			m["type"] = ChardevBackendKindQemuVdagent
			bytes, err = json.Marshal(m)
		}
	}

	if s.Dbus != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Dbus); err == nil {
			m["type"] = ChardevBackendKindDbus
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vc != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vc); err == nil {
			m["type"] = ChardevBackendKindVc
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ringbuf != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Ringbuf); err == nil {
			m["type"] = ChardevBackendKindRingbuf
			bytes, err = json.Marshal(m)
		}
	}

	if s.Memory != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Memory); err == nil {
			m["type"] = ChardevBackendKindMemory
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal ChardevBackend due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal ChardevBackend unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *ChardevBackend) UnmarshalJSON(data []byte) error {
	type ChardevBackendBase struct {
		Type ChardevBackendKind `json:"type"`
	}

	tmp := struct {
		ChardevBackendBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Type {
	case ChardevBackendKindFile:
		s.File = new(ChardevFileWrapper)
		if err := json.Unmarshal(data, s.File); err != nil {
			s.File = nil
			return err
		}
	case ChardevBackendKindSerial:
		s.Serial = new(ChardevHostdevWrapper)
		if err := json.Unmarshal(data, s.Serial); err != nil {
			s.Serial = nil
			return err
		}
	case ChardevBackendKindParallel:
		s.Parallel = new(ChardevHostdevWrapper)
		if err := json.Unmarshal(data, s.Parallel); err != nil {
			s.Parallel = nil
			return err
		}
	case ChardevBackendKindPipe:
		s.Pipe = new(ChardevHostdevWrapper)
		if err := json.Unmarshal(data, s.Pipe); err != nil {
			s.Pipe = nil
			return err
		}
	case ChardevBackendKindSocket:
		s.Socket = new(ChardevSocketWrapper)
		if err := json.Unmarshal(data, s.Socket); err != nil {
			s.Socket = nil
			return err
		}
	case ChardevBackendKindUdp:
		s.Udp = new(ChardevUdpWrapper)
		if err := json.Unmarshal(data, s.Udp); err != nil {
			s.Udp = nil
			return err
		}
	case ChardevBackendKindPty:
		s.Pty = new(ChardevPtyWrapper)
		if err := json.Unmarshal(data, s.Pty); err != nil {
			s.Pty = nil
			return err
		}
	case ChardevBackendKindNull:
		s.Null = new(ChardevCommonWrapper)
		if err := json.Unmarshal(data, s.Null); err != nil {
			s.Null = nil
			return err
		}
	case ChardevBackendKindMux:
		s.Mux = new(ChardevMuxWrapper)
		if err := json.Unmarshal(data, s.Mux); err != nil {
			s.Mux = nil
			return err
		}
	case ChardevBackendKindHub:
		s.Hub = new(ChardevHubWrapper)
		if err := json.Unmarshal(data, s.Hub); err != nil {
			s.Hub = nil
			return err
		}
	case ChardevBackendKindMsmouse:
		s.Msmouse = new(ChardevCommonWrapper)
		if err := json.Unmarshal(data, s.Msmouse); err != nil {
			s.Msmouse = nil
			return err
		}
	case ChardevBackendKindWctablet:
		s.Wctablet = new(ChardevCommonWrapper)
		if err := json.Unmarshal(data, s.Wctablet); err != nil {
			s.Wctablet = nil
			return err
		}
	case ChardevBackendKindBraille:
		s.Braille = new(ChardevCommonWrapper)
		if err := json.Unmarshal(data, s.Braille); err != nil {
			s.Braille = nil
			return err
		}
	case ChardevBackendKindTestdev:
		s.Testdev = new(ChardevCommonWrapper)
		if err := json.Unmarshal(data, s.Testdev); err != nil {
			s.Testdev = nil
			return err
		}
	case ChardevBackendKindStdio:
		s.Stdio = new(ChardevStdioWrapper)
		if err := json.Unmarshal(data, s.Stdio); err != nil {
			s.Stdio = nil
			return err
		}
	case ChardevBackendKindConsole:
		s.Console = new(ChardevCommonWrapper)
		if err := json.Unmarshal(data, s.Console); err != nil {
			s.Console = nil
			return err
		}
	case ChardevBackendKindSpicevmc:
		s.Spicevmc = new(ChardevSpiceChannelWrapper)
		if err := json.Unmarshal(data, s.Spicevmc); err != nil {
			s.Spicevmc = nil
			return err
		}
	case ChardevBackendKindSpiceport:
		s.Spiceport = new(ChardevSpicePortWrapper)
		if err := json.Unmarshal(data, s.Spiceport); err != nil {
			s.Spiceport = nil
			return err
		}
	case ChardevBackendKindQemuVdagent:
		s.QemuVdagent = new(ChardevQemuVDAgentWrapper)
		if err := json.Unmarshal(data, s.QemuVdagent); err != nil {
			s.QemuVdagent = nil
			return err
		}
	case ChardevBackendKindDbus:
		s.Dbus = new(ChardevDBusWrapper)
		if err := json.Unmarshal(data, s.Dbus); err != nil {
			s.Dbus = nil
			return err
		}
	case ChardevBackendKindVc:
		s.Vc = new(ChardevVCWrapper)
		if err := json.Unmarshal(data, s.Vc); err != nil {
			s.Vc = nil
			return err
		}
	case ChardevBackendKindRingbuf:
		s.Ringbuf = new(ChardevRingbufWrapper)
		if err := json.Unmarshal(data, s.Ringbuf); err != nil {
			s.Ringbuf = nil
			return err
		}
	case ChardevBackendKindMemory:
		s.Memory = new(ChardevRingbufWrapper)
		if err := json.Unmarshal(data, s.Memory); err != nil {
			s.Memory = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal ChardevBackend received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// Information about a virtual CPU
//
// Since: 2.12
type CpuInfoFast struct {
	// index of the virtual CPU
	CpuIndex int64 `json:"cpu-index"`
	// path to the CPU object in the QOM tree
	QomPath string `json:"qom-path"`
	// ID of the underlying host thread
	ThreadId int64 `json:"thread-id"`
	// properties associated with a virtual CPU, e.g. the socket id
	Props *CpuInstanceProperties `json:"props,omitempty"`
	// Variants fields
	S390X *CpuInfoS390 `json:"-"`
	// Unbranched enum fields
	Aarch64      bool `json:"-"`
	Alpha        bool `json:"-"`
	Arm          bool `json:"-"`
	Avr          bool `json:"-"`
	Hppa         bool `json:"-"`
	I386         bool `json:"-"`
	Loongarch64  bool `json:"-"`
	M68K         bool `json:"-"`
	Microblaze   bool `json:"-"`
	Microblazeel bool `json:"-"`
	Mips         bool `json:"-"`
	Mips64       bool `json:"-"`
	Mips64El     bool `json:"-"`
	Mipsel       bool `json:"-"`
	Or1K         bool `json:"-"`
	Ppc          bool `json:"-"`
	Ppc64        bool `json:"-"`
	Riscv32      bool `json:"-"`
	Riscv64      bool `json:"-"`
	Rx           bool `json:"-"`
	Sh4          bool `json:"-"`
	Sh4Eb        bool `json:"-"`
	Sparc        bool `json:"-"`
	Sparc64      bool `json:"-"`
	Tricore      bool `json:"-"`
	X8664        bool `json:"-"`
	Xtensa       bool `json:"-"`
	Xtensaeb     bool `json:"-"`
}

func (s CpuInfoFast) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias CpuInfoFast
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.S390X != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.S390X); err == nil {
			m["target"] = SysEmuTargetS390X
			bytes, err = json.Marshal(m)
		}
	}

	if s.Aarch64 && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetAarch64
			bytes, err = json.Marshal(m)
		}
	}

	if s.Alpha && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetAlpha
			bytes, err = json.Marshal(m)
		}
	}

	if s.Arm && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetArm
			bytes, err = json.Marshal(m)
		}
	}

	if s.Avr && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetAvr
			bytes, err = json.Marshal(m)
		}
	}

	if s.Hppa && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetHppa
			bytes, err = json.Marshal(m)
		}
	}

	if s.I386 && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetI386
			bytes, err = json.Marshal(m)
		}
	}

	if s.Loongarch64 && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetLoongarch64
			bytes, err = json.Marshal(m)
		}
	}

	if s.M68K && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetM68K
			bytes, err = json.Marshal(m)
		}
	}

	if s.Microblaze && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetMicroblaze
			bytes, err = json.Marshal(m)
		}
	}

	if s.Microblazeel && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetMicroblazeel
			bytes, err = json.Marshal(m)
		}
	}

	if s.Mips && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetMips
			bytes, err = json.Marshal(m)
		}
	}

	if s.Mips64 && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetMips64
			bytes, err = json.Marshal(m)
		}
	}

	if s.Mips64El && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetMips64El
			bytes, err = json.Marshal(m)
		}
	}

	if s.Mipsel && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetMipsel
			bytes, err = json.Marshal(m)
		}
	}

	if s.Or1K && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetOr1K
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ppc && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetPpc
			bytes, err = json.Marshal(m)
		}
	}

	if s.Ppc64 && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetPpc64
			bytes, err = json.Marshal(m)
		}
	}

	if s.Riscv32 && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetRiscv32
			bytes, err = json.Marshal(m)
		}
	}

	if s.Riscv64 && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetRiscv64
			bytes, err = json.Marshal(m)
		}
	}

	if s.Rx && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetRx
			bytes, err = json.Marshal(m)
		}
	}

	if s.Sh4 && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetSh4
			bytes, err = json.Marshal(m)
		}
	}

	if s.Sh4Eb && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetSh4Eb
			bytes, err = json.Marshal(m)
		}
	}

	if s.Sparc && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetSparc
			bytes, err = json.Marshal(m)
		}
	}

	if s.Sparc64 && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetSparc64
			bytes, err = json.Marshal(m)
		}
	}

	if s.Tricore && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetTricore
			bytes, err = json.Marshal(m)
		}
	}

	if s.X8664 && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetX86_64
			bytes, err = json.Marshal(m)
		}
	}

	if s.Xtensa && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetXtensa
			bytes, err = json.Marshal(m)
		}
	}

	if s.Xtensaeb && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = SysEmuTargetXtensaeb
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal CpuInfoFast due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal CpuInfoFast unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *CpuInfoFast) UnmarshalJSON(data []byte) error {
	type CpuInfoFastBase struct {
		CpuIndex int64                  `json:"cpu-index"`
		QomPath  string                 `json:"qom-path"`
		ThreadId int64                  `json:"thread-id"`
		Props    *CpuInstanceProperties `json:"props,omitempty"`
		Target   SysEmuTarget           `json:"target"`
	}

	tmp := struct {
		CpuInfoFastBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.CpuIndex = tmp.CpuIndex
	s.QomPath = tmp.QomPath
	s.ThreadId = tmp.ThreadId
	s.Props = tmp.Props
	switch tmp.Target {
	case SysEmuTargetS390X:
		s.S390X = new(CpuInfoS390)
		if err := json.Unmarshal(data, s.S390X); err != nil {
			s.S390X = nil
			return err
		}
	case SysEmuTargetAarch64:
		s.Aarch64 = true

	case SysEmuTargetAlpha:
		s.Alpha = true

	case SysEmuTargetArm:
		s.Arm = true

	case SysEmuTargetAvr:
		s.Avr = true

	case SysEmuTargetHppa:
		s.Hppa = true

	case SysEmuTargetI386:
		s.I386 = true

	case SysEmuTargetLoongarch64:
		s.Loongarch64 = true

	case SysEmuTargetM68K:
		s.M68K = true

	case SysEmuTargetMicroblaze:
		s.Microblaze = true

	case SysEmuTargetMicroblazeel:
		s.Microblazeel = true

	case SysEmuTargetMips:
		s.Mips = true

	case SysEmuTargetMips64:
		s.Mips64 = true

	case SysEmuTargetMips64El:
		s.Mips64El = true

	case SysEmuTargetMipsel:
		s.Mipsel = true

	case SysEmuTargetOr1K:
		s.Or1K = true

	case SysEmuTargetPpc:
		s.Ppc = true

	case SysEmuTargetPpc64:
		s.Ppc64 = true

	case SysEmuTargetRiscv32:
		s.Riscv32 = true

	case SysEmuTargetRiscv64:
		s.Riscv64 = true

	case SysEmuTargetRx:
		s.Rx = true

	case SysEmuTargetSh4:
		s.Sh4 = true

	case SysEmuTargetSh4Eb:
		s.Sh4Eb = true

	case SysEmuTargetSparc:
		s.Sparc = true

	case SysEmuTargetSparc64:
		s.Sparc64 = true

	case SysEmuTargetTricore:
		s.Tricore = true

	case SysEmuTargetX86_64:
		s.X8664 = true

	case SysEmuTargetXtensa:
		s.Xtensa = true

	case SysEmuTargetXtensaeb:
		s.Xtensaeb = true

	default:
		return fmt.Errorf("unmarshal CpuInfoFast received unrecognized value '%s'",
			tmp.Target)
	}
	return nil
}

// Display (user interface) options.
//
// Since: 2.12
type DisplayOptions struct {
	// Start user interface in fullscreen mode (default: off).
	FullScreen *bool `json:"full-screen,omitempty"`
	// Allow to quit qemu with window close button (default: on).
	WindowClose *bool `json:"window-close,omitempty"`
	// Force showing the mouse cursor (default: off). (since: 5.0)
	ShowCursor *bool `json:"show-cursor,omitempty"`
	// Enable OpenGL support (default: off).
	Gl *DisplayGLMode `json:"gl,omitempty"`
	// Variants fields
	Gtk         *DisplayGTK         `json:"-"`
	Cocoa       *DisplayCocoa       `json:"-"`
	Curses      *DisplayCurses      `json:"-"`
	EglHeadless *DisplayEGLHeadless `json:"-"`
	Dbus        *DisplayDBus        `json:"-"`
	Sdl         *DisplaySDL         `json:"-"`
	// Unbranched enum fields
	Default  bool `json:"-"`
	None     bool `json:"-"`
	SpiceApp bool `json:"-"`
}

func (s DisplayOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias DisplayOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Gtk != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Gtk); err == nil {
			m["type"] = DisplayTypeGtk
			bytes, err = json.Marshal(m)
		}
	}

	if s.Cocoa != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Cocoa); err == nil {
			m["type"] = DisplayTypeCocoa
			bytes, err = json.Marshal(m)
		}
	}

	if s.Curses != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Curses); err == nil {
			m["type"] = DisplayTypeCurses
			bytes, err = json.Marshal(m)
		}
	}

	if s.EglHeadless != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.EglHeadless); err == nil {
			m["type"] = DisplayTypeEglHeadless
			bytes, err = json.Marshal(m)
		}
	}

	if s.Dbus != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Dbus); err == nil {
			m["type"] = DisplayTypeDbus
			bytes, err = json.Marshal(m)
		}
	}

	if s.Sdl != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Sdl); err == nil {
			m["type"] = DisplayTypeSdl
			bytes, err = json.Marshal(m)
		}
	}

	if s.Default && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = DisplayTypeDefault
			bytes, err = json.Marshal(m)
		}
	}

	if s.None && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = DisplayTypeNone
			bytes, err = json.Marshal(m)
		}
	}

	if s.SpiceApp && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = DisplayTypeSpiceApp
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal DisplayOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal DisplayOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *DisplayOptions) UnmarshalJSON(data []byte) error {
	type DisplayOptionsBase struct {
		Type        DisplayType    `json:"type"`
		FullScreen  *bool          `json:"full-screen,omitempty"`
		WindowClose *bool          `json:"window-close,omitempty"`
		ShowCursor  *bool          `json:"show-cursor,omitempty"`
		Gl          *DisplayGLMode `json:"gl,omitempty"`
	}

	tmp := struct {
		DisplayOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.FullScreen = tmp.FullScreen
	s.WindowClose = tmp.WindowClose
	s.ShowCursor = tmp.ShowCursor
	s.Gl = tmp.Gl
	switch tmp.Type {
	case DisplayTypeGtk:
		s.Gtk = new(DisplayGTK)
		if err := json.Unmarshal(data, s.Gtk); err != nil {
			s.Gtk = nil
			return err
		}
	case DisplayTypeCocoa:
		s.Cocoa = new(DisplayCocoa)
		if err := json.Unmarshal(data, s.Cocoa); err != nil {
			s.Cocoa = nil
			return err
		}
	case DisplayTypeCurses:
		s.Curses = new(DisplayCurses)
		if err := json.Unmarshal(data, s.Curses); err != nil {
			s.Curses = nil
			return err
		}
	case DisplayTypeEglHeadless:
		s.EglHeadless = new(DisplayEGLHeadless)
		if err := json.Unmarshal(data, s.EglHeadless); err != nil {
			s.EglHeadless = nil
			return err
		}
	case DisplayTypeDbus:
		s.Dbus = new(DisplayDBus)
		if err := json.Unmarshal(data, s.Dbus); err != nil {
			s.Dbus = nil
			return err
		}
	case DisplayTypeSdl:
		s.Sdl = new(DisplaySDL)
		if err := json.Unmarshal(data, s.Sdl); err != nil {
			s.Sdl = nil
			return err
		}
	case DisplayTypeDefault:
		s.Default = true

	case DisplayTypeNone:
		s.None = true

	case DisplayTypeSpiceApp:
		s.SpiceApp = true

	default:
		return fmt.Errorf("unmarshal DisplayOptions received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// Options of the display configuration reload.
//
// Since: 6.0
type DisplayReloadOptions struct {
	// Variants fields
	Vnc *DisplayReloadOptionsVNC `json:"-"`
}

func (s DisplayReloadOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias DisplayReloadOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Vnc != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vnc); err == nil {
			m["type"] = DisplayReloadTypeVnc
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal DisplayReloadOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal DisplayReloadOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *DisplayReloadOptions) UnmarshalJSON(data []byte) error {
	type DisplayReloadOptionsBase struct {
		Type DisplayReloadType `json:"type"`
	}

	tmp := struct {
		DisplayReloadOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Type {
	case DisplayReloadTypeVnc:
		s.Vnc = new(DisplayReloadOptionsVNC)
		if err := json.Unmarshal(data, s.Vnc); err != nil {
			s.Vnc = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal DisplayReloadOptions received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// Options of the display configuration reload.
//
// Since: 7.1
type DisplayUpdateOptions struct {
	// Variants fields
	Vnc *DisplayUpdateOptionsVNC `json:"-"`
}

func (s DisplayUpdateOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias DisplayUpdateOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Vnc != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vnc); err == nil {
			m["type"] = DisplayUpdateTypeVnc
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal DisplayUpdateOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal DisplayUpdateOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *DisplayUpdateOptions) UnmarshalJSON(data []byte) error {
	type DisplayUpdateOptionsBase struct {
		Type DisplayUpdateType `json:"type"`
	}

	tmp := struct {
		DisplayUpdateOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Type {
	case DisplayUpdateTypeVnc:
		s.Vnc = new(DisplayUpdateOptionsVNC)
		if err := json.Unmarshal(data, s.Vnc); err != nil {
			s.Vnc = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal DisplayUpdateOptions received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// General options for expire_password.
//
// .. note:: Time is relative to the server and currently there is no
// way to coordinate server time with client time. It is not
// recommended to use the absolute time version of the @time
// parameter unless you're sure you are on the same machine as the
// QEMU instance.
//
// Since: 7.0
type ExpirePasswordOptions struct {
	// when to expire the password. - 'now' to expire the password
	// immediately - 'never' to cancel password expiration - '+INT'
	// where INT is the number of seconds from now (integer) - 'INT'
	// where INT is the absolute time in seconds
	Time string `json:"time"`
	// Variants fields
	Vnc *ExpirePasswordOptionsVnc `json:"-"`
	// Unbranched enum fields
	Spice bool `json:"-"`
}

func (s ExpirePasswordOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias ExpirePasswordOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Vnc != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vnc); err == nil {
			m["protocol"] = DisplayProtocolVnc
			bytes, err = json.Marshal(m)
		}
	}

	if s.Spice && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["protocol"] = DisplayProtocolSpice
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal ExpirePasswordOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal ExpirePasswordOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *ExpirePasswordOptions) UnmarshalJSON(data []byte) error {
	type ExpirePasswordOptionsBase struct {
		Protocol DisplayProtocol `json:"protocol"`
		Time     string          `json:"time"`
	}

	tmp := struct {
		ExpirePasswordOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.Time = tmp.Time
	switch tmp.Protocol {
	case DisplayProtocolVnc:
		s.Vnc = new(ExpirePasswordOptionsVnc)
		if err := json.Unmarshal(data, s.Vnc); err != nil {
			s.Vnc = nil
			return err
		}
	case DisplayProtocolSpice:
		s.Spice = true

	default:
		return fmt.Errorf("unmarshal ExpirePasswordOptions received unrecognized value '%s'",
			tmp.Protocol)
	}
	return nil
}

// Information about a guest panic
//
// Since: 2.9
type GuestPanicInformation struct {
	// Variants fields
	HyperV *GuestPanicInformationHyperV `json:"-"`
	S390   *GuestPanicInformationS390   `json:"-"`
}

func (s GuestPanicInformation) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias GuestPanicInformation
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.HyperV != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.HyperV); err == nil {
			m["type"] = GuestPanicInformationTypeHyperV
			bytes, err = json.Marshal(m)
		}
	}

	if s.S390 != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.S390); err == nil {
			m["type"] = GuestPanicInformationTypeS390
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal GuestPanicInformation due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal GuestPanicInformation unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *GuestPanicInformation) UnmarshalJSON(data []byte) error {
	type GuestPanicInformationBase struct {
		Type GuestPanicInformationType `json:"type"`
	}

	tmp := struct {
		GuestPanicInformationBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Type {
	case GuestPanicInformationTypeHyperV:
		s.HyperV = new(GuestPanicInformationHyperV)
		if err := json.Unmarshal(data, s.HyperV); err != nil {
			s.HyperV = nil
			return err
		}
	case GuestPanicInformationTypeS390:
		s.S390 = new(GuestPanicInformationS390)
		if err := json.Unmarshal(data, s.S390); err != nil {
			s.S390 = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal GuestPanicInformation received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// A discriminated record of image format specific information
// structures.
//
// Since: 1.7
type ImageInfoSpecific struct {
	// Variants fields
	Qcow2 *ImageInfoSpecificQCow2Wrapper `json:"-"`
	Vmdk  *ImageInfoSpecificVmdkWrapper  `json:"-"`
	Luks  *ImageInfoSpecificLUKSWrapper  `json:"-"`
	Rbd   *ImageInfoSpecificRbdWrapper   `json:"-"`
	File  *ImageInfoSpecificFileWrapper  `json:"-"`
}

func (s ImageInfoSpecific) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias ImageInfoSpecific
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Qcow2 != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Qcow2); err == nil {
			m["type"] = ImageInfoSpecificKindQcow2
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vmdk != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vmdk); err == nil {
			m["type"] = ImageInfoSpecificKindVmdk
			bytes, err = json.Marshal(m)
		}
	}

	if s.Luks != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Luks); err == nil {
			m["type"] = ImageInfoSpecificKindLuks
			bytes, err = json.Marshal(m)
		}
	}

	if s.Rbd != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Rbd); err == nil {
			m["type"] = ImageInfoSpecificKindRbd
			bytes, err = json.Marshal(m)
		}
	}

	if s.File != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.File); err == nil {
			m["type"] = ImageInfoSpecificKindFile
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal ImageInfoSpecific due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal ImageInfoSpecific unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *ImageInfoSpecific) UnmarshalJSON(data []byte) error {
	type ImageInfoSpecificBase struct {
		Type ImageInfoSpecificKind `json:"type"`
	}

	tmp := struct {
		ImageInfoSpecificBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Type {
	case ImageInfoSpecificKindQcow2:
		s.Qcow2 = new(ImageInfoSpecificQCow2Wrapper)
		if err := json.Unmarshal(data, s.Qcow2); err != nil {
			s.Qcow2 = nil
			return err
		}
	case ImageInfoSpecificKindVmdk:
		s.Vmdk = new(ImageInfoSpecificVmdkWrapper)
		if err := json.Unmarshal(data, s.Vmdk); err != nil {
			s.Vmdk = nil
			return err
		}
	case ImageInfoSpecificKindLuks:
		s.Luks = new(ImageInfoSpecificLUKSWrapper)
		if err := json.Unmarshal(data, s.Luks); err != nil {
			s.Luks = nil
			return err
		}
	case ImageInfoSpecificKindRbd:
		s.Rbd = new(ImageInfoSpecificRbdWrapper)
		if err := json.Unmarshal(data, s.Rbd); err != nil {
			s.Rbd = nil
			return err
		}
	case ImageInfoSpecificKindFile:
		s.File = new(ImageInfoSpecificFileWrapper)
		if err := json.Unmarshal(data, s.File); err != nil {
			s.File = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal ImageInfoSpecific received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// Since: 2.10
type ImageInfoSpecificQCow2Encryption struct {
	// Variants fields
	Luks *QCryptoBlockInfoLUKS `json:"-"`
	// Unbranched enum fields
	Aes bool `json:"-"`
}

func (s ImageInfoSpecificQCow2Encryption) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias ImageInfoSpecificQCow2Encryption
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Luks != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Luks); err == nil {
			m["format"] = BlockdevQcow2EncryptionFormatLuks
			bytes, err = json.Marshal(m)
		}
	}

	if s.Aes && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["format"] = BlockdevQcow2EncryptionFormatAes
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal ImageInfoSpecificQCow2Encryption due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal ImageInfoSpecificQCow2Encryption unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *ImageInfoSpecificQCow2Encryption) UnmarshalJSON(data []byte) error {
	type ImageInfoSpecificQCow2EncryptionBase struct {
		Format BlockdevQcow2EncryptionFormat `json:"format"`
	}

	tmp := struct {
		ImageInfoSpecificQCow2EncryptionBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Format {
	case BlockdevQcow2EncryptionFormatLuks:
		s.Luks = new(QCryptoBlockInfoLUKS)
		if err := json.Unmarshal(data, s.Luks); err != nil {
			s.Luks = nil
			return err
		}
	case BlockdevQcow2EncryptionFormatAes:
		s.Aes = true

	default:
		return fmt.Errorf("unmarshal ImageInfoSpecificQCow2Encryption received unrecognized value '%s'",
			tmp.Format)
	}
	return nil
}

// Input event union.
//
// Since: 2.0
type InputEvent struct {
	// Variants fields
	Key *InputKeyEventWrapper        `json:"-"`
	Btn *InputBtnEventWrapper        `json:"-"`
	Rel *InputMoveEventWrapper       `json:"-"`
	Abs *InputMoveEventWrapper       `json:"-"`
	Mtt *InputMultiTouchEventWrapper `json:"-"`
}

func (s InputEvent) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias InputEvent
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Key != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Key); err == nil {
			m["type"] = InputEventKindKey
			bytes, err = json.Marshal(m)
		}
	}

	if s.Btn != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Btn); err == nil {
			m["type"] = InputEventKindBtn
			bytes, err = json.Marshal(m)
		}
	}

	if s.Rel != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Rel); err == nil {
			m["type"] = InputEventKindRel
			bytes, err = json.Marshal(m)
		}
	}

	if s.Abs != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Abs); err == nil {
			m["type"] = InputEventKindAbs
			bytes, err = json.Marshal(m)
		}
	}

	if s.Mtt != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Mtt); err == nil {
			m["type"] = InputEventKindMtt
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal InputEvent due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal InputEvent unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *InputEvent) UnmarshalJSON(data []byte) error {
	type InputEventBase struct {
		Type InputEventKind `json:"type"`
	}

	tmp := struct {
		InputEventBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Type {
	case InputEventKindKey:
		s.Key = new(InputKeyEventWrapper)
		if err := json.Unmarshal(data, s.Key); err != nil {
			s.Key = nil
			return err
		}
	case InputEventKindBtn:
		s.Btn = new(InputBtnEventWrapper)
		if err := json.Unmarshal(data, s.Btn); err != nil {
			s.Btn = nil
			return err
		}
	case InputEventKindRel:
		s.Rel = new(InputMoveEventWrapper)
		if err := json.Unmarshal(data, s.Rel); err != nil {
			s.Rel = nil
			return err
		}
	case InputEventKindAbs:
		s.Abs = new(InputMoveEventWrapper)
		if err := json.Unmarshal(data, s.Abs); err != nil {
			s.Abs = nil
			return err
		}
	case InputEventKindMtt:
		s.Mtt = new(InputMultiTouchEventWrapper)
		if err := json.Unmarshal(data, s.Mtt); err != nil {
			s.Mtt = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal InputEvent received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// Represents a keyboard key.
//
// Since: 1.3
type KeyValue struct {
	// Variants fields
	Number *IntWrapper      `json:"-"`
	Qcode  *QKeyCodeWrapper `json:"-"`
}

func (s KeyValue) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias KeyValue
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Number != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Number); err == nil {
			m["type"] = KeyValueKindNumber
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qcode != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Qcode); err == nil {
			m["type"] = KeyValueKindQcode
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal KeyValue due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal KeyValue unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *KeyValue) UnmarshalJSON(data []byte) error {
	type KeyValueBase struct {
		Type KeyValueKind `json:"type"`
	}

	tmp := struct {
		KeyValueBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Type {
	case KeyValueKindNumber:
		s.Number = new(IntWrapper)
		if err := json.Unmarshal(data, s.Number); err != nil {
			s.Number = nil
			return err
		}
	case KeyValueKindQcode:
		s.Qcode = new(QKeyCodeWrapper)
		if err := json.Unmarshal(data, s.Qcode); err != nil {
			s.Qcode = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal KeyValue received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// Union containing information about a memory device
//
// Since: 2.1
type MemoryDeviceInfo struct {
	// Variants fields
	Dimm       *PCDIMMDeviceInfoWrapper     `json:"-"`
	Nvdimm     *PCDIMMDeviceInfoWrapper     `json:"-"`
	VirtioPmem *VirtioPMEMDeviceInfoWrapper `json:"-"`
	VirtioMem  *VirtioMEMDeviceInfoWrapper  `json:"-"`
	SgxEpc     *SgxEPCDeviceInfoWrapper     `json:"-"`
	HvBalloon  *HvBalloonDeviceInfoWrapper  `json:"-"`
}

func (s MemoryDeviceInfo) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias MemoryDeviceInfo
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Dimm != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Dimm); err == nil {
			m["type"] = MemoryDeviceInfoKindDimm
			bytes, err = json.Marshal(m)
		}
	}

	if s.Nvdimm != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Nvdimm); err == nil {
			m["type"] = MemoryDeviceInfoKindNvdimm
			bytes, err = json.Marshal(m)
		}
	}

	if s.VirtioPmem != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.VirtioPmem); err == nil {
			m["type"] = MemoryDeviceInfoKindVirtioPmem
			bytes, err = json.Marshal(m)
		}
	}

	if s.VirtioMem != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.VirtioMem); err == nil {
			m["type"] = MemoryDeviceInfoKindVirtioMem
			bytes, err = json.Marshal(m)
		}
	}

	if s.SgxEpc != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.SgxEpc); err == nil {
			m["type"] = MemoryDeviceInfoKindSgxEpc
			bytes, err = json.Marshal(m)
		}
	}

	if s.HvBalloon != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.HvBalloon); err == nil {
			m["type"] = MemoryDeviceInfoKindHvBalloon
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal MemoryDeviceInfo due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal MemoryDeviceInfo unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *MemoryDeviceInfo) UnmarshalJSON(data []byte) error {
	type MemoryDeviceInfoBase struct {
		Type MemoryDeviceInfoKind `json:"type"`
	}

	tmp := struct {
		MemoryDeviceInfoBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Type {
	case MemoryDeviceInfoKindDimm:
		s.Dimm = new(PCDIMMDeviceInfoWrapper)
		if err := json.Unmarshal(data, s.Dimm); err != nil {
			s.Dimm = nil
			return err
		}
	case MemoryDeviceInfoKindNvdimm:
		s.Nvdimm = new(PCDIMMDeviceInfoWrapper)
		if err := json.Unmarshal(data, s.Nvdimm); err != nil {
			s.Nvdimm = nil
			return err
		}
	case MemoryDeviceInfoKindVirtioPmem:
		s.VirtioPmem = new(VirtioPMEMDeviceInfoWrapper)
		if err := json.Unmarshal(data, s.VirtioPmem); err != nil {
			s.VirtioPmem = nil
			return err
		}
	case MemoryDeviceInfoKindVirtioMem:
		s.VirtioMem = new(VirtioMEMDeviceInfoWrapper)
		if err := json.Unmarshal(data, s.VirtioMem); err != nil {
			s.VirtioMem = nil
			return err
		}
	case MemoryDeviceInfoKindSgxEpc:
		s.SgxEpc = new(SgxEPCDeviceInfoWrapper)
		if err := json.Unmarshal(data, s.SgxEpc); err != nil {
			s.SgxEpc = nil
			return err
		}
	case MemoryDeviceInfoKindHvBalloon:
		s.HvBalloon = new(HvBalloonDeviceInfoWrapper)
		if err := json.Unmarshal(data, s.HvBalloon); err != nil {
			s.HvBalloon = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal MemoryDeviceInfo received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// Migration endpoint configuration.
//
// Since: 8.2
type MigrationAddress struct {
	// Variants fields
	Socket *SocketAddress        `json:"-"`
	Exec   *MigrationExecCommand `json:"-"`
	Rdma   *InetSocketAddress    `json:"-"`
	File   *FileMigrationArgs    `json:"-"`
}

func (s MigrationAddress) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias MigrationAddress
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Socket != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Socket); err == nil {
			m["transport"] = MigrationAddressTypeSocket
			bytes, err = json.Marshal(m)
		}
	}

	if s.Exec != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Exec); err == nil {
			m["transport"] = MigrationAddressTypeExec
			bytes, err = json.Marshal(m)
		}
	}

	if s.Rdma != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Rdma); err == nil {
			m["transport"] = MigrationAddressTypeRdma
			bytes, err = json.Marshal(m)
		}
	}

	if s.File != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.File); err == nil {
			m["transport"] = MigrationAddressTypeFile
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal MigrationAddress due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal MigrationAddress unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *MigrationAddress) UnmarshalJSON(data []byte) error {
	type MigrationAddressBase struct {
		Transport MigrationAddressType `json:"transport"`
	}

	tmp := struct {
		MigrationAddressBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Transport {
	case MigrationAddressTypeSocket:
		s.Socket = new(SocketAddress)
		if err := json.Unmarshal(data, s.Socket); err != nil {
			s.Socket = nil
			return err
		}
	case MigrationAddressTypeExec:
		s.Exec = new(MigrationExecCommand)
		if err := json.Unmarshal(data, s.Exec); err != nil {
			s.Exec = nil
			return err
		}
	case MigrationAddressTypeRdma:
		s.Rdma = new(InetSocketAddress)
		if err := json.Unmarshal(data, s.Rdma); err != nil {
			s.Rdma = nil
			return err
		}
	case MigrationAddressTypeFile:
		s.File = new(FileMigrationArgs)
		if err := json.Unmarshal(data, s.File); err != nil {
			s.File = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal MigrationAddress received unrecognized value '%s'",
			tmp.Transport)
	}
	return nil
}

// Captures the configuration of a network device.
//
// Since: 1.2
type Netdev struct {
	// identifier for monitor commands.
	Id string `json:"id"`
	// Variants fields
	Nic          *NetLegacyNicOptions       `json:"-"`
	User         *NetdevUserOptions         `json:"-"`
	Tap          *NetdevTapOptions          `json:"-"`
	L2Tpv3       *NetdevL2TPv3Options       `json:"-"`
	Socket       *NetdevSocketOptions       `json:"-"`
	Stream       *NetdevStreamOptions       `json:"-"`
	Dgram        *NetdevDgramOptions        `json:"-"`
	Vde          *NetdevVdeOptions          `json:"-"`
	Bridge       *NetdevBridgeOptions       `json:"-"`
	Hubport      *NetdevHubPortOptions      `json:"-"`
	Netmap       *NetdevNetmapOptions       `json:"-"`
	AfXdp        *NetdevAFXDPOptions        `json:"-"`
	VhostUser    *NetdevVhostUserOptions    `json:"-"`
	VhostVdpa    *NetdevVhostVDPAOptions    `json:"-"`
	VmnetHost    *NetdevVmnetHostOptions    `json:"-"`
	VmnetShared  *NetdevVmnetSharedOptions  `json:"-"`
	VmnetBridged *NetdevVmnetBridgedOptions `json:"-"`
	// Unbranched enum fields
	None bool `json:"-"`
}

func (s Netdev) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias Netdev
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Nic != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Nic); err == nil {
			m["type"] = NetClientDriverNic
			bytes, err = json.Marshal(m)
		}
	}

	if s.User != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.User); err == nil {
			m["type"] = NetClientDriverUser
			bytes, err = json.Marshal(m)
		}
	}

	if s.Tap != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Tap); err == nil {
			m["type"] = NetClientDriverTap
			bytes, err = json.Marshal(m)
		}
	}

	if s.L2Tpv3 != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.L2Tpv3); err == nil {
			m["type"] = NetClientDriverL2Tpv3
			bytes, err = json.Marshal(m)
		}
	}

	if s.Socket != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Socket); err == nil {
			m["type"] = NetClientDriverSocket
			bytes, err = json.Marshal(m)
		}
	}

	if s.Stream != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Stream); err == nil {
			m["type"] = NetClientDriverStream
			bytes, err = json.Marshal(m)
		}
	}

	if s.Dgram != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Dgram); err == nil {
			m["type"] = NetClientDriverDgram
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vde != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vde); err == nil {
			m["type"] = NetClientDriverVde
			bytes, err = json.Marshal(m)
		}
	}

	if s.Bridge != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Bridge); err == nil {
			m["type"] = NetClientDriverBridge
			bytes, err = json.Marshal(m)
		}
	}

	if s.Hubport != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Hubport); err == nil {
			m["type"] = NetClientDriverHubport
			bytes, err = json.Marshal(m)
		}
	}

	if s.Netmap != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Netmap); err == nil {
			m["type"] = NetClientDriverNetmap
			bytes, err = json.Marshal(m)
		}
	}

	if s.AfXdp != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.AfXdp); err == nil {
			m["type"] = NetClientDriverAfXdp
			bytes, err = json.Marshal(m)
		}
	}

	if s.VhostUser != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.VhostUser); err == nil {
			m["type"] = NetClientDriverVhostUser
			bytes, err = json.Marshal(m)
		}
	}

	if s.VhostVdpa != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.VhostVdpa); err == nil {
			m["type"] = NetClientDriverVhostVdpa
			bytes, err = json.Marshal(m)
		}
	}

	if s.VmnetHost != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.VmnetHost); err == nil {
			m["type"] = NetClientDriverVmnetHost
			bytes, err = json.Marshal(m)
		}
	}

	if s.VmnetShared != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.VmnetShared); err == nil {
			m["type"] = NetClientDriverVmnetShared
			bytes, err = json.Marshal(m)
		}
	}

	if s.VmnetBridged != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.VmnetBridged); err == nil {
			m["type"] = NetClientDriverVmnetBridged
			bytes, err = json.Marshal(m)
		}
	}

	if s.None && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = NetClientDriverNone
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal Netdev due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal Netdev unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *Netdev) UnmarshalJSON(data []byte) error {
	type NetdevBase struct {
		Id   string          `json:"id"`
		Type NetClientDriver `json:"type"`
	}

	tmp := struct {
		NetdevBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.Id = tmp.Id
	switch tmp.Type {
	case NetClientDriverNic:
		s.Nic = new(NetLegacyNicOptions)
		if err := json.Unmarshal(data, s.Nic); err != nil {
			s.Nic = nil
			return err
		}
	case NetClientDriverUser:
		s.User = new(NetdevUserOptions)
		if err := json.Unmarshal(data, s.User); err != nil {
			s.User = nil
			return err
		}
	case NetClientDriverTap:
		s.Tap = new(NetdevTapOptions)
		if err := json.Unmarshal(data, s.Tap); err != nil {
			s.Tap = nil
			return err
		}
	case NetClientDriverL2Tpv3:
		s.L2Tpv3 = new(NetdevL2TPv3Options)
		if err := json.Unmarshal(data, s.L2Tpv3); err != nil {
			s.L2Tpv3 = nil
			return err
		}
	case NetClientDriverSocket:
		s.Socket = new(NetdevSocketOptions)
		if err := json.Unmarshal(data, s.Socket); err != nil {
			s.Socket = nil
			return err
		}
	case NetClientDriverStream:
		s.Stream = new(NetdevStreamOptions)
		if err := json.Unmarshal(data, s.Stream); err != nil {
			s.Stream = nil
			return err
		}
	case NetClientDriverDgram:
		s.Dgram = new(NetdevDgramOptions)
		if err := json.Unmarshal(data, s.Dgram); err != nil {
			s.Dgram = nil
			return err
		}
	case NetClientDriverVde:
		s.Vde = new(NetdevVdeOptions)
		if err := json.Unmarshal(data, s.Vde); err != nil {
			s.Vde = nil
			return err
		}
	case NetClientDriverBridge:
		s.Bridge = new(NetdevBridgeOptions)
		if err := json.Unmarshal(data, s.Bridge); err != nil {
			s.Bridge = nil
			return err
		}
	case NetClientDriverHubport:
		s.Hubport = new(NetdevHubPortOptions)
		if err := json.Unmarshal(data, s.Hubport); err != nil {
			s.Hubport = nil
			return err
		}
	case NetClientDriverNetmap:
		s.Netmap = new(NetdevNetmapOptions)
		if err := json.Unmarshal(data, s.Netmap); err != nil {
			s.Netmap = nil
			return err
		}
	case NetClientDriverAfXdp:
		s.AfXdp = new(NetdevAFXDPOptions)
		if err := json.Unmarshal(data, s.AfXdp); err != nil {
			s.AfXdp = nil
			return err
		}
	case NetClientDriverVhostUser:
		s.VhostUser = new(NetdevVhostUserOptions)
		if err := json.Unmarshal(data, s.VhostUser); err != nil {
			s.VhostUser = nil
			return err
		}
	case NetClientDriverVhostVdpa:
		s.VhostVdpa = new(NetdevVhostVDPAOptions)
		if err := json.Unmarshal(data, s.VhostVdpa); err != nil {
			s.VhostVdpa = nil
			return err
		}
	case NetClientDriverVmnetHost:
		s.VmnetHost = new(NetdevVmnetHostOptions)
		if err := json.Unmarshal(data, s.VmnetHost); err != nil {
			s.VmnetHost = nil
			return err
		}
	case NetClientDriverVmnetShared:
		s.VmnetShared = new(NetdevVmnetSharedOptions)
		if err := json.Unmarshal(data, s.VmnetShared); err != nil {
			s.VmnetShared = nil
			return err
		}
	case NetClientDriverVmnetBridged:
		s.VmnetBridged = new(NetdevVmnetBridgedOptions)
		if err := json.Unmarshal(data, s.VmnetBridged); err != nil {
			s.VmnetBridged = nil
			return err
		}
	case NetClientDriverNone:
		s.None = true

	default:
		return fmt.Errorf("unmarshal Netdev received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// A discriminated record of NUMA options.  (for OptsVisitor)
//
// Since: 2.1
type NumaOptions struct {
	// Variants fields
	Node      *NumaNodeOptions      `json:"-"`
	Dist      *NumaDistOptions      `json:"-"`
	Cpu       *NumaCpuOptions       `json:"-"`
	HmatLb    *NumaHmatLBOptions    `json:"-"`
	HmatCache *NumaHmatCacheOptions `json:"-"`
}

func (s NumaOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias NumaOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Node != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Node); err == nil {
			m["type"] = NumaOptionsTypeNode
			bytes, err = json.Marshal(m)
		}
	}

	if s.Dist != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Dist); err == nil {
			m["type"] = NumaOptionsTypeDist
			bytes, err = json.Marshal(m)
		}
	}

	if s.Cpu != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Cpu); err == nil {
			m["type"] = NumaOptionsTypeCpu
			bytes, err = json.Marshal(m)
		}
	}

	if s.HmatLb != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.HmatLb); err == nil {
			m["type"] = NumaOptionsTypeHmatLb
			bytes, err = json.Marshal(m)
		}
	}

	if s.HmatCache != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.HmatCache); err == nil {
			m["type"] = NumaOptionsTypeHmatCache
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal NumaOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal NumaOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *NumaOptions) UnmarshalJSON(data []byte) error {
	type NumaOptionsBase struct {
		Type NumaOptionsType `json:"type"`
	}

	tmp := struct {
		NumaOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Type {
	case NumaOptionsTypeNode:
		s.Node = new(NumaNodeOptions)
		if err := json.Unmarshal(data, s.Node); err != nil {
			s.Node = nil
			return err
		}
	case NumaOptionsTypeDist:
		s.Dist = new(NumaDistOptions)
		if err := json.Unmarshal(data, s.Dist); err != nil {
			s.Dist = nil
			return err
		}
	case NumaOptionsTypeCpu:
		s.Cpu = new(NumaCpuOptions)
		if err := json.Unmarshal(data, s.Cpu); err != nil {
			s.Cpu = nil
			return err
		}
	case NumaOptionsTypeHmatLb:
		s.HmatLb = new(NumaHmatLBOptions)
		if err := json.Unmarshal(data, s.HmatLb); err != nil {
			s.HmatLb = nil
			return err
		}
	case NumaOptionsTypeHmatCache:
		s.HmatCache = new(NumaHmatCacheOptions)
		if err := json.Unmarshal(data, s.HmatCache); err != nil {
			s.HmatCache = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal NumaOptions received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// Describes the options of a user creatable QOM object.
//
// Since: 6.0
type ObjectOptions struct {
	// the name of the new object
	Id string `json:"id"`
	// Variants fields
	AcpiGenericInitiator    *AcpiGenericInitiatorProperties `json:"-"`
	AcpiGenericPort         *AcpiGenericPortProperties      `json:"-"`
	AuthzList               *AuthZListProperties            `json:"-"`
	AuthzListfile           *AuthZListFileProperties        `json:"-"`
	AuthzPam                *AuthZPAMProperties             `json:"-"`
	AuthzSimple             *AuthZSimpleProperties          `json:"-"`
	CanHostSocketcan        *CanHostSocketcanProperties     `json:"-"`
	ColoCompare             *ColoCompareProperties          `json:"-"`
	CryptodevBackend        *CryptodevBackendProperties     `json:"-"`
	CryptodevBackendBuiltin *CryptodevBackendProperties     `json:"-"`
	CryptodevBackendLkcf    *CryptodevBackendProperties     `json:"-"`
	CryptodevVhostUser      *CryptodevVhostUserProperties   `json:"-"`
	DbusVmstate             *DBusVMStateProperties          `json:"-"`
	FilterBuffer            *FilterBufferProperties         `json:"-"`
	FilterDump              *FilterDumpProperties           `json:"-"`
	FilterMirror            *FilterMirrorProperties         `json:"-"`
	FilterRedirector        *FilterRedirectorProperties     `json:"-"`
	FilterReplay            *NetfilterProperties            `json:"-"`
	FilterRewriter          *FilterRewriterProperties       `json:"-"`
	InputBarrier            *InputBarrierProperties         `json:"-"`
	InputLinux              *InputLinuxProperties           `json:"-"`
	Iommufd                 *IOMMUFDProperties              `json:"-"`
	Iothread                *IothreadProperties             `json:"-"`
	MainLoop                *MainLoopProperties             `json:"-"`
	MemoryBackendEpc        *MemoryBackendEpcProperties     `json:"-"`
	MemoryBackendFile       *MemoryBackendFileProperties    `json:"-"`
	MemoryBackendMemfd      *MemoryBackendMemfdProperties   `json:"-"`
	MemoryBackendRam        *MemoryBackendProperties        `json:"-"`
	MemoryBackendShm        *MemoryBackendShmProperties     `json:"-"`
	PrManagerHelper         *PrManagerHelperProperties      `json:"-"`
	Qtest                   *QtestProperties                `json:"-"`
	RngBuiltin              *RngProperties                  `json:"-"`
	RngEgd                  *RngEgdProperties               `json:"-"`
	RngRandom               *RngRandomProperties            `json:"-"`
	Secret                  *SecretProperties               `json:"-"`
	SecretKeyring           *SecretKeyringProperties        `json:"-"`
	SevGuest                *SevGuestProperties             `json:"-"`
	SevSnpGuest             *SevSnpGuestProperties          `json:"-"`
	ThreadContext           *ThreadContextProperties        `json:"-"`
	ThrottleGroup           *ThrottleGroupProperties        `json:"-"`
	TlsCredsAnon            *TlsCredsAnonProperties         `json:"-"`
	TlsCredsPsk             *TlsCredsPskProperties          `json:"-"`
	TlsCredsX509            *TlsCredsX509Properties         `json:"-"`
	TlsCipherSuites         *TlsCredsProperties             `json:"-"`
	XRemoteObject           *RemoteObjectProperties         `json:"-"`
	XVfioUserServer         *VfioUserServerProperties       `json:"-"`
	// Unbranched enum fields
	CanBus      bool `json:"-"`
	PefGuest    bool `json:"-"`
	S390PvGuest bool `json:"-"`
}

func (s ObjectOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias ObjectOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.AcpiGenericInitiator != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.AcpiGenericInitiator); err == nil {
			m["qom-type"] = ObjectTypeAcpiGenericInitiator
			bytes, err = json.Marshal(m)
		}
	}

	if s.AcpiGenericPort != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.AcpiGenericPort); err == nil {
			m["qom-type"] = ObjectTypeAcpiGenericPort
			bytes, err = json.Marshal(m)
		}
	}

	if s.AuthzList != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.AuthzList); err == nil {
			m["qom-type"] = ObjectTypeAuthzList
			bytes, err = json.Marshal(m)
		}
	}

	if s.AuthzListfile != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.AuthzListfile); err == nil {
			m["qom-type"] = ObjectTypeAuthzListfile
			bytes, err = json.Marshal(m)
		}
	}

	if s.AuthzPam != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.AuthzPam); err == nil {
			m["qom-type"] = ObjectTypeAuthzPam
			bytes, err = json.Marshal(m)
		}
	}

	if s.AuthzSimple != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.AuthzSimple); err == nil {
			m["qom-type"] = ObjectTypeAuthzSimple
			bytes, err = json.Marshal(m)
		}
	}

	if s.CanHostSocketcan != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.CanHostSocketcan); err == nil {
			m["qom-type"] = ObjectTypeCanHostSocketcan
			bytes, err = json.Marshal(m)
		}
	}

	if s.ColoCompare != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.ColoCompare); err == nil {
			m["qom-type"] = ObjectTypeColoCompare
			bytes, err = json.Marshal(m)
		}
	}

	if s.CryptodevBackend != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.CryptodevBackend); err == nil {
			m["qom-type"] = ObjectTypeCryptodevBackend
			bytes, err = json.Marshal(m)
		}
	}

	if s.CryptodevBackendBuiltin != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.CryptodevBackendBuiltin); err == nil {
			m["qom-type"] = ObjectTypeCryptodevBackendBuiltin
			bytes, err = json.Marshal(m)
		}
	}

	if s.CryptodevBackendLkcf != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.CryptodevBackendLkcf); err == nil {
			m["qom-type"] = ObjectTypeCryptodevBackendLkcf
			bytes, err = json.Marshal(m)
		}
	}

	if s.CryptodevVhostUser != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.CryptodevVhostUser); err == nil {
			m["qom-type"] = ObjectTypeCryptodevVhostUser
			bytes, err = json.Marshal(m)
		}
	}

	if s.DbusVmstate != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.DbusVmstate); err == nil {
			m["qom-type"] = ObjectTypeDbusVmstate
			bytes, err = json.Marshal(m)
		}
	}

	if s.FilterBuffer != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.FilterBuffer); err == nil {
			m["qom-type"] = ObjectTypeFilterBuffer
			bytes, err = json.Marshal(m)
		}
	}

	if s.FilterDump != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.FilterDump); err == nil {
			m["qom-type"] = ObjectTypeFilterDump
			bytes, err = json.Marshal(m)
		}
	}

	if s.FilterMirror != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.FilterMirror); err == nil {
			m["qom-type"] = ObjectTypeFilterMirror
			bytes, err = json.Marshal(m)
		}
	}

	if s.FilterRedirector != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.FilterRedirector); err == nil {
			m["qom-type"] = ObjectTypeFilterRedirector
			bytes, err = json.Marshal(m)
		}
	}

	if s.FilterReplay != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.FilterReplay); err == nil {
			m["qom-type"] = ObjectTypeFilterReplay
			bytes, err = json.Marshal(m)
		}
	}

	if s.FilterRewriter != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.FilterRewriter); err == nil {
			m["qom-type"] = ObjectTypeFilterRewriter
			bytes, err = json.Marshal(m)
		}
	}

	if s.InputBarrier != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.InputBarrier); err == nil {
			m["qom-type"] = ObjectTypeInputBarrier
			bytes, err = json.Marshal(m)
		}
	}

	if s.InputLinux != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.InputLinux); err == nil {
			m["qom-type"] = ObjectTypeInputLinux
			bytes, err = json.Marshal(m)
		}
	}

	if s.Iommufd != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Iommufd); err == nil {
			m["qom-type"] = ObjectTypeIommufd
			bytes, err = json.Marshal(m)
		}
	}

	if s.Iothread != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Iothread); err == nil {
			m["qom-type"] = ObjectTypeIothread
			bytes, err = json.Marshal(m)
		}
	}

	if s.MainLoop != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.MainLoop); err == nil {
			m["qom-type"] = ObjectTypeMainLoop
			bytes, err = json.Marshal(m)
		}
	}

	if s.MemoryBackendEpc != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.MemoryBackendEpc); err == nil {
			m["qom-type"] = ObjectTypeMemoryBackendEpc
			bytes, err = json.Marshal(m)
		}
	}

	if s.MemoryBackendFile != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.MemoryBackendFile); err == nil {
			m["qom-type"] = ObjectTypeMemoryBackendFile
			bytes, err = json.Marshal(m)
		}
	}

	if s.MemoryBackendMemfd != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.MemoryBackendMemfd); err == nil {
			m["qom-type"] = ObjectTypeMemoryBackendMemfd
			bytes, err = json.Marshal(m)
		}
	}

	if s.MemoryBackendRam != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.MemoryBackendRam); err == nil {
			m["qom-type"] = ObjectTypeMemoryBackendRam
			bytes, err = json.Marshal(m)
		}
	}

	if s.MemoryBackendShm != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.MemoryBackendShm); err == nil {
			m["qom-type"] = ObjectTypeMemoryBackendShm
			bytes, err = json.Marshal(m)
		}
	}

	if s.PrManagerHelper != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.PrManagerHelper); err == nil {
			m["qom-type"] = ObjectTypePrManagerHelper
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qtest != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Qtest); err == nil {
			m["qom-type"] = ObjectTypeQtest
			bytes, err = json.Marshal(m)
		}
	}

	if s.RngBuiltin != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.RngBuiltin); err == nil {
			m["qom-type"] = ObjectTypeRngBuiltin
			bytes, err = json.Marshal(m)
		}
	}

	if s.RngEgd != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.RngEgd); err == nil {
			m["qom-type"] = ObjectTypeRngEgd
			bytes, err = json.Marshal(m)
		}
	}

	if s.RngRandom != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.RngRandom); err == nil {
			m["qom-type"] = ObjectTypeRngRandom
			bytes, err = json.Marshal(m)
		}
	}

	if s.Secret != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Secret); err == nil {
			m["qom-type"] = ObjectTypeSecret
			bytes, err = json.Marshal(m)
		}
	}

	if s.SecretKeyring != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.SecretKeyring); err == nil {
			m["qom-type"] = ObjectTypeSecret_Keyring
			bytes, err = json.Marshal(m)
		}
	}

	if s.SevGuest != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.SevGuest); err == nil {
			m["qom-type"] = ObjectTypeSevGuest
			bytes, err = json.Marshal(m)
		}
	}

	if s.SevSnpGuest != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.SevSnpGuest); err == nil {
			m["qom-type"] = ObjectTypeSevSnpGuest
			bytes, err = json.Marshal(m)
		}
	}

	if s.ThreadContext != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.ThreadContext); err == nil {
			m["qom-type"] = ObjectTypeThreadContext
			bytes, err = json.Marshal(m)
		}
	}

	if s.ThrottleGroup != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.ThrottleGroup); err == nil {
			m["qom-type"] = ObjectTypeThrottleGroup
			bytes, err = json.Marshal(m)
		}
	}

	if s.TlsCredsAnon != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.TlsCredsAnon); err == nil {
			m["qom-type"] = ObjectTypeTlsCredsAnon
			bytes, err = json.Marshal(m)
		}
	}

	if s.TlsCredsPsk != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.TlsCredsPsk); err == nil {
			m["qom-type"] = ObjectTypeTlsCredsPsk
			bytes, err = json.Marshal(m)
		}
	}

	if s.TlsCredsX509 != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.TlsCredsX509); err == nil {
			m["qom-type"] = ObjectTypeTlsCredsX509
			bytes, err = json.Marshal(m)
		}
	}

	if s.TlsCipherSuites != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.TlsCipherSuites); err == nil {
			m["qom-type"] = ObjectTypeTlsCipherSuites
			bytes, err = json.Marshal(m)
		}
	}

	if s.XRemoteObject != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.XRemoteObject); err == nil {
			m["qom-type"] = ObjectTypeXRemoteObject
			bytes, err = json.Marshal(m)
		}
	}

	if s.XVfioUserServer != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.XVfioUserServer); err == nil {
			m["qom-type"] = ObjectTypeXVfioUserServer
			bytes, err = json.Marshal(m)
		}
	}

	if s.CanBus && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["qom-type"] = ObjectTypeCanBus
			bytes, err = json.Marshal(m)
		}
	}

	if s.PefGuest && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["qom-type"] = ObjectTypePefGuest
			bytes, err = json.Marshal(m)
		}
	}

	if s.S390PvGuest && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["qom-type"] = ObjectTypeS390PvGuest
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal ObjectOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal ObjectOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *ObjectOptions) UnmarshalJSON(data []byte) error {
	type ObjectOptionsBase struct {
		QomType ObjectType `json:"qom-type"`
		Id      string     `json:"id"`
	}

	tmp := struct {
		ObjectOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.Id = tmp.Id
	switch tmp.QomType {
	case ObjectTypeAcpiGenericInitiator:
		s.AcpiGenericInitiator = new(AcpiGenericInitiatorProperties)
		if err := json.Unmarshal(data, s.AcpiGenericInitiator); err != nil {
			s.AcpiGenericInitiator = nil
			return err
		}
	case ObjectTypeAcpiGenericPort:
		s.AcpiGenericPort = new(AcpiGenericPortProperties)
		if err := json.Unmarshal(data, s.AcpiGenericPort); err != nil {
			s.AcpiGenericPort = nil
			return err
		}
	case ObjectTypeAuthzList:
		s.AuthzList = new(AuthZListProperties)
		if err := json.Unmarshal(data, s.AuthzList); err != nil {
			s.AuthzList = nil
			return err
		}
	case ObjectTypeAuthzListfile:
		s.AuthzListfile = new(AuthZListFileProperties)
		if err := json.Unmarshal(data, s.AuthzListfile); err != nil {
			s.AuthzListfile = nil
			return err
		}
	case ObjectTypeAuthzPam:
		s.AuthzPam = new(AuthZPAMProperties)
		if err := json.Unmarshal(data, s.AuthzPam); err != nil {
			s.AuthzPam = nil
			return err
		}
	case ObjectTypeAuthzSimple:
		s.AuthzSimple = new(AuthZSimpleProperties)
		if err := json.Unmarshal(data, s.AuthzSimple); err != nil {
			s.AuthzSimple = nil
			return err
		}
	case ObjectTypeCanHostSocketcan:
		s.CanHostSocketcan = new(CanHostSocketcanProperties)
		if err := json.Unmarshal(data, s.CanHostSocketcan); err != nil {
			s.CanHostSocketcan = nil
			return err
		}
	case ObjectTypeColoCompare:
		s.ColoCompare = new(ColoCompareProperties)
		if err := json.Unmarshal(data, s.ColoCompare); err != nil {
			s.ColoCompare = nil
			return err
		}
	case ObjectTypeCryptodevBackend:
		s.CryptodevBackend = new(CryptodevBackendProperties)
		if err := json.Unmarshal(data, s.CryptodevBackend); err != nil {
			s.CryptodevBackend = nil
			return err
		}
	case ObjectTypeCryptodevBackendBuiltin:
		s.CryptodevBackendBuiltin = new(CryptodevBackendProperties)
		if err := json.Unmarshal(data, s.CryptodevBackendBuiltin); err != nil {
			s.CryptodevBackendBuiltin = nil
			return err
		}
	case ObjectTypeCryptodevBackendLkcf:
		s.CryptodevBackendLkcf = new(CryptodevBackendProperties)
		if err := json.Unmarshal(data, s.CryptodevBackendLkcf); err != nil {
			s.CryptodevBackendLkcf = nil
			return err
		}
	case ObjectTypeCryptodevVhostUser:
		s.CryptodevVhostUser = new(CryptodevVhostUserProperties)
		if err := json.Unmarshal(data, s.CryptodevVhostUser); err != nil {
			s.CryptodevVhostUser = nil
			return err
		}
	case ObjectTypeDbusVmstate:
		s.DbusVmstate = new(DBusVMStateProperties)
		if err := json.Unmarshal(data, s.DbusVmstate); err != nil {
			s.DbusVmstate = nil
			return err
		}
	case ObjectTypeFilterBuffer:
		s.FilterBuffer = new(FilterBufferProperties)
		if err := json.Unmarshal(data, s.FilterBuffer); err != nil {
			s.FilterBuffer = nil
			return err
		}
	case ObjectTypeFilterDump:
		s.FilterDump = new(FilterDumpProperties)
		if err := json.Unmarshal(data, s.FilterDump); err != nil {
			s.FilterDump = nil
			return err
		}
	case ObjectTypeFilterMirror:
		s.FilterMirror = new(FilterMirrorProperties)
		if err := json.Unmarshal(data, s.FilterMirror); err != nil {
			s.FilterMirror = nil
			return err
		}
	case ObjectTypeFilterRedirector:
		s.FilterRedirector = new(FilterRedirectorProperties)
		if err := json.Unmarshal(data, s.FilterRedirector); err != nil {
			s.FilterRedirector = nil
			return err
		}
	case ObjectTypeFilterReplay:
		s.FilterReplay = new(NetfilterProperties)
		if err := json.Unmarshal(data, s.FilterReplay); err != nil {
			s.FilterReplay = nil
			return err
		}
	case ObjectTypeFilterRewriter:
		s.FilterRewriter = new(FilterRewriterProperties)
		if err := json.Unmarshal(data, s.FilterRewriter); err != nil {
			s.FilterRewriter = nil
			return err
		}
	case ObjectTypeInputBarrier:
		s.InputBarrier = new(InputBarrierProperties)
		if err := json.Unmarshal(data, s.InputBarrier); err != nil {
			s.InputBarrier = nil
			return err
		}
	case ObjectTypeInputLinux:
		s.InputLinux = new(InputLinuxProperties)
		if err := json.Unmarshal(data, s.InputLinux); err != nil {
			s.InputLinux = nil
			return err
		}
	case ObjectTypeIommufd:
		s.Iommufd = new(IOMMUFDProperties)
		if err := json.Unmarshal(data, s.Iommufd); err != nil {
			s.Iommufd = nil
			return err
		}
	case ObjectTypeIothread:
		s.Iothread = new(IothreadProperties)
		if err := json.Unmarshal(data, s.Iothread); err != nil {
			s.Iothread = nil
			return err
		}
	case ObjectTypeMainLoop:
		s.MainLoop = new(MainLoopProperties)
		if err := json.Unmarshal(data, s.MainLoop); err != nil {
			s.MainLoop = nil
			return err
		}
	case ObjectTypeMemoryBackendEpc:
		s.MemoryBackendEpc = new(MemoryBackendEpcProperties)
		if err := json.Unmarshal(data, s.MemoryBackendEpc); err != nil {
			s.MemoryBackendEpc = nil
			return err
		}
	case ObjectTypeMemoryBackendFile:
		s.MemoryBackendFile = new(MemoryBackendFileProperties)
		if err := json.Unmarshal(data, s.MemoryBackendFile); err != nil {
			s.MemoryBackendFile = nil
			return err
		}
	case ObjectTypeMemoryBackendMemfd:
		s.MemoryBackendMemfd = new(MemoryBackendMemfdProperties)
		if err := json.Unmarshal(data, s.MemoryBackendMemfd); err != nil {
			s.MemoryBackendMemfd = nil
			return err
		}
	case ObjectTypeMemoryBackendRam:
		s.MemoryBackendRam = new(MemoryBackendProperties)
		if err := json.Unmarshal(data, s.MemoryBackendRam); err != nil {
			s.MemoryBackendRam = nil
			return err
		}
	case ObjectTypeMemoryBackendShm:
		s.MemoryBackendShm = new(MemoryBackendShmProperties)
		if err := json.Unmarshal(data, s.MemoryBackendShm); err != nil {
			s.MemoryBackendShm = nil
			return err
		}
	case ObjectTypePrManagerHelper:
		s.PrManagerHelper = new(PrManagerHelperProperties)
		if err := json.Unmarshal(data, s.PrManagerHelper); err != nil {
			s.PrManagerHelper = nil
			return err
		}
	case ObjectTypeQtest:
		s.Qtest = new(QtestProperties)
		if err := json.Unmarshal(data, s.Qtest); err != nil {
			s.Qtest = nil
			return err
		}
	case ObjectTypeRngBuiltin:
		s.RngBuiltin = new(RngProperties)
		if err := json.Unmarshal(data, s.RngBuiltin); err != nil {
			s.RngBuiltin = nil
			return err
		}
	case ObjectTypeRngEgd:
		s.RngEgd = new(RngEgdProperties)
		if err := json.Unmarshal(data, s.RngEgd); err != nil {
			s.RngEgd = nil
			return err
		}
	case ObjectTypeRngRandom:
		s.RngRandom = new(RngRandomProperties)
		if err := json.Unmarshal(data, s.RngRandom); err != nil {
			s.RngRandom = nil
			return err
		}
	case ObjectTypeSecret:
		s.Secret = new(SecretProperties)
		if err := json.Unmarshal(data, s.Secret); err != nil {
			s.Secret = nil
			return err
		}
	case ObjectTypeSecret_Keyring:
		s.SecretKeyring = new(SecretKeyringProperties)
		if err := json.Unmarshal(data, s.SecretKeyring); err != nil {
			s.SecretKeyring = nil
			return err
		}
	case ObjectTypeSevGuest:
		s.SevGuest = new(SevGuestProperties)
		if err := json.Unmarshal(data, s.SevGuest); err != nil {
			s.SevGuest = nil
			return err
		}
	case ObjectTypeSevSnpGuest:
		s.SevSnpGuest = new(SevSnpGuestProperties)
		if err := json.Unmarshal(data, s.SevSnpGuest); err != nil {
			s.SevSnpGuest = nil
			return err
		}
	case ObjectTypeThreadContext:
		s.ThreadContext = new(ThreadContextProperties)
		if err := json.Unmarshal(data, s.ThreadContext); err != nil {
			s.ThreadContext = nil
			return err
		}
	case ObjectTypeThrottleGroup:
		s.ThrottleGroup = new(ThrottleGroupProperties)
		if err := json.Unmarshal(data, s.ThrottleGroup); err != nil {
			s.ThrottleGroup = nil
			return err
		}
	case ObjectTypeTlsCredsAnon:
		s.TlsCredsAnon = new(TlsCredsAnonProperties)
		if err := json.Unmarshal(data, s.TlsCredsAnon); err != nil {
			s.TlsCredsAnon = nil
			return err
		}
	case ObjectTypeTlsCredsPsk:
		s.TlsCredsPsk = new(TlsCredsPskProperties)
		if err := json.Unmarshal(data, s.TlsCredsPsk); err != nil {
			s.TlsCredsPsk = nil
			return err
		}
	case ObjectTypeTlsCredsX509:
		s.TlsCredsX509 = new(TlsCredsX509Properties)
		if err := json.Unmarshal(data, s.TlsCredsX509); err != nil {
			s.TlsCredsX509 = nil
			return err
		}
	case ObjectTypeTlsCipherSuites:
		s.TlsCipherSuites = new(TlsCredsProperties)
		if err := json.Unmarshal(data, s.TlsCipherSuites); err != nil {
			s.TlsCipherSuites = nil
			return err
		}
	case ObjectTypeXRemoteObject:
		s.XRemoteObject = new(RemoteObjectProperties)
		if err := json.Unmarshal(data, s.XRemoteObject); err != nil {
			s.XRemoteObject = nil
			return err
		}
	case ObjectTypeXVfioUserServer:
		s.XVfioUserServer = new(VfioUserServerProperties)
		if err := json.Unmarshal(data, s.XVfioUserServer); err != nil {
			s.XVfioUserServer = nil
			return err
		}
	case ObjectTypeCanBus:
		s.CanBus = true

	case ObjectTypePefGuest:
		s.PefGuest = true

	case ObjectTypeS390PvGuest:
		s.S390PvGuest = true

	default:
		return fmt.Errorf("unmarshal ObjectOptions received unrecognized value '%s'",
			tmp.QomType)
	}
	return nil
}

// The options that are available for all asymmetric key algorithms
// when creating a new QCryptoAkCipher.
//
// Since: 7.1
type QCryptoAkCipherOptions struct {
	// Variants fields
	Rsa *QCryptoAkCipherOptionsRSA `json:"-"`
}

func (s QCryptoAkCipherOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias QCryptoAkCipherOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Rsa != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Rsa); err == nil {
			m["alg"] = QCryptoAkCipherAlgoRsa
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal QCryptoAkCipherOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal QCryptoAkCipherOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *QCryptoAkCipherOptions) UnmarshalJSON(data []byte) error {
	type QCryptoAkCipherOptionsBase struct {
		Alg QCryptoAkCipherAlgo `json:"alg"`
	}

	tmp := struct {
		QCryptoAkCipherOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Alg {
	case QCryptoAkCipherAlgoRsa:
		s.Rsa = new(QCryptoAkCipherOptionsRSA)
		if err := json.Unmarshal(data, s.Rsa); err != nil {
			s.Rsa = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal QCryptoAkCipherOptions received unrecognized value '%s'",
			tmp.Alg)
	}
	return nil
}

// The options that are available for all encryption formats when
// amending encryption settings
//
// Since: 5.1
type QCryptoBlockAmendOptions struct {
	// Variants fields
	Luks *QCryptoBlockAmendOptionsLUKS `json:"-"`
	// Unbranched enum fields
	Qcow bool `json:"-"`
}

func (s QCryptoBlockAmendOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias QCryptoBlockAmendOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Luks != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Luks); err == nil {
			m["format"] = QCryptoBlockFormatLuks
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qcow && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["format"] = QCryptoBlockFormatQcow
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal QCryptoBlockAmendOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal QCryptoBlockAmendOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *QCryptoBlockAmendOptions) UnmarshalJSON(data []byte) error {
	type QCryptoBlockOptionsBase struct {
		Format QCryptoBlockFormat `json:"format"`
	}

	tmp := struct {
		QCryptoBlockOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Format {
	case QCryptoBlockFormatLuks:
		s.Luks = new(QCryptoBlockAmendOptionsLUKS)
		if err := json.Unmarshal(data, s.Luks); err != nil {
			s.Luks = nil
			return err
		}
	case QCryptoBlockFormatQcow:
		s.Qcow = true

	default:
		return fmt.Errorf("unmarshal QCryptoBlockAmendOptions received unrecognized value '%s'",
			tmp.Format)
	}
	return nil
}

// The options that are available for all encryption formats when
// initializing a new volume
//
// Since: 2.6
type QCryptoBlockCreateOptions struct {
	// Variants fields
	Qcow *QCryptoBlockOptionsQCow       `json:"-"`
	Luks *QCryptoBlockCreateOptionsLUKS `json:"-"`
}

func (s QCryptoBlockCreateOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias QCryptoBlockCreateOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Qcow != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Qcow); err == nil {
			m["format"] = QCryptoBlockFormatQcow
			bytes, err = json.Marshal(m)
		}
	}

	if s.Luks != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Luks); err == nil {
			m["format"] = QCryptoBlockFormatLuks
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal QCryptoBlockCreateOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal QCryptoBlockCreateOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *QCryptoBlockCreateOptions) UnmarshalJSON(data []byte) error {
	type QCryptoBlockOptionsBase struct {
		Format QCryptoBlockFormat `json:"format"`
	}

	tmp := struct {
		QCryptoBlockOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Format {
	case QCryptoBlockFormatQcow:
		s.Qcow = new(QCryptoBlockOptionsQCow)
		if err := json.Unmarshal(data, s.Qcow); err != nil {
			s.Qcow = nil
			return err
		}
	case QCryptoBlockFormatLuks:
		s.Luks = new(QCryptoBlockCreateOptionsLUKS)
		if err := json.Unmarshal(data, s.Luks); err != nil {
			s.Luks = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal QCryptoBlockCreateOptions received unrecognized value '%s'",
			tmp.Format)
	}
	return nil
}

// Information about the block encryption options
//
// Since: 2.7
type QCryptoBlockInfo struct {
	// Variants fields
	Luks *QCryptoBlockInfoLUKS `json:"-"`
	// Unbranched enum fields
	Qcow bool `json:"-"`
}

func (s QCryptoBlockInfo) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias QCryptoBlockInfo
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Luks != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Luks); err == nil {
			m["format"] = QCryptoBlockFormatLuks
			bytes, err = json.Marshal(m)
		}
	}

	if s.Qcow && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["format"] = QCryptoBlockFormatQcow
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal QCryptoBlockInfo due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal QCryptoBlockInfo unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *QCryptoBlockInfo) UnmarshalJSON(data []byte) error {
	type QCryptoBlockInfoBase struct {
		Format QCryptoBlockFormat `json:"format"`
	}

	tmp := struct {
		QCryptoBlockInfoBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Format {
	case QCryptoBlockFormatLuks:
		s.Luks = new(QCryptoBlockInfoLUKS)
		if err := json.Unmarshal(data, s.Luks); err != nil {
			s.Luks = nil
			return err
		}
	case QCryptoBlockFormatQcow:
		s.Qcow = true

	default:
		return fmt.Errorf("unmarshal QCryptoBlockInfo received unrecognized value '%s'",
			tmp.Format)
	}
	return nil
}

// The options that are available for all encryption formats when
// opening an existing volume
//
// Since: 2.6
type QCryptoBlockOpenOptions struct {
	// Variants fields
	Qcow *QCryptoBlockOptionsQCow `json:"-"`
	Luks *QCryptoBlockOptionsLUKS `json:"-"`
}

func (s QCryptoBlockOpenOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias QCryptoBlockOpenOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Qcow != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Qcow); err == nil {
			m["format"] = QCryptoBlockFormatQcow
			bytes, err = json.Marshal(m)
		}
	}

	if s.Luks != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Luks); err == nil {
			m["format"] = QCryptoBlockFormatLuks
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal QCryptoBlockOpenOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal QCryptoBlockOpenOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *QCryptoBlockOpenOptions) UnmarshalJSON(data []byte) error {
	type QCryptoBlockOptionsBase struct {
		Format QCryptoBlockFormat `json:"format"`
	}

	tmp := struct {
		QCryptoBlockOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Format {
	case QCryptoBlockFormatQcow:
		s.Qcow = new(QCryptoBlockOptionsQCow)
		if err := json.Unmarshal(data, s.Qcow); err != nil {
			s.Qcow = nil
			return err
		}
	case QCryptoBlockFormatLuks:
		s.Luks = new(QCryptoBlockOptionsLUKS)
		if err := json.Unmarshal(data, s.Luks); err != nil {
			s.Luks = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal QCryptoBlockOpenOptions received unrecognized value '%s'",
			tmp.Format)
	}
	return nil
}

// Since: 6.1
type RbdEncryptionCreateOptions struct {
	// Variants fields
	Luks  *RbdEncryptionCreateOptionsLUKS  `json:"-"`
	Luks2 *RbdEncryptionCreateOptionsLUKS2 `json:"-"`
	// Unbranched enum fields
	LuksAny bool `json:"-"`
}

func (s RbdEncryptionCreateOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias RbdEncryptionCreateOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Luks != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Luks); err == nil {
			m["format"] = RbdImageEncryptionFormatLuks
			bytes, err = json.Marshal(m)
		}
	}

	if s.Luks2 != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Luks2); err == nil {
			m["format"] = RbdImageEncryptionFormatLuks2
			bytes, err = json.Marshal(m)
		}
	}

	if s.LuksAny && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["format"] = RbdImageEncryptionFormatLuksAny
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal RbdEncryptionCreateOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal RbdEncryptionCreateOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *RbdEncryptionCreateOptions) UnmarshalJSON(data []byte) error {
	type RbdEncryptionCreateOptionsBase struct {
		Format RbdImageEncryptionFormat `json:"format"`
	}

	tmp := struct {
		RbdEncryptionCreateOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Format {
	case RbdImageEncryptionFormatLuks:
		s.Luks = new(RbdEncryptionCreateOptionsLUKS)
		if err := json.Unmarshal(data, s.Luks); err != nil {
			s.Luks = nil
			return err
		}
	case RbdImageEncryptionFormatLuks2:
		s.Luks2 = new(RbdEncryptionCreateOptionsLUKS2)
		if err := json.Unmarshal(data, s.Luks2); err != nil {
			s.Luks2 = nil
			return err
		}
	case RbdImageEncryptionFormatLuksAny:
		s.LuksAny = true

	default:
		return fmt.Errorf("unmarshal RbdEncryptionCreateOptions received unrecognized value '%s'",
			tmp.Format)
	}
	return nil
}

// Since: 6.1
type RbdEncryptionOptions struct {
	// Parent image encryption options (for cloned images). Can be
	// left unspecified if this cloned image is encrypted using the
	// same format and secret as its parent image (i.e. not explicitly
	// formatted) or if its parent image is not encrypted. (Since 8.0)
	Parent *RbdEncryptionOptions `json:"parent,omitempty"`
	// Variants fields
	Luks    *RbdEncryptionOptionsLUKS    `json:"-"`
	Luks2   *RbdEncryptionOptionsLUKS2   `json:"-"`
	LuksAny *RbdEncryptionOptionsLUKSAny `json:"-"`
}

func (s RbdEncryptionOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias RbdEncryptionOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Luks != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Luks); err == nil {
			m["format"] = RbdImageEncryptionFormatLuks
			bytes, err = json.Marshal(m)
		}
	}

	if s.Luks2 != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Luks2); err == nil {
			m["format"] = RbdImageEncryptionFormatLuks2
			bytes, err = json.Marshal(m)
		}
	}

	if s.LuksAny != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.LuksAny); err == nil {
			m["format"] = RbdImageEncryptionFormatLuksAny
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal RbdEncryptionOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal RbdEncryptionOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *RbdEncryptionOptions) UnmarshalJSON(data []byte) error {
	type RbdEncryptionOptionsBase struct {
		Format RbdImageEncryptionFormat `json:"format"`
		Parent *RbdEncryptionOptions    `json:"parent,omitempty"`
	}

	tmp := struct {
		RbdEncryptionOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.Parent = tmp.Parent
	switch tmp.Format {
	case RbdImageEncryptionFormatLuks:
		s.Luks = new(RbdEncryptionOptionsLUKS)
		if err := json.Unmarshal(data, s.Luks); err != nil {
			s.Luks = nil
			return err
		}
	case RbdImageEncryptionFormatLuks2:
		s.Luks2 = new(RbdEncryptionOptionsLUKS2)
		if err := json.Unmarshal(data, s.Luks2); err != nil {
			s.Luks2 = nil
			return err
		}
	case RbdImageEncryptionFormatLuksAny:
		s.LuksAny = new(RbdEncryptionOptionsLUKSAny)
		if err := json.Unmarshal(data, s.LuksAny); err != nil {
			s.LuksAny = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal RbdEncryptionOptions received unrecognized value '%s'",
			tmp.Format)
	}
	return nil
}

// Since: 2.5
type SchemaInfo struct {
	// the entity's name, inherited from @base. The SchemaInfo is
	// always referenced by this name. Commands and events have the
	// name defined in the QAPI schema. Unlike command and event
	// names, type names are not part of the wire ABI. Consequently,
	// type names are meaningless strings here, although they are
	// still guaranteed unique regardless of @meta-type.
	Name string `json:"name"`
	// names of features associated with the entity, in no particular
	// order. (since 4.1 for object types, 4.2 for commands, 5.0 for
	// the rest)
	Features []string `json:"features,omitempty"`
	// Variants fields
	Builtin   *SchemaInfoBuiltin   `json:"-"`
	Enum      *SchemaInfoEnum      `json:"-"`
	Array     *SchemaInfoArray     `json:"-"`
	Object    *SchemaInfoObject    `json:"-"`
	Alternate *SchemaInfoAlternate `json:"-"`
	Command   *SchemaInfoCommand   `json:"-"`
	Event     *SchemaInfoEvent     `json:"-"`
}

func (s SchemaInfo) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias SchemaInfo
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Builtin != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Builtin); err == nil {
			m["meta-type"] = SchemaMetaTypeBuiltin
			bytes, err = json.Marshal(m)
		}
	}

	if s.Enum != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Enum); err == nil {
			m["meta-type"] = SchemaMetaTypeEnum
			bytes, err = json.Marshal(m)
		}
	}

	if s.Array != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Array); err == nil {
			m["meta-type"] = SchemaMetaTypeArray
			bytes, err = json.Marshal(m)
		}
	}

	if s.Object != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Object); err == nil {
			m["meta-type"] = SchemaMetaTypeObject
			bytes, err = json.Marshal(m)
		}
	}

	if s.Alternate != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Alternate); err == nil {
			m["meta-type"] = SchemaMetaTypeAlternate
			bytes, err = json.Marshal(m)
		}
	}

	if s.Command != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Command); err == nil {
			m["meta-type"] = SchemaMetaTypeCommand
			bytes, err = json.Marshal(m)
		}
	}

	if s.Event != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Event); err == nil {
			m["meta-type"] = SchemaMetaTypeEvent
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal SchemaInfo due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal SchemaInfo unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *SchemaInfo) UnmarshalJSON(data []byte) error {
	type SchemaInfoBase struct {
		Name     string         `json:"name"`
		MetaType SchemaMetaType `json:"meta-type"`
		Features []string       `json:"features,omitempty"`
	}

	tmp := struct {
		SchemaInfoBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.Name = tmp.Name
	s.Features = tmp.Features
	switch tmp.MetaType {
	case SchemaMetaTypeBuiltin:
		s.Builtin = new(SchemaInfoBuiltin)
		if err := json.Unmarshal(data, s.Builtin); err != nil {
			s.Builtin = nil
			return err
		}
	case SchemaMetaTypeEnum:
		s.Enum = new(SchemaInfoEnum)
		if err := json.Unmarshal(data, s.Enum); err != nil {
			s.Enum = nil
			return err
		}
	case SchemaMetaTypeArray:
		s.Array = new(SchemaInfoArray)
		if err := json.Unmarshal(data, s.Array); err != nil {
			s.Array = nil
			return err
		}
	case SchemaMetaTypeObject:
		s.Object = new(SchemaInfoObject)
		if err := json.Unmarshal(data, s.Object); err != nil {
			s.Object = nil
			return err
		}
	case SchemaMetaTypeAlternate:
		s.Alternate = new(SchemaInfoAlternate)
		if err := json.Unmarshal(data, s.Alternate); err != nil {
			s.Alternate = nil
			return err
		}
	case SchemaMetaTypeCommand:
		s.Command = new(SchemaInfoCommand)
		if err := json.Unmarshal(data, s.Command); err != nil {
			s.Command = nil
			return err
		}
	case SchemaMetaTypeEvent:
		s.Event = new(SchemaInfoEvent)
		if err := json.Unmarshal(data, s.Event); err != nil {
			s.Event = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal SchemaInfo received unrecognized value '%s'",
			tmp.MetaType)
	}
	return nil
}

// Options for set_password.
//
// Since: 7.0
type SetPasswordOptions struct {
	// the new password
	Password string `json:"password"`
	// How to handle existing clients when changing the password. If
	// nothing is specified, defaults to 'keep'. For VNC, only 'keep'
	// is currently implemented.
	Connected *SetPasswordAction `json:"connected,omitempty"`
	// Variants fields
	Vnc *SetPasswordOptionsVnc `json:"-"`
	// Unbranched enum fields
	Spice bool `json:"-"`
}

func (s SetPasswordOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias SetPasswordOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Vnc != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vnc); err == nil {
			m["protocol"] = DisplayProtocolVnc
			bytes, err = json.Marshal(m)
		}
	}

	if s.Spice && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["protocol"] = DisplayProtocolSpice
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal SetPasswordOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal SetPasswordOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *SetPasswordOptions) UnmarshalJSON(data []byte) error {
	type SetPasswordOptionsBase struct {
		Protocol  DisplayProtocol    `json:"protocol"`
		Password  string             `json:"password"`
		Connected *SetPasswordAction `json:"connected,omitempty"`
	}

	tmp := struct {
		SetPasswordOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.Password = tmp.Password
	s.Connected = tmp.Connected
	switch tmp.Protocol {
	case DisplayProtocolVnc:
		s.Vnc = new(SetPasswordOptionsVnc)
		if err := json.Unmarshal(data, s.Vnc); err != nil {
			s.Vnc = nil
			return err
		}
	case DisplayProtocolSpice:
		s.Spice = true

	default:
		return fmt.Errorf("unmarshal SetPasswordOptions received unrecognized value '%s'",
			tmp.Protocol)
	}
	return nil
}

// Information about Secure Encrypted Virtualization (SEV) support
//
// Since: 2.12
type SevInfo struct {
	// true if SEV is active
	Enabled bool `json:"enabled"`
	// SEV API major version
	ApiMajor uint8 `json:"api-major"`
	// SEV API minor version
	ApiMinor uint8 `json:"api-minor"`
	// SEV FW build id
	BuildId uint8 `json:"build-id"`
	// SEV guest state
	State SevState `json:"state"`
	// Variants fields
	Sev    *SevGuestInfo    `json:"-"`
	SevSnp *SevSnpGuestInfo `json:"-"`
}

func (s SevInfo) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias SevInfo
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Sev != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Sev); err == nil {
			m["sev-type"] = SevGuestTypeSev
			bytes, err = json.Marshal(m)
		}
	}

	if s.SevSnp != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.SevSnp); err == nil {
			m["sev-type"] = SevGuestTypeSevSnp
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal SevInfo due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal SevInfo unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *SevInfo) UnmarshalJSON(data []byte) error {
	type SevInfoBase struct {
		Enabled  bool         `json:"enabled"`
		ApiMajor uint8        `json:"api-major"`
		ApiMinor uint8        `json:"api-minor"`
		BuildId  uint8        `json:"build-id"`
		State    SevState     `json:"state"`
		SevType  SevGuestType `json:"sev-type"`
	}

	tmp := struct {
		SevInfoBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.Enabled = tmp.Enabled
	s.ApiMajor = tmp.ApiMajor
	s.ApiMinor = tmp.ApiMinor
	s.BuildId = tmp.BuildId
	s.State = tmp.State
	switch tmp.SevType {
	case SevGuestTypeSev:
		s.Sev = new(SevGuestInfo)
		if err := json.Unmarshal(data, s.Sev); err != nil {
			s.Sev = nil
			return err
		}
	case SevGuestTypeSevSnp:
		s.SevSnp = new(SevSnpGuestInfo)
		if err := json.Unmarshal(data, s.SevSnp); err != nil {
			s.SevSnp = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal SevInfo received unrecognized value '%s'",
			tmp.SevType)
	}
	return nil
}

// Captures the address of a socket, which could also be a socket file
// descriptor
//
// Since: 2.9
type SocketAddress struct {
	// Variants fields
	Inet  *InetSocketAddress  `json:"-"`
	Unix  *UnixSocketAddress  `json:"-"`
	Vsock *VsockSocketAddress `json:"-"`
	Fd    *FdSocketAddress    `json:"-"`
}

func (s SocketAddress) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias SocketAddress
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Inet != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Inet); err == nil {
			m["type"] = SocketAddressTypeInet
			bytes, err = json.Marshal(m)
		}
	}

	if s.Unix != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Unix); err == nil {
			m["type"] = SocketAddressTypeUnix
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vsock != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vsock); err == nil {
			m["type"] = SocketAddressTypeVsock
			bytes, err = json.Marshal(m)
		}
	}

	if s.Fd != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Fd); err == nil {
			m["type"] = SocketAddressTypeFd
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal SocketAddress due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal SocketAddress unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *SocketAddress) UnmarshalJSON(data []byte) error {
	type SocketAddressBase struct {
		Type SocketAddressType `json:"type"`
	}

	tmp := struct {
		SocketAddressBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Type {
	case SocketAddressTypeInet:
		s.Inet = new(InetSocketAddress)
		if err := json.Unmarshal(data, s.Inet); err != nil {
			s.Inet = nil
			return err
		}
	case SocketAddressTypeUnix:
		s.Unix = new(UnixSocketAddress)
		if err := json.Unmarshal(data, s.Unix); err != nil {
			s.Unix = nil
			return err
		}
	case SocketAddressTypeVsock:
		s.Vsock = new(VsockSocketAddress)
		if err := json.Unmarshal(data, s.Vsock); err != nil {
			s.Vsock = nil
			return err
		}
	case SocketAddressTypeFd:
		s.Fd = new(FdSocketAddress)
		if err := json.Unmarshal(data, s.Fd); err != nil {
			s.Fd = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal SocketAddress received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// Captures the address of a socket, which could also be a named file
// descriptor
//
// Since: 1.3
type SocketAddressLegacy struct {
	// Variants fields
	Inet  *InetSocketAddressWrapper  `json:"-"`
	Unix  *UnixSocketAddressWrapper  `json:"-"`
	Vsock *VsockSocketAddressWrapper `json:"-"`
	Fd    *FdSocketAddressWrapper    `json:"-"`
}

func (s SocketAddressLegacy) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias SocketAddressLegacy
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Inet != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Inet); err == nil {
			m["type"] = SocketAddressTypeInet
			bytes, err = json.Marshal(m)
		}
	}

	if s.Unix != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Unix); err == nil {
			m["type"] = SocketAddressTypeUnix
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vsock != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vsock); err == nil {
			m["type"] = SocketAddressTypeVsock
			bytes, err = json.Marshal(m)
		}
	}

	if s.Fd != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Fd); err == nil {
			m["type"] = SocketAddressTypeFd
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal SocketAddressLegacy due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal SocketAddressLegacy unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *SocketAddressLegacy) UnmarshalJSON(data []byte) error {
	type SocketAddressLegacyBase struct {
		Type SocketAddressType `json:"type"`
	}

	tmp := struct {
		SocketAddressLegacyBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Type {
	case SocketAddressTypeInet:
		s.Inet = new(InetSocketAddressWrapper)
		if err := json.Unmarshal(data, s.Inet); err != nil {
			s.Inet = nil
			return err
		}
	case SocketAddressTypeUnix:
		s.Unix = new(UnixSocketAddressWrapper)
		if err := json.Unmarshal(data, s.Unix); err != nil {
			s.Unix = nil
			return err
		}
	case SocketAddressTypeVsock:
		s.Vsock = new(VsockSocketAddressWrapper)
		if err := json.Unmarshal(data, s.Vsock); err != nil {
			s.Vsock = nil
			return err
		}
	case SocketAddressTypeFd:
		s.Fd = new(FdSocketAddressWrapper)
		if err := json.Unmarshal(data, s.Fd); err != nil {
			s.Fd = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal SocketAddressLegacy received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// Since: 2.12
type SshHostKeyCheck struct {
	// Variants fields
	Hash *SshHostKeyHash `json:"-"`
	// Unbranched enum fields
	None       bool `json:"-"`
	KnownHosts bool `json:"-"`
}

func (s SshHostKeyCheck) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias SshHostKeyCheck
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Hash != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Hash); err == nil {
			m["mode"] = SshHostKeyCheckModeHash
			bytes, err = json.Marshal(m)
		}
	}

	if s.None && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["mode"] = SshHostKeyCheckModeNone
			bytes, err = json.Marshal(m)
		}
	}

	if s.KnownHosts && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["mode"] = SshHostKeyCheckModeKnown_Hosts
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal SshHostKeyCheck due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal SshHostKeyCheck unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *SshHostKeyCheck) UnmarshalJSON(data []byte) error {
	type SshHostKeyCheckBase struct {
		Mode SshHostKeyCheckMode `json:"mode"`
	}

	tmp := struct {
		SshHostKeyCheckBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Mode {
	case SshHostKeyCheckModeHash:
		s.Hash = new(SshHostKeyHash)
		if err := json.Unmarshal(data, s.Hash); err != nil {
			s.Hash = nil
			return err
		}
	case SshHostKeyCheckModeNone:
		s.None = true

	case SshHostKeyCheckModeKnown_Hosts:
		s.KnownHosts = true

	default:
		return fmt.Errorf("unmarshal SshHostKeyCheck received unrecognized value '%s'",
			tmp.Mode)
	}
	return nil
}

// The arguments to the query-stats command; specifies a target for
// which to request statistics and optionally the required subset of
// information for that target.
//
// Since: 7.1
type StatsFilter struct {
	// which providers to request statistics from, and optionally
	// which named values to return within each provider
	Providers []StatsRequest `json:"providers,omitempty"`
	// Variants fields
	Vcpu *StatsVCPUFilter `json:"-"`
	// Unbranched enum fields
	Vm        bool `json:"-"`
	Cryptodev bool `json:"-"`
}

func (s StatsFilter) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias StatsFilter
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Vcpu != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Vcpu); err == nil {
			m["target"] = StatsTargetVcpu
			bytes, err = json.Marshal(m)
		}
	}

	if s.Vm && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = StatsTargetVm
			bytes, err = json.Marshal(m)
		}
	}

	if s.Cryptodev && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["target"] = StatsTargetCryptodev
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal StatsFilter due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal StatsFilter unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *StatsFilter) UnmarshalJSON(data []byte) error {
	type StatsFilterBase struct {
		Target    StatsTarget    `json:"target"`
		Providers []StatsRequest `json:"providers,omitempty"`
	}

	tmp := struct {
		StatsFilterBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.Providers = tmp.Providers
	switch tmp.Target {
	case StatsTargetVcpu:
		s.Vcpu = new(StatsVCPUFilter)
		if err := json.Unmarshal(data, s.Vcpu); err != nil {
			s.Vcpu = nil
			return err
		}
	case StatsTargetVm:
		s.Vm = true

	case StatsTargetCryptodev:
		s.Cryptodev = true

	default:
		return fmt.Errorf("unmarshal StatsFilter received unrecognized value '%s'",
			tmp.Target)
	}
	return nil
}

// A union referencing different TPM backend types' configuration
// options
//
// Since: 1.5
type TpmTypeOptions struct {
	// Variants fields
	Passthrough *TPMPassthroughOptionsWrapper `json:"-"`
	Emulator    *TPMEmulatorOptionsWrapper    `json:"-"`
}

func (s TpmTypeOptions) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias TpmTypeOptions
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Passthrough != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Passthrough); err == nil {
			m["type"] = TpmTypePassthrough
			bytes, err = json.Marshal(m)
		}
	}

	if s.Emulator != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Emulator); err == nil {
			m["type"] = TpmTypeEmulator
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal TpmTypeOptions due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal TpmTypeOptions unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *TpmTypeOptions) UnmarshalJSON(data []byte) error {
	type TpmTypeOptionsBase struct {
		Type TpmType `json:"type"`
	}

	tmp := struct {
		TpmTypeOptionsBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Type {
	case TpmTypePassthrough:
		s.Passthrough = new(TPMPassthroughOptionsWrapper)
		if err := json.Unmarshal(data, s.Passthrough); err != nil {
			s.Passthrough = nil
			return err
		}
	case TpmTypeEmulator:
		s.Emulator = new(TPMEmulatorOptionsWrapper)
		if err := json.Unmarshal(data, s.Emulator); err != nil {
			s.Emulator = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal TpmTypeOptions received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// A discriminated record of operations that can be performed with
// @transaction.
//
// Since: 1.1
type TransactionAction struct {
	// Variants fields
	Abort                        *AbortWrapper                    `json:"-"`
	BlockDirtyBitmapAdd          *BlockDirtyBitmapAddWrapper      `json:"-"`
	BlockDirtyBitmapRemove       *BlockDirtyBitmapWrapper         `json:"-"`
	BlockDirtyBitmapClear        *BlockDirtyBitmapWrapper         `json:"-"`
	BlockDirtyBitmapEnable       *BlockDirtyBitmapWrapper         `json:"-"`
	BlockDirtyBitmapDisable      *BlockDirtyBitmapWrapper         `json:"-"`
	BlockDirtyBitmapMerge        *BlockDirtyBitmapMergeWrapper    `json:"-"`
	BlockdevBackup               *BlockdevBackupWrapper           `json:"-"`
	BlockdevSnapshot             *BlockdevSnapshotWrapper         `json:"-"`
	BlockdevSnapshotInternalSync *BlockdevSnapshotInternalWrapper `json:"-"`
	BlockdevSnapshotSync         *BlockdevSnapshotSyncWrapper     `json:"-"`
	DriveBackup                  *DriveBackupWrapper              `json:"-"`
}

func (s TransactionAction) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias TransactionAction
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.Abort != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Abort); err == nil {
			m["type"] = TransactionActionKindAbort
			bytes, err = json.Marshal(m)
		}
	}

	if s.BlockDirtyBitmapAdd != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.BlockDirtyBitmapAdd); err == nil {
			m["type"] = TransactionActionKindBlockDirtyBitmapAdd
			bytes, err = json.Marshal(m)
		}
	}

	if s.BlockDirtyBitmapRemove != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.BlockDirtyBitmapRemove); err == nil {
			m["type"] = TransactionActionKindBlockDirtyBitmapRemove
			bytes, err = json.Marshal(m)
		}
	}

	if s.BlockDirtyBitmapClear != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.BlockDirtyBitmapClear); err == nil {
			m["type"] = TransactionActionKindBlockDirtyBitmapClear
			bytes, err = json.Marshal(m)
		}
	}

	if s.BlockDirtyBitmapEnable != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.BlockDirtyBitmapEnable); err == nil {
			m["type"] = TransactionActionKindBlockDirtyBitmapEnable
			bytes, err = json.Marshal(m)
		}
	}

	if s.BlockDirtyBitmapDisable != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.BlockDirtyBitmapDisable); err == nil {
			m["type"] = TransactionActionKindBlockDirtyBitmapDisable
			bytes, err = json.Marshal(m)
		}
	}

	if s.BlockDirtyBitmapMerge != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.BlockDirtyBitmapMerge); err == nil {
			m["type"] = TransactionActionKindBlockDirtyBitmapMerge
			bytes, err = json.Marshal(m)
		}
	}

	if s.BlockdevBackup != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.BlockdevBackup); err == nil {
			m["type"] = TransactionActionKindBlockdevBackup
			bytes, err = json.Marshal(m)
		}
	}

	if s.BlockdevSnapshot != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.BlockdevSnapshot); err == nil {
			m["type"] = TransactionActionKindBlockdevSnapshot
			bytes, err = json.Marshal(m)
		}
	}

	if s.BlockdevSnapshotInternalSync != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.BlockdevSnapshotInternalSync); err == nil {
			m["type"] = TransactionActionKindBlockdevSnapshotInternalSync
			bytes, err = json.Marshal(m)
		}
	}

	if s.BlockdevSnapshotSync != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.BlockdevSnapshotSync); err == nil {
			m["type"] = TransactionActionKindBlockdevSnapshotSync
			bytes, err = json.Marshal(m)
		}
	}

	if s.DriveBackup != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.DriveBackup); err == nil {
			m["type"] = TransactionActionKindDriveBackup
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal TransactionAction due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal TransactionAction unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *TransactionAction) UnmarshalJSON(data []byte) error {
	type TransactionActionBase struct {
		Type TransactionActionKind `json:"type"`
	}

	tmp := struct {
		TransactionActionBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Type {
	case TransactionActionKindAbort:
		s.Abort = new(AbortWrapper)
		if err := json.Unmarshal(data, s.Abort); err != nil {
			s.Abort = nil
			return err
		}
	case TransactionActionKindBlockDirtyBitmapAdd:
		s.BlockDirtyBitmapAdd = new(BlockDirtyBitmapAddWrapper)
		if err := json.Unmarshal(data, s.BlockDirtyBitmapAdd); err != nil {
			s.BlockDirtyBitmapAdd = nil
			return err
		}
	case TransactionActionKindBlockDirtyBitmapRemove:
		s.BlockDirtyBitmapRemove = new(BlockDirtyBitmapWrapper)
		if err := json.Unmarshal(data, s.BlockDirtyBitmapRemove); err != nil {
			s.BlockDirtyBitmapRemove = nil
			return err
		}
	case TransactionActionKindBlockDirtyBitmapClear:
		s.BlockDirtyBitmapClear = new(BlockDirtyBitmapWrapper)
		if err := json.Unmarshal(data, s.BlockDirtyBitmapClear); err != nil {
			s.BlockDirtyBitmapClear = nil
			return err
		}
	case TransactionActionKindBlockDirtyBitmapEnable:
		s.BlockDirtyBitmapEnable = new(BlockDirtyBitmapWrapper)
		if err := json.Unmarshal(data, s.BlockDirtyBitmapEnable); err != nil {
			s.BlockDirtyBitmapEnable = nil
			return err
		}
	case TransactionActionKindBlockDirtyBitmapDisable:
		s.BlockDirtyBitmapDisable = new(BlockDirtyBitmapWrapper)
		if err := json.Unmarshal(data, s.BlockDirtyBitmapDisable); err != nil {
			s.BlockDirtyBitmapDisable = nil
			return err
		}
	case TransactionActionKindBlockDirtyBitmapMerge:
		s.BlockDirtyBitmapMerge = new(BlockDirtyBitmapMergeWrapper)
		if err := json.Unmarshal(data, s.BlockDirtyBitmapMerge); err != nil {
			s.BlockDirtyBitmapMerge = nil
			return err
		}
	case TransactionActionKindBlockdevBackup:
		s.BlockdevBackup = new(BlockdevBackupWrapper)
		if err := json.Unmarshal(data, s.BlockdevBackup); err != nil {
			s.BlockdevBackup = nil
			return err
		}
	case TransactionActionKindBlockdevSnapshot:
		s.BlockdevSnapshot = new(BlockdevSnapshotWrapper)
		if err := json.Unmarshal(data, s.BlockdevSnapshot); err != nil {
			s.BlockdevSnapshot = nil
			return err
		}
	case TransactionActionKindBlockdevSnapshotInternalSync:
		s.BlockdevSnapshotInternalSync = new(BlockdevSnapshotInternalWrapper)
		if err := json.Unmarshal(data, s.BlockdevSnapshotInternalSync); err != nil {
			s.BlockdevSnapshotInternalSync = nil
			return err
		}
	case TransactionActionKindBlockdevSnapshotSync:
		s.BlockdevSnapshotSync = new(BlockdevSnapshotSyncWrapper)
		if err := json.Unmarshal(data, s.BlockdevSnapshotSync); err != nil {
			s.BlockdevSnapshotSync = nil
			return err
		}
	case TransactionActionKindDriveBackup:
		s.DriveBackup = new(DriveBackupWrapper)
		if err := json.Unmarshal(data, s.DriveBackup); err != nil {
			s.DriveBackup = nil
			return err
		}
	default:
		return fmt.Errorf("unmarshal TransactionAction received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}

// A yank instance can be yanked with the @yank qmp command to recover
// from a hanging QEMU.
//
// Currently implemented yank instances:  - nbd block device: Yanking
// it will shut down the connection to the  nbd server without
// attempting to reconnect. - socket chardev: Yanking it will shut
// down the connected socket. - migration: Yanking it will shut down
// all migration connections.  Unlike @migrate_cancel, it will not
// notify the migration process,  so migration will go into @failed
// state, instead of @cancelled  state. @yank should be used to
// recover from hangs.
//
// Since: 6.0
type YankInstance struct {
	// Variants fields
	BlockNode *YankInstanceBlockNode `json:"-"`
	Chardev   *YankInstanceChardev   `json:"-"`
	// Unbranched enum fields
	Migration bool `json:"-"`
}

func (s YankInstance) MarshalJSON() ([]byte, error) {
	var bytes []byte
	var err error
	m := make(map[string]any)
	{
		type Alias YankInstance
		v := Alias(s)
		unwrapToMap(m, &v)
	}
	if s.BlockNode != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.BlockNode); err == nil {
			m["type"] = YankInstanceTypeBlockNode
			bytes, err = json.Marshal(m)
		}
	}

	if s.Chardev != nil && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else if err = unwrapToMap(m, s.Chardev); err == nil {
			m["type"] = YankInstanceTypeChardev
			bytes, err = json.Marshal(m)
		}
	}

	if s.Migration && err == nil {
		if len(bytes) != 0 {
			err = errors.New(`multiple variant fields set`)
		} else {
			m["type"] = YankInstanceTypeMigration
			bytes, err = json.Marshal(m)
		}
	}

	if err != nil {
		return nil, fmt.Errorf("marshal YankInstance due:'%s' struct='%+v'", err, s)
	} else if len(bytes) == 0 {
		return nil, fmt.Errorf("marshal YankInstance unsupported, struct='%+v'", s)
	}
	return bytes, nil
}

func (s *YankInstance) UnmarshalJSON(data []byte) error {
	type YankInstanceBase struct {
		Type YankInstanceType `json:"type"`
	}

	tmp := struct {
		YankInstanceBase
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	switch tmp.Type {
	case YankInstanceTypeBlockNode:
		s.BlockNode = new(YankInstanceBlockNode)
		if err := json.Unmarshal(data, s.BlockNode); err != nil {
			s.BlockNode = nil
			return err
		}
	case YankInstanceTypeChardev:
		s.Chardev = new(YankInstanceChardev)
		if err := json.Unmarshal(data, s.Chardev); err != nil {
			s.Chardev = nil
			return err
		}
	case YankInstanceTypeMigration:
		s.Migration = true

	default:
		return fmt.Errorf("unmarshal YankInstance received unrecognized value '%s'",
			tmp.Type)
	}
	return nil
}
