/*
 * Copyright 2025 Red Hat, Inc.
 * SPDX-License-Identifier: (MIT-0 and GPL-2.0-or-later)
 */

/****************************************************************************
 * THIS CODE HAS BEEN GENERATED. DO NOT CHANGE IT DIRECTLY                  *
 ****************************************************************************/
package qapi

// Add a file descriptor, that was passed via SCM rights, to an fd
// set.
//
// Returns:   @AddfdInfo
//
// Errors:   - If file descriptor was not received, GenericError   -
// If @fdset-id is a negative value, GenericError
//
// .. note:: The list of fd sets is shared by all monitor connections.
// .. note:: If @fdset-id is not specified, a new fd set will be
// created.
//
// Since: 1.2
//
// .. qmp-example::    -> { "execute": "add-fd", "arguments": {
// "fdset-id": 1 } }   <- { "return": { "fdset-id": 1, "fd": 3 } }
type AddFdCommand struct {
	// The ID of the fd set to add the file descriptor to.
	FdsetId *int64 `json:"fdset-id,omitempty"`
	// A free-form string that can be used to describe the fd.
	Opaque *string `json:"opaque,omitempty"`
}

// Allow client connections for VNC, Spice and socket based character
// devices to be passed in to QEMU via SCM_RIGHTS.  If the FD
// associated with @fdname is not a socket, the command will fail and
// the FD will be closed.
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "add_client", "arguments": {
// "protocol": "vnc",                          "fdname": "myclient" }
// }   <- { "return": {} }
type AddClientCommand struct {
	// protocol name. Valid names are "vnc", "spice", "@dbus-display"
	// or the name of a character device (e.g. from -chardev id=XXXX)
	Protocol string `json:"protocol"`
	// file descriptor name previously passed via 'getfd' command
	Fdname string `json:"fdname"`
	// whether to skip authentication. Only applies to "vnc" and
	// "spice" protocols
	Skipauth *bool `json:"skipauth,omitempty"`
	// whether to perform TLS. Only applies to the "spice" protocol
	Tls *bool `json:"tls,omitempty"`
}

// Trigger generation of broadcast RARP frames to update network
// switches.  This can be useful when network bonds fail-over the
// active slave.
//
// .. qmp-example::    -> { "execute": "announce-self",
// "arguments": {        "initial": 50, "max": 550, "rounds": 10,
// "step": 50,        "interfaces": ["vn2", "vn3"], "id": "bob" } }
// <- { "return": {} }
//
// Since: 4.0
type AnnounceSelfCommand struct {
	AnnounceParameters
}

// Request the balloon driver to change its balloon size.
//
// Errors:   - If the balloon driver is enabled but not functional
// because    the KVM kernel module cannot support it, KVMMissingCap
// - If no balloon device is present, DeviceNotActive
//
// .. note:: This command just issues a request to the guest. When it
// returns, the balloon size may not have changed. A guest can
// change the balloon size independent of this command.
//
// Since: 0.14
//
// .. qmp-example::   :annotated:    ::     -> { "execute": "balloon",
// "arguments": { "value": 536870912 } }    <- { "return": {} }
// With a 2.5GiB guest this command inflated the ballon to 3GiB.
type BalloonCommand struct {
	// the target logical size of the VM in bytes. We can deduce the
	// size of the balloon using this formula: logical_vm_size =
	// vm_ram_size - balloon_size From it we have: balloon_size =
	// vm_ram_size - @value
	Value int64 `json:"value"`
}

// Live commit of data from overlay image nodes into backing nodes -
// i.e., writes data between 'top' and 'base' into 'base'.  If top ==
// base, that is an error.  If top has no overlays on top of it, or if
// it is in use by a writer, the job will not be completed by itself.
// The user needs to complete the job with the block-job-complete
// command after getting the ready event.  (Since 2.0)  If the base
// image is smaller than top, then the base image will be resized to
// be the same size as top.  If top is smaller than the base image,
// the base will not be truncated.  If you want the base image size to
// match the size of the smaller top, you can safely truncate it
// yourself once the commit operation successfully completes.
//
// Errors:   - If @device does not exist, DeviceNotFound
//
// Since: 1.3
//
// .. qmp-example::    -> { "execute": "block-commit",
// "arguments": { "device": "virtio0",             "top":
// "/tmp/snap1.qcow2" } }   <- { "return": {} }
type BlockCommitCommand struct {
	// identifier for the newly-created block job. If omitted, the
	// device name will be used. (Since 2.7)
	JobId *string `json:"job-id,omitempty"`
	// the device name or node-name of a root node
	Device string `json:"device"`
	// The node name of the backing image to write data into. If not
	// specified, this is the deepest backing image. (since: 3.1)
	BaseNode *string `json:"base-node,omitempty"`
	// Same as @base-node, except that it is a file name rather than a
	// node name. This must be the exact filename string that was used
	// to open the node; other strings, even if addressing the same
	// file, are not accepted
	Base *string `json:"base,omitempty"`
	// The node name of the backing image within the image chain which
	// contains the topmost data to be committed down. If not
	// specified, this is the active layer. (since: 3.1)
	TopNode *string `json:"top-node,omitempty"`
	// Same as @top-node, except that it is a file name rather than a
	// node name. This must be the exact filename string that was used
	// to open the node; other strings, even if addressing the same
	// file, are not accepted
	Top *string `json:"top,omitempty"`
	// The backing file string to write into the overlay image of
	// 'top'. If 'top' does not have an overlay image, or if 'top' is
	// in use by a writer, specifying a backing file string is an
	// error. This filename is not validated. If a pathname string is
	// such that it cannot be resolved by QEMU, that means that
	// subsequent QMP or HMP commands must use node-names for the
	// image in question, as filename lookup methods will fail. If not
	// specified, QEMU will automatically determine the backing file
	// string to use, or error out if there is no obvious choice. Care
	// should be taken when specifying the string, to specify a valid
	// filename or protocol. (Since 2.1)
	BackingFile *string `json:"backing-file,omitempty"`
	// If true, replace any protocol mentioned in the 'backing file
	// format' with 'raw', rather than storing the protocol name as
	// the backing format. Can be used even when no image header will
	// be updated (default false; since 9.0).
	BackingMaskProtocol *bool `json:"backing-mask-protocol,omitempty"`
	// the maximum speed, in bytes per second
	Speed *int64 `json:"speed,omitempty"`
	// the action to take on an error. 'ignore' means that the request
	// should be retried. (default: report; Since: 5.0)
	OnError *BlockdevOnError `json:"on-error,omitempty"`
	// the node name that should be assigned to the filter driver that
	// the commit job inserts into the graph above @top. If this
	// option is not given, a node name is autogenerated. (Since: 2.9)
	FilterNodeName *string `json:"filter-node-name,omitempty"`
	// When false, this job will wait in a PENDING state after it has
	// finished its work, waiting for @block-job-finalize before
	// making any block graph changes. When true, this job will
	// automatically perform its abort or commit actions. Defaults to
	// true. (Since 3.1)
	AutoFinalize *bool `json:"auto-finalize,omitempty"`
	// When false, this job will wait in a CONCLUDED state after it
	// has completely ceased all work, and awaits @block-job-dismiss.
	// When true, this job will automatically disappear from the query
	// list without user intervention. Defaults to true. (Since 3.1)
	AutoDismiss *bool `json:"auto-dismiss,omitempty"`
}

// Create a dirty bitmap with a name on the node, and start tracking
// the writes.
//
// Errors:   - If @node is not a valid block device or node,
// DeviceNotFound   - If @name is already taken, GenericError
//
// Since: 2.4
//
// .. qmp-example::    -> { "execute": "block-dirty-bitmap-add",
// "arguments": { "node": "drive0", "name": "bitmap0" } }   <- {
// "return": {} }
type BlockDirtyBitmapAddCommand struct {
	BlockDirtyBitmapAdd
}

// Clear (reset) a dirty bitmap on the device, so that an incremental
// backup from this point in time forward will only backup clusters
// modified after this clear operation.
//
// Errors:   - If @node is not a valid block device, DeviceNotFound
// - If @name is not found, GenericError
//
// Since: 2.4
//
// .. qmp-example::    -> { "execute": "block-dirty-bitmap-clear",
// "arguments": { "node": "drive0", "name": "bitmap0" } }   <- {
// "return": {} }
type BlockDirtyBitmapClearCommand struct {
	BlockDirtyBitmap
}

// Disables a dirty bitmap so that it will stop tracking disk changes.
//
// Errors:   - If @node is not a valid block device, DeviceNotFound
// - If @name is not found, GenericError
//
// Since: 4.0
//
// .. qmp-example::    -> { "execute": "block-dirty-bitmap-disable",
// "arguments": { "node": "drive0", "name": "bitmap0" } }   <- {
// "return": {} }
type BlockDirtyBitmapDisableCommand struct {
	BlockDirtyBitmap
}

// Enables a dirty bitmap so that it will begin tracking disk changes.
//
// Errors:   - If @node is not a valid block device, DeviceNotFound
// - If @name is not found, GenericError
//
// Since: 4.0
//
// .. qmp-example::    -> { "execute": "block-dirty-bitmap-enable",
// "arguments": { "node": "drive0", "name": "bitmap0" } }   <- {
// "return": {} }
type BlockDirtyBitmapEnableCommand struct {
	BlockDirtyBitmap
}

// Merge dirty bitmaps listed in @bitmaps to the @target dirty bitmap.
// Dirty bitmaps in @bitmaps will be unchanged, except if it also
// appears as the @target bitmap.  Any bits already set in @target
// will still be set after the merge, i.e., this operation does not
// clear the target.  On error, @target is unchanged.  The resulting
// bitmap will count as dirty any clusters that were dirty in any of
// the source bitmaps.  This can be used to achieve backup
// checkpoints, or in simpler usages, to copy bitmaps.
//
// Errors:   - If @node is not a valid block device, DeviceNotFound
// - If any bitmap in @bitmaps or @target is not found,
// GenericError   - If any of the bitmaps have different sizes or
// granularities,    GenericError
//
// Since: 4.0
//
// .. qmp-example::    -> { "execute": "block-dirty-bitmap-merge",
// "arguments": { "node": "drive0", "target": "bitmap0",
// "bitmaps": ["bitmap1"] } }   <- { "return": {} }
type BlockDirtyBitmapMergeCommand struct {
	BlockDirtyBitmapMerge
}

// Stop write tracking and remove the dirty bitmap that was created
// with block-dirty-bitmap-add.  If the bitmap is persistent, remove
// it from its storage too.
//
// Errors:   - If @node is not a valid block device or node,
// DeviceNotFound   - If @name is not found, GenericError   - if @name
// is frozen by an operation, GenericError
//
// Since: 2.4
//
// .. qmp-example::    -> { "execute": "block-dirty-bitmap-remove",
// "arguments": { "node": "drive0", "name": "bitmap0" } }   <- {
// "return": {} }
type BlockDirtyBitmapRemoveCommand struct {
	BlockDirtyBitmap
}

// Creates a new block export.
//
// Since: 5.2
type BlockExportAddCommand struct {
	BlockExportOptions
}

// Request to remove a block export.  This drops the user's reference
// to the export, but the export may still stay around after this
// command returns until the shutdown of the export has completed.
//
// Errors:   - if the export is not found   - if @mode is 'safe' and
// the export is still in use (e.g. by    existing client connections)
//
// Since: 5.2
type BlockExportDelCommand struct {
	// Block export id.
	Id string `json:"id"`
	// Mode of command operation. See @BlockExportRemoveMode
	// description. Default is 'safe'.
	Mode *BlockExportRemoveMode `json:"mode,omitempty"`
}

// Stop an active background block operation.  This command returns
// immediately after marking the active background block operation for
// cancellation.  It is an error to call this command if no operation
// is in progress.  The operation will cancel as soon as possible and
// then emit the BLOCK_JOB_CANCELLED event.  Before that happens the
// job is still visible when enumerated using query-block-jobs.  Note
// that if you issue 'block-job-cancel' after 'drive-mirror' has
// indicated (via the event BLOCK_JOB_READY) that the source and
// destination are synchronized, then the event triggered by this
// command changes to BLOCK_JOB_COMPLETED, to indicate that the
// mirroring has ended and the destination now has a point-in-time
// copy tied to the time of the cancellation.  For streaming, the
// image file retains its backing file unless the streaming operation
// happens to complete just as it is being cancelled.  A new streaming
// operation can be started at a later time to finish copying all data
// from the backing file.
//
// Errors:   - If no background operation is active on this device,
// DeviceNotActive
//
// Since: 1.1
type BlockJobCancelCommand struct {
	// The job identifier. This used to be a device name (hence the
	// name of the parameter), but since QEMU 2.7 it can have other
	// values.
	Device string `json:"device"`
	// If true, and the job has already emitted the event
	// BLOCK_JOB_READY, abandon the job immediately (even if it is
	// paused) instead of waiting for the destination to complete its
	// final synchronization (since 1.3)
	Force *bool `json:"force,omitempty"`
}

// Change the block job's options.
//
// Since: 8.2
type BlockJobChangeCommand struct {
	BlockJobChangeOptions
}

// Manually trigger completion of an active background block
// operation. This is supported for drive mirroring, where it also
// switches the device to write to the target path only.  The ability
// to complete is signaled with a BLOCK_JOB_READY event.  This command
// completes an active background block operation synchronously.  The
// ordering of this command's return with the BLOCK_JOB_COMPLETED
// event is not defined.  Note that if an I/O error occurs during the
// processing of this command: 1) the command itself will fail; 2) the
// error will be processed according to the rerror/werror arguments
// that were specified when starting the operation.  A cancelled or
// paused job cannot be completed.
//
// Errors:   - If no background operation is active on this device,
// DeviceNotActive
//
// Since: 1.3
type BlockJobCompleteCommand struct {
	// The job identifier. This used to be a device name (hence the
	// name of the parameter), but since QEMU 2.7 it can have other
	// values.
	Device string `json:"device"`
}

// For jobs that have already concluded, remove them from the block-
// job-query list.  This command only needs to be run for jobs which
// were started with QEMU 2.12+ job lifetime management semantics.
// This command will refuse to operate on any job that has not yet
// reached its terminal state, JOB_STATUS_CONCLUDED.  For jobs that
// make use of the BLOCK_JOB_READY event, block-job-cancel or block-
// job-complete will still need to be used as appropriate.
//
// Since: 2.12
type BlockJobDismissCommand struct {
	// The job identifier.
	Id string `json:"id"`
}

// Once a job that has manual=true reaches the pending state, it can
// be instructed to finalize any graph changes and do any necessary
// cleanup via this command.  For jobs in a transaction, instructing
// one job to finalize will force ALL jobs in the transaction to
// finalize, so it is only necessary to instruct a single member job
// to finalize.
//
// Since: 2.12
type BlockJobFinalizeCommand struct {
	// The job identifier.
	Id string `json:"id"`
}

// Pause an active background block operation.  This command returns
// immediately after marking the active background block operation for
// pausing.  It is an error to call this command if no operation is in
// progress or if the job is already paused.  The operation will pause
// as soon as possible.  No event is emitted when the operation is
// actually paused.  Cancelling a paused job automatically resumes it.
//
// Errors:   - If no background operation is active on this device,
// DeviceNotActive
//
// Since: 1.3
type BlockJobPauseCommand struct {
	// The job identifier. This used to be a device name (hence the
	// name of the parameter), but since QEMU 2.7 it can have other
	// values.
	Device string `json:"device"`
}

// Resume an active background block operation.  This command returns
// immediately after resuming a paused background block operation.  It
// is an error to call this command if no operation is in progress or
// if the job is not paused.  This command also clears the error
// status of the job.
//
// Errors:   - If no background operation is active on this device,
// DeviceNotActive
//
// Since: 1.3
type BlockJobResumeCommand struct {
	// The job identifier. This used to be a device name (hence the
	// name of the parameter), but since QEMU 2.7 it can have other
	// values.
	Device string `json:"device"`
}

// Set maximum speed for a background block operation.  This command
// can only be issued when there is an active block job.  Throttling
// can be disabled by setting the speed to 0.
//
// Errors:   - If no background operation is active on this device,
// DeviceNotActive
//
// Since: 1.1
type BlockJobSetSpeedCommand struct {
	// The job identifier. This used to be a device name (hence the
	// name of the parameter), but since QEMU 2.7 it can have other
	// values.
	Device string `json:"device"`
	// the maximum speed, in bytes per second, or 0 for unlimited.
	// Defaults to 0.
	Speed int64 `json:"speed"`
}

// Manage read, write and flush latency histograms for the device.  If
// only @id parameter is specified, remove all present latency
// histograms for the device.  Otherwise, add/reset some of (or all)
// latency histograms.
//
// Errors:   - if device is not found or any boundary arrays are
// invalid.
//
// Since: 4.0
//
// .. qmp-example::   :annotated:    Set new histograms for all io
// types with intervals   [0, 10), [10, 50), [50, 100), [100, +inf)::
// -> { "execute": "block-latency-histogram-set",      "arguments": {
// "id": "drive0",             "boundaries": [10, 50, 100] } }   <- {
// "return": {} }  .. qmp-example::   :annotated:    Set new histogram
// only for write, other histograms will remain   not changed (or not
// created)::    -> { "execute": "block-latency-histogram-set",
// "arguments": { "id": "drive0",             "boundaries-write": [10,
// 50, 100] } }   <- { "return": {} }  .. qmp-example::   :annotated:
// Set new histograms with the following intervals:    - read, flush:
// [0, 10), [10, 50), [50, 100), [100, +inf)   - write: [0, 1000),
// [1000, 5000), [5000, +inf)    ::    -> { "execute": "block-latency-
// histogram-set",      "arguments": { "id": "drive0",
// "boundaries": [10, 50, 100],             "boundaries-write": [1000,
// 5000] } }   <- { "return": {} }  .. qmp-example::   :annotated:
// Remove all latency histograms::    -> { "execute": "block-latency-
// histogram-set",      "arguments": { "id": "drive0" } }   <- {
// "return": {} }
type BlockLatencyHistogramSetCommand struct {
	// The name or QOM path of the guest device.
	Id string `json:"id"`
	// list of interval boundary values (see description in
	// BlockLatencyHistogramInfo definition). If specified, all
	// latency histograms are removed, and empty ones created for all
	// io types with intervals corresponding to @boundaries (except
	// for io types, for which specific boundaries are set through the
	// following parameters).
	Boundaries []uint64 `json:"boundaries,omitempty"`
	// list of interval boundary values for read latency histogram. If
	// specified, old read latency histogram is removed, and empty one
	// created with intervals corresponding to @boundaries-read. The
	// parameter has higher priority then @boundaries.
	BoundariesRead []uint64 `json:"boundaries-read,omitempty"`
	// list of interval boundary values for write latency histogram.
	BoundariesWrite []uint64 `json:"boundaries-write,omitempty"`
	// list of interval boundary values for zone append write latency
	// histogram.
	BoundariesZap []uint64 `json:"boundaries-zap,omitempty"`
	// list of interval boundary values for flush latency histogram.
	BoundariesFlush []uint64 `json:"boundaries-flush,omitempty"`
}

// Change the write threshold for a block drive.  An event will be
// delivered if a write to this block drive crosses the configured
// threshold.  The threshold is an offset, thus must be non-negative.
// Default is no write threshold.  Setting the threshold to zero
// disables it.  This is useful to transparently resize thin-
// provisioned drives without the guest OS noticing.
//
// Since: 2.3
//
// .. qmp-example::    -> { "execute": "block-set-write-threshold",
// "arguments": { "node-name": "mydev",             "write-threshold":
// 17179869184 } }   <- { "return": {} }
type BlockSetWriteThresholdCommand struct {
	// graph node name on which the threshold must be set.
	NodeName string `json:"node-name"`
	// configured threshold for the block device, bytes. Use 0 to
	// disable the threshold.
	WriteThreshold uint64 `json:"write-threshold"`
}

// Copy data from a backing file into a block device.  The block
// streaming operation is performed in the background until the entire
// backing file has been copied.  This command returns immediately
// once streaming has started.  The status of ongoing block streaming
// operations can be checked with query-block-jobs.  The operation can
// be stopped before it has completed using the block-job-cancel
// command.  The node that receives the data is called the top image,
// can be located in any part of the chain (but always above the base
// image; see below) and can be specified using its device or node
// name. Earlier qemu versions only allowed 'device' to name the top
// level node; presence of the 'base-node' parameter during
// introspection can be used as a witness of the enhanced semantics of
// 'device'.  If a base file is specified then sectors are not copied
// from that base file and its backing chain.  This can be used to
// stream a subset of the backing file chain instead of flattening the
// entire image.  When streaming completes the image file will have
// the base file as its backing file, unless that node was changed
// while the job was running.  In that case, base's parent's backing
// (or filtered, whichever exists) child (i.e., base at the beginning
// of the job) will be the new backing file.  On successful completion
// the image file is updated to drop the backing file and the
// BLOCK_JOB_COMPLETED event is emitted.  In case @device is a filter
// node, block-stream modifies the first non-filter overlay node below
// it to point to the new backing node instead of modifying @device
// itself.
//
// Errors:   - If @device does not exist, DeviceNotFound.
//
// Since: 1.1
//
// .. qmp-example::    -> { "execute": "block-stream",
// "arguments": { "device": "virtio0",             "base":
// "/tmp/master.qcow2" } }   <- { "return": {} }
type BlockStreamCommand struct {
	// identifier for the newly-created block job. If omitted, the
	// device name will be used. (Since 2.7)
	JobId *string `json:"job-id,omitempty"`
	// the device or node name of the top image
	Device string `json:"device"`
	// the common backing file name. It cannot be set if @base-node or
	// @bottom is also set.
	Base *string `json:"base,omitempty"`
	// the node name of the backing file. It cannot be set if @base or
	// @bottom is also set. (Since 2.8)
	BaseNode *string `json:"base-node,omitempty"`
	// The backing file string to write into the top image. This
	// filename is not validated. If a pathname string is such that it
	// cannot be resolved by QEMU, that means that subsequent QMP or
	// HMP commands must use node-names for the image in question, as
	// filename lookup methods will fail. If not specified, QEMU will
	// automatically determine the backing file string to use, or
	// error out if there is no obvious choice. Care should be taken
	// when specifying the string, to specify a valid filename or
	// protocol. (Since 2.1)
	BackingFile *string `json:"backing-file,omitempty"`
	// If true, replace any protocol mentioned in the 'backing file
	// format' with 'raw', rather than storing the protocol name as
	// the backing format. Can be used even when no image header will
	// be updated (default false; since 9.0).
	BackingMaskProtocol *bool `json:"backing-mask-protocol,omitempty"`
	// the last node in the chain that should be streamed into top. It
	// cannot be set if @base or @base-node is also set. It cannot be
	// filter node. (Since 6.0)
	Bottom *string `json:"bottom,omitempty"`
	// the maximum speed, in bytes per second
	Speed *int64 `json:"speed,omitempty"`
	// the action to take on an error (default report). 'stop' and
	// 'enospc' can only be used if the block device supports io-
	// status (see BlockInfo). (Since 1.3)
	OnError *BlockdevOnError `json:"on-error,omitempty"`
	// the node name that should be assigned to the filter driver that
	// the stream job inserts into the graph above @device. If this
	// option is not given, a node name is autogenerated. (Since: 6.0)
	FilterNodeName *string `json:"filter-node-name,omitempty"`
	// When false, this job will wait in a PENDING state after it has
	// finished its work, waiting for @block-job-finalize before
	// making any block graph changes. When true, this job will
	// automatically perform its abort or commit actions. Defaults to
	// true. (Since 3.1)
	AutoFinalize *bool `json:"auto-finalize,omitempty"`
	// When false, this job will wait in a CONCLUDED state after it
	// has completely ceased all work, and awaits @block-job-dismiss.
	// When true, this job will automatically disappear from the query
	// list without user intervention. Defaults to true. (Since 3.1)
	AutoDismiss *bool `json:"auto-dismiss,omitempty"`
}

// Resize a block image while a guest is running.  Either @device or
// @node-name must be set but not both.
//
// Errors:   - If @device is not a valid block device, DeviceNotFound
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "block_resize",
// "arguments": { "device": "scratch", "size": 1073741824 } }   <- {
// "return": {} }
type BlockResizeCommand struct {
	// the name of the device to get the image resized
	Device *string `json:"device,omitempty"`
	// graph node name to get the image resized (Since 2.0)
	NodeName *string `json:"node-name,omitempty"`
	// new image size in bytes
	Size int64 `json:"size"`
}

// Change I/O throttle limits for a block drive.  Since QEMU 2.4, each
// device with I/O limits is member of a throttle group.  If two or
// more devices are members of the same group, the limits will apply
// to the combined I/O of the whole group in a round-robin fashion.
// Therefore, setting new I/O limits to a device will affect the whole
// group.  The name of the group can be specified using the 'group'
// parameter. If the parameter is unset, it is assumed to be the
// current group of that device.  If it's not in any group yet, the
// name of the device will be used as the name for its group.  The
// 'group' parameter can also be used to move a device to a different
// group.  In this case the limits specified in the parameters will be
// applied to the new group only.  I/O limits can be disabled by
// setting all of them to 0.  In this case the device will be removed
// from its group and the rest of its members will not be affected.
// The 'group' parameter is ignored.
//
// Errors:   - If @device is not a valid block device, DeviceNotFound
//
// Since: 1.1
//
// .. qmp-example::    -> { "execute": "block_set_io_throttle",
// "arguments": { "id": "virtio-blk-pci0/virtio-backend",
// "bps": 0,             "bps_rd": 0,             "bps_wr": 0,
// "iops": 512,             "iops_rd": 0,             "iops_wr": 0,
// "bps_max": 0,             "bps_rd_max": 0,
// "bps_wr_max": 0,             "iops_max": 0,
// "iops_rd_max": 0,             "iops_wr_max": 0,
// "bps_max_length": 0,             "iops_size": 0 } }   <- {
// "return": {} }  .. qmp-example::    -> { "execute":
// "block_set_io_throttle",      "arguments": { "id": "ide0-1-0",
// "bps": 1000000,             "bps_rd": 0,             "bps_wr": 0,
// "iops": 0,             "iops_rd": 0,             "iops_wr": 0,
// "bps_max": 8000000,             "bps_rd_max": 0,
// "bps_wr_max": 0,             "iops_max": 0,
// "iops_rd_max": 0,             "iops_wr_max": 0,
// "bps_max_length": 60,             "iops_size": 0 } }   <- {
// "return": {} }
type BlockSetIoThrottleCommand struct {
	BlockIOThrottle
}

// Creates a new block device.
//
// Since: 2.9
//
// .. qmp-example::    -> { "execute": "blockdev-add",
// "arguments": {        "driver": "qcow2",        "node-name":
// "test1",        "file": {          "driver": "file",
// "filename": "test.qcow2"         }      }     }   <- { "return": {}
// }  .. qmp-example::    -> { "execute": "blockdev-add",
// "arguments": {        "driver": "qcow2",        "node-name":
// "node0",        "discard": "unmap",        "cache": {
// "direct": true         },         "file": {          "driver":
// "file",          "filename": "/tmp/test.qcow2"         },
// "backing": {          "driver": "raw",          "file": {
// "driver": "file",            "filename": "/dev/fdset/4"           }
// }       }      }    <- { "return": {} }
type BlockdevAddCommand struct {
	BlockdevOptions
}

// Start a point-in-time copy of a block device to a new destination.
// The status of ongoing blockdev-backup operations can be checked
// with query-block-jobs where the BlockJobInfo.type field has the
// value 'backup'.  The operation can be stopped before it has
// completed using the block-job-cancel command.
//
// Errors:   - If @device is not a valid block device, DeviceNotFound
//
// Since: 2.3
//
// .. qmp-example::    -> { "execute": "blockdev-backup",
// "arguments": { "device": "src-id",             "sync": "full",
// "target": "tgt-id" } }   <- { "return": {} }
type BlockdevBackupCommand struct {
	BlockdevBackup
}

// Changes the medium inserted into a block device by ejecting the
// current medium and loading a new image file which is inserted as
// the new medium (this command combines blockdev-open-tray, blockdev-
// remove-medium, blockdev-insert-medium and blockdev-close-tray).
//
// Since: 2.5
//
// .. qmp-example::   :title: Change a removable medium    -> {
// "execute": "blockdev-change-medium",      "arguments": { "id":
// "ide0-1-0",             "filename":
// "/srv/images/Fedora-12-x86_64-DVD.iso",             "format": "raw"
// } }   <- { "return": {} }  .. qmp-example::   :title: Load a read-
// only medium into a writable drive    -> { "execute": "blockdev-
// change-medium",      "arguments": { "id": "floppyA",
// "filename": "/srv/images/ro.img",             "format": "raw",
// "read-only-mode": "retain" } }    <- { "error":      { "class":
// "GenericError",       "desc": "Could not open '/srv/images/ro.img':
// Permission denied" } }    -> { "execute": "blockdev-change-medium",
// "arguments": { "id": "floppyA",             "filename":
// "/srv/images/ro.img",             "format": "raw",
// "read-only-mode": "read-only" } }    <- { "return": {} }
type BlockdevChangeMediumCommand struct {
	// Block device name
	Device *string `json:"device,omitempty"`
	// The name or QOM path of the guest device (since: 2.8)
	Id *string `json:"id,omitempty"`
	// filename of the new image to be loaded
	Filename string `json:"filename"`
	// format to open the new image with (defaults to the probed
	// format)
	Format *string `json:"format,omitempty"`
	// if false (the default), an eject request through blockdev-open-
	// tray will be sent to the guest if it has locked the tray (and
	// the tray will not be opened immediately); if true, the tray
	// will be opened regardless of whether it is locked. (since 7.1)
	Force *bool `json:"force,omitempty"`
	// change the read-only mode of the device; defaults to 'retain'
	ReadOnlyMode *BlockdevChangeReadOnlyMode `json:"read-only-mode,omitempty"`
}

// Closes a block device's tray.  If there is a block driver state
// tree associated with the block device (which is currently ejected),
// that tree will be loaded as the medium.  If the tray was already
// closed before, this will be a no-op.
//
// Since: 2.5
//
// .. qmp-example::    -> { "execute": "blockdev-close-tray",
// "arguments": { "id": "ide0-1-0" } }    <- { "timestamp": {
// "seconds": 1418751345,             "microseconds": 272147 },
// "event": "DEVICE_TRAY_MOVED",      "data": { "device": "ide1-cd0",
// "id": "ide0-1-0",           "tray-open": false } }    <- {
// "return": {} }
type BlockdevCloseTrayCommand struct {
	// Block device name
	Device *string `json:"device,omitempty"`
	// The name or QOM path of the guest device (since: 2.8)
	Id *string `json:"id,omitempty"`
}

// Starts a job to create an image format on a given node.  The job is
// automatically finalized, but a manual job-dismiss is required.
//
// Since: 3.0
type BlockdevCreateCommand struct {
	// Identifier for the newly created job.
	JobId string `json:"job-id"`
	// Options for the image creation.
	Options BlockdevCreateOptions `json:"options"`
}

// Deletes a block device that has been added using blockdev-add.  The
// command will fail if the node is attached to a device or is
// otherwise being used.
//
// Since: 2.9
//
// .. qmp-example::    -> { "execute": "blockdev-add",
// "arguments": {        "driver": "qcow2",        "node-name":
// "node0",        "file": {          "driver": "file",
// "filename": "test.qcow2"        }      }     }   <- { "return": {}
// }    -> { "execute": "blockdev-del",      "arguments": { "node-
// name": "node0" }     }   <- { "return": {} }
type BlockdevDelCommand struct {
	// Name of the graph node to delete.
	NodeName string `json:"node-name"`
}

// Inserts a medium (a block driver state tree) into a block device.
// That block device's tray must currently be open (unless there is no
// attached guest device) and there must be no medium inserted
// already.
//
// Since: 2.12
//
// .. qmp-example::    -> { "execute": "blockdev-add",
// "arguments": {        "node-name": "node0",        "driver": "raw",
// "file": { "driver": "file",             "filename": "fedora.iso" }
// } }   <- { "return": {} }    -> { "execute": "blockdev-insert-
// medium",      "arguments": { "id": "ide0-1-0",             "node-
// name": "node0" } }    <- { "return": {} }
type BlockdevInsertMediumCommand struct {
	// The name or QOM path of the guest device
	Id string `json:"id"`
	// name of a node in the block driver state graph
	NodeName string `json:"node-name"`
}

// Start mirroring a block device's writes to a new destination.
//
// Since: 2.6
//
// .. qmp-example::    -> { "execute": "blockdev-mirror",
// "arguments": { "device": "ide-hd0",             "target":
// "target0",             "sync": "full" } }   <- { "return": {} }
type BlockdevMirrorCommand struct {
	// identifier for the newly-created block job. If omitted, the
	// device name will be used. (Since 2.7)
	JobId *string `json:"job-id,omitempty"`
	// The device name or node-name of a root node whose writes should
	// be mirrored.
	Device string `json:"device"`
	// the id or node-name of the block device to mirror to. This
	// mustn't be attached to guest.
	Target string `json:"target"`
	// with sync=full graph node name to be replaced by the new image
	// when a whole image copy is done. This can be used to repair
	// broken Quorum files. By default, @device is replaced, although
	// implicitly created filters on it are kept.
	Replaces *string `json:"replaces,omitempty"`
	// what parts of the disk image should be copied to the
	// destination (all the disk, only the sectors allocated in the
	// topmost image, or only new I/O).
	Sync MirrorSyncMode `json:"sync"`
	// the maximum speed, in bytes per second
	Speed *int64 `json:"speed,omitempty"`
	// granularity of the dirty bitmap, default is 64K if the image
	// format doesn't have clusters, 4K if the clusters are smaller
	// than that, else the cluster size. Must be a power of 2 between
	// 512 and 64M
	Granularity *uint32 `json:"granularity,omitempty"`
	// maximum amount of data in flight from source to target
	BufSize *int64 `json:"buf-size,omitempty"`
	// the action to take on an error on the source, default 'report'.
	// 'stop' and 'enospc' can only be used if the block device
	// supports io-status (see BlockInfo).
	OnSourceError *BlockdevOnError `json:"on-source-error,omitempty"`
	// the action to take on an error on the target, default 'report'
	// (no limitations, since this applies to a different block device
	// than @device).
	OnTargetError *BlockdevOnError `json:"on-target-error,omitempty"`
	// the node name that should be assigned to the filter driver that
	// the mirror job inserts into the graph above @device. If this
	// option is not given, a node name is autogenerated. (Since: 2.9)
	FilterNodeName *string `json:"filter-node-name,omitempty"`
	// when to copy data to the destination; defaults to 'background'
	// (Since: 3.0)
	CopyMode *MirrorCopyMode `json:"copy-mode,omitempty"`
	// When false, this job will wait in a PENDING state after it has
	// finished its work, waiting for @block-job-finalize before
	// making any block graph changes. When true, this job will
	// automatically perform its abort or commit actions. Defaults to
	// true. (Since 3.1)
	AutoFinalize *bool `json:"auto-finalize,omitempty"`
	// When false, this job will wait in a CONCLUDED state after it
	// has completely ceased all work, and awaits @block-job-dismiss.
	// When true, this job will automatically disappear from the query
	// list without user intervention. Defaults to true. (Since 3.1)
	AutoDismiss *bool `json:"auto-dismiss,omitempty"`
}

// Opens a block device's tray.  If there is a block driver state tree
// inserted as a medium, it will become inaccessible to the guest (but
// it will remain associated to the block device, so closing the tray
// will make it accessible again).  If the tray was already open
// before, this will be a no-op.  Once the tray opens, a
// DEVICE_TRAY_MOVED event is emitted.  There are cases in which no
// such event will be generated, these include:  - if the guest has
// locked the tray, @force is false and the guest   does not respond
// to the eject request - if the BlockBackend denoted by @device does
// not have a guest   device attached to it - if the guest device does
// not have an actual tray
//
// Since: 2.5
//
// .. qmp-example::    -> { "execute": "blockdev-open-tray",
// "arguments": { "id": "ide0-1-0" } }    <- { "timestamp": {
// "seconds": 1418751016,             "microseconds": 716996 },
// "event": "DEVICE_TRAY_MOVED",      "data": { "device": "ide1-cd0",
// "id": "ide0-1-0",           "tray-open": true } }    <- { "return":
// {} }
type BlockdevOpenTrayCommand struct {
	// Block device name
	Device *string `json:"device,omitempty"`
	// The name or QOM path of the guest device (since: 2.8)
	Id *string `json:"id,omitempty"`
	// if false (the default), an eject request will be sent to the
	// guest if it has locked the tray (and the tray will not be
	// opened immediately); if true, the tray will be opened
	// regardless of whether it is locked
	Force *bool `json:"force,omitempty"`
}

// Removes a medium (a block driver state tree) from a block device.
// That block device's tray must currently be open (unless there is no
// attached guest device).  If the tray is open and there is no medium
// inserted, this will be a no-op.
//
// Since: 2.12
//
// .. qmp-example::    -> { "execute": "blockdev-remove-medium",
// "arguments": { "id": "ide0-1-0" } }    <- { "error": { "class":
// "GenericError",           "desc": "Tray of device 'ide0-1-0' is not
// open" } }    -> { "execute": "blockdev-open-tray",
// "arguments": { "id": "ide0-1-0" } }    <- { "timestamp": {
// "seconds": 1418751627,             "microseconds": 549958 },
// "event": "DEVICE_TRAY_MOVED",      "data": { "device": "ide1-cd0",
// "id": "ide0-1-0",           "tray-open": true } }    <- { "return":
// {} }    -> { "execute": "blockdev-remove-medium",      "arguments":
// { "id": "ide0-1-0" } }    <- { "return": {} }
type BlockdevRemoveMediumCommand struct {
	// The name or QOM path of the guest device
	Id string `json:"id"`
}

// Reopens one or more block devices using the given set of options.
// Any option not specified will be reset to its default value
// regardless of its previous status.  If an option cannot be changed
// or a particular driver does not support reopening then the command
// will return an error.  All devices in the list are reopened in one
// transaction, so if one of them fails then the whole transaction is
// cancelled.  The command receives a list of block devices to reopen.
// For each one of them, the top-level @node-name option (from
// BlockdevOptions) must be specified and is used to select the block
// device to be reopened.  Other @node-name options must be either
// omitted or set to the current name of the appropriate node.  This
// command won't change any node name and any attempt to do it will
// result in an error.  In the case of options that refer to child
// nodes, the behavior of this command depends on the value:   1) A
// set of options (BlockdevOptions): the child is reopened with
// the specified set of options.   2) A reference to the current
// child: the child is reopened using     its existing set of options.
// 3) A reference to a different node: the current child is replaced
// with the specified one.   4) NULL: the current child (if any) is
// detached.  Options (1) and (2) are supported in all cases.  Option
// (3) is supported for @file and @backing, and option (4) for
// @backing only.  Unlike with blockdev-add, the @backing option must
// always be present unless the node being reopened does not have a
// backing file and its image does not have a default backing file
// name as part of its metadata.
//
// Since: 6.1
type BlockdevReopenCommand struct {
	Options []BlockdevOptions `json:"options"`
}

// Activate or inactivate a block device. Use this to manage the
// handover of block devices on migration with qemu-storage-daemon.
// Activating a node automatically activates all of its child nodes
// first. Inactivating a node automatically inactivates any of its
// child nodes that are not in use by a still active node.
//
// Since: 10.0
//
// .. qmp-example::    -> { "execute": "blockdev-set-active",
// "arguments": {        "node-name": "node0",        "active": false
// }     }   <- { "return": {} }
type BlockdevSetActiveCommand struct {
	// Name of the graph node to activate or inactivate. By default,
	// all nodes are affected by the operation.
	NodeName *string `json:"node-name,omitempty"`
	// true if the nodes should be active when the command returns
	// success, false if they should be inactive.
	Active bool `json:"active"`
}

// Takes a snapshot of a block device.  Take a snapshot, by installing
// 'node' as the backing image of 'overlay'.  Additionally, if 'node'
// is associated with a block device, the block device changes to
// using 'overlay' as its new active image.
//
// Since: 2.5
//
// .. qmp-example::    -> { "execute": "blockdev-add",
// "arguments": { "driver": "qcow2",             "node-name":
// "node1534",             "file": { "driver": "file",
// "filename": "hd1.qcow2" },             "backing": null } }    <- {
// "return": {} }    -> { "execute": "blockdev-snapshot",
// "arguments": { "node": "ide-hd0",             "overlay": "node1534"
// } }   <- { "return": {} }
type BlockdevSnapshotCommand struct {
	BlockdevSnapshot
}

// Synchronously delete an internal snapshot of a block device, when
// the format of the image used support it.  The snapshot is
// identified by name or id or both.  One of the name or id is
// required.  Return SnapshotInfo for the successfully deleted
// snapshot.
//
// Returns:   SnapshotInfo
//
// Errors:   - If @device is not a valid block device, GenericError
// - If snapshot not found, GenericError   - If the format of the
// image used does not support it,    GenericError   - If @id and
// @name are both not specified, GenericError
//
// Since: 1.7
//
// .. qmp-example::    -> { "execute": "blockdev-snapshot-delete-
// internal-sync",      "arguments": { "device": "ide-hd0",
// "name": "snapshot0" }     }   <- { "return": {             "id":
// "1",             "name": "snapshot0",             "vm-state-size":
// 0,             "date-sec": 1000012,             "date-nsec": 10,
// "vm-clock-sec": 100,             "vm-clock-nsec": 20,
// "icount": 220414      }     }
type BlockdevSnapshotDeleteInternalSyncCommand struct {
	// the device name or node-name of a root node to delete the
	// snapshot from
	Device string `json:"device"`
	// optional the snapshot's ID to be deleted
	Id *string `json:"id,omitempty"`
	// optional the snapshot's name to be deleted
	Name *string `json:"name,omitempty"`
}

// Synchronously take an internal snapshot of a block device, when the
// format of the image used supports it.  If the name is an empty
// string, or a snapshot with name already exists, the operation will
// fail.
//
// Errors:   - If @device is not a valid block device, GenericError
// - If any snapshot matching @name exists, or @name is empty,
// GenericError   - If the format of the image used does not support
// it,    GenericError
//
// .. note:: Only some image formats such as qcow2 and rbd support
// internal snapshots.
//
// Since: 1.7
//
// .. qmp-example::    -> { "execute": "blockdev-snapshot-internal-
// sync",      "arguments": { "device": "ide-hd0",             "name":
// "snapshot0" }     }   <- { "return": {} }
type BlockdevSnapshotInternalSyncCommand struct {
	BlockdevSnapshotInternal
}

// Takes a synchronous snapshot of a block device.
//
// Errors:   - If @device is not a valid block device, DeviceNotFound
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "blockdev-snapshot-sync",
// "arguments": { "device": "ide-hd0",             "snapshot-file":
// "/some/place/my-image",             "format": "qcow2" } }   <- {
// "return": {} }
type BlockdevSnapshotSyncCommand struct {
	BlockdevSnapshotSync
}

// Start measuring dirty page rate of the VM.  Results can be
// retrieved with @query-dirty-rate after measurements are completed.
// Dirty page rate is the number of pages changed in a given time
// period expressed in MiB/s.  The following methods of calculation
// are available:  1. In page sampling mode, a random subset of pages
// are selected and    hashed twice: once at the beginning of
// measurement time period,    and once again at the end.  If two
// hashes for some page are    different, the page is counted as
// changed.  Since this method    relies on sampling and hashing,
// calculated dirty page rate is    only an estimate of its true
// value.  Increasing @sample-pages    improves estimation quality at
// the cost of higher computational    overhead.  2. Dirty bitmap mode
// captures writes to memory (for example by    temporarily revoking
// write access to all pages) and counting page    faults.
// Information about modified pages is collected into a    bitmap,
// where each bit corresponds to one guest page.  This mode
// requires that KVM accelerator property "dirty-ring-size" is *not*
// set.  3. Dirty ring mode is similar to dirty bitmap mode, but the
// information about modified pages is collected into ring buffer.
// This mode tracks page modification per each vCPU separately.  It
// requires that KVM accelerator property "dirty-ring-size" is set.
//
// Since: 5.2
//
// .. qmp-example::    -> {"execute": "calc-dirty-rate", "arguments":
// {"calc-time": 1,                           "sample-pages": 512} }
// <- { "return": {} }  .. qmp-example::   :annotated:    Measure
// dirty rate using dirty bitmap for 500 milliseconds::    ->
// {"execute": "calc-dirty-rate", "arguments": {"calc-time": 500,
// "calc-time-unit": "millisecond", "mode": "dirty-bitmap"} }    <- {
// "return": {} }
type CalcDirtyRateCommand struct {
	// time period for which dirty page rate is calculated. By default
	// it is specified in seconds, but the unit can be set explicitly
	// with @calc-time-unit. Note that larger @calc-time values will
	// typically result in smaller dirty page rates because page
	// dirtying is a one-time event. Once some page is counted as
	// dirty during @calc-time period, further writes to this page
	// will not increase dirty page rate anymore.
	CalcTime int64 `json:"calc-time"`
	// time unit in which @calc-time is specified. By default it is
	// seconds. (Since 8.2)
	CalcTimeUnit *TimeUnit `json:"calc-time-unit,omitempty"`
	// number of sampled pages per each GiB of guest memory. Default
	// value is 512. For 4KiB guest pages this corresponds to sampling
	// ratio of 0.2%. This argument is used only in page sampling
	// mode. (Since 6.1)
	SamplePages *int64 `json:"sample-pages,omitempty"`
	// mechanism for tracking dirty pages. Default value is 'page-
	// sampling'. Others are 'dirty-bitmap' and 'dirty-ring'. (Since
	// 6.1)
	Mode *DirtyRateMeasureMode `json:"mode,omitempty"`
}

// Cancel the upper limit of dirty page rate for virtual CPUs.  Cancel
// the dirty page limit for the vCPU which has been set with set-vcpu-
// dirty-limit command.  Note that this command requires support from
// dirty ring, same as the "set-vcpu-dirty-limit".
//
// Since: 7.1
//
// .. qmp-example::    -> {"execute": "cancel-vcpu-dirty-limit"},
// "arguments": { "cpu-index": 1 } }   <- { "return": {} }
type CancelVcpuDirtyLimitCommand struct {
	// index of a virtual CPU, default is all.
	CpuIndex *int64 `json:"cpu-index,omitempty"`
}

// Change the backing file in the image file metadata.  This does not
// cause QEMU to reopen the image file to reparse the backing filename
// (it may, however, perform a reopen to change permissions from r/o
// -> r/w -> r/o, if needed).  The new backing file string is written
// into the image file metadata, and the QEMU internal strings are
// updated.
//
// Errors:   - If "device" does not exist or cannot be determined,
// DeviceNotFound
//
// Since: 2.1
type ChangeBackingFileCommand struct {
	// The device name or node-name of the root node that owns image-
	// node-name.
	Device string `json:"device"`
	// The name of the block driver state node of the image to modify.
	// The "device" argument is used to verify "image-node-name" is in
	// the chain described by "device".
	ImageNodeName string `json:"image-node-name"`
	// The string to write as the backing file. This string is not
	// validated, so care should be taken when specifying the string
	// or the image chain may not be able to be reopened again.
	BackingFile string `json:"backing-file"`
}

// Change the VNC server password.
//
// Since: 1.1
//
// .. note:: An empty password in this command will set the password
// to   the empty string. Existing clients are unaffected by executing
// this command.
type ChangeVncPasswordCommand struct {
	// the new password to use with VNC authentication
	Password string `json:"password"`
}

// Add a character device backend
//
// Returns: ChardevReturn.
//
// Since: 1.4
//
// .. qmp-example::    -> { "execute" : "chardev-add",
// "arguments" : { "id" : "foo",              "backend" : { "type" :
// "null", "data" : {} } } }   <- { "return": {} }  .. qmp-example::
// -> { "execute" : "chardev-add",      "arguments" : { "id" : "bar",
// "backend" : { "type" : "file",                     "data" : { "out"
// : "/tmp/bar.log" } } } }   <- { "return": {} }  .. qmp-example::
// -> { "execute" : "chardev-add",      "arguments" : { "id" : "baz",
// "backend" : { "type" : "pty", "data" : {} } } }   <- { "return": {
// "pty" : "/dev/pty/42" } }
type ChardevAddCommand struct {
	// the chardev's ID, must be unique
	Id string `json:"id"`
	// backend type and parameters
	Backend ChardevBackend `json:"backend"`
}

// Change a character device backend
//
// Returns: ChardevReturn.
//
// Since: 2.10
//
// .. qmp-example::    -> { "execute" : "chardev-change",
// "arguments" : { "id" : "baz",              "backend" : { "type" :
// "pty", "data" : {} } } }   <- { "return": { "pty" : "/dev/pty/42" }
// }  .. qmp-example::    -> {"execute" : "chardev-change",
// "arguments" : {       "id" : "charchannel2",       "backend" : {
// "type" : "socket",         "data" : {           "addr" : {
// "type" : "unix" ,             "data" : {               "path" :
// "/tmp/charchannel2.socket"             }            },
// "server" : true,            "wait" : false }}}}   <- {"return": {}}
type ChardevChangeCommand struct {
	// the chardev's ID, must exist
	Id string `json:"id"`
	// new backend type and parameters
	Backend ChardevBackend `json:"backend"`
}

// Remove a character device backend
//
// Since: 1.4
//
// .. qmp-example::    -> { "execute": "chardev-remove", "arguments":
// { "id" : "foo" } }   <- { "return": {} }
type ChardevRemoveCommand struct {
	// the chardev's ID, must exist and not be in use
	Id string `json:"id"`
}

// Send a break to a character device
//
// Since: 2.10
//
// .. qmp-example::    -> { "execute": "chardev-send-break",
// "arguments": { "id" : "foo" } }   <- { "return": {} }
type ChardevSendBreakCommand struct {
	// the chardev's ID, must exist
	Id string `json:"id"`
}

// Set migration information for remote display.  This makes the
// server ask the client to automatically reconnect using the new
// parameters once migration finished successfully.  Only implemented
// for SPICE.
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "client_migrate_info",
// "arguments": { "protocol": "spice",             "hostname":
// "virt42.lab.kraxel.org",             "port": 1234 } }   <- {
// "return": {} }
type ClientMigrateInfoCommand struct {
	// must be "spice"
	Protocol string `json:"protocol"`
	// migration target hostname
	Hostname string `json:"hostname"`
	// spice tcp port for plaintext channels
	Port *int64 `json:"port,omitempty"`
	// spice tcp port for tls-secured channels
	TlsPort *int64 `json:"tls-port,omitempty"`
	// server certificate subject
	CertSubject *string `json:"cert-subject,omitempty"`
}

// Close a file descriptor previously passed via SCM rights
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "closefd", "arguments": {
// "fdname": "fd1" } }   <- { "return": {} }
type ClosefdCommand struct {
	// file descriptor name
	Fdname string `json:"fdname"`
}

// Resume guest VM execution.
//
// Since: 0.14
//
// .. note:: This command will succeed if the guest is currently
// running. It will also succeed if the guest is in the "inmigrate"
// state; in this case, the effect of the command is to make sure
// the guest starts once migration finishes, removing the effect of
// the "-S" command line option if it was passed.    If the VM was
// previously suspended, and not been reset or woken,   this command
// will transition back to the "suspended" state.   (Since 9.0)  ..
// qmp-example::    -> { "execute": "cont" }   <- { "return": {} }
type ContCommand struct {
}

// Initiate adding dynamic capacity extents to a host.  This simulates
// operations defined in Compute Express Link (CXL) Specification,
// Revision 3.1, Section 7.6.7.6.5.  Note that, currently,
// establishing success or failure of the full Add Dynamic Capacity
// flow requires out of band communication with the OS of the CXL
// host.
//
// Since: 9.1
type CxlAddDynamicCapacityCommand struct {
	// path to the CXL Dynamic Capacity Device in the QOM tree.
	Path string `json:"path"`
	// The "Host ID" field as defined in Compute Express Link (CXL)
	// Specification, Revision 3.1, Table 7-70.
	HostId uint16 `json:"host-id"`
	// The "Selection Policy" bits as defined in Compute Express Link
	// (CXL) Specification, Revision 3.1, Table 7-70. It specifies the
	// policy to use for selecting which extents comprise the added
	// capacity.
	SelectionPolicy CxlExtentSelectionPolicy `json:"selection-policy"`
	// The "Region Number" field as defined in Compute Express Link
	// (CXL) Specification, Revision 3.1, Table 7-70. Valid range is
	// from 0-7.
	Region uint8 `json:"region"`
	// The "Tag" field as defined in Compute Express Link (CXL)
	// Specification, Revision 3.1, Table 7-70.
	Tag *string `json:"tag,omitempty"`
	// The "Extent List" field as defined in Compute Express Link
	// (CXL) Specification, Revision 3.1, Table 7-70.
	Extents []CxlDynamicCapacityExtent `json:"extents"`
}

// Command to inject a single correctable error.  Multiple error
// injection of this error type is not interesting as there is no
// associated header log.  These errors are reported via AER as a
// correctable internal error, with additional detail available from
// the CXL device.
//
// Since: 8.0
type CxlInjectCorrectableErrorCommand struct {
	// CXL Type 3 device canonical QOM path
	Path string `json:"path"`
	// Type of error.
	Type CxlCorErrorType `json:"type"`
}

// Inject an event record for a DRAM Event (CXL r3.0 8.2.9.2.1.2).
// This event type is reported via one of the event logs specified via
// the log parameter.
//
// Since: 8.1
type CxlInjectDramEventCommand struct {
	// CXL type 3 device canonical QOM path
	Path string `json:"path"`
	// Event log to add the event to
	Log CxlEventLog `json:"log"`
	// Event Record Flags. See CXL r3.0 Table 8-42 Common Event Record
	// Format, Event Record Flags for subfield definitions.
	Flags uint8 `json:"flags"`
	// Device Physical Address (relative to @path device). Note lower
	// bits include some flags. See CXL r3.0 Table 8-44 DRAM Event
	// Record, Physical Address.
	Dpa uint64 `json:"dpa"`
	// Memory Event Descriptor with additional memory event
	// information. See CXL r3.0 Table 8-44 DRAM Event Record, Memory
	// Event Descriptor for bit definitions.
	Descriptor uint8 `json:"descriptor"`
	// Type of memory event that occurred. See CXL r3.0 Table 8-44
	// DRAM Event Record, Memory Event Type for possible values.
	Type uint8 `json:"type"`
	// Type of first transaction that caused the event to occur. See
	// CXL r3.0 Table 8-44 DRAM Event Record, Transaction Type for
	// possible values.
	TransactionType uint8 `json:"transaction-type"`
	// The channel of the memory event location. A channel is an
	// interface that can be independently accessed for a transaction.
	Channel *uint8 `json:"channel,omitempty"`
	// The rank of the memory event location. A rank is a set of
	// memory devices on a channel that together execute a
	// transaction.
	Rank *uint8 `json:"rank,omitempty"`
	// Identifies one or more nibbles that the error affects
	NibbleMask *uint32 `json:"nibble-mask,omitempty"`
	// Bank group of the memory event location, incorporating a number
	// of Banks.
	BankGroup *uint8 `json:"bank-group,omitempty"`
	// Bank of the memory event location. A single bank is accessed
	// per read or write of the memory.
	Bank *uint8 `json:"bank,omitempty"`
	// Row address within the DRAM.
	Row *uint32 `json:"row,omitempty"`
	// Column address within the DRAM.
	Column *uint16 `json:"column,omitempty"`
	// Bits within each nibble. Used in order of bits set in the
	// nibble-mask. Up to 4 nibbles may be covered.
	CorrectionMask []uint64 `json:"correction-mask,omitempty"`
}

// Inject an event record for a General Media Event (CXL r3.0
// 8.2.9.2.1.1).  This event type is reported via one of the event
// logs specified via the log parameter.
//
// Since: 8.1
type CxlInjectGeneralMediaEventCommand struct {
	// CXL type 3 device canonical QOM path
	Path string `json:"path"`
	// event log to add the event to
	Log CxlEventLog `json:"log"`
	// Event Record Flags. See CXL r3.0 Table 8-42 Common Event Record
	// Format, Event Record Flags for subfield definitions.
	Flags uint8 `json:"flags"`
	// Device Physical Address (relative to @path device). Note lower
	// bits include some flags. See CXL r3.0 Table 8-43 General Media
	// Event Record, Physical Address.
	Dpa uint64 `json:"dpa"`
	// Memory Event Descriptor with additional memory event
	// information. See CXL r3.0 Table 8-43 General Media Event
	// Record, Memory Event Descriptor for bit definitions.
	Descriptor uint8 `json:"descriptor"`
	// Type of memory event that occurred. See CXL r3.0 Table 8-43
	// General Media Event Record, Memory Event Type for possible
	// values.
	Type uint8 `json:"type"`
	// Type of first transaction that caused the event to occur. See
	// CXL r3.0 Table 8-43 General Media Event Record, Transaction
	// Type for possible values.
	TransactionType uint8 `json:"transaction-type"`
	// The channel of the memory event location. A channel is an
	// interface that can be independently accessed for a transaction.
	Channel *uint8 `json:"channel,omitempty"`
	// The rank of the memory event location. A rank is a set of
	// memory devices on a channel that together execute a
	// transaction.
	Rank *uint8 `json:"rank,omitempty"`
	// Bitmask that represents all devices in the rank associated with
	// the memory event location.
	Device *uint32 `json:"device,omitempty"`
	// Device specific component identifier for the event. May
	// describe a field replaceable sub-component of the device.
	ComponentId *string `json:"component-id,omitempty"`
}

// Inject an event record for a Memory Module Event (CXL r3.0
// 8.2.9.2.1.3).  This event includes a copy of the Device Health info
// at the time of the event.
//
// Since: 8.1
type CxlInjectMemoryModuleEventCommand struct {
	// CXL type 3 device canonical QOM path
	Path string `json:"path"`
	// Event Log to add the event to
	Log CxlEventLog `json:"log"`
	// Event Record Flags. See CXL r3.0 Table 8-42 Common Event Record
	// Format, Event Record Flags for subfield definitions.
	Flags uint8 `json:"flags"`
	// Device Event Type. See CXL r3.0 Table 8-45 Memory Module Event
	// Record for bit definitions for bit definiions.
	Type uint8 `json:"type"`
	// Overall health summary bitmap. See CXL r3.0 Table 8-100 Get
	// Health Info Output Payload, Health Status for bit definitions.
	HealthStatus uint8 `json:"health-status"`
	// Overall media health summary. See CXL r3.0 Table 8-100 Get
	// Health Info Output Payload, Media Status for bit definitions.
	MediaStatus uint8 `json:"media-status"`
	// See CXL r3.0 Table 8-100 Get Health Info Output Payload,
	// Additional Status for subfield definitions.
	AdditionalStatus uint8 `json:"additional-status"`
	// Percentage (0-100) of factory expected life span.
	LifeUsed uint8 `json:"life-used"`
	// Device temperature in degrees Celsius.
	Temperature int16 `json:"temperature"`
	// Number of times the device has been unable to determine whether
	// data loss may have occurred.
	DirtyShutdownCount uint32 `json:"dirty-shutdown-count"`
	// Total number of correctable errors in volatile memory.
	CorrectedVolatileErrorCount uint32 `json:"corrected-volatile-error-count"`
	// Total number of correctable errors in persistent memory
	CorrectedPersistentErrorCount uint32 `json:"corrected-persistent-error-count"`
}

// Poison records indicate that a CXL memory device knows that a
// particular memory region may be corrupted.  This may be because of
// locally detected errors (e.g. ECC failure) or poisoned writes
// received from other components in the system.  This injection
// mechanism enables testing of the OS handling of poison records
// which may be queried via the CXL mailbox.
//
// Since: 8.1
type CxlInjectPoisonCommand struct {
	// CXL type 3 device canonical QOM path
	Path string `json:"path"`
	// Start address; must be 64 byte aligned.
	Start uint64 `json:"start"`
	// Length of poison to inject; must be a multiple of 64 bytes.
	Length uint64 `json:"length"`
}

// Command to allow injection of multiple errors in one go.  This
// allows testing of multiple header log handling in the OS.
//
// Since: 8.0
type CxlInjectUncorrectableErrorsCommand struct {
	// CXL Type 3 device canonical QOM path
	Path string `json:"path"`
	// Errors to inject
	Errors []CXLUncorErrorRecord `json:"errors"`
}

// Initiate release of dynamic capacity extents from a host.  This
// simulates operations defined in Compute Express Link (CXL)
// Specification, Revision 3.1, Section 7.6.7.6.6.  Note that,
// currently, success or failure of the full Release Dynamic Capacity
// flow requires out of band communication with the OS of the CXL
// host.
//
// Since: 9.1
type CxlReleaseDynamicCapacityCommand struct {
	// path to the CXL Dynamic Capacity Device in the QOM tree.
	Path string `json:"path"`
	// The "Host ID" field as defined in Compute Express Link (CXL)
	// Specification, Revision 3.1, Table 7-71.
	HostId uint16 `json:"host-id"`
	// Bit[3:0] of the "Flags" field as defined in Compute Express
	// Link (CXL) Specification, Revision 3.1, Table 7-71.
	RemovalPolicy CxlExtentRemovalPolicy `json:"removal-policy"`
	// Bit[4] of the "Flags" field in Compute Express Link (CXL)
	// Specification, Revision 3.1, Table 7-71. When set, the device
	// does not wait for a Release Dynamic Capacity command from the
	// host. Instead, the host immediately looses access to the
	// released capacity.
	ForcedRemoval *bool `json:"forced-removal,omitempty"`
	// Bit[5] of the "Flags" field in Compute Express Link (CXL)
	// Specification, Revision 3.1, Table 7-71. When set, the device
	// should sanitize all released capacity as a result of this
	// request. This ensures that all user data and metadata is made
	// permanently unavailable by whatever means is appropriate for
	// the media type. Note that changing encryption keys is not
	// sufficient.
	SanitizeOnRelease *bool `json:"sanitize-on-release,omitempty"`
	// The "Region Number" field as defined in Compute Express Link
	// Specification, Revision 3.1, Table 7-71. Valid range is from
	// 0-7.
	Region uint8 `json:"region"`
	// The "Tag" field as defined in Compute Express Link (CXL)
	// Specification, Revision 3.1, Table 7-71.
	Tag *string `json:"tag,omitempty"`
	// The "Extent List" field as defined in Compute Express Link
	// (CXL) Specification, Revision 3.1, Table 7-71.
	Extents []CxlDynamicCapacityExtent `json:"extents"`
}

// List properties associated with a device.
//
// Returns: a list of ObjectPropertyInfo describing a devices
// properties
//
// .. note:: Objects can create properties at runtime, for example to
// describe links between different devices and/or objects. These
// properties are not included in the output of this command.
//
// Since: 1.2
type DeviceListPropertiesCommand struct {
	// the type name of a device
	Typename string `json:"typename"`
}

// Synchronize device configuration from host to guest part.  First,
// copy the configuration from the host part (backend) to the guest
// part (frontend).  Then notify guest software that device
// configuration changed.  The command may be used to notify the guest
// about block device capcity change.  Currently only vhost-user-blk
// device supports this.
//
// Since: 9.2
type DeviceSyncConfigCommand struct {
	// the device's ID or QOM path
	Id string `json:"id"`
}

// Add a device.
//
// .. admonition:: Notes    1. Additional arguments depend on the
// type.    2. For detailed information about this command, please
// refer to     the 'docs/qdev-device-use.txt' file.    3. It's
// possible to list device properties by running QEMU with     the
// "-device DEVICE,help" command-line argument, where     DEVICE is
// the device's name.  .. qmp-example::    -> { "execute":
// "device_add",      "arguments": { "driver": "e1000", "id": "net1",
// "bus": "pci.0",             "mac": "52:54:00:12:34:56" } }   <- {
// "return": {} }
//
// Since: 0.13
type DeviceAddCommand struct {
	// the name of the new device's driver
	Driver string `json:"driver"`
	// the device's parent bus (device tree path)
	Bus *string `json:"bus,omitempty"`
	// the device's ID, must be unique
	Id *string `json:"id,omitempty"`
}

// Remove a device from a guest
//
// Errors:   - If @id is not a valid device, DeviceNotFound
//
// .. note:: When this command completes, the device may not be
// removed   from the guest. Hot removal is an operation that requires
// guest   cooperation. This command merely requests that the guest
// begin   the hot removal process. Completion of the device removal
// process is signaled with a DEVICE_DELETED event. Guest reset   will
// automatically complete removal for all devices. If a   guest-side
// error in the hot removal process is detected, the   device will not
// be removed and a DEVICE_UNPLUG_GUEST_ERROR event   is sent. Some
// errors cannot be detected.
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "device_del",      "arguments":
// { "id": "net1" } }   <- { "return": {} }  .. qmp-example::    -> {
// "execute": "device_del",      "arguments": { "id":
// "/machine/peripheral-anon/device[0]" } }   <- { "return": {} }
type DeviceDelCommand struct {
	// the device's ID or QOM path
	Id string `json:"id"`
}

// Reload display configuration.
//
// Since: 6.0
//
// .. qmp-example::    -> { "execute": "display-reload",
// "arguments": { "type": "vnc", "tls-certs": true } }   <- {
// "return": {} }
type DisplayReloadCommand struct {
	DisplayReloadOptions
}

// Update display configuration.
//
// Since: 7.1
//
// .. qmp-example::    -> { "execute": "display-update",
// "arguments": { "type": "vnc", "addresses":             [ { "type":
// "inet", "host": "0.0.0.0",               "port": "5901" } ] } }
// <- { "return": {} }
type DisplayUpdateCommand struct {
	DisplayUpdateOptions
}

// Start a point-in-time copy of a block device to a new destination.
// The status of ongoing drive-backup operations can be checked with
// query-block-jobs where the BlockJobInfo.type field has the value
// 'backup'.  The operation can be stopped before it has completed
// using the block-job-cancel command.
//
// Errors:   - If @device is not a valid block device, GenericError
//
// Since: 1.6
//
// .. qmp-example::    -> { "execute": "drive-backup",
// "arguments": { "device": "drive0",             "sync": "full",
// "target": "backup.img" } }   <- { "return": {} }
type DriveBackupCommand struct {
	DriveBackup
}

// Start mirroring a block device's writes to a new destination.
// target specifies the target of the new image.  If the file exists,
// or if it is a device, it will be used as the new destination for
// writes.  If it does not exist, a new file will be created.  @format
// specifies the format of the mirror image, default is to probe if
// mode='existing', else the format of the source.
//
// Errors:   - If @device is not a valid block device, GenericError
//
// Since: 1.3
//
// .. qmp-example::    -> { "execute": "drive-mirror",
// "arguments": { "device": "ide-hd0",             "target":
// "/some/place/my-image",             "sync": "full",
// "format": "qcow2" } }   <- { "return": {} }
type DriveMirrorCommand struct {
	DriveMirror
}

// Dump guest's memory to vmcore.  It is a synchronous operation that
// can take very long depending on the amount of guest memory.
//
// .. note:: All boolean arguments default to false.
//
// Since: 1.2
//
// .. qmp-example::    -> { "execute": "dump-guest-memory",
// "arguments": { "paging": false, "protocol": "fd:dump" } }   <- {
// "return": {} }
type DumpGuestMemoryCommand struct {
	// if true, do paging to get guest's memory mapping. This allows
	// using gdb to process the core file. IMPORTANT: this option can
	// make QEMU allocate several gigabytes of RAM. This can happen
	// for a large guest, or a malicious guest pretending to be large.
	// Also, paging=true has the following limitations: 1. The guest
	// may be in a catastrophic state or can have corrupted memory,
	// which cannot be trusted 2. The guest can be in real-mode even
	// if paging is enabled. For example, the guest uses ACPI to
	// sleep, and ACPI sleep state goes in real-mode 3. Currently only
	// supported on i386 and x86_64.
	Paging bool `json:"paging"`
	// the filename or file descriptor of the vmcore. The supported
	// protocols are: 1. file: the protocol starts with "file:", and
	// the following string is the file's path. 2. fd: the protocol
	// starts with "fd:", and the following string is the fd's name.
	Protocol string `json:"protocol"`
	// if true, QMP will return immediately rather than waiting for
	// the dump to finish. The user can track progress using "query-
	// dump". (since 2.6).
	Detach *bool `json:"detach,omitempty"`
	// if specified, the starting physical address.
	Begin *int64 `json:"begin,omitempty"`
	// if specified, the memory size, in bytes. If you don't want to
	// dump all guest's memory, please specify the start @begin and
	// @length
	Length *int64 `json:"length,omitempty"`
	// if specified, the format of guest memory dump. But non-elf
	// format is conflict with paging and filter, ie. @paging, @begin
	// and @length is not allowed to be specified with non-elf @format
	// at the same time (since 2.0)
	Format *DumpGuestMemoryFormat `json:"format,omitempty"`
}

// Dump guest's storage keys
//
// Since: 2.5
//
// .. qmp-example::    -> { "execute": "dump-skeys",      "arguments":
// { "filename": "/tmp/skeys" } }   <- { "return": {} }
type DumpSkeysCommand struct {
	// the path to the file to dump to
	Filename string `json:"filename"`
}

// Save the FDT in dtb format.
//
// Since: 7.2
//
// .. qmp-example::    -> { "execute": "dumpdtb" }      "arguments": {
// "filename": "fdt.dtb" } }   <- { "return": {} }
type DumpdtbCommand struct {
	// name of the dtb file to be created
	Filename string `json:"filename"`
}

// Ejects the medium from a removable drive.
//
// Errors:   - If @device is not a valid block device, DeviceNotFound
//
// .. note:: Ejecting a device with no media results in success.
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "eject", "arguments": { "id":
// "ide1-0-1" } }   <- { "return": {} }
type EjectCommand struct {
	// Block device name
	Device *string `json:"device,omitempty"`
	// The name or QOM path of the guest device (since: 2.8)
	Id *string `json:"id,omitempty"`
	// If true, eject regardless of whether the drive is locked. If
	// not specified, the default value is false.
	Force *bool `json:"force,omitempty"`
}

// Expire the password of a remote display server.
//
// Errors:   - If @protocol is 'spice' and Spice is not active,
// DeviceNotFound
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "expire_password", "arguments":
// { "protocol": "vnc",                            "time": "+60" } }
// <- { "return": {} }
type ExpirePasswordCommand struct {
	ExpirePasswordOptions
}

// Add a socket that was duplicated to QEMU process with
// WSADuplicateSocketW() via WSASocket() & WSAPROTOCOL_INFOW structure
// and assign it a name (the SOCKET is associated with a CRT file
// descriptor)
//
// Since: 8.0
//
// .. note:: If @fdname already exists, the file descriptor assigned
// to   it will be closed and replaced by the received file
// descriptor.    The 'closefd' command can be used to explicitly
// close the file   descriptor when it is no longer needed.  .. qmp-
// example::    -> { "execute": "get-win32-socket",      "arguments":
// { "info": "abcd123..", "fdname": "skclient" } }   <- { "return": {}
// }
type GetWin32SocketCommand struct {
	// the WSAPROTOCOL_INFOW structure (encoded in base64)
	Info string `json:"info"`
	// file descriptor name
	Fdname string `json:"fdname"`
}

// Receive a file descriptor via SCM rights and assign it a name
//
// Since: 0.14
//
// .. note:: If @fdname already exists, the file descriptor assigned
// to   it will be closed and replaced by the received file
// descriptor.    The 'closefd' command can be used to explicitly
// close the file   descriptor when it is no longer needed.  .. qmp-
// example::    -> { "execute": "getfd", "arguments": { "fdname":
// "fd1" } }   <- { "return": {} }
type GetfdCommand struct {
	// file descriptor name
	Fdname string `json:"fdname"`
}

// Execute a command on the human monitor and return the output.
//
// Returns: the output of the command as a string
//
// Since: 0.14
//
// .. note:: This command only exists as a stop-gap. Its use is highly
// discouraged. The semantics of this command are not guaranteed:
// this means that command names, arguments and responses can change
// or be removed at ANY time. Applications that rely on long term
// stability guarantees should NOT use this command.    Known
// limitations:    * This command is stateless, this means that
// commands that depend    on state information (such as getfd) might
// not work.    * Commands that prompt the user for data don't
// currently work.  .. qmp-example::    -> { "execute": "human-
// monitor-command",      "arguments": { "command-line": "info kvm" }
// }   <- { "return": "kvm support: enabled\r\n" }
type HumanMonitorCommandCommand struct {
	// the command to execute in the human monitor
	CommandLine string `json:"command-line"`
	// The CPU to use for commands that require an implicit CPU
	CpuIndex *int64 `json:"cpu-index,omitempty"`
}

// Injects a Non-Maskable Interrupt into the default CPU (x86/s390) or
// all CPUs (ppc64).  The command fails when the guest doesn't support
// injecting.
//
// Since: 0.14
//
// .. note:: Prior to 2.1, this command was only supported for x86 and
// s390 VMs.  .. qmp-example::    -> { "execute": "inject-nmi" }   <-
// { "return": {} }
type InjectNmiCommand struct {
}

// Send input event(s) to guest.  The @device and @head parameters can
// be used to send the input event to specific input devices in case
// (a) multiple input devices of the same kind are added to the
// virtual machine and (b) you have configured input routing (see
// docs/multiseat.txt) for those input devices.  The parameters work
// exactly like the device and head properties of input devices.  If
// @device is missing, only devices that have no input routing config
// are admissible.  If @device is specified, both input devices with
// and without input routing config are admissible, but devices with
// input routing config take precedence.
//
// Since: 2.6
//
// .. note:: The consoles are visible in the qom tree, under
// "/backend/console[$index]". They have a device link and head
// property, so it is possible to map which console belongs to which
// device and display.  .. qmp-example::   :title: Press left mouse
// button    -> { "execute": "input-send-event",     "arguments": {
// "device": "video0",             "events": [ { "type": "btn",
// "data" : { "down": true, "button": "left" } } ] } }   <- {
// "return": {} }    -> { "execute": "input-send-event",
// "arguments": { "device": "video0",             "events": [ {
// "type": "btn",             "data" : { "down": false, "button":
// "left" } } ] } }   <- { "return": {} }  .. qmp-example::   :title:
// Press ctrl-alt-del    -> { "execute": "input-send-event",
// "arguments": { "events": [       { "type": "key", "data" : {
// "down": true,        "key": {"type": "qcode", "data": "ctrl" } } },
// { "type": "key", "data" : { "down": true,        "key": {"type":
// "qcode", "data": "alt" } } },       { "type": "key", "data" : {
// "down": true,        "key": {"type": "qcode", "data": "delete" } }
// } ] } }   <- { "return": {} }  .. qmp-example::   :title: Move
// mouse pointer to absolute coordinates    -> { "execute": "input-
// send-event" ,    "arguments": { "events": [           { "type":
// "abs", "data" : { "axis": "x", "value" : 20000 } },           {
// "type": "abs", "data" : { "axis": "y", "value" : 400 } } ] } }   <-
// { "return": {} }
type InputSendEventCommand struct {
	// display device to send event(s) to.
	Device *string `json:"device,omitempty"`
	// head to send event(s) to, in case the display device supports
	// multiple scanouts.
	Head *int64 `json:"head,omitempty"`
	// List of InputEvent union.
	Events []InputEvent `json:"events"`
}

// Instruct an active background job to cancel at the next
// opportunity. This command returns immediately after marking the
// active job for cancellation.  The job will cancel as soon as
// possible and then emit a JOB_STATUS_CHANGE event.  Usually, the
// status will change to ABORTING, but it is possible that a job
// successfully completes (e.g. because it was almost done and there
// was no opportunity to cancel earlier than completing the job) and
// transitions to PENDING instead.
//
// Since: 3.0
type JobCancelCommand struct {
	// The job identifier.
	Id string `json:"id"`
}

// Manually trigger completion of an active job in the READY state.
//
// Since: 3.0
type JobCompleteCommand struct {
	// The job identifier.
	Id string `json:"id"`
}

// Deletes a job that is in the CONCLUDED state.  This command only
// needs to be run explicitly for jobs that don't have automatic
// dismiss enabled.  This command will refuse to operate on any job
// that has not yet reached its terminal state, JOB_STATUS_CONCLUDED.
// For jobs that make use of JOB_READY event, job-cancel or job-
// complete will still need to be used as appropriate.
//
// Since: 3.0
type JobDismissCommand struct {
	// The job identifier.
	Id string `json:"id"`
}

// Instructs all jobs in a transaction (or a single job if it is not
// part of any transaction) to finalize any graph changes and do any
// necessary cleanup.  This command requires that all involved jobs
// are in the PENDING state.  For jobs in a transaction, instructing
// one job to finalize will force ALL jobs in the transaction to
// finalize, so it is only necessary to instruct a single member job
// to finalize.
//
// Since: 3.0
type JobFinalizeCommand struct {
	// The identifier of any job in the transaction, or of a job that
	// is not part of any transaction.
	Id string `json:"id"`
}

// Pause an active job.  This command returns immediately after
// marking the active job for pausing.  Pausing an already paused job
// is an error.  The job will pause as soon as possible, which means
// transitioning into the PAUSED state if it was RUNNING, or into
// STANDBY if it was READY.  The corresponding JOB_STATUS_CHANGE event
// will be emitted.  Cancelling a paused job automatically resumes it.
//
// Since: 3.0
type JobPauseCommand struct {
	// The job identifier.
	Id string `json:"id"`
}

// Resume a paused job.  This command returns immediately after
// resuming a paused job. Resuming an already running job is an error.
//
// Since: 3.0
type JobResumeCommand struct {
	// The job identifier.
	Id string `json:"id"`
}

// Save a portion of guest memory to a file.
//
// Since: 0.14
//
// .. caution:: Errors were not reliably returned until 1.1.  .. qmp-
// example::    -> { "execute": "memsave",      "arguments": { "val":
// 10,             "size": 100,             "filename": "/tmp/virtual-
// mem-dump" } }   <- { "return": {} }
type MemsaveCommand struct {
	// the virtual address of the guest to start from
	Val uint64 `json:"val"`
	// the size of memory region to save
	Size uint64 `json:"size"`
	// the file to save the memory to as binary data
	Filename string `json:"filename"`
	// the index of the virtual CPU to use for translating the virtual
	// address (defaults to CPU 0)
	CpuIndex *int64 `json:"cpu-index,omitempty"`
}

// Migrates the current running guest to another Virtual Machine.
//
// Since: 0.14
//
// .. admonition:: Notes    1. The 'query-migrate' command should be
// used to check     migration's progress and final result (this
// information is     provided by the 'status' member).    2. All
// boolean arguments default to false.    3. The user Monitor's
// "detach" argument is invalid in QMP and     should not be used.
// 4. The uri argument should have the Uniform Resource Identifier
// of default destination VM. This connection will be bound to
// default network.    5. For now, number of migration streams is
// restricted to one,     i.e. number of items in 'channels' list is
// just 1.    6. The 'uri' and 'channels' arguments are mutually
// exclusive;     exactly one of the two should be present.  .. qmp-
// example::    -> { "execute": "migrate", "arguments": { "uri":
// "tcp:0:4446" } }   <- { "return": {} }    -> { "execute":
// "migrate",      "arguments": {        "channels": [ { "channel-
// type": "main",                "addr": { "transport": "socket",
// "type": "inet",                     "host": "10.12.34.9",
// "port": "1050" } } ] } }   <- { "return": {} }    -> { "execute":
// "migrate",      "arguments": {        "channels": [ { "channel-
// type": "main",                "addr": { "transport": "exec",
// "args": [ "/bin/nc", "-p", "6000",
// "/some/sock" ] } } ] } }   <- { "return": {} }    -> { "execute":
// "migrate",      "arguments": {        "channels": [ { "channel-
// type": "main",                "addr": { "transport": "rdma",
// "host": "10.12.34.9",                     "port": "1050" } } ] } }
// <- { "return": {} }    -> { "execute": "migrate",      "arguments":
// {        "channels": [ { "channel-type": "main",
// "addr": { "transport": "file",                     "filename":
// "/tmp/migfile",                     "offset": "0x1000" } } ] } }
// <- { "return": {} }
type MigrateCommand struct {
	// the Uniform Resource Identifier of the destination VM
	Uri *string `json:"uri,omitempty"`
	// list of migration stream channels with each stream in the list
	// connected to a destination interface endpoint.
	Channels []MigrationChannel `json:"channels,omitempty"`
	// this argument exists only for compatibility reasons and is
	// ignored by QEMU
	Detach *bool `json:"detach,omitempty"`
	// resume one paused migration, default "off". (since 3.0)
	Resume *bool `json:"resume,omitempty"`
}

// Continue migration when it's in a paused state.
//
// Since: 2.11
//
// .. qmp-example::    -> { "execute": "migrate-continue" ,
// "arguments":      { "state": "pre-switchover" } }   <- { "return":
// {} }
type MigrateContinueCommand struct {
	// The state the migration is currently expected to be in
	State MigrationStatus `json:"state"`
}

// Start an incoming migration, the qemu must have been started with
// -incoming defer
//
// Since: 2.3
//
// .. admonition:: Notes    1. It's a bad idea to use a string for the
// uri, but it needs to     stay compatible with -incoming and the
// format of the uri is     already exposed above libvirt.    2. QEMU
// must be started with -incoming defer to allow     migrate-incoming
// to be used.    3. The uri format is the same as for -incoming    4.
// For now, number of migration streams is restricted to one,     i.e.
// number of items in 'channels' list is just 1.    5. The 'uri' and
// 'channels' arguments are mutually exclusive;     exactly one of the
// two should be present.  .. qmp-example::    -> { "execute":
// "migrate-incoming",      "arguments": { "uri": "tcp:0:4446" } }
// <- { "return": {} }    -> { "execute": "migrate-incoming",
// "arguments": {        "channels": [ { "channel-type": "main",
// "addr": { "transport": "socket",                     "type":
// "inet",                     "host": "10.12.34.9",
// "port": "1050" } } ] } }   <- { "return": {} }    -> { "execute":
// "migrate-incoming",      "arguments": {        "channels": [ {
// "channel-type": "main",                "addr": { "transport":
// "exec",                     "args": [ "/bin/nc", "-p", "6000",
// "/some/sock" ] } } ] } }   <- { "return": {} }    -> { "execute":
// "migrate-incoming",      "arguments": {        "channels": [ {
// "channel-type": "main",                "addr": { "transport":
// "rdma",                     "host": "10.12.34.9",
// "port": "1050" } } ] } }   <- { "return": {} }
type MigrateIncomingCommand struct {
	// The Uniform Resource Identifier identifying the source or
	// address to listen on
	Uri *string `json:"uri,omitempty"`
	// list of migration stream channels with each stream in the list
	// connected to a destination interface endpoint.
	Channels []MigrationChannel `json:"channels,omitempty"`
	// Exit on incoming migration failure. Default true. When set to
	// false, the failure triggers a MIGRATION event, and error
	// details could be retrieved with query-migrate. (since 9.1)
	ExitOnError *bool `json:"exit-on-error,omitempty"`
}

// Pause a migration.  Currently it only supports postcopy.  .. qmp-
// example::      -> { "execute": "migrate-pause" }     <- { "return":
// {} }
//
// Since: 3.0
type MigratePauseCommand struct {
}

// Provide a recovery migration stream URI.
//
// .. qmp-example::    -> { "execute": "migrate-recover",
// "arguments": { "uri": "tcp:192.168.1.200:12345" } }   <- {
// "return": {} }
//
// Since: 3.0
type MigrateRecoverCommand struct {
	// the URI to be used for the recovery of migration stream.
	Uri string `json:"uri"`
}

// Enable/Disable the following migration capabilities (like xbzrle)
//
// Since: 1.2
//
// .. qmp-example::    -> { "execute": "migrate-set-capabilities" ,
// "arguments":      { "capabilities": [ { "capability": "xbzrle",
// "state": true } ] } }   <- { "return": {} }
type MigrateSetCapabilitiesCommand struct {
	// json array of capability modifications to make
	Capabilities []MigrationCapabilityStatus `json:"capabilities"`
}

// Set various migration parameters.
//
// Since: 2.4
//
// .. qmp-example::    -> { "execute": "migrate-set-parameters" ,
// "arguments": { "multifd-channels": 5 } }   <- { "return": {} }
type MigrateSetParametersCommand struct {
	MigrateSetParameters
}

// Followup to a migration command to switch the migration to postcopy
// mode.  The postcopy-ram capability must be set on both source and
// destination before the original migration command.
//
// Since: 2.5
//
// .. qmp-example::    -> { "execute": "migrate-start-postcopy" }   <-
// { "return": {} }
type MigrateStartPostcopyCommand struct {
}

// Cancel the current executing migration process.  .. note:: This
// command succeeds even if there is no migration    process running.
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "migrate_cancel" }   <- {
// "return": {} }
type MigrateCancelCommand struct {
}

// Export a block node to QEMU's embedded NBD server.  The export name
// will be used as the id for the resulting block export.
//
// Errors:   - if the server is not running   - if an export with the
// same name already exists
//
// Since: 1.3
type NbdServerAddCommand struct {
	NbdServerAddOptions
}

// Remove NBD export by name.
//
// Errors:   - if the server is not running   - if export is not found
// - if mode is 'safe' and there are existing connections
//
// Since: 2.12
type NbdServerRemoveCommand struct {
	// Block export id.
	Name string `json:"name"`
	// Mode of command operation. See @BlockExportRemoveMode
	// description. Default is 'safe'.
	Mode *BlockExportRemoveMode `json:"mode,omitempty"`
}

// Start an NBD server listening on the given host and port.  Block
// devices can then be exported using @nbd-server-add.  The NBD server
// will present them as named exports; for example, another QEMU
// instance could refer to them as "nbd:HOST:PORT:exportname=NAME".
// Keep this type consistent with the NbdServerOptions type.  The only
// intended difference is using SocketAddressLegacy instead of
// SocketAddress.
//
// Errors:   - if the server is already running
//
// Since: 1.3
type NbdServerStartCommand struct {
	// Address on which to listen.
	Addr SocketAddressLegacy `json:"addr"`
	// Time limit, in seconds, at which a client that has not
	// completed the negotiation handshake will be disconnected, or 0
	// for no limit (since 10.0; default: 10).
	HandshakeMaxSeconds *uint32 `json:"handshake-max-seconds,omitempty"`
	// ID of the TLS credentials object (since 2.6).
	TlsCreds *string `json:"tls-creds,omitempty"`
	// ID of the QAuthZ authorization object used to validate the
	// client's x509 distinguished name. This object is is only
	// resolved at time of use, so can be deleted and recreated on the
	// fly while the NBD server is active. If missing, it will default
	// to denying access (since 4.0).
	TlsAuthz *string `json:"tls-authz,omitempty"`
	// The maximum number of connections to allow at the same time, 0
	// for unlimited. Setting this to 1 also stops the server from
	// advertising multiple client support (since 5.2; default: 100).
	MaxConnections *uint32 `json:"max-connections,omitempty"`
}

// Stop QEMU's embedded NBD server, and unregister all devices
// previously added via @nbd-server-add.
//
// Since: 1.3
type NbdServerStopCommand struct {
}

// Add a network backend.  Additional arguments depend on the type.
//
// Since: 0.14
//
// Errors:   - If @type is not a valid network backend, DeviceNotFound
//
// .. qmp-example::    -> { "execute": "netdev_add",      "arguments":
// { "type": "user", "id": "netdev1",             "dnssearch": [ {
// "str": "example.org" } ] } }   <- { "return": {} }
type NetdevAddCommand struct {
	Netdev
}

// Remove a network backend.
//
// Errors:   - If @id is not a valid network backend, DeviceNotFound
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "netdev_del", "arguments": {
// "id": "netdev1" } }   <- { "return": {} }
type NetdevDelCommand struct {
	// the name of the network backend to remove
	Id string `json:"id"`
}

// Create a QOM object.
//
// Errors:   - Error if @qom-type is not a valid class name
//
// Since: 2.0
//
// .. qmp-example::    -> { "execute": "object-add",      "arguments":
// { "qom-type": "rng-random", "id": "rng1",             "filename":
// "/dev/hwrng" } }   <- { "return": {} }
type ObjectAddCommand struct {
	ObjectOptions
}

// Remove a QOM object.
//
// Errors:   - Error if @id is not a valid id for a QOM object
//
// Since: 2.0
//
// .. qmp-example::    -> { "execute": "object-del", "arguments": {
// "id": "rng1" } }   <- { "return": {} }
type ObjectDelCommand struct {
	// the name of the QOM object to remove
	Id string `json:"id"`
}

// Save a portion of guest physical memory to a file.
//
// Since: 0.14
//
// .. caution:: Errors were not reliably returned until 1.1.  .. qmp-
// example::    -> { "execute": "pmemsave",      "arguments": { "val":
// 10,             "size": 100,             "filename":
// "/tmp/physical-mem-dump" } }   <- { "return": {} }
type PmemsaveCommand struct {
	// the physical address of the guest to start from
	Val uint64 `json:"val"`
	// the size of memory region to save
	Size uint64 `json:"size"`
	// the file to save the memory to as binary data
	Filename string `json:"filename"`
}

// Enable QMP capabilities.
//
// .. qmp-example::    -> { "execute": "qmp_capabilities",
// "arguments": { "enable": [ "oob" ] } }   <- { "return": {} }  ..
// note:: This command is valid exactly when first connecting: it
// must be issued before any other command will be accepted, and
// will fail once the monitor is accepting other commands. (see
// :doc:`/interop/qmp-spec`)  .. note:: The QMP client needs to
// explicitly enable QMP   capabilities, otherwise all the QMP
// capabilities will be turned   off by default.
//
// Since: 0.13
type QmpCapabilitiesCommand struct {
	// An optional list of QMPCapability values to enable. The client
	// must not enable any capability that is not mentioned in the QMP
	// greeting message. If the field is not provided, it means no QMP
	// capabilities will be enabled. (since 2.12)
	Enable []QMPCapability `json:"enable,omitempty"`
}

// This command will get a property from a object model path and
// return the value.
//
// Returns: The property value. The type depends on the property type.
// child<> and link<> properties are returned as #str pathnames.   All
// integer property types (u8, u16, etc) are returned as #int.
//
// Since: 1.2
//
// .. qmp-example::   :title: Use absolute path    -> { "execute":
// "qom-get",      "arguments": { "path":
// "/machine/unattached/device[0]",             "property":
// "hotplugged" } }   <- { "return": false }  .. qmp-example::
// :title: Use partial path    -> { "execute": "qom-get",
// "arguments": { "path": "unattached/sysbus",             "property":
// "type" } }   <- { "return": "System" }
type QomGetCommand struct {
	// The path within the object model. There are two forms of
	// supported paths--absolute and partial paths. Absolute paths are
	// derived from the root object and can follow child<> or link<>
	// properties. Since they can follow link<> properties, they can
	// be arbitrarily long. Absolute paths look like absolute
	// filenames and are prefixed with a leading slash. Partial paths
	// look like relative filenames. They do not begin with a prefix.
	// The matching rules for partial paths are subtle but designed to
	// make specifying objects easy. At each level of the composition
	// tree, the partial path is matched as an absolute path. The
	// first match is not returned. At least two matches are searched
	// for. A successful result is only returned if only one match is
	// found. If more than one match is found, a flag is return to
	// indicate that the match was ambiguous.
	Path string `json:"path"`
	// The property name to read
	Property string `json:"property"`
}

// This command will list any properties of a object given a path in
// the object model.
//
// Returns: a list of @ObjectPropertyInfo that describe the properties
// of the object.
//
// Since: 1.2
//
// .. qmp-example::    -> { "execute": "qom-list",      "arguments": {
// "path": "/chardevs" } }   <- { "return": [ { "name": "type",
// "type": "string" },            { "name": "parallel0", "type":
// "child<chardev-vc>" },            { "name": "serial0", "type":
// "child<chardev-vc>" },            { "name": "mon0", "type":
// "child<chardev-stdio>" } ] }
type QomListCommand struct {
	// the path within the object model. See @qom-get for a
	// description of this parameter.
	Path string `json:"path"`
}

// List properties associated with a QOM object.
//
// .. note:: Objects can create properties at runtime, for example to
// describe links between different devices and/or objects. These
// properties are not included in the output of this command.
//
// Returns: a list of ObjectPropertyInfo describing object properties
//
// Since: 2.12
type QomListPropertiesCommand struct {
	// the type name of an object
	Typename string `json:"typename"`
}

// This command will return a list of types given search parameters
//
// Returns: a list of @ObjectTypeInfo or an empty list if no results
// are found
//
// Since: 1.1
type QomListTypesCommand struct {
	// if specified, only return types that implement this type name
	Implements *string `json:"implements,omitempty"`
	// if true, include abstract types in the results
	Abstract *bool `json:"abstract,omitempty"`
}

// This command will set a property from a object model path.
//
// Since: 1.2
//
// .. qmp-example::    -> { "execute": "qom-set",      "arguments": {
// "path": "/machine",             "property": "graphics",
// "value": false } }   <- { "return": {} }
type QomSetCommand struct {
	// see @qom-get for a description of this parameter
	Path string `json:"path"`
	// the property name to set
	Property string `json:"property"`
	// a value who's type is appropriate for the property type. See
	// @qom-get for a description of type mapping.
	Value any `json:"value"`
}

// Return a list of ACPIOSTInfo for devices that support status
// reporting via ACPI _OST method.
//
// Since: 2.1
//
// .. qmp-example::    -> { "execute": "query-acpi-ospm-status" }   <-
// { "return": [ { "device": "d1", "slot": "0", "slot-type": "DIMM",
// "source": 1, "status": 0},            { "slot": "1", "slot-type":
// "DIMM", "source": 0, "status": 0},            { "slot": "2", "slot-
// type": "DIMM", "source": 0, "status": 0},            { "slot": "3",
// "slot-type": "DIMM", "source": 0, "status": 0}     ]}
type QueryAcpiOspmStatusCommand struct {
}

// Returns information about audiodev configuration
//
// Returns: array of @Audiodev
//
// Since: 8.0
type QueryAudiodevsCommand struct {
}

// Return information about the balloon device.
//
// Returns:   @BalloonInfo
//
// Errors:   - If the balloon driver is enabled but not functional
// because    the KVM kernel module cannot support it, KVMMissingCap
// - If no balloon device is present, DeviceNotActive
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "query-balloon" }   <- {
// "return": {        "actual": 1073741824      }     }
type QueryBalloonCommand struct {
}

// Get a list of BlockInfo for all virtual block devices.
//
// Returns: a list of @BlockInfo describing each virtual block device.
// Filter nodes that were created implicitly are skipped over.
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "query-block" }   <- {
// "return":[        {         "io-status": "ok",
// "device":"ide0-hd0",         "locked":false,
// "removable":false,         "inserted":{           "ro":false,
// "drv":"qcow2",           "encrypted":false,
// "file":"disks/test.qcow2",           "backing_file_depth":1,
// "bps":1000000,           "bps_rd":0,           "bps_wr":0,
// "iops":1000000,           "iops_rd":0,           "iops_wr":0,
// "bps_max": 8000000,           "bps_rd_max": 0,
// "bps_wr_max": 0,           "iops_max": 0,           "iops_rd_max":
// 0,           "iops_wr_max": 0,           "iops_size": 0,
// "detect_zeroes": "on",           "write_threshold": 0,
// "image":{            "filename":"disks/test.qcow2",
// "format":"qcow2",            "virtual-size":2048000,
// "backing_file":"base.qcow2",            "full-backing-
// filename":"disks/base.qcow2",            "backing-filename-
// format":"qcow2",            "snapshots":[              {
// "id": "1",               "name": "snapshot1",               "vm-
// state-size": 0,               "date-sec": 10000200,
// "date-nsec": 12,               "vm-clock-sec": 206,
// "vm-clock-nsec": 30              }            ],
// "backing-image":{              "filename":"disks/base.qcow2",
// "format":"qcow2",              "virtual-size":2048000            }
// }         },         "qdev": "ide_disk",         "type":"unknown"
// },        {         "io-status": "ok",         "device":"ide1-cd0",
// "locked":false,         "removable":true,         "qdev":
// "/machine/unattached/device[23]",         "tray_open": false,
// "type":"unknown"        },        {         "device":"floppy0",
// "locked":false,         "removable":true,         "qdev":
// "/machine/unattached/device[20]",         "type":"unknown"
// },        {         "device":"sd0",         "locked":false,
// "removable":true,         "type":"unknown"        }      ]     }
type QueryBlockCommand struct {
}

// Returns: A list of BlockExportInfo describing all block exports
//
// Since: 5.2
type QueryBlockExportsCommand struct {
}

// Return information about long-running block device operations.
//
// Returns: a list of @BlockJobInfo for each active block job
//
// Since: 1.1
type QueryBlockJobsCommand struct {
}

// Query the @BlockStats for all virtual block devices.
//
// Returns: A list of @BlockStats for each virtual block devices.
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "query-blockstats" }   <- {
// "return":[        {         "device":"ide0-hd0",         "parent":{
// "stats":{            "wr_highest_offset":3686448128,
// "wr_bytes":9786368,            "wr_operations":751,
// "rd_bytes":122567168,            "rd_operations":36772
// "wr_total_times_ns":313253456
// "rd_total_times_ns":3465673657
// "flush_total_times_ns":49653            "flush_operations":61,
// "rd_merged":0,            "wr_merged":0,
// "idle_time_ns":2953431879,            "account_invalid":true,
// "account_failed":false           }         },         "stats":{
// "wr_highest_offset":2821110784,           "wr_bytes":9786368,
// "wr_operations":692,           "rd_bytes":122739200,
// "rd_operations":36604           "flush_operations":51,
// "wr_total_times_ns":313253456
// "rd_total_times_ns":3465673657
// "flush_total_times_ns":49653,           "rd_merged":0,
// "wr_merged":0,           "idle_time_ns":2953431879,
// "account_invalid":true,           "account_failed":false         },
// "qdev": "/machine/unattached/device[23]"        },        {
// "device":"ide1-cd0",         "stats":{
// "wr_highest_offset":0,           "wr_bytes":0,
// "wr_operations":0,           "rd_bytes":0,
// "rd_operations":0           "flush_operations":0,
// "wr_total_times_ns":0           "rd_total_times_ns":0
// "flush_total_times_ns":0,           "rd_merged":0,
// "wr_merged":0,           "account_invalid":false,
// "account_failed":false         },         "qdev":
// "/machine/unattached/device[24]"        },        {
// "device":"floppy0",         "stats":{
// "wr_highest_offset":0,           "wr_bytes":0,
// "wr_operations":0,           "rd_bytes":0,
// "rd_operations":0           "flush_operations":0,
// "wr_total_times_ns":0           "rd_total_times_ns":0
// "flush_total_times_ns":0,           "rd_merged":0,
// "wr_merged":0,           "account_invalid":false,
// "account_failed":false         },         "qdev":
// "/machine/unattached/device[16]"        },        {
// "device":"sd0",         "stats":{           "wr_highest_offset":0,
// "wr_bytes":0,           "wr_operations":0,           "rd_bytes":0,
// "rd_operations":0           "flush_operations":0,
// "wr_total_times_ns":0           "rd_total_times_ns":0
// "flush_total_times_ns":0,           "rd_merged":0,
// "wr_merged":0,           "account_invalid":false,
// "account_failed":false         }        }      ]     }
type QueryBlockstatsCommand struct {
	// If true, the command will query all the block nodes that have a
	// node name, in a list which will include "parent" information,
	// but not "backing". If false or omitted, the behavior is as
	// before - query all the device backends, recursively including
	// their "parent" and "backing". Filter nodes that were created
	// implicitly are skipped over in this mode. (Since 2.3)
	QueryNodes *bool `json:"query-nodes,omitempty"`
}

// Returns information about current character devices.
//
// Returns: a list of @ChardevInfo
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "query-chardev" }   <- {
// "return": [        {         "label": "charchannel0",
// "filename":
// "unix:/var/lib/libvirt/qemu/seabios.rhel6.agent,server=on",
// "frontend-open": false        },        {         "label":
// "charmonitor",         "filename":
// "unix:/var/lib/libvirt/qemu/seabios.rhel6.monitor,server=on",
// "frontend-open": true        },        {         "label":
// "charserial0",         "filename": "pty:/dev/pts/2",
// "frontend-open": true        }      ]     }
type QueryChardevCommand struct {
}

// Returns information about character device backends.
//
// Returns: a list of @ChardevBackendInfo
//
// Since: 2.0
//
// .. qmp-example::    -> { "execute": "query-chardev-backends" }   <-
// {      "return":[        {         "name":"udp"        },        {
// "name":"tcp"        },        {         "name":"unix"        },
// {         "name":"spiceport"        }      ]     }
type QueryChardevBackendsCommand struct {
}

// Query COLO status while the vm is running.
//
// Returns: A @COLOStatus object showing the status.
//
// .. qmp-example::    -> { "execute": "query-colo-status" }   <- {
// "return": { "mode": "primary", "last-mode": "none", "reason":
// "request" } }
//
// Since: 3.1
type QueryColoStatusCommand struct {
}

// Query command line option schema.
//
// Returns: list of @CommandLineOptionInfo for all options (or for the
// given @option).
//
// Errors:   - if the given @option doesn't exist
//
// Since: 1.5
//
// .. qmp-example::    -> { "execute": "query-command-line-options",
// "arguments": { "option": "option-rom" } }   <- { "return": [
// {         "parameters": [           {             "name":
// "romfile",             "type": "string"           },           {
// "name": "bootindex",             "type": "number"           }
// ],         "option": "option-rom"       }      ]     }
type QueryCommandLineOptionsCommand struct {
	// option name
	Option *string `json:"option,omitempty"`
}

// Return a list of supported QMP commands by this server
//
// Returns: A list of @CommandInfo for all supported commands
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "query-commands" }   <- {
// "return":[       {         "name":"query-balloon"       },       {
// "name":"system_powerdown"       },       ...      ]     }  This
// example has been shortened as the real response is too long.
type QueryCommandsCommand struct {
}

// Return a list of supported virtual CPU definitions
//
// Returns: a list of CpuDefinitionInfo
//
// Since: 1.2
type QueryCpuDefinitionsCommand struct {
}

// Baseline two CPU models, @modela and @modelb, creating a compatible
// third model.  The created model will always be a static, migration-
// safe CPU model (see "static" CPU model expansion for details).
// This interface can be used by tooling to create a compatible CPU
// model out two CPU models.  The created CPU model will be identical
// to or a subset of both CPU models when comparing them.  Therefore,
// the created CPU model is guaranteed to run where the given CPU
// models run.  The result returned by this command may be affected
// by:  * QEMU version: CPU models may look different depending on the
// QEMU   version.  (Except for CPU models reported as "static" in
// query-cpu-definitions.) * machine-type: CPU model may look
// different depending on the   machine-type.  (Except for CPU models
// reported as "static" in   query-cpu-definitions.) * machine options
// (including accelerator): in some architectures,   CPU models may
// look different depending on machine and accelerator   options.
// (Except for CPU models reported as "static" in   query-cpu-
// definitions.) * "-cpu" arguments and global properties: arguments
// to the -cpu   option and global properties may affect expansion of
// CPU models.   Using query-cpu-model-expansion while using these is
// not advised.  Some architectures may not support baselining CPU
// models.  s390x supports baselining CPU models.
//
// Returns: a CpuModelBaselineInfo describing the baselined CPU model
//
// Errors:   - if baselining CPU models is not supported   - if a
// model cannot be used   - if a model contains an unknown cpu
// definition name, unknown    properties or properties with wrong
// types.
//
// .. note:: This command isn't specific to s390x, but is only
// implemented on this architecture currently.
//
// Since: 2.8
type QueryCpuModelBaselineCommand struct {
	// description of the first CPU model to baseline
	Modela CpuModelInfo `json:"modela"`
	// description of the second CPU model to baseline
	Modelb CpuModelInfo `json:"modelb"`
}

// Compares two CPU models, @modela and @modelb, returning how they
// compare in a specific configuration.  The results indicates how
// both models compare regarding runnability.  This result can be used
// by tooling to make decisions if a certain CPU model will run in a
// certain configuration or if a compatible CPU model has to be
// created by baselining.  Usually, a CPU model is compared against
// the maximum possible CPU model of a certain configuration (e.g. the
// "host" model for KVM). If that CPU model is identical or a subset,
// it will run in that configuration.  The result returned by this
// command may be affected by:  * QEMU version: CPU models may look
// different depending on the QEMU   version.  (Except for CPU models
// reported as "static" in   query-cpu-definitions.) * machine-type:
// CPU model may look different depending on the   machine-type.
// (Except for CPU models reported as "static" in   query-cpu-
// definitions.) * machine options (including accelerator): in some
// architectures,   CPU models may look different depending on machine
// and accelerator   options.  (Except for CPU models reported as
// "static" in   query-cpu-definitions.) * "-cpu" arguments and global
// properties: arguments to the -cpu   option and global properties
// may affect expansion of CPU models.   Using query-cpu-model-
// expansion while using these is not advised.  Some architectures may
// not support comparing CPU models.  s390x supports comparing CPU
// models.
//
// Returns: a CpuModelCompareInfo describing how both CPU models
// compare
//
// Errors:   - if comparing CPU models is not supported   - if a model
// cannot be used   - if a model contains an unknown cpu definition
// name, unknown    properties or properties with wrong types.
//
// .. note:: This command isn't specific to s390x, but is only
// implemented on this architecture currently.
//
// Since: 2.8
type QueryCpuModelComparisonCommand struct {
	// description of the first CPU model to compare, referred to as
	// "model A" in CpuModelCompareResult
	Modela CpuModelInfo `json:"modela"`
	// description of the second CPU model to compare, referred to as
	// "model B" in CpuModelCompareResult
	Modelb CpuModelInfo `json:"modelb"`
}

// Expands a given CPU model, @model, (or a combination of CPU model +
// additional options) to different granularities, specified by @type,
// allowing tooling to get an understanding what a specific CPU model
// looks like in QEMU under a certain configuration.  This interface
// can be used to query the "host" CPU model.  The data returned by
// this command may be affected by:  * QEMU version: CPU models may
// look different depending on the QEMU   version.  (Except for CPU
// models reported as "static" in   query-cpu-definitions.) * machine-
// type: CPU model may look different depending on the   machine-type.
// (Except for CPU models reported as "static" in   query-cpu-
// definitions.) * machine options (including accelerator): in some
// architectures,   CPU models may look different depending on machine
// and accelerator   options.  (Except for CPU models reported as
// "static" in   query-cpu-definitions.) * "-cpu" arguments and global
// properties: arguments to the -cpu   option and global properties
// may affect expansion of CPU models.   Using query-cpu-model-
// expansion while using these is not advised.  Some architectures may
// not support all expansion types.  s390x supports "full" and
// "static".  Arm only supports "full".
//
// Returns: a CpuModelExpansionInfo describing the expanded CPU model
//
// Errors:   - if expanding CPU models is not supported   - if the
// model cannot be expanded   - if the model contains an unknown CPU
// definition name, unknown    properties or properties with a wrong
// type   - if an expansion type is not supported
//
// Since: 2.8
type QueryCpuModelExpansionCommand struct {
	// expansion type, specifying how to expand the CPU model
	Type CpuModelExpansionType `json:"type"`
	// description of the CPU model to expand
	Model CpuModelInfo `json:"model"`
}

// Returns information about all virtual CPUs.
//
// Returns: list of @CpuInfoFast
//
// Since: 2.12
//
// .. qmp-example::    -> { "execute": "query-cpus-fast" }   <- {
// "return": [       {         "thread-id": 25627,         "props": {
// "core-id": 0,           "thread-id": 0,           "socket-id": 0
// },         "qom-path": "/machine/unattached/device[0]",
// "target":"x86_64",         "cpu-index": 0       },       {
// "thread-id": 25628,         "props": {           "core-id": 0,
// "thread-id": 0,           "socket-id": 1         },         "qom-
// path": "/machine/unattached/device[2]",         "target":"x86_64",
// "cpu-index": 1       }     ]   }
type QueryCpusFastCommand struct {
}

// Returns information about current crypto devices.
//
// Returns: a list of @QCryptodevInfo
//
// Since: 8.0
type QueryCryptodevCommand struct {
}

// Return information on the current virtual machine.
//
// Returns: CurrentMachineParams
//
// Since: 4.0
type QueryCurrentMachineCommand struct {
}

// Query results of the most recent invocation of @calc-dirty-rate.
//
// Since: 5.2
//
// .. qmp-example::   :title: Measurement is in progress    <-
// {"status": "measuring", "sample-pages": 512,     "mode": "page-
// sampling", "start-time": 1693900454, "calc-time": 10,     "calc-
// time-unit": "second"}  .. qmp-example::   :title: Measurement has
// been completed    <- {"status": "measured", "sample-pages": 512,
// "dirty-rate": 108,     "mode": "page-sampling", "start-time":
// 1693900454, "calc-time": 10,     "calc-time-unit": "second"}
type QueryDirtyRateCommand struct {
	// time unit in which to report calculation time. By default it is
	// reported in seconds. (Since 8.2)
	CalcTimeUnit *TimeUnit `json:"calc-time-unit,omitempty"`
}

// Returns information about display configuration
//
// Returns: @DisplayOptions
//
// Since: 3.1
type QueryDisplayOptionsCommand struct {
}

// Query latest dump status.
//
// Returns: A @DumpStatus object showing the dump status.
//
// Since: 2.6
//
// .. qmp-example::    -> { "execute": "query-dump" }   <- { "return":
// { "status": "active", "completed": 1024000,            "total":
// 2048000 } }
type QueryDumpCommand struct {
}

// Returns the available formats for dump-guest-memory
//
// Returns: A @DumpGuestMemoryCapability object listing available
// formats for dump-guest-memory
//
// Since: 2.0
//
// .. qmp-example::    -> { "execute": "query-dump-guest-memory-
// capability" }   <- { "return": { "formats":            ["elf",
// "kdump-zlib", "kdump-lzo", "kdump-snappy"] } }
type QueryDumpGuestMemoryCapabilityCommand struct {
}

// Return information describing all fd sets.
//
// Returns: A list of @FdsetInfo
//
// Since: 1.2
//
// .. note:: The list of fd sets is shared by all monitor connections.
// .. qmp-example::    -> { "execute": "query-fdsets" }   <- {
// "return": [       {        "fds": [         {          "fd": 30,
// "opaque": "rdonly:/path/to/file"         },         {
// "fd": 24,          "opaque": "rdwr:/path/to/file"         }
// ],        "fdset-id": 1       },       {        "fds": [         {
// "fd": 28         },         {          "fd": 29         }        ],
// "fdset-id": 0       }      ]     }
type QueryFdsetsCommand struct {
}

// This command is ARM-only.  It will return a list of GICCapability
// objects that describe its capability bits.
//
// Returns: a list of GICCapability objects.
//
// Since: 2.6
//
// .. qmp-example::    -> { "execute": "query-gic-capabilities" }   <-
// { "return": [{ "version": 2, "emulated": true, "kernel": false },
// { "version": 3, "emulated": false, "kernel": true } ] }
type QueryGicCapabilitiesCommand struct {
}

// Returns: a list of HotpluggableCPU objects.
//
// Since: 2.7
//
// .. qmp-example::   :annotated:    For pseries machine type started
// with   "-smp 2,cores=2,maxcpus=4 -cpu POWER8"::    -> {
// "execute": "query-hotpluggable-cpus" }   <- {"return": [      {
// "props": { "core-id": 8 }, "type": "POWER8-spapr-cpu-core",
// "vcpus-count": 1 },      { "props": { "core-id": 0 }, "type":
// "POWER8-spapr-cpu-core",       "vcpus-count": 1, "qom-path":
// "/machine/unattached/device[0]"}     ]}  .. qmp-example::
// :annotated:    For pc machine type started with "-smp
// 1,maxcpus=2"::    -> { "execute": "query-hotpluggable-cpus" }   <-
// {"return": [      {       "type": "qemu64-x86_64-cpu", "vcpus-
// count": 1,       "props": {"core-id": 0, "socket-id": 1, "thread-
// id": 0}      },      {       "qom-path":
// "/machine/unattached/device[0]",       "type": "qemu64-x86_64-cpu",
// "vcpus-count": 1,       "props": {"core-id": 0, "socket-id": 0,
// "thread-id": 0}      }     ]}  .. qmp-example::   :annotated:
// For s390x-virtio-ccw machine type started with   "-smp 1,maxcpus=2
// -cpu qemu" (Since: 2.11)::    -> { "execute": "query-hotpluggable-
// cpus" }   <- {"return": [      {       "type": "qemu-s390x-cpu",
// "vcpus-count": 1,       "props": { "core-id": 1 }      },      {
// "qom-path": "/machine/unattached/device[0]",       "type":
// "qemu-s390x-cpu", "vcpus-count": 1,       "props": { "core-id": 0 }
// }     ]}
type QueryHotpluggableCpusCommand struct {
}

// Returns the hv-balloon driver data contained in the last received
// "STATUS" message from the guest.
//
// Returns:   @HvBalloonInfo
//
// Errors:   - If no hv-balloon device is present, guest memory status
// reporting is not enabled or no guest memory status report
// received yet, GenericError
//
// Since: 8.2
//
// .. qmp-example::    -> { "execute": "query-hv-balloon-status-
// report" }   <- { "return": {        "committed": 816640000,
// "available": 3333054464      }     }
type QueryHvBalloonStatusReportCommand struct {
}

// Returns a list of information about each iothread.  .. note:: This
// list excludes the QEMU main loop thread, which is not    declared
// using the "-object iothread" command-line option.  It    is
// always the main thread of the process.
//
// Returns: a list of @IOThreadInfo for each iothread
//
// Since: 2.0
//
// .. qmp-example::    -> { "execute": "query-iothreads" }   <- {
// "return": [        {         "id":"iothread0",         "thread-
// id":3134        },        {         "id":"iothread1",
// "thread-id":3135        }      ]     }
type QueryIothreadsCommand struct {
}

// Return information about jobs.
//
// Returns: a list with a @JobInfo for each active job
//
// Since: 3.0
type QueryJobsCommand struct {
}

// Returns information about KVM acceleration
//
// Returns: @KvmInfo
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "query-kvm" }   <- { "return":
// { "enabled": true, "present": true } }
type QueryKvmCommand struct {
}

// Return a list of supported machines
//
// Returns: a list of MachineInfo
//
// Since: 1.2
//
// .. qmp-example::    -> { "execute": "query-machines", "arguments":
// { "compat-props": true } }   <- { "return": [        {
// "hotpluggable-cpus": true,          "name": "pc-q35-6.2",
// "compat-props": [            {              "qom-type": "virtio-
// mem",              "property": "unplugged-inaccessible",
// "value": "off"            }          ],          "numa-mem-
// supported": false,          "default-cpu-type":
// "qemu64-x86_64-cpu",          "cpu-max": 288,
// "deprecated": false,          "default-ram-id": "pc.ram"        },
// ...     }
type QueryMachinesCommand struct {
	// if true, also return compatibility properties. (default: false)
	// (since 9.1)
	CompatProps *bool `json:"compat-props,omitempty"`
}

// Returns information for all memory backends.
//
// Returns: a list of @Memdev.
//
// Since: 2.1
//
// .. qmp-example::    -> { "execute": "query-memdev" }   <- {
// "return": [       {        "id": "mem1",        "size": 536870912,
// "merge": false,        "dump": true,        "prealloc": false,
// "share": false,        "host-nodes": [0, 1],        "policy":
// "bind"       },       {        "size": 536870912,        "merge":
// false,        "dump": true,        "prealloc": true,
// "share": false,        "host-nodes": [2, 3],        "policy":
// "preferred"       }      ]     }
type QueryMemdevCommand struct {
}

// Lists available memory devices and their state
//
// Since: 2.1
//
// .. qmp-example::    -> { "execute": "query-memory-devices" }   <- {
// "return": [ { "data":              { "addr": 5368709120,
// "hotpluggable": true,               "hotplugged": true,
// "id": "d1",               "memdev": "/objects/memX",
// "node": 0,               "size": 1073741824,               "slot":
// 0},             "type": "dimm"            } ] }
type QueryMemoryDevicesCommand struct {
}

// Return the amount of initially allocated and present hotpluggable
// (if enabled) memory in bytes.  .. qmp-example::      -> {
// "execute": "query-memory-size-summary" }     <- { "return": {
// "base-memory": 4294967296, "plugged-memory": 0 } }
//
// Since: 2.11
type QueryMemorySizeSummaryCommand struct {
}

// Returns information about each active mouse device
//
// Returns: a list of @MouseInfo for each device
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "query-mice" }   <- { "return":
// [        {         "name":"QEMU Microsoft Mouse",
// "index":0,         "current":false,         "absolute":false
// },        {         "name":"QEMU PS/2 Mouse",         "index":1,
// "current":true,         "absolute":true        }      ]     }
type QueryMiceCommand struct {
}

// Returns information about current migration process.  If migration
// is active there will be another json-object with RAM migration
// status.
//
// Returns: @MigrationInfo
//
// Since: 0.14
//
// .. qmp-example::   :title: Before the first migration    -> {
// "execute": "query-migrate" }   <- { "return": {} }  .. qmp-
// example::   :title: Migration is done and has succeeded    -> {
// "execute": "query-migrate" }   <- { "return": {       "status":
// "completed",       "total-time":12345,       "setup-time":12345,
// "downtime":12345,       "ram":{        "transferred":123,
// "remaining":123,        "total":246,        "duplicate":123,
// "normal":123,        "normal-bytes":123456,        "dirty-sync-
// count":15       }      }     }  .. qmp-example::   :title:
// Migration is done and has failed    -> { "execute": "query-migrate"
// }   <- { "return": { "status": "failed" } }  .. qmp-example::
// :title: Migration is being performed    -> { "execute": "query-
// migrate" }   <- {      "return":{        "status":"active",
// "total-time":12345,        "setup-time":12345,        "expected-
// downtime":12345,        "ram":{         "transferred":123,
// "remaining":123,         "total":246,         "duplicate":123,
// "normal":123,         "normal-bytes":123456,         "dirty-sync-
// count":15        }      }     }  .. qmp-example::   :title:
// Migration is being performed and XBZRLE is active    -> {
// "execute": "query-migrate" }   <- {      "return":{
// "status":"active",        "total-time":12345,        "setup-
// time":12345,        "expected-downtime":12345,        "ram":{
// "total":1057024,         "remaining":1053304,
// "transferred":3720,         "duplicate":10,         "normal":3333,
// "normal-bytes":3412992,         "dirty-sync-count":15        },
// "xbzrle-cache":{         "cache-size":67108864,
// "bytes":20971520,         "pages":2444343,         "cache-
// miss":2244,         "cache-miss-rate":0.123,         "encoding-
// rate":80.1,         "overflow":34434        }      }     }
type QueryMigrateCommand struct {
}

// Returns information about the current migration capabilities status
//
// Returns: @MigrationCapabilityStatus
//
// Since: 1.2
//
// .. qmp-example::    -> { "execute": "query-migrate-capabilities" }
// <- { "return": [      {"state": false, "capability": "xbzrle"},
// {"state": false, "capability": "rdma-pin-all"},      {"state":
// false, "capability": "auto-converge"},      {"state": false,
// "capability": "zero-blocks"},      {"state": true, "capability":
// "events"},      {"state": false, "capability": "postcopy-ram"},
// {"state": false, "capability": "x-colo"}     ]}
type QueryMigrateCapabilitiesCommand struct {
}

// Returns information about the current migration parameters
//
// Returns: @MigrationParameters
//
// Since: 2.4
//
// .. qmp-example::    -> { "execute": "query-migrate-parameters" }
// <- { "return": {        "multifd-channels": 2,        "cpu-
// throttle-increment": 10,        "cpu-throttle-initial": 20,
// "max-bandwidth": 33554432,        "downtime-limit": 300      }
// }
type QueryMigrateParametersCommand struct {
}

// Returns information of migration threads
//
// Returns: @MigrationThreadInfo
//
// Since: 7.2
type QueryMigrationthreadsCommand struct {
}

// Return the name information of a guest.
//
// Returns: @NameInfo of the guest
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "query-name" }   <- { "return":
// { "name": "qemu-name" } }
type QueryNameCommand struct {
}

// Get the named block driver list
//
// Returns: the list of BlockDeviceInfo
//
// Since: 2.0
//
// .. qmp-example::    -> { "execute": "query-named-block-nodes" }
// <- { "return": [ { "ro":false,             "drv":"qcow2",
// "encrypted":false,             "file":"disks/test.qcow2",
// "node-name": "my-node",             "backing_file_depth":1,
// "detect_zeroes":"off",             "bps":1000000,
// "bps_rd":0,             "bps_wr":0,             "iops":1000000,
// "iops_rd":0,             "iops_wr":0,             "bps_max":
// 8000000,             "bps_rd_max": 0,             "bps_wr_max": 0,
// "iops_max": 0,             "iops_rd_max": 0,
// "iops_wr_max": 0,             "iops_size": 0,
// "write_threshold": 0,             "image":{
// "filename":"disks/test.qcow2",              "format":"qcow2",
// "virtual-size":2048000,              "backing_file":"base.qcow2",
// "full-backing-filename":"disks/base.qcow2",              "backing-
// filename-format":"qcow2",              "snapshots":[
// {                 "id": "1",                 "name": "snapshot1",
// "vm-state-size": 0,                 "date-sec": 10000200,
// "date-nsec": 12,                 "vm-clock-sec": 206,
// "vm-clock-nsec": 30                }              ],
// "backing-image":{                "filename":"disks/base.qcow2",
// "format":"qcow2",                "virtual-size":2048000
// }             } } ] }
type QueryNamedBlockNodesCommand struct {
	// Omit the nested data about backing image ("backing-image" key)
	// if true. Default is false (Since 5.0)
	Flat *bool `json:"flat,omitempty"`
}

// Return information about the PCI bus topology of the guest.
//
// Returns: a list of @PciInfo for each PCI bus. Each bus is
// represented by a json-object, which has a key with a json-array
// of all PCI devices attached to it. Each device is represented   by
// a json-object.
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "query-pci" }   <- { "return":
// [        {         "bus": 0,         "devices": [           {
// "bus": 0,            "qdev_id": "",            "slot": 0,
// "class_info": {              "class": 1536,              "desc":
// "Host bridge"            },            "id": {
// "device": 32902,              "vendor": 4663            },
// "function": 0,            "regions": [            ]           },
// {            "bus": 0,            "qdev_id": "",            "slot":
// 1,            "class_info": {              "class": 1537,
// "desc": "ISA bridge"            },            "id": {
// "device": 32902,              "vendor": 28672            },
// "function": 0,            "regions": [            ]           },
// {            "bus": 0,            "qdev_id": "",            "slot":
// 1,            "class_info": {              "class": 257,
// "desc": "IDE controller"            },            "id": {
// "device": 32902,              "vendor": 28688            },
// "function": 1,            "regions": [              {
// "bar": 4,               "size": 16,               "address": 49152,
// "type": "io"              }            ]           },           {
// "bus": 0,            "qdev_id": "",            "slot": 2,
// "class_info": {              "class": 768,              "desc":
// "VGA controller"            },            "id": {
// "device": 4115,              "vendor": 184            },
// "function": 0,            "regions": [              {
// "prefetch": true,               "mem_type_64": false,
// "bar": 0,               "size": 33554432,               "address":
// 4026531840,               "type": "memory"              },
// {               "prefetch": false,               "mem_type_64":
// false,               "bar": 1,               "size": 4096,
// "address": 4060086272,               "type": "memory"
// },              {               "prefetch": false,
// "mem_type_64": false,               "bar": 6,               "size":
// 65536,               "address": -1,               "type": "memory"
// }            ]           },           {            "bus": 0,
// "qdev_id": "",            "irq": 11,            "slot": 4,
// "class_info": {              "class": 1280,              "desc":
// "RAM controller"            },            "id": {
// "device": 6900,              "vendor": 4098            },
// "function": 0,            "regions": [              {
// "bar": 0,               "size": 32,               "address": 49280,
// "type": "io"              }            ]           }         ]
// }      ]     }  This example has been shortened as the real
// response is too long.
type QueryPciCommand struct {
}

// Returns a list of information about each persistent reservation
// manager.
//
// Returns: a list of @PRManagerInfo for each persistent reservation
// manager
//
// Since: 3.0
type QueryPrManagersCommand struct {
}

// Command query-qmp-schema exposes the QMP wire ABI as an array of
// SchemaInfo.  This lets QMP clients figure out what commands and
// events are available in this QEMU, and their parameters and
// results.  However, the SchemaInfo can't reflect all the rules and
// restrictions that apply to QMP.  It's interface introspection
// (figuring out what's there), not interface specification.  The
// specification is in the QAPI schema.  Furthermore, while we strive
// to keep the QMP wire format backwards-compatible across qemu
// versions, the introspection output is not guaranteed to have the
// same stability.  For example, one version of qemu may list an
// object member as an optional non-variant, while another lists the
// same member only through the object's variants; or the type of a
// member may change from a generic string into a specific enum or
// from one specific type into an alternate that includes the original
// type alongside something else.
//
// Returns: array of @SchemaInfo, where each element describes an
// entity in the ABI: command, event, type, ...    The order of the
// various SchemaInfo is unspecified; however, all   names are
// guaranteed to be unique (no name will be duplicated   with
// different meta-types).
//
// .. note:: The QAPI schema is also used to help define *internal*
// interfaces, by defining QAPI types. These are not part of the   QMP
// wire ABI, and therefore not returned by this command.
//
// Since: 2.5
type QueryQmpSchemaCommand struct {
}

// Retrieve the record/replay information.  It includes current
// instruction count which may be used for @replay-break and @replay-
// seek commands.
//
// Returns: record/replay information.
//
// Since: 5.2
//
// .. qmp-example::    -> { "execute": "query-replay" }   <- {
// "return": { "mode": "play", "filename": "log.rr", "icount": 220414
// } }
type QueryReplayCommand struct {
}

// Return rocker switch information.
//
// Returns: @Rocker information
//
// Since: 2.4
//
// .. qmp-example::    -> { "execute": "query-rocker", "arguments": {
// "name": "sw1" } }   <- { "return": {"name": "sw1", "ports": 2,
// "id": 1327446905938}}
type QueryRockerCommand struct {
	// switch name
	Name string `json:"name"`
}

// Return rocker OF-DPA flow information.
//
// Returns: rocker OF-DPA flow information
//
// Since: 2.4
//
// .. qmp-example::    -> { "execute": "query-rocker-of-dpa-flows",
// "arguments": { "name": "sw1" } }   <- { "return": [ {"key": {"in-
// pport": 0, "priority": 1, "tbl-id": 0},            "hits": 138,
// "cookie": 0,            "action": {"goto-tbl": 10},
// "mask": {"in-pport": 4294901760}            },            {...},
// ]}
type QueryRockerOfDpaFlowsCommand struct {
	// switch name
	Name string `json:"name"`
	// flow table ID. If tbl-id is not specified, returns flow
	// information for all tables.
	TblId *uint32 `json:"tbl-id,omitempty"`
}

// Return rocker OF-DPA group information.
//
// Returns: rocker OF-DPA group information
//
// Since: 2.4
//
// .. qmp-example::    -> { "execute": "query-rocker-of-dpa-groups",
// "arguments": { "name": "sw1" } }   <- { "return": [ {"type": 0,
// "out-pport": 2,            "pport": 2, "vlan-id": 3841,
// "pop-vlan": 1, "id": 251723778},            {"type": 0, "out-
// pport": 0,            "pport": 0, "vlan-id": 3841,            "pop-
// vlan": 1, "id": 251723776},            {"type": 0, "out-pport": 1,
// "pport": 1, "vlan-id": 3840,            "pop-vlan": 1, "id":
// 251658241},            {"type": 0, "out-pport": 0,
// "pport": 0, "vlan-id": 3840,            "pop-vlan": 1, "id":
// 251658240}     ]}
type QueryRockerOfDpaGroupsCommand struct {
	// switch name
	Name string `json:"name"`
	// group type. If type is not specified, returns group information
	// for all group types.
	Type *uint8 `json:"type,omitempty"`
}

// Return rocker switch port information.
//
// Returns: a list of @RockerPort information
//
// Since: 2.4
//
// .. qmp-example::    -> { "execute": "query-rocker-ports",
// "arguments": { "name": "sw1" } }   <- { "return": [ {"duplex":
// "full", "enabled": true, "name": "sw1.1",            "autoneg":
// "off", "link-up": true, "speed": 10000},            {"duplex":
// "full", "enabled": true, "name": "sw1.2",            "autoneg":
// "off", "link-up": true, "speed": 10000}     ]}
type QueryRockerPortsCommand struct {
	// port name
	Name string `json:"name"`
}

// Return rx-filter information for all NICs (or for the given NIC).
//
// Returns: list of @RxFilterInfo for all NICs (or for the given NIC).
//
// Errors:   - if the given @name doesn't exist   - if the given NIC
// doesn't support rx-filter querying   - if the given net client
// isn't a NIC
//
// Since: 1.6
//
// .. qmp-example::    -> { "execute": "query-rx-filter", "arguments":
// { "name": "vnet0" } }   <- { "return": [       {
// "promiscuous": true,         "name": "vnet0",         "main-mac":
// "52:54:00:12:34:56",         "unicast": "normal",         "vlan":
// "normal",         "vlan-table": [           4,           0
// ],         "unicast-table": [         ],         "multicast":
// "normal",         "multicast-overflow": false,         "unicast-
// overflow": false,         "multicast-table": [
// "01:00:5e:00:00:01",           "33:33:00:00:00:01",
// "33:33:ff:12:34:56"         ],         "broadcast-allowed": false
// }      ]     }
type QueryRxFilterCommand struct {
	// net client name
	Name *string `json:"name,omitempty"`
}

// Returns: the machine's CPU polarization
//
// Since: 8.2
type QueryS390XCpuPolarizationCommand struct {
}

// Returns information about SEV
//
// Returns: @SevInfo
//
// Since: 2.12
//
// .. qmp-example::    -> { "execute": "query-sev" }   <- { "return":
// { "enabled": true, "api-major" : 0, "api-minor" : 0,
// "build-id" : 0, "policy" : 0, "state" : "running",
// "handle" : 1 } }
type QuerySevCommand struct {
}

// This command is used to get the SEV attestation report, and is
// supported on AMD X86 platforms only.
//
// Returns: SevAttestationReport objects.
//
// Since: 6.1
//
// .. qmp-example::    -> { "execute" : "query-sev-attestation-
// report",            "arguments": { "mnonce": "aaaaaaa" } }   <- {
// "return" : { "data": "aaaaaaaabbbddddd"} }
type QuerySevAttestationReportCommand struct {
	// a random 16 bytes value encoded in base64 (it will be included
	// in report)
	Mnonce string `json:"mnonce"`
}

// This command is used to get the SEV capabilities, and is supported
// on AMD X86 platforms only.
//
// Returns: SevCapability objects.
//
// Since: 2.12
//
// .. qmp-example::    -> { "execute": "query-sev-capabilities" }   <-
// { "return": { "pdh": "8CCDD8DDD", "cert-chain": "888CCCDDDEE",
// "cpu0-id": "2lvmGwo+...61iEinw==",            "cbitpos": 47,
// "reduced-phys-bits": 1}}
type QuerySevCapabilitiesCommand struct {
}

// Query the SEV guest launch information.
//
// Returns: The @SevLaunchMeasureInfo for the guest
//
// Since: 2.12
//
// .. qmp-example::    -> { "execute": "query-sev-launch-measure" }
// <- { "return": { "data": "4l8LXeNlSPUDlXPJG5966/8%YZ" } }
type QuerySevLaunchMeasureCommand struct {
}

// Returns information about SGX
//
// Returns: @SGXInfo
//
// Since: 6.2
//
// .. qmp-example::    -> { "execute": "query-sgx" }   <- { "return":
// { "sgx": true, "sgx1" : true, "sgx2" : true,            "flc":
// true,            "sections": [{"node": 0, "size": 67108864},
// {"node": 1, "size": 29360128}]} }
type QuerySgxCommand struct {
}

// Returns information from host SGX capabilities
//
// Returns: @SGXInfo
//
// Since: 6.2
//
// .. qmp-example::    -> { "execute": "query-sgx-capabilities" }   <-
// { "return": { "sgx": true, "sgx1" : true, "sgx2" : true,
// "flc": true,            "section" : [{"node": 0, "size": 67108864},
// {"node": 1, "size": 29360128}]} }
type QuerySgxCapabilitiesCommand struct {
}

// Returns information about the current SPICE server
//
// Returns: @SpiceInfo
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "query-spice" }   <- {
// "return": {        "enabled": true,        "auth": "spice",
// "port": 5920,        "migrated":false,        "tls-port": 5921,
// "host": "0.0.0.0",        "mouse-mode":"client",        "channels":
// [         {           "port": "54924",           "family": "ipv4",
// "channel-type": 1,           "connection-id": 1804289383,
// "host": "127.0.0.1",           "channel-id": 0,           "tls":
// true         },         {           "port": "36710",
// "family": "ipv4",           "channel-type": 4,
// "connection-id": 1804289383,           "host": "127.0.0.1",
// "channel-id": 0,           "tls": false         },         ...
// ]      }     }
type QuerySpiceCommand struct {
}

// Return runtime-collected statistics for objects such as the VM or
// its vCPUs.  The arguments are a StatsFilter and specify the
// provider and objects to return statistics about.
//
// Returns: a list of StatsResult, one for each provider and object
// (e.g., for each vCPU).
//
// Since: 7.1
type QueryStatsCommand struct {
	StatsFilter
}

// Return the schema for all available runtime-collected statistics.
//
// .. note:: Runtime-collected statistics and their names fall outside
// QEMU's usual deprecation policies. QEMU will try to keep the set
// of available data stable, together with their names, but will not
// guarantee stability at all costs; the same is true of providers
// that source statistics externally, e.g. from Linux. For example,
// if the same value is being tracked with different names on
// different architectures or by different providers, one of them
// might be renamed. A statistic might go away if an algorithm is
// changed or some code is removed; changing a default might cause
// previously useful statistics to always report 0. Such changes,
// however, are expected to be rare.
//
// Since: 7.1
type QueryStatsSchemasCommand struct {
	// a provider to restrict the query to.
	Provider *StatsProvider `json:"provider,omitempty"`
}

// Query the run status of the VM
//
// Returns: @StatusInfo reflecting the VM
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "query-status" }   <- {
// "return": { "running": true,            "status": "running" } }
type QueryStatusCommand struct {
}

// Return information about the target for this QEMU
//
// Returns: TargetInfo
//
// Since: 1.2
type QueryTargetCommand struct {
}

// Return information about the TPM device
//
// Since: 1.5
//
// .. qmp-example::    -> { "execute": "query-tpm" }   <- { "return":
// [       { "model": "tpm-tis",        "options":         { "type":
// "passthrough",          "data":           { "cancel-path":
// "/sys/class/misc/tpm0/device/cancel",            "path":
// "/dev/tpm0"           }         },        "id": "tpm0"       }
// ]     }
type QueryTpmCommand struct {
}

// Return a list of supported TPM models
//
// Returns: a list of TpmModel
//
// Since: 1.5
//
// .. qmp-example::    -> { "execute": "query-tpm-models" }   <- {
// "return": [ "tpm-tis", "tpm-crb", "tpm-spapr" ] }
type QueryTpmModelsCommand struct {
}

// Return a list of supported TPM types
//
// Returns: a list of TpmType
//
// Since: 1.5
//
// .. qmp-example::    -> { "execute": "query-tpm-types" }   <- {
// "return": [ "passthrough", "emulator" ] }
type QueryTpmTypesCommand struct {
}

// Query the guest UUID information.
//
// Returns: The @UuidInfo for the guest
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "query-uuid" }   <- { "return":
// { "UUID": "550e8400-e29b-41d4-a716-446655440000" } }
type QueryUuidCommand struct {
}

// Returns information about virtual CPU dirty page rate limits, if
// any.
//
// Since: 7.1
//
// .. qmp-example::    -> {"execute": "query-vcpu-dirty-limit"}   <-
// {"return": [       { "limit-rate": 60, "current-rate": 3, "cpu-
// index": 0},       { "limit-rate": 60, "current-rate": 3, "cpu-
// index": 1}]}
type QueryVcpuDirtyLimitCommand struct {
}

// Returns the current version of QEMU.
//
// Returns: A @VersionInfo object describing the current version of
// QEMU.
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "query-version" }   <- {
// "return":{        "qemu":{         "major":0,         "minor":11,
// "micro":5        },        "package":""      }     }
type QueryVersionCommand struct {
}

// Show Virtual Machine Generation ID
//
// Since: 2.9
type QueryVmGenerationIdCommand struct {
}

// Returns information about the current VNC server
//
// Returns: @VncInfo
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "query-vnc" }   <- { "return":
// {        "enabled":true,        "host":"0.0.0.0",
// "service":"50402",        "auth":"vnc",        "family":"ipv4",
// "clients":[         {           "host":"127.0.0.1",
// "service":"50401",           "family":"ipv4",
// "websocket":false         }        ]      }     }
type QueryVncCommand struct {
}

// Returns a list of vnc servers.  The list can be empty.
//
// Returns: a list of @VncInfo2
//
// Since: 2.3
type QueryVncServersCommand struct {
}

// Query replication status while the vm is running.
//
// Returns: A @ReplicationStatus object showing the status.
//
// .. qmp-example::    -> { "execute": "query-xen-replication-status"
// }   <- { "return": { "error": false } }
//
// Since: 2.9
type QueryXenReplicationStatusCommand struct {
}

// Query yank instances.  See @YankInstance for more information.
//
// Returns: list of @YankInstance
//
// .. qmp-example::    -> { "execute": "query-yank" }   <- { "return":
// [        { "type": "block-node",         "node-name": "nbd0" }
// ] }
//
// Since: 6.0
type QueryYankCommand struct {
}

// This command will cause the QEMU process to exit gracefully.  While
// every attempt is made to send the QMP response before terminating,
// this is not guaranteed.  When using this interface, a premature EOF
// would not be unexpected.
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "quit" }   <- { "return": {} }
type QuitCommand struct {
}

// Remove a file descriptor from an fd set.
//
// Errors:   - If @fdset-id or @fd is not found, GenericError
//
// Since: 1.2
//
// .. note:: The list of fd sets is shared by all monitor connections.
// .. note:: If @fd is not specified, all file descriptors in @fdset-
// id   will be removed.  .. qmp-example::    -> { "execute": "remove-
// fd", "arguments": { "fdset-id": 1, "fd": 3 } }   <- { "return": {}
// }
type RemoveFdCommand struct {
	// The ID of the fd set that the file descriptor belongs to.
	FdsetId int64 `json:"fdset-id"`
	// The file descriptor that is to be removed.
	Fd *int64 `json:"fd,omitempty"`
}

// Set replay breakpoint at instruction count @icount.  Execution
// stops when the specified instruction is reached.  There can be at
// most one breakpoint.  When breakpoint is set, any prior one is
// removed.  The breakpoint may be set only in replay mode and only
// "in the future", i.e. at instruction counts greater than the
// current one.  The current instruction count can be observed with
// @query-replay.
//
// Since: 5.2
//
// .. qmp-example::    -> { "execute": "replay-break", "arguments": {
// "icount": 220414 } }   <- { "return": {} }
type ReplayBreakCommand struct {
	// instruction count to stop at
	Icount int64 `json:"icount"`
}

// Remove replay breakpoint which was set with @replay-break.  The
// command is ignored when there are no replay breakpoints.
//
// Since: 5.2
//
// .. qmp-example::    -> { "execute": "replay-delete-break" }   <- {
// "return": {} }
type ReplayDeleteBreakCommand struct {
}

// Automatically proceed to the instruction count @icount, when
// replaying the execution.  The command automatically loads nearest
// snapshot and replays the execution to find the desired instruction.
// When there is no preceding snapshot or the execution is not
// replayed, then the command fails.  Instruction count can be
// obtained with the @query-replay command.
//
// Since: 5.2
//
// .. qmp-example::    -> { "execute": "replay-seek", "arguments": {
// "icount": 220414 } }   <- { "return": {} }
type ReplaySeekCommand struct {
	// target instruction count
	Icount int64 `json:"icount"`
}

// Retrieve an eBPF object that can be loaded with libbpf.  Management
// applications (e.g. libvirt) may load it and pass file descriptors
// to QEMU, so they can run running QEMU without BPF capabilities.
//
// Returns: eBPF object encoded in base64.
//
// Since: 9.0
type RequestEbpfCommand struct {
	// The ID of the program to return.
	Id EbpfProgramID `json:"id"`
}

// Read from a ring buffer character device.
//
// Returns: data read from the device
//
// Since: 1.4
//
// .. qmp-example::    -> { "execute": "ringbuf-read",
// "arguments": { "device": "foo",             "size": 1000,
// "format": "utf8" } }   <- { "return": "abcdefgh" }
type RingbufReadCommand struct {
	// the ring buffer character device name
	Device string `json:"device"`
	// how many bytes to read at most
	Size int64 `json:"size"`
	// data encoding (default 'utf8'). - base64: the data read is
	// returned in base64 encoding. - utf8: the data read is
	// interpreted as UTF-8. Bug: can screw up when the buffer
	// contains invalid UTF-8 sequences, NUL characters, after the
	// ring buffer lost data, and when reading stops because the size
	// limit is reached. - The return value is always Unicode
	// regardless of format, like any other string.
	Format *DataFormat `json:"format,omitempty"`
}

// Write to a ring buffer character device.
//
// Since: 1.4
//
// .. qmp-example::    -> { "execute": "ringbuf-write",
// "arguments": { "device": "foo",             "data": "abcdefgh",
// "format": "utf8" } }   <- { "return": {} }
type RingbufWriteCommand struct {
	// the ring buffer character device name
	Device string `json:"device"`
	// data to write
	Data string `json:"data"`
	// data encoding (default 'utf8'). - base64: data must be base64
	// encoded text. Its binary decoding gets written. - utf8: data's
	// UTF-8 encoding is written - data itself is always Unicode
	// regardless of format, like any other string.
	Format *DataFormat `json:"format,omitempty"`
}

// This command will reset the RTC interrupt reinjection backlog.  Can
// be used if another mechanism to synchronize guest time is in
// effect, for example QEMU guest agent's guest-set-time command.
//
// Since: 2.1
//
// .. qmp-example::    -> { "execute": "rtc-reset-reinjection" }   <-
// { "return": {} }
type RtcResetReinjectionCommand struct {
}

// Capture the contents of a screen and write it to a file.
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "screendump",      "arguments":
// { "filename": "/tmp/image" } }   <- { "return": {} }
type ScreendumpCommand struct {
	// the path of a new file to store the image
	Filename string `json:"filename"`
	// ID of the display device that should be dumped. If this
	// parameter is missing, the primary display will be used. (Since
	// 2.12)
	Device *string `json:"device,omitempty"`
	// head to use in case the device supports multiple heads. If this
	// parameter is missing, head #0 will be used. Also note that the
	// head can only be specified in conjunction with the device ID.
	// (Since 2.12)
	Head *int64 `json:"head,omitempty"`
	// image format for screendump. (default: ppm) (Since 7.1)
	Format *ImageFormat `json:"format,omitempty"`
}

// Send keys to guest.
//
// Errors:   - If key is unknown or redundant, GenericError
//
// Since: 1.3
//
// .. qmp-example::    -> { "execute": "send-key",      "arguments": {
// "keys": [ { "type": "qcode", "data": "ctrl" },                  {
// "type": "qcode", "data": "alt" },                  { "type":
// "qcode", "data": "delete" } ] } }   <- { "return": {} }
type SendKeyCommand struct {
	// An array of @KeyValue elements. All @KeyValues in this array
	// are simultaneously sent to the guest. A @KeyValue.number value
	// is sent directly to the guest, while @KeyValue.qcode must be a
	// valid @QKeyCode value
	Keys []KeyValue `json:"keys"`
	// time to delay key up events, milliseconds. Defaults to 100
	HoldTime *int64 `json:"hold-time,omitempty"`
}

// Set the actions that will be taken by the emulator in response to
// guest events.
//
// Since: 6.0
//
// .. qmp-example::    -> { "execute": "set-action",      "arguments":
// { "reboot": "shutdown",             "shutdown" : "pause",
// "panic": "pause",             "watchdog": "inject-nmi" } }   <- {
// "return": {} }
type SetActionCommand struct {
	// @RebootAction action taken on guest reboot.
	Reboot *RebootAction `json:"reboot,omitempty"`
	// @ShutdownAction action taken on guest shutdown.
	Shutdown *ShutdownAction `json:"shutdown,omitempty"`
	// @PanicAction action taken on guest panic.
	Panic *PanicAction `json:"panic,omitempty"`
	// @WatchdogAction action taken when watchdog timer expires.
	Watchdog *WatchdogAction `json:"watchdog,omitempty"`
}

// Modify the topology by moving the CPU inside the topology tree, or
// by changing a modifier attribute of a CPU.  Absent values will not
// be modified.
//
// Since: 8.2
type SetCpuTopologyCommand struct {
	// the vCPU ID to be moved
	CoreId uint16 `json:"core-id"`
	// destination socket to move the vCPU to
	SocketId *uint16 `json:"socket-id,omitempty"`
	// destination book to move the vCPU to
	BookId *uint16 `json:"book-id,omitempty"`
	// destination drawer to move the vCPU to
	DrawerId *uint16 `json:"drawer-id,omitempty"`
	// entitlement to set
	Entitlement *S390CpuEntitlement `json:"entitlement,omitempty"`
	// whether the provisioning of real to virtual CPU is dedicated
	Dedicated *bool `json:"dedicated,omitempty"`
}

// Runtime equivalent of '-numa' CLI option, available at preconfigure
// stage to configure numa mapping before initializing machine.
//
// Since: 3.0
type SetNumaNodeCommand struct {
	NumaOptions
}

// Set the upper limit of dirty page rate for virtual CPUs.  Requires
// KVM with accelerator property "dirty-ring-size" set.  A virtual
// CPU's dirty page rate is a measure of its memory load.  To observe
// dirty page rates, use @calc-dirty-rate.
//
// Since: 7.1
//
// .. qmp-example::    -> {"execute": "set-vcpu-dirty-limit"}
// "arguments": { "dirty-rate": 200,             "cpu-index": 1 } }
// <- { "return": {} }
type SetVcpuDirtyLimitCommand struct {
	// index of a virtual CPU, default is all.
	CpuIndex *int64 `json:"cpu-index,omitempty"`
	// upper limit of dirty page rate (MB/s) for virtual CPUs.
	DirtyRate uint64 `json:"dirty-rate"`
}

// Sets the link status of a virtual network adapter.
//
// Errors:   - If @name is not a valid network device, DeviceNotFound
//
// Since: 0.14
//
// .. note:: Not all network adapters support setting link status.
// This command will succeed even if the network adapter does not
// support link status notification.  .. qmp-example::    -> {
// "execute": "set_link",      "arguments": { "name": "e1000.0", "up":
// false } }   <- { "return": {} }
type SetLinkCommand struct {
	// the device name of the virtual network adapter
	Name string `json:"name"`
	// true to set the link status to be up
	Up bool `json:"up"`
}

// Set the password of a remote display server.
//
// Errors:   - If Spice is not enabled, DeviceNotFound
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "set_password", "arguments": {
// "protocol": "vnc",                           "password": "secret" }
// }   <- { "return": {} }
type SetPasswordCommand struct {
	SetPasswordOptions
}

// This command injects a secret blob into memory of SEV guest.
//
// Since: 6.0
type SevInjectLaunchSecretCommand struct {
	// the launch secret packet header encoded in base64
	PacketHeader string `json:"packet-header"`
	// the launch secret data to be injected encoded in base64
	Secret string `json:"secret"`
	// the guest physical address where secret will be injected.
	Gpa *uint64 `json:"gpa,omitempty"`
}

// Delete a VM snapshot
//
// Applications should not assume that the snapshot delete is complete
// when this command returns. The job commands / events must be used
// to determine completion and to fetch details of any errors that
// arise.  .. qmp-example::    -> { "execute": "snapshot-delete",
// "arguments": {       "job-id": "snapdelete0",       "tag": "my-
// snap",       "devices": ["disk0", "disk1"]      }     }   <- {
// "return": { } }   <- {"event": "JOB_STATUS_CHANGE",
// "timestamp": {"seconds": 1442124172, "microseconds": 744001},
// "data": {"status": "created", "id": "snapdelete0"}}   <- {"event":
// "JOB_STATUS_CHANGE",     "timestamp": {"seconds": 1442125172,
// "microseconds": 744001},     "data": {"status": "running", "id":
// "snapdelete0"}}   <- {"event": "JOB_STATUS_CHANGE",
// "timestamp": {"seconds": 1442126172, "microseconds": 744001},
// "data": {"status": "waiting", "id": "snapdelete0"}}   <- {"event":
// "JOB_STATUS_CHANGE",     "timestamp": {"seconds": 1442127172,
// "microseconds": 744001},     "data": {"status": "pending", "id":
// "snapdelete0"}}   <- {"event": "JOB_STATUS_CHANGE",
// "timestamp": {"seconds": 1442128172, "microseconds": 744001},
// "data": {"status": "concluded", "id": "snapdelete0"}}   ->
// {"execute": "query-jobs"}   <- {"return": [{"current-progress": 1,
// "status": "concluded",           "total-progress": 1,
// "type": "snapshot-delete",           "id": "snapdelete0"}]}
//
// Since: 6.0
type SnapshotDeleteCommand struct {
	// identifier for the newly created job
	JobId string `json:"job-id"`
	// name of the snapshot to delete.
	Tag string `json:"tag"`
	// list of block device node names to delete a snapshot from
	Devices []string `json:"devices"`
}

// Load a VM snapshot
//
// Applications should not assume that the snapshot load is complete
// when this command returns. The job commands / events must be used
// to determine completion and to fetch details of any errors that
// arise.  Note that execution of the guest CPUs will be stopped
// during the time it takes to load the snapshot.  It is strongly
// recommended that @devices contain all writable block device nodes
// that can have changed since the original @snapshot-save command
// execution.  .. qmp-example::    -> { "execute": "snapshot-load",
// "arguments": {       "job-id": "snapload0",       "tag": "my-snap",
// "vmstate": "disk0",       "devices": ["disk0", "disk1"]      }
// }   <- { "return": { } }   <- {"event": "JOB_STATUS_CHANGE",
// "timestamp": {"seconds": 1472124172, "microseconds": 744001},
// "data": {"status": "created", "id": "snapload0"}}   <- {"event":
// "JOB_STATUS_CHANGE",     "timestamp": {"seconds": 1472125172,
// "microseconds": 744001},     "data": {"status": "running", "id":
// "snapload0"}}   <- {"event": "STOP",     "timestamp": {"seconds":
// 1472125472, "microseconds": 744001} }   <- {"event": "RESUME",
// "timestamp": {"seconds": 1472125872, "microseconds": 744001} }   <-
// {"event": "JOB_STATUS_CHANGE",     "timestamp": {"seconds":
// 1472126172, "microseconds": 744001},     "data": {"status":
// "waiting", "id": "snapload0"}}   <- {"event": "JOB_STATUS_CHANGE",
// "timestamp": {"seconds": 1472127172, "microseconds": 744001},
// "data": {"status": "pending", "id": "snapload0"}}   <- {"event":
// "JOB_STATUS_CHANGE",     "timestamp": {"seconds": 1472128172,
// "microseconds": 744001},     "data": {"status": "concluded", "id":
// "snapload0"}}   -> {"execute": "query-jobs"}   <- {"return":
// [{"current-progress": 1,           "status": "concluded",
// "total-progress": 1,           "type": "snapshot-load",
// "id": "snapload0"}]}
//
// Since: 6.0
type SnapshotLoadCommand struct {
	// identifier for the newly created job
	JobId string `json:"job-id"`
	// name of the snapshot to load.
	Tag string `json:"tag"`
	// block device node name to load vmstate from
	Vmstate string `json:"vmstate"`
	// list of block device node names to load a snapshot from
	Devices []string `json:"devices"`
}

// Save a VM snapshot
//
// Applications should not assume that the snapshot save is complete
// when this command returns. The job commands / events must be used
// to determine completion and to fetch details of any errors that
// arise.  Note that execution of the guest CPUs may be stopped during
// the time it takes to save the snapshot. A future version of QEMU
// may ensure CPUs are executing continuously.  It is strongly
// recommended that @devices contain all writable block device nodes
// if a consistent snapshot is required.  If @tag already exists, an
// error will be reported  .. qmp-example::    -> { "execute":
// "snapshot-save",      "arguments": {       "job-id": "snapsave0",
// "tag": "my-snap",       "vmstate": "disk0",       "devices":
// ["disk0", "disk1"]      }     }   <- { "return": { } }   <-
// {"event": "JOB_STATUS_CHANGE",     "timestamp": {"seconds":
// 1432121972, "microseconds": 744001},     "data": {"status":
// "created", "id": "snapsave0"}}   <- {"event": "JOB_STATUS_CHANGE",
// "timestamp": {"seconds": 1432122172, "microseconds": 744001},
// "data": {"status": "running", "id": "snapsave0"}}   <- {"event":
// "STOP",     "timestamp": {"seconds": 1432122372, "microseconds":
// 744001} }   <- {"event": "RESUME",     "timestamp": {"seconds":
// 1432122572, "microseconds": 744001} }   <- {"event":
// "JOB_STATUS_CHANGE",     "timestamp": {"seconds": 1432122772,
// "microseconds": 744001},     "data": {"status": "waiting", "id":
// "snapsave0"}}   <- {"event": "JOB_STATUS_CHANGE",     "timestamp":
// {"seconds": 1432122972, "microseconds": 744001},     "data":
// {"status": "pending", "id": "snapsave0"}}   <- {"event":
// "JOB_STATUS_CHANGE",     "timestamp": {"seconds": 1432123172,
// "microseconds": 744001},     "data": {"status": "concluded", "id":
// "snapsave0"}}   -> {"execute": "query-jobs"}   <- {"return":
// [{"current-progress": 1,           "status": "concluded",
// "total-progress": 1,           "type": "snapshot-save",
// "id": "snapsave0"}]}
//
// Since: 6.0
type SnapshotSaveCommand struct {
	// identifier for the newly created job
	JobId string `json:"job-id"`
	// name of the snapshot to create
	Tag string `json:"tag"`
	// block device node name to save vmstate to
	Vmstate string `json:"vmstate"`
	// list of block device node names to save a snapshot to
	Devices []string `json:"devices"`
}

// Stop guest VM execution.
//
// Since: 0.14
//
// .. note:: This function will succeed even if the guest is already
// in   the stopped state. In "inmigrate" state, it will ensure that
// the   guest remains paused once migration finishes, as if the
// "-S"   option was passed on the command line.    In the
// "suspended" state, it will completely stop the VM and   cause a
// transition to the "paused" state. (Since 9.0)  .. qmp-example::
// -> { "execute": "stop" }   <- { "return": {} }
type StopCommand struct {
}

// Requests that a guest perform a powerdown operation.
//
// Since: 0.14
//
// .. note:: A guest may or may not respond to this command. This
// command returning does not indicate that a guest has accepted the
// request or that it has shut down. Many guests will respond to
// this command by prompting the user in some way.  .. qmp-example::
// -> { "execute": "system_powerdown" }   <- { "return": {} }
type SystemPowerdownCommand struct {
}

// Performs a hard reset of a guest.
//
// Since: 0.14
//
// .. qmp-example::    -> { "execute": "system_reset" }   <- {
// "return": {} }
type SystemResetCommand struct {
}

// Wake up guest from suspend.  If the guest has wake-up from suspend
// support enabled (wakeup-suspend-support flag from query-current-
// machine), wake-up guest from suspend if the guest is in SUSPENDED
// state.  Return an error otherwise.
//
// Since: 1.1
//
// .. note:: Prior to 4.0, this command does nothing in case the guest
// isn't suspended.  .. qmp-example::    -> { "execute":
// "system_wakeup" }   <- { "return": {} }
type SystemWakeupCommand struct {
}

// Query the state of events.
//
// Returns: a list of @TraceEventInfo for the matching events
//
// Since: 2.2
//
// .. qmp-example::    -> { "execute": "trace-event-get-state",
// "arguments": { "name": "qemu_memalign" } }   <- { "return": [ {
// "name": "qemu_memalign", "state": "disabled", "vcpu": false } ] }
type TraceEventGetStateCommand struct {
	// Event name pattern (case-sensitive glob).
	Name string `json:"name"`
}

// Set the dynamic tracing state of events.
//
// Since: 2.2
//
// .. qmp-example::    -> { "execute": "trace-event-set-state",
// "arguments": { "name": "qemu_memalign", "enable": true } }   <- {
// "return": {} }
type TraceEventSetStateCommand struct {
	// Event name pattern (case-sensitive glob).
	Name string `json:"name"`
	// Whether to enable tracing.
	Enable bool `json:"enable"`
	// Do not match unavailable events with @name.
	IgnoreUnavailable *bool `json:"ignore-unavailable,omitempty"`
}

// Executes a number of transactionable QMP commands atomically.  If
// any operation fails, then the entire set of actions will be
// abandoned and the appropriate error returned.  For external
// snapshots, the dictionary contains the device, the file to use for
// the new snapshot, and the format.  The default format, if not
// specified, is qcow2.  Each new snapshot defaults to being created
// by QEMU (wiping any contents if the file already exists), but it is
// also possible to reuse an externally-created file.  In the latter
// case, you should ensure that the new image file has the same
// contents as the current one; QEMU cannot perform any meaningful
// check.  Typically this is achieved by using the current image file
// as the backing file for the new image.  On failure, the original
// disks pre-snapshot attempt will be used.  For internal snapshots,
// the dictionary contains the device and the snapshot's name.  If an
// internal snapshot matching name already exists, the request will be
// rejected.  Only some image formats support it, for example, qcow2,
// and rbd,  On failure, qemu will try delete the newly created
// internal snapshot in the transaction.  When an I/O error occurs
// during deletion, the user needs to fix it later with qemu-img or
// other command.
//
// Errors:   - Any errors from commands in the transaction
//
// .. note:: The transaction aborts on the first failure. Therefore,
// there will be information on only one failed operation returned
// in an error condition, and subsequent actions will not have been
// attempted.
//
// Since: 1.1
//
// .. qmp-example::    -> { "execute": "transaction",
// "arguments": { "actions": [        { "type": "blockdev-snapshot-
// sync", "data" : { "device": "ide-hd0",
// "snapshot-file": "/some/place/my-image",
// "format": "qcow2" } },        { "type": "blockdev-snapshot-sync",
// "data" : { "node-name": "myfile",                      "snapshot-
// file": "/some/place/my-image2",                      "snapshot-
// node-name": "node3432",                      "mode": "existing",
// "format": "qcow2" } },        { "type": "blockdev-snapshot-sync",
// "data" : { "device": "ide-hd1",                      "snapshot-
// file": "/some/place/my-image2",                      "mode":
// "existing",                      "format": "qcow2" } },        {
// "type": "blockdev-snapshot-internal-sync", "data" : {
// "device": "ide-hd2",                      "name": "snapshot0" } } ]
// } }   <- { "return": {} }
type TransactionCommand struct {
	// List of @TransactionAction; information needed for the
	// respective operations.
	Actions []TransactionAction `json:"actions"`
	// structure of additional options to control the execution of the
	// transaction. See @TransactionProperties for additional detail.
	Properties *TransactionProperties `json:"properties,omitempty"`
}

// Set watchdog action.
//
// Since: 2.11
//
// .. qmp-example::    -> { "execute": "watchdog-set-action",
// "arguments": { "action": "inject-nmi" } }   <- { "return": {} }
type WatchdogSetActionCommand struct {
	// @WatchdogAction action taken when watchdog timer expires.
	Action WatchdogAction `json:"action"`
}

// Starts a job to amend format specific options of an existing open
// block device The job is automatically finalized, but a manual job-
// dismiss is required.
//
// Since: 5.1
type XBlockdevAmendCommand struct {
	// Identifier for the newly created job.
	JobId string `json:"job-id"`
	// Name of the block node to work on
	NodeName string `json:"node-name"`
	// Options (driver specific)
	Options BlockdevAmendOptions `json:"options"`
	// Allow unsafe operations, format specific For luks that allows
	// erase of the last active keyslot (permanent loss of data), and
	// replacement of an active keyslot (possible loss of data if IO
	// error happens)
	Force *bool `json:"force,omitempty"`
}

// Dynamically reconfigure the block driver state graph.  It can be
// used to add, remove, insert or replace a graph node.  Currently
// only the Quorum driver implements this feature to add or remove its
// child.  This is useful to fix a broken quorum child.  If @node is
// specified, it will be inserted under @parent.  @child may not be
// specified in this case.  If both @parent and @child are specified
// but @node is not, @child will be detached from @parent.
//
// Since: 2.7
//
// .. qmp-example::   :title: Add a new node to a quorum    -> {
// "execute": "blockdev-add",      "arguments": {        "driver":
// "raw",        "node-name": "new_node",        "file": { "driver":
// "file",             "filename": "test.raw" } } }   <- { "return":
// {} }   -> { "execute": "x-blockdev-change",      "arguments": {
// "parent": "disk1",             "node": "new_node" } }   <- {
// "return": {} }  .. qmp-example::   :title: Delete a quorum's node
// -> { "execute": "x-blockdev-change",      "arguments": { "parent":
// "disk1",             "child": "children.1" } }   <- { "return": {}
// }
type XBlockdevChangeCommand struct {
	// the id or name of the parent node.
	Parent string `json:"parent"`
	// the name of a child under the given parent node.
	Child *string `json:"child,omitempty"`
	// the name of the node that will be added.
	Node *string `json:"node,omitempty"`
}

// Move @node and its children into the @iothread.  If @iothread is
// null then move @node and its children into the main loop.  The node
// must not be attached to a BlockBackend.
//
// Since: 2.12
//
// .. qmp-example::   :title: Move a node into an IOThread    -> {
// "execute": "x-blockdev-set-iothread",      "arguments": { "node-
// name": "disk1",             "iothread": "iothread0" } }   <- {
// "return": {} }  .. qmp-example::   :title: Move a node into the
// main loop    -> { "execute": "x-blockdev-set-iothread",
// "arguments": { "node-name": "disk1",             "iothread": null }
// }   <- { "return": {} }
type XBlockdevSetIothreadCommand struct {
	// the name of the block driver node
	NodeName string `json:"node-name"`
	// the name of the IOThread object or null for the main loop
	Iothread StrOrNull `json:"iothread"`
	// true if the node and its children should be moved when a
	// BlockBackend is already attached
	Force *bool `json:"force,omitempty"`
}

// Tell qemu that heartbeat is lost, request it to do takeover
// procedures.  If this command is sent to the PVM, the Primary side
// will exit COLO mode.  If sent to the Secondary, the Secondary side
// will run failover work, then takes over server operation to become
// the service VM.
//
// Since: 2.8
//
// .. qmp-example::    -> { "execute": "x-colo-lost-heartbeat" }   <-
// { "return": {} }
type XColoLostHeartbeatCommand struct {
}

// Get bitmap SHA256.
//
// Returns:   BlockDirtyBitmapSha256
//
// Errors:   - If @node is not a valid block device, DeviceNotFound
// - If @name is not found or if hashing has failed, GenericError
//
// Since: 2.10
type XDebugBlockDirtyBitmapSha256Command struct {
	BlockDirtyBitmap
}

// Get the block graph.
//
// Since: 4.0
type XDebugQueryBlockGraphCommand struct {
}

// Exit from "preconfig" state  This command makes QEMU exit the
// preconfig state and proceed with VM initialization using
// configuration data provided on the command line and via the QMP
// monitor during the preconfig state.  The command is only available
// during the preconfig state (i.e. when the --preconfig command line
// option was in use).
//
// Since: 3.0
//
// .. qmp-example::    -> { "execute": "x-exit-preconfig" }   <- {
// "return": {} }
type XExitPreconfigCommand struct {
}

// Query information on interrupt controller devices
//
// Returns: Interrupt controller devices information
//
// Since: 9.1
type XQueryInterruptControllersCommand struct {
}

// Query interrupt statistics
//
// Returns: interrupt statistics
//
// Since: 6.2
type XQueryIrqCommand struct {
}

// Query TCG compiler statistics
//
// Returns: TCG compiler statistics
//
// Since: 6.2
type XQueryJitCommand struct {
}

// Query NUMA topology information
//
// Returns: topology information
//
// Since: 6.2
type XQueryNumaCommand struct {
}

// Query TCG opcode counters
//
// Returns: TCG opcode counters
//
// Since: 6.2
type XQueryOpcountCommand struct {
}

// Query system ramblock information
//
// Returns: system ramblock information
//
// Since: 6.2
type XQueryRamblockCommand struct {
}

// Query information on the registered ROMS
//
// Returns: registered ROMs
//
// Since: 6.2
type XQueryRomsCommand struct {
}

// Query information on the USB devices
//
// Returns: USB device information
//
// Since: 6.2
type XQueryUsbCommand struct {
}

// Returns a list of all realized VirtIODevices
//
// Returns: List of gathered VirtIODevices
//
// Since: 7.2
//
// .. qmp-example::    -> { "execute": "x-query-virtio" }   <- {
// "return": [        {          "name": "virtio-input",
// "path": "/machine/peripheral-anon/device[4]/virtio-backend"
// },        {          "name": "virtio-crypto",          "path":
// "/machine/peripheral/crypto0/virtio-backend"        },        {
// "name": "virtio-scsi",          "path": "/machine/peripheral-
// anon/device[2]/virtio-backend"        },        {          "name":
// "virtio-net",          "path": "/machine/peripheral-
// anon/device[1]/virtio-backend"        },        {          "name":
// "virtio-serial",          "path": "/machine/peripheral-
// anon/device[0]/virtio-backend"        }      ]     }
type XQueryVirtioCommand struct {
}

// Return the information about a VirtQueue's VirtQueueElement
//
// Returns: VirtioQueueElement information
//
// Since: 7.2
//
// .. qmp-example::   :title: Introspect on virtio-net's VirtQueue 0
// at index 5    -> { "execute": "x-query-virtio-queue-element",
// "arguments": { "path": "/machine/peripheral-anon/device[1]/virtio-
// backend",             "queue": 0,             "index": 5 }     }
// <- { "return": {        "index": 5,        "name": "virtio-net",
// "descs": [          {            "flags": ["write"],
// "len": 1536,            "addr": 5257305600          }        ],
// "avail": {          "idx": 256,          "flags": 0,
// "ring": 5        },        "used": {          "idx": 13,
// "flags": 0        }      }     }  .. qmp-example::   :title:
// Introspect on virtio-crypto's VirtQueue 1 at head    -> {
// "execute": "x-query-virtio-queue-element",      "arguments": {
// "path": "/machine/peripheral/crypto0/virtio-backend",
// "queue": 1 }     }   <- { "return": {        "index": 0,
// "name": "virtio-crypto",        "descs": [          {
// "flags": [],            "len": 0,            "addr":
// 8080268923184214134          }        ],        "avail": {
// "idx": 280,          "flags": 0,          "ring": 0        },
// "used": {          "idx": 280,          "flags": 0        }      }
// }  .. qmp-example::   :title: Introspect on virtio-scsi's VirtQueue
// 2 at head    -> { "execute": "x-query-virtio-queue-element",
// "arguments": { "path": "/machine/peripheral-anon/device[2]/virtio-
// backend",             "queue": 2 }     }   <- { "return": {
// "index": 19,        "name": "virtio-scsi",        "descs": [
// {            "flags": ["used", "indirect", "write"],
// "len": 4099327944,            "addr": 12055409292258155293
// }        ],        "avail": {          "idx": 1147,
// "flags": 0,          "ring": 19        },        "used": {
// "idx": 280,          "flags": 0        }      }     }
type XQueryVirtioQueueElementCommand struct {
	// VirtIODevice canonical QOM path
	Path string `json:"path"`
	// VirtQueue index to examine
	Queue uint16 `json:"queue"`
	// Index of the element in the queue (default: head of the queue)
	Index *uint16 `json:"index,omitempty"`
}

// Return the status of a given VirtIODevice's VirtQueue
//
// Returns: VirtQueueStatus of the VirtQueue
//
// .. note:: last_avail_idx will not be displayed in the case where
// the   selected VirtIODevice has a running vhost device and the
// VirtIODevice VirtQueue index (queue) does not exist for the
// corresponding vhost device vhost_virtqueue. Also,
// shadow_avail_idx will not be displayed in the case where the
// selected VirtIODevice has a running vhost device.
//
// Since: 7.2
//
// .. qmp-example::   :annotated:    Get VirtQueueStatus for virtio-
// vsock (vhost-vsock running)   ::    -> { "execute": "x-query-
// virtio-queue-status",      "arguments": { "path":
// "/machine/peripheral/vsock0/virtio-backend",             "queue": 1
// }     }   <- { "return": {        "signalled-used": 0,
// "inuse": 0,        "name": "vhost-vsock",        "vring-align":
// 4096,        "vring-desc": 5217370112,        "signalled-used-
// valid": false,        "vring-num-default": 128,        "vring-
// avail": 5217372160,        "queue-index": 1,        "last-avail-
// idx": 0,        "vring-used": 5217372480,        "used-idx": 0,
// "vring-num": 128      }     }  .. qmp-example::   :annotated:
// Get VirtQueueStatus for virtio-serial (no vhost)   ::    -> {
// "execute": "x-query-virtio-queue-status",      "arguments": {
// "path": "/machine/peripheral-anon/device[0]/virtio-backend",
// "queue": 20 }     }   <- { "return": {        "signalled-used": 0,
// "inuse": 0,        "name": "virtio-serial",        "vring-align":
// 4096,        "vring-desc": 5182074880,        "signalled-used-
// valid": false,        "vring-num-default": 128,        "vring-
// avail": 5182076928,        "queue-index": 20,        "last-avail-
// idx": 0,        "vring-used": 5182077248,        "used-idx": 0,
// "shadow-avail-idx": 0,        "vring-num": 128      }     }
type XQueryVirtioQueueStatusCommand struct {
	// VirtIODevice canonical QOM path
	Path string `json:"path"`
	// VirtQueue index to examine
	Queue uint16 `json:"queue"`
}

// Poll for a comprehensive status of a given virtio device
//
// Returns: VirtioStatus of the virtio device
//
// Since: 7.2
//
// .. qmp-example::   :annotated:    Poll for the status of virtio-
// crypto (no vhost-crypto active)   ::    -> { "execute": "x-query-
// virtio-status",      "arguments": { "path":
// "/machine/peripheral/crypto0/virtio-backend" }     }   <- {
// "return": {        "device-endian": "little",        "bus-name":
// "",        "disable-legacy-check": false,        "name": "virtio-
// crypto",        "started": true,        "device-id": 20,
// "backend-features": {          "transports": [],          "dev-
// features": []        },        "start-on-kick": false,
// "isr": 1,        "broken": false,        "status": {
// "statuses": [            "VIRTIO_CONFIG_S_ACKNOWLEDGE: Valid virtio
// device found",            "VIRTIO_CONFIG_S_DRIVER: Guest OS
// compatible with device",            "VIRTIO_CONFIG_S_FEATURES_OK:
// Feature negotiation complete",
// "VIRTIO_CONFIG_S_DRIVER_OK: Driver setup and ready"          ]
// },        "num-vqs": 2,        "guest-features": {          "dev-
// features": [],          "transports": [
// "VIRTIO_RING_F_EVENT_IDX: Used & avail. event fields enabled",
// "VIRTIO_RING_F_INDIRECT_DESC: Indirect descriptors supported",
// "VIRTIO_F_VERSION_1: Device compliant for v1 spec (legacy)"
// ]        },        "host-features": {          "unknown-dev-
// features": 1073741824,          "dev-features": [],
// "transports": [            "VIRTIO_RING_F_EVENT_IDX: Used & avail.
// event fields enabled",            "VIRTIO_RING_F_INDIRECT_DESC:
// Indirect descriptors supported",            "VIRTIO_F_VERSION_1:
// Device compliant for v1 spec (legacy)",
// "VIRTIO_F_ANY_LAYOUT: Device accepts arbitrary desc. layouts",
// "VIRTIO_F_NOTIFY_ON_EMPTY: Notify when device runs out of avail.
// descs. on VQ"          ]        },        "use-guest-notifier-
// mask": true,        "vm-running": true,        "queue-sel": 1,
// "disabled": false,        "vhost-started": false,        "use-
// started": true      }     }  .. qmp-example::   :annotated:    Poll
// for the status of virtio-net (vhost-net is active)   ::    -> {
// "execute": "x-query-virtio-status",      "arguments": { "path":
// "/machine/peripheral-anon/device[1]/virtio-backend" }     }   <- {
// "return": {        "device-endian": "little",        "bus-name":
// "",        "disabled-legacy-check": false,        "name": "virtio-
// net",        "started": true,        "device-id": 1,        "vhost-
// dev": {          "n-tmp-sections": 4,          "n-mem-sections": 4,
// "max-queues": 1,          "backend-cap": 2,          "log-size": 0,
// "backend-features": {            "dev-features": [],
// "transports": []          },          "nvqs": 2,
// "protocol-features": {            "protocols": []          },
// "vq-index": 0,          "log-enabled": false,          "acked-
// features": {            "dev-features": [
// "VIRTIO_NET_F_MRG_RXBUF: Driver can merge receive buffers"
// ],            "transports": [
// "VIRTIO_RING_F_EVENT_IDX: Used & avail. event fields enabled",
// "VIRTIO_RING_F_INDIRECT_DESC: Indirect descriptors supported",
// "VIRTIO_F_VERSION_1: Device compliant for v1 spec (legacy)"
// ]          },          "features": {            "dev-features": [
// "VHOST_F_LOG_ALL: Logging write descriptors supported",
// "VIRTIO_NET_F_MRG_RXBUF: Driver can merge receive buffers"
// ],            "transports": [
// "VIRTIO_RING_F_EVENT_IDX: Used & avail. event fields enabled",
// "VIRTIO_RING_F_INDIRECT_DESC: Indirect descriptors supported",
// "VIRTIO_F_IOMMU_PLATFORM: Device can be used on IOMMU platform",
// "VIRTIO_F_VERSION_1: Device compliant for v1 spec (legacy)",
// "VIRTIO_F_ANY_LAYOUT: Device accepts arbitrary desc. layouts",
// "VIRTIO_F_NOTIFY_ON_EMPTY: Notify when device runs out of avail.
// descs. on VQ"            ]          }        },        "backend-
// features": {          "dev-features": [
// "VHOST_USER_F_PROTOCOL_FEATURES: Vhost-user protocol features
// negotiation supported",            "VIRTIO_NET_F_GSO: Handling GSO-
// type packets supported",            "VIRTIO_NET_F_CTRL_MAC_ADDR:
// MAC address set through control channel",
// "VIRTIO_NET_F_GUEST_ANNOUNCE: Driver sending gratuitous packets
// supported",            "VIRTIO_NET_F_CTRL_RX_EXTRA: Extra RX mode
// control supported",            "VIRTIO_NET_F_CTRL_VLAN: Control
// channel VLAN filtering supported",
// "VIRTIO_NET_F_CTRL_RX: Control channel RX mode supported",
// "VIRTIO_NET_F_CTRL_VQ: Control channel available",
// "VIRTIO_NET_F_STATUS: Configuration status field available",
// "VIRTIO_NET_F_MRG_RXBUF: Driver can merge receive buffers",
// "VIRTIO_NET_F_HOST_UFO: Device can receive UFO",
// "VIRTIO_NET_F_HOST_ECN: Device can receive TSO with ECN",
// "VIRTIO_NET_F_HOST_TSO6: Device can receive TSOv6",
// "VIRTIO_NET_F_HOST_TSO4: Device can receive TSOv4",
// "VIRTIO_NET_F_GUEST_UFO: Driver can receive UFO",
// "VIRTIO_NET_F_GUEST_ECN: Driver can receive TSO with ECN",
// "VIRTIO_NET_F_GUEST_TSO6: Driver can receive TSOv6",
// "VIRTIO_NET_F_GUEST_TSO4: Driver can receive TSOv4",
// "VIRTIO_NET_F_MAC: Device has given MAC address",
// "VIRTIO_NET_F_CTRL_GUEST_OFFLOADS: Control channel offloading
// reconfig. supported",            "VIRTIO_NET_F_GUEST_CSUM: Driver
// handling packets with partial checksum supported",
// "VIRTIO_NET_F_CSUM: Device handling packets with partial checksum
// supported"          ],          "transports": [
// "VIRTIO_RING_F_EVENT_IDX: Used & avail. event fields enabled",
// "VIRTIO_RING_F_INDIRECT_DESC: Indirect descriptors supported",
// "VIRTIO_F_VERSION_1: Device compliant for v1 spec (legacy)",
// "VIRTIO_F_ANY_LAYOUT: Device accepts arbitrary desc. layouts",
// "VIRTIO_F_NOTIFY_ON_EMPTY: Notify when device runs out of avail.
// descs. on VQ"          ]        },        "start-on-kick": false,
// "isr": 1,        "broken": false,        "status": {
// "statuses": [            "VIRTIO_CONFIG_S_ACKNOWLEDGE: Valid virtio
// device found",            "VIRTIO_CONFIG_S_DRIVER: Guest OS
// compatible with device",            "VIRTIO_CONFIG_S_FEATURES_OK:
// Feature negotiation complete",
// "VIRTIO_CONFIG_S_DRIVER_OK: Driver setup and ready"          ]
// },        "num-vqs": 3,        "guest-features": {          "dev-
// features": [            "VIRTIO_NET_F_CTRL_MAC_ADDR: MAC address
// set through control channel",
// "VIRTIO_NET_F_GUEST_ANNOUNCE: Driver sending gratuitous packets
// supported",            "VIRTIO_NET_F_CTRL_VLAN: Control channel
// VLAN filtering supported",            "VIRTIO_NET_F_CTRL_RX:
// Control channel RX mode supported",
// "VIRTIO_NET_F_CTRL_VQ: Control channel available",
// "VIRTIO_NET_F_STATUS: Configuration status field available",
// "VIRTIO_NET_F_MRG_RXBUF: Driver can merge receive buffers",
// "VIRTIO_NET_F_HOST_UFO: Device can receive UFO",
// "VIRTIO_NET_F_HOST_ECN: Device can receive TSO with ECN",
// "VIRTIO_NET_F_HOST_TSO6: Device can receive TSOv6",
// "VIRTIO_NET_F_HOST_TSO4: Device can receive TSOv4",
// "VIRTIO_NET_F_GUEST_UFO: Driver can receive UFO",
// "VIRTIO_NET_F_GUEST_ECN: Driver can receive TSO with ECN",
// "VIRTIO_NET_F_GUEST_TSO6: Driver can receive TSOv6",
// "VIRTIO_NET_F_GUEST_TSO4: Driver can receive TSOv4",
// "VIRTIO_NET_F_MAC: Device has given MAC address",
// "VIRTIO_NET_F_CTRL_GUEST_OFFLOADS: Control channel offloading
// reconfig. supported",            "VIRTIO_NET_F_GUEST_CSUM: Driver
// handling packets with partial checksum supported",
// "VIRTIO_NET_F_CSUM: Device handling packets with partial checksum
// supported"          ],          "transports": [
// "VIRTIO_RING_F_EVENT_IDX: Used & avail. event fields enabled",
// "VIRTIO_RING_F_INDIRECT_DESC: Indirect descriptors supported",
// "VIRTIO_F_VERSION_1: Device compliant for v1 spec (legacy)"
// ]        },        "host-features": {          "dev-features": [
// "VHOST_USER_F_PROTOCOL_FEATURES: Vhost-user protocol features
// negotiation supported",            "VIRTIO_NET_F_GSO: Handling GSO-
// type packets supported",            "VIRTIO_NET_F_CTRL_MAC_ADDR:
// MAC address set through control channel",
// "VIRTIO_NET_F_GUEST_ANNOUNCE: Driver sending gratuitous packets
// supported",            "VIRTIO_NET_F_CTRL_RX_EXTRA: Extra RX mode
// control supported",            "VIRTIO_NET_F_CTRL_VLAN: Control
// channel VLAN filtering supported",
// "VIRTIO_NET_F_CTRL_RX: Control channel RX mode supported",
// "VIRTIO_NET_F_CTRL_VQ: Control channel available",
// "VIRTIO_NET_F_STATUS: Configuration status field available",
// "VIRTIO_NET_F_MRG_RXBUF: Driver can merge receive buffers",
// "VIRTIO_NET_F_HOST_UFO: Device can receive UFO",
// "VIRTIO_NET_F_HOST_ECN: Device can receive TSO with ECN",
// "VIRTIO_NET_F_HOST_TSO6: Device can receive TSOv6",
// "VIRTIO_NET_F_HOST_TSO4: Device can receive TSOv4",
// "VIRTIO_NET_F_GUEST_UFO: Driver can receive UFO",
// "VIRTIO_NET_F_GUEST_ECN: Driver can receive TSO with ECN",
// "VIRTIO_NET_F_GUEST_TSO6: Driver can receive TSOv6",
// "VIRTIO_NET_F_GUEST_TSO4: Driver can receive TSOv4",
// "VIRTIO_NET_F_MAC: Device has given MAC address",
// "VIRTIO_NET_F_CTRL_GUEST_OFFLOADS: Control channel offloading
// reconfig. supported",            "VIRTIO_NET_F_GUEST_CSUM: Driver
// handling packets with partial checksum supported",
// "VIRTIO_NET_F_CSUM: Device handling packets with partial checksum
// supported"          ],          "transports": [
// "VIRTIO_RING_F_EVENT_IDX: Used & avail. event fields enabled",
// "VIRTIO_RING_F_INDIRECT_DESC: Indirect descriptors supported",
// "VIRTIO_F_VERSION_1: Device compliant for v1 spec (legacy)",
// "VIRTIO_F_ANY_LAYOUT: Device accepts arbitrary desc. layouts",
// "VIRTIO_F_NOTIFY_ON_EMPTY: Notify when device runs out of avail.
// descs. on VQ"         ]        },        "use-guest-notifier-mask":
// true,        "vm-running": true,        "queue-sel": 2,
// "disabled": false,        "vhost-started": true,        "use-
// started": true      }     }
type XQueryVirtioStatusCommand struct {
	// Canonical QOM path of the VirtIODevice
	Path string `json:"path"`
}

// Return information of a given vhost device's vhost_virtqueue
//
// Returns: VirtVhostQueueStatus of the vhost_virtqueue
//
// Since: 7.2
//
// .. qmp-example::   :title: Get vhost_virtqueue status for vhost-
// crypto    -> { "execute": "x-query-virtio-vhost-queue-status",
// "arguments": { "path": "/machine/peripheral/crypto0/virtio-
// backend",             "queue": 0 }     }   <- { "return": {
// "avail-phys": 5216124928,        "name": "virtio-crypto",
// "used-phys": 5216127040,        "avail-size": 2054,        "desc-
// size": 16384,        "used-size": 8198,        "desc":
// 140141447430144,        "num": 1024,        "call": 0,
// "avail": 140141447446528,        "desc-phys": 5216108544,
// "used": 140141447448640,        "kick": 0      }     }  .. qmp-
// example::   :title: Get vhost_virtqueue status for vhost-vsock
// -> { "execute": "x-query-virtio-vhost-queue-status",
// "arguments": { "path": "/machine/peripheral/vsock0/virtio-backend",
// "queue": 0 }     }   <- { "return": {        "avail-phys":
// 5182261248,        "name": "vhost-vsock",        "used-phys":
// 5182261568,        "avail-size": 262,        "desc-size": 2048,
// "used-size": 1030,        "desc": 140141413580800,        "num":
// 128,        "call": 0,        "avail": 140141413582848,
// "desc-phys": 5182259200,        "used": 140141413583168,
// "kick": 0      }     }
type XQueryVirtioVhostQueueStatusCommand struct {
	// VirtIODevice canonical QOM path
	Path string `json:"path"`
	// vhost_virtqueue index to examine
	Queue uint16 `json:"queue"`
}

// Xen uses this command to notify replication to trigger a
// checkpoint.  .. qmp-example::      -> { "execute": "xen-colo-do-
// checkpoint" }     <- { "return": {} }
//
// Since: 2.9
type XenColoDoCheckpointCommand struct {
}

// Inject a Xen event channel port (interrupt) to the guest.
//
// Since: 8.0
//
// .. qmp-example::    -> { "execute": "xen-event-inject",
// "arguments": { "port": 1 } }   <- { "return": { } }
type XenEventInjectCommand struct {
	// The port number
	Port uint32 `json:"port"`
}

// Query the Xen event channels opened by the guest.
//
// Returns: list of open event channel ports.
//
// Since: 8.0
//
// .. qmp-example::    -> { "execute": "xen-event-list" }   <- {
// "return": [       {         "pending": false,         "port": 1,
// "vcpu": 1,         "remote-domain": "qemu",         "masked":
// false,         "type": "interdomain",         "target": 1       },
// {         "pending": false,         "port": 2,         "vcpu": 0,
// "remote-domain": "",         "masked": false,         "type":
// "virq",         "target": 0       }      ]     }
type XenEventListCommand struct {
}

// Load the state of all devices from file.  The RAM and the block
// devices of the VM are not loaded by this command.
//
// Since: 2.7
//
// .. qmp-example::    -> { "execute": "xen-load-devices-state",
// "arguments": { "filename": "/tmp/resume" } }   <- { "return": {} }
type XenLoadDevicesStateCommand struct {
	// the file to load the state of the devices from as binary data.
	// See xen-save-devices-state.txt for a description of the binary
	// format.
	Filename string `json:"filename"`
}

// Save the state of all devices to file.  The RAM and the block
// devices of the VM are not saved by this command.
//
// Since: 1.1
//
// .. qmp-example::    -> { "execute": "xen-save-devices-state",
// "arguments": { "filename": "/tmp/save" } }   <- { "return": {} }
type XenSaveDevicesStateCommand struct {
	// the file to save the state of the devices to as binary data.
	// See xen-save-devices-state.txt for a description of the binary
	// format.
	Filename string `json:"filename"`
	// Optional argument to ask QEMU to treat this command as part of
	// a live migration. Default to true. (since 2.11)
	Live *bool `json:"live,omitempty"`
}

// Enable or disable the global dirty log mode.
//
// Since: 1.3
//
// .. qmp-example::    -> { "execute": "xen-set-global-dirty-log",
// "arguments": { "enable": true } }   <- { "return": {} }
type XenSetGlobalDirtyLogCommand struct {
	// true to enable, false to disable.
	Enable bool `json:"enable"`
}

// Enable or disable replication.
//
// .. qmp-example::    -> { "execute": "xen-set-replication",
// "arguments": {"enable": true, "primary": false} }   <- { "return":
// {} }
//
// Since: 2.9
type XenSetReplicationCommand struct {
	// true to enable, false to disable.
	Enable bool `json:"enable"`
	// true for primary or false for secondary.
	Primary bool `json:"primary"`
	// true to do failover, false to stop. Cannot be specified if
	// 'enable' is true. Default value is false.
	Failover *bool `json:"failover,omitempty"`
}

// Try to recover from hanging QEMU by yanking the specified
// instances. See @YankInstance for more information.
//
// Errors:   - If any of the YankInstances doesn't exist,
// DeviceNotFound
//
// .. qmp-example::    -> { "execute": "yank",      "arguments": {
// "instances": [          { "type": "block-node",           "node-
// name": "nbd0" }        ] } }   <- { "return": {} }
//
// Since: 6.0
type YankCommand struct {
	// the instances to be yanked
	Instances []YankInstance `json:"instances"`
}
