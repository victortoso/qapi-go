/*
 * Copyright 2025 Red Hat, Inc.
 * SPDX-License-Identifier: (MIT-0 and GPL-2.0-or-later)
 */

/****************************************************************************
 * THIS CODE HAS BEEN GENERATED. DO NOT CHANGE IT DIRECTLY                  *
 ****************************************************************************/
package qapi

import "encoding/json"

// OSPM Status Indication for a device For description of possible
// values of @source and @status fields see "_OST (OSPM Status
// Indication)" chapter of ACPI5.0 spec.
//
// Since: 2.1
type ACPIOSTInfo struct {
	// device ID associated with slot
	Device *string `json:"device,omitempty"`
	// slot ID, unique per slot of a given @slot-type
	Slot string `json:"slot"`
	// type of the slot
	SlotType ACPISlotType `json:"slot-type"`
	// an integer containing the source event
	Source int64 `json:"source"`
	// an integer containing the status code
	Status int64 `json:"status"`
}

// This action can be used to test transaction failure.
//
// Since: 1.6
type Abort struct {
}

// Since: 1.6
type AbortWrapper struct {
	Data Abort `json:"data"`
}

// Properties for acpi-generic-initiator objects.
//
// Since: 9.0
type AcpiGenericInitiatorProperties struct {
	// PCI device ID to be associated with the node
	PciDev string `json:"pci-dev"`
	// NUMA node associated with the PCI device
	Node uint32 `json:"node"`
}

// Properties for acpi-generic-port objects.
//
// Since: 9.2
type AcpiGenericPortProperties struct {
	// QOM path of the PCI bus of the hostbridge associated with this
	// SRAT Generic Port Affinity Structure. This is the same as the
	// bus parameter for the root ports attached to this host bridge.
	// The resulting SRAT Generic Port Affinity Structure will refer
	// to the ACPI object in DSDT that represents the host bridge
	// (e.g. ACPI0016 for CXL host bridges). See ACPI 6.5 Section
	// 5.2.16.7 for more information.
	PciBus string `json:"pci-bus"`
	// Similar to a NUMA node ID, but instead of providing a reference
	// point used for defining NUMA distances and access
	// characteristics to memory or from an initiator (e.g. CPU), this
	// node defines the boundary point between non-discoverable system
	// buses which must be described by firmware, and a discoverable
	// bus. NUMA distances and access characteristics are defined to
	// and from that point. For system software to establish full
	// initiator to target characteristics this information must be
	// combined with information retrieved from the discoverable part
	// of the path. An example would use CDAT (see UEFI.org)
	// information read from devices and switches in conjunction with
	// link characteristics read from PCIe Configuration space. To get
	// the full path latency from CPU to CXL attached DRAM CXL device:
	// Add the latency from CPU to Generic Port (from HMAT indexed via
	// the the node ID in this SRAT structure) to that for CXL bus
	// links, the latency across intermediate switches and from the EP
	// port to the actual memory. Bandwidth is more complex as there
	// may be interleaving across multiple devices and shared links in
	// the path.
	Node uint32 `json:"node"`
}

// Specify an ACPI table on the command line to load.  At most one of
// @file and @data can be specified.  The list of files specified by
// any one of them is loaded and concatenated in order. If both are
// omitted, @data is implied.  Other fields / optargs can be used to
// override fields of the generic ACPI table header; refer to the ACPI
// specification 5.0, section 5.2.6 System Description Table Header.
// If a header field is not overridden, then the corresponding value
// from the concatenated blob is used (in case of @file), or it is
// filled in with a hard-coded value (in case of @data).  String
// fields are copied into the matching ACPI member from lowest address
// upwards, and silently truncated / NUL-padded to length.
//
// Since: 1.5
type AcpiTableOptions struct {
	// table signature / identifier (4 bytes)
	Sig *string `json:"sig,omitempty"`
	// table revision number (dependent on signature, 1 byte)
	Rev *uint8 `json:"rev,omitempty"`
	// OEM identifier (6 bytes)
	OemId *string `json:"oem_id,omitempty"`
	// OEM table identifier (8 bytes)
	OemTableId *string `json:"oem_table_id,omitempty"`
	// OEM-supplied revision number (4 bytes)
	OemRev *uint32 `json:"oem_rev,omitempty"`
	// identifier of the utility that created the table (4 bytes)
	AslCompilerId *string `json:"asl_compiler_id,omitempty"`
	// revision number of the utility that created the table (4 bytes)
	AslCompilerRev *uint32 `json:"asl_compiler_rev,omitempty"`
	// colon (:) separated list of pathnames to load and concatenate
	// as table data. The resultant binary blob is expected to have an
	// ACPI table header. At least one file is required. This field
	// excludes @data.
	File *string `json:"file,omitempty"`
	// colon (:) separated list of pathnames to load and concatenate
	// as table data. The resultant binary blob must not have an ACPI
	// table header. At least one file is required. This field
	// excludes @file.
	Data *string `json:"data,omitempty"`
}

// Information about a file descriptor that was added to an fd set.
//
// Since: 1.2
type AddfdInfo struct {
	// The ID of the fd set that @fd was added to.
	FdsetId int64 `json:"fdset-id"`
	// The file descriptor that was received via SCM rights and added
	// to the fd set.
	Fd int64 `json:"fd"`
}

// Parameters for self-announce timers
//
// Since: 4.0
type AnnounceParameters struct {
	// Initial delay (in ms) before sending the first GARP/RARP
	// announcement
	Initial int64 `json:"initial"`
	// Maximum delay (in ms) between GARP/RARP announcement packets
	Max int64 `json:"max"`
	// Number of self-announcement attempts
	Rounds int64 `json:"rounds"`
	// Delay increase (in ms) after each self-announcement attempt
	Step int64 `json:"step"`
	// An optional list of interface names, which restricts the
	// announcement to the listed interfaces. (Since 4.1)
	Interfaces []string `json:"interfaces,omitempty"`
	// A name to be used to identify an instance of announce-timers
	// and to allow it to modified later. Not for use as part of the
	// migration parameters. (Since 4.1)
	Id *string `json:"id,omitempty"`
}

// Options of the ALSA audio backend.
//
// Since: 4.0
type AudiodevAlsaOptions struct {
	// options of the capture stream
	In *AudiodevAlsaPerDirectionOptions `json:"in,omitempty"`
	// options of the playback stream
	Out *AudiodevAlsaPerDirectionOptions `json:"out,omitempty"`
	// set the threshold (in microseconds) when playback starts
	Threshold *uint32 `json:"threshold,omitempty"`
}

// Options of the ALSA backend that are used for both playback and
// recording.
//
// Since: 4.0
type AudiodevAlsaPerDirectionOptions struct {
	// use QEMU's mixing engine to mix all streams inside QEMU and
	// convert audio formats when not supported by the backend. When
	// set to off, fixed-settings must be also off (default on, since
	// 4.2)
	MixingEngine *bool `json:"mixing-engine,omitempty"`
	// use fixed settings for host input/output. When off, frequency,
	// channels and format must not be specified (default true)
	FixedSettings *bool `json:"fixed-settings,omitempty"`
	// frequency to use when using fixed settings (default 44100)
	Frequency *uint32 `json:"frequency,omitempty"`
	// number of channels when using fixed settings (default 2)
	Channels *uint32 `json:"channels,omitempty"`
	// number of voices to use (default 1)
	Voices *uint32 `json:"voices,omitempty"`
	// sample format to use when using fixed settings (default s16)
	Format *AudioFormat `json:"format,omitempty"`
	// the buffer length in microseconds
	BufferLength *uint32 `json:"buffer-length,omitempty"`
	// the name of the ALSA device to use (default 'default')
	Dev *string `json:"dev,omitempty"`
	// the period length in microseconds
	PeriodLength *uint32 `json:"period-length,omitempty"`
	// attempt to use poll mode, falling back to non-polling access on
	// failure (default true)
	TryPoll *bool `json:"try-poll,omitempty"`
}

// Options of the coreaudio audio backend.
//
// Since: 4.0
type AudiodevCoreaudioOptions struct {
	// options of the capture stream
	In *AudiodevCoreaudioPerDirectionOptions `json:"in,omitempty"`
	// options of the playback stream
	Out *AudiodevCoreaudioPerDirectionOptions `json:"out,omitempty"`
}

// Options of the Core Audio backend that are used for both playback
// and recording.
//
// Since: 4.0
type AudiodevCoreaudioPerDirectionOptions struct {
	// use QEMU's mixing engine to mix all streams inside QEMU and
	// convert audio formats when not supported by the backend. When
	// set to off, fixed-settings must be also off (default on, since
	// 4.2)
	MixingEngine *bool `json:"mixing-engine,omitempty"`
	// use fixed settings for host input/output. When off, frequency,
	// channels and format must not be specified (default true)
	FixedSettings *bool `json:"fixed-settings,omitempty"`
	// frequency to use when using fixed settings (default 44100)
	Frequency *uint32 `json:"frequency,omitempty"`
	// number of channels when using fixed settings (default 2)
	Channels *uint32 `json:"channels,omitempty"`
	// number of voices to use (default 1)
	Voices *uint32 `json:"voices,omitempty"`
	// sample format to use when using fixed settings (default s16)
	Format *AudioFormat `json:"format,omitempty"`
	// the buffer length in microseconds
	BufferLength *uint32 `json:"buffer-length,omitempty"`
	// number of buffers
	BufferCount *uint32 `json:"buffer-count,omitempty"`
}

// Options of the D-Bus audio backend.
//
// 10ms at 48kHz).
//
// Since: 10.0
type AudiodevDBusOptions struct {
	// options of the capture stream
	In *AudiodevPerDirectionOptions `json:"in,omitempty"`
	// options of the playback stream
	Out *AudiodevPerDirectionOptions `json:"out,omitempty"`
	// set the number of samples per read/write calls (default to 480,
	Nsamples *uint32 `json:"nsamples,omitempty"`
}

// Options of the DirectSound audio backend.
//
// Since: 4.0
type AudiodevDsoundOptions struct {
	// options of the capture stream
	In *AudiodevPerDirectionOptions `json:"in,omitempty"`
	// options of the playback stream
	Out *AudiodevPerDirectionOptions `json:"out,omitempty"`
	// add extra latency to playback in microseconds (default 10000)
	Latency *uint32 `json:"latency,omitempty"`
}

// Generic driver-specific options.
//
// Since: 4.0
type AudiodevGenericOptions struct {
	// options of the capture stream
	In *AudiodevPerDirectionOptions `json:"in,omitempty"`
	// options of the playback stream
	Out *AudiodevPerDirectionOptions `json:"out,omitempty"`
}

// Options of the JACK audio backend.
//
// Since: 5.1
type AudiodevJackOptions struct {
	// options of the capture stream
	In *AudiodevJackPerDirectionOptions `json:"in,omitempty"`
	// options of the playback stream
	Out *AudiodevJackPerDirectionOptions `json:"out,omitempty"`
}

// Options of the JACK backend that are used for both playback and
// recording.
//
// Since: 5.1
type AudiodevJackPerDirectionOptions struct {
	// use QEMU's mixing engine to mix all streams inside QEMU and
	// convert audio formats when not supported by the backend. When
	// set to off, fixed-settings must be also off (default on, since
	// 4.2)
	MixingEngine *bool `json:"mixing-engine,omitempty"`
	// use fixed settings for host input/output. When off, frequency,
	// channels and format must not be specified (default true)
	FixedSettings *bool `json:"fixed-settings,omitempty"`
	// frequency to use when using fixed settings (default 44100)
	Frequency *uint32 `json:"frequency,omitempty"`
	// number of channels when using fixed settings (default 2)
	Channels *uint32 `json:"channels,omitempty"`
	// number of voices to use (default 1)
	Voices *uint32 `json:"voices,omitempty"`
	// sample format to use when using fixed settings (default s16)
	Format *AudioFormat `json:"format,omitempty"`
	// the buffer length in microseconds
	BufferLength *uint32 `json:"buffer-length,omitempty"`
	// select from among several possible concurrent server instances
	// (default: environment variable $JACK_DEFAULT_SERVER if set,
	// else "default")
	ServerName *string `json:"server-name,omitempty"`
	// the client name to use. The server will modify this name to
	// create a unique variant, if needed unless @exact-name is true
	// (default: the guest's name)
	ClientName *string `json:"client-name,omitempty"`
	// if set, a regular expression of JACK client port name(s) to
	// monitor for and automatically connect to
	ConnectPorts *string `json:"connect-ports,omitempty"`
	// start a jack server process if one is not already present
	// (default: false)
	StartServer *bool `json:"start-server,omitempty"`
	// use the exact name requested otherwise JACK automatically
	// generates a unique one, if needed (default: false)
	ExactName *bool `json:"exact-name,omitempty"`
}

// Options of the OSS audio backend.
//
// Since: 4.0
type AudiodevOssOptions struct {
	// options of the capture stream
	In *AudiodevOssPerDirectionOptions `json:"in,omitempty"`
	// options of the playback stream
	Out *AudiodevOssPerDirectionOptions `json:"out,omitempty"`
	// try using memory-mapped access, falling back to non-memory-
	// mapped access on failure (default true)
	TryMmap *bool `json:"try-mmap,omitempty"`
	// open device in exclusive mode (vmix won't work) (default false)
	Exclusive *bool `json:"exclusive,omitempty"`
	// set the timing policy of the device (between 0 and 10, where
	// smaller number means smaller latency but higher CPU usage) or
	// -1 to use fragment mode (option ignored on some platforms)
	// (default 5)
	DspPolicy *uint32 `json:"dsp-policy,omitempty"`
}

// Options of the OSS backend that are used for both playback and
// recording.
//
// Since: 4.0
type AudiodevOssPerDirectionOptions struct {
	// use QEMU's mixing engine to mix all streams inside QEMU and
	// convert audio formats when not supported by the backend. When
	// set to off, fixed-settings must be also off (default on, since
	// 4.2)
	MixingEngine *bool `json:"mixing-engine,omitempty"`
	// use fixed settings for host input/output. When off, frequency,
	// channels and format must not be specified (default true)
	FixedSettings *bool `json:"fixed-settings,omitempty"`
	// frequency to use when using fixed settings (default 44100)
	Frequency *uint32 `json:"frequency,omitempty"`
	// number of channels when using fixed settings (default 2)
	Channels *uint32 `json:"channels,omitempty"`
	// number of voices to use (default 1)
	Voices *uint32 `json:"voices,omitempty"`
	// sample format to use when using fixed settings (default s16)
	Format *AudioFormat `json:"format,omitempty"`
	// the buffer length in microseconds
	BufferLength *uint32 `json:"buffer-length,omitempty"`
	// file name of the OSS device (default '/dev/dsp')
	Dev *string `json:"dev,omitempty"`
	// number of buffers
	BufferCount *uint32 `json:"buffer-count,omitempty"`
	// attempt to use poll mode, falling back to non-polling access on
	// failure (default true)
	TryPoll *bool `json:"try-poll,omitempty"`
}

// Options of the PulseAudio audio backend.
//
// Since: 4.0
type AudiodevPaOptions struct {
	// options of the capture stream
	In *AudiodevPaPerDirectionOptions `json:"in,omitempty"`
	// options of the playback stream
	Out *AudiodevPaPerDirectionOptions `json:"out,omitempty"`
	// PulseAudio server address (default: let PulseAudio choose)
	Server *string `json:"server,omitempty"`
}

// Options of the Pulseaudio backend that are used for both playback
// and recording.
//
// Since: 4.0
type AudiodevPaPerDirectionOptions struct {
	// use QEMU's mixing engine to mix all streams inside QEMU and
	// convert audio formats when not supported by the backend. When
	// set to off, fixed-settings must be also off (default on, since
	// 4.2)
	MixingEngine *bool `json:"mixing-engine,omitempty"`
	// use fixed settings for host input/output. When off, frequency,
	// channels and format must not be specified (default true)
	FixedSettings *bool `json:"fixed-settings,omitempty"`
	// frequency to use when using fixed settings (default 44100)
	Frequency *uint32 `json:"frequency,omitempty"`
	// number of channels when using fixed settings (default 2)
	Channels *uint32 `json:"channels,omitempty"`
	// number of voices to use (default 1)
	Voices *uint32 `json:"voices,omitempty"`
	// sample format to use when using fixed settings (default s16)
	Format *AudioFormat `json:"format,omitempty"`
	// the buffer length in microseconds
	BufferLength *uint32 `json:"buffer-length,omitempty"`
	// name of the sink/source to use
	Name *string `json:"name,omitempty"`
	// name of the PulseAudio stream created by qemu. Can be used to
	// identify the stream in PulseAudio when you create multiple
	// PulseAudio devices or run multiple qemu instances (default:
	// audiodev's id, since 4.2)
	StreamName *string `json:"stream-name,omitempty"`
	// latency you want PulseAudio to achieve in microseconds (default
	// 15000)
	Latency *uint32 `json:"latency,omitempty"`
}

// General audio backend options that are used for both playback and
// recording.
//
// Since: 4.0
type AudiodevPerDirectionOptions struct {
	// use QEMU's mixing engine to mix all streams inside QEMU and
	// convert audio formats when not supported by the backend. When
	// set to off, fixed-settings must be also off (default on, since
	// 4.2)
	MixingEngine *bool `json:"mixing-engine,omitempty"`
	// use fixed settings for host input/output. When off, frequency,
	// channels and format must not be specified (default true)
	FixedSettings *bool `json:"fixed-settings,omitempty"`
	// frequency to use when using fixed settings (default 44100)
	Frequency *uint32 `json:"frequency,omitempty"`
	// number of channels when using fixed settings (default 2)
	Channels *uint32 `json:"channels,omitempty"`
	// number of voices to use (default 1)
	Voices *uint32 `json:"voices,omitempty"`
	// sample format to use when using fixed settings (default s16)
	Format *AudioFormat `json:"format,omitempty"`
	// the buffer length in microseconds
	BufferLength *uint32 `json:"buffer-length,omitempty"`
}

// Options of the PipeWire audio backend.
//
// Since: 8.1
type AudiodevPipewireOptions struct {
	// options of the capture stream
	In *AudiodevPipewirePerDirectionOptions `json:"in,omitempty"`
	// options of the playback stream
	Out *AudiodevPipewirePerDirectionOptions `json:"out,omitempty"`
}

// Options of the PipeWire backend that are used for both playback and
// recording.
//
// Since: 8.1
type AudiodevPipewirePerDirectionOptions struct {
	// use QEMU's mixing engine to mix all streams inside QEMU and
	// convert audio formats when not supported by the backend. When
	// set to off, fixed-settings must be also off (default on, since
	// 4.2)
	MixingEngine *bool `json:"mixing-engine,omitempty"`
	// use fixed settings for host input/output. When off, frequency,
	// channels and format must not be specified (default true)
	FixedSettings *bool `json:"fixed-settings,omitempty"`
	// frequency to use when using fixed settings (default 44100)
	Frequency *uint32 `json:"frequency,omitempty"`
	// number of channels when using fixed settings (default 2)
	Channels *uint32 `json:"channels,omitempty"`
	// number of voices to use (default 1)
	Voices *uint32 `json:"voices,omitempty"`
	// sample format to use when using fixed settings (default s16)
	Format *AudioFormat `json:"format,omitempty"`
	// the buffer length in microseconds
	BufferLength *uint32 `json:"buffer-length,omitempty"`
	// name of the sink/source to use
	Name *string `json:"name,omitempty"`
	// name of the PipeWire stream created by qemu. Can be used to
	// identify the stream in PipeWire when you create multiple
	// PipeWire devices or run multiple qemu instances (default:
	// audiodev's id)
	StreamName *string `json:"stream-name,omitempty"`
	// latency you want PipeWire to achieve in microseconds (default
	// 46000)
	Latency *uint32 `json:"latency,omitempty"`
}

// Options of the SDL audio backend.
//
// Since: 6.0
type AudiodevSdlOptions struct {
	// options of the recording stream
	In *AudiodevSdlPerDirectionOptions `json:"in,omitempty"`
	// options of the playback stream
	Out *AudiodevSdlPerDirectionOptions `json:"out,omitempty"`
}

// Options of the SDL audio backend that are used for both playback
// and recording.
//
// Since: 6.0
type AudiodevSdlPerDirectionOptions struct {
	// use QEMU's mixing engine to mix all streams inside QEMU and
	// convert audio formats when not supported by the backend. When
	// set to off, fixed-settings must be also off (default on, since
	// 4.2)
	MixingEngine *bool `json:"mixing-engine,omitempty"`
	// use fixed settings for host input/output. When off, frequency,
	// channels and format must not be specified (default true)
	FixedSettings *bool `json:"fixed-settings,omitempty"`
	// frequency to use when using fixed settings (default 44100)
	Frequency *uint32 `json:"frequency,omitempty"`
	// number of channels when using fixed settings (default 2)
	Channels *uint32 `json:"channels,omitempty"`
	// number of voices to use (default 1)
	Voices *uint32 `json:"voices,omitempty"`
	// sample format to use when using fixed settings (default s16)
	Format *AudioFormat `json:"format,omitempty"`
	// the buffer length in microseconds
	BufferLength *uint32 `json:"buffer-length,omitempty"`
	// number of buffers (default 4)
	BufferCount *uint32 `json:"buffer-count,omitempty"`
}

// Options of the sndio audio backend.
//
// Since: 7.2
type AudiodevSndioOptions struct {
	// options of the capture stream
	In *AudiodevPerDirectionOptions `json:"in,omitempty"`
	// options of the playback stream
	Out *AudiodevPerDirectionOptions `json:"out,omitempty"`
	// the name of the sndio device to use (default 'default')
	Dev *string `json:"dev,omitempty"`
	// play buffer size (in microseconds)
	Latency *uint32 `json:"latency,omitempty"`
}

// Options of the wav audio backend.
//
// Since: 4.0
type AudiodevWavOptions struct {
	// options of the capture stream
	In *AudiodevPerDirectionOptions `json:"in,omitempty"`
	// options of the playback stream
	Out *AudiodevPerDirectionOptions `json:"out,omitempty"`
	// name of the wav file to record (default 'qemu.wav')
	Path *string `json:"path,omitempty"`
}

// Properties for authz-listfile objects.
//
// Since: 4.0
type AuthZListFileProperties struct {
	// File name to load the configuration from. The file must contain
	// valid JSON for AuthZListProperties.
	Filename string `json:"filename"`
	// If true, inotify is used to monitor the file, automatically
	// reloading changes. If an error occurs during reloading, all
	// authorizations will fail until the file is next successfully
	// loaded. (default: true if the binary was built with
	// CONFIG_INOTIFY1, false otherwise)
	Refresh *bool `json:"refresh,omitempty"`
}

// Properties for authz-list objects.
//
// Since: 4.0
type AuthZListProperties struct {
	// Default policy to apply when no rule matches (default: deny)
	Policy *QAuthZListPolicy `json:"policy,omitempty"`
	// Authorization rules based on matching user
	Rules []QAuthZListRule `json:"rules,omitempty"`
}

// Properties for authz-pam objects.
//
// Since: 4.0
type AuthZPAMProperties struct {
	// PAM service name to use for authorization
	Service string `json:"service"`
}

// Properties for authz-simple objects.
//
// Since: 4.0
type AuthZSimpleProperties struct {
	// Identifies the allowed user. Its format depends on the network
	// service that authorization object is associated with. For
	// authorizing based on TLS x509 certificates, the identity must
	// be the x509 distinguished name.
	Identity string `json:"identity"`
}

// .. note:: @on-source-error and @on-target-error only affect
// background I/O. If an error occurs during a guest write request,
// the device's rerror/werror actions will be used.
//
// Since: 4.2
type BackupCommon struct {
	// identifier for the newly-created block job. If omitted, the
	// device name will be used. (Since 2.7)
	JobId *string `json:"job-id,omitempty"`
	// the device name or node-name of a root node which should be
	// copied.
	Device string `json:"device"`
	// what parts of the disk image should be copied to the
	// destination (all the disk, only the sectors allocated in the
	// topmost image, from a dirty bitmap, or only new I/O).
	Sync MirrorSyncMode `json:"sync"`
	// the maximum speed, in bytes per second. The default is 0, for
	// unlimited.
	Speed *int64 `json:"speed,omitempty"`
	// The name of a dirty bitmap to use. Must be present if sync is
	// "bitmap" or "incremental". Can be present if sync is "full" or
	// "top". Must not be present otherwise. (Since 2.4 (drive-
	// backup), 3.1 (blockdev-backup))
	Bitmap *string `json:"bitmap,omitempty"`
	// Specifies the type of data the bitmap should contain after the
	// operation concludes. Must be present if a bitmap was provided,
	// Must NOT be present otherwise. (Since 4.2)
	BitmapMode *BitmapSyncMode `json:"bitmap-mode,omitempty"`
	// true to compress data, if the target format supports it.
	// (default: false) (since 2.8)
	Compress *bool `json:"compress,omitempty"`
	// the action to take on an error on the source, default 'report'.
	// 'stop' and 'enospc' can only be used if the block device
	// supports io-status (see BlockInfo).
	OnSourceError *BlockdevOnError `json:"on-source-error,omitempty"`
	// the action to take on an error on the target, default 'report'
	// (no limitations, since this applies to a different block device
	// than @device).
	OnTargetError *BlockdevOnError `json:"on-target-error,omitempty"`
	// When false, this job will wait in a PENDING state after it has
	// finished its work, waiting for @block-job-finalize before
	// making any block graph changes. When true, this job will
	// automatically perform its abort or commit actions. Defaults to
	// true. (Since 2.12)
	AutoFinalize *bool `json:"auto-finalize,omitempty"`
	// When false, this job will wait in a CONCLUDED state after it
	// has completely ceased all work, and awaits @block-job-dismiss.
	// When true, this job will automatically disappear from the query
	// list without user intervention. Defaults to true. (Since 2.12)
	AutoDismiss *bool `json:"auto-dismiss,omitempty"`
	// the node name that should be assigned to the filter driver that
	// the backup job inserts into the graph above node specified by
	// @drive. If this option is not given, a node name is
	// autogenerated. (Since: 4.2)
	FilterNodeName *string `json:"filter-node-name,omitempty"`
	// Discard blocks on source which have already been copied to the
	// target. (Since 9.1)
	DiscardSource *bool `json:"discard-source,omitempty"`
	// Performance options. (Since 6.0)
	XPerf *BackupPerf `json:"x-perf,omitempty"`
}

// Optional parameters for backup.  These parameters don't affect
// functionality, but may significantly affect performance.
//
// Since: 6.0
type BackupPerf struct {
	// Use copy offloading. Default false.
	UseCopyRange *bool `json:"use-copy-range,omitempty"`
	// Maximum number of parallel requests for the sustained
	// background copying process. Doesn't influence copy-before-write
	// operations. Default 64.
	MaxWorkers *int64 `json:"max-workers,omitempty"`
	// Maximum request length for the sustained background copying
	// process. Doesn't influence copy-before-write operations. 0
	// means unlimited. If max-chunk is non-zero then it should not be
	// less than job cluster size which is calculated as maximum of
	// target image cluster size and 64k. Default 0.
	MaxChunk *int64 `json:"max-chunk,omitempty"`
	// Minimum size of blocks used by copy-before-write and background
	// copy operations. Has to be a power of 2. No effect if smaller
	// than the maximum of the target's cluster size and 64 KiB.
	// Default 0. (Since 9.2)
	MinClusterSize *uint64 `json:"min-cluster-size,omitempty"`
}

// Information about the guest balloon device.
//
// Since: 0.14
type BalloonInfo struct {
	// the logical size of the VM in bytes Formula used:
	// logical_vm_size = vm_ram_size - balloon_size
	Actual int64 `json:"actual"`
}

// Since: 5.2
type BitmapMigrationBitmapAlias struct {
	// The name of the bitmap.
	Name string `json:"name"`
	// An alias name for migration (for example the bitmap name on the
	// opposite site).
	Alias string `json:"alias"`
	// Allows the modification of the migrated bitmap. (since 6.0)
	Transform *BitmapMigrationBitmapAliasTransform `json:"transform,omitempty"`
}

// Since: 6.0
type BitmapMigrationBitmapAliasTransform struct {
	// If present, the bitmap will be made persistent or transient
	// depending on this parameter.
	Persistent *bool `json:"persistent,omitempty"`
}

// Maps a block node name and the bitmaps it has to aliases for dirty
// bitmap migration.
//
// Since: 5.2
type BitmapMigrationNodeAlias struct {
	// A block node name.
	NodeName string `json:"node-name"`
	// An alias block node name for migration (for example the node
	// name on the opposite site).
	Alias string `json:"alias"`
	// Mappings for the bitmaps on this node.
	Bitmaps []BitmapMigrationBitmapAlias `json:"bitmaps"`
}

// Describes a single error injection for blkdebug.
//
// Since: 2.9
type BlkdebugInjectErrorOptions struct {
	// trigger event
	Event BlkdebugEvent `json:"event"`
	// the state identifier blkdebug needs to be in to actually
	// trigger the event; defaults to "any"
	State *int64 `json:"state,omitempty"`
	// the type of I/O operations on which this error should be
	// injected; defaults to "all read, write, write-zeroes, discard,
	// and flush operations" (since: 4.1)
	Iotype *BlkdebugIOType `json:"iotype,omitempty"`
	// error identifier (errno) to be returned; defaults to EIO
	Errno *int64 `json:"errno,omitempty"`
	// specifies the sector index which has to be affected in order to
	// actually trigger the event; defaults to "any sector"
	Sector *int64 `json:"sector,omitempty"`
	// disables further events after this one has been triggered;
	// defaults to false
	Once *bool `json:"once,omitempty"`
	// fail immediately; defaults to false
	Immediately *bool `json:"immediately,omitempty"`
}

// Describes a single state-change event for blkdebug.
//
// Since: 2.9
type BlkdebugSetStateOptions struct {
	// trigger event
	Event BlkdebugEvent `json:"event"`
	// the current state identifier blkdebug needs to be in; defaults
	// to "any"
	State *int64 `json:"state,omitempty"`
	// the state identifier blkdebug is supposed to assume if this
	// event is triggered
	NewState int64 `json:"new_state"`
}

// Information about all nodes in the block graph starting at some
// node, annotated with information about that node in relation to its
// parent.
//
// Since: 8.0
type BlockChildInfo struct {
	// Child name of the root node in the BlockGraphInfo struct, in
	// its role as the child of some undescribed parent node
	Name string `json:"name"`
	// Block graph information starting at this node
	Info BlockGraphInfo `json:"info"`
}

// Information about the backing device for a block device.
//
// Since: 0.14
type BlockDeviceInfo struct {
	// the filename of the backing device
	File string `json:"file"`
	// the name of the block driver node (Since 2.0)
	NodeName *string `json:"node-name,omitempty"`
	// true if the backing device was open read-only
	Ro bool `json:"ro"`
	// the name of the block format used to open the backing device.
	// As of 0.14 this can be: 'blkdebug', 'bochs', 'cloop', 'cow',
	// 'dmg', 'file', 'file', 'ftp', 'ftps', 'host_cdrom',
	// 'host_device', 'http', 'https', 'luks', 'nbd', 'parallels',
	// 'qcow', 'qcow2', 'raw', 'vdi', 'vmdk', 'vpc', 'vvfat' 2.2:
	// 'archipelago' added, 'cow' dropped 2.3: 'host_floppy'
	// deprecated 2.5: 'host_floppy' dropped 2.6: 'luks' added 2.8:
	// 'replication' added, 'tftp' dropped 2.9: 'archipelago' dropped
	Drv string `json:"drv"`
	// the name of the backing file (for copy-on-write)
	BackingFile *string `json:"backing_file,omitempty"`
	// number of files in the backing file chain (since: 1.2)
	BackingFileDepth int64 `json:"backing_file_depth"`
	// true if the backend is active; typical cases for inactive
	// backends are on the migration source instance after migration
	// completes and on the destination before it completes. (since:
	// 10.0)
	Active bool `json:"active"`
	// true if the backing device is encrypted
	Encrypted bool `json:"encrypted"`
	// detect and optimize zero writes (Since 2.1)
	DetectZeroes BlockdevDetectZeroesOptions `json:"detect_zeroes"`
	// total throughput limit in bytes per second is specified
	Bps int64 `json:"bps"`
	// read throughput limit in bytes per second is specified
	BpsRd int64 `json:"bps_rd"`
	// write throughput limit in bytes per second is specified
	BpsWr int64 `json:"bps_wr"`
	// total I/O operations per second is specified
	Iops int64 `json:"iops"`
	// read I/O operations per second is specified
	IopsRd int64 `json:"iops_rd"`
	// write I/O operations per second is specified
	IopsWr int64 `json:"iops_wr"`
	// the info of image used (since: 1.6)
	Image ImageInfo `json:"image"`
	// total throughput limit during bursts, in bytes (Since 1.7)
	BpsMax *int64 `json:"bps_max,omitempty"`
	// read throughput limit during bursts, in bytes (Since 1.7)
	BpsRdMax *int64 `json:"bps_rd_max,omitempty"`
	// write throughput limit during bursts, in bytes (Since 1.7)
	BpsWrMax *int64 `json:"bps_wr_max,omitempty"`
	// total I/O operations per second during bursts, in bytes (Since
	// 1.7)
	IopsMax *int64 `json:"iops_max,omitempty"`
	// read I/O operations per second during bursts, in bytes (Since
	// 1.7)
	IopsRdMax *int64 `json:"iops_rd_max,omitempty"`
	// write I/O operations per second during bursts, in bytes (Since
	// 1.7)
	IopsWrMax *int64 `json:"iops_wr_max,omitempty"`
	// maximum length of the @bps_max burst period, in seconds. (Since
	// 2.6)
	BpsMaxLength *int64 `json:"bps_max_length,omitempty"`
	// maximum length of the @bps_rd_max burst period, in seconds.
	// (Since 2.6)
	BpsRdMaxLength *int64 `json:"bps_rd_max_length,omitempty"`
	// maximum length of the @bps_wr_max burst period, in seconds.
	// (Since 2.6)
	BpsWrMaxLength *int64 `json:"bps_wr_max_length,omitempty"`
	// maximum length of the @iops burst period, in seconds. (Since
	// 2.6)
	IopsMaxLength *int64 `json:"iops_max_length,omitempty"`
	// maximum length of the @iops_rd_max burst period, in seconds.
	// (Since 2.6)
	IopsRdMaxLength *int64 `json:"iops_rd_max_length,omitempty"`
	// maximum length of the @iops_wr_max burst period, in seconds.
	// (Since 2.6)
	IopsWrMaxLength *int64 `json:"iops_wr_max_length,omitempty"`
	// an I/O size in bytes (Since 1.7)
	IopsSize *int64 `json:"iops_size,omitempty"`
	// throttle group name (Since 2.4)
	Group *string `json:"group,omitempty"`
	// the cache mode used for the block device (since: 2.3)
	Cache BlockdevCacheInfo `json:"cache"`
	// configured write threshold for the device. 0 if disabled.
	// (Since 2.3)
	WriteThreshold int64 `json:"write_threshold"`
	// dirty bitmaps information (only present if node has one or more
	// dirty bitmaps) (Since 4.2)
	DirtyBitmaps []BlockDirtyInfo `json:"dirty-bitmaps,omitempty"`
}

// Statistics of a virtual block device or a block backing device.
//
// Since: 0.14
type BlockDeviceStats struct {
	// The number of bytes read by the device.
	RdBytes int64 `json:"rd_bytes"`
	// The number of bytes written by the device.
	WrBytes int64 `json:"wr_bytes"`
	// The number of bytes appended by the zoned devices (since 8.1)
	ZoneAppendBytes int64 `json:"zone_append_bytes"`
	// The number of bytes unmapped by the device (Since 4.2)
	UnmapBytes int64 `json:"unmap_bytes"`
	// The number of read operations performed by the device.
	RdOperations int64 `json:"rd_operations"`
	// The number of write operations performed by the device.
	WrOperations int64 `json:"wr_operations"`
	// The number of zone append operations performed by the zoned
	// devices (since 8.1)
	ZoneAppendOperations int64 `json:"zone_append_operations"`
	// The number of cache flush operations performed by the device
	// (since 0.15)
	FlushOperations int64 `json:"flush_operations"`
	// The number of unmap operations performed by the device (Since
	// 4.2)
	UnmapOperations int64 `json:"unmap_operations"`
	// Total time spent on reads in nanoseconds (since 0.15).
	RdTotalTimeNs int64 `json:"rd_total_time_ns"`
	// Total time spent on writes in nanoseconds (since 0.15).
	WrTotalTimeNs int64 `json:"wr_total_time_ns"`
	// Total time spent on zone append writes in nanoseconds (since
	// 8.1)
	ZoneAppendTotalTimeNs int64 `json:"zone_append_total_time_ns"`
	// Total time spent on cache flushes in nanoseconds (since 0.15).
	FlushTotalTimeNs int64 `json:"flush_total_time_ns"`
	// Total time spent on unmap operations in nanoseconds (Since 4.2)
	UnmapTotalTimeNs int64 `json:"unmap_total_time_ns"`
	// The offset after the greatest byte written to the device. The
	// intended use of this information is for growable sparse files
	// (like qcow2) that are used on top of a physical device.
	WrHighestOffset int64 `json:"wr_highest_offset"`
	// Number of read requests that have been merged into another
	// request (Since 2.3).
	RdMerged int64 `json:"rd_merged"`
	// Number of write requests that have been merged into another
	// request (Since 2.3).
	WrMerged int64 `json:"wr_merged"`
	// Number of zone append requests that have been merged into
	// another request (since 8.1)
	ZoneAppendMerged int64 `json:"zone_append_merged"`
	// Number of unmap requests that have been merged into another
	// request (Since 4.2)
	UnmapMerged int64 `json:"unmap_merged"`
	// Time since the last I/O operation, in nanoseconds. If the field
	// is absent it means that there haven't been any operations yet
	// (Since 2.5).
	IdleTimeNs *int64 `json:"idle_time_ns,omitempty"`
	// The number of failed read operations performed by the device
	// (Since 2.5)
	FailedRdOperations int64 `json:"failed_rd_operations"`
	// The number of failed write operations performed by the device
	// (Since 2.5)
	FailedWrOperations int64 `json:"failed_wr_operations"`
	// The number of failed zone append write operations performed by
	// the zoned devices (since 8.1)
	FailedZoneAppendOperations int64 `json:"failed_zone_append_operations"`
	// The number of failed flush operations performed by the device
	// (Since 2.5)
	FailedFlushOperations int64 `json:"failed_flush_operations"`
	// The number of failed unmap operations performed by the device
	// (Since 4.2)
	FailedUnmapOperations int64 `json:"failed_unmap_operations"`
	// The number of invalid read operations performed by the device
	// (Since 2.5)
	InvalidRdOperations int64 `json:"invalid_rd_operations"`
	// The number of invalid write operations performed by the device
	// (Since 2.5)
	InvalidWrOperations int64 `json:"invalid_wr_operations"`
	// The number of invalid zone append operations performed by the
	// zoned device (since 8.1)
	InvalidZoneAppendOperations int64 `json:"invalid_zone_append_operations"`
	// The number of invalid flush operations performed by the device
	// (Since 2.5)
	InvalidFlushOperations int64 `json:"invalid_flush_operations"`
	// The number of invalid unmap operations performed by the device
	// (Since 4.2)
	InvalidUnmapOperations int64 `json:"invalid_unmap_operations"`
	// Whether invalid operations are included in the last access
	// statistics (Since 2.5)
	AccountInvalid bool `json:"account_invalid"`
	// Whether failed operations are included in the latency and last
	// access statistics (Since 2.5)
	AccountFailed bool `json:"account_failed"`
	// Statistics specific to the set of previously defined intervals
	// of time (Since 2.5)
	TimedStats []BlockDeviceTimedStats `json:"timed_stats"`
	// @BlockLatencyHistogramInfo. (Since 4.0)
	RdLatencyHistogram *BlockLatencyHistogramInfo `json:"rd_latency_histogram,omitempty"`
	// @BlockLatencyHistogramInfo. (Since 4.0)
	WrLatencyHistogram *BlockLatencyHistogramInfo `json:"wr_latency_histogram,omitempty"`
	// @BlockLatencyHistogramInfo. (since 8.1)
	ZoneAppendLatencyHistogram *BlockLatencyHistogramInfo `json:"zone_append_latency_histogram,omitempty"`
	// @BlockLatencyHistogramInfo. (Since 4.0)
	FlushLatencyHistogram *BlockLatencyHistogramInfo `json:"flush_latency_histogram,omitempty"`
}

// Statistics of a block device during a given interval of time.
//
// Since: 2.5
type BlockDeviceTimedStats struct {
	// Interval used for calculating the statistics, in seconds.
	IntervalLength int64 `json:"interval_length"`
	// Minimum latency of read operations in the defined interval, in
	// nanoseconds.
	MinRdLatencyNs int64 `json:"min_rd_latency_ns"`
	// Maximum latency of read operations in the defined interval, in
	// nanoseconds.
	MaxRdLatencyNs int64 `json:"max_rd_latency_ns"`
	// Average latency of read operations in the defined interval, in
	// nanoseconds.
	AvgRdLatencyNs int64 `json:"avg_rd_latency_ns"`
	// Minimum latency of write operations in the defined interval, in
	// nanoseconds.
	MinWrLatencyNs int64 `json:"min_wr_latency_ns"`
	// Maximum latency of write operations in the defined interval, in
	// nanoseconds.
	MaxWrLatencyNs int64 `json:"max_wr_latency_ns"`
	// Average latency of write operations in the defined interval, in
	// nanoseconds.
	AvgWrLatencyNs int64 `json:"avg_wr_latency_ns"`
	// Minimum latency of zone append operations in the defined
	// interval, in nanoseconds (since 8.1)
	MinZoneAppendLatencyNs int64 `json:"min_zone_append_latency_ns"`
	// Maximum latency of zone append operations in the defined
	// interval, in nanoseconds (since 8.1)
	MaxZoneAppendLatencyNs int64 `json:"max_zone_append_latency_ns"`
	// Average latency of zone append operations in the defined
	// interval, in nanoseconds (since 8.1)
	AvgZoneAppendLatencyNs int64 `json:"avg_zone_append_latency_ns"`
	// Minimum latency of flush operations in the defined interval, in
	// nanoseconds.
	MinFlushLatencyNs int64 `json:"min_flush_latency_ns"`
	// Maximum latency of flush operations in the defined interval, in
	// nanoseconds.
	MaxFlushLatencyNs int64 `json:"max_flush_latency_ns"`
	// Average latency of flush operations in the defined interval, in
	// nanoseconds.
	AvgFlushLatencyNs int64 `json:"avg_flush_latency_ns"`
	// Average number of pending read operations in the defined
	// interval.
	AvgRdQueueDepth float64 `json:"avg_rd_queue_depth"`
	// Average number of pending write operations in the defined
	// interval.
	AvgWrQueueDepth float64 `json:"avg_wr_queue_depth"`
	// Average number of pending zone append operations in the defined
	// interval (since 8.1).
	AvgZoneAppendQueueDepth float64 `json:"avg_zone_append_queue_depth"`
}

// Since: 2.4
type BlockDirtyBitmap struct {
	// name of device/node which the bitmap is tracking
	Node string `json:"node"`
	// name of the dirty bitmap
	Name string `json:"name"`
}

// Since: 2.4
type BlockDirtyBitmapAdd struct {
	// name of device/node which the bitmap is tracking
	Node string `json:"node"`
	// name of the dirty bitmap (must be less than 1024 bytes)
	Name string `json:"name"`
	// the bitmap granularity, default is 64k for block-dirty-bitmap-
	// add
	Granularity *uint32 `json:"granularity,omitempty"`
	// the bitmap is persistent, i.e. it will be saved to the
	// corresponding block device image file on its close. For now
	// only Qcow2 disks support persistent bitmaps. Default is false
	// for block-dirty-bitmap-add. (Since: 2.10)
	Persistent *bool `json:"persistent,omitempty"`
	// the bitmap is created in the disabled state, which means that
	// it will not track drive changes. The bitmap may be enabled with
	// block-dirty-bitmap-enable. Default is false. (Since: 4.0)
	Disabled *bool `json:"disabled,omitempty"`
}

// Since: 2.5
type BlockDirtyBitmapAddWrapper struct {
	Data BlockDirtyBitmapAdd `json:"data"`
}

// Since: 4.0
type BlockDirtyBitmapMerge struct {
	// name of device/node which the @target bitmap is tracking
	Node string `json:"node"`
	// name of the destination dirty bitmap
	Target string `json:"target"`
	// name(s) of the source dirty bitmap(s) at @node and/or fully
	// specified BlockDirtyBitmap elements. The latter are supported
	// since 4.1.
	Bitmaps []BlockDirtyBitmapOrStr `json:"bitmaps"`
}

// Since: 4.0
type BlockDirtyBitmapMergeWrapper struct {
	Data BlockDirtyBitmapMerge `json:"data"`
}

// SHA256 hash of dirty bitmap data
//
// Since: 2.10
type BlockDirtyBitmapSha256 struct {
	// ASCII representation of SHA256 bitmap hash
	Sha256 string `json:"sha256"`
}

// Since: 2.5
type BlockDirtyBitmapWrapper struct {
	Data BlockDirtyBitmap `json:"data"`
}

// Block dirty bitmap information.
//
// Since: 1.3
type BlockDirtyInfo struct {
	// the name of the dirty bitmap (Since 2.4)
	Name *string `json:"name,omitempty"`
	// number of dirty bytes according to the dirty bitmap
	Count int64 `json:"count"`
	// granularity of the dirty bitmap in bytes (since 1.4)
	Granularity uint32 `json:"granularity"`
	// true if the bitmap is recording new writes from the guest.
	// (since 4.0)
	Recording bool `json:"recording"`
	// true if the bitmap is in-use by some operation (NBD or jobs)
	// and cannot be modified via QMP or used by another operation.
	// (since 4.0)
	Busy bool `json:"busy"`
	// true if the bitmap was stored on disk, is scheduled to be
	// stored on disk, or both. (since 4.0)
	Persistent bool `json:"persistent"`
	// true if this is a persistent bitmap that was improperly stored.
	// Implies @persistent to be true; @recording and @busy to be
	// false. This bitmap cannot be used. To remove it, use @block-
	// dirty-bitmap-remove. (Since 4.0)
	Inconsistent *bool `json:"inconsistent,omitempty"`
}

// Information about a single block export.
//
// Since: 5.2
type BlockExportInfo struct {
	// The unique identifier for the block export
	Id string `json:"id"`
	// The block export type
	Type BlockExportType `json:"type"`
	// The node name of the block node that is exported
	NodeName string `json:"node-name"`
	// True if the export is shutting down (e.g. after a block-export-
	// del command, but before the shutdown has completed)
	ShuttingDown bool `json:"shutting-down"`
}

// Options for exporting a block graph node on some (file) mountpoint
// as a raw image.
//
// Since: 6.0
type BlockExportOptionsFuse struct {
	// Path on which to export the block device via FUSE. This must
	// point to an existing regular file.
	Mountpoint string `json:"mountpoint"`
	// Whether writes beyond the EOF should grow the block node
	// accordingly. (default: false)
	Growable *bool `json:"growable,omitempty"`
	// If this is off, only qemu's user is allowed access to this
	// export. That cannot be changed even with chmod or chown.
	// Enabling this option will allow other users access to the
	// export with the FUSE mount option "allow_other". Note that
	// using allow_other as a non-root user requires user_allow_other
	// to be enabled in the global fuse.conf configuration file. In
	// auto mode (the default), the FUSE export driver will first
	// attempt to mount the export with allow_other, and if that
	// fails, try again without. (since 6.1; default: auto)
	AllowOther *FuseExportAllowOther `json:"allow-other,omitempty"`
}

// An NBD block export (distinct options used in the NBD branch of
// block-export-add).
//
// Since: 5.2
type BlockExportOptionsNbd struct {
	// Export name. If unspecified, the @device parameter is used as
	// the export name. (Since 2.12)
	Name *string `json:"name,omitempty"`
	// Free-form description of the export, up to 4096 bytes. (Since
	// 5.0)
	Description *string `json:"description,omitempty"`
	// Also export each of the named dirty bitmaps reachable from
	// @device, so the NBD client can use NBD_OPT_SET_META_CONTEXT
	// with the metadata context name "qemu:dirty-bitmap:BITMAP" to
	// inspect each bitmap. Since 7.1 bitmap may be specified by
	// node/name pair.
	Bitmaps []BlockDirtyBitmapOrStr `json:"bitmaps,omitempty"`
	// Also export the allocation depth map for @device, so the NBD
	// client can use NBD_OPT_SET_META_CONTEXT with the metadata
	// context name "qemu:allocation-depth" to inspect allocation
	// details. (since 5.2)
	AllocationDepth *bool `json:"allocation-depth,omitempty"`
}

// An NBD block export (common options shared between nbd-server-add
// and the NBD branch of block-export-add).
//
// Since: 5.0
type BlockExportOptionsNbdBase struct {
	// Export name. If unspecified, the @device parameter is used as
	// the export name. (Since 2.12)
	Name *string `json:"name,omitempty"`
	// Free-form description of the export, up to 4096 bytes. (Since
	// 5.0)
	Description *string `json:"description,omitempty"`
}

// A vduse-blk block export.
//
// Since: 7.1
type BlockExportOptionsVduseBlk struct {
	// the name of VDUSE device (must be unique across the host).
	Name string `json:"name"`
	// the number of virtqueues. Defaults to 1.
	NumQueues *uint16 `json:"num-queues,omitempty"`
	// the size of virtqueue. Defaults to 256.
	QueueSize *uint16 `json:"queue-size,omitempty"`
	// Logical block size in bytes. Range [512, PAGE_SIZE] and must be
	// power of 2. Defaults to 512 bytes.
	LogicalBlockSize *uint64 `json:"logical-block-size,omitempty"`
	// the serial number of virtio block device. Defaults to empty
	// string.
	Serial *string `json:"serial,omitempty"`
}

// A vhost-user-blk block export.
//
// Since: 5.2
type BlockExportOptionsVhostUserBlk struct {
	// The vhost-user socket on which to listen. Both 'unix' and 'fd'
	// SocketAddress types are supported. Passed fds must be UNIX
	// domain sockets.
	Addr SocketAddress `json:"addr"`
	// Logical block size in bytes. Defaults to 512 bytes.
	LogicalBlockSize *uint64 `json:"logical-block-size,omitempty"`
	// Number of request virtqueues. Must be greater than 0. Defaults
	// to 1.
	NumQueues *uint16 `json:"num-queues,omitempty"`
}

// Information about all nodes in a block (sub)graph in the form of
// BlockNodeInfo data.  The base BlockNodeInfo struct contains the
// information for the (sub)graph's root node.
//
// Since: 8.0
type BlockGraphInfo struct {
	// name of the image file
	Filename string `json:"filename"`
	// format of the image file
	Format string `json:"format"`
	// true if image is not cleanly closed
	DirtyFlag *bool `json:"dirty-flag,omitempty"`
	// actual size on disk in bytes of the image
	ActualSize *int64 `json:"actual-size,omitempty"`
	// maximum capacity in bytes of the image
	VirtualSize int64 `json:"virtual-size"`
	// size of a cluster in bytes
	ClusterSize *int64 `json:"cluster-size,omitempty"`
	// true if the image is encrypted
	Encrypted *bool `json:"encrypted,omitempty"`
	// true if the image is compressed (Since 1.7)
	Compressed *bool `json:"compressed,omitempty"`
	// name of the backing file
	BackingFilename *string `json:"backing-filename,omitempty"`
	// full path of the backing file
	FullBackingFilename *string `json:"full-backing-filename,omitempty"`
	// the format of the backing file
	BackingFilenameFormat *string `json:"backing-filename-format,omitempty"`
	// list of VM snapshots
	Snapshots []SnapshotInfo `json:"snapshots,omitempty"`
	// structure supplying additional format-specific information
	// (since 1.7)
	FormatSpecific *ImageInfoSpecific `json:"format-specific,omitempty"`
	// Array of links to this node's child nodes' information
	Children []BlockChildInfo `json:"children"`
}

// A set of parameters describing block throttling.
//
// Since: 1.1
type BlockIOThrottle struct {
	// Block device name
	Device *string `json:"device,omitempty"`
	// The name or QOM path of the guest device (since: 2.8)
	Id *string `json:"id,omitempty"`
	// total throughput limit in bytes per second
	Bps int64 `json:"bps"`
	// read throughput limit in bytes per second
	BpsRd int64 `json:"bps_rd"`
	// write throughput limit in bytes per second
	BpsWr int64 `json:"bps_wr"`
	// total I/O operations per second
	Iops int64 `json:"iops"`
	// read I/O operations per second
	IopsRd int64 `json:"iops_rd"`
	// write I/O operations per second
	IopsWr int64 `json:"iops_wr"`
	// total throughput limit during bursts, in bytes (Since 1.7)
	BpsMax *int64 `json:"bps_max,omitempty"`
	// read throughput limit during bursts, in bytes (Since 1.7)
	BpsRdMax *int64 `json:"bps_rd_max,omitempty"`
	// write throughput limit during bursts, in bytes (Since 1.7)
	BpsWrMax *int64 `json:"bps_wr_max,omitempty"`
	// total I/O operations per second during bursts, in bytes (Since
	// 1.7)
	IopsMax *int64 `json:"iops_max,omitempty"`
	// read I/O operations per second during bursts, in bytes (Since
	// 1.7)
	IopsRdMax *int64 `json:"iops_rd_max,omitempty"`
	// write I/O operations per second during bursts, in bytes (Since
	// 1.7)
	IopsWrMax *int64 `json:"iops_wr_max,omitempty"`
	// maximum length of the @bps_max burst period, in seconds. It
	// must only be set if @bps_max is set as well. Defaults to 1.
	// (Since 2.6)
	BpsMaxLength *int64 `json:"bps_max_length,omitempty"`
	// maximum length of the @bps_rd_max burst period, in seconds. It
	// must only be set if @bps_rd_max is set as well. Defaults to 1.
	// (Since 2.6)
	BpsRdMaxLength *int64 `json:"bps_rd_max_length,omitempty"`
	// maximum length of the @bps_wr_max burst period, in seconds. It
	// must only be set if @bps_wr_max is set as well. Defaults to 1.
	// (Since 2.6)
	BpsWrMaxLength *int64 `json:"bps_wr_max_length,omitempty"`
	// maximum length of the @iops burst period, in seconds. It must
	// only be set if @iops_max is set as well. Defaults to 1. (Since
	// 2.6)
	IopsMaxLength *int64 `json:"iops_max_length,omitempty"`
	// maximum length of the @iops_rd_max burst period, in seconds. It
	// must only be set if @iops_rd_max is set as well. Defaults to 1.
	// (Since 2.6)
	IopsRdMaxLength *int64 `json:"iops_rd_max_length,omitempty"`
	// maximum length of the @iops_wr_max burst period, in seconds. It
	// must only be set if @iops_wr_max is set as well. Defaults to 1.
	// (Since 2.6)
	IopsWrMaxLength *int64 `json:"iops_wr_max_length,omitempty"`
	// an I/O size in bytes (Since 1.7)
	IopsSize *int64 `json:"iops_size,omitempty"`
	// throttle group name (Since 2.4)
	Group *string `json:"group,omitempty"`
}

// Block device information.  This structure describes a virtual
// device and the backing device associated with it.
//
// Since: 0.14
type BlockInfo struct {
	// The device name associated with the virtual device.
	Device string `json:"device"`
	// The qdev ID, or if no ID is assigned, the QOM path of the block
	// device. (since 2.10)
	Qdev *string `json:"qdev,omitempty"`
	// This field is returned only for compatibility reasons, it
	// should not be used (always returns 'unknown')
	Type string `json:"type"`
	// True if the device supports removable media.
	Removable bool `json:"removable"`
	// True if the guest has locked this device from having its media
	// removed
	Locked bool `json:"locked"`
	// @BlockDeviceInfo describing the device if media is present
	Inserted *BlockDeviceInfo `json:"inserted,omitempty"`
	// True if the device's tray is open (only present if it has a
	// tray)
	TrayOpen *bool `json:"tray_open,omitempty"`
	// @BlockDeviceIoStatus. Only present if the device supports it
	// and the VM is configured to stop on errors (supported device
	// models: virtio-blk, IDE, SCSI except scsi-generic)
	IoStatus *BlockDeviceIoStatus `json:"io-status,omitempty"`
}

// Since: 8.2
type BlockJobChangeOptionsMirror struct {
	// Switch to this copy mode. Currently, only the switch from
	// 'background' to 'write-blocking' is implemented.
	CopyMode MirrorCopyMode `json:"copy-mode"`
}

// Information specific to mirror block jobs.
//
// Since: 8.2
type BlockJobInfoMirror struct {
	// Whether the source is actively synced to the target, i.e. same
	// data and new writes are done synchronously to both.
	ActivelySynced bool `json:"actively-synced"`
}

// Block latency histogram.
//
// Since: 4.0
type BlockLatencyHistogramInfo struct {
	// list of interval boundary values in nanoseconds, all greater
	// than zero and in ascending order. For example, the list [10,
	// 50, 100] produces the following histogram intervals: [0, 10),
	// [10, 50), [50, 100), [100, +inf).
	Boundaries []uint64 `json:"boundaries"`
	// list of io request counts corresponding to histogram intervals,
	// one more element than @boundaries has. For the example above,
	// @bins may be something like [3, 1, 5, 2], and corresponding
	// histogram looks like:: 5| * 4| * 3| * * 2| * * * 1| * * * *
	// +------------------ 10 50 100
	Bins []uint64 `json:"bins"`
}

// Image file size calculation information.  This structure describes
// the size requirements for creating a new image file.  The size
// requirements depend on the new image file format.  File size always
// equals virtual disk size for the 'raw' format, even for sparse
// POSIX files.  Compact formats such as 'qcow2' represent unallocated
// and zero regions efficiently so file size may be smaller than
// virtual disk size.  The values are upper bounds that are guaranteed
// to fit the new image file.  Subsequent modification, such as
// internal snapshot or further bitmap creation, may require
// additional space and is not covered here.
//
// Since: 2.10
type BlockMeasureInfo struct {
	// Size required for a new image file, in bytes, when copying just
	// allocated guest-visible contents.
	Required int64 `json:"required"`
	// Image file size, in bytes, once data has been written to all
	// sectors, when copying just guest-visible contents.
	FullyAllocated int64 `json:"fully-allocated"`
	// Additional size required if all the top-level bitmap metadata
	// in the source image were to be copied to the destination,
	// present only when source and destination both support
	// persistent bitmaps. (since 5.1)
	Bitmaps *int64 `json:"bitmaps,omitempty"`
}

// Information about a QEMU image file
//
// Since: 8.0
type BlockNodeInfo struct {
	// name of the image file
	Filename string `json:"filename"`
	// format of the image file
	Format string `json:"format"`
	// true if image is not cleanly closed
	DirtyFlag *bool `json:"dirty-flag,omitempty"`
	// actual size on disk in bytes of the image
	ActualSize *int64 `json:"actual-size,omitempty"`
	// maximum capacity in bytes of the image
	VirtualSize int64 `json:"virtual-size"`
	// size of a cluster in bytes
	ClusterSize *int64 `json:"cluster-size,omitempty"`
	// true if the image is encrypted
	Encrypted *bool `json:"encrypted,omitempty"`
	// true if the image is compressed (Since 1.7)
	Compressed *bool `json:"compressed,omitempty"`
	// name of the backing file
	BackingFilename *string `json:"backing-filename,omitempty"`
	// full path of the backing file
	FullBackingFilename *string `json:"full-backing-filename,omitempty"`
	// the format of the backing file
	BackingFilenameFormat *string `json:"backing-filename-format,omitempty"`
	// list of VM snapshots
	Snapshots []SnapshotInfo `json:"snapshots,omitempty"`
	// structure supplying additional format-specific information
	// (since 1.7)
	FormatSpecific *ImageInfoSpecific `json:"format-specific,omitempty"`
}

// Statistics of a virtual block device or a block backing device.
//
// Since: 0.14
type BlockStats struct {
	// If the stats are for a virtual block device, the name
	// corresponding to the virtual block device.
	Device *string `json:"device,omitempty"`
	// The qdev ID, or if no ID is assigned, the QOM path of the block
	// device. (since 3.0)
	Qdev *string `json:"qdev,omitempty"`
	// The node name of the device. (Since 2.3)
	NodeName *string `json:"node-name,omitempty"`
	// A @BlockDeviceStats for the device.
	Stats BlockDeviceStats `json:"stats"`
	// Optional driver-specific stats. (Since 4.2)
	DriverSpecific *BlockStatsSpecific `json:"driver-specific,omitempty"`
	// This describes the file block device if it has one. Contains
	// recursively the statistics of the underlying protocol (e.g. the
	// host file for a qcow2 image). If there is no underlying
	// protocol, this field is omitted
	Parent *BlockStats `json:"parent,omitempty"`
	// This describes the backing block device if it has one. (Since
	// 2.0)
	Backing *BlockStats `json:"backing,omitempty"`
}

// File driver statistics
//
// Since: 4.2
type BlockStatsSpecificFile struct {
	// The number of successful discard operations performed by the
	// driver.
	DiscardNbOk uint64 `json:"discard-nb-ok"`
	// The number of failed discard operations performed by the
	// driver.
	DiscardNbFailed uint64 `json:"discard-nb-failed"`
	// The number of bytes discarded by the driver.
	DiscardBytesOk uint64 `json:"discard-bytes-ok"`
}

// NVMe driver statistics
//
// Since: 5.2
type BlockStatsSpecificNvme struct {
	// The number of completion errors.
	CompletionErrors uint64 `json:"completion-errors"`
	// The number of aligned accesses performed by the driver.
	AlignedAccesses uint64 `json:"aligned-accesses"`
	// The number of unaligned accesses performed by the driver.
	UnalignedAccesses uint64 `json:"unaligned-accesses"`
}

// Driver specific image amend options for LUKS.
//
// Since: 5.1
type BlockdevAmendOptionsLUKS struct {
	// the desired state of the keyslots
	State QCryptoBlockLUKSKeyslotState `json:"state"`
	// The ID of a QCryptoSecret object providing the password to be
	// written into added active keyslots
	NewSecret *string `json:"new-secret,omitempty"`
	// Optional (for deactivation only) If given will deactivate all
	// keyslots that match password located in QCryptoSecret with this
	// ID
	OldSecret *string `json:"old-secret,omitempty"`
	// Optional. ID of the keyslot to activate/deactivate. For keyslot
	// activation, keyslot should not be active already (this is
	// unsafe to update an active keyslot), but possible if 'force'
	// parameter is given. If keyslot is not given, first free keyslot
	// will be written. For keyslot deactivation, this parameter
	// specifies the exact keyslot to deactivate
	Keyslot *int64 `json:"keyslot,omitempty"`
	// Optional (for activation only) Number of milliseconds to spend
	// in PBKDF passphrase processing for the newly activated keyslot.
	// Currently defaults to 2000.
	IterTime *int64 `json:"iter-time,omitempty"`
	// Optional. The ID of a QCryptoSecret object providing the
	// password to use to retrieve current master key. Defaults to the
	// same secret that was used to open the image
	Secret *string `json:"secret,omitempty"`
}

// Driver specific image amend options for qcow2.  For now, only
// encryption options can be amended
//
// Since: 5.1
type BlockdevAmendOptionsQcow2 struct {
	// Encryption options to be amended
	Encrypt *QCryptoBlockAmendOptions `json:"encrypt,omitempty"`
}

// Since: 2.3
type BlockdevBackup struct {
	// identifier for the newly-created block job. If omitted, the
	// device name will be used. (Since 2.7)
	JobId *string `json:"job-id,omitempty"`
	// the device name or node-name of a root node which should be
	// copied.
	Device string `json:"device"`
	// what parts of the disk image should be copied to the
	// destination (all the disk, only the sectors allocated in the
	// topmost image, from a dirty bitmap, or only new I/O).
	Sync MirrorSyncMode `json:"sync"`
	// the maximum speed, in bytes per second. The default is 0, for
	// unlimited.
	Speed *int64 `json:"speed,omitempty"`
	// The name of a dirty bitmap to use. Must be present if sync is
	// "bitmap" or "incremental". Can be present if sync is "full" or
	// "top". Must not be present otherwise. (Since 2.4 (drive-
	// backup), 3.1 (blockdev-backup))
	Bitmap *string `json:"bitmap,omitempty"`
	// Specifies the type of data the bitmap should contain after the
	// operation concludes. Must be present if a bitmap was provided,
	// Must NOT be present otherwise. (Since 4.2)
	BitmapMode *BitmapSyncMode `json:"bitmap-mode,omitempty"`
	// true to compress data, if the target format supports it.
	// (default: false) (since 2.8)
	Compress *bool `json:"compress,omitempty"`
	// the action to take on an error on the source, default 'report'.
	// 'stop' and 'enospc' can only be used if the block device
	// supports io-status (see BlockInfo).
	OnSourceError *BlockdevOnError `json:"on-source-error,omitempty"`
	// the action to take on an error on the target, default 'report'
	// (no limitations, since this applies to a different block device
	// than @device).
	OnTargetError *BlockdevOnError `json:"on-target-error,omitempty"`
	// When false, this job will wait in a PENDING state after it has
	// finished its work, waiting for @block-job-finalize before
	// making any block graph changes. When true, this job will
	// automatically perform its abort or commit actions. Defaults to
	// true. (Since 2.12)
	AutoFinalize *bool `json:"auto-finalize,omitempty"`
	// When false, this job will wait in a CONCLUDED state after it
	// has completely ceased all work, and awaits @block-job-dismiss.
	// When true, this job will automatically disappear from the query
	// list without user intervention. Defaults to true. (Since 2.12)
	AutoDismiss *bool `json:"auto-dismiss,omitempty"`
	// the node name that should be assigned to the filter driver that
	// the backup job inserts into the graph above node specified by
	// @drive. If this option is not given, a node name is
	// autogenerated. (Since: 4.2)
	FilterNodeName *string `json:"filter-node-name,omitempty"`
	// Discard blocks on source which have already been copied to the
	// target. (Since 9.1)
	DiscardSource *bool `json:"discard-source,omitempty"`
	// Performance options. (Since 6.0)
	XPerf *BackupPerf `json:"x-perf,omitempty"`
	// the device name or node-name of the backup target node.
	Target string `json:"target"`
}

// Since: 2.3
type BlockdevBackupWrapper struct {
	Data BlockdevBackup `json:"data"`
}

// Cache mode information for a block device
//
// Since: 2.3
type BlockdevCacheInfo struct {
	// true if writeback mode is enabled
	Writeback bool `json:"writeback"`
	// true if the host page cache is bypassed (O_DIRECT)
	Direct bool `json:"direct"`
	// true if flush requests are ignored for the device
	NoFlush bool `json:"no-flush"`
}

// Includes cache-related options for block devices
//
// Since: 2.9
type BlockdevCacheOptions struct {
	// enables use of O_DIRECT (bypass the host page cache; default:
	// false)
	Direct *bool `json:"direct,omitempty"`
	// ignore any flush requests for the device (default: false)
	NoFlush *bool `json:"no-flush,omitempty"`
}

// Driver specific image creation options for file.
//
// Since: 2.12
type BlockdevCreateOptionsFile struct {
	// Filename for the new image file
	Filename string `json:"filename"`
	// Size of the virtual disk in bytes
	Size uint64 `json:"size"`
	// Preallocation mode for the new image (default: off; allowed
	// values: off, falloc (if CONFIG_POSIX_FALLOCATE), full (if
	// CONFIG_POSIX))
	Preallocation *PreallocMode `json:"preallocation,omitempty"`
	// Turn off copy-on-write (valid only on btrfs; default: off)
	Nocow *bool `json:"nocow,omitempty"`
	// Extent size hint to add to the image file; 0 for not adding an
	// extent size hint (default: 1 MB, since 5.1)
	ExtentSizeHint *uint64 `json:"extent-size-hint,omitempty"`
}

// Driver specific image creation options for gluster.
//
// Since: 2.12
type BlockdevCreateOptionsGluster struct {
	// Where to store the new image file
	Location BlockdevOptionsGluster `json:"location"`
	// Size of the virtual disk in bytes
	Size uint64 `json:"size"`
	// Preallocation mode for the new image (default: off; allowed
	// values: off, falloc (if CONFIG_GLUSTERFS_FALLOCATE), full (if
	// CONFIG_GLUSTERFS_ZEROFILL))
	Preallocation *PreallocMode `json:"preallocation,omitempty"`
}

// Driver specific image creation options for LUKS.
//
// Since: 2.12
type BlockdevCreateOptionsLUKS struct {
	// the ID of a QCryptoSecret object providing the decryption key.
	// Mandatory except when probing image for metadata only.
	KeySecret *string `json:"key-secret,omitempty"`
	// the cipher algorithm for data encryption Currently defaults to
	// 'aes-256'.
	CipherAlg *QCryptoCipherAlgo `json:"cipher-alg,omitempty"`
	// the cipher mode for data encryption Currently defaults to 'xts'
	CipherMode *QCryptoCipherMode `json:"cipher-mode,omitempty"`
	// the initialization vector generator Currently defaults to
	// 'plain64'
	IvgenAlg *QCryptoIVGenAlgo `json:"ivgen-alg,omitempty"`
	// the initialization vector generator hash Currently defaults to
	// 'sha256'
	IvgenHashAlg *QCryptoHashAlgo `json:"ivgen-hash-alg,omitempty"`
	// the master key hash algorithm Currently defaults to 'sha256'
	HashAlg *QCryptoHashAlgo `json:"hash-alg,omitempty"`
	// number of milliseconds to spend in PBKDF passphrase processing.
	// Currently defaults to 2000. (since 2.8)
	IterTime *int64 `json:"iter-time,omitempty"`
	// Node to create the image format on, mandatory except when
	// 'preallocation' is not requested
	File *BlockdevRef `json:"file,omitempty"`
	// Block device holding a detached LUKS header. (since 9.0)
	Header *BlockdevRef `json:"header,omitempty"`
	// Size of the virtual disk in bytes
	Size uint64 `json:"size"`
	// Preallocation mode for the new image (since: 4.2) (default:
	// off; allowed values: off, metadata, falloc, full)
	Preallocation *PreallocMode `json:"preallocation,omitempty"`
}

// Driver specific image creation options for NFS.
//
// Since: 2.12
type BlockdevCreateOptionsNfs struct {
	// Where to store the new image file
	Location BlockdevOptionsNfs `json:"location"`
	// Size of the virtual disk in bytes
	Size uint64 `json:"size"`
}

// Driver specific image creation options for parallels.
//
// Since: 2.12
type BlockdevCreateOptionsParallels struct {
	// Node to create the image format on
	File BlockdevRef `json:"file"`
	// Size of the virtual disk in bytes
	Size uint64 `json:"size"`
	// Cluster size in bytes (default: 1 MB)
	ClusterSize *uint64 `json:"cluster-size,omitempty"`
}

// Driver specific image creation options for qcow.
//
// Since: 2.12
type BlockdevCreateOptionsQcow struct {
	// Node to create the image format on
	File BlockdevRef `json:"file"`
	// Size of the virtual disk in bytes
	Size uint64 `json:"size"`
	// File name of the backing file if a backing file should be used
	BackingFile *string `json:"backing-file,omitempty"`
	// Encryption options if the image should be encrypted
	Encrypt *QCryptoBlockCreateOptions `json:"encrypt,omitempty"`
}

// Driver specific image creation options for qcow2.
//
// Since: 2.12
type BlockdevCreateOptionsQcow2 struct {
	// Node to create the image format on
	File BlockdevRef `json:"file"`
	// Node to use as an external data file in which all guest data is
	// stored so that only metadata remains in the qcow2 file (since:
	// 4.0)
	DataFile *BlockdevRef `json:"data-file,omitempty"`
	// True if the external data file must stay valid as a standalone
	// (read-only) raw image without looking at qcow2 metadata
	// (default: false; since: 4.0)
	DataFileRaw *bool `json:"data-file-raw,omitempty"`
	// True to make the image have extended L2 entries (default:
	// false; since 5.2)
	ExtendedL2 *bool `json:"extended-l2,omitempty"`
	// Size of the virtual disk in bytes
	Size uint64 `json:"size"`
	// Compatibility level (default: v3)
	Version *BlockdevQcow2Version `json:"version,omitempty"`
	// File name of the backing file if a backing file should be used
	BackingFile *string `json:"backing-file,omitempty"`
	// Name of the block driver to use for the backing file
	BackingFmt *BlockdevDriver `json:"backing-fmt,omitempty"`
	// Encryption options if the image should be encrypted
	Encrypt *QCryptoBlockCreateOptions `json:"encrypt,omitempty"`
	// qcow2 cluster size in bytes (default: 65536)
	ClusterSize *uint64 `json:"cluster-size,omitempty"`
	// Preallocation mode for the new image (default: off; allowed
	// values: off, falloc, full, metadata)
	Preallocation *PreallocMode `json:"preallocation,omitempty"`
	// True if refcounts may be updated lazily (default: off)
	LazyRefcounts *bool `json:"lazy-refcounts,omitempty"`
	// Width of reference counts in bits (default: 16)
	RefcountBits *int64 `json:"refcount-bits,omitempty"`
	// The image cluster compression method (default: zlib, since 5.1)
	CompressionType *Qcow2CompressionType `json:"compression-type,omitempty"`
}

// Driver specific image creation options for qed.
//
// Since: 2.12
type BlockdevCreateOptionsQed struct {
	// Node to create the image format on
	File BlockdevRef `json:"file"`
	// Size of the virtual disk in bytes
	Size uint64 `json:"size"`
	// File name of the backing file if a backing file should be used
	BackingFile *string `json:"backing-file,omitempty"`
	// Name of the block driver to use for the backing file
	BackingFmt *BlockdevDriver `json:"backing-fmt,omitempty"`
	// Cluster size in bytes (default: 65536)
	ClusterSize *uint64 `json:"cluster-size,omitempty"`
	// L1/L2 table size (in clusters)
	TableSize *int64 `json:"table-size,omitempty"`
}

// Driver specific image creation options for rbd/Ceph.
//
// Since: 2.12
type BlockdevCreateOptionsRbd struct {
	// Where to store the new image file. This location cannot point
	// to a snapshot.
	Location BlockdevOptionsRbd `json:"location"`
	// Size of the virtual disk in bytes
	Size uint64 `json:"size"`
	// RBD object size
	ClusterSize *uint64 `json:"cluster-size,omitempty"`
	// Image encryption options. (Since 6.1)
	Encrypt *RbdEncryptionCreateOptions `json:"encrypt,omitempty"`
}

// Driver specific image creation options for SSH.
//
// Since: 2.12
type BlockdevCreateOptionsSsh struct {
	// Where to store the new image file
	Location BlockdevOptionsSsh `json:"location"`
	// Size of the virtual disk in bytes
	Size uint64 `json:"size"`
}

// Driver specific image creation options for VDI.
//
// Since: 2.12
type BlockdevCreateOptionsVdi struct {
	// Node to create the image format on
	File BlockdevRef `json:"file"`
	// Size of the virtual disk in bytes
	Size uint64 `json:"size"`
	// Preallocation mode for the new image (default: off; allowed
	// values: off, metadata)
	Preallocation *PreallocMode `json:"preallocation,omitempty"`
}

// Driver specific image creation options for vhdx.
//
// Since: 2.12
type BlockdevCreateOptionsVhdx struct {
	// Node to create the image format on
	File BlockdevRef `json:"file"`
	// Size of the virtual disk in bytes
	Size uint64 `json:"size"`
	// Log size in bytes, must be a multiple of 1 MB (default: 1 MB)
	LogSize *uint64 `json:"log-size,omitempty"`
	// Block size in bytes, must be a multiple of 1 MB and not larger
	// than 256 MB (default: automatically choose a block size
	// depending on the image size)
	BlockSize *uint64 `json:"block-size,omitempty"`
	// vhdx subformat (default: dynamic)
	Subformat *BlockdevVhdxSubformat `json:"subformat,omitempty"`
	// Force use of payload blocks of type 'ZERO'. Non-standard, but
	// default. Do not set to 'off' when using 'qemu-img convert' with
	// subformat=dynamic.
	BlockStateZero *bool `json:"block-state-zero,omitempty"`
}

// Driver specific image creation options for VMDK.
//
// Since: 4.0
type BlockdevCreateOptionsVmdk struct {
	// Where to store the new image file. This refers to the image
	// file for monolithcSparse and streamOptimized format, or the
	// descriptor file for other formats.
	File BlockdevRef `json:"file"`
	// Size of the virtual disk in bytes
	Size uint64 `json:"size"`
	// Where to store the data extents. Required for monolithcFlat,
	// twoGbMaxExtentSparse and twoGbMaxExtentFlat formats. For
	// monolithicFlat, only one entry is required; for twoGbMaxExtent*
	// formats, the number of entries required is calculated as
	// extent_number = virtual_size / 2GB. Providing more extents than
	// will be used is an error.
	Extents []BlockdevRef `json:"extents,omitempty"`
	// The subformat of the VMDK image. Default: "monolithicSparse".
	Subformat *BlockdevVmdkSubformat `json:"subformat,omitempty"`
	// The path of backing file. Default: no backing file is used.
	BackingFile *string `json:"backing-file,omitempty"`
	// The adapter type used to fill in the descriptor. Default: ide.
	AdapterType *BlockdevVmdkAdapterType `json:"adapter-type,omitempty"`
	// Hardware version. The meaningful options are "4" or "6".
	// Default: "4".
	Hwversion *string `json:"hwversion,omitempty"`
	// VMware guest tools version. Default: "2147483647" (Since 6.2)
	Toolsversion *string `json:"toolsversion,omitempty"`
	// Whether to enable zeroed-grain feature for sparse subformats.
	// Default: false.
	ZeroedGrain *bool `json:"zeroed-grain,omitempty"`
}

// Driver specific image creation options for vpc (VHD).
//
// Since: 2.12
type BlockdevCreateOptionsVpc struct {
	// Node to create the image format on
	File BlockdevRef `json:"file"`
	// Size of the virtual disk in bytes
	Size uint64 `json:"size"`
	// vhdx subformat (default: dynamic)
	Subformat *BlockdevVpcSubformat `json:"subformat,omitempty"`
	// Force use of the exact byte size instead of rounding to the
	// next size that can be represented in CHS geometry (default:
	// false)
	ForceSize *bool `json:"force-size,omitempty"`
}

// Driver specific block device options for blkdebug.
//
// Since: 2.9
type BlockdevOptionsBlkdebug struct {
	// underlying raw block device (or image file)
	Image BlockdevRef `json:"image"`
	// filename of the configuration file
	Config *string `json:"config,omitempty"`
	// required alignment for requests in bytes, must be positive
	// power of 2, or 0 for default
	Align *int64 `json:"align,omitempty"`
	// maximum size for I/O transfers in bytes, must be positive
	// multiple of @align and of the underlying file's request
	// alignment (but need not be a power of 2), or 0 for default
	// (since 2.10)
	MaxTransfer *int32 `json:"max-transfer,omitempty"`
	// preferred alignment for write zero requests in bytes, must be
	// positive multiple of @align and of the underlying file's
	// request alignment (but need not be a power of 2), or 0 for
	// default (since 2.10)
	OptWriteZero *int32 `json:"opt-write-zero,omitempty"`
	// maximum size for write zero requests in bytes, must be positive
	// multiple of @align, of @opt-write-zero, and of the underlying
	// file's request alignment (but need not be a power of 2), or 0
	// for default (since 2.10)
	MaxWriteZero *int32 `json:"max-write-zero,omitempty"`
	// preferred alignment for discard requests in bytes, must be
	// positive multiple of @align and of the underlying file's
	// request alignment (but need not be a power of 2), or 0 for
	// default (since 2.10)
	OptDiscard *int32 `json:"opt-discard,omitempty"`
	// maximum size for discard requests in bytes, must be positive
	// multiple of @align, of @opt-discard, and of the underlying
	// file's request alignment (but need not be a power of 2), or 0
	// for default (since 2.10)
	MaxDiscard *int32 `json:"max-discard,omitempty"`
	// array of error injection descriptions
	InjectError []BlkdebugInjectErrorOptions `json:"inject-error,omitempty"`
	// array of state-change descriptions
	SetState []BlkdebugSetStateOptions `json:"set-state,omitempty"`
	// Permissions to take on @image in addition to what is necessary
	// anyway (which depends on how the blkdebug node is used).
	// Defaults to none. (since 5.0)
	TakeChildPerms []BlockPermission `json:"take-child-perms,omitempty"`
	// Permissions not to share on @image in addition to what cannot
	// be shared anyway (which depends on how the blkdebug node is
	// used). Defaults to none. (since 5.0)
	UnshareChildPerms []BlockPermission `json:"unshare-child-perms,omitempty"`
}

// Driver specific block device options for blklogwrites.
//
// Since: 3.0
type BlockdevOptionsBlklogwrites struct {
	// block device
	File BlockdevRef `json:"file"`
	// block device used to log writes to @file
	Log BlockdevRef `json:"log"`
	// sector size used in logging writes to @file, determines
	// granularity of offsets and sizes of writes (default: 512)
	LogSectorSize *uint32 `json:"log-sector-size,omitempty"`
	// append to an existing log (default: false)
	LogAppend *bool `json:"log-append,omitempty"`
	// interval of write requests after which the log super block is
	// updated to disk (default: 4096)
	LogSuperUpdateInterval *uint64 `json:"log-super-update-interval,omitempty"`
}

// Driver specific block device options for blkreplay.
//
// Since: 4.2
type BlockdevOptionsBlkreplay struct {
	// disk image which should be controlled with blkreplay
	Image BlockdevRef `json:"image"`
}

// Driver specific block device options for blkverify.
//
// Since: 2.9
type BlockdevOptionsBlkverify struct {
	// block device to be tested
	Test BlockdevRef `json:"test"`
	// raw image used for verification
	Raw BlockdevRef `json:"raw"`
}

// Driver specific block device options for the copy-before-write
// driver, which does so called copy-before-write operations: when
// data is written to the filter, the filter first reads corresponding
// blocks from its file child and copies them to @target child.  After
// successfully copying, the write request is propagated to file
// child. If copying fails, the original write request is failed too
// and no data is written to file child.
//
// Since: 6.2
type BlockdevOptionsCbw struct {
	// reference to or definition of the data source block device
	File BlockdevRef `json:"file"`
	// The target for copy-before-write operations.
	Target BlockdevRef `json:"target"`
	// If specified, copy-before-write filter will do copy-before-
	// write operations only for dirty regions of the bitmap. Bitmap
	// size must be equal to length of file and target child of the
	// filter. Note also, that bitmap is used only to initialize
	// internal bitmap of the process, so further modifications (or
	// removing) of specified bitmap doesn't influence the filter.
	// (Since 7.0)
	Bitmap *BlockDirtyBitmap `json:"bitmap,omitempty"`
	// Behavior on failure of copy-before-write operation. Default is
	// @break-guest-write. (Since 7.1)
	OnCbwError *OnCbwError `json:"on-cbw-error,omitempty"`
	// Zero means no limit. Non-zero sets the timeout in seconds for
	// copy-before-write operation. When a timeout occurs, the
	// respective copy-before-write operation will fail, and the @on-
	// cbw-error parameter will decide how this failure is handled.
	// Default 0. (Since 7.1)
	CbwTimeout *uint32 `json:"cbw-timeout,omitempty"`
	// Minimum size of blocks used by copy-before-write operations.
	// Has to be a power of 2. No effect if smaller than the maximum
	// of the target's cluster size and 64 KiB. Default 0. (Since 9.2)
	MinClusterSize *uint64 `json:"min-cluster-size,omitempty"`
}

// Driver specific block device options for the copy-on-read driver.
//
// Since: 6.0
type BlockdevOptionsCor struct {
	// reference to or definition of the data source block device
	File BlockdevRef `json:"file"`
	// The name of a non-filter node (allocation-bearing layer) that
	// limits the COR operations in the backing chain (inclusive), so
	// that no data below this node will be copied by this filter. If
	// option is absent, the limit is not applied, so that data from
	// all backing layers may be copied.
	Bottom *string `json:"bottom,omitempty"`
}

// Driver specific block device options shared by all protocols
// supported by the curl backend.
//
// Since: 2.9
type BlockdevOptionsCurlBase struct {
	// URL of the image file
	Url string `json:"url"`
	// Size of the read-ahead cache; must be a multiple of 512
	// (defaults to 256 kB)
	Readahead *int64 `json:"readahead,omitempty"`
	// Timeout for connections, in seconds (defaults to 5)
	Timeout *int64 `json:"timeout,omitempty"`
	// Username for authentication (defaults to none)
	Username *string `json:"username,omitempty"`
	// ID of a QCryptoSecret object providing a password for
	// authentication (defaults to no password)
	PasswordSecret *string `json:"password-secret,omitempty"`
	// Username for proxy authentication (defaults to none)
	ProxyUsername *string `json:"proxy-username,omitempty"`
	// ID of a QCryptoSecret object providing a password for proxy
	// authentication (defaults to no password)
	ProxyPasswordSecret *string `json:"proxy-password-secret,omitempty"`
}

// Driver specific block device options for FTP connections over the
// curl backend.  URLs must start with "ftp://".
//
// Since: 2.9
type BlockdevOptionsCurlFtp struct {
	// URL of the image file
	Url string `json:"url"`
	// Size of the read-ahead cache; must be a multiple of 512
	// (defaults to 256 kB)
	Readahead *int64 `json:"readahead,omitempty"`
	// Timeout for connections, in seconds (defaults to 5)
	Timeout *int64 `json:"timeout,omitempty"`
	// Username for authentication (defaults to none)
	Username *string `json:"username,omitempty"`
	// ID of a QCryptoSecret object providing a password for
	// authentication (defaults to no password)
	PasswordSecret *string `json:"password-secret,omitempty"`
	// Username for proxy authentication (defaults to none)
	ProxyUsername *string `json:"proxy-username,omitempty"`
	// ID of a QCryptoSecret object providing a password for proxy
	// authentication (defaults to no password)
	ProxyPasswordSecret *string `json:"proxy-password-secret,omitempty"`
}

// Driver specific block device options for FTPS connections over the
// curl backend.  URLs must start with "ftps://".
//
// Since: 2.9
type BlockdevOptionsCurlFtps struct {
	// URL of the image file
	Url string `json:"url"`
	// Size of the read-ahead cache; must be a multiple of 512
	// (defaults to 256 kB)
	Readahead *int64 `json:"readahead,omitempty"`
	// Timeout for connections, in seconds (defaults to 5)
	Timeout *int64 `json:"timeout,omitempty"`
	// Username for authentication (defaults to none)
	Username *string `json:"username,omitempty"`
	// ID of a QCryptoSecret object providing a password for
	// authentication (defaults to no password)
	PasswordSecret *string `json:"password-secret,omitempty"`
	// Username for proxy authentication (defaults to none)
	ProxyUsername *string `json:"proxy-username,omitempty"`
	// ID of a QCryptoSecret object providing a password for proxy
	// authentication (defaults to no password)
	ProxyPasswordSecret *string `json:"proxy-password-secret,omitempty"`
	// Whether to verify the SSL certificate's validity (defaults to
	// true)
	Sslverify *bool `json:"sslverify,omitempty"`
}

// Driver specific block device options for HTTP connections over the
// curl backend.  URLs must start with "http://".
//
// Since: 2.9
type BlockdevOptionsCurlHttp struct {
	// URL of the image file
	Url string `json:"url"`
	// Size of the read-ahead cache; must be a multiple of 512
	// (defaults to 256 kB)
	Readahead *int64 `json:"readahead,omitempty"`
	// Timeout for connections, in seconds (defaults to 5)
	Timeout *int64 `json:"timeout,omitempty"`
	// Username for authentication (defaults to none)
	Username *string `json:"username,omitempty"`
	// ID of a QCryptoSecret object providing a password for
	// authentication (defaults to no password)
	PasswordSecret *string `json:"password-secret,omitempty"`
	// Username for proxy authentication (defaults to none)
	ProxyUsername *string `json:"proxy-username,omitempty"`
	// ID of a QCryptoSecret object providing a password for proxy
	// authentication (defaults to no password)
	ProxyPasswordSecret *string `json:"proxy-password-secret,omitempty"`
	// List of cookies to set; format is "name1=content1;
	// name2=content2;" as explained by CURLOPT_COOKIE(3). Defaults to
	// no cookies.
	Cookie *string `json:"cookie,omitempty"`
	// ID of a QCryptoSecret object providing the cookie data in a
	// secure way. See @cookie for the format. (since 2.10)
	CookieSecret *string `json:"cookie-secret,omitempty"`
}

// Driver specific block device options for HTTPS connections over the
// curl backend.  URLs must start with "https://".
//
// Since: 2.9
type BlockdevOptionsCurlHttps struct {
	// URL of the image file
	Url string `json:"url"`
	// Size of the read-ahead cache; must be a multiple of 512
	// (defaults to 256 kB)
	Readahead *int64 `json:"readahead,omitempty"`
	// Timeout for connections, in seconds (defaults to 5)
	Timeout *int64 `json:"timeout,omitempty"`
	// Username for authentication (defaults to none)
	Username *string `json:"username,omitempty"`
	// ID of a QCryptoSecret object providing a password for
	// authentication (defaults to no password)
	PasswordSecret *string `json:"password-secret,omitempty"`
	// Username for proxy authentication (defaults to none)
	ProxyUsername *string `json:"proxy-username,omitempty"`
	// ID of a QCryptoSecret object providing a password for proxy
	// authentication (defaults to no password)
	ProxyPasswordSecret *string `json:"proxy-password-secret,omitempty"`
	// List of cookies to set; format is "name1=content1;
	// name2=content2;" as explained by CURLOPT_COOKIE(3). Defaults to
	// no cookies.
	Cookie *string `json:"cookie,omitempty"`
	// Whether to verify the SSL certificate's validity (defaults to
	// true)
	Sslverify *bool `json:"sslverify,omitempty"`
	// ID of a QCryptoSecret object providing the cookie data in a
	// secure way. See @cookie for the format. (since 2.10)
	CookieSecret *string `json:"cookie-secret,omitempty"`
}

// Driver specific block device options for the file backend.
//
// Since: 2.9
type BlockdevOptionsFile struct {
	// path to the image file
	Filename string `json:"filename"`
	// the id for the object that will handle persistent reservations
	// for this device (default: none, forward the commands via SG_IO;
	// since 2.11)
	PrManager *string `json:"pr-manager,omitempty"`
	// whether to enable file locking. If set to 'auto', only enable
	// when Open File Descriptor (OFD) locking API is available
	// (default: auto, since 2.10)
	Locking *OnOffAuto `json:"locking,omitempty"`
	// AIO backend (default: threads) (since: 2.8)
	Aio *BlockdevAioOptions `json:"aio,omitempty"`
	// maximum number of requests to batch together into a single
	// submission in the AIO backend. The smallest value between this
	// and the aio-max-batch value of the IOThread object is chosen. 0
	// means that the AIO backend will handle it automatically.
	// (default: 0, since 6.2)
	AioMaxBatch *int64 `json:"aio-max-batch,omitempty"`
	// invalidate page cache during live migration. This prevents
	// stale data on the migration destination with cache.direct=off.
	// Currently only supported on Linux hosts. (default: on, since:
	// 4.0)
	DropCache *bool `json:"drop-cache,omitempty"`
	// whether to check that page cache was dropped on live migration.
	// May cause noticeable delays if the image file is large, do not
	// use in production. (default: off) (since: 3.0)
	XCheckCacheDropped *bool `json:"x-check-cache-dropped,omitempty"`
}

// Driver specific block device options for image format that have no
// option besides their data source and an optional backing file.
//
// Since: 2.9
type BlockdevOptionsGenericCOWFormat struct {
	// reference to or definition of the data source block device
	File BlockdevRef `json:"file"`
	// reference to or definition of the backing file block device,
	// null disables the backing file entirely. Defaults to the
	// backing file stored the image file.
	Backing *BlockdevRefOrNull `json:"backing"`
}

func (s BlockdevOptionsGenericCOWFormat) MarshalJSON() ([]byte, error) {
	m := make(map[string]any)
	m["file"] = s.File

	if val, absent := s.Backing.ToAnyOrAbsent(); !absent {
		m["backing"] = val
	}

	return json.Marshal(&m)
}

func (s *BlockdevOptionsGenericCOWFormat) UnmarshalJSON(data []byte) error {
	tmp := struct {
		// reference to or definition of the data source block device
		File BlockdevRef `json:"file"`
		// reference to or definition of the backing file block
		// device, null disables the backing file entirely. Defaults
		// to the backing file stored the image file.
		Backing BlockdevRefOrNull `json:"backing"`
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.File = tmp.File

	if _, absent := (&tmp.Backing).ToAnyOrAbsent(); !absent {
		s.Backing = &tmp.Backing
	}

	return nil
}

// Driver specific block device options for image format that have no
// option besides their data source.
//
// Since: 2.9
type BlockdevOptionsGenericFormat struct {
	// reference to or definition of the data source block device
	File BlockdevRef `json:"file"`
}

// Driver specific block device options for Gluster
//
// Since: 2.9
type BlockdevOptionsGluster struct {
	// name of gluster volume where VM image resides
	Volume string `json:"volume"`
	// absolute path to image file in gluster volume
	Path string `json:"path"`
	// gluster servers description
	Server []SocketAddress `json:"server"`
	// libgfapi log level (default '4' which is Error) (Since 2.8)
	Debug *int64 `json:"debug,omitempty"`
	// libgfapi log file (default /dev/stderr) (Since 2.8)
	Logfile *string `json:"logfile,omitempty"`
}

// Driver specific block device options for the io_uring backend.
//
// Since: 7.2
type BlockdevOptionsIoUring struct {
	// path to the image file
	Filename string `json:"filename"`
}

// Driver specific block device options for iscsi
//
// Since: 2.9
type BlockdevOptionsIscsi struct {
	// The iscsi transport type
	Transport IscsiTransport `json:"transport"`
	// The address of the iscsi portal
	Portal string `json:"portal"`
	// The target iqn name
	Target string `json:"target"`
	// LUN to connect to. Defaults to 0.
	Lun *int64 `json:"lun,omitempty"`
	// User name to log in with. If omitted, no CHAP authentication is
	// performed.
	User *string `json:"user,omitempty"`
	// The ID of a QCryptoSecret object providing the password for the
	// login. This option is required if @user is specified.
	PasswordSecret *string `json:"password-secret,omitempty"`
	// The iqn name we want to identify to the target as. If this
	// option is not specified, an initiator name is generated
	// automatically.
	InitiatorName *string `json:"initiator-name,omitempty"`
	// The desired header digest. Defaults to none-crc32c.
	HeaderDigest *IscsiHeaderDigest `json:"header-digest,omitempty"`
	// Timeout in seconds after which a request will timeout. 0 means
	// no timeout and is the default.
	Timeout *int64 `json:"timeout,omitempty"`
}

// Driver specific block device options for LUKS.
//
// Since: 2.9
type BlockdevOptionsLUKS struct {
	// reference to or definition of the data source block device
	File BlockdevRef `json:"file"`
	// the ID of a QCryptoSecret object providing the decryption key
	// (since 2.6). Mandatory except when doing a metadata-only probe
	// of the image.
	KeySecret *string `json:"key-secret,omitempty"`
	// block device holding a detached LUKS header. (since 9.0)
	Header *BlockdevRef `json:"header,omitempty"`
}

// Driver specific block device options for the NVMe backend.
//
// Note that the PCI @device must have been unbound from any host
// kernel driver before instructing QEMU to add the blockdev.
//
// Since: 2.12
type BlockdevOptionsNVMe struct {
	// PCI controller address of the NVMe device in format
	// hhhh:bb:ss.f (host:bus:slot.function)
	Device string `json:"device"`
	// namespace number of the device, starting from 1.
	Namespace int64 `json:"namespace"`
}

// Driver specific block device options for NBD.
//
// Since: 2.9
type BlockdevOptionsNbd struct {
	// NBD server address
	Server SocketAddress `json:"server"`
	// export name
	Export *string `json:"export,omitempty"`
	// TLS credentials ID
	TlsCreds *string `json:"tls-creds,omitempty"`
	// TLS hostname override for certificate validation (Since 7.0)
	TlsHostname *string `json:"tls-hostname,omitempty"`
	// A metadata context name such as "qemu:dirty-bitmap:NAME" or
	// "qemu:allocation-depth" to query in place of the traditional
	// "base:allocation" block status (see NBD_OPT_LIST_META_CONTEXT
	// in the NBD protocol; and yes, naming this option x-context
	// would have made more sense) (since 3.0)
	XDirtyBitmap *string `json:"x-dirty-bitmap,omitempty"`
	// On an unexpected disconnect, the nbd client tries to connect
	// again until succeeding or encountering a serious error. During
	// the first @reconnect-delay seconds, all requests are paused and
	// will be rerun on a successful reconnect. After that time, any
	// delayed requests and all future requests before a successful
	// reconnect will immediately fail. Default 0 (Since 4.2)
	ReconnectDelay *uint32 `json:"reconnect-delay,omitempty"`
	// In seconds. If zero, the nbd driver tries the connection only
	// once, and fails to open if the connection fails. If non-zero,
	// the nbd driver will repeat connection attempts until successful
	// or until @open-timeout seconds have elapsed. Default 0 (Since
	// 7.0)
	OpenTimeout *uint32 `json:"open-timeout,omitempty"`
}

// Driver specific block device option for NFS
//
// Since: 2.9
type BlockdevOptionsNfs struct {
	// host address
	Server NFSServer `json:"server"`
	// path of the image on the host
	Path string `json:"path"`
	// UID value to use when talking to the server (defaults to 65534
	// on Windows and getuid() on unix)
	User *int64 `json:"user,omitempty"`
	// GID value to use when talking to the server (defaults to 65534
	// on Windows and getgid() in unix)
	Group *int64 `json:"group,omitempty"`
	// number of SYNs during the session establishment (defaults to
	// libnfs default)
	TcpSynCount *int64 `json:"tcp-syn-count,omitempty"`
	// set the readahead size in bytes (defaults to libnfs default)
	ReadaheadSize *int64 `json:"readahead-size,omitempty"`
	// set the pagecache size in bytes (defaults to libnfs default)
	PageCacheSize *int64 `json:"page-cache-size,omitempty"`
	// set the NFS debug level (max 2) (defaults to libnfs default)
	Debug *int64 `json:"debug,omitempty"`
}

// Driver specific block device options for the null backend.
//
// Since: 2.9
type BlockdevOptionsNull struct {
	// size of the device in bytes.
	Size *int64 `json:"size,omitempty"`
	// emulated latency (in nanoseconds) in processing requests.
	// Default to zero which completes requests immediately. (Since
	// 2.4)
	LatencyNs *uint64 `json:"latency-ns,omitempty"`
	// if true, reads from the device produce zeroes; if false, the
	// buffer is left unchanged. (default: false; since: 4.1)
	ReadZeroes *bool `json:"read-zeroes,omitempty"`
}

// Driver specific block device options for the nvme-io_uring backend.
//
// Since: 7.2
type BlockdevOptionsNvmeIoUring struct {
	// path to the NVMe namespace's character device (e.g.
	// /dev/ng0n1).
	Path string `json:"path"`
}

// Filter driver intended to be inserted between format and protocol
// node and do preallocation in protocol node on write.
//
// Since: 6.0
type BlockdevOptionsPreallocate struct {
	// reference to or definition of the data source block device
	File BlockdevRef `json:"file"`
	// on preallocation, align file length to this number, default
	// 1048576 (1M)
	PreallocAlign *int64 `json:"prealloc-align,omitempty"`
	// how much to preallocate, default 134217728 (128M)
	PreallocSize *int64 `json:"prealloc-size,omitempty"`
}

// Driver specific block device options for qcow.
//
// Since: 2.10
type BlockdevOptionsQcow struct {
	// reference to or definition of the data source block device
	File BlockdevRef `json:"file"`
	// reference to or definition of the backing file block device,
	// null disables the backing file entirely. Defaults to the
	// backing file stored the image file.
	Backing *BlockdevRefOrNull `json:"backing"`
	// Image decryption options. Mandatory for encrypted images,
	// except when doing a metadata-only probe of the image.
	Encrypt *BlockdevQcowEncryption `json:"encrypt,omitempty"`
}

func (s BlockdevOptionsQcow) MarshalJSON() ([]byte, error) {
	m := make(map[string]any)
	m["file"] = s.File

	if s.Encrypt != nil {
		m["encrypt"] = s.Encrypt
	}

	if val, absent := s.Backing.ToAnyOrAbsent(); !absent {
		m["backing"] = val
	}

	return json.Marshal(&m)
}

func (s *BlockdevOptionsQcow) UnmarshalJSON(data []byte) error {
	tmp := struct {
		// reference to or definition of the data source block device
		File BlockdevRef `json:"file"`
		// reference to or definition of the backing file block
		// device, null disables the backing file entirely. Defaults
		// to the backing file stored the image file.
		Backing BlockdevRefOrNull `json:"backing"`
		// Image decryption options. Mandatory for encrypted images,
		// except when doing a metadata-only probe of the image.
		Encrypt *BlockdevQcowEncryption `json:"encrypt,omitempty"`
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.File = tmp.File
	s.Encrypt = tmp.Encrypt

	if _, absent := (&tmp.Backing).ToAnyOrAbsent(); !absent {
		s.Backing = &tmp.Backing
	}

	return nil
}

// Driver specific block device options for qcow2.
//
// Since: 2.9
type BlockdevOptionsQcow2 struct {
	// reference to or definition of the data source block device
	File BlockdevRef `json:"file"`
	// reference to or definition of the backing file block device,
	// null disables the backing file entirely. Defaults to the
	// backing file stored the image file.
	Backing *BlockdevRefOrNull `json:"backing"`
	// whether to enable the lazy refcounts feature (default is taken
	// from the image file)
	LazyRefcounts *bool `json:"lazy-refcounts,omitempty"`
	// whether discard requests to the qcow2 device should be
	// forwarded to the data source
	PassDiscardRequest *bool `json:"pass-discard-request,omitempty"`
	// whether discard requests for the data source should be issued
	// when a snapshot operation (e.g. deleting a snapshot) frees
	// clusters in the qcow2 file
	PassDiscardSnapshot *bool `json:"pass-discard-snapshot,omitempty"`
	// whether discard requests for the data source should be issued
	// on other occasions where a cluster gets freed
	PassDiscardOther *bool `json:"pass-discard-other,omitempty"`
	// when enabled, data clusters will remain preallocated when they
	// are no longer used, e.g. because they are discarded or
	// converted to zero clusters. As usual, whether the old data is
	// discarded or kept on the protocol level (i.e. in the image
	// file) depends on the setting of the pass-discard-request
	// option. Keeping the clusters preallocated prevents qcow2
	// fragmentation that would otherwise be caused by freeing and re-
	// allocating them later. Besides potential performance
	// degradation, such fragmentation can lead to increased
	// allocation of clusters past the end of the image file,
	// resulting in image files whose file length can grow much larger
	// than their guest disk size would suggest. If image file length
	// is of concern (e.g. when storing qcow2 images directly on block
	// devices), you should consider enabling this option. (since 8.1)
	DiscardNoUnref *bool `json:"discard-no-unref,omitempty"`
	// which overlap checks to perform for writes to the image,
	// defaults to 'cached' (since 2.2)
	OverlapCheck *Qcow2OverlapChecks `json:"overlap-check,omitempty"`
	// the maximum total size of the L2 table and refcount block
	// caches in bytes (since 2.2)
	CacheSize *int64 `json:"cache-size,omitempty"`
	// the maximum size of the L2 table cache in bytes (since 2.2)
	L2CacheSize *int64 `json:"l2-cache-size,omitempty"`
	// the size of each entry in the L2 cache in bytes. It must be a
	// power of two between 512 and the cluster size. The default
	// value is the cluster size (since 2.12)
	L2CacheEntrySize *int64 `json:"l2-cache-entry-size,omitempty"`
	// the maximum size of the refcount block cache in bytes (since
	// 2.2)
	RefcountCacheSize *int64 `json:"refcount-cache-size,omitempty"`
	// clean unused entries in the L2 and refcount caches. The
	// interval is in seconds. The default value is 600 on supporting
	// platforms, and 0 on other platforms. 0 disables this feature.
	// (since 2.5)
	CacheCleanInterval *int64 `json:"cache-clean-interval,omitempty"`
	// Image decryption options. Mandatory for encrypted images,
	// except when doing a metadata-only probe of the image. (since
	// 2.10)
	Encrypt *BlockdevQcow2Encryption `json:"encrypt,omitempty"`
	// reference to or definition of the external data file. This may
	// only be specified for images that require an external data
	// file. If it is not specified for such an image, the data file
	// name is loaded from the image file. (since 4.0)
	DataFile *BlockdevRef `json:"data-file,omitempty"`
}

func (s BlockdevOptionsQcow2) MarshalJSON() ([]byte, error) {
	m := make(map[string]any)
	m["file"] = s.File

	if s.LazyRefcounts != nil {
		m["lazy-refcounts"] = s.LazyRefcounts
	}

	if s.PassDiscardRequest != nil {
		m["pass-discard-request"] = s.PassDiscardRequest
	}

	if s.PassDiscardSnapshot != nil {
		m["pass-discard-snapshot"] = s.PassDiscardSnapshot
	}

	if s.PassDiscardOther != nil {
		m["pass-discard-other"] = s.PassDiscardOther
	}

	if s.DiscardNoUnref != nil {
		m["discard-no-unref"] = s.DiscardNoUnref
	}

	if s.OverlapCheck != nil {
		m["overlap-check"] = s.OverlapCheck
	}

	if s.CacheSize != nil {
		m["cache-size"] = s.CacheSize
	}

	if s.L2CacheSize != nil {
		m["l2-cache-size"] = s.L2CacheSize
	}

	if s.L2CacheEntrySize != nil {
		m["l2-cache-entry-size"] = s.L2CacheEntrySize
	}

	if s.RefcountCacheSize != nil {
		m["refcount-cache-size"] = s.RefcountCacheSize
	}

	if s.CacheCleanInterval != nil {
		m["cache-clean-interval"] = s.CacheCleanInterval
	}

	if s.Encrypt != nil {
		m["encrypt"] = s.Encrypt
	}

	if s.DataFile != nil {
		m["data-file"] = s.DataFile
	}

	if val, absent := s.Backing.ToAnyOrAbsent(); !absent {
		m["backing"] = val
	}

	return json.Marshal(&m)
}

func (s *BlockdevOptionsQcow2) UnmarshalJSON(data []byte) error {
	tmp := struct {
		// reference to or definition of the data source block device
		File BlockdevRef `json:"file"`
		// reference to or definition of the backing file block
		// device, null disables the backing file entirely. Defaults
		// to the backing file stored the image file.
		Backing BlockdevRefOrNull `json:"backing"`
		// whether to enable the lazy refcounts feature (default is
		// taken from the image file)
		LazyRefcounts *bool `json:"lazy-refcounts,omitempty"`
		// whether discard requests to the qcow2 device should be
		// forwarded to the data source
		PassDiscardRequest *bool `json:"pass-discard-request,omitempty"`
		// whether discard requests for the data source should be
		// issued when a snapshot operation (e.g. deleting a snapshot)
		// frees clusters in the qcow2 file
		PassDiscardSnapshot *bool `json:"pass-discard-snapshot,omitempty"`
		// whether discard requests for the data source should be
		// issued on other occasions where a cluster gets freed
		PassDiscardOther *bool `json:"pass-discard-other,omitempty"`
		// when enabled, data clusters will remain preallocated when
		// they are no longer used, e.g. because they are discarded or
		// converted to zero clusters. As usual, whether the old data
		// is discarded or kept on the protocol level (i.e. in the
		// image file) depends on the setting of the pass-discard-
		// request option. Keeping the clusters preallocated prevents
		// qcow2 fragmentation that would otherwise be caused by
		// freeing and re-allocating them later. Besides potential
		// performance degradation, such fragmentation can lead to
		// increased allocation of clusters past the end of the image
		// file, resulting in image files whose file length can grow
		// much larger than their guest disk size would suggest. If
		// image file length is of concern (e.g. when storing qcow2
		// images directly on block devices), you should consider
		// enabling this option. (since 8.1)
		DiscardNoUnref *bool `json:"discard-no-unref,omitempty"`
		// which overlap checks to perform for writes to the image,
		// defaults to 'cached' (since 2.2)
		OverlapCheck *Qcow2OverlapChecks `json:"overlap-check,omitempty"`
		// the maximum total size of the L2 table and refcount block
		// caches in bytes (since 2.2)
		CacheSize *int64 `json:"cache-size,omitempty"`
		// the maximum size of the L2 table cache in bytes (since 2.2)
		L2CacheSize *int64 `json:"l2-cache-size,omitempty"`
		// the size of each entry in the L2 cache in bytes. It must be
		// a power of two between 512 and the cluster size. The
		// default value is the cluster size (since 2.12)
		L2CacheEntrySize *int64 `json:"l2-cache-entry-size,omitempty"`
		// the maximum size of the refcount block cache in bytes
		// (since 2.2)
		RefcountCacheSize *int64 `json:"refcount-cache-size,omitempty"`
		// clean unused entries in the L2 and refcount caches. The
		// interval is in seconds. The default value is 600 on
		// supporting platforms, and 0 on other platforms. 0 disables
		// this feature. (since 2.5)
		CacheCleanInterval *int64 `json:"cache-clean-interval,omitempty"`
		// Image decryption options. Mandatory for encrypted images,
		// except when doing a metadata-only probe of the image.
		// (since 2.10)
		Encrypt *BlockdevQcow2Encryption `json:"encrypt,omitempty"`
		// reference to or definition of the external data file. This
		// may only be specified for images that require an external
		// data file. If it is not specified for such an image, the
		// data file name is loaded from the image file. (since 4.0)
		DataFile *BlockdevRef `json:"data-file,omitempty"`
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.File = tmp.File
	s.LazyRefcounts = tmp.LazyRefcounts
	s.PassDiscardRequest = tmp.PassDiscardRequest
	s.PassDiscardSnapshot = tmp.PassDiscardSnapshot
	s.PassDiscardOther = tmp.PassDiscardOther
	s.DiscardNoUnref = tmp.DiscardNoUnref
	s.OverlapCheck = tmp.OverlapCheck
	s.CacheSize = tmp.CacheSize
	s.L2CacheSize = tmp.L2CacheSize
	s.L2CacheEntrySize = tmp.L2CacheEntrySize
	s.RefcountCacheSize = tmp.RefcountCacheSize
	s.CacheCleanInterval = tmp.CacheCleanInterval
	s.Encrypt = tmp.Encrypt
	s.DataFile = tmp.DataFile

	if _, absent := (&tmp.Backing).ToAnyOrAbsent(); !absent {
		s.Backing = &tmp.Backing
	}

	return nil
}

// Driver specific block device options for Quorum
//
// Since: 2.9
type BlockdevOptionsQuorum struct {
	// true if the driver must print content mismatch set to false by
	// default
	Blkverify *bool `json:"blkverify,omitempty"`
	// the children block devices to use
	Children []BlockdevRef `json:"children"`
	// the vote limit under which a read will fail
	VoteThreshold int64 `json:"vote-threshold"`
	// rewrite corrupted data when quorum is reached (Since 2.1)
	RewriteCorrupted *bool `json:"rewrite-corrupted,omitempty"`
	// choose read pattern and set to quorum by default (Since 2.2)
	ReadPattern *QuorumReadPattern `json:"read-pattern,omitempty"`
}

// Driver specific block device options for the raw driver.
//
// Since: 2.9
type BlockdevOptionsRaw struct {
	// reference to or definition of the data source block device
	File BlockdevRef `json:"file"`
	// position where the block device starts
	Offset *int64 `json:"offset,omitempty"`
	// the assumed size of the device
	Size *int64 `json:"size,omitempty"`
}

// Since: 2.9
type BlockdevOptionsRbd struct {
	// Ceph pool name.
	Pool string `json:"pool"`
	// Rados namespace name in the Ceph pool. (Since 5.0)
	Namespace *string `json:"namespace,omitempty"`
	// Image name in the Ceph pool.
	Image string `json:"image"`
	// path to Ceph configuration file. Values in the configuration
	// file will be overridden by options specified via QAPI.
	Conf *string `json:"conf,omitempty"`
	// Ceph snapshot name.
	Snapshot *string `json:"snapshot,omitempty"`
	// Image encryption options. (Since 6.1)
	Encrypt *RbdEncryptionOptions `json:"encrypt,omitempty"`
	// Ceph id name.
	User *string `json:"user,omitempty"`
	// Acceptable authentication modes. This maps to Ceph
	// configuration option "auth_client_required". (Since 3.0)
	AuthClientRequired []RbdAuthMode `json:"auth-client-required,omitempty"`
	// ID of a QCryptoSecret object providing a key for cephx
	// authentication. This maps to Ceph configuration option "key".
	// (Since 3.0)
	KeySecret *string `json:"key-secret,omitempty"`
	// Monitor host address and port. This maps to the "mon_host" Ceph
	// option.
	Server []InetSocketAddressBase `json:"server,omitempty"`
}

// Driver specific block device options for replication
//
// Since: 2.9
type BlockdevOptionsReplication struct {
	// reference to or definition of the data source block device
	File BlockdevRef `json:"file"`
	// the replication mode
	Mode ReplicationMode `json:"mode"`
	// In secondary mode, node name or device ID of the root node who
	// owns the replication node chain. Must not be given in primary
	// mode.
	TopId *string `json:"top-id,omitempty"`
}

// Since: 2.9
type BlockdevOptionsSsh struct {
	// host address
	Server InetSocketAddress `json:"server"`
	// path to the image on the host
	Path string `json:"path"`
	// user as which to connect, defaults to current local user name
	User *string `json:"user,omitempty"`
	// Defines how and what to check the host key against (default:
	// known_hosts)
	HostKeyCheck *SshHostKeyCheck `json:"host-key-check,omitempty"`
}

// Driver specific block device options for the throttle driver
//
// Since: 2.11
type BlockdevOptionsThrottle struct {
	// the name of the throttle-group object to use. It must already
	// exist.
	ThrottleGroup string `json:"throttle-group"`
	// reference to or definition of the data source block device
	File BlockdevRef `json:"file"`
}

// Driver specific block device options for the vvfat protocol.
//
// Since: 2.9
type BlockdevOptionsVVFAT struct {
	// directory to be exported as FAT image
	Dir string `json:"dir"`
	// FAT type: 12, 16 or 32
	FatType *int64 `json:"fat-type,omitempty"`
	// whether to export a floppy image (true) or partitioned hard
	// disk (false; default)
	Floppy *bool `json:"floppy,omitempty"`
	// set the volume label, limited to 11 bytes. FAT16 and FAT32
	// traditionally have some restrictions on labels, which are
	// ignored by most operating systems. Defaults to "QEMU VVFAT".
	// (since 2.4)
	Label *string `json:"label,omitempty"`
	// whether to allow write operations (default: false)
	Rw *bool `json:"rw,omitempty"`
}

// Driver specific block device options for the virtio-blk-vfio-pci
// backend.
//
// Since: 7.2
type BlockdevOptionsVirtioBlkVfioPci struct {
	// path to the PCI device's sysfs directory (e.g.
	// /sys/bus/pci/devices/0000:00:01.0).
	Path string `json:"path"`
}

// Driver specific block device options for the virtio-blk-vhost-user
// backend.
//
// Since: 7.2
type BlockdevOptionsVirtioBlkVhostUser struct {
	// path to the vhost-user UNIX domain socket.
	Path string `json:"path"`
}

// Driver specific block device options for the virtio-blk-vhost-vdpa
// backend.
//
// Since: 7.2
type BlockdevOptionsVirtioBlkVhostVdpa struct {
	// path to the vhost-vdpa character device.
	Path string `json:"path"`
}

// Since: 2.5
type BlockdevSnapshot struct {
	// device or node name that will have a snapshot taken.
	Node string `json:"node"`
	// reference to the existing block device that will become the
	// overlay of @node, as part of taking the snapshot. It must not
	// have a current backing file (this can be achieved by passing
	// "backing": null to blockdev-add).
	Overlay string `json:"overlay"`
}

// Since: 1.7
type BlockdevSnapshotInternal struct {
	// the device name or node-name of a root node to generate the
	// snapshot from
	Device string `json:"device"`
	// the name of the internal snapshot to be created
	Name string `json:"name"`
}

// Since: 1.7
type BlockdevSnapshotInternalWrapper struct {
	Data BlockdevSnapshotInternal `json:"data"`
}

// Either @device or @node-name must be set but not both.
type BlockdevSnapshotSync struct {
	// the name of the device to take a snapshot of.
	Device *string `json:"device,omitempty"`
	// graph node name to generate the snapshot from (Since 2.0)
	NodeName *string `json:"node-name,omitempty"`
	// the target of the new overlay image. If the file exists, or if
	// it is a device, the overlay will be created in the existing
	// file/device. Otherwise, a new file will be created.
	SnapshotFile string `json:"snapshot-file"`
	// the graph node name of the new image (Since 2.0)
	SnapshotNodeName *string `json:"snapshot-node-name,omitempty"`
	// the format of the overlay image, default is 'qcow2'.
	Format *string `json:"format,omitempty"`
	// whether and how QEMU should create a new image, default is
	// 'absolute-paths'.
	Mode *NewImageMode `json:"mode,omitempty"`
}

// Since: 1.1
type BlockdevSnapshotSyncWrapper struct {
	Data BlockdevSnapshotSync `json:"data"`
}

// Since: 2.5
type BlockdevSnapshotWrapper struct {
	Data BlockdevSnapshot `json:"data"`
}

// Schema for virtual machine boot configuration.
//
// Since: 7.1
type BootConfiguration struct {
	// Boot order (a=floppy, c=hard disk, d=CD-ROM, n=network)
	Order *string `json:"order,omitempty"`
	// Boot order to apply on first boot
	Once *string `json:"once,omitempty"`
	// Whether to show a boot menu
	Menu *bool `json:"menu,omitempty"`
	// The name of the file to be passed to the firmware as logo
	// picture, if @menu is true.
	Splash *string `json:"splash,omitempty"`
	// How long to show the logo picture, in milliseconds
	SplashTime *int64 `json:"splash-time,omitempty"`
	// Timeout before guest reboots after boot fails
	RebootTimeout *int64 `json:"reboot-timeout,omitempty"`
	// Whether to attempt booting from devices not included in the
	// boot order
	Strict *bool `json:"strict,omitempty"`
}

// The result format for 'query-colo-status'.
//
// Since: 3.1
type COLOStatus struct {
	// COLO running mode. If COLO is running, this field will return
	// 'primary' or 'secondary'.
	Mode COLOMode `json:"mode"`
	// COLO last running mode. If COLO is running, this field will
	// return same like mode field, after failover we can use this
	// field to get last colo mode. (since 4.0)
	LastMode COLOMode `json:"last-mode"`
	// describes the reason for the COLO exit.
	Reason COLOExitReason `json:"reason"`
}

// List of CXL Fixed Memory Windows.
//
// Since: 7.1
type CXLFMWProperties struct {
	// List of CXLFixedMemoryWindowOptions
	CxlFmw []CXLFixedMemoryWindowOptions `json:"cxl-fmw"`
}

// Create a CXL Fixed Memory Window
//
// Since: 7.1
type CXLFixedMemoryWindowOptions struct {
	// Size of the Fixed Memory Window in bytes. Must be a multiple of
	// 256MiB.
	Size uint64 `json:"size"`
	// Number of contiguous bytes for which accesses will go to a
	// given interleave target. Accepted values [256, 512, 1k, 2k, 4k,
	// 8k, 16k]
	InterleaveGranularity *uint64 `json:"interleave-granularity,omitempty"`
	// Target root bridge IDs from -device ...,id=<ID> for each root
	// bridge.
	Targets []string `json:"targets"`
}

// Record of a single error including header log.
//
// Since: 8.0
type CXLUncorErrorRecord struct {
	// Type of error
	Type CxlUncorErrorType `json:"type"`
	// 16 DWORD of header.
	Header []uint32 `json:"header"`
}

// Properties for can-host-socketcan objects.
//
// Since: 2.12
type CanHostSocketcanProperties struct {
	// interface name of the host system CAN bus to connect to
	If string `json:"if"`
	// object ID of the can-bus object to connect to the host
	// interface
	Canbus string `json:"canbus"`
}

// Information about a character device backend
//
// Since: 2.0
type ChardevBackendInfo struct {
	// The backend name
	Name string `json:"name"`
}

// Configuration shared across all chardev backends
//
// Since: 2.6
type ChardevCommon struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
}

// Since: 2.6
type ChardevCommonWrapper struct {
	// Configuration shared across all chardev backends
	Data ChardevCommon `json:"data"`
}

// Configuration info for DBus chardevs.
//
// Since: 7.0
type ChardevDBus struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
	// name of the channel (following docs/spice-port-fqdn.txt)
	Name string `json:"name"`
}

// Since: 7.0
type ChardevDBusWrapper struct {
	// Configuration info for DBus chardevs
	Data ChardevDBus `json:"data"`
}

// Configuration info for file chardevs.
//
// Since: 1.4
type ChardevFile struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
	// The name of the input file
	In *string `json:"in,omitempty"`
	// The name of the output file
	Out string `json:"out"`
	// Open the file in append mode (default false to truncate) (Since
	// 2.6)
	Append *bool `json:"append,omitempty"`
}

// Since: 1.4
type ChardevFileWrapper struct {
	// Configuration info for file chardevs
	Data ChardevFile `json:"data"`
}

// Configuration info for device and pipe chardevs.
//
// Since: 1.4
type ChardevHostdev struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
	// The name of the special file for the device, i.e. /dev/ttyS0 on
	// Unix or COM1: on Windows
	Device string `json:"device"`
}

// Since: 1.4
type ChardevHostdevWrapper struct {
	// Configuration info for device and pipe chardevs
	Data ChardevHostdev `json:"data"`
}

// Configuration info for hub chardevs.
//
// Since: 10.0
type ChardevHub struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
	// List of chardev IDs, which should be added to this hub
	Chardevs []string `json:"chardevs"`
}

// Since: 10.0
type ChardevHubWrapper struct {
	// Configuration info for hub chardevs
	Data ChardevHub `json:"data"`
}

// Information about a character device.
//
// .. note:: @filename is encoded using the QEMU command line
// character   device encoding. See the QEMU man page for details.
//
// Since: 0.14
type ChardevInfo struct {
	// the label of the character device
	Label string `json:"label"`
	// the filename of the character device
	Filename string `json:"filename"`
	// shows whether the frontend device attached to this backend
	// (e.g. with the chardev=... option) is in open or closed state
	// (since 2.1)
	FrontendOpen bool `json:"frontend-open"`
}

// Configuration info for mux chardevs.
//
// Since: 1.5
type ChardevMux struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
	// name of the base chardev.
	Chardev string `json:"chardev"`
}

// Since: 1.5
type ChardevMuxWrapper struct {
	// Configuration info for mux chardevs
	Data ChardevMux `json:"data"`
}

// Configuration info for pty implementation.
//
// Since: 9.2
type ChardevPty struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
	// optional path to create a symbolic link that points to the
	// allocated PTY
	Path *string `json:"path,omitempty"`
}

// Since: 9.2
type ChardevPtyWrapper struct {
	// Configuration info for pty chardevs
	Data ChardevPty `json:"data"`
}

// Configuration info for qemu vdagent implementation.
//
// Since: 6.1
type ChardevQemuVDAgent struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
	// enable/disable mouse, default is enabled.
	Mouse *bool `json:"mouse,omitempty"`
	// enable/disable clipboard, default is disabled.
	Clipboard *bool `json:"clipboard,omitempty"`
}

// Since: 6.1
type ChardevQemuVDAgentWrapper struct {
	// Configuration info for qemu vdagent implementation
	Data ChardevQemuVDAgent `json:"data"`
}

// Return info about the chardev backend just created.
//
// Since: 1.4
type ChardevReturn struct {
	// name of the slave pseudoterminal device, present if and only if
	// a chardev of type 'pty' was created
	Pty *string `json:"pty,omitempty"`
}

// Configuration info for ring buffer chardevs.
//
// Since: 1.5
type ChardevRingbuf struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
	// ring buffer size, must be power of two, default is 65536
	Size *int64 `json:"size,omitempty"`
}

// Since: 1.5
type ChardevRingbufWrapper struct {
	// Configuration info for ring buffer chardevs
	Data ChardevRingbuf `json:"data"`
}

// Configuration info for (stream) socket chardevs.
//
// Since: 1.4
type ChardevSocket struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
	// socket address to listen on (server=true) or connect to
	// (server=false)
	Addr SocketAddressLegacy `json:"addr"`
	// the ID of the TLS credentials object (since 2.6)
	TlsCreds *string `json:"tls-creds,omitempty"`
	// the ID of the QAuthZ authorization object against which the
	// client's x509 distinguished name will be validated. This object
	// is only resolved at time of use, so can be deleted and
	// recreated on the fly while the chardev server is active. If
	// missing, it will default to denying access (since 4.0)
	TlsAuthz *string `json:"tls-authz,omitempty"`
	// create server socket (default: true)
	Server *bool `json:"server,omitempty"`
	// wait for incoming connection on server sockets (default:
	// false). Silently ignored with server: false. This use is
	// deprecated.
	Wait *bool `json:"wait,omitempty"`
	// set TCP_NODELAY socket option (default: false)
	Nodelay *bool `json:"nodelay,omitempty"`
	// enable telnet protocol on server sockets (default: false)
	Telnet *bool `json:"telnet,omitempty"`
	// enable tn3270 protocol on server sockets (default: false)
	// (Since: 2.10)
	Tn3270 *bool `json:"tn3270,omitempty"`
	// enable websocket protocol on server sockets (default: false)
	// (Since: 3.1)
	Websocket *bool `json:"websocket,omitempty"`
	// For a client socket, if a socket is disconnected, then attempt
	// a reconnect after the given number of seconds. Setting this to
	// zero disables this function. The use of this member is
	// deprecated, use @reconnect-ms instead. (default: 0) (Since:
	// 2.2)
	Reconnect *int64 `json:"reconnect,omitempty"`
	// For a client socket, if a socket is disconnected, then attempt
	// a reconnect after the given number of milliseconds. Setting
	// this to zero disables this function. This member is mutually
	// exclusive with @reconnect. (default: 0) (Since: 9.2)
	ReconnectMs *int64 `json:"reconnect-ms,omitempty"`
}

// Since: 1.4
type ChardevSocketWrapper struct {
	// Configuration info for (stream) socket chardevs
	Data ChardevSocket `json:"data"`
}

// Configuration info for spice vm channel chardevs.
//
// Since: 1.5
type ChardevSpiceChannel struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
	// kind of channel (for example vdagent).
	Type string `json:"type"`
}

// Since: 1.5
type ChardevSpiceChannelWrapper struct {
	// Configuration info for spice vm channel chardevs
	Data ChardevSpiceChannel `json:"data"`
}

// Configuration info for spice port chardevs.
//
// Since: 1.5
type ChardevSpicePort struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
	// name of the channel (see docs/spice-port-fqdn.txt)
	Fqdn string `json:"fqdn"`
}

// Since: 1.5
type ChardevSpicePortWrapper struct {
	// Configuration info for spice port chardevs
	Data ChardevSpicePort `json:"data"`
}

// Configuration info for stdio chardevs.
//
// Since: 1.5
type ChardevStdio struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
	// Allow signals (such as SIGINT triggered by ^C) be delivered to
	// qemu. Default: true.
	Signal *bool `json:"signal,omitempty"`
}

// Since: 1.5
type ChardevStdioWrapper struct {
	// Configuration info for stdio chardevs
	Data ChardevStdio `json:"data"`
}

// Configuration info for datagram socket chardevs.
//
// Since: 1.5
type ChardevUdp struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
	// remote address
	Remote SocketAddressLegacy `json:"remote"`
	// local address
	Local *SocketAddressLegacy `json:"local,omitempty"`
}

// Since: 1.5
type ChardevUdpWrapper struct {
	// Configuration info for datagram socket chardevs
	Data ChardevUdp `json:"data"`
}

// Configuration info for virtual console chardevs.
//
// .. note:: The options are only effective when the VNC or SDL
// graphical display backend is active. They are ignored with the
// GTK, Spice, VNC and D-Bus display backends.
//
// Since: 1.5
type ChardevVC struct {
	// The name of a logfile to save output
	Logfile *string `json:"logfile,omitempty"`
	// true to append instead of truncate (default to false to
	// truncate)
	Logappend *bool `json:"logappend,omitempty"`
	// console width, in pixels
	Width *int64 `json:"width,omitempty"`
	// console height, in pixels
	Height *int64 `json:"height,omitempty"`
	// console width, in chars
	Cols *int64 `json:"cols,omitempty"`
	// console height, in chars
	Rows *int64 `json:"rows,omitempty"`
}

// Since: 1.5
type ChardevVCWrapper struct {
	// Configuration info for virtual console chardevs
	Data ChardevVC `json:"data"`
}

// Properties for colo-compare objects.
//
// Since: 2.8
type ColoCompareProperties struct {
	// name of the character device backend to use for the primary
	// input (incoming packets are redirected to @outdev)
	PrimaryIn string `json:"primary_in"`
	// name of the character device backend to use for secondary input
	// (incoming packets are only compared to the input on @primary_in
	// and then dropped)
	SecondaryIn string `json:"secondary_in"`
	// name of the character device backend to use for output
	Outdev string `json:"outdev"`
	// name of the iothread to run in
	Iothread string `json:"iothread"`
	// name of the character device backend to be used to communicate
	// with the remote colo-frame (only for Xen COLO)
	NotifyDev *string `json:"notify_dev,omitempty"`
	// the maximum time to hold a packet from @primary_in for
	// comparison with an incoming packet on @secondary_in in
	// milliseconds (default: 3000)
	CompareTimeout *uint64 `json:"compare_timeout,omitempty"`
	// the interval at which colo-compare checks whether packets from
	// @primary have timed out, in milliseconds (default: 3000)
	ExpiredScanCycle *uint32 `json:"expired_scan_cycle,omitempty"`
	// the maximum number of packets to keep in the queue for
	// comparing with incoming packets from @secondary_in. If the
	// queue is full and additional packets are received, the
	// additional packets are dropped. (default: 1024)
	MaxQueueSize *uint32 `json:"max_queue_size,omitempty"`
	// if true, vnet header support is enabled (default: false)
	VnetHdrSupport *bool `json:"vnet_hdr_support,omitempty"`
}

// Information about a QMP command
//
// Since: 0.14
type CommandInfo struct {
	// The command name
	Name string `json:"name"`
}

// Details about a command line option, including its list of
// parameter details
//
// Since: 1.5
type CommandLineOptionInfo struct {
	// option name
	Option string `json:"option"`
	// an array of @CommandLineParameterInfo
	Parameters []CommandLineParameterInfo `json:"parameters"`
}

// Details about a single parameter of a command line option.
//
// Since: 1.5
type CommandLineParameterInfo struct {
	// parameter name
	Name string `json:"name"`
	// parameter @CommandLineParameterType
	Type CommandLineParameterType `json:"type"`
	// human readable text string, not suitable for parsing.
	Help *string `json:"help,omitempty"`
	// default value string (since 2.1)
	Default *string `json:"default,omitempty"`
}

// Policy for handling deprecated management interfaces.  This is
// intended for testing users of the management interfaces.
// Limitation: covers only syntactic aspects of QMP, i.e. stuff tagged
// with feature 'deprecated' or 'unstable'.  We may want to extend it
// to cover semantic aspects and CLI.  Limitation: deprecated-output
// policy @hide is not implemented for enumeration values.  They
// behave the same as with policy @accept.
//
// Since: 6.0
type CompatPolicy struct {
	// how to handle deprecated input (default 'accept')
	DeprecatedInput *CompatPolicyInput `json:"deprecated-input,omitempty"`
	// how to handle deprecated output (default 'accept')
	DeprecatedOutput *CompatPolicyOutput `json:"deprecated-output,omitempty"`
	// how to handle unstable input (default 'accept') (since 6.2)
	UnstableInput *CompatPolicyInput `json:"unstable-input,omitempty"`
	// how to handle unstable output (default 'accept') (since 6.2)
	UnstableOutput *CompatPolicyOutput `json:"unstable-output,omitempty"`
}

// Property default values specific to a machine type, for use by
// scripts/compare-machine-types.
//
// Since: 9.1
type CompatProperty struct {
	// name of the QOM type to which the default applies
	QomType string `json:"qom-type"`
	// name of its property to which the default applies
	Property string `json:"property"`
	// the default value (machine-specific default can overwrite the
	// "default" default, to avoid this use -machine none)
	Value string `json:"value"`
}

// Detailed migration compression statistics
//
// Since: 3.1
type CompressionStats struct {
	// amount of pages compressed and transferred to the target VM
	Pages int64 `json:"pages"`
	// count of times that no free thread was available to compress
	// data
	Busy int64 `json:"busy"`
	// rate of thread busy
	BusyRate float64 `json:"busy-rate"`
	// amount of bytes after compression
	CompressedSize int64 `json:"compressed-size"`
	// rate of compressed size
	CompressionRate float64 `json:"compression-rate"`
}

// Virtual CPU definition.
//
// @unavailable-features is a list of QOM property names that
// represent CPU model attributes that prevent the CPU from running.
// If the QOM property is read-only, that means there's no known way
// to make the CPU model run in the current host. Implementations that
// choose not to provide specific information return the property name
// "type". If the property is read-write, it means that it MAY be
// possible to run the CPU model in the current host if that property
// is changed. Management software can use it as hints to suggest or
// choose an alternative for the user, or just to generate meaningful
// error messages explaining why the CPU model can't be used. If
// @unavailable-features is an empty list, the CPU model is runnable
// using the current host and machine-type. If @unavailable-features
// is not present, runnability information for the CPU is not
// available.
//
// Since: 1.2
type CpuDefinitionInfo struct {
	// the name of the CPU definition
	Name string `json:"name"`
	// whether a CPU definition can be safely used for migration in
	// combination with a QEMU compatibility machine when migrating
	// between different QEMU versions and between hosts with
	// different sets of (hardware or software) capabilities. If not
	// provided, information is not available and callers should not
	// assume the CPU definition to be migration-safe. (since 2.8)
	MigrationSafe *bool `json:"migration-safe,omitempty"`
	// whether a CPU definition is static and will not change
	// depending on QEMU version, machine type, machine options and
	// accelerator options. A static model is always migration-safe.
	// (since 2.8)
	Static bool `json:"static"`
	// List of properties that prevent the CPU model from running in
	// the current host. (since 2.8)
	UnavailableFeatures []string `json:"unavailable-features,omitempty"`
	// Type name that can be used as argument to @device-list-
	// properties, to introspect properties configurable using -cpu or
	// -global. (since 2.9)
	Typename string `json:"typename"`
	// Name of CPU model this model is an alias for. The target of the
	// CPU model alias may change depending on the machine type.
	// Management software is supposed to translate CPU model aliases
	// in the VM configuration, because aliases may stop being
	// migration-safe in the future (since 4.1)
	AliasOf *string `json:"alias-of,omitempty"`
	// If true, this CPU model is deprecated and may be removed in in
	// some future version of QEMU according to the QEMU deprecation
	// policy. (since 5.2)
	Deprecated bool `json:"deprecated"`
}

// Additional information about a virtual S390 CPU
//
// Since: 2.12
type CpuInfoS390 struct {
	// the virtual CPU's state
	CpuState S390CpuState `json:"cpu-state"`
	// the virtual CPU's dedication (since 8.2)
	Dedicated *bool `json:"dedicated,omitempty"`
	// the virtual CPU's entitlement (since 8.2)
	Entitlement *S390CpuEntitlement `json:"entitlement,omitempty"`
}

// Properties identifying a CPU.  Which members are optional and which
// mandatory depends on the architecture and board.  For s390x see
// :ref:`cpu-topology-s390x`.  The ids other than the node-id specify
// the position of the CPU within the CPU topology (as defined by the
// machine property "smp", thus see also type @SMPConfiguration)
//
// Since: 2.7
type CpuInstanceProperties struct {
	// NUMA node ID the CPU belongs to
	NodeId *int64 `json:"node-id,omitempty"`
	// drawer number within CPU topology the CPU belongs to (since
	// 8.2)
	DrawerId *int64 `json:"drawer-id,omitempty"`
	// book number within parent container the CPU belongs to (since
	// 8.2)
	BookId *int64 `json:"book-id,omitempty"`
	// socket number within parent container the CPU belongs to
	SocketId *int64 `json:"socket-id,omitempty"`
	// die number within the parent container the CPU belongs to
	// (since 4.1)
	DieId *int64 `json:"die-id,omitempty"`
	// cluster number within the parent container the CPU belongs to
	// (since 7.1)
	ClusterId *int64 `json:"cluster-id,omitempty"`
	// module number within the parent container the CPU belongs to
	// (since 9.1)
	ModuleId *int64 `json:"module-id,omitempty"`
	// core number within the parent container the CPU belongs to
	CoreId *int64 `json:"core-id,omitempty"`
	// thread number within the core the CPU belongs to
	ThreadId *int64 `json:"thread-id,omitempty"`
}

// The result of a CPU model baseline.
//
// Since: 2.8
type CpuModelBaselineInfo struct {
	// the baselined CpuModelInfo.
	Model CpuModelInfo `json:"model"`
}

// The result of a CPU model comparison.
//
// @responsible-properties is a list of QOM property names that led to
// both CPUs not being detected as identical. For identical models,
// this list is empty. If a QOM property is read-only, that means
// there's no known way to make the CPU models identical. If the
// special property name "type" is included, the models are by
// definition not identical and cannot be made identical.
//
// Since: 2.8
type CpuModelCompareInfo struct {
	// The result of the compare operation.
	Result CpuModelCompareResult `json:"result"`
	// List of properties that led to the comparison result not being
	// identical.
	ResponsibleProperties []string `json:"responsible-properties"`
}

// The result of a cpu model expansion.
//
// Since: 2.8
type CpuModelExpansionInfo struct {
	// the expanded CpuModelInfo.
	Model CpuModelInfo `json:"model"`
	// a list of properties that are flagged as deprecated by the CPU
	// vendor. The list depends on the CpuModelExpansionType: "static"
	// properties are a subset of the enabled-properties for the
	// expanded model; "full" properties are a set of properties that
	// are deprecated across all models for the architecture. (since:
	// 9.1).
	DeprecatedProps []string `json:"deprecated-props"`
}

// Virtual CPU model.  A CPU model consists of the name of a CPU
// definition, to which delta changes are applied (e.g. features
// added/removed).  Most magic values that an architecture might
// require should be hidden behind the name.  However, if required,
// architectures can expose relevant properties.
//
// Since: 2.8
type CpuModelInfo struct {
	// the name of the CPU definition the model is based on
	Name string `json:"name"`
	// a dictionary of QOM properties to be applied
	Props *any `json:"props,omitempty"`
}

// The result of a CPU polarization query.
//
// Since: 8.2
type CpuPolarizationInfo struct {
	// the CPU polarization
	Polarization S390CpuPolarization `json:"polarization"`
}

// Properties for cryptodev-backend and cryptodev-backend-builtin
// objects.
//
// Since: 2.8
type CryptodevBackendProperties struct {
	// the number of queues for the cryptodev backend. Ignored for
	// cryptodev-backend and must be 1 for cryptodev-backend-builtin.
	// (default: 1)
	Queues *uint32 `json:"queues,omitempty"`
	// limit total bytes per second (Since 8.0)
	ThrottleBps *uint64 `json:"throttle-bps,omitempty"`
	// limit total operations per second (Since 8.0)
	ThrottleOps *uint64 `json:"throttle-ops,omitempty"`
}

// Properties for cryptodev-vhost-user objects.
//
// Since: 2.12
type CryptodevVhostUserProperties struct {
	// the number of queues for the cryptodev backend. Ignored for
	// cryptodev-backend and must be 1 for cryptodev-backend-builtin.
	// (default: 1)
	Queues *uint32 `json:"queues,omitempty"`
	// limit total bytes per second (Since 8.0)
	ThrottleBps *uint64 `json:"throttle-bps,omitempty"`
	// limit total operations per second (Since 8.0)
	ThrottleOps *uint64 `json:"throttle-ops,omitempty"`
	// the name of a Unix domain socket character device that connects
	// to the vhost-user server
	Chardev string `json:"chardev"`
}

// Information describing the running machine parameters.
//
// Since: 4.0
type CurrentMachineParams struct {
	// true if the machine supports wake up from suspend
	WakeupSuspendSupport bool `json:"wakeup-suspend-support"`
}

// A single dynamic capacity extent.  This is a contiguous allocation
// of memory by Device Physical Address within a single Dynamic
// Capacity Region on a CXL Type 3 Device.
//
// Since: 9.1
type CxlDynamicCapacityExtent struct {
	// The offset (in bytes) to the start of the region where the
	// extent belongs to.
	Offset uint64 `json:"offset"`
	// The length of the extent in bytes.
	Len uint64 `json:"len"`
}

// Properties for dbus-vmstate objects.
//
// Since: 5.0
type DBusVMStateProperties struct {
	// the name of the DBus bus to connect to
	Addr string `json:"addr"`
	// a comma separated list of DBus IDs of helpers whose data should
	// be included in the VM state on migration
	IdList *string `json:"id-list,omitempty"`
}

// Dirty page rate limit information of a virtual CPU.
//
// Since: 7.1
type DirtyLimitInfo struct {
	// index of a virtual CPU.
	CpuIndex int64 `json:"cpu-index"`
	// upper limit of dirty page rate (MB/s) for a virtual CPU, 0
	// means unlimited.
	LimitRate uint64 `json:"limit-rate"`
	// current dirty page rate (MB/s) for a virtual CPU.
	CurrentRate uint64 `json:"current-rate"`
}

// Information about measured dirty page rate.
//
// Since: 5.2
type DirtyRateInfo struct {
	// an estimate of the dirty page rate of the VM in units of MiB/s.
	// Value is present only when @status is 'measured'.
	DirtyRate *int64 `json:"dirty-rate,omitempty"`
	// current status of dirty page rate measurements
	Status DirtyRateStatus `json:"status"`
	// start time in units of second for calculation
	StartTime int64 `json:"start-time"`
	// time period for which dirty page rate was measured, expressed
	// and rounded down to @calc-time-unit.
	CalcTime int64 `json:"calc-time"`
	// time unit of @calc-time (Since 8.2)
	CalcTimeUnit TimeUnit `json:"calc-time-unit"`
	// number of sampled pages per GiB of guest memory. Valid only in
	// page-sampling mode (Since 6.1)
	SamplePages uint64 `json:"sample-pages"`
	// mode that was used to measure dirty page rate (Since 6.2)
	Mode DirtyRateMeasureMode `json:"mode"`
	// dirty rate for each vCPU if dirty-ring mode was specified
	// (Since 6.2)
	VcpuDirtyRate []DirtyRateVcpu `json:"vcpu-dirty-rate,omitempty"`
}

// Dirty rate of vcpu.
//
// Since: 6.2
type DirtyRateVcpu struct {
	// vcpu index.
	Id int64 `json:"id"`
	// dirty rate.
	DirtyRate int64 `json:"dirty-rate"`
}

// Cocoa display options.
//
// Since: 7.0
type DisplayCocoa struct {
	// Enable/disable forwarding of left command key to guest. Allows
	// command-tab window switching on the host without sending this
	// key to the guest when "off". Defaults to "on"
	LeftCommandKey *bool `json:"left-command-key,omitempty"`
	// Capture all key presses, including system combos. This requires
	// accessibility permissions, since it performs a global grab on
	// key events. (default: off) See https://support.apple.com/en-
	// in/guide/mac-help/mh32356/mac
	FullGrab *bool `json:"full-grab,omitempty"`
	// Swap the Option and Command keys so that their key codes match
	// their position on non-Mac keyboards and you can use Meta/Super
	// and Alt where you expect them. (default: off)
	SwapOptCmd *bool `json:"swap-opt-cmd,omitempty"`
	// Zoom guest display to fit into the host window. When turned off
	// the host window will be resized instead. Defaults to "off".
	// (Since 8.2)
	ZoomToFit *bool `json:"zoom-to-fit,omitempty"`
	// Apply interpolation to smooth output when zoom-to-fit is
	// enabled. Defaults to "off". (Since 9.0)
	ZoomInterpolation *bool `json:"zoom-interpolation,omitempty"`
}

// Curses display options.
//
// Since: 4.0
type DisplayCurses struct {
	// Font charset used by guest (default: CP437).
	Charset *string `json:"charset,omitempty"`
}

// DBus display options.
//
// Since: 7.0
type DisplayDBus struct {
	// Which DRM render node should be used. Default is the first
	// available node on the host.
	Rendernode *string `json:"rendernode,omitempty"`
	// The D-Bus bus address (default to the session bus).
	Addr *string `json:"addr,omitempty"`
	// Whether to use peer-to-peer connections (accepted through
	// @add_client).
	P2P *bool `json:"p2p,omitempty"`
	// Use the specified DBus audiodev to export audio.
	Audiodev *string `json:"audiodev,omitempty"`
}

// EGL headless display options.
//
// Since: 3.1
type DisplayEGLHeadless struct {
	// Which DRM render node should be used. Default is the first
	// available node on the host.
	Rendernode *string `json:"rendernode,omitempty"`
}

// GTK display options.
//
// Since: 2.12
type DisplayGTK struct {
	// Grab keyboard input on mouse hover.
	GrabOnHover *bool `json:"grab-on-hover,omitempty"`
	// Zoom guest display to fit into the host window. When turned off
	// the host window will be resized instead. In case the display
	// device can notify the guest on window resizes (virtio-gpu) this
	// will default to "on", assuming the guest will resize the
	// display to match the window size then. Otherwise it defaults to
	// "off". (Since 3.1)
	ZoomToFit *bool `json:"zoom-to-fit,omitempty"`
	// Display the tab bar for switching between the various graphical
	// interfaces (e.g. VGA and virtual console character devices) by
	// default. (Since 7.1)
	ShowTabs *bool `json:"show-tabs,omitempty"`
	// Display the main window menubar. Defaults to "on". (Since 8.0)
	ShowMenubar *bool `json:"show-menubar,omitempty"`
}

// Specify the VNC reload options.
//
// Since: 6.0
type DisplayReloadOptionsVNC struct {
	// reload tls certs or not.
	TlsCerts *bool `json:"tls-certs,omitempty"`
}

// SDL2 display options.
//
// Since: 7.1
type DisplaySDL struct {
	// Modifier keys that should be pressed together with the "G" key
	// to release the mouse grab.
	GrabMod *HotKeyMod `json:"grab-mod,omitempty"`
}

// Specify the VNC reload options.
//
// Since: 7.1
type DisplayUpdateOptionsVNC struct {
	// If specified, change set of addresses to listen for
	// connections. Addresses configured for websockets are not
	// touched.
	Addresses []SocketAddress `json:"addresses,omitempty"`
}

// Since: 1.6
type DriveBackup struct {
	// identifier for the newly-created block job. If omitted, the
	// device name will be used. (Since 2.7)
	JobId *string `json:"job-id,omitempty"`
	// the device name or node-name of a root node which should be
	// copied.
	Device string `json:"device"`
	// what parts of the disk image should be copied to the
	// destination (all the disk, only the sectors allocated in the
	// topmost image, from a dirty bitmap, or only new I/O).
	Sync MirrorSyncMode `json:"sync"`
	// the maximum speed, in bytes per second. The default is 0, for
	// unlimited.
	Speed *int64 `json:"speed,omitempty"`
	// The name of a dirty bitmap to use. Must be present if sync is
	// "bitmap" or "incremental". Can be present if sync is "full" or
	// "top". Must not be present otherwise. (Since 2.4 (drive-
	// backup), 3.1 (blockdev-backup))
	Bitmap *string `json:"bitmap,omitempty"`
	// Specifies the type of data the bitmap should contain after the
	// operation concludes. Must be present if a bitmap was provided,
	// Must NOT be present otherwise. (Since 4.2)
	BitmapMode *BitmapSyncMode `json:"bitmap-mode,omitempty"`
	// true to compress data, if the target format supports it.
	// (default: false) (since 2.8)
	Compress *bool `json:"compress,omitempty"`
	// the action to take on an error on the source, default 'report'.
	// 'stop' and 'enospc' can only be used if the block device
	// supports io-status (see BlockInfo).
	OnSourceError *BlockdevOnError `json:"on-source-error,omitempty"`
	// the action to take on an error on the target, default 'report'
	// (no limitations, since this applies to a different block device
	// than @device).
	OnTargetError *BlockdevOnError `json:"on-target-error,omitempty"`
	// When false, this job will wait in a PENDING state after it has
	// finished its work, waiting for @block-job-finalize before
	// making any block graph changes. When true, this job will
	// automatically perform its abort or commit actions. Defaults to
	// true. (Since 2.12)
	AutoFinalize *bool `json:"auto-finalize,omitempty"`
	// When false, this job will wait in a CONCLUDED state after it
	// has completely ceased all work, and awaits @block-job-dismiss.
	// When true, this job will automatically disappear from the query
	// list without user intervention. Defaults to true. (Since 2.12)
	AutoDismiss *bool `json:"auto-dismiss,omitempty"`
	// the node name that should be assigned to the filter driver that
	// the backup job inserts into the graph above node specified by
	// @drive. If this option is not given, a node name is
	// autogenerated. (Since: 4.2)
	FilterNodeName *string `json:"filter-node-name,omitempty"`
	// Discard blocks on source which have already been copied to the
	// target. (Since 9.1)
	DiscardSource *bool `json:"discard-source,omitempty"`
	// Performance options. (Since 6.0)
	XPerf *BackupPerf `json:"x-perf,omitempty"`
	// the target of the new image. If the file exists, or if it is a
	// device, the existing file/device will be used as the new
	// destination. If it does not exist, a new file will be created.
	Target string `json:"target"`
	// the format of the new destination, default is to probe if @mode
	// is 'existing', else the format of the source
	Format *string `json:"format,omitempty"`
	// whether and how QEMU should create a new image, default is
	// 'absolute-paths'.
	Mode *NewImageMode `json:"mode,omitempty"`
}

// Since: 1.6
type DriveBackupWrapper struct {
	Data DriveBackup `json:"data"`
}

// A set of parameters describing drive mirror setup.
//
// Since: 1.3
type DriveMirror struct {
	// identifier for the newly-created block job. If omitted, the
	// device name will be used. (Since 2.7)
	JobId *string `json:"job-id,omitempty"`
	// the device name or node-name of a root node whose writes should
	// be mirrored.
	Device string `json:"device"`
	// the target of the new image. If the file exists, or if it is a
	// device, the existing file/device will be used as the new
	// destination. If it does not exist, a new file will be created.
	Target string `json:"target"`
	// the format of the new destination, default is to probe if @mode
	// is 'existing', else the format of the source
	Format *string `json:"format,omitempty"`
	// the new block driver state node name in the graph (Since 2.1)
	NodeName *string `json:"node-name,omitempty"`
	// with sync=full graph node name to be replaced by the new image
	// when a whole image copy is done. This can be used to repair
	// broken Quorum files. By default, @device is replaced, although
	// implicitly created filters on it are kept. (Since 2.1)
	Replaces *string `json:"replaces,omitempty"`
	// what parts of the disk image should be copied to the
	// destination (all the disk, only the sectors allocated in the
	// topmost image, or only new I/O).
	Sync MirrorSyncMode `json:"sync"`
	// whether and how QEMU should create a new image, default is
	// 'absolute-paths'.
	Mode *NewImageMode `json:"mode,omitempty"`
	// the maximum speed, in bytes per second
	Speed *int64 `json:"speed,omitempty"`
	// granularity of the dirty bitmap, default is 64K if the image
	// format doesn't have clusters, 4K if the clusters are smaller
	// than that, else the cluster size. Must be a power of 2 between
	// 512 and 64M (since 1.4).
	Granularity *uint32 `json:"granularity,omitempty"`
	// maximum amount of data in flight from source to target (since
	// 1.4).
	BufSize *int64 `json:"buf-size,omitempty"`
	// the action to take on an error on the source, default 'report'.
	// 'stop' and 'enospc' can only be used if the block device
	// supports io-status (see BlockInfo).
	OnSourceError *BlockdevOnError `json:"on-source-error,omitempty"`
	// the action to take on an error on the target, default 'report'
	// (no limitations, since this applies to a different block device
	// than @device).
	OnTargetError *BlockdevOnError `json:"on-target-error,omitempty"`
	// Whether to try to unmap target sectors where source has only
	// zero. If true, and target unallocated sectors will read as
	// zero, target image sectors will be unmapped; otherwise, zeroes
	// will be written. Both will result in identical contents.
	// Default is true. (Since 2.4)
	Unmap *bool `json:"unmap,omitempty"`
	// when to copy data to the destination; defaults to 'background'
	// (Since: 3.0)
	CopyMode *MirrorCopyMode `json:"copy-mode,omitempty"`
	// When false, this job will wait in a PENDING state after it has
	// finished its work, waiting for @block-job-finalize before
	// making any block graph changes. When true, this job will
	// automatically perform its abort or commit actions. Defaults to
	// true. (Since 3.1)
	AutoFinalize *bool `json:"auto-finalize,omitempty"`
	// When false, this job will wait in a CONCLUDED state after it
	// has completely ceased all work, and awaits @block-job-dismiss.
	// When true, this job will automatically disappear from the query
	// list without user intervention. Defaults to true. (Since 3.1)
	AutoDismiss *bool `json:"auto-dismiss,omitempty"`
}

// Not used by QMP; hack to let us use BlockGraphInfoList internally
//
// Since: 8.0
type DummyBlockCoreForceArrays struct {
	UnusedBlockGraphInfo []BlockGraphInfo `json:"unused-block-graph-info"`
}

// Not used by QMP; hack to let us use X86CPUFeatureWordInfoList
// internally
//
// Since: 2.5
type DummyForceArrays struct {
	Unused []X86CPUFeatureWordInfo `json:"unused"`
}

// Not used by QMP; hack to let us use IOThreadVirtQueueMappingList
// internally
//
// Since: 9.0
type DummyVirtioForceArrays struct {
	UnusedIothreadVqMapping []IOThreadVirtQueueMapping `json:"unused-iothread-vq-mapping"`
}

// Since: 2.0
type DumpGuestMemoryCapability struct {
	// the available formats for dump-guest-memory
	Formats []DumpGuestMemoryFormat `json:"formats"`
}

// The result format for 'query-dump'.
//
// Since: 2.6
type DumpQueryResult struct {
	// enum of @DumpStatus, which shows current dump status
	Status DumpStatus `json:"status"`
	// bytes written in latest dump (uncompressed)
	Completed int64 `json:"completed"`
	// total bytes to be written in latest dump (uncompressed)
	Total int64 `json:"total"`
}

// An eBPF ELF object.
//
// Since: 9.0
type EbpfObject struct {
	// the eBPF object encoded in base64
	Object string `json:"object"`
}

// Common properties for event loops
//
// Since: 7.1
type EventLoopBaseProperties struct {
	// maximum number of requests in a batch for the AIO engine, 0
	// means that the engine will use its default. (default: 0)
	AioMaxBatch *int64 `json:"aio-max-batch,omitempty"`
	// minimum number of threads reserved in the thread pool
	// (default:0)
	ThreadPoolMin *int64 `json:"thread-pool-min,omitempty"`
	// maximum number of threads the thread pool can contain
	// (default:64)
	ThreadPoolMax *int64 `json:"thread-pool-max,omitempty"`
}

// Information about a Xen event channel port
//
// Since: 8.0
type EvtchnInfo struct {
	// the port number
	Port uint16 `json:"port"`
	// target vCPU for this port
	Vcpu uint32 `json:"vcpu"`
	// the port type
	Type EvtchnPortType `json:"type"`
	// remote domain for interdomain ports
	RemoteDomain string `json:"remote-domain"`
	// remote port ID, or virq/pirq number
	Target uint16 `json:"target"`
	// port is currently active pending delivery
	Pending bool `json:"pending"`
	// port is masked
	Masked bool `json:"masked"`
}

// Options for expire_password specific to the VNC protocol.
//
// Since: 7.0
type ExpirePasswordOptionsVnc struct {
	// The id of the display where the expiration should be changed.
	// Defaults to the first.
	Display *string `json:"display,omitempty"`
}

// A file descriptor name or number.
//
// Since: 1.2
type FdSocketAddress struct {
	// decimal is for file descriptor number, otherwise it's a file
	// descriptor name. Named file descriptors are permitted in
	// monitor commands, in combination with the 'getfd' command.
	// Decimal file descriptors are permitted at startup or other
	// contexts where no monitor context is active.
	Str string `json:"str"`
}

// Since: 1.3
type FdSocketAddressWrapper struct {
	// file descriptor name or number
	Data FdSocketAddress `json:"data"`
}

// Information about a file descriptor that belongs to an fd set.
//
// Since: 1.2
type FdsetFdInfo struct {
	// The file descriptor value.
	Fd int64 `json:"fd"`
	// A free-form string that can be used to describe the fd.
	Opaque *string `json:"opaque,omitempty"`
}

// Information about an fd set.
//
// Since: 1.2
type FdsetInfo struct {
	// The ID of the fd set.
	FdsetId int64 `json:"fdset-id"`
	// A list of file descriptors that belong to this fd set.
	Fds []FdsetFdInfo `json:"fds"`
}

// Since: 8.2
type FileMigrationArgs struct {
	// The file to receive the migration stream
	Filename string `json:"filename"`
	// The file offset where the migration stream will start
	Offset uint64 `json:"offset"`
}

// Properties for filter-buffer objects.
//
// Since: 2.5
type FilterBufferProperties struct {
	// id of the network device backend to filter
	Netdev string `json:"netdev"`
	// indicates which queue(s) to filter (default: all)
	Queue *NetFilterDirection `json:"queue,omitempty"`
	// indicates whether the filter is enabled ("on") or disabled
	// ("off") (default: "on")
	Status *string `json:"status,omitempty"`
	// specifies where the filter should be inserted in the filter
	// list. "head" means the filter is inserted at the head of the
	// filter list, before any existing filters. "tail" means the
	// filter is inserted at the tail of the filter list, behind any
	// existing filters (default). "id=<id>" means the filter is
	// inserted before or behind the filter specified by <id>,
	// depending on the @insert property. (default: "tail")
	Position *string `json:"position,omitempty"`
	// where to insert the filter relative to the filter given in
	// @position. Ignored if @position is "head" or "tail". (default:
	// behind)
	Insert *NetfilterInsert `json:"insert,omitempty"`
	// a non-zero interval in microseconds. All packets arriving in
	// the given interval are delayed until the end of the interval.
	Interval uint32 `json:"interval"`
}

// Properties for filter-dump objects.
//
// Since: 2.5
type FilterDumpProperties struct {
	// id of the network device backend to filter
	Netdev string `json:"netdev"`
	// indicates which queue(s) to filter (default: all)
	Queue *NetFilterDirection `json:"queue,omitempty"`
	// indicates whether the filter is enabled ("on") or disabled
	// ("off") (default: "on")
	Status *string `json:"status,omitempty"`
	// specifies where the filter should be inserted in the filter
	// list. "head" means the filter is inserted at the head of the
	// filter list, before any existing filters. "tail" means the
	// filter is inserted at the tail of the filter list, behind any
	// existing filters (default). "id=<id>" means the filter is
	// inserted before or behind the filter specified by <id>,
	// depending on the @insert property. (default: "tail")
	Position *string `json:"position,omitempty"`
	// where to insert the filter relative to the filter given in
	// @position. Ignored if @position is "head" or "tail". (default:
	// behind)
	Insert *NetfilterInsert `json:"insert,omitempty"`
	// the filename where the dumped packets should be stored
	File string `json:"file"`
	// maximum number of bytes in a packet that are stored (default:
	// 65536)
	Maxlen *uint32 `json:"maxlen,omitempty"`
}

// Properties for filter-mirror objects.
//
// Since: 2.6
type FilterMirrorProperties struct {
	// id of the network device backend to filter
	Netdev string `json:"netdev"`
	// indicates which queue(s) to filter (default: all)
	Queue *NetFilterDirection `json:"queue,omitempty"`
	// indicates whether the filter is enabled ("on") or disabled
	// ("off") (default: "on")
	Status *string `json:"status,omitempty"`
	// specifies where the filter should be inserted in the filter
	// list. "head" means the filter is inserted at the head of the
	// filter list, before any existing filters. "tail" means the
	// filter is inserted at the tail of the filter list, behind any
	// existing filters (default). "id=<id>" means the filter is
	// inserted before or behind the filter specified by <id>,
	// depending on the @insert property. (default: "tail")
	Position *string `json:"position,omitempty"`
	// where to insert the filter relative to the filter given in
	// @position. Ignored if @position is "head" or "tail". (default:
	// behind)
	Insert *NetfilterInsert `json:"insert,omitempty"`
	// the name of a character device backend to which all incoming
	// packets are mirrored
	Outdev string `json:"outdev"`
	// if true, vnet header support is enabled (default: false)
	VnetHdrSupport *bool `json:"vnet_hdr_support,omitempty"`
}

// Properties for filter-redirector objects.  At least one of @indev
// or @outdev must be present.  If both are present, they must not
// refer to the same character device backend.
//
// Since: 2.6
type FilterRedirectorProperties struct {
	// id of the network device backend to filter
	Netdev string `json:"netdev"`
	// indicates which queue(s) to filter (default: all)
	Queue *NetFilterDirection `json:"queue,omitempty"`
	// indicates whether the filter is enabled ("on") or disabled
	// ("off") (default: "on")
	Status *string `json:"status,omitempty"`
	// specifies where the filter should be inserted in the filter
	// list. "head" means the filter is inserted at the head of the
	// filter list, before any existing filters. "tail" means the
	// filter is inserted at the tail of the filter list, behind any
	// existing filters (default). "id=<id>" means the filter is
	// inserted before or behind the filter specified by <id>,
	// depending on the @insert property. (default: "tail")
	Position *string `json:"position,omitempty"`
	// where to insert the filter relative to the filter given in
	// @position. Ignored if @position is "head" or "tail". (default:
	// behind)
	Insert *NetfilterInsert `json:"insert,omitempty"`
	// the name of a character device backend from which packets are
	// received and redirected to the filtered network device
	Indev *string `json:"indev,omitempty"`
	// the name of a character device backend to which all incoming
	// packets are redirected
	Outdev *string `json:"outdev,omitempty"`
	// if true, vnet header support is enabled (default: false)
	VnetHdrSupport *bool `json:"vnet_hdr_support,omitempty"`
}

// Properties for filter-rewriter objects.
//
// Since: 2.8
type FilterRewriterProperties struct {
	// id of the network device backend to filter
	Netdev string `json:"netdev"`
	// indicates which queue(s) to filter (default: all)
	Queue *NetFilterDirection `json:"queue,omitempty"`
	// indicates whether the filter is enabled ("on") or disabled
	// ("off") (default: "on")
	Status *string `json:"status,omitempty"`
	// specifies where the filter should be inserted in the filter
	// list. "head" means the filter is inserted at the head of the
	// filter list, before any existing filters. "tail" means the
	// filter is inserted at the tail of the filter list, behind any
	// existing filters (default). "id=<id>" means the filter is
	// inserted before or behind the filter specified by <id>,
	// depending on the @insert property. (default: "tail")
	Position *string `json:"position,omitempty"`
	// where to insert the filter relative to the filter given in
	// @position. Ignored if @position is "head" or "tail". (default:
	// behind)
	Insert *NetfilterInsert `json:"insert,omitempty"`
	// if true, vnet header support is enabled (default: false)
	VnetHdrSupport *bool `json:"vnet_hdr_support,omitempty"`
}

// The struct describes capability for a specific GIC (Generic
// Interrupt Controller) version.  These bits are not only decided by
// QEMU/KVM software version, but also decided by the hardware that
// the program is running upon.
//
// Since: 2.6
type GICCapability struct {
	// version of GIC to be described. Currently, only 2 and 3 are
	// supported.
	Version int64 `json:"version"`
	// whether current QEMU/hardware supports emulated GIC device in
	// user space.
	Emulated bool `json:"emulated"`
	// whether current QEMU/hardware supports hardware accelerated GIC
	// device in kernel.
	Kernel bool `json:"kernel"`
}

// Hyper-V specific guest panic information (HV crash MSRs)
//
// Since: 2.9
type GuestPanicInformationHyperV struct {
	// for Windows, STOP code for the guest crash. For Linux, an error
	// code.
	Arg1 uint64 `json:"arg1"`
	// for Windows, first argument of the STOP. For Linux, the guest
	// OS ID, which has the kernel version in bits 16-47 and 0x8100 in
	// bits 48-63.
	Arg2 uint64 `json:"arg2"`
	// for Windows, second argument of the STOP. For Linux, the
	// program counter of the guest.
	Arg3 uint64 `json:"arg3"`
	// for Windows, third argument of the STOP. For Linux, the RAX
	// register (x86) or the stack pointer (aarch64) of the guest.
	Arg4 uint64 `json:"arg4"`
	// for Windows, fourth argument of the STOP. For x86 Linux, the
	// stack pointer of the guest.
	Arg5 uint64 `json:"arg5"`
}

// S390 specific guest panic information (PSW)
//
// Since: 2.12
type GuestPanicInformationS390 struct {
	// core id of the CPU that crashed
	Core uint32 `json:"core"`
	// control fields of guest PSW
	PswMask uint64 `json:"psw-mask"`
	// guest instruction address
	PswAddr uint64 `json:"psw-addr"`
	// guest crash reason
	Reason S390CrashReason `json:"reason"`
}

// GUID information.
//
// Since: 2.9
type GuidInfo struct {
	// the globally unique identifier
	Guid string `json:"guid"`
}

// .. note:: Management should be prepared to pass through additional
// properties with device_add.
//
// Since: 2.7
type HotpluggableCPU struct {
	// CPU object type for usage with device_add command
	Type string `json:"type"`
	// number of logical VCPU threads @HotpluggableCPU provides
	VcpusCount int64 `json:"vcpus-count"`
	// list of properties to pass for hotplugging a CPU with
	// device_add
	Props CpuInstanceProperties `json:"props"`
	// link to existing CPU object if CPU is present or omitted if CPU
	// is not present.
	QomPath *string `json:"qom-path,omitempty"`
}

// Since: 6.2
type HumanReadableText struct {
	// Formatted output intended for humans.
	HumanReadableText string `json:"human-readable-text"`
}

// hv-balloon provided memory state information
//
// Since: 8.2
type HvBalloonDeviceInfo struct {
	// device's ID
	Id *string `json:"id,omitempty"`
	// physical address in memory, where device is mapped
	Memaddr *uint64 `json:"memaddr,omitempty"`
	// the maximum size of memory that the device can provide
	MaxSize uint64 `json:"max-size"`
	// memory backend linked with device
	Memdev *string `json:"memdev,omitempty"`
}

// Since: 8.2
type HvBalloonDeviceInfoWrapper struct {
	// hv-balloon provided memory state information
	Data HvBalloonDeviceInfo `json:"data"`
}

// hv-balloon guest-provided memory status information.
//
// Since: 8.2
type HvBalloonInfo struct {
	// the amount of memory in use inside the guest plus the amount of
	// the memory unusable inside the guest (ballooned out, offline,
	// etc.)
	Committed uint64 `json:"committed"`
	// the amount of the memory inside the guest available for new
	// allocations ("free")
	Available uint64 `json:"available"`
}

// Properties for iommufd objects.
//
// Since: 9.0
type IOMMUFDProperties struct {
	// file descriptor name previously passed via 'getfd' command,
	// which represents a pre-opened /dev/iommu. This allows the
	// iommufd object to be shared across several subsystems (VFIO,
	// VDPA, ...), and the file descriptor to be shared with other
	// process, e.g. DPDK. (default: QEMU opens /dev/iommu by itself)
	Fd *string `json:"fd,omitempty"`
}

// Information about an iothread
//
// Since: 2.0
type IOThreadInfo struct {
	// the identifier of the iothread
	Id string `json:"id"`
	// ID of the underlying host thread
	ThreadId int64 `json:"thread-id"`
	// maximum polling time in ns, 0 means polling is disabled (since
	// 2.9)
	PollMaxNs int64 `json:"poll-max-ns"`
	// how many ns will be added to polling time, 0 means that it's
	// not configured (since 2.9)
	PollGrow int64 `json:"poll-grow"`
	// how many ns will be removed from polling time, 0 means that
	// it's not configured (since 2.9)
	PollShrink int64 `json:"poll-shrink"`
	// maximum number of requests in a batch for the AIO engine, 0
	// means that the engine will use its default (since 6.1)
	AioMaxBatch int64 `json:"aio-max-batch"`
}

// Describes the subset of virtqueues assigned to an IOThread.
//
// Since: 9.0
type IOThreadVirtQueueMapping struct {
	// the id of IOThread object
	Iothread string `json:"iothread"`
	// an optional array of virtqueue indices that will be handled by
	// this IOThread. When absent, virtqueues are assigned round-robin
	// across all IOThreadVirtQueueMappings provided. Either all
	// IOThreadVirtQueueMappings must have @vqs or none of them must
	// have it.
	Vqs []uint16 `json:"vqs,omitempty"`
}

// Information about a QEMU image file check
//
// Since: 1.4
type ImageCheck struct {
	// name of the image file checked
	Filename string `json:"filename"`
	// format of the image file checked
	Format string `json:"format"`
	// number of unexpected errors occurred during check
	CheckErrors int64 `json:"check-errors"`
	// offset (in bytes) where the image ends, this field is present
	// if the driver for the image format supports it
	ImageEndOffset *int64 `json:"image-end-offset,omitempty"`
	// number of corruptions found during the check if any
	Corruptions *int64 `json:"corruptions,omitempty"`
	// number of leaks found during the check if any
	Leaks *int64 `json:"leaks,omitempty"`
	// number of corruptions fixed during the check if any
	CorruptionsFixed *int64 `json:"corruptions-fixed,omitempty"`
	// number of leaks fixed during the check if any
	LeaksFixed *int64 `json:"leaks-fixed,omitempty"`
	// total number of clusters, this field is present if the driver
	// for the image format supports it
	TotalClusters *int64 `json:"total-clusters,omitempty"`
	// total number of allocated clusters, this field is present if
	// the driver for the image format supports it
	AllocatedClusters *int64 `json:"allocated-clusters,omitempty"`
	// total number of fragmented clusters, this field is present if
	// the driver for the image format supports it
	FragmentedClusters *int64 `json:"fragmented-clusters,omitempty"`
	// total number of compressed clusters, this field is present if
	// the driver for the image format supports it
	CompressedClusters *int64 `json:"compressed-clusters,omitempty"`
}

// Information about a QEMU image file, and potentially its backing
// image
//
// Since: 1.3
type ImageInfo struct {
	// name of the image file
	Filename string `json:"filename"`
	// format of the image file
	Format string `json:"format"`
	// true if image is not cleanly closed
	DirtyFlag *bool `json:"dirty-flag,omitempty"`
	// actual size on disk in bytes of the image
	ActualSize *int64 `json:"actual-size,omitempty"`
	// maximum capacity in bytes of the image
	VirtualSize int64 `json:"virtual-size"`
	// size of a cluster in bytes
	ClusterSize *int64 `json:"cluster-size,omitempty"`
	// true if the image is encrypted
	Encrypted *bool `json:"encrypted,omitempty"`
	// true if the image is compressed (Since 1.7)
	Compressed *bool `json:"compressed,omitempty"`
	// name of the backing file
	BackingFilename *string `json:"backing-filename,omitempty"`
	// full path of the backing file
	FullBackingFilename *string `json:"full-backing-filename,omitempty"`
	// the format of the backing file
	BackingFilenameFormat *string `json:"backing-filename-format,omitempty"`
	// list of VM snapshots
	Snapshots []SnapshotInfo `json:"snapshots,omitempty"`
	// structure supplying additional format-specific information
	// (since 1.7)
	FormatSpecific *ImageInfoSpecific `json:"format-specific,omitempty"`
	// info of the backing image
	BackingImage *ImageInfo `json:"backing-image,omitempty"`
}

// Since: 8.0
type ImageInfoSpecificFile struct {
	// Extent size hint (if available)
	ExtentSizeHint *uint64 `json:"extent-size-hint,omitempty"`
}

// Since: 8.0
type ImageInfoSpecificFileWrapper struct {
	// image information specific to files
	Data ImageInfoSpecificFile `json:"data"`
}

// Since: 2.7
type ImageInfoSpecificLUKSWrapper struct {
	// image information specific to LUKS
	Data QCryptoBlockInfoLUKS `json:"data"`
}

// Since: 1.7
type ImageInfoSpecificQCow2 struct {
	// compatibility level
	Compat string `json:"compat"`
	// the filename of the external data file that is stored in the
	// image and used as a default for opening the image (since: 4.0)
	DataFile *string `json:"data-file,omitempty"`
	// True if the external data file must stay valid as a standalone
	// (read-only) raw image without looking at qcow2 metadata (since:
	// 4.0)
	DataFileRaw *bool `json:"data-file-raw,omitempty"`
	// true if the image has extended L2 entries; only valid for
	// compat >= 1.1 (since 5.2)
	ExtendedL2 *bool `json:"extended-l2,omitempty"`
	// on or off; only valid for compat >= 1.1
	LazyRefcounts *bool `json:"lazy-refcounts,omitempty"`
	// true if the image has been marked corrupt; only valid for
	// compat >= 1.1 (since 2.2)
	Corrupt *bool `json:"corrupt,omitempty"`
	// width of a refcount entry in bits (since 2.3)
	RefcountBits int64 `json:"refcount-bits"`
	// details about encryption parameters; only set if image is
	// encrypted (since 2.10)
	Encrypt *ImageInfoSpecificQCow2Encryption `json:"encrypt,omitempty"`
	// A list of qcow2 bitmap details (since 4.0)
	Bitmaps []Qcow2BitmapInfo `json:"bitmaps,omitempty"`
	// the image cluster compression method (since 5.1)
	CompressionType Qcow2CompressionType `json:"compression-type"`
}

// Since: 2.10
type ImageInfoSpecificQCow2EncryptionBase struct {
	// The encryption format
	Format BlockdevQcow2EncryptionFormat `json:"format"`
}

// Since: 1.7
type ImageInfoSpecificQCow2Wrapper struct {
	// image information specific to QCOW2
	Data ImageInfoSpecificQCow2 `json:"data"`
}

// Since: 6.1
type ImageInfoSpecificRbd struct {
	// Image encryption format
	EncryptionFormat *RbdImageEncryptionFormat `json:"encryption-format,omitempty"`
}

// Since: 6.1
type ImageInfoSpecificRbdWrapper struct {
	// image information specific to RBD
	Data ImageInfoSpecificRbd `json:"data"`
}

// Since: 1.7
type ImageInfoSpecificVmdk struct {
	// The create type of VMDK image
	CreateType string `json:"create-type"`
	// Content id of image
	Cid int64 `json:"cid"`
	// Parent VMDK image's cid
	ParentCid int64 `json:"parent-cid"`
	// List of extent files
	Extents []VmdkExtentInfo `json:"extents"`
}

// Since: 6.1
type ImageInfoSpecificVmdkWrapper struct {
	// image information specific to VMDK
	Data ImageInfoSpecificVmdk `json:"data"`
}

// Captures a socket address or address range in the Internet
// namespace.
//
// Since: 1.3
type InetSocketAddress struct {
	// host part of the address
	Host string `json:"host"`
	// port part of the address
	Port string `json:"port"`
	// true if the host/port are guaranteed to be numeric, false if
	// name resolution should be attempted. Defaults to false. (Since
	// 2.9)
	Numeric *bool `json:"numeric,omitempty"`
	// If present, this is range of possible addresses, with port
	// between @port and @to.
	To *uint16 `json:"to,omitempty"`
	// whether to accept IPv4 addresses, default try both IPv4 and
	// IPv6
	Ipv4 *bool `json:"ipv4,omitempty"`
	// whether to accept IPv6 addresses, default try both IPv4 and
	// IPv6
	Ipv6 *bool `json:"ipv6,omitempty"`
	// enable keep-alive when connecting to this socket. Not supported
	// for passive sockets. (Since 4.2)
	KeepAlive *bool `json:"keep-alive,omitempty"`
	// enable multi-path TCP. (Since 6.1)
	Mptcp *bool `json:"mptcp,omitempty"`
}

type InetSocketAddressBase struct {
	// host part of the address
	Host string `json:"host"`
	// port part of the address
	Port string `json:"port"`
}

// Since: 1.3
type InetSocketAddressWrapper struct {
	// internet domain socket address
	Data InetSocketAddress `json:"data"`
}

// Properties for input-barrier objects.
//
// Since: 4.2
type InputBarrierProperties struct {
	// the screen name as declared in the screens section of
	// barrier.conf
	Name string `json:"name"`
	// hostname of the Barrier server (default: "localhost")
	Server *string `json:"server,omitempty"`
	// TCP port of the Barrier server (default: "24800")
	Port *string `json:"port,omitempty"`
	// x coordinate of the leftmost pixel on the guest screen
	// (default: "0")
	XOrigin *string `json:"x-origin,omitempty"`
	// y coordinate of the topmost pixel on the guest screen (default:
	// "0")
	YOrigin *string `json:"y-origin,omitempty"`
	// the width of secondary screen in pixels (default: "1920")
	Width *string `json:"width,omitempty"`
	// the height of secondary screen in pixels (default: "1080")
	Height *string `json:"height,omitempty"`
}

// Pointer button input event.
//
// Since: 2.0
type InputBtnEvent struct {
	// Which button this event is for.
	Button InputButton `json:"button"`
	// True for key-down and false for key-up events.
	Down bool `json:"down"`
}

// Since: 2.0
type InputBtnEventWrapper struct {
	// Pointer button input event
	Data InputBtnEvent `json:"data"`
}

// Keyboard input event.
//
// Since: 2.0
type InputKeyEvent struct {
	// Which key this event is for.
	Key KeyValue `json:"key"`
	// True for key-down and false for key-up events.
	Down bool `json:"down"`
}

// Since: 2.0
type InputKeyEventWrapper struct {
	// Keyboard input event
	Data InputKeyEvent `json:"data"`
}

// Properties for input-linux objects.
//
// Since: 2.6
type InputLinuxProperties struct {
	// the path of the host evdev device to use
	Evdev string `json:"evdev"`
	// if true, grab is toggled for all devices (e.g. both keyboard
	// and mouse) instead of just one device (default: false)
	GrabAll *bool `json:"grab_all,omitempty"`
	// enables auto-repeat events (default: false)
	Repeat *bool `json:"repeat,omitempty"`
	// the key or key combination that toggles device grab (default:
	// ctrl-ctrl)
	GrabToggle *GrabToggleKeys `json:"grab-toggle,omitempty"`
}

// Pointer motion input event.
//
// Since: 2.0
type InputMoveEvent struct {
	// Which axis is referenced by @value.
	Axis InputAxis `json:"axis"`
	// Pointer position. For absolute coordinates the valid range is 0
	// to 0x7fff.
	Value int64 `json:"value"`
}

// Since: 2.0
type InputMoveEventWrapper struct {
	// Pointer motion input event
	Data InputMoveEvent `json:"data"`
}

// MultiTouch input event.
//
// Since: 8.1
type InputMultiTouchEvent struct {
	// The type of multi-touch event.
	Type InputMultiTouchType `json:"type"`
	// Which slot has generated the event.
	Slot int64 `json:"slot"`
	// ID to correlate this event with previously generated events.
	TrackingId int64 `json:"tracking-id"`
	// Which axis is referenced by @value.
	Axis InputAxis `json:"axis"`
	// Contact position.
	Value int64 `json:"value"`
}

// Since: 8.1
type InputMultiTouchEventWrapper struct {
	// MultiTouch input event
	Data InputMultiTouchEvent `json:"data"`
}

// Since: 1.3
type IntWrapper struct {
	// a numeric key code
	Data int64 `json:"data"`
}

// Properties for iothread objects.
//
// The @aio-max-batch option is available since 6.1.
//
// Since: 2.0
type IothreadProperties struct {
	// maximum number of requests in a batch for the AIO engine, 0
	// means that the engine will use its default. (default: 0)
	AioMaxBatch *int64 `json:"aio-max-batch,omitempty"`
	// minimum number of threads reserved in the thread pool
	// (default:0)
	ThreadPoolMin *int64 `json:"thread-pool-min,omitempty"`
	// maximum number of threads the thread pool can contain
	// (default:64)
	ThreadPoolMax *int64 `json:"thread-pool-max,omitempty"`
	// the maximum number of nanoseconds to busy wait for events. 0
	// means polling is disabled (default: 32768 on POSIX hosts, 0
	// otherwise)
	PollMaxNs *int64 `json:"poll-max-ns,omitempty"`
	// the multiplier used to increase the polling time when the
	// algorithm detects it is missing events due to not polling long
	// enough. 0 selects a default behaviour (default: 0)
	PollGrow *int64 `json:"poll-grow,omitempty"`
	// the divisor used to decrease the polling time when the
	// algorithm detects it is spending too long polling without
	// encountering events. 0 selects a default behaviour (default: 0)
	PollShrink *int64 `json:"poll-shrink,omitempty"`
}

// Information about a job.
//
// Since: 3.0
type JobInfo struct {
	// The job identifier
	Id string `json:"id"`
	// The kind of job that is being performed
	Type JobType `json:"type"`
	// Current job state/status
	Status JobStatus `json:"status"`
	// Progress made until now. The unit is arbitrary and the value
	// can only meaningfully be used for the ratio of @current-
	// progress to @total-progress. The value is monotonically
	// increasing.
	CurrentProgress int64 `json:"current-progress"`
	// Estimated @current-progress value at the completion of the job.
	// This value can arbitrarily change while the job is running, in
	// both directions.
	TotalProgress int64 `json:"total-progress"`
	// If this field is present, the job failed; if it is still
	// missing in the CONCLUDED state, this indicates successful
	// completion. The value is a human-readable error message to
	// describe the reason for the job failure. It should not be
	// parsed by applications.
	Error *string `json:"error,omitempty"`
}

// Information about support for KVM acceleration
//
// Since: 0.14
type KvmInfo struct {
	// true if KVM acceleration is active
	Enabled bool `json:"enabled"`
	// true if KVM acceleration is built into this executable
	Present bool `json:"present"`
}

// Information describing a machine.
//
// Since: 1.2
type MachineInfo struct {
	// the name of the machine
	Name string `json:"name"`
	// an alias for the machine name
	Alias *string `json:"alias,omitempty"`
	// whether the machine is default
	IsDefault *bool `json:"is-default,omitempty"`
	// maximum number of CPUs supported by the machine type (since
	// 1.5)
	CpuMax int64 `json:"cpu-max"`
	// cpu hotplug via -device is supported (since 2.7)
	HotpluggableCpus bool `json:"hotpluggable-cpus"`
	// true if '-numa node,mem' option is supported by the machine
	// type and false otherwise (since 4.1)
	NumaMemSupported bool `json:"numa-mem-supported"`
	// if true, the machine type is deprecated and may be removed in
	// future versions of QEMU according to the QEMU deprecation
	// policy (since 4.1)
	Deprecated bool `json:"deprecated"`
	// default CPU model typename if none is requested via the -cpu
	// argument. (since 4.2)
	DefaultCpuType *string `json:"default-cpu-type,omitempty"`
	// the default ID of initial RAM memory backend (since 5.2)
	DefaultRamId *string `json:"default-ram-id,omitempty"`
	// machine type supports ACPI (since 8.0)
	Acpi bool `json:"acpi"`
	// The machine type's compatibility properties. Only present when
	// query-machines argument @compat-props is true. (since 9.1)
	CompatProps []CompatProperty `json:"compat-props,omitempty"`
}

// Properties for the main-loop object.
//
// Since: 7.1
type MainLoopProperties struct {
	// maximum number of requests in a batch for the AIO engine, 0
	// means that the engine will use its default. (default: 0)
	AioMaxBatch *int64 `json:"aio-max-batch,omitempty"`
	// minimum number of threads reserved in the thread pool
	// (default:0)
	ThreadPoolMin *int64 `json:"thread-pool-min,omitempty"`
	// maximum number of threads the thread pool can contain
	// (default:64)
	ThreadPoolMax *int64 `json:"thread-pool-max,omitempty"`
}

// Mapping information from a virtual block range to a host file range
//
// Since: 2.6
type MapEntry struct {
	// virtual (guest) offset of the first byte described by this
	// entry
	Start int64 `json:"start"`
	// the number of bytes of the mapped virtual range
	Length int64 `json:"length"`
	// reading the image will actually read data from a file (in
	// particular, if @offset is present this means that the sectors
	// are not simply preallocated, but contain actual data in raw
	// format)
	Data bool `json:"data"`
	// whether the virtual blocks read as zeroes
	Zero bool `json:"zero"`
	// true if the data is stored compressed (since 8.2)
	Compressed bool `json:"compressed"`
	// number of layers (0 = top image, 1 = top image's backing file,
	// ..., n - 1 = bottom image (where n is the number of images in
	// the chain)) before reaching one for which the range is
	// allocated
	Depth int64 `json:"depth"`
	// true if this layer provides the data, false if adding a backing
	// layer could impact this region (since 6.1)
	Present bool `json:"present"`
	// if present, the image file stores the data for this range in
	// raw format at the given (host) offset
	Offset *int64 `json:"offset,omitempty"`
	// filename that is referred to by @offset
	Filename *string `json:"filename,omitempty"`
}

// Information about memory backend
//
// Since: 2.1
type Memdev struct {
	// backend's ID if backend has 'id' property (since 2.9)
	Id *string `json:"id,omitempty"`
	// memory backend size
	Size uint64 `json:"size"`
	// whether memory merge support is enabled
	Merge bool `json:"merge"`
	// whether memory backend's memory is included in a core dump
	Dump bool `json:"dump"`
	// whether memory was preallocated
	Prealloc bool `json:"prealloc"`
	// whether memory is private to QEMU or shared (since 6.1)
	Share bool `json:"share"`
	// whether swap space (or huge pages) was reserved if applicable.
	// This corresponds to the user configuration and not the actual
	// behavior implemented in the OS to perform the reservation. For
	// example, Linux will never reserve swap space for shared file
	// mappings. (since 6.1)
	Reserve *bool `json:"reserve,omitempty"`
	// host nodes for its memory policy
	HostNodes []uint16 `json:"host-nodes"`
	// memory policy of memory backend
	Policy HostMemPolicy `json:"policy"`
}

// Properties for memory-backend-epc objects.  The @merge boolean
// option is false by default with epc  The @dump boolean option is
// false by default with epc
//
// Since: 6.2
type MemoryBackendEpcProperties struct {
	// if true, include the memory in core dumps (default depends on
	// the machine type)
	Dump *bool `json:"dump,omitempty"`
	// the list of NUMA host nodes to bind the memory to
	HostNodes []uint16 `json:"host-nodes,omitempty"`
	// if true, mark the memory as mergeable (default depends on the
	// machine type)
	Merge *bool `json:"merge,omitempty"`
	// the NUMA policy (default: 'default')
	Policy *HostMemPolicy `json:"policy,omitempty"`
	// if true, preallocate memory (default: false)
	Prealloc *bool `json:"prealloc,omitempty"`
	// number of CPU threads to use for prealloc (default: 1)
	PreallocThreads *uint32 `json:"prealloc-threads,omitempty"`
	// thread context to use for creation of preallocation threads
	// (default: none) (since 7.2)
	PreallocContext *string `json:"prealloc-context,omitempty"`
	// if false, the memory is private to QEMU; if true, it is shared
	// (default false for backends memory-backend-file and memory-
	// backend-ram, true for backends memory-backend-epc, memory-
	// backend-memfd, and memory-backend-shm)
	Share *bool `json:"share,omitempty"`
	// if true, reserve swap space (or huge pages) if applicable
	// (default: true) (since 6.1)
	Reserve *bool `json:"reserve,omitempty"`
	// size of the memory region in bytes
	Size uint64 `json:"size"`
	// if true, the canonical path is used for ramblock-id. Disable
	// this for 4.0 machine types or older to allow migration with
	// newer QEMU versions. (default: false generally, but true for
	// machine types <= 4.0)
	XUseCanonicalPathForRamblockId *bool `json:"x-use-canonical-path-for-ramblock-id,omitempty"`
}

// Properties for memory-backend-file objects.
//
// Since: 2.1
type MemoryBackendFileProperties struct {
	// if true, include the memory in core dumps (default depends on
	// the machine type)
	Dump *bool `json:"dump,omitempty"`
	// the list of NUMA host nodes to bind the memory to
	HostNodes []uint16 `json:"host-nodes,omitempty"`
	// if true, mark the memory as mergeable (default depends on the
	// machine type)
	Merge *bool `json:"merge,omitempty"`
	// the NUMA policy (default: 'default')
	Policy *HostMemPolicy `json:"policy,omitempty"`
	// if true, preallocate memory (default: false)
	Prealloc *bool `json:"prealloc,omitempty"`
	// number of CPU threads to use for prealloc (default: 1)
	PreallocThreads *uint32 `json:"prealloc-threads,omitempty"`
	// thread context to use for creation of preallocation threads
	// (default: none) (since 7.2)
	PreallocContext *string `json:"prealloc-context,omitempty"`
	// if false, the memory is private to QEMU; if true, it is shared
	// (default false for backends memory-backend-file and memory-
	// backend-ram, true for backends memory-backend-epc, memory-
	// backend-memfd, and memory-backend-shm)
	Share *bool `json:"share,omitempty"`
	// if true, reserve swap space (or huge pages) if applicable
	// (default: true) (since 6.1)
	Reserve *bool `json:"reserve,omitempty"`
	// size of the memory region in bytes
	Size uint64 `json:"size"`
	// if true, the canonical path is used for ramblock-id. Disable
	// this for 4.0 machine types or older to allow migration with
	// newer QEMU versions. (default: false generally, but true for
	// machine types <= 4.0)
	XUseCanonicalPathForRamblockId *bool `json:"x-use-canonical-path-for-ramblock-id,omitempty"`
	// the base address alignment when QEMU mmap(2)s @mem-path. Some
	// backend stores specified by @mem-path require an alignment
	// different than the default one used by QEMU, e.g. the device
	// DAX /dev/dax0.0 requires 2M alignment rather than 4K. In such
	// cases, users can specify the required alignment via this
	// option. 0 selects a default alignment (currently the page
	// size). (default: 0)
	Align *uint64 `json:"align,omitempty"`
	// the offset into the target file that the region starts at. You
	// can use this option to back multiple regions with a single
	// file. Must be a multiple of the page size. (default: 0) (since
	// 8.1)
	Offset *uint64 `json:"offset,omitempty"`
	// if true, the file contents can be destroyed when QEMU exits, to
	// avoid unnecessarily flushing data to the backing file. Note
	// that @discard-data is only an optimization, and QEMU might not
	// discard file contents if it aborts unexpectedly or is
	// terminated using SIGKILL. (default: false)
	DiscardData *bool `json:"discard-data,omitempty"`
	// the path to either a shared memory or huge page filesystem
	// mount
	MemPath string `json:"mem-path"`
	// specifies whether the backing file specified by @mem-path is in
	// host persistent memory that can be accessed using the SNIA NVM
	// programming model (e.g. Intel NVDIMM).
	Pmem *bool `json:"pmem,omitempty"`
	// if true, the backing file is opened read-only; if false, it is
	// opened read-write. (default: false)
	Readonly *bool `json:"readonly,omitempty"`
	// whether to create Read Only Memory (ROM) that cannot be
	// modified by the VM. Any write attempts to such ROM will be
	// denied. Most use cases want writable RAM instead of ROM.
	// However, selected use cases, like R/O NVDIMMs, can benefit from
	// ROM. If set to 'on', create ROM; if set to 'off', create
	// writable RAM; if set to 'auto', the value of the @readonly
	// property is used. This property is primarily helpful when we
	// want to have proper RAM in configurations that would
	// traditionally create ROM before this property was introduced:
	// VM templating, where we want to open a file readonly (@readonly
	// set to true) and mark the memory to be private for QEMU (@share
	// set to false). For this use case, we need writable RAM instead
	// of ROM, and want to set this property to 'off'. (default: auto,
	// since 8.2)
	Rom *OnOffAuto `json:"rom,omitempty"`
}

// Properties for memory-backend-memfd objects.
//
// Since: 2.12
type MemoryBackendMemfdProperties struct {
	// if true, include the memory in core dumps (default depends on
	// the machine type)
	Dump *bool `json:"dump,omitempty"`
	// the list of NUMA host nodes to bind the memory to
	HostNodes []uint16 `json:"host-nodes,omitempty"`
	// if true, mark the memory as mergeable (default depends on the
	// machine type)
	Merge *bool `json:"merge,omitempty"`
	// the NUMA policy (default: 'default')
	Policy *HostMemPolicy `json:"policy,omitempty"`
	// if true, preallocate memory (default: false)
	Prealloc *bool `json:"prealloc,omitempty"`
	// number of CPU threads to use for prealloc (default: 1)
	PreallocThreads *uint32 `json:"prealloc-threads,omitempty"`
	// thread context to use for creation of preallocation threads
	// (default: none) (since 7.2)
	PreallocContext *string `json:"prealloc-context,omitempty"`
	// if false, the memory is private to QEMU; if true, it is shared
	// (default false for backends memory-backend-file and memory-
	// backend-ram, true for backends memory-backend-epc, memory-
	// backend-memfd, and memory-backend-shm)
	Share *bool `json:"share,omitempty"`
	// if true, reserve swap space (or huge pages) if applicable
	// (default: true) (since 6.1)
	Reserve *bool `json:"reserve,omitempty"`
	// size of the memory region in bytes
	Size uint64 `json:"size"`
	// if true, the canonical path is used for ramblock-id. Disable
	// this for 4.0 machine types or older to allow migration with
	// newer QEMU versions. (default: false generally, but true for
	// machine types <= 4.0)
	XUseCanonicalPathForRamblockId *bool `json:"x-use-canonical-path-for-ramblock-id,omitempty"`
	// if true, the file to be created resides in the hugetlbfs
	// filesystem (default: false)
	Hugetlb *bool `json:"hugetlb,omitempty"`
	// the hugetlb page size on systems that support multiple hugetlb
	// page sizes (it must be a power of 2 value supported by the
	// system). 0 selects a default page size. This option is ignored
	// if @hugetlb is false. (default: 0)
	Hugetlbsize *uint64 `json:"hugetlbsize,omitempty"`
	// if true, create a sealed-file, which will block further
	// resizing of the memory (default: true)
	Seal *bool `json:"seal,omitempty"`
}

// Properties for objects of classes derived from memory-backend.
//
// .. note:: prealloc=true and reserve=false cannot be set at the same
// time. With reserve=true, the behavior depends on the operating
// system: for example, Linux will not reserve swap space for shared
// file mappings -- "not applicable". In contrast, reserve=false
// will bail out if it cannot be configured accordingly.
//
// Since: 2.1
type MemoryBackendProperties struct {
	// if true, include the memory in core dumps (default depends on
	// the machine type)
	Dump *bool `json:"dump,omitempty"`
	// the list of NUMA host nodes to bind the memory to
	HostNodes []uint16 `json:"host-nodes,omitempty"`
	// if true, mark the memory as mergeable (default depends on the
	// machine type)
	Merge *bool `json:"merge,omitempty"`
	// the NUMA policy (default: 'default')
	Policy *HostMemPolicy `json:"policy,omitempty"`
	// if true, preallocate memory (default: false)
	Prealloc *bool `json:"prealloc,omitempty"`
	// number of CPU threads to use for prealloc (default: 1)
	PreallocThreads *uint32 `json:"prealloc-threads,omitempty"`
	// thread context to use for creation of preallocation threads
	// (default: none) (since 7.2)
	PreallocContext *string `json:"prealloc-context,omitempty"`
	// if false, the memory is private to QEMU; if true, it is shared
	// (default false for backends memory-backend-file and memory-
	// backend-ram, true for backends memory-backend-epc, memory-
	// backend-memfd, and memory-backend-shm)
	Share *bool `json:"share,omitempty"`
	// if true, reserve swap space (or huge pages) if applicable
	// (default: true) (since 6.1)
	Reserve *bool `json:"reserve,omitempty"`
	// size of the memory region in bytes
	Size uint64 `json:"size"`
	// if true, the canonical path is used for ramblock-id. Disable
	// this for 4.0 machine types or older to allow migration with
	// newer QEMU versions. (default: false generally, but true for
	// machine types <= 4.0)
	XUseCanonicalPathForRamblockId *bool `json:"x-use-canonical-path-for-ramblock-id,omitempty"`
}

// Properties for memory-backend-shm objects.  This memory backend
// supports only shared memory, which is the default.
//
// Since: 9.1
type MemoryBackendShmProperties struct {
	// if true, include the memory in core dumps (default depends on
	// the machine type)
	Dump *bool `json:"dump,omitempty"`
	// the list of NUMA host nodes to bind the memory to
	HostNodes []uint16 `json:"host-nodes,omitempty"`
	// if true, mark the memory as mergeable (default depends on the
	// machine type)
	Merge *bool `json:"merge,omitempty"`
	// the NUMA policy (default: 'default')
	Policy *HostMemPolicy `json:"policy,omitempty"`
	// if true, preallocate memory (default: false)
	Prealloc *bool `json:"prealloc,omitempty"`
	// number of CPU threads to use for prealloc (default: 1)
	PreallocThreads *uint32 `json:"prealloc-threads,omitempty"`
	// thread context to use for creation of preallocation threads
	// (default: none) (since 7.2)
	PreallocContext *string `json:"prealloc-context,omitempty"`
	// if false, the memory is private to QEMU; if true, it is shared
	// (default false for backends memory-backend-file and memory-
	// backend-ram, true for backends memory-backend-epc, memory-
	// backend-memfd, and memory-backend-shm)
	Share *bool `json:"share,omitempty"`
	// if true, reserve swap space (or huge pages) if applicable
	// (default: true) (since 6.1)
	Reserve *bool `json:"reserve,omitempty"`
	// size of the memory region in bytes
	Size uint64 `json:"size"`
	// if true, the canonical path is used for ramblock-id. Disable
	// this for 4.0 machine types or older to allow migration with
	// newer QEMU versions. (default: false generally, but true for
	// machine types <= 4.0)
	XUseCanonicalPathForRamblockId *bool `json:"x-use-canonical-path-for-ramblock-id,omitempty"`
}

// Additional information on memory failures.
//
// Since: 5.2
type MemoryFailureFlags struct {
	// whether a memory failure event is action-required or action-
	// optional (e.g. a failure during memory scrub).
	ActionRequired bool `json:"action-required"`
	// whether the failure occurred while the previous failure was
	// still in progress.
	Recursive bool `json:"recursive"`
}

// Actual memory information in bytes.
//
// Since: 2.11
type MemoryInfo struct {
	// size of "base" memory specified with command line option -m.
	BaseMemory uint64 `json:"base-memory"`
	// size of memory that can be hot-unplugged. This field is omitted
	// if target doesn't support memory hotplug (i.e.
	// CONFIG_MEM_DEVICE not defined at build time).
	PluggedMemory *uint64 `json:"plugged-memory,omitempty"`
}

// Schema for memory size configuration.
//
// Since: 7.1
type MemorySizeConfiguration struct {
	// memory size in bytes
	Size *uint64 `json:"size,omitempty"`
	// maximum hotpluggable memory size in bytes
	MaxSize *uint64 `json:"max-size,omitempty"`
	// number of available memory slots for hotplug
	Slots *uint64 `json:"slots,omitempty"`
}

// Since: 2.4
type MigrateSetParameters struct {
	// Initial delay (in milliseconds) before sending the first
	// announce (Since 4.0)
	AnnounceInitial *uint64 `json:"announce-initial,omitempty"`
	// Maximum delay (in milliseconds) between packets in the
	// announcement (Since 4.0)
	AnnounceMax *uint64 `json:"announce-max,omitempty"`
	// Number of self-announce packets sent after migration (Since
	// 4.0)
	AnnounceRounds *uint64 `json:"announce-rounds,omitempty"`
	// Increase in delay (in milliseconds) between subsequent packets
	// in the announcement (Since 4.0)
	AnnounceStep *uint64 `json:"announce-step,omitempty"`
	// The ratio of bytes_dirty_period and bytes_xfer_period to
	// trigger throttling. It is expressed as percentage. The default
	// value is 50. (Since 5.0)
	ThrottleTriggerThreshold *uint8 `json:"throttle-trigger-threshold,omitempty"`
	// Initial percentage of time guest cpus are throttled when
	// migration auto-converge is activated. The default value is 20.
	// (Since 2.7)
	CpuThrottleInitial *uint8 `json:"cpu-throttle-initial,omitempty"`
	// throttle percentage increase each time auto-converge detects
	// that migration is not making progress. The default value is 10.
	// (Since 2.7)
	CpuThrottleIncrement *uint8 `json:"cpu-throttle-increment,omitempty"`
	// Make CPU throttling slower at tail stage At the tail stage of
	// throttling, the Guest is very sensitive to CPU percentage while
	// the @cpu-throttle -increment is excessive usually at tail
	// stage. If this parameter is true, we will compute the ideal CPU
	// percentage used by the Guest, which may exactly make the dirty
	// rate match the dirty rate threshold. Then we will choose a
	// smaller throttle increment between the one specified by @cpu-
	// throttle-increment and the one generated by ideal CPU
	// percentage. Therefore, it is compatible to traditional
	// throttling, meanwhile the throttle increment won't be excessive
	// at tail stage. The default value is false. (Since 5.1)
	CpuThrottleTailslow *bool `json:"cpu-throttle-tailslow,omitempty"`
	// ID of the 'tls-creds' object that provides credentials for
	// establishing a TLS connection over the migration data channel.
	// On the outgoing side of the migration, the credentials must be
	// for a 'client' endpoint, while for the incoming side the
	// credentials must be for a 'server' endpoint. Setting this to a
	// non-empty string enables TLS for all migrations. An empty
	// string means that QEMU will use plain text mode for migration,
	// rather than TLS. This is the default. (Since 2.7)
	TlsCreds *StrOrNull `json:"tls-creds"`
	// migration target's hostname for validating the server's x509
	// certificate identity. If empty, QEMU will use the hostname from
	// the migration URI, if any. A non-empty value is required when
	// using x509 based TLS credentials and the migration URI does not
	// include a hostname, such as fd: or exec: based migration.
	// (Since 2.7) Note: empty value works only since 2.9.
	TlsHostname *StrOrNull `json:"tls-hostname"`
	// ID of the 'authz' object subclass that provides access control
	// checking of the TLS x509 certificate distinguished name. This
	// object is only resolved at time of use, so can be deleted and
	// recreated on the fly while the migration server is active. If
	// missing, it will default to denying access (Since 4.0)
	TlsAuthz *StrOrNull `json:"tls-authz"`
	// maximum speed for migration, in bytes per second. (Since 2.8)
	MaxBandwidth *uint64 `json:"max-bandwidth,omitempty"`
	// to set the available bandwidth that migration can use during
	// switchover phase. NOTE! This does not limit the bandwidth
	// during switchover, but only for calculations when making
	// decisions to switchover. By default, this value is zero, which
	// means QEMU will estimate the bandwidth automatically. This can
	// be set when the estimated value is not accurate, while the user
	// is able to guarantee such bandwidth is available when switching
	// over. When specified correctly, this can make the switchover
	// decision much more accurate. (Since 8.2)
	AvailSwitchoverBandwidth *uint64 `json:"avail-switchover-bandwidth,omitempty"`
	// set maximum tolerated downtime for migration. maximum downtime
	// in milliseconds (Since 2.8)
	DowntimeLimit *uint64 `json:"downtime-limit,omitempty"`
	// The delay time (in ms) between two COLO checkpoints in periodic
	// mode. (Since 2.8)
	XCheckpointDelay *uint32 `json:"x-checkpoint-delay,omitempty"`
	// Number of channels used to migrate data in parallel. This is
	// the same number that the number of sockets used for migration.
	// The default value is 2 (since 4.0)
	MultifdChannels *uint8 `json:"multifd-channels,omitempty"`
	// cache size to be used by XBZRLE migration. It needs to be a
	// multiple of the target page size and a power of 2 (Since 2.11)
	XbzrleCacheSize *uint64 `json:"xbzrle-cache-size,omitempty"`
	// Background transfer bandwidth during postcopy. Defaults to 0
	// (unlimited). In bytes per second. (Since 3.0)
	MaxPostcopyBandwidth *uint64 `json:"max-postcopy-bandwidth,omitempty"`
	// maximum cpu throttle percentage. Defaults to 99. (Since 3.1)
	MaxCpuThrottle *uint8 `json:"max-cpu-throttle,omitempty"`
	// Which compression method to use. Defaults to none. (Since 5.0)
	MultifdCompression *MultiFDCompression `json:"multifd-compression,omitempty"`
	// Set the compression level to be used in live migration, the
	// compression level is an integer between 0 and 9, where 0 means
	// no compression, 1 means the best compression speed, and 9 means
	// best compression ratio which will consume more CPU. Defaults to
	// 1. (Since 5.0)
	MultifdZlibLevel *uint8 `json:"multifd-zlib-level,omitempty"`
	// Set the compression level to be used in live migration. The
	// level is an integer between 1 and 9, where 1 means the best
	// compression speed, and 9 means the best compression ratio which
	// will consume more CPU. Defaults to 1. (Since 9.2)
	MultifdQatzipLevel *uint8 `json:"multifd-qatzip-level,omitempty"`
	// Set the compression level to be used in live migration, the
	// compression level is an integer between 0 and 20, where 0 means
	// no compression, 1 means the best compression speed, and 20
	// means best compression ratio which will consume more CPU.
	// Defaults to 1. (Since 5.0)
	MultifdZstdLevel *uint8 `json:"multifd-zstd-level,omitempty"`
	// Maps block nodes and bitmaps on them to aliases for the purpose
	// of dirty bitmap migration. Such aliases may for example be the
	// corresponding names on the opposite site. The mapping must be
	// one-to-one, but not necessarily complete: On the source,
	// unmapped bitmaps and all bitmaps on unmapped nodes will be
	// ignored. On the destination, encountering an unmapped alias in
	// the incoming migration stream will result in a report, and all
	// further bitmap migration data will then be discarded. Note that
	// the destination does not know about bitmaps it does not
	// receive, so there is no limitation or requirement regarding the
	// number of bitmaps received, or how they are named, or on which
	// nodes they are placed. By default (when this parameter has
	// never been set), bitmap names are mapped to themselves. Nodes
	// are mapped to their block device name if there is one, and to
	// their node name otherwise. (Since 5.2)
	BlockBitmapMapping []BitmapMigrationNodeAlias `json:"block-bitmap-mapping,omitempty"`
	// Periodic time (in milliseconds) of dirty limit during live
	// migration. Should be in the range 1 to 1000ms. Defaults to
	// 1000ms. (Since 8.1)
	XVcpuDirtyLimitPeriod *uint64 `json:"x-vcpu-dirty-limit-period,omitempty"`
	// Dirtyrate limit (MB/s) during live migration. Defaults to 1.
	// (Since 8.1)
	VcpuDirtyLimit *uint64 `json:"vcpu-dirty-limit,omitempty"`
	// Migration mode. See description in @MigMode. Default is
	// 'normal'. (Since 8.2)
	Mode *MigMode `json:"mode,omitempty"`
	// Whether and how to detect zero pages. See description in
	// @ZeroPageDetection. Default is 'multifd'. (since 9.0)
	ZeroPageDetection *ZeroPageDetection `json:"zero-page-detection,omitempty"`
	// Open migration files with O_DIRECT when possible. This only has
	// effect if the @mapped-ram capability is enabled. (Since 9.1)
	DirectIo *bool `json:"direct-io,omitempty"`
}

func (s MigrateSetParameters) MarshalJSON() ([]byte, error) {
	m := make(map[string]any)

	if s.AnnounceInitial != nil {
		m["announce-initial"] = s.AnnounceInitial
	}

	if s.AnnounceMax != nil {
		m["announce-max"] = s.AnnounceMax
	}

	if s.AnnounceRounds != nil {
		m["announce-rounds"] = s.AnnounceRounds
	}

	if s.AnnounceStep != nil {
		m["announce-step"] = s.AnnounceStep
	}

	if s.ThrottleTriggerThreshold != nil {
		m["throttle-trigger-threshold"] = s.ThrottleTriggerThreshold
	}

	if s.CpuThrottleInitial != nil {
		m["cpu-throttle-initial"] = s.CpuThrottleInitial
	}

	if s.CpuThrottleIncrement != nil {
		m["cpu-throttle-increment"] = s.CpuThrottleIncrement
	}

	if s.CpuThrottleTailslow != nil {
		m["cpu-throttle-tailslow"] = s.CpuThrottleTailslow
	}

	if s.MaxBandwidth != nil {
		m["max-bandwidth"] = s.MaxBandwidth
	}

	if s.AvailSwitchoverBandwidth != nil {
		m["avail-switchover-bandwidth"] = s.AvailSwitchoverBandwidth
	}

	if s.DowntimeLimit != nil {
		m["downtime-limit"] = s.DowntimeLimit
	}

	if s.XCheckpointDelay != nil {
		m["x-checkpoint-delay"] = s.XCheckpointDelay
	}

	if s.MultifdChannels != nil {
		m["multifd-channels"] = s.MultifdChannels
	}

	if s.XbzrleCacheSize != nil {
		m["xbzrle-cache-size"] = s.XbzrleCacheSize
	}

	if s.MaxPostcopyBandwidth != nil {
		m["max-postcopy-bandwidth"] = s.MaxPostcopyBandwidth
	}

	if s.MaxCpuThrottle != nil {
		m["max-cpu-throttle"] = s.MaxCpuThrottle
	}

	if s.MultifdCompression != nil {
		m["multifd-compression"] = s.MultifdCompression
	}

	if s.MultifdZlibLevel != nil {
		m["multifd-zlib-level"] = s.MultifdZlibLevel
	}

	if s.MultifdQatzipLevel != nil {
		m["multifd-qatzip-level"] = s.MultifdQatzipLevel
	}

	if s.MultifdZstdLevel != nil {
		m["multifd-zstd-level"] = s.MultifdZstdLevel
	}

	if s.BlockBitmapMapping != nil {
		m["block-bitmap-mapping"] = s.BlockBitmapMapping
	}

	if s.XVcpuDirtyLimitPeriod != nil {
		m["x-vcpu-dirty-limit-period"] = s.XVcpuDirtyLimitPeriod
	}

	if s.VcpuDirtyLimit != nil {
		m["vcpu-dirty-limit"] = s.VcpuDirtyLimit
	}

	if s.Mode != nil {
		m["mode"] = s.Mode
	}

	if s.ZeroPageDetection != nil {
		m["zero-page-detection"] = s.ZeroPageDetection
	}

	if s.DirectIo != nil {
		m["direct-io"] = s.DirectIo
	}

	if val, absent := s.TlsCreds.ToAnyOrAbsent(); !absent {
		m["tls-creds"] = val
	}

	if val, absent := s.TlsHostname.ToAnyOrAbsent(); !absent {
		m["tls-hostname"] = val
	}

	if val, absent := s.TlsAuthz.ToAnyOrAbsent(); !absent {
		m["tls-authz"] = val
	}

	return json.Marshal(&m)
}

func (s *MigrateSetParameters) UnmarshalJSON(data []byte) error {
	tmp := struct {
		// Initial delay (in milliseconds) before sending the first
		// announce (Since 4.0)
		AnnounceInitial *uint64 `json:"announce-initial,omitempty"`
		// Maximum delay (in milliseconds) between packets in the
		// announcement (Since 4.0)
		AnnounceMax *uint64 `json:"announce-max,omitempty"`
		// Number of self-announce packets sent after migration (Since
		// 4.0)
		AnnounceRounds *uint64 `json:"announce-rounds,omitempty"`
		// Increase in delay (in milliseconds) between subsequent
		// packets in the announcement (Since 4.0)
		AnnounceStep *uint64 `json:"announce-step,omitempty"`
		// The ratio of bytes_dirty_period and bytes_xfer_period to
		// trigger throttling. It is expressed as percentage. The
		// default value is 50. (Since 5.0)
		ThrottleTriggerThreshold *uint8 `json:"throttle-trigger-threshold,omitempty"`
		// Initial percentage of time guest cpus are throttled when
		// migration auto-converge is activated. The default value is
		// 20. (Since 2.7)
		CpuThrottleInitial *uint8 `json:"cpu-throttle-initial,omitempty"`
		// throttle percentage increase each time auto-converge
		// detects that migration is not making progress. The default
		// value is 10. (Since 2.7)
		CpuThrottleIncrement *uint8 `json:"cpu-throttle-increment,omitempty"`
		// Make CPU throttling slower at tail stage At the tail stage
		// of throttling, the Guest is very sensitive to CPU
		// percentage while the @cpu-throttle -increment is excessive
		// usually at tail stage. If this parameter is true, we will
		// compute the ideal CPU percentage used by the Guest, which
		// may exactly make the dirty rate match the dirty rate
		// threshold. Then we will choose a smaller throttle increment
		// between the one specified by @cpu-throttle-increment and
		// the one generated by ideal CPU percentage. Therefore, it is
		// compatible to traditional throttling, meanwhile the
		// throttle increment won't be excessive at tail stage. The
		// default value is false. (Since 5.1)
		CpuThrottleTailslow *bool `json:"cpu-throttle-tailslow,omitempty"`
		// ID of the 'tls-creds' object that provides credentials for
		// establishing a TLS connection over the migration data
		// channel. On the outgoing side of the migration, the
		// credentials must be for a 'client' endpoint, while for the
		// incoming side the credentials must be for a 'server'
		// endpoint. Setting this to a non-empty string enables TLS
		// for all migrations. An empty string means that QEMU will
		// use plain text mode for migration, rather than TLS. This is
		// the default. (Since 2.7)
		TlsCreds StrOrNull `json:"tls-creds"`
		// migration target's hostname for validating the server's
		// x509 certificate identity. If empty, QEMU will use the
		// hostname from the migration URI, if any. A non-empty value
		// is required when using x509 based TLS credentials and the
		// migration URI does not include a hostname, such as fd: or
		// exec: based migration. (Since 2.7) Note: empty value works
		// only since 2.9.
		TlsHostname StrOrNull `json:"tls-hostname"`
		// ID of the 'authz' object subclass that provides access
		// control checking of the TLS x509 certificate distinguished
		// name. This object is only resolved at time of use, so can
		// be deleted and recreated on the fly while the migration
		// server is active. If missing, it will default to denying
		// access (Since 4.0)
		TlsAuthz StrOrNull `json:"tls-authz"`
		// maximum speed for migration, in bytes per second. (Since
		// 2.8)
		MaxBandwidth *uint64 `json:"max-bandwidth,omitempty"`
		// to set the available bandwidth that migration can use
		// during switchover phase. NOTE! This does not limit the
		// bandwidth during switchover, but only for calculations when
		// making decisions to switchover. By default, this value is
		// zero, which means QEMU will estimate the bandwidth
		// automatically. This can be set when the estimated value is
		// not accurate, while the user is able to guarantee such
		// bandwidth is available when switching over. When specified
		// correctly, this can make the switchover decision much more
		// accurate. (Since 8.2)
		AvailSwitchoverBandwidth *uint64 `json:"avail-switchover-bandwidth,omitempty"`
		// set maximum tolerated downtime for migration. maximum
		// downtime in milliseconds (Since 2.8)
		DowntimeLimit *uint64 `json:"downtime-limit,omitempty"`
		// The delay time (in ms) between two COLO checkpoints in
		// periodic mode. (Since 2.8)
		XCheckpointDelay *uint32 `json:"x-checkpoint-delay,omitempty"`
		// Number of channels used to migrate data in parallel. This
		// is the same number that the number of sockets used for
		// migration. The default value is 2 (since 4.0)
		MultifdChannels *uint8 `json:"multifd-channels,omitempty"`
		// cache size to be used by XBZRLE migration. It needs to be a
		// multiple of the target page size and a power of 2 (Since
		// 2.11)
		XbzrleCacheSize *uint64 `json:"xbzrle-cache-size,omitempty"`
		// Background transfer bandwidth during postcopy. Defaults to
		// 0 (unlimited). In bytes per second. (Since 3.0)
		MaxPostcopyBandwidth *uint64 `json:"max-postcopy-bandwidth,omitempty"`
		// maximum cpu throttle percentage. Defaults to 99. (Since
		// 3.1)
		MaxCpuThrottle *uint8 `json:"max-cpu-throttle,omitempty"`
		// Which compression method to use. Defaults to none. (Since
		// 5.0)
		MultifdCompression *MultiFDCompression `json:"multifd-compression,omitempty"`
		// Set the compression level to be used in live migration, the
		// compression level is an integer between 0 and 9, where 0
		// means no compression, 1 means the best compression speed,
		// and 9 means best compression ratio which will consume more
		// CPU. Defaults to 1. (Since 5.0)
		MultifdZlibLevel *uint8 `json:"multifd-zlib-level,omitempty"`
		// Set the compression level to be used in live migration. The
		// level is an integer between 1 and 9, where 1 means the best
		// compression speed, and 9 means the best compression ratio
		// which will consume more CPU. Defaults to 1. (Since 9.2)
		MultifdQatzipLevel *uint8 `json:"multifd-qatzip-level,omitempty"`
		// Set the compression level to be used in live migration, the
		// compression level is an integer between 0 and 20, where 0
		// means no compression, 1 means the best compression speed,
		// and 20 means best compression ratio which will consume more
		// CPU. Defaults to 1. (Since 5.0)
		MultifdZstdLevel *uint8 `json:"multifd-zstd-level,omitempty"`
		// Maps block nodes and bitmaps on them to aliases for the
		// purpose of dirty bitmap migration. Such aliases may for
		// example be the corresponding names on the opposite site.
		// The mapping must be one-to-one, but not necessarily
		// complete: On the source, unmapped bitmaps and all bitmaps
		// on unmapped nodes will be ignored. On the destination,
		// encountering an unmapped alias in the incoming migration
		// stream will result in a report, and all further bitmap
		// migration data will then be discarded. Note that the
		// destination does not know about bitmaps it does not
		// receive, so there is no limitation or requirement regarding
		// the number of bitmaps received, or how they are named, or
		// on which nodes they are placed. By default (when this
		// parameter has never been set), bitmap names are mapped to
		// themselves. Nodes are mapped to their block device name if
		// there is one, and to their node name otherwise. (Since 5.2)
		BlockBitmapMapping []BitmapMigrationNodeAlias `json:"block-bitmap-mapping,omitempty"`
		// Periodic time (in milliseconds) of dirty limit during live
		// migration. Should be in the range 1 to 1000ms. Defaults to
		// 1000ms. (Since 8.1)
		XVcpuDirtyLimitPeriod *uint64 `json:"x-vcpu-dirty-limit-period,omitempty"`
		// Dirtyrate limit (MB/s) during live migration. Defaults to
		// 1. (Since 8.1)
		VcpuDirtyLimit *uint64 `json:"vcpu-dirty-limit,omitempty"`
		// Migration mode. See description in @MigMode. Default is
		// 'normal'. (Since 8.2)
		Mode *MigMode `json:"mode,omitempty"`
		// Whether and how to detect zero pages. See description in
		// @ZeroPageDetection. Default is 'multifd'. (since 9.0)
		ZeroPageDetection *ZeroPageDetection `json:"zero-page-detection,omitempty"`
		// Open migration files with O_DIRECT when possible. This only
		// has effect if the @mapped-ram capability is enabled. (Since
		// 9.1)
		DirectIo *bool `json:"direct-io,omitempty"`
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	s.AnnounceInitial = tmp.AnnounceInitial
	s.AnnounceMax = tmp.AnnounceMax
	s.AnnounceRounds = tmp.AnnounceRounds
	s.AnnounceStep = tmp.AnnounceStep
	s.ThrottleTriggerThreshold = tmp.ThrottleTriggerThreshold
	s.CpuThrottleInitial = tmp.CpuThrottleInitial
	s.CpuThrottleIncrement = tmp.CpuThrottleIncrement
	s.CpuThrottleTailslow = tmp.CpuThrottleTailslow
	s.MaxBandwidth = tmp.MaxBandwidth
	s.AvailSwitchoverBandwidth = tmp.AvailSwitchoverBandwidth
	s.DowntimeLimit = tmp.DowntimeLimit
	s.XCheckpointDelay = tmp.XCheckpointDelay
	s.MultifdChannels = tmp.MultifdChannels
	s.XbzrleCacheSize = tmp.XbzrleCacheSize
	s.MaxPostcopyBandwidth = tmp.MaxPostcopyBandwidth
	s.MaxCpuThrottle = tmp.MaxCpuThrottle
	s.MultifdCompression = tmp.MultifdCompression
	s.MultifdZlibLevel = tmp.MultifdZlibLevel
	s.MultifdQatzipLevel = tmp.MultifdQatzipLevel
	s.MultifdZstdLevel = tmp.MultifdZstdLevel
	s.BlockBitmapMapping = tmp.BlockBitmapMapping
	s.XVcpuDirtyLimitPeriod = tmp.XVcpuDirtyLimitPeriod
	s.VcpuDirtyLimit = tmp.VcpuDirtyLimit
	s.Mode = tmp.Mode
	s.ZeroPageDetection = tmp.ZeroPageDetection
	s.DirectIo = tmp.DirectIo

	if _, absent := (&tmp.TlsCreds).ToAnyOrAbsent(); !absent {
		s.TlsCreds = &tmp.TlsCreds
	}

	if _, absent := (&tmp.TlsHostname).ToAnyOrAbsent(); !absent {
		s.TlsHostname = &tmp.TlsHostname
	}

	if _, absent := (&tmp.TlsAuthz).ToAnyOrAbsent(); !absent {
		s.TlsAuthz = &tmp.TlsAuthz
	}

	return nil
}

// Migration capability information
//
// Since: 1.2
type MigrationCapabilityStatus struct {
	// capability enum
	Capability MigrationCapability `json:"capability"`
	// capability state bool
	State bool `json:"state"`
}

// Migration stream channel parameters.
//
// Since: 8.1
type MigrationChannel struct {
	// Channel type for transferring packet information.
	ChannelType MigrationChannelType `json:"channel-type"`
	// Migration endpoint configuration on destination interface.
	Addr MigrationAddress `json:"addr"`
}

// Since: 8.2
type MigrationExecCommand struct {
	// command (list head) and arguments to execute.
	Args []string `json:"args"`
}

// Information about current migration process.
//
// Since: 0.14
type MigrationInfo struct {
	// @MigrationStatus describing the current migration status. If
	// this field is not returned, no migration process has been
	// initiated
	Status *MigrationStatus `json:"status,omitempty"`
	// @MigrationStats containing detailed migration status, only
	// returned if status is 'active' or 'completed'(since 1.2)
	Ram *MigrationStats `json:"ram,omitempty"`
	// @VfioStats containing detailed VFIO devices migration
	// statistics, only returned if VFIO device is present, migration
	// is supported by all VFIO devices and status is 'active' or
	// 'completed' (since 5.2)
	Vfio *VfioStats `json:"vfio,omitempty"`
	// @XBZRLECacheStats containing detailed XBZRLE migration
	// statistics, only returned if XBZRLE feature is on and status is
	// 'active' or 'completed' (since 1.2)
	XbzrleCache *XBZRLECacheStats `json:"xbzrle-cache,omitempty"`
	// total amount of milliseconds since migration started. If
	// migration has ended, it returns the total migration time.
	// (since 1.2)
	TotalTime *int64 `json:"total-time,omitempty"`
	// only present while migration is active expected downtime in
	// milliseconds for the guest in last walk of the dirty bitmap.
	// (since 1.3)
	ExpectedDowntime *int64 `json:"expected-downtime,omitempty"`
	// only present when migration finishes correctly total downtime
	// in milliseconds for the guest. (since 1.3)
	Downtime *int64 `json:"downtime,omitempty"`
	// amount of setup time in milliseconds *before* the iterations
	// begin but *after* the QMP command is issued. This is designed
	// to provide an accounting of any activities (such as RDMA
	// pinning) which may be expensive, but do not actually occur
	// during the iterative migration rounds themselves. (since 1.6)
	SetupTime *int64 `json:"setup-time,omitempty"`
	// percentage of time guest cpus are being throttled during auto-
	// converge. This is only present when auto-converge has started
	// throttling guest cpus. (Since 2.7)
	CpuThrottlePercentage *int64 `json:"cpu-throttle-percentage,omitempty"`
	// the human readable error description string. Clients should not
	// attempt to parse the error strings. (Since 2.7)
	ErrorDesc *string `json:"error-desc,omitempty"`
	// A list of reasons an outgoing migration is blocked. Present and
	// non-empty when migration is blocked. (since 6.0)
	BlockedReasons []string `json:"blocked-reasons,omitempty"`
	// total time when all vCPU were blocked during postcopy live
	// migration. This is only present when the postcopy-blocktime
	// migration capability is enabled. (Since 3.0)
	PostcopyBlocktime *uint32 `json:"postcopy-blocktime,omitempty"`
	// list of the postcopy blocktime per vCPU. This is only present
	// when the postcopy-blocktime migration capability is enabled.
	// (Since 3.0)
	PostcopyVcpuBlocktime []uint32 `json:"postcopy-vcpu-blocktime,omitempty"`
	// Only used for tcp, to know what the real port is (Since 4.0)
	SocketAddress []SocketAddress `json:"socket-address,omitempty"`
	// Maximum throttle time (in microseconds) of virtual CPUs each
	// dirty ring full round, which shows how MigrationCapability
	// dirty-limit affects the guest during live migration. (Since
	// 8.1)
	DirtyLimitThrottleTimePerRound *uint64 `json:"dirty-limit-throttle-time-per-round,omitempty"`
	// Estimated average dirty ring full time (in microseconds) for
	// each dirty ring full round. The value equals the dirty ring
	// memory size divided by the average dirty page rate of the
	// virtual CPU, which can be used to observe the average memory
	// load of the virtual CPU indirectly. Note that zero means guest
	// doesn't dirty memory. (Since 8.1)
	DirtyLimitRingFullTime *uint64 `json:"dirty-limit-ring-full-time,omitempty"`
}

// The optional members aren't actually optional.
//
// Since: 2.4
type MigrationParameters struct {
	// Initial delay (in milliseconds) before sending the first
	// announce (Since 4.0)
	AnnounceInitial *uint64 `json:"announce-initial,omitempty"`
	// Maximum delay (in milliseconds) between packets in the
	// announcement (Since 4.0)
	AnnounceMax *uint64 `json:"announce-max,omitempty"`
	// Number of self-announce packets sent after migration (Since
	// 4.0)
	AnnounceRounds *uint64 `json:"announce-rounds,omitempty"`
	// Increase in delay (in milliseconds) between subsequent packets
	// in the announcement (Since 4.0)
	AnnounceStep *uint64 `json:"announce-step,omitempty"`
	// The ratio of bytes_dirty_period and bytes_xfer_period to
	// trigger throttling. It is expressed as percentage. The default
	// value is 50. (Since 5.0)
	ThrottleTriggerThreshold *uint8 `json:"throttle-trigger-threshold,omitempty"`
	// Initial percentage of time guest cpus are throttled when
	// migration auto-converge is activated. (Since 2.7)
	CpuThrottleInitial *uint8 `json:"cpu-throttle-initial,omitempty"`
	// throttle percentage increase each time auto-converge detects
	// that migration is not making progress. (Since 2.7)
	CpuThrottleIncrement *uint8 `json:"cpu-throttle-increment,omitempty"`
	// Make CPU throttling slower at tail stage At the tail stage of
	// throttling, the Guest is very sensitive to CPU percentage while
	// the @cpu-throttle -increment is excessive usually at tail
	// stage. If this parameter is true, we will compute the ideal CPU
	// percentage used by the Guest, which may exactly make the dirty
	// rate match the dirty rate threshold. Then we will choose a
	// smaller throttle increment between the one specified by @cpu-
	// throttle-increment and the one generated by ideal CPU
	// percentage. Therefore, it is compatible to traditional
	// throttling, meanwhile the throttle increment won't be excessive
	// at tail stage. The default value is false. (Since 5.1)
	CpuThrottleTailslow *bool `json:"cpu-throttle-tailslow,omitempty"`
	// ID of the 'tls-creds' object that provides credentials for
	// establishing a TLS connection over the migration data channel.
	// On the outgoing side of the migration, the credentials must be
	// for a 'client' endpoint, while for the incoming side the
	// credentials must be for a 'server' endpoint. An empty string
	// means that QEMU will use plain text mode for migration, rather
	// than TLS. (Since 2.7) Note: 2.8 omits empty @tls-creds instead.
	TlsCreds *string `json:"tls-creds,omitempty"`
	// migration target's hostname for validating the server's x509
	// certificate identity. If empty, QEMU will use the hostname from
	// the migration URI, if any. (Since 2.7) Note: 2.8 omits empty
	// @tls-hostname instead.
	TlsHostname *string `json:"tls-hostname,omitempty"`
	// ID of the 'authz' object subclass that provides access control
	// checking of the TLS x509 certificate distinguished name. (Since
	// 4.0)
	TlsAuthz *string `json:"tls-authz,omitempty"`
	// maximum speed for migration, in bytes per second. (Since 2.8)
	MaxBandwidth *uint64 `json:"max-bandwidth,omitempty"`
	// to set the available bandwidth that migration can use during
	// switchover phase. NOTE! This does not limit the bandwidth
	// during switchover, but only for calculations when making
	// decisions to switchover. By default, this value is zero, which
	// means QEMU will estimate the bandwidth automatically. This can
	// be set when the estimated value is not accurate, while the user
	// is able to guarantee such bandwidth is available when switching
	// over. When specified correctly, this can make the switchover
	// decision much more accurate. (Since 8.2)
	AvailSwitchoverBandwidth *uint64 `json:"avail-switchover-bandwidth,omitempty"`
	// set maximum tolerated downtime for migration. maximum downtime
	// in milliseconds (Since 2.8)
	DowntimeLimit *uint64 `json:"downtime-limit,omitempty"`
	// the delay time between two COLO checkpoints. (Since 2.8)
	XCheckpointDelay *uint32 `json:"x-checkpoint-delay,omitempty"`
	// Number of channels used to migrate data in parallel. This is
	// the same number that the number of sockets used for migration.
	// The default value is 2 (since 4.0)
	MultifdChannels *uint8 `json:"multifd-channels,omitempty"`
	// cache size to be used by XBZRLE migration. It needs to be a
	// multiple of the target page size and a power of 2 (Since 2.11)
	XbzrleCacheSize *uint64 `json:"xbzrle-cache-size,omitempty"`
	// Background transfer bandwidth during postcopy. Defaults to 0
	// (unlimited). In bytes per second. (Since 3.0)
	MaxPostcopyBandwidth *uint64 `json:"max-postcopy-bandwidth,omitempty"`
	// maximum cpu throttle percentage. Defaults to 99. (Since 3.1)
	MaxCpuThrottle *uint8 `json:"max-cpu-throttle,omitempty"`
	// Which compression method to use. Defaults to none. (Since 5.0)
	MultifdCompression *MultiFDCompression `json:"multifd-compression,omitempty"`
	// Set the compression level to be used in live migration, the
	// compression level is an integer between 0 and 9, where 0 means
	// no compression, 1 means the best compression speed, and 9 means
	// best compression ratio which will consume more CPU. Defaults to
	// 1. (Since 5.0)
	MultifdZlibLevel *uint8 `json:"multifd-zlib-level,omitempty"`
	// Set the compression level to be used in live migration. The
	// level is an integer between 1 and 9, where 1 means the best
	// compression speed, and 9 means the best compression ratio which
	// will consume more CPU. Defaults to 1. (Since 9.2)
	MultifdQatzipLevel *uint8 `json:"multifd-qatzip-level,omitempty"`
	// Set the compression level to be used in live migration, the
	// compression level is an integer between 0 and 20, where 0 means
	// no compression, 1 means the best compression speed, and 20
	// means best compression ratio which will consume more CPU.
	// Defaults to 1. (Since 5.0)
	MultifdZstdLevel *uint8 `json:"multifd-zstd-level,omitempty"`
	// Maps block nodes and bitmaps on them to aliases for the purpose
	// of dirty bitmap migration. Such aliases may for example be the
	// corresponding names on the opposite site. The mapping must be
	// one-to-one, but not necessarily complete: On the source,
	// unmapped bitmaps and all bitmaps on unmapped nodes will be
	// ignored. On the destination, encountering an unmapped alias in
	// the incoming migration stream will result in a report, and all
	// further bitmap migration data will then be discarded. Note that
	// the destination does not know about bitmaps it does not
	// receive, so there is no limitation or requirement regarding the
	// number of bitmaps received, or how they are named, or on which
	// nodes they are placed. By default (when this parameter has
	// never been set), bitmap names are mapped to themselves. Nodes
	// are mapped to their block device name if there is one, and to
	// their node name otherwise. (Since 5.2)
	BlockBitmapMapping []BitmapMigrationNodeAlias `json:"block-bitmap-mapping,omitempty"`
	// Periodic time (in milliseconds) of dirty limit during live
	// migration. Should be in the range 1 to 1000ms. Defaults to
	// 1000ms. (Since 8.1)
	XVcpuDirtyLimitPeriod *uint64 `json:"x-vcpu-dirty-limit-period,omitempty"`
	// Dirtyrate limit (MB/s) during live migration. Defaults to 1.
	// (Since 8.1)
	VcpuDirtyLimit *uint64 `json:"vcpu-dirty-limit,omitempty"`
	// Migration mode. See description in @MigMode. Default is
	// 'normal'. (Since 8.2)
	Mode *MigMode `json:"mode,omitempty"`
	// Whether and how to detect zero pages. See description in
	// @ZeroPageDetection. Default is 'multifd'. (since 9.0)
	ZeroPageDetection *ZeroPageDetection `json:"zero-page-detection,omitempty"`
	// Open migration files with O_DIRECT when possible. This only has
	// effect if the @mapped-ram capability is enabled. (Since 9.1)
	DirectIo *bool `json:"direct-io,omitempty"`
}

// Detailed migration status.
//
// Since: 0.14
type MigrationStats struct {
	// amount of bytes already transferred to the target VM
	Transferred int64 `json:"transferred"`
	// amount of bytes remaining to be transferred to the target VM
	Remaining int64 `json:"remaining"`
	// total amount of bytes involved in the migration process
	Total int64 `json:"total"`
	// number of duplicate (zero) pages (since 1.2)
	Duplicate int64 `json:"duplicate"`
	// number of normal pages (since 1.2)
	Normal int64 `json:"normal"`
	// number of normal bytes sent (since 1.2)
	NormalBytes int64 `json:"normal-bytes"`
	// number of pages dirtied by second by the guest (since 1.3)
	DirtyPagesRate int64 `json:"dirty-pages-rate"`
	// throughput in megabits/sec. (since 1.6)
	Mbps float64 `json:"mbps"`
	// number of times that dirty ram was synchronized (since 2.1)
	DirtySyncCount int64 `json:"dirty-sync-count"`
	// The number of page requests received from the destination
	// (since 2.7)
	PostcopyRequests int64 `json:"postcopy-requests"`
	// The number of bytes per page for the various page-based
	// statistics (since 2.10)
	PageSize int64 `json:"page-size"`
	// The number of bytes sent through multifd (since 3.0)
	MultifdBytes uint64 `json:"multifd-bytes"`
	// the number of memory pages transferred per second (Since 4.0)
	PagesPerSecond uint64 `json:"pages-per-second"`
	// The number of bytes sent in the pre-copy phase (since 7.0).
	PrecopyBytes uint64 `json:"precopy-bytes"`
	// The number of bytes sent while the guest is paused (since 7.0).
	DowntimeBytes uint64 `json:"downtime-bytes"`
	// The number of bytes sent during the post-copy phase (since
	// 7.0).
	PostcopyBytes uint64 `json:"postcopy-bytes"`
	// Number of times dirty RAM synchronization could not avoid
	// copying dirty pages. This is between 0 and @dirty-sync-count *
	// @multifd-channels. (since 7.1)
	DirtySyncMissedZeroCopy uint64 `json:"dirty-sync-missed-zero-copy"`
}

// Information about migrationthreads
//
// Since: 7.2
type MigrationThreadInfo struct {
	// the name of migration thread
	Name string `json:"name"`
	// ID of the underlying host thread
	ThreadId int64 `json:"thread-id"`
}

// Options to be used for adding a new monitor.
//
// Since: 5.0
type MonitorOptions struct {
	// Name of the monitor
	Id *string `json:"id,omitempty"`
	// Selects the monitor mode (default: readline in the system
	// emulator, control in qemu-storage-daemon)
	Mode *MonitorMode `json:"mode,omitempty"`
	// Enables pretty printing (QMP only)
	Pretty *bool `json:"pretty,omitempty"`
	// Name of a character device to expose the monitor on
	Chardev string `json:"chardev"`
}

// Information about a mouse device.
//
// Since: 0.14
type MouseInfo struct {
	// the name of the mouse device
	Name string `json:"name"`
	// the index of the mouse device
	Index int64 `json:"index"`
	// true if this device is currently receiving mouse events
	Current bool `json:"current"`
	// true if this device supports absolute coordinates as input
	Absolute bool `json:"absolute"`
}

// Captures the address of the socket
//
// Since: 2.9
type NFSServer struct {
	// transport type used for NFS (only TCP supported)
	Type NFSTransport `json:"type"`
	// host address for NFS server
	Host string `json:"host"`
}

// Guest name information.
//
// Since: 0.14
type NameInfo struct {
	// The name of the guest
	Name *string `json:"name,omitempty"`
}

// An NBD block export, per legacy nbd-server-add command.
//
// Since: 5.0
type NbdServerAddOptions struct {
	// Export name. If unspecified, the @device parameter is used as
	// the export name. (Since 2.12)
	Name *string `json:"name,omitempty"`
	// Free-form description of the export, up to 4096 bytes. (Since
	// 5.0)
	Description *string `json:"description,omitempty"`
	// The device name or node name of the node to be exported
	Device string `json:"device"`
	// Whether clients should be able to write to the device via the
	// NBD connection (default false).
	Writable *bool `json:"writable,omitempty"`
	// Also export a single dirty bitmap reachable from @device, so
	// the NBD client can use NBD_OPT_SET_META_CONTEXT with the
	// metadata context name "qemu:dirty-bitmap:BITMAP" to inspect the
	// bitmap (since 4.0).
	Bitmap *string `json:"bitmap,omitempty"`
}

// Keep this type consistent with the nbd-server-start arguments.  The
// only intended difference is using SocketAddress instead of
// SocketAddressLegacy.
//
// Since: 4.2
type NbdServerOptions struct {
	// Address on which to listen.
	Addr SocketAddress `json:"addr"`
	// Time limit, in seconds, at which a client that has not
	// completed the negotiation handshake will be disconnected, 0 for
	// no limit (since 10.0; default: 10).
	HandshakeMaxSeconds *uint32 `json:"handshake-max-seconds,omitempty"`
	// ID of the TLS credentials object (since 2.6).
	TlsCreds *string `json:"tls-creds,omitempty"`
	// ID of the QAuthZ authorization object used to validate the
	// client's x509 distinguished name. This object is is only
	// resolved at time of use, so can be deleted and recreated on the
	// fly while the NBD server is active. If missing, it will default
	// to denying access (since 4.0).
	TlsAuthz *string `json:"tls-authz,omitempty"`
	// The maximum number of connections to allow at the same time, 0
	// for unlimited. Setting this to 1 also stops the server from
	// advertising multiple client support (since 5.2; default: 100)
	MaxConnections *uint32 `json:"max-connections,omitempty"`
}

// Create a new Network Interface Card.
//
// Since: 1.2
type NetLegacyNicOptions struct {
	// id of -netdev to connect to
	Netdev *string `json:"netdev,omitempty"`
	// MAC address
	Macaddr *string `json:"macaddr,omitempty"`
	// device model (e1000, rtl8139, virtio etc.)
	Model *string `json:"model,omitempty"`
	// PCI device address
	Addr *string `json:"addr,omitempty"`
	// number of MSI-x vectors, 0 to disable MSI-X
	Vectors *uint32 `json:"vectors,omitempty"`
}

// AF_XDP network backend
//
// Since: 8.2
type NetdevAFXDPOptions struct {
	// The name of an existing network interface.
	Ifname string `json:"ifname"`
	// Attach mode for a default XDP program. If not specified, then
	// 'native' will be tried first, then 'skb'.
	Mode *AFXDPMode `json:"mode,omitempty"`
	// Force XDP copy mode even if device supports zero-copy.
	// (default: false)
	ForceCopy *bool `json:"force-copy,omitempty"`
	// number of queues to be used for multiqueue interfaces (default:
	// 1).
	Queues *int64 `json:"queues,omitempty"`
	// Use @queues starting from this queue number (default: 0).
	StartQueue *int64 `json:"start-queue,omitempty"`
	// Don't load a default XDP program, use one already loaded to the
	// interface (default: false). Requires @sock-fds.
	Inhibit *bool `json:"inhibit,omitempty"`
	// A colon (:) separated list of file descriptors for already open
	// but not bound AF_XDP sockets in the queue order. One fd per
	// queue. These descriptors should already be added into XDP
	// socket map for corresponding queues. Requires @inhibit.
	SockFds *string `json:"sock-fds,omitempty"`
}

// Connect a host TAP network interface to a host bridge device.
//
// Since: 1.2
type NetdevBridgeOptions struct {
	// bridge name
	Br *string `json:"br,omitempty"`
	// command to execute to configure bridge
	Helper *string `json:"helper,omitempty"`
}

// Configuration info for datagram socket netdev.
//
// Only SocketAddress types 'unix', 'inet' and 'fd' are supported.  If
// remote address is present and it's a multicast address, local
// address is optional. Otherwise local address is required and remote
// address is optional.  .. table:: Valid parameters combination table
// :widths: auto    ============= ======== =====   remote     local
// okay?   ============= ======== =====   absent     absent  no
// absent     not fd  no   absent     fd    yes   multicast   absent
// yes   multicast   present  yes   not multicast absent  no   not
// multicast present  yes   ============= ======== =====
//
// Since: 7.2
type NetdevDgramOptions struct {
	// local address
	Local *SocketAddress `json:"local,omitempty"`
	// remote address
	Remote *SocketAddress `json:"remote,omitempty"`
}

// Connect two or more net clients through a software hub.
//
// Since: 1.2
type NetdevHubPortOptions struct {
	// hub identifier number
	Hubid int32 `json:"hubid"`
	// used to connect hub to a netdev instead of a device (since
	// 2.12)
	Netdev *string `json:"netdev,omitempty"`
}

// Configure an Ethernet over L2TPv3 tunnel.
//
// Since: 2.1
type NetdevL2TPv3Options struct {
	// source address
	Src string `json:"src"`
	// destination address
	Dst string `json:"dst"`
	// source port - mandatory for udp, optional for ip
	Srcport *string `json:"srcport,omitempty"`
	// destination port - mandatory for udp, optional for ip
	Dstport *string `json:"dstport,omitempty"`
	// force the use of ipv6
	Ipv6 *bool `json:"ipv6,omitempty"`
	// use the udp version of l2tpv3 encapsulation
	Udp *bool `json:"udp,omitempty"`
	// use 64 bit cookies
	Cookie64 *bool `json:"cookie64,omitempty"`
	// have sequence counter
	Counter *bool `json:"counter,omitempty"`
	// pin sequence counter to zero - workaround for buggy
	// implementations or networks with packet reorder
	Pincounter *bool `json:"pincounter,omitempty"`
	// 32 or 64 bit transmit cookie
	Txcookie *uint64 `json:"txcookie,omitempty"`
	// 32 or 64 bit receive cookie
	Rxcookie *uint64 `json:"rxcookie,omitempty"`
	// 32 bit transmit session
	Txsession uint32 `json:"txsession"`
	// 32 bit receive session - if not specified set to the same value
	// as transmit
	Rxsession *uint32 `json:"rxsession,omitempty"`
	// additional offset - allows the insertion of additional
	// application-specific data before the packet payload
	Offset *uint32 `json:"offset,omitempty"`
}

// Connect a client to a netmap-enabled NIC or to a VALE switch port
//
// Since: 2.0
type NetdevNetmapOptions struct {
	// Either the name of an existing network interface supported by
	// netmap, or the name of a VALE port (created on the fly). A VALE
	// port name is in the form 'valeXXX:YYY', where XXX and YYY are
	// non-negative integers. XXX identifies a switch and YYY
	// identifies a port of the switch. VALE ports having the same XXX
	// are therefore connected to the same switch.
	Ifname string `json:"ifname"`
	// path of the netmap device (default: '/dev/netmap').
	Devname *string `json:"devname,omitempty"`
}

// Socket netdevs are used to establish a network connection to
// another QEMU virtual machine via a TCP socket.
//
// Since: 1.2
type NetdevSocketOptions struct {
	// file descriptor of an already opened socket
	Fd *string `json:"fd,omitempty"`
	// port number, and optional hostname, to listen on
	Listen *string `json:"listen,omitempty"`
	// port number, and optional hostname, to connect to
	Connect *string `json:"connect,omitempty"`
	// UDP multicast address and port number
	Mcast *string `json:"mcast,omitempty"`
	// source address and port for multicast and udp packets
	Localaddr *string `json:"localaddr,omitempty"`
	// UDP unicast address and port number
	Udp *string `json:"udp,omitempty"`
}

// Configuration info for stream socket netdev
//
// Only SocketAddress types 'unix', 'inet' and 'fd' are supported.
//
// Since: 7.2
type NetdevStreamOptions struct {
	// socket address to listen on (server=true) or connect to
	// (server=false)
	Addr SocketAddress `json:"addr"`
	// create server socket (default: false)
	Server *bool `json:"server,omitempty"`
	// For a client socket, if a socket is disconnected, then attempt
	// a reconnect after the given number of seconds. Setting this to
	// zero disables this function. (default: 0) (since 8.0)
	Reconnect *int64 `json:"reconnect,omitempty"`
	// For a client socket, if a socket is disconnected, then attempt
	// a reconnect after the given number of milliseconds. Setting
	// this to zero disables this function. This member is mutually
	// exclusive with @reconnect. (default: 0) (Since: 9.2)
	ReconnectMs *int64 `json:"reconnect-ms,omitempty"`
}

// Used to configure a host TAP network interface backend.
//
// Since: 1.2
type NetdevTapOptions struct {
	// interface name
	Ifname *string `json:"ifname,omitempty"`
	// file descriptor of an already opened tap
	Fd *string `json:"fd,omitempty"`
	// multiple file descriptors of already opened multiqueue capable
	// tap
	Fds *string `json:"fds,omitempty"`
	// script to initialize the interface
	Script *string `json:"script,omitempty"`
	// script to shut down the interface
	Downscript *string `json:"downscript,omitempty"`
	// bridge name (since 2.8)
	Br *string `json:"br,omitempty"`
	// command to execute to configure bridge
	Helper *string `json:"helper,omitempty"`
	// send buffer limit. Understands [TGMKkb] suffixes.
	Sndbuf *uint64 `json:"sndbuf,omitempty"`
	// enable the IFF_VNET_HDR flag on the tap interface
	VnetHdr *bool `json:"vnet_hdr,omitempty"`
	// enable vhost-net network accelerator
	Vhost *bool `json:"vhost,omitempty"`
	// file descriptor of an already opened vhost net device
	Vhostfd *string `json:"vhostfd,omitempty"`
	// file descriptors of multiple already opened vhost net devices
	Vhostfds *string `json:"vhostfds,omitempty"`
	// vhost on for non-MSIX virtio guests
	Vhostforce *bool `json:"vhostforce,omitempty"`
	// number of queues to be created for multiqueue capable tap
	Queues *uint32 `json:"queues,omitempty"`
	// maximum number of microseconds that could be spent on busy
	// polling for tap (since 2.7)
	PollUs *uint32 `json:"poll-us,omitempty"`
}

// Use the user mode network stack which requires no administrator
// privilege to run.
//
// Since: 1.2
type NetdevUserOptions struct {
	// client hostname reported by the builtin DHCP server
	Hostname *string `json:"hostname,omitempty"`
	// isolate the guest from the host
	Restrict *bool `json:"restrict,omitempty"`
	// whether to support IPv4, default true for enabled (since 2.6)
	Ipv4 *bool `json:"ipv4,omitempty"`
	// whether to support IPv6, default true for enabled (since 2.6)
	Ipv6 *bool `json:"ipv6,omitempty"`
	// legacy parameter, use net= instead
	Ip *string `json:"ip,omitempty"`
	// IP network address that the guest will see, in the form
	// addr[/netmask] The netmask is optional, and can be either in
	// the form a.b.c.d or as a number of valid top-most bits. Default
	// is 10.0.2.0/24.
	Net *string `json:"net,omitempty"`
	// guest-visible address of the host
	Host *string `json:"host,omitempty"`
	// root directory of the built-in TFTP server
	Tftp *string `json:"tftp,omitempty"`
	// BOOTP filename, for use with tftp=
	Bootfile *string `json:"bootfile,omitempty"`
	// the first of the 16 IPs the built-in DHCP server can assign
	Dhcpstart *string `json:"dhcpstart,omitempty"`
	// guest-visible address of the virtual nameserver
	Dns *string `json:"dns,omitempty"`
	// list of DNS suffixes to search, passed as DHCP option to the
	// guest
	Dnssearch []String `json:"dnssearch,omitempty"`
	// guest-visible domain name of the virtual nameserver (since 3.0)
	Domainname *string `json:"domainname,omitempty"`
	// IPv6 network prefix (default is fec0::) (since 2.6). The
	// network prefix is given in the usual hexadecimal IPv6 address
	// notation.
	Ipv6Prefix *string `json:"ipv6-prefix,omitempty"`
	// IPv6 network prefix length (default is 64) (since 2.6)
	Ipv6Prefixlen *int64 `json:"ipv6-prefixlen,omitempty"`
	// guest-visible IPv6 address of the host (since 2.6)
	Ipv6Host *string `json:"ipv6-host,omitempty"`
	// guest-visible IPv6 address of the virtual nameserver (since
	// 2.6)
	Ipv6Dns *string `json:"ipv6-dns,omitempty"`
	// root directory of the built-in SMB server
	Smb *string `json:"smb,omitempty"`
	// IP address of the built-in SMB server
	Smbserver *string `json:"smbserver,omitempty"`
	// redirect incoming TCP or UDP host connections to guest
	// endpoints
	Hostfwd []String `json:"hostfwd,omitempty"`
	// forward guest TCP connections
	Guestfwd []String `json:"guestfwd,omitempty"`
	// RFC2132 "TFTP server name" string (Since 3.1)
	TftpServerName *string `json:"tftp-server-name,omitempty"`
}

// Connect to a vde switch running on the host.
//
// Since: 1.2
type NetdevVdeOptions struct {
	// socket path
	Sock *string `json:"sock,omitempty"`
	// port number
	Port *uint16 `json:"port,omitempty"`
	// group owner of socket
	Group *string `json:"group,omitempty"`
	// permissions for socket
	Mode *uint16 `json:"mode,omitempty"`
}

// Vhost-user network backend
//
// Since: 2.1
type NetdevVhostUserOptions struct {
	// name of a unix socket chardev
	Chardev string `json:"chardev"`
	// vhost on for non-MSIX virtio guests (default: false).
	Vhostforce *bool `json:"vhostforce,omitempty"`
	// number of queues to be created for multiqueue vhost-user
	// (default: 1) (Since 2.5)
	Queues *int64 `json:"queues,omitempty"`
}

// Vhost-vdpa network backend  vDPA device is a device that uses a
// datapath which complies with the virtio specifications with a
// vendor specific control path.
//
// Since: 5.1
type NetdevVhostVDPAOptions struct {
	// path of vhost-vdpa device (default:'/dev/vhost-vdpa-0')
	Vhostdev *string `json:"vhostdev,omitempty"`
	// file descriptor of an already opened vhost vdpa device
	Vhostfd *string `json:"vhostfd,omitempty"`
	// number of queues to be created for multiqueue vhost-vdpa
	// (default: 1)
	Queues *int64 `json:"queues,omitempty"`
	// Start device with (experimental) shadow virtqueue. (Since 7.1)
	// (default: false)
	XSvq *bool `json:"x-svq,omitempty"`
}

// vmnet (bridged mode) network backend.  Bridges the vmnet interface
// with a physical network interface.
//
// Since: 7.1
type NetdevVmnetBridgedOptions struct {
	// The name of the physical interface to be bridged.
	Ifname string `json:"ifname"`
	// Enable isolation for this interface. Interface isolation
	// ensures that vmnet interface is not able to communicate with
	// any other vmnet interfaces. Only communication with host is
	// allowed. Requires at least macOS Big Sur 11.0.
	Isolated *bool `json:"isolated,omitempty"`
}

// vmnet (host mode) network backend.  Allows the vmnet interface to
// communicate with other vmnet interfaces that are in host mode and
// also with the host.
//
// Since: 7.1
type NetdevVmnetHostOptions struct {
	// The starting IPv4 address to use for the interface. Must be in
	// the private IP range (RFC 1918). Must be specified along with
	// @end-address and @subnet-mask. This address is used as the
	// gateway address. The subsequent address up to and including
	// end-address are placed in the DHCP pool.
	StartAddress *string `json:"start-address,omitempty"`
	// The DHCP IPv4 range end address to use for the interface. Must
	// be in the private IP range (RFC 1918). Must be specified along
	// with @start-address and @subnet-mask.
	EndAddress *string `json:"end-address,omitempty"`
	// The IPv4 subnet mask to use on the interface. Must be specified
	// along with @start-address and @subnet-mask.
	SubnetMask *string `json:"subnet-mask,omitempty"`
	// Enable isolation for this interface. Interface isolation
	// ensures that vmnet interface is not able to communicate with
	// any other vmnet interfaces. Only communication with host is
	// allowed. Requires at least macOS Big Sur 11.0.
	Isolated *bool `json:"isolated,omitempty"`
	// The identifier (UUID) to uniquely identify the isolated network
	// vmnet interface should be added to. If set, no DHCP service is
	// provided for this interface and network communication is
	// allowed only with other interfaces added to this network
	// identified by the UUID. Requires at least macOS Big Sur 11.0.
	NetUuid *string `json:"net-uuid,omitempty"`
}

// vmnet (shared mode) network backend.  Allows traffic originating
// from the vmnet interface to reach the Internet through a network
// address translator (NAT).  The vmnet interface can communicate with
// the host and with other shared mode interfaces on the same subnet.
// If no DHCP settings, subnet mask and IPv6 prefix specified, the
// interface can communicate with any of other interfaces in shared
// mode.
//
// Since: 7.1
type NetdevVmnetSharedOptions struct {
	// The starting IPv4 address to use for the interface. Must be in
	// the private IP range (RFC 1918). Must be specified along with
	// @end-address and @subnet-mask. This address is used as the
	// gateway address. The subsequent address up to and including
	// end-address are placed in the DHCP pool.
	StartAddress *string `json:"start-address,omitempty"`
	// The DHCP IPv4 range end address to use for the interface. Must
	// be in the private IP range (RFC 1918). Must be specified along
	// with @start-address and @subnet-mask.
	EndAddress *string `json:"end-address,omitempty"`
	// The IPv4 subnet mask to use on the interface. Must be specified
	// along with @start-address and @subnet-mask.
	SubnetMask *string `json:"subnet-mask,omitempty"`
	// Enable isolation for this interface. Interface isolation
	// ensures that vmnet interface is not able to communicate with
	// any other vmnet interfaces. Only communication with host is
	// allowed. Requires at least macOS Big Sur 11.0.
	Isolated *bool `json:"isolated,omitempty"`
	// The IPv6 prefix to use into guest network. Must be a unique
	// local address i.e. start with fd00::/8 and have length of 64.
	Nat66Prefix *string `json:"nat66-prefix,omitempty"`
}

// Properties for objects of classes derived from netfilter.
//
// Since: 2.5
type NetfilterProperties struct {
	// id of the network device backend to filter
	Netdev string `json:"netdev"`
	// indicates which queue(s) to filter (default: all)
	Queue *NetFilterDirection `json:"queue,omitempty"`
	// indicates whether the filter is enabled ("on") or disabled
	// ("off") (default: "on")
	Status *string `json:"status,omitempty"`
	// specifies where the filter should be inserted in the filter
	// list. "head" means the filter is inserted at the head of the
	// filter list, before any existing filters. "tail" means the
	// filter is inserted at the tail of the filter list, behind any
	// existing filters (default). "id=<id>" means the filter is
	// inserted before or behind the filter specified by <id>,
	// depending on the @insert property. (default: "tail")
	Position *string `json:"position,omitempty"`
	// where to insert the filter relative to the filter given in
	// @position. Ignored if @position is "head" or "tail". (default:
	// behind)
	Insert *NetfilterInsert `json:"insert,omitempty"`
}

// Option "-numa cpu" overrides default cpu to node mapping.  It
// accepts the same set of cpu properties as returned by query-
// hotpluggable-cpus[].props, where node-id could be used to override
// default node mapping.
//
// Since: 2.10
type NumaCpuOptions struct {
	// NUMA node ID the CPU belongs to
	NodeId *int64 `json:"node-id,omitempty"`
	// drawer number within CPU topology the CPU belongs to (since
	// 8.2)
	DrawerId *int64 `json:"drawer-id,omitempty"`
	// book number within parent container the CPU belongs to (since
	// 8.2)
	BookId *int64 `json:"book-id,omitempty"`
	// socket number within parent container the CPU belongs to
	SocketId *int64 `json:"socket-id,omitempty"`
	// die number within the parent container the CPU belongs to
	// (since 4.1)
	DieId *int64 `json:"die-id,omitempty"`
	// cluster number within the parent container the CPU belongs to
	// (since 7.1)
	ClusterId *int64 `json:"cluster-id,omitempty"`
	// module number within the parent container the CPU belongs to
	// (since 9.1)
	ModuleId *int64 `json:"module-id,omitempty"`
	// core number within the parent container the CPU belongs to
	CoreId *int64 `json:"core-id,omitempty"`
	// thread number within the core the CPU belongs to
	ThreadId *int64 `json:"thread-id,omitempty"`
}

// Set the distance between 2 NUMA nodes.
//
// Since: 2.10
type NumaDistOptions struct {
	// source NUMA node.
	Src uint16 `json:"src"`
	// destination NUMA node.
	Dst uint16 `json:"dst"`
	// NUMA distance from source node to destination node. When a node
	// is unreachable from another node, set the distance between them
	// to 255.
	Val uint8 `json:"val"`
}

// Set the memory side cache information for a given memory domain.
// For more information of @NumaHmatCacheOptions, see chapter
// 5.2.27.5: Table 5-147: Field "Cache Attributes" of ACPI 6.3 spec.
//
// Since: 5.0
type NumaHmatCacheOptions struct {
	// the memory proximity domain to which the memory belongs.
	NodeId uint32 `json:"node-id"`
	// the size of memory side cache in bytes.
	Size uint64 `json:"size"`
	// the cache level described in this structure.
	Level uint8 `json:"level"`
	// the cache associativity, none/direct-mapped/complex(complex
	// cache indexing).
	Associativity HmatCacheAssociativity `json:"associativity"`
	// the write policy, none/write-back/write-through.
	Policy HmatCacheWritePolicy `json:"policy"`
	// the cache Line size in bytes.
	Line uint16 `json:"line"`
}

// Set the system locality latency and bandwidth information between
// Initiator and Target proximity Domains.  For more information about
// @NumaHmatLBOptions, see chapter 5.2.27.4: Table 5-146 of ACPI 6.3
// spec.
//
// Since: 5.0
type NumaHmatLBOptions struct {
	// the Initiator Proximity Domain.
	Initiator uint16 `json:"initiator"`
	// the Target Proximity Domain.
	Target uint16 `json:"target"`
	// the Memory Hierarchy. Indicates the performance of memory or
	// side cache.
	Hierarchy HmatLBMemoryHierarchy `json:"hierarchy"`
	// presents the type of data, access/read/write latency or hit
	// latency.
	DataType HmatLBDataType `json:"data-type"`
	// the value of latency from @initiator to @target proximity
	// domain, the latency unit is "ns(nanosecond)".
	Latency *uint64 `json:"latency,omitempty"`
	// the value of bandwidth between @initiator and @target proximity
	// domain, the bandwidth unit is "Bytes per second".
	Bandwidth *uint64 `json:"bandwidth,omitempty"`
}

// Create a guest NUMA node.  (for OptsVisitor)
//
// Since: 2.1
type NumaNodeOptions struct {
	// NUMA node ID (increase by 1 from 0 if omitted)
	Nodeid *uint16 `json:"nodeid,omitempty"`
	// VCPUs belonging to this node (assign VCPUS round-robin if
	// omitted)
	Cpus []uint16 `json:"cpus,omitempty"`
	// memory size of this node; mutually exclusive with @memdev.
	// Equally divide total memory among nodes if both @mem and
	// @memdev are omitted.
	Mem *uint64 `json:"mem,omitempty"`
	// memory backend object. If specified for one node, it must be
	// specified for all nodes.
	Memdev *string `json:"memdev,omitempty"`
	// defined in ACPI 6.3 Chapter 5.2.27.3 Table 5-145, points to the
	// nodeid which has the memory controller responsible for this
	// NUMA node. This field provides additional information as to the
	// initiator node that is closest (as in directly attached) to
	// this node, and therefore has the best performance (since 5.0)
	Initiator *uint16 `json:"initiator,omitempty"`
}

// Since: 1.2
type ObjectPropertyInfo struct {
	// the name of the property
	Name string `json:"name"`
	// the type of the property. This will typically come in one of
	// four forms: 1) A primitive type such as 'u8', 'u16', 'bool',
	// 'str', or 'double'. These types are mapped to the appropriate
	// JSON type. 2) A child type in the form 'child<subtype>' where
	// subtype is a qdev device type name. Child properties create the
	// composition tree. 3) A link type in the form 'link<subtype>'
	// where subtype is a qdev device type name. Link properties form
	// the device model graph.
	Type string `json:"type"`
	// if specified, the description of the property.
	Description *string `json:"description,omitempty"`
	// the default value, if any (since 5.0)
	DefaultValue *any `json:"default-value,omitempty"`
}

// This structure describes a search result from @qom-list-types
//
// Since: 1.1
type ObjectTypeInfo struct {
	// the type name found in the search
	Name string `json:"name"`
	// the type is abstract and can't be directly instantiated.
	// Omitted if false. (since 2.10)
	Abstract *bool `json:"abstract,omitempty"`
	// Name of parent type, if any (since 2.10)
	Parent *string `json:"parent,omitempty"`
}

// PCDIMMDevice state information
//
// Since: 2.1
type PCDIMMDeviceInfo struct {
	// device's ID
	Id *string `json:"id,omitempty"`
	// physical address, where device is mapped
	Addr int64 `json:"addr"`
	// size of memory that the device provides
	Size int64 `json:"size"`
	// slot number at which device is plugged in
	Slot int64 `json:"slot"`
	// NUMA node number where device is plugged in
	Node int64 `json:"node"`
	// memory backend linked with device
	Memdev string `json:"memdev"`
	// true if device was hotplugged
	Hotplugged bool `json:"hotplugged"`
	// true if device if could be added/removed while machine is
	// running
	Hotpluggable bool `json:"hotpluggable"`
}

// Since: 2.1
type PCDIMMDeviceInfoWrapper struct {
	// PCDIMMDevice state information
	Data PCDIMMDeviceInfo `json:"data"`
}

// Information about a persistent reservation manager
//
// Since: 3.0
type PRManagerInfo struct {
	// the identifier of the persistent reservation manager
	Id string `json:"id"`
	// true if the persistent reservation manager is connected to the
	// underlying storage or helper
	Connected bool `json:"connected"`
}

// Information about a PCI Bridge device
//
// Since: 0.14
type PciBridgeInfo struct {
	// information about the bus the device resides on
	Bus PciBusInfo `json:"bus"`
	// a list of @PciDeviceInfo for each device on this bridge
	Devices []PciDeviceInfo `json:"devices,omitempty"`
}

// Information about a bus of a PCI Bridge device
//
// Since: 2.4
type PciBusInfo struct {
	// primary bus interface number. This should be the number of the
	// bus the device resides on.
	Number int64 `json:"number"`
	// secondary bus interface number. This is the number of the main
	// bus for the bridge
	Secondary int64 `json:"secondary"`
	// This is the highest number bus that resides below the bridge.
	Subordinate int64 `json:"subordinate"`
	// The PIO range for all devices on this bridge
	IoRange PciMemoryRange `json:"io_range"`
	// The MMIO range for all devices on this bridge
	MemoryRange PciMemoryRange `json:"memory_range"`
	// The range of prefetchable MMIO for all devices on this bridge
	PrefetchableRange PciMemoryRange `json:"prefetchable_range"`
}

// Information about the Class of a PCI device
//
// Since: 2.4
type PciDeviceClass struct {
	// a string description of the device's class (not stable, and
	// should only be treated as informational)
	Desc *string `json:"desc,omitempty"`
	// the class code of the device
	Class int64 `json:"class"`
}

// Information about the Id of a PCI device
//
// Since: 2.4
type PciDeviceId struct {
	// the PCI device id
	Device int64 `json:"device"`
	// the PCI vendor id
	Vendor int64 `json:"vendor"`
	// the PCI subsystem id (since 3.1)
	Subsystem *int64 `json:"subsystem,omitempty"`
	// the PCI subsystem vendor id (since 3.1)
	SubsystemVendor *int64 `json:"subsystem-vendor,omitempty"`
}

// Information about a PCI device
//
// Since: 0.14
type PciDeviceInfo struct {
	// the bus number of the device
	Bus int64 `json:"bus"`
	// the slot the device is located in
	Slot int64 `json:"slot"`
	// the function of the slot used by the device
	Function int64 `json:"function"`
	// the class of the device
	ClassInfo PciDeviceClass `json:"class_info"`
	// the PCI device id
	Id PciDeviceId `json:"id"`
	// if an IRQ is assigned to the device, the IRQ number
	Irq *int64 `json:"irq,omitempty"`
	// the IRQ pin, zero means no IRQ (since 5.1)
	IrqPin int64 `json:"irq_pin"`
	// the device name of the PCI device
	QdevId string `json:"qdev_id"`
	// if the device is a PCI bridge, the bridge information
	PciBridge *PciBridgeInfo `json:"pci_bridge,omitempty"`
	// a list of the PCI I/O regions associated with the device
	Regions []PciMemoryRegion `json:"regions"`
}

// Information about a PCI bus
//
// Since: 0.14
type PciInfo struct {
	// the bus index
	Bus int64 `json:"bus"`
	// a list of devices on this bus
	Devices []PciDeviceInfo `json:"devices"`
}

// A PCI device memory region
//
// Since: 0.14
type PciMemoryRange struct {
	// the starting address (guest physical)
	Base int64 `json:"base"`
	// the ending address (guest physical)
	Limit int64 `json:"limit"`
}

// Information about a PCI device I/O region.
//
// Since: 0.14
type PciMemoryRegion struct {
	// the index of the Base Address Register for this region
	Bar int64 `json:"bar"`
	// - 'io' if the region is a PIO region - 'memory' if the region
	// is a MMIO region
	Type string `json:"type"`
	// memory address
	Address int64 `json:"address"`
	// memory size
	Size int64 `json:"size"`
	// if @type is 'memory', true if the memory is prefetchable
	Prefetch *bool `json:"prefetch,omitempty"`
	// if @type is 'memory', true if the BAR is 64-bit
	MemType64 *bool `json:"mem_type_64,omitempty"`
}

// Properties for pr-manager-helper objects.
//
// Since: 2.11
type PrManagerHelperProperties struct {
	// the path to a Unix domain socket for connecting to the external
	// helper
	Path string `json:"path"`
}

// A single authorization rule.
//
// Since: 4.0
type QAuthZListRule struct {
	// a string or glob to match against a user identity
	Match string `json:"match"`
	// the result to return if @match evaluates to true
	Policy QAuthZListPolicy `json:"policy"`
	// the format of the @match rule (default 'exact')
	Format *QAuthZListFormat `json:"format,omitempty"`
}

// Specific parameters for RSA algorithm.
//
// Since: 7.1
type QCryptoAkCipherOptionsRSA struct {
	// QCryptoHashAlgo
	HashAlg QCryptoHashAlgo `json:"hash-alg"`
	// QCryptoRSAPaddingAlgo
	PaddingAlg QCryptoRSAPaddingAlgo `json:"padding-alg"`
}

// This struct defines the update parameters that activate/de-activate
// set of keyslots
//
// Since: 5.1
type QCryptoBlockAmendOptionsLUKS struct {
	// the desired state of the keyslots
	State QCryptoBlockLUKSKeyslotState `json:"state"`
	// The ID of a QCryptoSecret object providing the password to be
	// written into added active keyslots
	NewSecret *string `json:"new-secret,omitempty"`
	// Optional (for deactivation only) If given will deactivate all
	// keyslots that match password located in QCryptoSecret with this
	// ID
	OldSecret *string `json:"old-secret,omitempty"`
	// Optional. ID of the keyslot to activate/deactivate. For keyslot
	// activation, keyslot should not be active already (this is
	// unsafe to update an active keyslot), but possible if 'force'
	// parameter is given. If keyslot is not given, first free keyslot
	// will be written. For keyslot deactivation, this parameter
	// specifies the exact keyslot to deactivate
	Keyslot *int64 `json:"keyslot,omitempty"`
	// Optional (for activation only) Number of milliseconds to spend
	// in PBKDF passphrase processing for the newly activated keyslot.
	// Currently defaults to 2000.
	IterTime *int64 `json:"iter-time,omitempty"`
	// Optional. The ID of a QCryptoSecret object providing the
	// password to use to retrieve current master key. Defaults to the
	// same secret that was used to open the image
	Secret *string `json:"secret,omitempty"`
}

// The options that apply to LUKS encryption format initialization
//
// Since: 2.6
type QCryptoBlockCreateOptionsLUKS struct {
	// the ID of a QCryptoSecret object providing the decryption key.
	// Mandatory except when probing image for metadata only.
	KeySecret *string `json:"key-secret,omitempty"`
	// the cipher algorithm for data encryption Currently defaults to
	// 'aes-256'.
	CipherAlg *QCryptoCipherAlgo `json:"cipher-alg,omitempty"`
	// the cipher mode for data encryption Currently defaults to 'xts'
	CipherMode *QCryptoCipherMode `json:"cipher-mode,omitempty"`
	// the initialization vector generator Currently defaults to
	// 'plain64'
	IvgenAlg *QCryptoIVGenAlgo `json:"ivgen-alg,omitempty"`
	// the initialization vector generator hash Currently defaults to
	// 'sha256'
	IvgenHashAlg *QCryptoHashAlgo `json:"ivgen-hash-alg,omitempty"`
	// the master key hash algorithm Currently defaults to 'sha256'
	HashAlg *QCryptoHashAlgo `json:"hash-alg,omitempty"`
	// number of milliseconds to spend in PBKDF passphrase processing.
	// Currently defaults to 2000. (since 2.8)
	IterTime *int64 `json:"iter-time,omitempty"`
}

// The common information that applies to all full disk encryption
// formats
//
// Since: 2.7
type QCryptoBlockInfoBase struct {
	// the encryption format
	Format QCryptoBlockFormat `json:"format"`
}

// Information about the LUKS block encryption options
//
// Since: 2.7
type QCryptoBlockInfoLUKS struct {
	// the cipher algorithm for data encryption
	CipherAlg QCryptoCipherAlgo `json:"cipher-alg"`
	// the cipher mode for data encryption
	CipherMode QCryptoCipherMode `json:"cipher-mode"`
	// the initialization vector generator
	IvgenAlg QCryptoIVGenAlgo `json:"ivgen-alg"`
	// the initialization vector generator hash
	IvgenHashAlg *QCryptoHashAlgo `json:"ivgen-hash-alg,omitempty"`
	// the master key hash algorithm
	HashAlg QCryptoHashAlgo `json:"hash-alg"`
	// whether the LUKS header is detached (Since 9.0)
	DetachedHeader bool `json:"detached-header"`
	// offset to the payload data in bytes
	PayloadOffset int64 `json:"payload-offset"`
	// number of PBKDF2 iterations for key material
	MasterKeyIters int64 `json:"master-key-iters"`
	// unique identifier for the volume
	Uuid string `json:"uuid"`
	// information about each key slot
	Slots []QCryptoBlockInfoLUKSSlot `json:"slots"`
}

// Information about the LUKS block encryption key slot options
//
// Since: 2.7
type QCryptoBlockInfoLUKSSlot struct {
	// whether the key slot is currently in use
	Active bool `json:"active"`
	// number of PBKDF2 iterations for key material
	Iters *int64 `json:"iters,omitempty"`
	// number of stripes for splitting key material
	Stripes *int64 `json:"stripes,omitempty"`
	// offset to the key material in bytes
	KeyOffset int64 `json:"key-offset"`
}

// The common options that apply to all full disk encryption formats
//
// Since: 2.6
type QCryptoBlockOptionsBase struct {
	// the encryption format
	Format QCryptoBlockFormat `json:"format"`
}

// The options that apply to LUKS encryption format
//
// Since: 2.6
type QCryptoBlockOptionsLUKS struct {
	// the ID of a QCryptoSecret object providing the decryption key.
	// Mandatory except when probing image for metadata only.
	KeySecret *string `json:"key-secret,omitempty"`
}

// The options that apply to QCow/QCow2 AES-CBC encryption format
//
// Since: 2.6
type QCryptoBlockOptionsQCow struct {
	// the ID of a QCryptoSecret object providing the decryption key.
	// Mandatory except when probing image for metadata only.
	KeySecret *string `json:"key-secret,omitempty"`
}

// Information about a queue of crypto device.
//
// Since: 8.0
type QCryptodevBackendClient struct {
	// the queue index of the crypto device
	Queue uint32 `json:"queue"`
	// the type of the crypto device
	Type QCryptodevBackendType `json:"type"`
}

// Information about a crypto device.
//
// Since: 8.0
type QCryptodevInfo struct {
	// the id of the crypto device
	Id string `json:"id"`
	// supported service types of a crypto device
	Service []QCryptodevBackendServiceType `json:"service"`
	// the additional information of the crypto device
	Client []QCryptodevBackendClient `json:"client"`
}

// Since: 1.3
type QKeyCodeWrapper struct {
	// An enumeration of key name
	Data QKeyCode `json:"data"`
}

// Qcow2 bitmap information.
//
// Since: 4.0
type Qcow2BitmapInfo struct {
	// the name of the bitmap
	Name string `json:"name"`
	// granularity of the bitmap in bytes
	Granularity uint32 `json:"granularity"`
	// flags of the bitmap
	Flags []Qcow2BitmapInfoFlags `json:"flags"`
}

// Structure of flags for each metadata structure.  Setting a field to
// 'true' makes QEMU guard that Qcow2 format structure against
// unintended overwriting.  See Qcow2 format specification for
// detailed information on these structures.  The default value is
// chosen according to the template given.
//
// Since: 2.9
type Qcow2OverlapCheckFlags struct {
	// Specifies a template mode which can be adjusted using the other
	// flags, defaults to 'cached'
	Template *Qcow2OverlapCheckMode `json:"template,omitempty"`
	// Qcow2 format header
	MainHeader *bool `json:"main-header,omitempty"`
	// Qcow2 active L1 table
	ActiveL1 *bool `json:"active-l1,omitempty"`
	// Qcow2 active L2 table
	ActiveL2 *bool `json:"active-l2,omitempty"`
	// Qcow2 refcount table
	RefcountTable *bool `json:"refcount-table,omitempty"`
	// Qcow2 refcount blocks
	RefcountBlock *bool `json:"refcount-block,omitempty"`
	// Qcow2 snapshot table
	SnapshotTable *bool `json:"snapshot-table,omitempty"`
	// Qcow2 inactive L1 tables
	InactiveL1 *bool `json:"inactive-l1,omitempty"`
	// Qcow2 inactive L2 tables
	InactiveL2 *bool `json:"inactive-l2,omitempty"`
	// Qcow2 bitmap directory (since 3.0)
	BitmapDirectory *bool `json:"bitmap-directory,omitempty"`
}

// Properties for qtest objects.
//
// Since: 6.0
type QtestProperties struct {
	// the chardev to be used to receive qtest commands on.
	Chardev string `json:"chardev"`
	// the path to a log file
	Log *string `json:"log,omitempty"`
}

// Since: 6.1
type RbdEncryptionCreateOptionsLUKS struct {
	// ID of a QCryptoSecret object providing a passphrase for
	// unlocking the encryption
	KeySecret string `json:"key-secret"`
	// The encryption algorithm
	CipherAlg *QCryptoCipherAlgo `json:"cipher-alg,omitempty"`
}

// Since: 6.1
type RbdEncryptionCreateOptionsLUKS2 struct {
	// ID of a QCryptoSecret object providing a passphrase for
	// unlocking the encryption
	KeySecret string `json:"key-secret"`
	// The encryption algorithm
	CipherAlg *QCryptoCipherAlgo `json:"cipher-alg,omitempty"`
}

// Since: 6.1
type RbdEncryptionCreateOptionsLUKSBase struct {
	// ID of a QCryptoSecret object providing a passphrase for
	// unlocking the encryption
	KeySecret string `json:"key-secret"`
	// The encryption algorithm
	CipherAlg *QCryptoCipherAlgo `json:"cipher-alg,omitempty"`
}

// Since: 6.1
type RbdEncryptionOptionsLUKS struct {
	// ID of a QCryptoSecret object providing a passphrase for
	// unlocking the encryption
	KeySecret string `json:"key-secret"`
}

// Since: 6.1
type RbdEncryptionOptionsLUKS2 struct {
	// ID of a QCryptoSecret object providing a passphrase for
	// unlocking the encryption
	KeySecret string `json:"key-secret"`
}

// Since: 8.0
type RbdEncryptionOptionsLUKSAny struct {
	// ID of a QCryptoSecret object providing a passphrase for
	// unlocking the encryption
	KeySecret string `json:"key-secret"`
}

// Since: 6.1
type RbdEncryptionOptionsLUKSBase struct {
	// ID of a QCryptoSecret object providing a passphrase for
	// unlocking the encryption
	KeySecret string `json:"key-secret"`
}

// Properties for x-remote-object objects.
//
// Since: 6.0
type RemoteObjectProperties struct {
	// file descriptor name previously passed via 'getfd' command
	Fd string `json:"fd"`
	// the id of the device to be associated with the file descriptor
	Devid string `json:"devid"`
}

// Record/replay information.
//
// Since: 5.2
type ReplayInfo struct {
	// current mode.
	Mode ReplayMode `json:"mode"`
	// name of the record/replay log file. It is present only in
	// record or replay modes, when the log is recorded or replayed.
	Filename *string `json:"filename,omitempty"`
	// current number of executed instructions.
	Icount int64 `json:"icount"`
}

// The result format for 'query-xen-replication-status'.
//
// Since: 2.9
type ReplicationStatus struct {
	// true if an error happened, false if replication is normal.
	Error bool `json:"error"`
	// the human readable error description string, when @error is
	// 'true'.
	Desc *string `json:"desc,omitempty"`
}

// Properties for rng-egd objects.
//
// Since: 1.3
type RngEgdProperties struct {
	// if true, the device is opened immediately when applying this
	// option and will probably fail when processing the next option.
	// Don't use; only provided for compatibility. (default: false)
	Opened *bool `json:"opened,omitempty"`
	// the name of a character device backend that provides the
	// connection to the RNG daemon
	Chardev string `json:"chardev"`
}

// Properties for objects of classes derived from rng.
//
// Since: 1.3
type RngProperties struct {
	// if true, the device is opened immediately when applying this
	// option and will probably fail when processing the next option.
	// Don't use; only provided for compatibility. (default: false)
	Opened *bool `json:"opened,omitempty"`
}

// Properties for rng-random objects.
//
// Since: 1.3
type RngRandomProperties struct {
	// if true, the device is opened immediately when applying this
	// option and will probably fail when processing the next option.
	// Don't use; only provided for compatibility. (default: false)
	Opened *bool `json:"opened,omitempty"`
	// the filename of the device on the host to obtain entropy from
	// (default: "/dev/urandom")
	Filename *string `json:"filename,omitempty"`
}

// Rocker switch OF-DPA flow
//
// Since: 2.4
type RockerOfDpaFlow struct {
	// flow unique cookie ID
	Cookie uint64 `json:"cookie"`
	// count of matches (hits) on flow
	Hits uint64 `json:"hits"`
	// flow key
	Key RockerOfDpaFlowKey `json:"key"`
	// flow mask
	Mask RockerOfDpaFlowMask `json:"mask"`
	// flow action
	Action RockerOfDpaFlowAction `json:"action"`
}

// Rocker switch OF-DPA flow action
//
// .. note:: Optional members may or may not appear in the flow action
// depending if they're relevant to the flow action.
//
// Since: 2.4
type RockerOfDpaFlowAction struct {
	// next table ID
	GotoTbl *uint32 `json:"goto-tbl,omitempty"`
	// group ID
	GroupId *uint32 `json:"group-id,omitempty"`
	// tunnel logical port ID
	TunnelLport *uint32 `json:"tunnel-lport,omitempty"`
	// VLAN ID
	VlanId *uint16 `json:"vlan-id,omitempty"`
	// new VLAN ID
	NewVlanId *uint16 `json:"new-vlan-id,omitempty"`
	// physical output port
	OutPport *uint32 `json:"out-pport,omitempty"`
}

// Rocker switch OF-DPA flow key
//
// .. note:: Optional members may or may not appear in the flow key
// depending if they're relevant to the flow key.
//
// Since: 2.4
type RockerOfDpaFlowKey struct {
	// key priority, 0 being lowest priority
	Priority uint32 `json:"priority"`
	// flow table ID
	TblId uint32 `json:"tbl-id"`
	// physical input port
	InPport *uint32 `json:"in-pport,omitempty"`
	// tunnel ID
	TunnelId *uint32 `json:"tunnel-id,omitempty"`
	// VLAN ID
	VlanId *uint16 `json:"vlan-id,omitempty"`
	// Ethernet header type
	EthType *uint16 `json:"eth-type,omitempty"`
	// Ethernet header source MAC address
	EthSrc *string `json:"eth-src,omitempty"`
	// Ethernet header destination MAC address
	EthDst *string `json:"eth-dst,omitempty"`
	// IP Header protocol field
	IpProto *uint8 `json:"ip-proto,omitempty"`
	// IP header TOS field
	IpTos *uint8 `json:"ip-tos,omitempty"`
	// IP header destination address
	IpDst *string `json:"ip-dst,omitempty"`
}

// Rocker switch OF-DPA flow mask
//
// .. note:: Optional members may or may not appear in the flow mask
// depending if they're relevant to the flow mask.
//
// Since: 2.4
type RockerOfDpaFlowMask struct {
	// physical input port
	InPport *uint32 `json:"in-pport,omitempty"`
	// tunnel ID
	TunnelId *uint32 `json:"tunnel-id,omitempty"`
	// VLAN ID
	VlanId *uint16 `json:"vlan-id,omitempty"`
	// Ethernet header source MAC address
	EthSrc *string `json:"eth-src,omitempty"`
	// Ethernet header destination MAC address
	EthDst *string `json:"eth-dst,omitempty"`
	// IP Header protocol field
	IpProto *uint8 `json:"ip-proto,omitempty"`
	// IP header TOS field
	IpTos *uint8 `json:"ip-tos,omitempty"`
}

// Rocker switch OF-DPA group
//
// .. note:: Optional members may or may not appear in the group
// depending if they're relevant to the group type.
//
// Since: 2.4
type RockerOfDpaGroup struct {
	// group unique ID
	Id uint32 `json:"id"`
	// group type
	Type uint8 `json:"type"`
	// VLAN ID
	VlanId *uint16 `json:"vlan-id,omitempty"`
	// physical port number
	Pport *uint32 `json:"pport,omitempty"`
	// group index, unique with group type
	Index *uint32 `json:"index,omitempty"`
	// output physical port number
	OutPport *uint32 `json:"out-pport,omitempty"`
	// next group ID
	GroupId *uint32 `json:"group-id,omitempty"`
	// VLAN ID to set
	SetVlanId *uint16 `json:"set-vlan-id,omitempty"`
	// pop VLAN headr from packet
	PopVlan *uint8 `json:"pop-vlan,omitempty"`
	// list of next group IDs
	GroupIds []uint32 `json:"group-ids,omitempty"`
	// set source MAC address in Ethernet header
	SetEthSrc *string `json:"set-eth-src,omitempty"`
	// set destination MAC address in Ethernet header
	SetEthDst *string `json:"set-eth-dst,omitempty"`
	// perform TTL check
	TtlCheck *uint8 `json:"ttl-check,omitempty"`
}

// Rocker switch port information.
//
// Since: 2.4
type RockerPort struct {
	// port name
	Name string `json:"name"`
	// port is enabled for I/O
	Enabled bool `json:"enabled"`
	// physical link is UP on port
	LinkUp bool `json:"link-up"`
	// port link speed in Mbps
	Speed uint32 `json:"speed"`
	// port link duplex
	Duplex RockerPortDuplex `json:"duplex"`
	// port link autoneg
	Autoneg RockerPortAutoneg `json:"autoneg"`
}

// Rocker switch information.
//
// Since: 2.4
type RockerSwitch struct {
	// switch name
	Name string `json:"name"`
	// switch ID
	Id uint64 `json:"id"`
	// number of front-panel ports
	Ports uint32 `json:"ports"`
}

// Rx-filter information for a NIC.
//
// Since: 1.6
type RxFilterInfo struct {
	// net client name
	Name string `json:"name"`
	// whether promiscuous mode is enabled
	Promiscuous bool `json:"promiscuous"`
	// multicast receive state
	Multicast RxState `json:"multicast"`
	// unicast receive state
	Unicast RxState `json:"unicast"`
	// vlan receive state (Since 2.0)
	Vlan RxState `json:"vlan"`
	// whether to receive broadcast
	BroadcastAllowed bool `json:"broadcast-allowed"`
	// multicast table is overflowed or not
	MulticastOverflow bool `json:"multicast-overflow"`
	// unicast table is overflowed or not
	UnicastOverflow bool `json:"unicast-overflow"`
	// the main macaddr string
	MainMac string `json:"main-mac"`
	// a list of active vlan id
	VlanTable []int64 `json:"vlan-table"`
	// a list of unicast macaddr string
	UnicastTable []string `json:"unicast-table"`
	// a list of multicast macaddr string
	MulticastTable []string `json:"multicast-table"`
}

// Information about intel SGX EPC section info
//
// Since: 7.0
type SGXEPCSection struct {
	// the numa node
	Node int64 `json:"node"`
	// the size of EPC section
	Size uint64 `json:"size"`
}

// Information about intel Safe Guard eXtension (SGX) support
//
// Since: 6.2
type SGXInfo struct {
	// true if SGX is supported
	Sgx bool `json:"sgx"`
	// true if SGX1 is supported
	Sgx1 bool `json:"sgx1"`
	// true if SGX2 is supported
	Sgx2 bool `json:"sgx2"`
	// true if FLC is supported
	Flc bool `json:"flc"`
	// The EPC sections info for guest (Since: 7.0)
	Sections []SGXEPCSection `json:"sections"`
}

// Schema for CPU topology configuration.  A missing value lets QEMU
// figure out a suitable value based on the ones that are provided.
// The members other than @cpus and @maxcpus define a topology of
// containers.  The ordering from highest/coarsest to lowest/finest
// is: @drawers, @books, @sockets, @dies, @clusters, @cores, @threads.
// Different architectures support different subsets of topology
// containers.  For example, s390x does not have clusters and dies,
// and the socket is the parent container of cores.
//
// Since: 6.1
type SMPConfiguration struct {
	// number of virtual CPUs in the virtual machine
	Cpus *int64 `json:"cpus,omitempty"`
	// number of drawers in the CPU topology (since 8.2)
	Drawers *int64 `json:"drawers,omitempty"`
	// number of books in the CPU topology (since 8.2)
	Books *int64 `json:"books,omitempty"`
	// number of sockets per parent container
	Sockets *int64 `json:"sockets,omitempty"`
	// number of dies per parent container
	Dies *int64 `json:"dies,omitempty"`
	// number of clusters per parent container (since 7.0)
	Clusters *int64 `json:"clusters,omitempty"`
	// number of modules per parent container (since 9.1)
	Modules *int64 `json:"modules,omitempty"`
	// number of cores per parent container
	Cores *int64 `json:"cores,omitempty"`
	// number of threads per core
	Threads *int64 `json:"threads,omitempty"`
	// maximum number of hotpluggable virtual CPUs in the virtual
	// machine
	Maxcpus *int64 `json:"maxcpus,omitempty"`
}

// Additional SchemaInfo members for meta-type 'alternate'.
//
// On the wire, this can be any of the members.
//
// Since: 2.5
type SchemaInfoAlternate struct {
	// the alternate type's members, in no particular order. The
	// members' wire encoding is distinct, see :doc:`/devel/qapi-code-
	// gen` section Alternate types.
	Members []SchemaInfoAlternateMember `json:"members"`
}

// An alternate member.
//
// Since: 2.5
type SchemaInfoAlternateMember struct {
	// the name of the member's type.
	Type string `json:"type"`
}

// Additional SchemaInfo members for meta-type 'array'.
//
// Values of this type are JSON array on the wire.
//
// Since: 2.5
type SchemaInfoArray struct {
	// the array type's element type.
	ElementType string `json:"element-type"`
}

// Additional SchemaInfo members for meta-type 'builtin'.
//
// Since: 2.5
type SchemaInfoBuiltin struct {
	// the JSON type used for this type on the wire.
	JsonType JSONType `json:"json-type"`
}

// Additional SchemaInfo members for meta-type 'command'.
//
// Since: 2.5
type SchemaInfoCommand struct {
	// the name of the object type that provides the command's
	// parameters.
	ArgType string `json:"arg-type"`
	// the name of the command's result type.
	RetType string `json:"ret-type"`
	// whether the command allows out-of-band execution, defaults to
	// false (Since: 2.12)
	AllowOob *bool `json:"allow-oob,omitempty"`
}

// Additional SchemaInfo members for meta-type 'enum'.
//
// Values of this type are JSON string on the wire.
//
// Since: 2.5
type SchemaInfoEnum struct {
	// the enum type's members, in no particular order (since 6.2).
	Members []SchemaInfoEnumMember `json:"members"`
	// the enumeration type's member names, in no particular order.
	// Redundant with @members. Just for backward compatibility.
	Values []string `json:"values"`
}

// An object member.
//
// Since: 6.2
type SchemaInfoEnumMember struct {
	// the member's name, as defined in the QAPI schema.
	Name string `json:"name"`
	// names of features associated with the member, in no particular
	// order.
	Features []string `json:"features,omitempty"`
}

// Additional SchemaInfo members for meta-type 'event'.
//
// Since: 2.5
type SchemaInfoEvent struct {
	// the name of the object type that provides the event's
	// parameters.
	ArgType string `json:"arg-type"`
}

// Additional SchemaInfo members for meta-type 'object'.
//
// Values of this type are JSON object on the wire.
//
// Since: 2.5
type SchemaInfoObject struct {
	// the object type's (non-variant) members, in no particular
	// order.
	Members []SchemaInfoObjectMember `json:"members"`
	// the name of the member serving as type tag. An element of
	// @members with this name must exist.
	Tag *string `json:"tag,omitempty"`
	// variant members, i.e. additional members that depend on the
	// type tag's value. Present exactly when @tag is present. The
	// variants are in no particular order, and may even differ from
	// the order of the values of the enum type of the @tag.
	Variants []SchemaInfoObjectVariant `json:"variants,omitempty"`
}

// An object member.
//
// Since: 2.5
type SchemaInfoObjectMember struct {
	// the member's name, as defined in the QAPI schema.
	Name string `json:"name"`
	// the name of the member's type.
	Type string `json:"type"`
	// default when used as command parameter. If absent, the
	// parameter is mandatory. If present, the value must be null. The
	// parameter is optional, and behavior when it's missing is not
	// specified here. Future extension: if present and non-null, the
	// parameter is optional, and defaults to this value.
	Default *any `json:"default,omitempty"`
	// names of features associated with the member, in no particular
	// order. (since 5.0)
	Features []string `json:"features,omitempty"`
}

// The variant members for a value of the type tag.
//
// Since: 2.5
type SchemaInfoObjectVariant struct {
	// a value of the type tag.
	Case string `json:"case"`
	// the name of the object type that provides the variant members
	// when the type tag has value @case.
	Type string `json:"type"`
}

// Properties for objects of classes derived from secret-common.
//
// Since: 2.6
type SecretCommonProperties struct {
	// the data format that the secret is provided in (default: raw)
	Format *QCryptoSecretFormat `json:"format,omitempty"`
	// the name of another secret that should be used to decrypt the
	// provided data. If not present, the data is assumed to be
	// unencrypted.
	Keyid *string `json:"keyid,omitempty"`
	// the random initialization vector used for encryption of this
	// particular secret. Should be a base64 encrypted string of the
	// 16-byte IV. Mandatory if @keyid is given. Ignored if @keyid is
	// absent.
	Iv *string `json:"iv,omitempty"`
}

// Properties for secret_keyring objects.
//
// Since: 5.1
type SecretKeyringProperties struct {
	// the data format that the secret is provided in (default: raw)
	Format *QCryptoSecretFormat `json:"format,omitempty"`
	// the name of another secret that should be used to decrypt the
	// provided data. If not present, the data is assumed to be
	// unencrypted.
	Keyid *string `json:"keyid,omitempty"`
	// the random initialization vector used for encryption of this
	// particular secret. Should be a base64 encrypted string of the
	// 16-byte IV. Mandatory if @keyid is given. Ignored if @keyid is
	// absent.
	Iv *string `json:"iv,omitempty"`
	// serial number that identifies a key to get from the kernel
	Serial int32 `json:"serial"`
}

// Properties for secret objects.  Either @data or @file must be
// provided, but not both.
//
// Since: 2.6
type SecretProperties struct {
	// the data format that the secret is provided in (default: raw)
	Format *QCryptoSecretFormat `json:"format,omitempty"`
	// the name of another secret that should be used to decrypt the
	// provided data. If not present, the data is assumed to be
	// unencrypted.
	Keyid *string `json:"keyid,omitempty"`
	// the random initialization vector used for encryption of this
	// particular secret. Should be a base64 encrypted string of the
	// 16-byte IV. Mandatory if @keyid is given. Ignored if @keyid is
	// absent.
	Iv *string `json:"iv,omitempty"`
	// the associated with the secret from
	Data *string `json:"data,omitempty"`
	// the filename to load the data associated with the secret from
	File *string `json:"file,omitempty"`
}

// Options for set_password specific to the VNC protocol.
//
// Since: 7.0
type SetPasswordOptionsVnc struct {
	// The id of the display where the password should be changed.
	// Defaults to the first.
	Display *string `json:"display,omitempty"`
}

// The struct describes attestation report for a Secure Encrypted
// Virtualization feature.
//
// Since: 6.1
type SevAttestationReport struct {
	// guest attestation report (base64 encoded)
	Data string `json:"data"`
}

// The struct describes capability for a Secure Encrypted
// Virtualization feature.
//
// Since: 2.12
type SevCapability struct {
	// Platform Diffie-Hellman key (base64 encoded)
	Pdh string `json:"pdh"`
	// PDH certificate chain (base64 encoded)
	CertChain string `json:"cert-chain"`
	// Unique ID of CPU0 (base64 encoded) (since 7.1)
	Cpu0Id string `json:"cpu0-id"`
	// C-bit location in page table entry
	Cbitpos int64 `json:"cbitpos"`
	// Number of physical Address bit reduction when SEV is enabled
	ReducedPhysBits int64 `json:"reduced-phys-bits"`
}

// Properties common to objects that are derivatives of sev-common.
//
// Since: 9.1
type SevCommonProperties struct {
	// SEV device to use (default: "/dev/sev")
	SevDevice *string `json:"sev-device,omitempty"`
	// C-bit location in page table entry (default: 0)
	Cbitpos *uint32 `json:"cbitpos,omitempty"`
	// number of bits in physical addresses that become unavailable
	// when SEV is enabled
	ReducedPhysBits uint32 `json:"reduced-phys-bits"`
	// if true, add hashes of kernel/initrd/cmdline to a designated
	// guest firmware page for measured boot with -kernel (default:
	// false) (since 6.2)
	KernelHashes *bool `json:"kernel-hashes,omitempty"`
}

// Information specific to legacy SEV/SEV-ES guests.
//
// Since: 2.12
type SevGuestInfo struct {
	// SEV policy value
	Policy uint32 `json:"policy"`
	// SEV firmware handle
	Handle uint32 `json:"handle"`
}

// Properties for sev-guest objects.
//
// Since: 2.12
type SevGuestProperties struct {
	// SEV device to use (default: "/dev/sev")
	SevDevice *string `json:"sev-device,omitempty"`
	// C-bit location in page table entry (default: 0)
	Cbitpos *uint32 `json:"cbitpos,omitempty"`
	// number of bits in physical addresses that become unavailable
	// when SEV is enabled
	ReducedPhysBits uint32 `json:"reduced-phys-bits"`
	// if true, add hashes of kernel/initrd/cmdline to a designated
	// guest firmware page for measured boot with -kernel (default:
	// false) (since 6.2)
	KernelHashes *bool `json:"kernel-hashes,omitempty"`
	// guest owners DH certificate (encoded with base64)
	DhCertFile *string `json:"dh-cert-file,omitempty"`
	// guest owners session parameters (encoded with base64)
	SessionFile *string `json:"session-file,omitempty"`
	// SEV policy value (default: 0x1)
	Policy *uint32 `json:"policy,omitempty"`
	// SEV firmware handle (default: 0)
	Handle *uint32 `json:"handle,omitempty"`
	// Use legacy KVM_SEV_INIT KVM interface for creating the VM. The
	// newer KVM_SEV_INIT2 interface, from Linux >= 6.10, syncs
	// additional vCPU state when initializing the VMSA structures,
	// which will result in a different guest measurement. Set this to
	// 'on' to force compatibility with older QEMU or kernel versions
	// that rely on legacy KVM_SEV_INIT behavior. 'auto' will behave
	// identically to 'on', but will automatically switch to using
	// KVM_SEV_INIT2 if the user specifies any additional options that
	// require it. If set to 'off', QEMU will require KVM_SEV_INIT2
	// unconditionally. (default: off) (since 9.1)
	LegacyVmType *OnOffAuto `json:"legacy-vm-type,omitempty"`
}

// SEV Guest Launch measurement information
//
// Since: 2.12
type SevLaunchMeasureInfo struct {
	// the measurement value encoded in base64
	Data string `json:"data"`
}

// Information specific to SEV-SNP guests.
//
// Since: 9.1
type SevSnpGuestInfo struct {
	// SEV-SNP policy value
	SnpPolicy uint64 `json:"snp-policy"`
}

// Properties for sev-snp-guest objects.  Most of these are direct
// arguments for the KVM_SNP_* interfaces documented in the Linux
// kernel source under Documentation/arch/x86/amd-memory-
// encryption.rst, which are in turn closely coupled with the
// SNP_INIT/SNP_LAUNCH_* firmware commands documented in the SEV-SNP
// Firmware ABI Specification (Rev 0.9).  More usage information is
// also available in the QEMU source tree under docs/amd-memory-
// encryption.
//
// Since: 9.1
type SevSnpGuestProperties struct {
	// SEV device to use (default: "/dev/sev")
	SevDevice *string `json:"sev-device,omitempty"`
	// C-bit location in page table entry (default: 0)
	Cbitpos *uint32 `json:"cbitpos,omitempty"`
	// number of bits in physical addresses that become unavailable
	// when SEV is enabled
	ReducedPhysBits uint32 `json:"reduced-phys-bits"`
	// if true, add hashes of kernel/initrd/cmdline to a designated
	// guest firmware page for measured boot with -kernel (default:
	// false) (since 6.2)
	KernelHashes *bool `json:"kernel-hashes,omitempty"`
	// the 'POLICY' parameter to the SNP_LAUNCH_START command, as
	// defined in the SEV-SNP firmware ABI (default: 0x30000)
	Policy *uint64 `json:"policy,omitempty"`
	// 16-byte, base64-encoded blob to report hypervisor-defined
	// workarounds, corresponding to the 'GOSVW' parameter of the
	// SNP_LAUNCH_START command defined in the SEV-SNP firmware ABI
	// (default: all-zero)
	GuestVisibleWorkarounds *string `json:"guest-visible-workarounds,omitempty"`
	// 96-byte, base64-encoded blob to provide the 'ID Block'
	// structure for the SNP_LAUNCH_FINISH command defined in the SEV-
	// SNP firmware ABI (default: all-zero)
	IdBlock *string `json:"id-block,omitempty"`
	// 4096-byte, base64-encoded blob to provide the 'ID
	// Authentication Information Structure' for the SNP_LAUNCH_FINISH
	// command defined in the SEV-SNP firmware ABI (default: all-zero)
	IdAuth *string `json:"id-auth,omitempty"`
	// true if 'id-auth' blob contains the 'AUTHOR_KEY' field defined
	// SEV-SNP firmware ABI (default: false)
	AuthorKeyEnabled *bool `json:"author-key-enabled,omitempty"`
	// 32-byte, base64-encoded, user-defined blob to provide to the
	// guest, as documented for the 'HOST_DATA' parameter of the
	// SNP_LAUNCH_FINISH command in the SEV-SNP firmware ABI (default:
	// all-zero)
	HostData *string `json:"host-data,omitempty"`
	// Guests are by default allowed to choose between VLEK (Versioned
	// Loaded Endorsement Key) or VCEK (Versioned Chip Endorsement
	// Key) when requesting attestation reports from firmware. Set
	// this to true to disable the use of VCEK. (default: false)
	// (since: 9.1)
	VcekDisabled *bool `json:"vcek-disabled,omitempty"`
}

// Sgx EPC cmdline information
//
// Since: 6.2
type SgxEPC struct {
	// memory backend linked with device
	Memdev string `json:"memdev"`
	// the numa node (Since: 7.0)
	Node int64 `json:"node"`
}

// Sgx EPC state information
//
// Since: 6.2
type SgxEPCDeviceInfo struct {
	// device's ID
	Id *string `json:"id,omitempty"`
	// physical address in memory, where device is mapped
	Memaddr uint64 `json:"memaddr"`
	// size of memory that the device provides
	Size uint64 `json:"size"`
	// the numa node (Since: 7.0)
	Node int64 `json:"node"`
	// memory backend linked with device
	Memdev string `json:"memdev"`
}

// Since: 6.2
type SgxEPCDeviceInfoWrapper struct {
	// Sgx EPC state information
	Data SgxEPCDeviceInfo `json:"data"`
}

// SGX properties of machine types.
//
// Since: 6.2
type SgxEPCProperties struct {
	// list of ids of memory-backend-epc objects.
	SgxEpc []SgxEPC `json:"sgx-epc"`
}

// Cache information for SMP system.
//
// Since: 9.2
type SmpCacheProperties struct {
	// Cache name, which is the combination of cache level and cache
	// type.
	Cache CacheLevelAndType `json:"cache"`
	// Cache topology level. It accepts the CPU topology enumeration
	// as the parameter, i.e., CPUs in the same topology container
	// share the same cache.
	Topology CpuTopologyLevel `json:"topology"`
}

// List wrapper of SmpCacheProperties.
//
// Since 9.2
type SmpCachePropertiesWrapper struct {
	// the list of SmpCacheProperties.
	Caches []SmpCacheProperties `json:"caches"`
}

// Since: 1.3
type SnapshotInfo struct {
	// unique snapshot id
	Id string `json:"id"`
	// user chosen name
	Name string `json:"name"`
	// size of the VM state
	VmStateSize int64 `json:"vm-state-size"`
	// UTC date of the snapshot in seconds
	DateSec int64 `json:"date-sec"`
	// fractional part in nano seconds to be used with date-sec
	DateNsec int64 `json:"date-nsec"`
	// VM clock relative to boot in seconds
	VmClockSec int64 `json:"vm-clock-sec"`
	// fractional part in nano seconds to be used with vm-clock-sec
	VmClockNsec int64 `json:"vm-clock-nsec"`
	// Current instruction count. Appears when execution record/replay
	// is enabled. Used for "time-traveling" to match the moment in
	// the recorded execution with the snapshots. This counter may be
	// obtained through @query-replay command (since 5.2)
	Icount *int64 `json:"icount,omitempty"`
}

// The basic information for SPICE network connection
//
// Since: 2.1
type SpiceBasicInfo struct {
	// IP address
	Host string `json:"host"`
	// port number
	Port string `json:"port"`
	// address family
	Family NetworkAddressFamily `json:"family"`
}

// Information about a SPICE client channel.
//
// Since: 0.14
type SpiceChannel struct {
	// IP address
	Host string `json:"host"`
	// port number
	Port string `json:"port"`
	// address family
	Family NetworkAddressFamily `json:"family"`
	// SPICE connection id number. All channels with the same id
	// belong to the same SPICE session.
	ConnectionId int64 `json:"connection-id"`
	// SPICE channel type number. "1" is the main control channel,
	// filter for this one if you want to track spice sessions only
	ChannelType int64 `json:"channel-type"`
	// SPICE channel ID number. Usually "0", might be different when
	// multiple channels of the same type exist, such as multiple
	// display channels in a multihead setup
	ChannelId int64 `json:"channel-id"`
	// true if the channel is encrypted, false otherwise.
	Tls bool `json:"tls"`
}

// Information about the SPICE session.
//
// Since: 0.14
type SpiceInfo struct {
	// true if the SPICE server is enabled, false otherwise
	Enabled bool `json:"enabled"`
	// true if the last guest migration completed and spice migration
	// had completed as well, false otherwise (since 1.4)
	Migrated bool `json:"migrated"`
	// The hostname the SPICE server is bound to. This depends on the
	// name resolution on the host and may be an IP address.
	Host *string `json:"host,omitempty"`
	// The SPICE server's port number.
	Port *int64 `json:"port,omitempty"`
	// The SPICE server's TLS port number.
	TlsPort *int64 `json:"tls-port,omitempty"`
	// the current authentication type used by the server - 'none' if
	// no authentication is being used - 'spice' uses SASL or direct
	// TLS authentication, depending on command line options
	Auth *string `json:"auth,omitempty"`
	// SPICE server version.
	CompiledVersion *string `json:"compiled-version,omitempty"`
	// The mode in which the mouse cursor is displayed currently. Can
	// be determined by the client or the server, or unknown if spice
	// server doesn't provide this information. (since: 1.1)
	MouseMode SpiceQueryMouseMode `json:"mouse-mode"`
	// a list of @SpiceChannel for each active spice channel
	Channels []SpiceChannel `json:"channels,omitempty"`
}

// Information about a SPICE server
//
// Since: 2.1
type SpiceServerInfo struct {
	// IP address
	Host string `json:"host"`
	// port number
	Port string `json:"port"`
	// address family
	Family NetworkAddressFamily `json:"family"`
	// authentication method
	Auth *string `json:"auth,omitempty"`
}

// Since: 2.12
type SshHostKeyHash struct {
	// The hash algorithm used for the hash
	Type SshHostKeyCheckHashType `json:"type"`
	// The expected hash value
	Hash string `json:"hash"`
}

// Since: 7.1
type Stats struct {
	// name of stat.
	Name string `json:"name"`
	// stat value.
	Value StatsValue `json:"value"`
}

// Indicates a set of statistics that should be returned by query-
// stats.
//
// Since: 7.1
type StatsRequest struct {
	// provider for which to return statistics.
	Provider StatsProvider `json:"provider"`
	// statistics to be returned (all if omitted).
	Names []string `json:"names,omitempty"`
}

// Since: 7.1
type StatsResult struct {
	// provider for this set of statistics.
	Provider StatsProvider `json:"provider"`
	// Path to the object for which the statistics are returned, if
	// the object is exposed in the QOM tree
	QomPath *string `json:"qom-path,omitempty"`
	// list of statistics.
	Stats []Stats `json:"stats"`
}

// Schema for all available statistics for a provider and target.
//
// Since: 7.1
type StatsSchema struct {
	// provider for this set of statistics.
	Provider StatsProvider `json:"provider"`
	// the kind of object that can be queried through the provider.
	Target StatsTarget `json:"target"`
	// list of statistics.
	Stats []StatsSchemaValue `json:"stats"`
}

// Schema for a single statistic.
//
// Since: 7.1
type StatsSchemaValue struct {
	// name of the statistic; each element of the schema is uniquely
	// identified by a target, a provider (both available in
	// @StatsSchema) and the name.
	Name string `json:"name"`
	// kind of statistic.
	Type StatsType `json:"type"`
	// basic unit of measure for the statistic; if missing, the
	// statistic is a simple number or counter.
	Unit *StatsUnit `json:"unit,omitempty"`
	// base for the multiple of @unit in which the statistic is
	// measured. Only present if @exponent is non-zero; @base and
	// @exponent together form a SI prefix (e.g., _nano-_ for
	// "base=10" and "exponent=-9") or IEC binary prefix (e.g.
	// _kibi-_ for "base=2" and "exponent=10")
	Base *int8 `json:"base,omitempty"`
	// exponent for the multiple of @unit in which the statistic is
	// expressed, or 0 for the basic unit
	Exponent int16 `json:"exponent"`
	// Present when @type is "linear-histogram", contains the width of
	// each bucket of the histogram.
	BucketSize *uint32 `json:"bucket-size,omitempty"`
}

// Since: 7.1
type StatsVCPUFilter struct {
	// list of QOM paths for the desired vCPU objects.
	Vcpus []string `json:"vcpus,omitempty"`
}

// Information about VM run state
//
// Since: 0.14
type StatusInfo struct {
	// true if all VCPUs are runnable, false if not runnable
	Running bool `json:"running"`
	// the virtual machine @RunState
	Status RunState `json:"status"`
}

// A fat type wrapping 'str', to be embedded in lists.
//
// Since: 1.2
type String struct {
	Str string `json:"str"`
}

// Information about the TPM emulator type
//
// Since: 2.11
type TPMEmulatorOptions struct {
	// Name of a unix socket chardev
	Chardev string `json:"chardev"`
}

// Since: 2.11
type TPMEmulatorOptionsWrapper struct {
	// Information about the TPM emulator type
	Data TPMEmulatorOptions `json:"data"`
}

// Information about the TPM
//
// Since: 1.5
type TPMInfo struct {
	// The Id of the TPM
	Id string `json:"id"`
	// The TPM frontend model
	Model TpmModel `json:"model"`
	// The TPM (backend) type configuration options
	Options TpmTypeOptions `json:"options"`
}

// Information about the TPM passthrough type
//
// Since: 1.5
type TPMPassthroughOptions struct {
	// string describing the path used for accessing the TPM device
	Path *string `json:"path,omitempty"`
	// string showing the TPM's sysfs cancel file for cancellation of
	// TPM commands while they are executing
	CancelPath *string `json:"cancel-path,omitempty"`
}

// Since: 1.5
type TPMPassthroughOptionsWrapper struct {
	// Information about the TPM passthrough type
	Data TPMPassthroughOptions `json:"data"`
}

// Information describing the QEMU target.
//
// Since: 1.2
type TargetInfo struct {
	// the target architecture
	Arch SysEmuTarget `json:"arch"`
}

// Properties for thread context objects.
//
// Since: 7.2
type ThreadContextProperties struct {
	// the list of host CPU numbers used as CPU affinity for all
	// threads created in the thread context (default: QEMU main
	// thread CPU affinity)
	CpuAffinity []uint16 `json:"cpu-affinity,omitempty"`
	// the list of host node numbers that will be resolved to a list
	// of host CPU numbers used as CPU affinity. This is a shortcut
	// for specifying the list of host CPU numbers belonging to the
	// host nodes manually by setting @cpu-affinity. (default: QEMU
	// main thread affinity)
	NodeAffinity []uint16 `json:"node-affinity,omitempty"`
}

// Properties for throttle-group objects.
//
// Since: 2.11
type ThrottleGroupProperties struct {
	// limits to apply for this throttle group
	Limits              *ThrottleLimits `json:"limits,omitempty"`
	XIopsTotal          *int64          `json:"x-iops-total,omitempty"`
	XIopsTotalMax       *int64          `json:"x-iops-total-max,omitempty"`
	XIopsTotalMaxLength *int64          `json:"x-iops-total-max-length,omitempty"`
	XIopsRead           *int64          `json:"x-iops-read,omitempty"`
	XIopsReadMax        *int64          `json:"x-iops-read-max,omitempty"`
	XIopsReadMaxLength  *int64          `json:"x-iops-read-max-length,omitempty"`
	XIopsWrite          *int64          `json:"x-iops-write,omitempty"`
	XIopsWriteMax       *int64          `json:"x-iops-write-max,omitempty"`
	XIopsWriteMaxLength *int64          `json:"x-iops-write-max-length,omitempty"`
	XBpsTotal           *int64          `json:"x-bps-total,omitempty"`
	XBpsTotalMax        *int64          `json:"x-bps-total-max,omitempty"`
	XBpsTotalMaxLength  *int64          `json:"x-bps-total-max-length,omitempty"`
	XBpsRead            *int64          `json:"x-bps-read,omitempty"`
	XBpsReadMax         *int64          `json:"x-bps-read-max,omitempty"`
	XBpsReadMaxLength   *int64          `json:"x-bps-read-max-length,omitempty"`
	XBpsWrite           *int64          `json:"x-bps-write,omitempty"`
	XBpsWriteMax        *int64          `json:"x-bps-write-max,omitempty"`
	XBpsWriteMaxLength  *int64          `json:"x-bps-write-max-length,omitempty"`
	XIopsSize           *int64          `json:"x-iops-size,omitempty"`
}

// Limit parameters for throttling.  Since some limit combinations are
// illegal, limits should always be set in one transaction.  All
// fields are optional.  When setting limits, if a field is missing
// the current value is not changed.
//
// Since: 2.11
type ThrottleLimits struct {
	// limit total I/O operations per second
	IopsTotal *int64 `json:"iops-total,omitempty"`
	// I/O operations burst
	IopsTotalMax *int64 `json:"iops-total-max,omitempty"`
	// length of the iops-total-max burst period, in seconds It must
	// only be set if @iops-total-max is set as well.
	IopsTotalMaxLength *int64 `json:"iops-total-max-length,omitempty"`
	// limit read operations per second
	IopsRead *int64 `json:"iops-read,omitempty"`
	// I/O operations read burst
	IopsReadMax *int64 `json:"iops-read-max,omitempty"`
	// length of the iops-read-max burst period, in seconds It must
	// only be set if @iops-read-max is set as well.
	IopsReadMaxLength *int64 `json:"iops-read-max-length,omitempty"`
	// limit write operations per second
	IopsWrite *int64 `json:"iops-write,omitempty"`
	// I/O operations write burst
	IopsWriteMax *int64 `json:"iops-write-max,omitempty"`
	// length of the iops-write-max burst period, in seconds It must
	// only be set if @iops-write-max is set as well.
	IopsWriteMaxLength *int64 `json:"iops-write-max-length,omitempty"`
	// limit total bytes per second
	BpsTotal *int64 `json:"bps-total,omitempty"`
	// total bytes burst
	BpsTotalMax *int64 `json:"bps-total-max,omitempty"`
	// length of the bps-total-max burst period, in seconds. It must
	// only be set if @bps-total-max is set as well.
	BpsTotalMaxLength *int64 `json:"bps-total-max-length,omitempty"`
	// limit read bytes per second
	BpsRead *int64 `json:"bps-read,omitempty"`
	// total bytes read burst
	BpsReadMax *int64 `json:"bps-read-max,omitempty"`
	// length of the bps-read-max burst period, in seconds It must
	// only be set if @bps-read-max is set as well.
	BpsReadMaxLength *int64 `json:"bps-read-max-length,omitempty"`
	// limit write bytes per second
	BpsWrite *int64 `json:"bps-write,omitempty"`
	// total bytes write burst
	BpsWriteMax *int64 `json:"bps-write-max,omitempty"`
	// length of the bps-write-max burst period, in seconds It must
	// only be set if @bps-write-max is set as well.
	BpsWriteMaxLength *int64 `json:"bps-write-max-length,omitempty"`
	// when limiting by iops max size of an I/O in bytes
	IopsSize *int64 `json:"iops-size,omitempty"`
}

// Properties for tls-creds-anon objects.
//
// Since: 2.5
type TlsCredsAnonProperties struct {
	// if true the peer credentials will be verified once the
	// handshake is completed. This is a no-op for anonymous
	// credentials. (default: true)
	VerifyPeer *bool `json:"verify-peer,omitempty"`
	// the path of the directory that contains the credential files
	Dir *string `json:"dir,omitempty"`
	// whether the QEMU network backend that uses the credentials will
	// be acting as a client or as a server (default: client)
	Endpoint *QCryptoTLSCredsEndpoint `json:"endpoint,omitempty"`
	// a gnutls priority string as described at
	// https://gnutls.org/manual/html_node/Priority-Strings.html
	Priority *string `json:"priority,omitempty"`
}

// Properties for objects of classes derived from tls-creds.
//
// Since: 2.5
type TlsCredsProperties struct {
	// if true the peer credentials will be verified once the
	// handshake is completed. This is a no-op for anonymous
	// credentials. (default: true)
	VerifyPeer *bool `json:"verify-peer,omitempty"`
	// the path of the directory that contains the credential files
	Dir *string `json:"dir,omitempty"`
	// whether the QEMU network backend that uses the credentials will
	// be acting as a client or as a server (default: client)
	Endpoint *QCryptoTLSCredsEndpoint `json:"endpoint,omitempty"`
	// a gnutls priority string as described at
	// https://gnutls.org/manual/html_node/Priority-Strings.html
	Priority *string `json:"priority,omitempty"`
}

// Properties for tls-creds-psk objects.
//
// Since: 3.0
type TlsCredsPskProperties struct {
	// if true the peer credentials will be verified once the
	// handshake is completed. This is a no-op for anonymous
	// credentials. (default: true)
	VerifyPeer *bool `json:"verify-peer,omitempty"`
	// the path of the directory that contains the credential files
	Dir *string `json:"dir,omitempty"`
	// whether the QEMU network backend that uses the credentials will
	// be acting as a client or as a server (default: client)
	Endpoint *QCryptoTLSCredsEndpoint `json:"endpoint,omitempty"`
	// a gnutls priority string as described at
	// https://gnutls.org/manual/html_node/Priority-Strings.html
	Priority *string `json:"priority,omitempty"`
	// the username which will be sent to the server. For clients
	// only. If absent, "qemu" is sent and the property will read back
	// as an empty string.
	Username *string `json:"username,omitempty"`
}

// Properties for tls-creds-x509 objects.
//
// Since: 2.5
type TlsCredsX509Properties struct {
	// if true the peer credentials will be verified once the
	// handshake is completed. This is a no-op for anonymous
	// credentials. (default: true)
	VerifyPeer *bool `json:"verify-peer,omitempty"`
	// the path of the directory that contains the credential files
	Dir *string `json:"dir,omitempty"`
	// whether the QEMU network backend that uses the credentials will
	// be acting as a client or as a server (default: client)
	Endpoint *QCryptoTLSCredsEndpoint `json:"endpoint,omitempty"`
	// a gnutls priority string as described at
	// https://gnutls.org/manual/html_node/Priority-Strings.html
	Priority *string `json:"priority,omitempty"`
	// if true, perform some sanity checks before using the
	// credentials (default: true)
	SanityCheck *bool `json:"sanity-check,omitempty"`
	// For the server-key.pem and client-key.pem files which contain
	// sensitive private keys, it is possible to use an encrypted
	// version by providing the @passwordid parameter. This provides
	// the ID of a previously created secret object containing the
	// password for decryption.
	Passwordid *string `json:"passwordid,omitempty"`
}

// Information of a tracing event.
//
// Since: 2.2
type TraceEventInfo struct {
	// Event name.
	Name string `json:"name"`
	// Tracing state.
	State TraceEventState `json:"state"`
}

// Optional arguments to modify the behavior of a Transaction.
//
// Since: 2.5
type TransactionProperties struct {
	// Controls how jobs launched asynchronously by Actions will
	// complete or fail as a group. See @ActionCompletionMode for
	// details.
	CompletionMode *ActionCompletionMode `json:"completion-mode,omitempty"`
}

// Captures a socket address in the local ("Unix socket") namespace.
//
// Since: 1.3
type UnixSocketAddress struct {
	// filesystem path to use
	Path string `json:"path"`
	// if true, this is a Linux abstract socket address. @path will be
	// prefixed by a null byte, and optionally padded with null bytes.
	// Defaults to false. (Since 5.1)
	Abstract *bool `json:"abstract,omitempty"`
	// if false, pad an abstract socket address with enough null bytes
	// to make it fill struct sockaddr_un member sun_path. Defaults to
	// true. (Since 5.1)
	Tight *bool `json:"tight,omitempty"`
}

// Since: 1.3
type UnixSocketAddressWrapper struct {
	// UNIX domain socket address
	Data UnixSocketAddress `json:"data"`
}

// Guest UUID information (Universally Unique Identifier).
//
// Since: 0.14
//
// .. note:: If no UUID was specified for the guest, the nil UUID (all
// zeroes) is returned.
type UuidInfo struct {
	// the UUID of the guest
	Uuid string `json:"UUID"`
}

// A description of QEMU's version.
//
// Since: 0.14
type VersionInfo struct {
	// The version of QEMU. By current convention, a micro version of
	// 50 signifies a development branch. A micro version greater than
	// or equal to 90 signifies a release candidate for the next minor
	// version. A micro version of less than 50 signifies a stable
	// release.
	Qemu VersionTriple `json:"qemu"`
	// QEMU will always set this field to an empty string. Downstream
	// versions of QEMU should set this to a non-empty string. The
	// exact format depends on the downstream however it highly
	// recommended that a unique name is used.
	Package string `json:"package"`
}

// A three-part version number.
//
// Since: 2.4
type VersionTriple struct {
	// The major version number.
	Major int64 `json:"major"`
	// The minor version number.
	Minor int64 `json:"minor"`
	// The micro version number.
	Micro int64 `json:"micro"`
}

// Detailed VFIO devices migration statistics
//
// Since: 5.2
type VfioStats struct {
	// amount of bytes transferred to the target VM by VFIO devices
	Transferred int64 `json:"transferred"`
}

// Properties for x-vfio-user-server objects.
//
// Since: 7.1
type VfioUserServerProperties struct {
	// socket to be used by the libvfio-user library
	Socket SocketAddress `json:"socket"`
	// the ID of the device to be emulated at the server
	Device string `json:"device"`
}

// A structure defined to list the vhost user protocol features of a
// Vhost User device
//
// Since: 7.2
type VhostDeviceProtocols struct {
	// List of decoded vhost user protocol features of a vhost user
	// device
	Protocols []string `json:"protocols"`
	// Vhost user device protocol features bitmap that have not been
	// decoded
	UnknownProtocols *uint64 `json:"unknown-protocols,omitempty"`
}

// Information about a vhost device.  This information will only be
// displayed if the vhost device is active.
//
// Since: 7.2
type VhostStatus struct {
	// vhost_dev n_mem_sections
	NMemSections int64 `json:"n-mem-sections"`
	// vhost_dev n_tmp_sections
	NTmpSections int64 `json:"n-tmp-sections"`
	// vhost_dev nvqs (number of virtqueues being used)
	Nvqs uint32 `json:"nvqs"`
	// vhost_dev vq_index
	VqIndex int64 `json:"vq-index"`
	// vhost_dev features
	Features VirtioDeviceFeatures `json:"features"`
	// vhost_dev acked_features
	AckedFeatures VirtioDeviceFeatures `json:"acked-features"`
	// vhost_dev backend_features
	BackendFeatures VirtioDeviceFeatures `json:"backend-features"`
	// vhost_dev protocol_features
	ProtocolFeatures VhostDeviceProtocols `json:"protocol-features"`
	// vhost_dev max_queues
	MaxQueues uint64 `json:"max-queues"`
	// vhost_dev backend_cap
	BackendCap uint64 `json:"backend-cap"`
	// vhost_dev log_enabled flag
	LogEnabled bool `json:"log-enabled"`
	// vhost_dev log_size
	LogSize uint64 `json:"log-size"`
}

// Information of a VirtIODevice VirtQueue, including most members of
// the VirtQueue data structure.
//
// Since: 7.2
type VirtQueueStatus struct {
	// Name of the VirtIODevice that uses this VirtQueue
	Name string `json:"name"`
	// VirtQueue queue_index
	QueueIndex uint16 `json:"queue-index"`
	// VirtQueue inuse
	Inuse uint32 `json:"inuse"`
	// VirtQueue vring.num
	VringNum uint32 `json:"vring-num"`
	// VirtQueue vring.num_default
	VringNumDefault uint32 `json:"vring-num-default"`
	// VirtQueue vring.align
	VringAlign uint32 `json:"vring-align"`
	// VirtQueue vring.desc (descriptor area)
	VringDesc uint64 `json:"vring-desc"`
	// VirtQueue vring.avail (driver area)
	VringAvail uint64 `json:"vring-avail"`
	// VirtQueue vring.used (device area)
	VringUsed uint64 `json:"vring-used"`
	// VirtQueue last_avail_idx or return of vhost_dev
	// vhost_get_vring_base (if vhost active)
	LastAvailIdx *uint16 `json:"last-avail-idx,omitempty"`
	// VirtQueue shadow_avail_idx
	ShadowAvailIdx *uint16 `json:"shadow-avail-idx,omitempty"`
	// VirtQueue used_idx
	UsedIdx uint16 `json:"used-idx"`
	// VirtQueue signalled_used
	SignalledUsed uint16 `json:"signalled-used"`
	// VirtQueue signalled_used_valid flag
	SignalledUsedValid bool `json:"signalled-used-valid"`
}

// Information of a vhost device's vhost_virtqueue, including most
// members of the vhost_dev vhost_virtqueue data structure.
//
// Since: 7.2
type VirtVhostQueueStatus struct {
	// Name of the VirtIODevice that uses this vhost_virtqueue
	Name string `json:"name"`
	// vhost_virtqueue kick
	Kick int64 `json:"kick"`
	// vhost_virtqueue call
	Call int64 `json:"call"`
	// vhost_virtqueue desc
	Desc uint64 `json:"desc"`
	// vhost_virtqueue avail
	Avail uint64 `json:"avail"`
	// vhost_virtqueue used
	Used uint64 `json:"used"`
	// vhost_virtqueue num
	Num int64 `json:"num"`
	// vhost_virtqueue desc_phys (descriptor area physical address)
	DescPhys uint64 `json:"desc-phys"`
	// vhost_virtqueue desc_size
	DescSize uint32 `json:"desc-size"`
	// vhost_virtqueue avail_phys (driver area physical address)
	AvailPhys uint64 `json:"avail-phys"`
	// vhost_virtqueue avail_size
	AvailSize uint32 `json:"avail-size"`
	// vhost_virtqueue used_phys (device area physical address)
	UsedPhys uint64 `json:"used-phys"`
	// vhost_virtqueue used_size
	UsedSize uint32 `json:"used-size"`
}

// The common fields that apply to most Virtio devices.  Some devices
// may not have their own device-specific features (e.g. virtio-rng).
//
// Since: 7.2
type VirtioDeviceFeatures struct {
	// List of transport features of the virtio device
	Transports []string `json:"transports"`
	// List of device-specific features (if the device has unique
	// features)
	DevFeatures []string `json:"dev-features,omitempty"`
	// Virtio device features bitmap that have not been decoded
	UnknownDevFeatures *uint64 `json:"unknown-dev-features,omitempty"`
}

// A structure defined to list the configuration statuses of a virtio
// device
//
// Since: 7.2
type VirtioDeviceStatus struct {
	// List of decoded configuration statuses of the virtio device
	Statuses []string `json:"statuses"`
	// Virtio device statuses bitmap that have not been decoded
	UnknownStatuses *uint8 `json:"unknown-statuses,omitempty"`
}

// Basic information about a given VirtIODevice
//
// Since: 7.2
type VirtioInfo struct {
	// The VirtIODevice's canonical QOM path
	Path string `json:"path"`
	// Name of the VirtIODevice
	Name string `json:"name"`
}

// VirtioMEMDevice state information
//
// Since: 5.1
type VirtioMEMDeviceInfo struct {
	// device's ID
	Id *string `json:"id,omitempty"`
	// physical address in memory, where device is mapped
	Memaddr uint64 `json:"memaddr"`
	// the user requested size of the device
	RequestedSize uint64 `json:"requested-size"`
	// the (current) size of memory that the device provides
	Size uint64 `json:"size"`
	// the maximum size of memory that the device can provide
	MaxSize uint64 `json:"max-size"`
	// the block size of memory that the device provides
	BlockSize uint64 `json:"block-size"`
	// NUMA node number where device is assigned to
	Node int64 `json:"node"`
	// memory backend linked with the region
	Memdev string `json:"memdev"`
}

// Since: 2.1
type VirtioMEMDeviceInfoWrapper struct {
	// VirtioMEMDevice state information
	Data VirtioMEMDeviceInfo `json:"data"`
}

// VirtioPMEM state information
//
// Since: 4.1
type VirtioPMEMDeviceInfo struct {
	// device's ID
	Id *string `json:"id,omitempty"`
	// physical address in memory, where device is mapped
	Memaddr uint64 `json:"memaddr"`
	// size of memory that the device provides
	Size uint64 `json:"size"`
	// memory backend linked with device
	Memdev string `json:"memdev"`
}

// Since: 2.1
type VirtioPMEMDeviceInfoWrapper struct {
	// VirtioPMEM state information
	Data VirtioPMEMDeviceInfo `json:"data"`
}

// Information regarding a VirtQueue's VirtQueueElement including
// descriptor, driver, and device areas
//
// Since: 7.2
type VirtioQueueElement struct {
	// Name of the VirtIODevice that uses this VirtQueue
	Name string `json:"name"`
	// Index of the element in the queue
	Index uint32 `json:"index"`
	// List of descriptors (VirtioRingDesc)
	Descs []VirtioRingDesc `json:"descs"`
	// VRingAvail info
	Avail VirtioRingAvail `json:"avail"`
	// VRingUsed info
	Used VirtioRingUsed `json:"used"`
}

// Information regarding the avail vring (a.k.a. driver area)
//
// Since: 7.2
type VirtioRingAvail struct {
	// VRingAvail flags
	Flags uint16 `json:"flags"`
	// VRingAvail index
	Idx uint16 `json:"idx"`
	// VRingAvail ring[] entry at provided index
	Ring uint16 `json:"ring"`
}

// Information regarding the vring descriptor area
//
// Since: 7.2
type VirtioRingDesc struct {
	// Guest physical address of the descriptor area
	Addr uint64 `json:"addr"`
	// Length of the descriptor area
	Len uint32 `json:"len"`
	// List of descriptor flags
	Flags []string `json:"flags"`
}

// Information regarding the used vring (a.k.a. device area)
//
// Since: 7.2
type VirtioRingUsed struct {
	// VRingUsed flags
	Flags uint16 `json:"flags"`
	// VRingUsed index
	Idx uint16 `json:"idx"`
}

// Full status of the virtio device with most VirtIODevice members.
// Also includes the full status of the corresponding vhost device if
// the vhost device is active.
//
// Since: 7.2
type VirtioStatus struct {
	// VirtIODevice name
	Name string `json:"name"`
	// VirtIODevice ID
	DeviceId uint16 `json:"device-id"`
	// VirtIODevice vhost_started flag
	VhostStarted bool `json:"vhost-started"`
	// VirtIODevice device_endian
	DeviceEndian string `json:"device-endian"`
	// VirtIODevice guest_features
	GuestFeatures VirtioDeviceFeatures `json:"guest-features"`
	// VirtIODevice host_features
	HostFeatures VirtioDeviceFeatures `json:"host-features"`
	// VirtIODevice backend_features
	BackendFeatures VirtioDeviceFeatures `json:"backend-features"`
	// VirtIODevice virtqueue count. This is the number of active
	// virtqueues being used by the VirtIODevice.
	NumVqs int64 `json:"num-vqs"`
	// VirtIODevice configuration status (VirtioDeviceStatus)
	Status VirtioDeviceStatus `json:"status"`
	// VirtIODevice ISR
	Isr uint8 `json:"isr"`
	// VirtIODevice queue_sel
	QueueSel uint16 `json:"queue-sel"`
	// VirtIODevice vm_running flag
	VmRunning bool `json:"vm-running"`
	// VirtIODevice broken flag
	Broken bool `json:"broken"`
	// VirtIODevice disabled flag
	Disabled bool `json:"disabled"`
	// VirtIODevice use_started flag
	UseStarted bool `json:"use-started"`
	// VirtIODevice started flag
	Started bool `json:"started"`
	// VirtIODevice start_on_kick flag
	StartOnKick bool `json:"start-on-kick"`
	// VirtIODevice disabled_legacy_check flag
	DisableLegacyCheck bool `json:"disable-legacy-check"`
	// VirtIODevice bus_name
	BusName string `json:"bus-name"`
	// VirtIODevice use_guest_notifier_mask flag
	UseGuestNotifierMask bool `json:"use-guest-notifier-mask"`
	// Corresponding vhost device info for a given VirtIODevice.
	// Present if the given VirtIODevice has an active vhost device.
	VhostDev *VhostStatus `json:"vhost-dev,omitempty"`
}

// Information about a VMDK extent file
//
// Since: 8.0
type VmdkExtentInfo struct {
	// Name of the extent file
	Filename string `json:"filename"`
	// Extent type (e.g. FLAT or SPARSE)
	Format string `json:"format"`
	// Number of bytes covered by this extent
	VirtualSize int64 `json:"virtual-size"`
	// Cluster size in bytes (for non-flat extents)
	ClusterSize *int64 `json:"cluster-size,omitempty"`
	// Whether this extent contains compressed data
	Compressed *bool `json:"compressed,omitempty"`
}

// The basic information for vnc network connection
//
// Since: 2.1
type VncBasicInfo struct {
	// IP address
	Host string `json:"host"`
	// The service name of the vnc port. This may depend on the host
	// system's service database so symbolic names should not be
	// relied on.
	Service string `json:"service"`
	// address family
	Family NetworkAddressFamily `json:"family"`
	// true in case the socket is a websocket (since 2.3).
	Websocket bool `json:"websocket"`
}

// Information about a connected VNC client.
//
// Since: 0.14
type VncClientInfo struct {
	// IP address
	Host string `json:"host"`
	// The service name of the vnc port. This may depend on the host
	// system's service database so symbolic names should not be
	// relied on.
	Service string `json:"service"`
	// address family
	Family NetworkAddressFamily `json:"family"`
	// true in case the socket is a websocket (since 2.3).
	Websocket bool `json:"websocket"`
	// If x509 authentication is in use, the Distinguished Name of the
	// client.
	X509Dname *string `json:"x509_dname,omitempty"`
	// If SASL authentication is in use, the SASL username used for
	// authentication.
	SaslUsername *string `json:"sasl_username,omitempty"`
}

// Information about the VNC session.
//
// Since: 0.14
type VncInfo struct {
	// true if the VNC server is enabled, false otherwise
	Enabled bool `json:"enabled"`
	// The hostname the VNC server is bound to. This depends on the
	// name resolution on the host and may be an IP address.
	Host *string `json:"host,omitempty"`
	// - 'ipv6' if the host is listening for IPv6 connections - 'ipv4'
	// if the host is listening for IPv4 connections - 'unix' if the
	// host is listening on a unix domain socket - 'unknown' otherwise
	Family *NetworkAddressFamily `json:"family,omitempty"`
	// The service name of the server's port. This may depends on the
	// host system's service database so symbolic names should not be
	// relied on.
	Service *string `json:"service,omitempty"`
	// the current authentication type used by the server - 'none' if
	// no authentication is being used - 'vnc' if VNC authentication
	// is being used - 'vencrypt+plain' if VEncrypt is used with plain
	// text authentication - 'vencrypt+tls+none' if VEncrypt is used
	// with TLS and no authentication - 'vencrypt+tls+vnc' if VEncrypt
	// is used with TLS and VNC authentication - 'vencrypt+tls+plain'
	// if VEncrypt is used with TLS and plain text auth -
	// 'vencrypt+x509+none' if VEncrypt is used with x509 and no auth
	// - 'vencrypt+x509+vnc' if VEncrypt is used with x509 and VNC
	// auth - 'vencrypt+x509+plain' if VEncrypt is used with x509 and
	// plain text auth - 'vencrypt+tls+sasl' if VEncrypt is used with
	// TLS and SASL auth - 'vencrypt+x509+sasl' if VEncrypt is used
	// with x509 and SASL auth
	Auth *string `json:"auth,omitempty"`
	// a list of @VncClientInfo of all currently connected clients
	Clients []VncClientInfo `json:"clients,omitempty"`
}

// Information about a vnc server
//
// Since: 2.3
type VncInfo2 struct {
	// vnc server name.
	Id string `json:"id"`
	// A list of @VncBasincInfo describing all listening sockets. The
	// list can be empty (in case the vnc server is disabled). It also
	// may have multiple entries: normal + websocket, possibly also
	// ipv4 + ipv6 in the future.
	Server []VncServerInfo2 `json:"server"`
	// A list of @VncClientInfo of all currently connected clients.
	// The list can be empty, for obvious reasons.
	Clients []VncClientInfo `json:"clients"`
	// The current authentication type used by the non-websockets
	// servers
	Auth VncPrimaryAuth `json:"auth"`
	// The vencrypt authentication type used by the servers, only
	// specified in case auth == vencrypt.
	Vencrypt *VncVencryptSubAuth `json:"vencrypt,omitempty"`
	// The display device the vnc server is linked to.
	Display *string `json:"display,omitempty"`
}

// The network connection information for server
//
// Since: 2.1
type VncServerInfo struct {
	// IP address
	Host string `json:"host"`
	// The service name of the vnc port. This may depend on the host
	// system's service database so symbolic names should not be
	// relied on.
	Service string `json:"service"`
	// address family
	Family NetworkAddressFamily `json:"family"`
	// true in case the socket is a websocket (since 2.3).
	Websocket bool `json:"websocket"`
	// authentication method used for the plain (non-websocket) VNC
	// server
	Auth *string `json:"auth,omitempty"`
}

// The network connection information for server
//
// Since: 2.9
type VncServerInfo2 struct {
	// IP address
	Host string `json:"host"`
	// The service name of the vnc port. This may depend on the host
	// system's service database so symbolic names should not be
	// relied on.
	Service string `json:"service"`
	// address family
	Family NetworkAddressFamily `json:"family"`
	// true in case the socket is a websocket (since 2.3).
	Websocket bool `json:"websocket"`
	// The current authentication type used by the servers
	Auth VncPrimaryAuth `json:"auth"`
	// The vencrypt sub authentication type used by the servers, only
	// specified in case auth == vencrypt.
	Vencrypt *VncVencryptSubAuth `json:"vencrypt,omitempty"`
}

// Captures a socket address in the vsock namespace.
//
// .. note:: String types are used to allow for possible future
// hostname or service resolution support.
//
// Since: 2.8
type VsockSocketAddress struct {
	// unique host identifier
	Cid string `json:"cid"`
	// port
	Port string `json:"port"`
}

// Since: 2.8
type VsockSocketAddressWrapper struct {
	// VSOCK domain socket address
	Data VsockSocketAddress `json:"data"`
}

// Information about a X86 CPU feature word
//
// Since: 1.5
type X86CPUFeatureWordInfo struct {
	// Input EAX value for CPUID instruction for that feature word
	CpuidInputEax int64 `json:"cpuid-input-eax"`
	// Input ECX value for CPUID instruction for that feature word
	CpuidInputEcx *int64 `json:"cpuid-input-ecx,omitempty"`
	// Output register containing the feature bits
	CpuidRegister X86CPURegister32 `json:"cpuid-register"`
	// value of output register, containing the feature bits
	Features int64 `json:"features"`
}

// Detailed XBZRLE migration cache statistics
//
// Since: 1.2
type XBZRLECacheStats struct {
	// XBZRLE cache size
	CacheSize uint64 `json:"cache-size"`
	// amount of bytes already transferred to the target VM
	Bytes int64 `json:"bytes"`
	// amount of pages transferred to the target VM
	Pages int64 `json:"pages"`
	// number of cache miss
	CacheMiss int64 `json:"cache-miss"`
	// rate of cache miss (since 2.1)
	CacheMissRate float64 `json:"cache-miss-rate"`
	// rate of encoded bytes (since 5.1)
	EncodingRate float64 `json:"encoding-rate"`
	// number of overflows
	Overflow int64 `json:"overflow"`
}

// Block Graph - list of nodes and list of edges.
//
// Since: 4.0
type XDbgBlockGraph struct {
	Nodes []XDbgBlockGraphNode `json:"nodes"`
	Edges []XDbgBlockGraphEdge `json:"edges"`
}

// Block Graph edge description for x-debug-query-block-graph.
//
// Since: 4.0
type XDbgBlockGraphEdge struct {
	// parent id
	Parent uint64 `json:"parent"`
	// child id
	Child uint64 `json:"child"`
	// name of the relation (examples are 'file' and 'backing')
	Name string `json:"name"`
	// granted permissions for the parent operating on the child
	Perm []BlockPermission `json:"perm"`
	// permissions that can still be granted to other users of the
	// child while it is still attached to this parent
	SharedPerm []BlockPermission `json:"shared-perm"`
}

// Since: 4.0
type XDbgBlockGraphNode struct {
	// Block graph node identifier. This @id is generated only for
	// x-debug-query-block-graph and does not relate to any other
	// identifiers in Qemu.
	Id uint64 `json:"id"`
	// Type of graph node. Can be one of block-backend, block-job or
	// block-driver-state.
	Type XDbgBlockGraphNodeType `json:"type"`
	// Human readable name of the node. Corresponds to node-name for
	// block-driver-state nodes; is not guaranteed to be unique in the
	// whole graph (with block-jobs and block-backends).
	Name string `json:"name"`
}

// Specifies which block graph node to yank.  See @YankInstance for
// more information.
//
// Since: 6.0
type YankInstanceBlockNode struct {
	// the name of the block graph node
	NodeName string `json:"node-name"`
}

// Specifies which character device to yank.  See @YankInstance for
// more information.
//
// Since: 6.0
type YankInstanceChardev struct {
	// the chardev's ID
	Id string `json:"id"`
}
