/*
 * Copyright 2025 Red Hat, Inc.
 * SPDX-License-Identifier: (MIT-0 and GPL-2.0-or-later)
 */

/****************************************************************************
 * THIS CODE HAS BEEN GENERATED. DO NOT CHANGE IT DIRECTLY                  *
 ****************************************************************************/
package qapi

// Emitted when guest executes ACPI _OST method.
//
// Since: 2.1
//
// .. qmp-example::    <- { "event": "ACPI_DEVICE_OST",      "data": {
// "info": { "device": "d1", "slot": "0",                "slot-type":
// "DIMM", "source": 1, "status": 0 } },      "timestamp": {
// "seconds": 1265044230, "microseconds": 450486 } }
type AcpiDeviceOstEvent struct {
	// OSPM Status Indication
	Info ACPIOSTInfo `json:"info"`
}

// Emitted when the guest changes the actual BALLOON level.  This
// value is equivalent to the @actual field return by the 'query-
// balloon' command
//
// .. note:: This event is rate-limited.
//
// Since: 1.2
//
// .. qmp-example::    <- { "event": "BALLOON_CHANGE",      "data": {
// "actual": 944766976 },      "timestamp": { "seconds": 1267020223,
// "microseconds": 435656 } }
type BalloonChangeEvent struct {
	// the logical size of the VM in bytes Formula used:
	// logical_vm_size = vm_ram_size - balloon_size
	Actual int64 `json:"actual"`
}

// Emitted when a block export is removed and its id can be reused.
//
// Since: 5.2
type BlockExportDeletedEvent struct {
	// Block export id.
	Id string `json:"id"`
}

// Emitted when a disk image is being marked corrupt.  The image can
// be identified by its device or node name.  The 'device' field is
// always present for compatibility reasons, but it can be empty ("")
// if the image does not have a device name associated.
//
// .. note:: If action is "stop", a STOP event will eventually follow
// the BLOCK_IO_ERROR event.  .. qmp-example::    <- { "event":
// "BLOCK_IMAGE_CORRUPTED",      "data": { "device": "", "node-name":
// "drive", "fatal": false,           "msg": "L2 table offset
// 0x2a2a2a00 unaligned (L1 index: 0)" },      "timestamp": {
// "seconds": 1648243240, "microseconds": 906060 } }
//
// Since: 1.7
type BlockImageCorruptedEvent struct {
	// device name. This is always present for compatibility reasons,
	// but it can be empty ("") if the image does not have a device
	// name associated.
	Device string `json:"device"`
	// node name (Since: 2.4)
	NodeName *string `json:"node-name,omitempty"`
	// informative message for human consumption, such as the kind of
	// corruption being detected. It should not be parsed by machine
	// as it is not guaranteed to be stable
	Msg string `json:"msg"`
	// if the corruption resulted from an image access, this is the
	// host's access offset into the image
	Offset *int64 `json:"offset,omitempty"`
	// if the corruption resulted from an image access, this is the
	// access size
	Size *int64 `json:"size,omitempty"`
	// if set, the image is marked corrupt and therefore unusable
	// after this event and must be repaired (Since 2.2; before, every
	// BLOCK_IMAGE_CORRUPTED event was fatal)
	Fatal bool `json:"fatal"`
}

// Emitted when a disk I/O error occurs
//
// .. note:: If action is "stop", a STOP event will eventually follow
// the BLOCK_IO_ERROR event.  .. note:: This event is rate-limited.
//
// Since: 0.13
//
// .. qmp-example::    <- { "event": "BLOCK_IO_ERROR",      "data": {
// "qom-path": "/machine/unattached/device[0]",           "device":
// "ide0-hd1",           "node-name": "#block212",
// "operation": "write",           "action": "stop",
// "reason": "No space left on device" },      "timestamp": {
// "seconds": 1265044230, "microseconds": 450486 } }
type BlockIoErrorEvent struct {
	// path to the device object in the QOM tree (since 9.2)
	QomPath string `json:"qom-path"`
	// device name. This is always present for compatibility reasons,
	// but it can be empty ("") if the image does not have a device
	// name associated.
	Device string `json:"device"`
	// node name. Note that errors may be reported for the root node
	// that is directly attached to a guest device rather than for the
	// node where the error occurred. The node name is not present if
	// the drive is empty. (Since: 2.8)
	NodeName *string `json:"node-name,omitempty"`
	// I/O operation
	Operation IoOperationType `json:"operation"`
	// action that has been taken
	Action BlockErrorAction `json:"action"`
	// true if I/O error was caused due to a no-space condition. This
	// key is only present if query-block's io-status is present,
	// please see query-block documentation for more information
	// (since: 2.2)
	Nospace *bool `json:"nospace,omitempty"`
	// human readable string describing the error cause. (This field
	// is a debugging aid for humans, it should not be parsed by
	// applications) (since: 2.2)
	Reason string `json:"reason"`
}

// Emitted when a block job has been cancelled
//
// Since: 1.1
//
// .. qmp-example::    <- { "event": "BLOCK_JOB_CANCELLED",
// "data": { "type": "stream", "device": "virtio-disk0",
// "len": 10737418240, "offset": 134217728,           "speed": 0 },
// "timestamp": { "seconds": 1267061043, "microseconds": 959568 } }
type BlockJobCancelledEvent struct {
	// job type
	Type JobType `json:"type"`
	// The job identifier. Originally the device name but other values
	// are allowed since QEMU 2.7
	Device string `json:"device"`
	// maximum progress value
	Len int64 `json:"len"`
	// current progress value. On success this is equal to len. On
	// failure this is less than len
	Offset int64 `json:"offset"`
	// rate limit, bytes per second
	Speed int64 `json:"speed"`
}

// Emitted when a block job has completed
//
// Since: 1.1
//
// .. qmp-example::    <- { "event": "BLOCK_JOB_COMPLETED",
// "data": { "type": "stream", "device": "virtio-disk0",
// "len": 10737418240, "offset": 10737418240,           "speed": 0 },
// "timestamp": { "seconds": 1267061043, "microseconds": 959568 } }
type BlockJobCompletedEvent struct {
	// job type
	Type JobType `json:"type"`
	// The job identifier. Originally the device name but other values
	// are allowed since QEMU 2.7
	Device string `json:"device"`
	// maximum progress value
	Len int64 `json:"len"`
	// current progress value. On success this is equal to len. On
	// failure this is less than len
	Offset int64 `json:"offset"`
	// rate limit, bytes per second
	Speed int64 `json:"speed"`
	// error message. Only present on failure. This field contains a
	// human-readable error message. There are no semantics other than
	// that streaming has failed and clients should not try to
	// interpret the error string
	Error *string `json:"error,omitempty"`
}

// Emitted when a block job encounters an error
//
// Since: 1.3
//
// .. qmp-example::    <- { "event": "BLOCK_JOB_ERROR",      "data": {
// "device": "ide0-hd1",           "operation": "write",
// "action": "stop" },      "timestamp": { "seconds": 1265044230,
// "microseconds": 450486 } }
type BlockJobErrorEvent struct {
	// The job identifier. Originally the device name but other values
	// are allowed since QEMU 2.7
	Device string `json:"device"`
	// I/O operation
	Operation IoOperationType `json:"operation"`
	// action that has been taken
	Action BlockErrorAction `json:"action"`
}

// Emitted when a block job is awaiting explicit authorization to
// finalize graph changes via @block-job-finalize.  If this job is
// part of a transaction, it will not emit this event until the
// transaction has converged first.
//
// Since: 2.12
//
// .. qmp-example::    <- { "event": "BLOCK_JOB_PENDING",      "data":
// { "type": "mirror", "id": "backup_1" },      "timestamp": {
// "seconds": 1265044230, "microseconds": 450486 } }
type BlockJobPendingEvent struct {
	// job type
	Type JobType `json:"type"`
	// The job identifier.
	Id string `json:"id"`
}

// Emitted when a block job is ready to complete
//
// .. note:: The "ready to complete" status is always reset by a
// @BLOCK_JOB_ERROR event.
//
// Since: 1.3
//
// .. qmp-example::    <- { "event": "BLOCK_JOB_READY",      "data": {
// "device": "drive0", "type": "mirror", "speed": 0,           "len":
// 2097152, "offset": 2097152 },      "timestamp": { "seconds":
// 1265044230, "microseconds": 450486 } }
type BlockJobReadyEvent struct {
	// job type
	Type JobType `json:"type"`
	// The job identifier. Originally the device name but other values
	// are allowed since QEMU 2.7
	Device string `json:"device"`
	// maximum progress value
	Len int64 `json:"len"`
	// current progress value. On success this is equal to len. On
	// failure this is less than len
	Offset int64 `json:"offset"`
	// rate limit, bytes per second
	Speed int64 `json:"speed"`
}

// Emitted when writes on block device reaches or exceeds the
// configured write threshold.  For thin-provisioned devices, this
// means the device should be extended to avoid pausing for disk
// exhaustion.  The event is one shot.  Once triggered, it needs to be
// re-registered with another block-set-write-threshold command.
//
// Since: 2.3
type BlockWriteThresholdEvent struct {
	// graph node name on which the threshold was exceeded.
	NodeName string `json:"node-name"`
	// amount of data which exceeded the threshold, in bytes.
	AmountExceeded uint64 `json:"amount-exceeded"`
	// last configured threshold, in bytes.
	WriteThreshold uint64 `json:"write-threshold"`
}

// Emitted when VM finishes COLO mode due to some errors happening or
// at the request of users.
//
// Since: 3.1
//
// .. qmp-example::    <- { "timestamp": {"seconds": 2032141960,
// "microseconds": 417172},      "event": "COLO_EXIT", "data":
// {"mode": "primary", "reason": "request" } }
type ColoExitEvent struct {
	// report COLO mode when COLO exited.
	Mode COLOMode `json:"mode"`
	// describes the reason for the COLO exit.
	Reason COLOExitReason `json:"reason"`
}

// Emitted when the guest asks to change the polarization.  The guest
// can tell the host (via the PTF instruction) whether the CPUs should
// be provisioned using horizontal or vertical polarization.  On
// horizontal polarization the host is expected to provision all vCPUs
// equally.  On vertical polarization the host can provision each vCPU
// differently.  The guest will get information on the details of the
// provisioning the next time it uses the STSI(15) instruction.
//
// Since: 8.2
//
// .. qmp-example::    <- { "event": "CPU_POLARIZATION_CHANGE",
// "data": { "polarization": "horizontal" },      "timestamp": {
// "seconds": 1401385907, "microseconds": 422329 } }
type CpuPolarizationChangeEvent struct {
	// polarization specified by the guest
	Polarization S390CpuPolarization `json:"polarization"`
}

// Emitted whenever the device removal completion is acknowledged by
// the guest.  At this point, it's safe to reuse the specified device
// ID.  Device removal can be initiated by the guest or by HMP/QMP
// commands.
//
// Since: 1.5
//
// .. qmp-example::    <- { "event": "DEVICE_DELETED",      "data": {
// "device": "virtio-net-pci-0",           "path":
// "/machine/peripheral/virtio-net-pci-0" },      "timestamp": {
// "seconds": 1265044230, "microseconds": 450486 } }
type DeviceDeletedEvent struct {
	// the device's ID if it has one
	Device *string `json:"device,omitempty"`
	// the device's QOM path
	Path string `json:"path"`
}

// Emitted whenever the tray of a removable device is moved by the
// guest or by HMP/QMP commands
//
// Since: 1.1
//
// .. qmp-example::    <- { "event": "DEVICE_TRAY_MOVED",      "data":
// { "device": "ide1-cd0",           "id":
// "/machine/unattached/device[22]",           "tray-open": true
// },      "timestamp": { "seconds": 1265044230, "microseconds":
// 450486 } }
type DeviceTrayMovedEvent struct {
	// Block device name. This is always present for compatibility
	// reasons, but it can be empty ("") if the image does not have a
	// device name associated.
	Device string `json:"device"`
	// The name or QOM path of the guest device (since 2.8)
	Id string `json:"id"`
	// true if the tray has been opened or false if it has been closed
	TrayOpen bool `json:"tray-open"`
}

// Emitted when a device hot unplug fails due to a guest reported
// error.
//
// Since: 6.2
//
// .. qmp-example::    <- { "event": "DEVICE_UNPLUG_GUEST_ERROR",
// "data": { "device": "core1",           "path":
// "/machine/peripheral/core1" },      "timestamp": { "seconds":
// 1615570772, "microseconds": 202844 } }
type DeviceUnplugGuestErrorEvent struct {
	// the device's ID if it has one
	Device *string `json:"device,omitempty"`
	// the device's QOM path
	Path string `json:"path"`
}

// Emitted when background dump has completed
//
// Since: 2.6
//
// .. qmp-example::    <- { "event": "DUMP_COMPLETED",      "data": {
// "result": { "total": 1090650112, "status": "completed",
// "completed": 1090650112 } },      "timestamp": { "seconds":
// 1648244171, "microseconds": 950316 } }
type DumpCompletedEvent struct {
	// final dump status
	Result DumpQueryResult `json:"result"`
	// human-readable error string that provides hint on why dump
	// failed. Only presents on failure. The user should not try to
	// interpret the error string.
	Error *string `json:"error,omitempty"`
}

// Emitted when VIRTIO_NET_F_STANDBY was enabled during feature
// negotiation.  Failover primary devices which were hidden (not
// hotplugged when requested) before will now be hotplugged by the
// virtio-net standby device.
//
// Since: 4.2
//
// .. qmp-example::    <- { "event": "FAILOVER_NEGOTIATED",
// "data": { "device-id": "net1" },      "timestamp": { "seconds":
// 1368697518, "microseconds": 326866 } }
type FailoverNegotiatedEvent struct {
	// QEMU device id of the unplugged device
	DeviceId string `json:"device-id"`
}

// Emitted when guest OS crash loaded is detected
//
// Since: 5.0
//
// .. qmp-example::    <- { "event": "GUEST_CRASHLOADED",      "data":
// { "action": "run" },      "timestamp": { "seconds": 1648245259,
// "microseconds": 893771 } }
type GuestCrashloadedEvent struct {
	// action that has been taken, currently always "run"
	Action GuestPanicAction `json:"action"`
	// information about a panic
	Info *GuestPanicInformation `json:"info,omitempty"`
}

// Emitted when guest OS panic is detected
//
// Since: 1.5
//
// .. qmp-example::    <- { "event": "GUEST_PANICKED",      "data": {
// "action": "pause" },      "timestamp": { "seconds": 1648245231,
// "microseconds": 900001 } }
type GuestPanickedEvent struct {
	// action that has been taken, currently always "pause"
	Action GuestPanicAction `json:"action"`
	// information about a panic (since 2.9)
	Info *GuestPanicInformation `json:"info,omitempty"`
}

// Emitted when guest submits a shutdown request via pvpanic interface
//
// Since: 9.1
//
// .. qmp-example::    <- { "event": "GUEST_PVSHUTDOWN",
// "timestamp": { "seconds": 1648245259, "microseconds": 893771 } }
type GuestPvshutdownEvent struct{}

// Emitted when the hv-balloon driver receives a "STATUS" message from
// the guest.  .. note:: This event is rate-limited.
//
// Since: 8.2
//
// .. qmp-example::    <- { "event": "HV_BALLOON_STATUS_REPORT",
// "data": { "committed": 816640000, "available": 3333054464 },
// "timestamp": { "seconds": 1600295492, "microseconds": 661044 } }
type HvBalloonStatusReportEvent struct {
	Committed uint64 `json:"committed"`
	Available uint64 `json:"available"`
}

// Emitted when a job transitions to a different status.
//
// Since: 3.0
type JobStatusChangeEvent struct {
	// The job identifier
	Id string `json:"id"`
	// The new job status
	Status JobStatus `json:"status"`
}

// Emitted when the size of a memory device changes.  Only emitted for
// memory devices that can actually change the size (e.g., virtio-mem
// due to guest action).
//
// .. note:: This event is rate-limited.
//
// Since: 5.1
//
// .. qmp-example::    <- { "event": "MEMORY_DEVICE_SIZE_CHANGE",
// "data": { "id": "vm0", "size": 1073741824,           "qom-path":
// "/machine/unattached/device[2]" },      "timestamp": { "seconds":
// 1588168529, "microseconds": 201316 } }
type MemoryDeviceSizeChangeEvent struct {
	// device's ID
	Id *string `json:"id,omitempty"`
	// the new size of memory that the device provides
	Size uint64 `json:"size"`
	// path to the device object in the QOM tree (since 6.2)
	QomPath string `json:"qom-path"`
}

// Emitted when a memory failure occurs on host side.
//
// Since: 5.2
//
// .. qmp-example::    <- { "event": "MEMORY_FAILURE",      "data": {
// "recipient": "hypervisor",           "action": "fatal",
// "flags": { "action-required": false,                "recursive":
// false } },      "timestamp": { "seconds": 1267061043,
// "microseconds": 959568 } }
type MemoryFailureEvent struct {
	// recipient is defined as @MemoryFailureRecipient.
	Recipient MemoryFailureRecipient `json:"recipient"`
	// action that has been taken.
	Action MemoryFailureAction `json:"action"`
	// flags for MemoryFailureAction.
	Flags MemoryFailureFlags `json:"flags"`
}

// Emitted when a migration event happens
//
// Since: 2.4
//
// .. qmp-example::    <- {"timestamp": {"seconds": 1432121972,
// "microseconds": 744001},     "event": "MIGRATION",     "data":
// {"status": "completed"} }
type MigrationEvent struct {
	// @MigrationStatus describing the current migration status.
	Status MigrationStatus `json:"status"`
}

// Emitted from the source side of a migration at the start of each
// pass (when it syncs the dirty bitmap)
//
// Since: 2.6
//
// .. qmp-example::    <- { "timestamp": {"seconds": 1449669631,
// "microseconds": 239225},      "event": "MIGRATION_PASS", "data":
// {"pass": 2} }
type MigrationPassEvent struct {
	// An incrementing count (starting at 1 on the first pass)
	Pass int64 `json:"pass"`
}

// Emitted when the netdev stream backend is connected
//
// Since: 7.2
//
// .. qmp-example::    <- { "event": "NETDEV_STREAM_CONNECTED",
// "data": { "netdev-id": "netdev0",           "addr": { "port":
// "47666", "ipv6": true,                "host": "::1", "type": "inet"
// } },      "timestamp": { "seconds": 1666269863, "microseconds":
// 311222 } }  .. qmp-example::    <- { "event":
// "NETDEV_STREAM_CONNECTED",      "data": { "netdev-id": "netdev0",
// "addr": { "path": "/tmp/qemu0", "type": "unix" } },
// "timestamp": { "seconds": 1666269706, "microseconds": 413651 } }
type NetdevStreamConnectedEvent struct {
	// QEMU netdev id that is connected
	NetdevId string `json:"netdev-id"`
	// The destination address
	Addr SocketAddress `json:"addr"`
}

// Emitted when the netdev stream backend is disconnected
//
// Since: 7.2
//
// .. qmp-example::    <- { "event": "NETDEV_STREAM_DISCONNECTED",
// "data": {"netdev-id": "netdev0"},      "timestamp": {"seconds":
// 1663330937, "microseconds": 526695} }
type NetdevStreamDisconnectedEvent struct {
	// QEMU netdev id that is disconnected
	NetdevId string `json:"netdev-id"`
}

// Emitted once until the 'query-rx-filter' command is executed, the
// first event will always be emitted
//
// Since: 1.6
//
// .. qmp-example::    <- { "event": "NIC_RX_FILTER_CHANGED",
// "data": { "name": "vnet0",           "path":
// "/machine/peripheral/vnet0/virtio-backend" },      "timestamp": {
// "seconds": 1368697518, "microseconds": 326866 } }
type NicRxFilterChangedEvent struct {
	// net client name
	Name *string `json:"name,omitempty"`
	// device path
	Path string `json:"path"`
}

// Emitted when the virtual machine is powered down through the power
// control system, such as via ACPI.
//
// Since: 0.12
//
// .. qmp-example::    <- { "event": "POWERDOWN",      "timestamp": {
// "seconds": 1267040730, "microseconds": 682951 } }
type PowerdownEvent struct{}

// Emitted whenever the connected status of a persistent reservation
// manager changes.
//
// Since: 3.0
//
// .. qmp-example::    <- { "event": "PR_MANAGER_STATUS_CHANGED",
// "data": { "id": "pr-helper0",           "connected": true      },
// "timestamp": { "seconds": 1519840375, "microseconds": 450486 } }
type PrManagerStatusChangedEvent struct {
	// The id of the PR manager object
	Id string `json:"id"`
	// true if the PR manager is connected to a backend
	Connected bool `json:"connected"`
}

// Emitted by the Quorum block driver if it fails to establish a
// quorum
//
// .. note:: This event is rate-limited.
//
// Since: 2.0
//
// .. qmp-example::    <- { "event": "QUORUM_FAILURE",      "data": {
// "reference": "usr1", "sector-num": 345435, "sectors-count": 5 },
// "timestamp": { "seconds": 1344522075, "microseconds": 745528 } }
type QuorumFailureEvent struct {
	// device name if defined else node name
	Reference string `json:"reference"`
	// number of the first sector of the failed read operation
	SectorNum int64 `json:"sector-num"`
	// failed read operation sector count
	SectorsCount int64 `json:"sectors-count"`
}

// Emitted to report a corruption of a Quorum file
//
// .. note:: This event is rate-limited.
//
// Since: 2.0
//
// .. qmp-example::   :title: Read operation    <- { "event":
// "QUORUM_REPORT_BAD",      "data": { "node-name": "node0", "sector-
// num": 345435, "sectors-count": 5,           "type": "read" },
// "timestamp": { "seconds": 1344522075, "microseconds": 745528 } }
// .. qmp-example::   :title: Flush operation    <- { "event":
// "QUORUM_REPORT_BAD",      "data": { "node-name": "node0", "sector-
// num": 0, "sectors-count": 2097120,           "type": "flush",
// "error": "Broken pipe" },      "timestamp": { "seconds":
// 1456406829, "microseconds": 291763 } }
type QuorumReportBadEvent struct {
	// quorum operation type (Since 2.6)
	Type QuorumOpType `json:"type"`
	// error message. Only present on failure. This field contains a
	// human-readable error message. There are no semantics other than
	// that the block layer reported an error and clients should not
	// try to interpret the error string.
	Error *string `json:"error,omitempty"`
	// the graph node name of the block driver state
	NodeName string `json:"node-name"`
	// number of the first sector of the failed read operation
	SectorNum int64 `json:"sector-num"`
	// failed read operation sector count
	SectorsCount int64 `json:"sectors-count"`
}

// Emitted when the virtual machine is reset
//
// Since: 0.12
//
// .. qmp-example::    <- { "event": "RESET",      "data": { "guest":
// false, "reason": "guest-reset" },      "timestamp": { "seconds":
// 1267041653, "microseconds": 9518 } }
type ResetEvent struct {
	// If true, the reset was triggered by a guest request (such as a
	// guest-initiated ACPI reboot request or other hardware-specific
	// action) rather than a host request (such as the QMP command
	// system_reset). (since 2.10)
	Guest bool `json:"guest"`
	// The @ShutdownCause of the RESET. (since 4.0)
	Reason ShutdownCause `json:"reason"`
}

// Emitted when the virtual machine resumes execution
//
// Since: 0.12
//
// .. qmp-example::    <- { "event": "RESUME",      "timestamp": {
// "seconds": 1271770767, "microseconds": 582542 } }
type ResumeEvent struct{}

// Emitted when the guest changes the RTC time.
//
// .. note:: This event is rate-limited. It is not guaranteed that the
// RTC in the system implements this event, or even that the system
// has an RTC at all.
//
// Since: 0.13
//
// .. qmp-example::    <- { "event": "RTC_CHANGE",      "data": {
// "offset": 78 },      "timestamp": { "seconds": 1267020223,
// "microseconds": 435656 } }
type RtcChangeEvent struct {
	// offset in seconds between base RTC clock (as specified by -rtc
	// base), and new RTC clock value
	Offset int64 `json:"offset"`
	// path to the RTC object in the QOM tree
	QomPath string `json:"qom-path"`
}

// Emitted when the virtual machine has shut down, indicating that
// qemu is about to exit.
//
// .. note:: If the command-line option "-no-shutdown" has been
// specified, qemu will not exit, and a STOP event will eventually
// follow the SHUTDOWN event.
//
// Since: 0.12
//
// .. qmp-example::    <- { "event": "SHUTDOWN",      "data": {
// "guest": true, "reason": "guest-shutdown" },      "timestamp": {
// "seconds": 1267040730, "microseconds": 682951 } }
type ShutdownEvent struct {
	// If true, the shutdown was triggered by a guest request (such as
	// a guest-initiated ACPI shutdown request or other hardware-
	// specific action) rather than a host request (such as sending
	// qemu a SIGINT). (since 2.10)
	Guest bool `json:"guest"`
	// The @ShutdownCause which resulted in the SHUTDOWN. (since 4.0)
	Reason ShutdownCause `json:"reason"`
}

// Emitted when a SPICE client establishes a connection
//
// Since: 0.14
//
// .. qmp-example::    <- { "timestamp": {"seconds": 1290688046,
// "microseconds": 388707},      "event": "SPICE_CONNECTED",
// "data": {       "server": { "port": "5920", "family": "ipv4",
// "host": "127.0.0.1"},       "client": {"port": "52873", "family":
// "ipv4", "host": "127.0.0.1"}     }}
type SpiceConnectedEvent struct {
	// server information
	Server SpiceBasicInfo `json:"server"`
	// client information
	Client SpiceBasicInfo `json:"client"`
}

// Emitted when the SPICE connection is closed
//
// Since: 0.14
//
// .. qmp-example::    <- { "timestamp": {"seconds": 1290688046,
// "microseconds": 388707},      "event": "SPICE_DISCONNECTED",
// "data": {       "server": { "port": "5920", "family": "ipv4",
// "host": "127.0.0.1"},       "client": {"port": "52873", "family":
// "ipv4", "host": "127.0.0.1"}     }}
type SpiceDisconnectedEvent struct {
	// server information
	Server SpiceBasicInfo `json:"server"`
	// client information
	Client SpiceBasicInfo `json:"client"`
}

// Emitted after initial handshake and authentication takes place (if
// any) and the SPICE channel is up and running
//
// Since: 0.14
//
// .. qmp-example::    <- { "timestamp": {"seconds": 1290688046,
// "microseconds": 417172},      "event": "SPICE_INITIALIZED",
// "data": {"server": {"auth": "spice", "port": "5921",
// "family": "ipv4", "host": "127.0.0.1"},          "client": {"port":
// "49004", "family": "ipv4", "channel-type": 3,
// "connection-id": 1804289383, "host": "127.0.0.1",
// "channel-id": 0, "tls": true}     }}
type SpiceInitializedEvent struct {
	// server information
	Server SpiceServerInfo `json:"server"`
	// client information
	Client SpiceChannel `json:"client"`
}

// Emitted when SPICE migration has completed
//
// Since: 1.3
//
// .. qmp-example::    <- { "timestamp": {"seconds": 1290688046,
// "microseconds": 417172},      "event": "SPICE_MIGRATE_COMPLETED" }
type SpiceMigrateCompletedEvent struct{}

// Emitted when the virtual machine is stopped
//
// Since: 0.12
//
// .. qmp-example::    <- { "event": "STOP",      "timestamp": {
// "seconds": 1267041730, "microseconds": 281295 } }
type StopEvent struct{}

// Emitted when guest enters a hardware suspension state, for example,
// S3 state, which is sometimes called standby state
//
// Since: 1.1
//
// .. qmp-example::    <- { "event": "SUSPEND",      "timestamp": {
// "seconds": 1344456160, "microseconds": 309119 } }
type SuspendEvent struct{}

// Emitted when guest enters a hardware suspension state with data
// saved on disk, for example, S4 state, which is sometimes called
// hibernate state  .. note:: QEMU shuts down (similar to event
// @SHUTDOWN) when entering    this state.
//
// Since: 1.2
//
// .. qmp-example::    <- { "event": "SUSPEND_DISK",      "timestamp":
// { "seconds": 1344456160, "microseconds": 309119 } }
type SuspendDiskEvent struct{}

// Emitted from source side of a migration when migration state is
// WAIT_UNPLUG.  Device was unplugged by guest operating system.
// Device resources in QEMU are kept on standby to be able to re-plug
// it in case of migration failure.
//
// Since: 4.2
//
// .. qmp-example::    <- { "event": "UNPLUG_PRIMARY",      "data": {
// "device-id": "hostdev0" },      "timestamp": { "seconds":
// 1265044230, "microseconds": 450486 } }
type UnplugPrimaryEvent struct {
	// QEMU device id of the unplugged device
	DeviceId string `json:"device-id"`
}

// This event is emitted when a VFIO device migration state is
// changed.
//
// Since: 9.1
//
// .. qmp-example::    <- { "timestamp": { "seconds": 1713771323,
// "microseconds": 212268 },      "event": "VFIO_MIGRATION",
// "data": {        "device-id": "vfio_dev1",        "qom-path":
// "/machine/peripheral/vfio_dev1",        "device-state": "stop" } }
type VfioMigrationEvent struct {
	// The device's id, if it has one.
	DeviceId string `json:"device-id"`
	// The device's QOM path.
	QomPath string `json:"qom-path"`
	// The new changed device migration state.
	DeviceState QapiVfioMigrationState `json:"device-state"`
}

// Emitted when the client of a TYPE_VFIO_USER_SERVER closes the
// communication channel
//
// Since: 7.1
//
// .. qmp-example::    <- { "event": "VFU_CLIENT_HANGUP",      "data":
// { "vfu-id": "vfu1",           "vfu-qom-path": "/objects/vfu1",
// "dev-id": "sas1",           "dev-qom-path":
// "/machine/peripheral/sas1" },      "timestamp": { "seconds":
// 1265044230, "microseconds": 450486 } }
type VfuClientHangupEvent struct {
	// ID of the TYPE_VFIO_USER_SERVER object. It is the last
	// component of @vfu-qom-path referenced below
	VfuId string `json:"vfu-id"`
	// path to the TYPE_VFIO_USER_SERVER object in the QOM tree
	VfuQomPath string `json:"vfu-qom-path"`
	// ID of attached PCI device
	DevId string `json:"dev-id"`
	// path to attached PCI device in the QOM tree
	DevQomPath string `json:"dev-qom-path"`
}

// Emitted when a VNC client establishes a connection
//
// .. note:: This event is emitted before any authentication takes
// place, thus the authentication ID is not provided.
//
// Since: 0.13
//
// .. qmp-example::    <- { "event": "VNC_CONNECTED",      "data": {
// "server": { "auth": "sasl", "family": "ipv4", "websocket": false,
// "service": "5901", "host": "0.0.0.0" },         "client": {
// "family": "ipv4", "service": "58425",               "host":
// "127.0.0.1", "websocket": false } },      "timestamp": { "seconds":
// 1262976601, "microseconds": 975795 } }
type VncConnectedEvent struct {
	// server information
	Server VncServerInfo `json:"server"`
	// client information
	Client VncBasicInfo `json:"client"`
}

// Emitted when the connection is closed
//
// Since: 0.13
//
// .. qmp-example::    <- { "event": "VNC_DISCONNECTED",      "data":
// {         "server": { "auth": "sasl", "family": "ipv4",
// "websocket": false,               "service": "5901", "host":
// "0.0.0.0" },         "client": { "family": "ipv4", "service":
// "58425", "websocket": false,               "host": "127.0.0.1",
// "sasl_username": "luiz" } },      "timestamp": { "seconds":
// 1262976601, "microseconds": 975795 } }
type VncDisconnectedEvent struct {
	// server information
	Server VncServerInfo `json:"server"`
	// client information
	Client VncClientInfo `json:"client"`
}

// Emitted after authentication takes place (if any) and the VNC
// session is made active
//
// Since: 0.13
//
// .. qmp-example::    <- { "event": "VNC_INITIALIZED",      "data": {
// "server": { "auth": "sasl", "family": "ipv4", "websocket": false,
// "service": "5901", "host": "0.0.0.0"},         "client": {
// "family": "ipv4", "service": "46089", "websocket": false,
// "host": "127.0.0.1", "sasl_username": "luiz" } },      "timestamp":
// { "seconds": 1263475302, "microseconds": 150772 } }
type VncInitializedEvent struct {
	// server information
	Server VncServerInfo `json:"server"`
	// client information
	Client VncClientInfo `json:"client"`
}

// Emitted when the guest opens or closes a virtio-serial port.
//
// .. note:: This event is rate-limited.
//
// Since: 2.1
//
// .. qmp-example::    <- { "event": "VSERPORT_CHANGE",      "data": {
// "id": "channel0", "open": true },      "timestamp": { "seconds":
// 1401385907, "microseconds": 422329 } }
type VserportChangeEvent struct {
	// device identifier of the virtio-serial port
	Id string `json:"id"`
	// true if the guest has opened the virtio-serial port
	Open bool `json:"open"`
}

// Emitted when the guest has woken up from suspend state and is
// running
//
// Since: 1.1
//
// .. qmp-example::    <- { "event": "WAKEUP",      "timestamp": {
// "seconds": 1344522075, "microseconds": 745528 } }
type WakeupEvent struct{}

// Emitted when the watchdog device's timer is expired
//
// .. note:: If action is "reset", "shutdown", or "pause" the WATCHDOG
// event is followed respectively by the RESET, SHUTDOWN, or STOP
// events.  .. note:: This event is rate-limited.
//
// Since: 0.13
//
// .. qmp-example::    <- { "event": "WATCHDOG",      "data": {
// "action": "reset" },      "timestamp": { "seconds": 1267061043,
// "microseconds": 959568 } }
type WatchdogEvent struct {
	// action that has been taken
	Action WatchdogAction `json:"action"`
}
