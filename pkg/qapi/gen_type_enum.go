/*
 * Copyright 2025 Red Hat, Inc.
 * SPDX-License-Identifier: (MIT-0 and GPL-2.0-or-later)
 */

/****************************************************************************
 * THIS CODE HAS BEEN GENERATED. DO NOT CHANGE IT DIRECTLY                  *
 ****************************************************************************/
package qapi

type ACPISlotType string

const (
	// memory slot
	ACPISlotTypeDimm ACPISlotType = "DIMM"
	// logical CPU slot (since 2.7)
	ACPISlotTypeCpu ACPISlotType = "CPU"
)

// Attach mode for a default XDP program
//
// Since: 8.2
type AFXDPMode string

const (
	// DRV mode, program is attached to a driver, packets are passed to the socket
	// without allocation of skb.
	AFXDPModeNative AFXDPMode = "native"
	// generic mode, no driver support necessary
	AFXDPModeSkb AFXDPMode = "skb"
)

// An enumeration of Transactional completion modes.
//
// Since: 2.5
type ActionCompletionMode string

const (
	// Do not attempt to cancel any other Actions if any Actions fail after the
	// Transaction request succeeds. All Actions that can complete successfully will do
	// so without waiting on others. This is the default.
	ActionCompletionModeIndividual ActionCompletionMode = "individual"
	// If any Action fails after the Transaction succeeds, cancel all Actions. Actions
	// do not complete until all Actions are ready to complete. May be rejected by
	// Actions that do not support this completion mode.
	ActionCompletionModeGrouped ActionCompletionMode = "grouped"
)

// An enumeration of possible audio formats.
//
// Since: 4.0
type AudioFormat string

const (
	// unsigned 8 bit integer
	AudioFormatU8 AudioFormat = "u8"
	// signed 8 bit integer
	AudioFormatS8 AudioFormat = "s8"
	// unsigned 16 bit integer
	AudioFormatU16 AudioFormat = "u16"
	// signed 16 bit integer
	AudioFormatS16 AudioFormat = "s16"
	// unsigned 32 bit integer
	AudioFormatU32 AudioFormat = "u32"
	// signed 32 bit integer
	AudioFormatS32 AudioFormat = "s32"
	// single precision floating-point (since 5.0)
	AudioFormatF32 AudioFormat = "f32"
)

// An enumeration of possible audio backend drivers.
//
// Since: 4.0
type AudiodevDriver string

const (
	AudiodevDriverNone      AudiodevDriver = "none"
	AudiodevDriverAlsa      AudiodevDriver = "alsa"
	AudiodevDriverCoreaudio AudiodevDriver = "coreaudio"
	AudiodevDriverDbus      AudiodevDriver = "dbus"
	AudiodevDriverDsound    AudiodevDriver = "dsound"
	// JACK audio backend (since 5.1)
	AudiodevDriverJack     AudiodevDriver = "jack"
	AudiodevDriverOss      AudiodevDriver = "oss"
	AudiodevDriverPa       AudiodevDriver = "pa"
	AudiodevDriverPipewire AudiodevDriver = "pipewire"
	AudiodevDriverSdl      AudiodevDriver = "sdl"
	AudiodevDriverSndio    AudiodevDriver = "sndio"
	AudiodevDriverSpice    AudiodevDriver = "spice"
	AudiodevDriverWav      AudiodevDriver = "wav"
)

// Policy that BIOS should use to interpret cylinder/head/sector
// addresses.  Note that Bochs BIOS and SeaBIOS will not actually
// translate logical CHS to physical; instead, they will use logical
// block addressing.
//
// Since: 2.0
type BiosAtaTranslation string

const (
	// If cylinder/heads/sizes are passed, choose between none and LBA depending on the
	// size of the disk. If they are not passed, choose none if QEMU can guess that the
	// disk had 16 or fewer heads, large if QEMU can guess that the disk had 131072 or
	// fewer tracks across all heads (i.e. cylinders*heads<131072), otherwise LBA.
	BiosAtaTranslationAuto BiosAtaTranslation = "auto"
	// The physical disk geometry is equal to the logical geometry.
	BiosAtaTranslationNone BiosAtaTranslation = "none"
	// Assume 63 sectors per track and one of 16, 32, 64, 128 or 255 heads (if fewer
	// than 255 are enough to cover the whole disk with 1024 cylinders/head). The
	// number of cylinders/head is then computed based on the number of sectors and
	// heads.
	BiosAtaTranslationLba BiosAtaTranslation = "lba"
	// The number of cylinders per head is scaled down to 1024 by correspondingly
	// scaling up the number of heads.
	BiosAtaTranslationLarge BiosAtaTranslation = "large"
	// Same as @large, but first convert a 16-head geometry to 15-head, by
	// proportionally scaling up the number of cylinders/head.
	BiosAtaTranslationRechs BiosAtaTranslation = "rechs"
)

// An enumeration of possible behaviors for the synchronization of a
// bitmap when used for data copy operations.
//
// Since: 4.2
type BitmapSyncMode string

const (
	// The bitmap is only synced when the operation is successful. This is the behavior
	// always used for 'INCREMENTAL' backups.
	BitmapSyncModeOnSuccess BitmapSyncMode = "on-success"
	// The bitmap is never synchronized with the operation, and is treated solely as a
	// read-only manifest of blocks to copy.
	BitmapSyncModeNever BitmapSyncMode = "never"
	// The bitmap is always synchronized with the operation, regardless of whether or
	// not the operation was successful.
	BitmapSyncModeAlways BitmapSyncMode = "always"
)

// Trigger events supported by blkdebug.
//
// Since: 2.9
type BlkdebugEvent string

const (
	BlkdebugEventL1_Update                   BlkdebugEvent = "l1_update"
	BlkdebugEventL1_Grow_Alloc_Table         BlkdebugEvent = "l1_grow_alloc_table"
	BlkdebugEventL1_Grow_Write_Table         BlkdebugEvent = "l1_grow_write_table"
	BlkdebugEventL1_Grow_Activate_Table      BlkdebugEvent = "l1_grow_activate_table"
	BlkdebugEventL2_Load                     BlkdebugEvent = "l2_load"
	BlkdebugEventL2_Update                   BlkdebugEvent = "l2_update"
	BlkdebugEventL2_Update_Compressed        BlkdebugEvent = "l2_update_compressed"
	BlkdebugEventL2_Alloc_Cow_Read           BlkdebugEvent = "l2_alloc_cow_read"
	BlkdebugEventL2_Alloc_Write              BlkdebugEvent = "l2_alloc_write"
	BlkdebugEventRead_Aio                    BlkdebugEvent = "read_aio"
	BlkdebugEventRead_Backing_Aio            BlkdebugEvent = "read_backing_aio"
	BlkdebugEventRead_Compressed             BlkdebugEvent = "read_compressed"
	BlkdebugEventWrite_Aio                   BlkdebugEvent = "write_aio"
	BlkdebugEventWrite_Compressed            BlkdebugEvent = "write_compressed"
	BlkdebugEventVmstate_Load                BlkdebugEvent = "vmstate_load"
	BlkdebugEventVmstate_Save                BlkdebugEvent = "vmstate_save"
	BlkdebugEventCow_Read                    BlkdebugEvent = "cow_read"
	BlkdebugEventCow_Write                   BlkdebugEvent = "cow_write"
	BlkdebugEventReftable_Load               BlkdebugEvent = "reftable_load"
	BlkdebugEventReftable_Grow               BlkdebugEvent = "reftable_grow"
	BlkdebugEventReftable_Update             BlkdebugEvent = "reftable_update"
	BlkdebugEventRefblock_Load               BlkdebugEvent = "refblock_load"
	BlkdebugEventRefblock_Update             BlkdebugEvent = "refblock_update"
	BlkdebugEventRefblock_Update_Part        BlkdebugEvent = "refblock_update_part"
	BlkdebugEventRefblock_Alloc              BlkdebugEvent = "refblock_alloc"
	BlkdebugEventRefblock_Alloc_Hookup       BlkdebugEvent = "refblock_alloc_hookup"
	BlkdebugEventRefblock_Alloc_Write        BlkdebugEvent = "refblock_alloc_write"
	BlkdebugEventRefblock_Alloc_Write_Blocks BlkdebugEvent = "refblock_alloc_write_blocks"
	BlkdebugEventRefblock_Alloc_Write_Table  BlkdebugEvent = "refblock_alloc_write_table"
	BlkdebugEventRefblock_Alloc_Switch_Table BlkdebugEvent = "refblock_alloc_switch_table"
	BlkdebugEventCluster_Alloc               BlkdebugEvent = "cluster_alloc"
	BlkdebugEventCluster_Alloc_Bytes         BlkdebugEvent = "cluster_alloc_bytes"
	BlkdebugEventCluster_Free                BlkdebugEvent = "cluster_free"
	BlkdebugEventFlush_To_Os                 BlkdebugEvent = "flush_to_os"
	BlkdebugEventFlush_To_Disk               BlkdebugEvent = "flush_to_disk"
	BlkdebugEventPwritev_Rmw_Head            BlkdebugEvent = "pwritev_rmw_head"
	BlkdebugEventPwritev_Rmw_After_Head      BlkdebugEvent = "pwritev_rmw_after_head"
	BlkdebugEventPwritev_Rmw_Tail            BlkdebugEvent = "pwritev_rmw_tail"
	BlkdebugEventPwritev_Rmw_After_Tail      BlkdebugEvent = "pwritev_rmw_after_tail"
	BlkdebugEventPwritev                     BlkdebugEvent = "pwritev"
	BlkdebugEventPwritev_Zero                BlkdebugEvent = "pwritev_zero"
	BlkdebugEventPwritev_Done                BlkdebugEvent = "pwritev_done"
	BlkdebugEventEmpty_Image_Prepare         BlkdebugEvent = "empty_image_prepare"
	// write zeros to the l1 table to shrink image. (since 2.11)
	BlkdebugEventL1_Shrink_Write_Table BlkdebugEvent = "l1_shrink_write_table"
	// discard the l2 tables. (since 2.11)
	BlkdebugEventL1_Shrink_Free_L2_Clusters BlkdebugEvent = "l1_shrink_free_l2_clusters"
	// a write due to copy-on-read (since 2.11)
	BlkdebugEventCor_Write BlkdebugEvent = "cor_write"
	// an allocation of file space for a cluster (since 4.1)
	BlkdebugEventCluster_Alloc_Space BlkdebugEvent = "cluster_alloc_space"
	// triggers once at creation of the blkdebug node (since 4.1)
	BlkdebugEventNone BlkdebugEvent = "none"
)

// Kinds of I/O that blkdebug can inject errors in.
//
// Since: 4.1
type BlkdebugIOType string

const (
	// .bdrv_co_preadv()
	BlkdebugIOTypeRead BlkdebugIOType = "read"
	// .bdrv_co_pwritev()
	BlkdebugIOTypeWrite BlkdebugIOType = "write"
	// .bdrv_co_pwrite_zeroes()
	BlkdebugIOTypeWriteZeroes BlkdebugIOType = "write-zeroes"
	// .bdrv_co_pdiscard()
	BlkdebugIOTypeDiscard BlkdebugIOType = "discard"
	// .bdrv_co_flush_to_disk()
	BlkdebugIOTypeFlush BlkdebugIOType = "flush"
	// .bdrv_co_block_status()
	BlkdebugIOTypeBlockStatus BlkdebugIOType = "block-status"
)

// An enumeration of block device I/O status.
//
// Since: 1.0
type BlockDeviceIoStatus string

const (
	// The last I/O operation has succeeded
	BlockDeviceIoStatusOk BlockDeviceIoStatus = "ok"
	// The last I/O operation has failed
	BlockDeviceIoStatusFailed BlockDeviceIoStatus = "failed"
	// The last I/O operation has failed due to a no-space condition
	BlockDeviceIoStatusNospace BlockDeviceIoStatus = "nospace"
)

// An enumeration of action that has been taken when a DISK I/O occurs
//
// Since: 2.1
type BlockErrorAction string

const (
	// error has been ignored
	BlockErrorActionIgnore BlockErrorAction = "ignore"
	// error has been reported to the device
	BlockErrorActionReport BlockErrorAction = "report"
	// error caused VM to be stopped
	BlockErrorActionStop BlockErrorAction = "stop"
)

// Mode for removing a block export.
//
// Since: 2.12
type BlockExportRemoveMode string

const (
	// Remove export if there are no existing connections, fail otherwise.
	BlockExportRemoveModeSafe BlockExportRemoveMode = "safe"
	// Drop all connections immediately and remove export.
	BlockExportRemoveModeHard BlockExportRemoveMode = "hard"
)

// An enumeration of block export types
//
// Since: 4.2
type BlockExportType string

const (
	// NBD export
	BlockExportTypeNbd BlockExportType = "nbd"
	// vhost-user-blk export (since 5.2)
	BlockExportTypeVhostUserBlk BlockExportType = "vhost-user-blk"
	// FUSE export (since: 6.0)
	BlockExportTypeFuse BlockExportType = "fuse"
	// vduse-blk export (since 7.1)
	BlockExportTypeVduseBlk BlockExportType = "vduse-blk"
)

// Enum of base block permissions.
//
// Since: 4.0
type BlockPermission string

const (
	// A user that has the "permission" of consistent reads is guaranteed that their
	// view of the contents of the block device is complete and self-consistent,
	// representing the contents of a disk at a specific point. For most block devices
	// (including their backing files) this is true, but the property cannot be
	// maintained in a few situations like for intermediate nodes of a commit block
	// job.
	BlockPermissionConsistentRead BlockPermission = "consistent-read"
	// This permission is required to change the visible disk contents.
	BlockPermissionWrite BlockPermission = "write"
	// This permission (which is weaker than BLK_PERM_WRITE) is both enough and
	// required for writes to the block node when the caller promises that the visible
	// disk content doesn't change. As the BLK_PERM_WRITE permission is strictly
	// stronger, either is sufficient to perform an unchanging write.
	BlockPermissionWriteUnchanged BlockPermission = "write-unchanged"
	// This permission is required to change the size of a block node.
	BlockPermissionResize BlockPermission = "resize"
)

// Selects the AIO backend to handle I/O requests
//
// Since: 2.9
type BlockdevAioOptions string

const (
	// Use qemu's thread pool
	BlockdevAioOptionsThreads BlockdevAioOptions = "threads"
	// Use native AIO backend (only Linux and Windows)
	BlockdevAioOptionsNative BlockdevAioOptions = "native"
	// Use linux io_uring (since 5.0)
	BlockdevAioOptionsIo_Uring BlockdevAioOptions = "io_uring"
)

// Specifies the new read-only mode of a block device subject to the
// @blockdev-change-medium command.
//
// Since: 2.3
type BlockdevChangeReadOnlyMode string

const (
	// Retains the current read-only mode
	BlockdevChangeReadOnlyModeRetain BlockdevChangeReadOnlyMode = "retain"
	// Makes the device read-only
	BlockdevChangeReadOnlyModeReadOnly BlockdevChangeReadOnlyMode = "read-only"
	// Makes the device writable
	BlockdevChangeReadOnlyModeReadWrite BlockdevChangeReadOnlyMode = "read-write"
)

// Describes the operation mode for the automatic conversion of plain
// zero writes by the OS to driver specific optimized zero write
// commands.
//
// Since: 2.1
type BlockdevDetectZeroesOptions string

const (
	// Disabled (default)
	BlockdevDetectZeroesOptionsOff BlockdevDetectZeroesOptions = "off"
	// Enabled
	BlockdevDetectZeroesOptionsOn BlockdevDetectZeroesOptions = "on"
	// Enabled and even try to unmap blocks if possible. This requires also that
	// @BlockdevDiscardOptions is set to unmap for this device.
	BlockdevDetectZeroesOptionsUnmap BlockdevDetectZeroesOptions = "unmap"
)

// Determines how to handle discard requests.
//
// Since: 2.9
type BlockdevDiscardOptions string

const (
	// Ignore the request
	BlockdevDiscardOptionsIgnore BlockdevDiscardOptions = "ignore"
	// Forward as an unmap request
	BlockdevDiscardOptionsUnmap BlockdevDiscardOptions = "unmap"
)

// Drivers that are supported in block device operations.
//
// Since: 2.9
type BlockdevDriver string

const (
	BlockdevDriverBlkdebug BlockdevDriver = "blkdebug"
	// Since 3.0
	BlockdevDriverBlklogwrites BlockdevDriver = "blklogwrites"
	// Since 4.2
	BlockdevDriverBlkreplay BlockdevDriver = "blkreplay"
	BlockdevDriverBlkverify BlockdevDriver = "blkverify"
	BlockdevDriverBochs     BlockdevDriver = "bochs"
	BlockdevDriverCloop     BlockdevDriver = "cloop"
	// Since 5.0
	BlockdevDriverCompress BlockdevDriver = "compress"
	// Since 6.2
	BlockdevDriverCopyBeforeWrite BlockdevDriver = "copy-before-write"
	// Since 3.0
	BlockdevDriverCopyOnRead BlockdevDriver = "copy-on-read"
	BlockdevDriverDmg        BlockdevDriver = "dmg"
	BlockdevDriverFile       BlockdevDriver = "file"
	// Since 7.0
	BlockdevDriverSnapshotAccess BlockdevDriver = "snapshot-access"
	BlockdevDriverFtp            BlockdevDriver = "ftp"
	BlockdevDriverFtps           BlockdevDriver = "ftps"
	BlockdevDriverGluster        BlockdevDriver = "gluster"
	BlockdevDriverHost_Cdrom     BlockdevDriver = "host_cdrom"
	BlockdevDriverHost_Device    BlockdevDriver = "host_device"
	BlockdevDriverHttp           BlockdevDriver = "http"
	BlockdevDriverHttps          BlockdevDriver = "https"
	BlockdevDriverIo_Uring       BlockdevDriver = "io_uring"
	BlockdevDriverIscsi          BlockdevDriver = "iscsi"
	BlockdevDriverLuks           BlockdevDriver = "luks"
	BlockdevDriverNbd            BlockdevDriver = "nbd"
	BlockdevDriverNfs            BlockdevDriver = "nfs"
	BlockdevDriverNullAio        BlockdevDriver = "null-aio"
	BlockdevDriverNullCo         BlockdevDriver = "null-co"
	// Since 2.12
	BlockdevDriverNvme         BlockdevDriver = "nvme"
	BlockdevDriverNvmeIo_Uring BlockdevDriver = "nvme-io_uring"
	BlockdevDriverParallels    BlockdevDriver = "parallels"
	BlockdevDriverPreallocate  BlockdevDriver = "preallocate"
	BlockdevDriverQcow         BlockdevDriver = "qcow"
	BlockdevDriverQcow2        BlockdevDriver = "qcow2"
	BlockdevDriverQed          BlockdevDriver = "qed"
	BlockdevDriverQuorum       BlockdevDriver = "quorum"
	BlockdevDriverRaw          BlockdevDriver = "raw"
	BlockdevDriverRbd          BlockdevDriver = "rbd"
	BlockdevDriverReplication  BlockdevDriver = "replication"
	BlockdevDriverSsh          BlockdevDriver = "ssh"
	// Since 2.11
	BlockdevDriverThrottle           BlockdevDriver = "throttle"
	BlockdevDriverVdi                BlockdevDriver = "vdi"
	BlockdevDriverVhdx               BlockdevDriver = "vhdx"
	BlockdevDriverVirtioBlkVfioPci   BlockdevDriver = "virtio-blk-vfio-pci"
	BlockdevDriverVirtioBlkVhostUser BlockdevDriver = "virtio-blk-vhost-user"
	BlockdevDriverVirtioBlkVhostVdpa BlockdevDriver = "virtio-blk-vhost-vdpa"
	BlockdevDriverVmdk               BlockdevDriver = "vmdk"
	BlockdevDriverVpc                BlockdevDriver = "vpc"
	BlockdevDriverVvfat              BlockdevDriver = "vvfat"
)

// An enumeration of possible behaviors for errors on I/O operations.
// The exact meaning depends on whether the I/O was initiated by a
// guest or by a block job
//
// Since: 1.3
type BlockdevOnError string

const (
	// for guest operations, report the error to the guest; for jobs, cancel the job
	BlockdevOnErrorReport BlockdevOnError = "report"
	// ignore the error, only report a QMP event (BLOCK_IO_ERROR or BLOCK_JOB_ERROR).
	// The backup, mirror and commit block jobs retry the failing request later and may
	// still complete successfully. The stream block job continues to stream and will
	// complete with an error.
	BlockdevOnErrorIgnore BlockdevOnError = "ignore"
	// same as @stop on ENOSPC, same as @report otherwise.
	BlockdevOnErrorEnospc BlockdevOnError = "enospc"
	// for guest operations, stop the virtual machine; for jobs, pause the job
	BlockdevOnErrorStop BlockdevOnError = "stop"
	// inherit the error handling policy of the backend (since: 2.7)
	BlockdevOnErrorAuto BlockdevOnError = "auto"
)

// Since: 2.10
type BlockdevQcow2EncryptionFormat string

const (
	// AES-CBC with plain64 initialization vectors
	BlockdevQcow2EncryptionFormatAes  BlockdevQcow2EncryptionFormat = "aes"
	BlockdevQcow2EncryptionFormatLuks BlockdevQcow2EncryptionFormat = "luks"
)

// Since: 2.12
type BlockdevQcow2Version string

const (
	// The original QCOW2 format as introduced in qemu 0.10 (version 2)
	BlockdevQcow2VersionV2 BlockdevQcow2Version = "v2"
	// The extended QCOW2 format as introduced in qemu 1.1 (version 3)
	BlockdevQcow2VersionV3 BlockdevQcow2Version = "v3"
)

// Since: 2.10
type BlockdevQcowEncryptionFormat string

const (
	// AES-CBC with plain64 initialization vectors
	BlockdevQcowEncryptionFormatAes BlockdevQcowEncryptionFormat = "aes"
)

// Since: 2.12
type BlockdevVhdxSubformat string

const (
	// Growing image file
	BlockdevVhdxSubformatDynamic BlockdevVhdxSubformat = "dynamic"
	// Preallocated fixed-size image file
	BlockdevVhdxSubformatFixed BlockdevVhdxSubformat = "fixed"
)

// Adapter type info for VMDK images
//
// Since: 4.0
type BlockdevVmdkAdapterType string

const (
	BlockdevVmdkAdapterTypeIde       BlockdevVmdkAdapterType = "ide"
	BlockdevVmdkAdapterTypeBuslogic  BlockdevVmdkAdapterType = "buslogic"
	BlockdevVmdkAdapterTypeLsilogic  BlockdevVmdkAdapterType = "lsilogic"
	BlockdevVmdkAdapterTypeLegacyesx BlockdevVmdkAdapterType = "legacyESX"
)

// Subformat options for VMDK images
//
// Since: 4.0
type BlockdevVmdkSubformat string

const (
	// Single file image with sparse cluster allocation
	BlockdevVmdkSubformatMonolithicsparse BlockdevVmdkSubformat = "monolithicSparse"
	// Single flat data image and a descriptor file
	BlockdevVmdkSubformatMonolithicflat BlockdevVmdkSubformat = "monolithicFlat"
	// Data is split into 2GB (per virtual LBA) sparse extent files, in addition to a
	// descriptor file
	BlockdevVmdkSubformatTwogbmaxextentsparse BlockdevVmdkSubformat = "twoGbMaxExtentSparse"
	// Data is split into 2GB (per virtual LBA) flat extent files, in addition to a
	// descriptor file
	BlockdevVmdkSubformatTwogbmaxextentflat BlockdevVmdkSubformat = "twoGbMaxExtentFlat"
	// Single file image sparse cluster allocation, optimized for streaming over
	// network.
	BlockdevVmdkSubformatStreamoptimized BlockdevVmdkSubformat = "streamOptimized"
)

// Since: 2.12
type BlockdevVpcSubformat string

const (
	// Growing image file
	BlockdevVpcSubformatDynamic BlockdevVpcSubformat = "dynamic"
	// Preallocated fixed-size image file
	BlockdevVpcSubformatFixed BlockdevVpcSubformat = "fixed"
)

// The reason for a COLO exit.
//
// Since: 3.1
type COLOExitReason string

const (
	// failover has never happened. This state does not occur in the COLO_EXIT event,
	// and is only visible in the result of query-colo-status.
	COLOExitReasonNone COLOExitReason = "none"
	// COLO exit is due to an external request.
	COLOExitReasonRequest COLOExitReason = "request"
	// COLO exit is due to an internal error.
	COLOExitReasonError COLOExitReason = "error"
	// COLO is currently handling a failover (since 4.0).
	COLOExitReasonProcessing COLOExitReason = "processing"
)

// The message transmission between Primary side and Secondary side.
//
// Since: 2.8
type COLOMessage string

const (
	// Secondary VM (SVM) is ready for checkpointing
	COLOMessageCheckpointReady COLOMessage = "checkpoint-ready"
	// Primary VM (PVM) tells SVM to prepare for checkpointing
	COLOMessageCheckpointRequest COLOMessage = "checkpoint-request"
	// SVM gets PVM's checkpoint request
	COLOMessageCheckpointReply COLOMessage = "checkpoint-reply"
	// VM's state will be sent by PVM.
	COLOMessageVmstateSend COLOMessage = "vmstate-send"
	// The total size of VMstate.
	COLOMessageVmstateSize COLOMessage = "vmstate-size"
	// VM's state has been received by SVM.
	COLOMessageVmstateReceived COLOMessage = "vmstate-received"
	// VM's state has been loaded by SVM.
	COLOMessageVmstateLoaded COLOMessage = "vmstate-loaded"
)

// The COLO current mode.
//
// Since: 2.8
type COLOMode string

const (
	// COLO is disabled.
	COLOModeNone COLOMode = "none"
	// COLO node in primary side.
	COLOModePrimary COLOMode = "primary"
	// COLO node in slave side.
	COLOModeSecondary COLOMode = "secondary"
)

// Caches a system may have.  The enumeration value here is the
// combination of cache level and cache type.
//
// Since: 9.2
type CacheLevelAndType string

const (
	// L1 data cache.
	CacheLevelAndTypeL1D CacheLevelAndType = "l1d"
	// L1 instruction cache.
	CacheLevelAndTypeL1I CacheLevelAndType = "l1i"
	// L2 (unified) cache.
	CacheLevelAndTypeL2 CacheLevelAndType = "l2"
	// L3 (unified) cache
	CacheLevelAndTypeL3 CacheLevelAndType = "l3"
)

// Since: 1.4
type ChardevBackendKind string

const (
	// regular files
	ChardevBackendKindFile ChardevBackendKind = "file"
	// serial host device
	ChardevBackendKindSerial ChardevBackendKind = "serial"
	// parallel host device
	ChardevBackendKindParallel ChardevBackendKind = "parallel"
	// pipes (since 1.5)
	ChardevBackendKindPipe ChardevBackendKind = "pipe"
	// stream socket
	ChardevBackendKindSocket ChardevBackendKind = "socket"
	// datagram socket (since 1.5)
	ChardevBackendKindUdp ChardevBackendKind = "udp"
	// pseudo-terminal
	ChardevBackendKindPty ChardevBackendKind = "pty"
	// provides no input, throws away output
	ChardevBackendKindNull ChardevBackendKind = "null"
	// (since 1.5)
	ChardevBackendKindMux ChardevBackendKind = "mux"
	// (since 10.0)
	ChardevBackendKindHub ChardevBackendKind = "hub"
	// emulated Microsoft serial mouse (since 1.5)
	ChardevBackendKindMsmouse ChardevBackendKind = "msmouse"
	// emulated Wacom Penpartner serial tablet (since 2.9)
	ChardevBackendKindWctablet ChardevBackendKind = "wctablet"
	// Baum Braille device (since 1.5)
	ChardevBackendKindBraille ChardevBackendKind = "braille"
	// device for test-suite control (since 2.2)
	ChardevBackendKindTestdev ChardevBackendKind = "testdev"
	// standard I/O (since 1.5)
	ChardevBackendKindStdio ChardevBackendKind = "stdio"
	// Windows console (since 1.5)
	ChardevBackendKindConsole ChardevBackendKind = "console"
	// spice vm channel (since 1.5)
	ChardevBackendKindSpicevmc ChardevBackendKind = "spicevmc"
	// Spice port channel (since 1.5)
	ChardevBackendKindSpiceport ChardevBackendKind = "spiceport"
	// Spice vdagent (since 6.1)
	ChardevBackendKindQemuVdagent ChardevBackendKind = "qemu-vdagent"
	// D-Bus channel (since 7.0)
	ChardevBackendKindDbus ChardevBackendKind = "dbus"
	// virtual console (since 1.5)
	ChardevBackendKindVc ChardevBackendKind = "vc"
	// memory ring buffer (since 1.6)
	ChardevBackendKindRingbuf ChardevBackendKind = "ringbuf"
	// synonym for @ringbuf (since 1.5)
	ChardevBackendKindMemory ChardevBackendKind = "memory"
)

// Possible types for an option parameter.
//
// Since: 1.5
type CommandLineParameterType string

const (
	// accepts a character string
	CommandLineParameterTypeString CommandLineParameterType = "string"
	// accepts "on" or "off"
	CommandLineParameterTypeBoolean CommandLineParameterType = "boolean"
	// accepts a number
	CommandLineParameterTypeNumber CommandLineParameterType = "number"
	// accepts a number followed by an optional suffix (K)ilo, (M)ega, (G)iga, (T)era
	CommandLineParameterTypeSize CommandLineParameterType = "size"
)

// Policy for handling "funny" input.
//
// Since: 6.0
type CompatPolicyInput string

const (
	// Accept silently
	CompatPolicyInputAccept CompatPolicyInput = "accept"
	// Reject with an error
	CompatPolicyInputReject CompatPolicyInput = "reject"
	// abort() the process
	CompatPolicyInputCrash CompatPolicyInput = "crash"
)

// Policy for handling "funny" output.
//
// Since: 6.0
type CompatPolicyOutput string

const (
	// Pass on unchanged
	CompatPolicyOutputAccept CompatPolicyOutput = "accept"
	// Filter out
	CompatPolicyOutputHide CompatPolicyOutput = "hide"
)

// An enumeration of CPU model comparison results.  The result is
// usually calculated using e.g. CPU features or CPU generations.
//
// Since: 2.8
type CpuModelCompareResult string

const (
	// If model A is incompatible to model B, model A is not guaranteed to run where
	// model B runs and the other way around.
	CpuModelCompareResultIncompatible CpuModelCompareResult = "incompatible"
	// If model A is identical to model B, model A is guaranteed to run where model B
	// runs and the other way around.
	CpuModelCompareResultIdentical CpuModelCompareResult = "identical"
	// If model A is a superset of model B, model B is guaranteed to run where model A
	// runs. There are no guarantees about the other way.
	CpuModelCompareResultSuperset CpuModelCompareResult = "superset"
	// If model A is a subset of model B, model A is guaranteed to run where model B
	// runs. There are no guarantees about the other way.
	CpuModelCompareResultSubset CpuModelCompareResult = "subset"
)

// An enumeration of CPU model expansion types.
//
// .. note:: When a non-migration-safe CPU model is expanded in static
// mode, some features enabled by the CPU model may be omitted,
// because they can't be implemented by a static CPU model
// definition (e.g. cache info passthrough and PMU passthrough in
// x86). If you need an accurate representation of the features
// enabled by a non-migration-safe CPU model, use @full. If you   need
// a static representation that will keep ABI compatibility   even
// when changing QEMU version or machine-type, use @static (but   keep
// in mind that some features may be omitted).
//
// Since: 2.8
type CpuModelExpansionType string

const (
	// Expand to a static CPU model, a combination of a static base model name and
	// property delta changes. As the static base model will never change, the expanded
	// CPU model will be the same, independent of QEMU version, machine type, machine
	// options, and accelerator options. Therefore, the resulting model can be used by
	// tooling without having to specify a compatibility machine - e.g. when displaying
	// the "host" model. The @static CPU models are migration-safe.
	CpuModelExpansionTypeStatic CpuModelExpansionType = "static"
	// Expand all properties. The produced model is not guaranteed to be migration-
	// safe, but allows tooling to get an insight and work with model details.
	CpuModelExpansionTypeFull CpuModelExpansionType = "full"
)

// An enumeration of CPU topology levels.
//
// Since: 9.2
type CpuTopologyLevel string

const (
	// thread level, which would also be called SMT level or logical processor level.
	// The @threads option in SMPConfiguration is used to configure the topology of
	// this level.
	CpuTopologyLevelThread CpuTopologyLevel = "thread"
	// core level. The @cores option in SMPConfiguration is used to configure the
	// topology of this level.
	CpuTopologyLevelCore CpuTopologyLevel = "core"
	// module level. The @modules option in SMPConfiguration is used to configure the
	// topology of this level.
	CpuTopologyLevelModule CpuTopologyLevel = "module"
	// cluster level. The @clusters option in SMPConfiguration is used to configure the
	// topology of this level.
	CpuTopologyLevelCluster CpuTopologyLevel = "cluster"
	// die level. The @dies option in SMPConfiguration is used to configure the
	// topology of this level.
	CpuTopologyLevelDie CpuTopologyLevel = "die"
	// socket level, which would also be called package level. The @sockets option in
	// SMPConfiguration is used to configure the topology of this level.
	CpuTopologyLevelSocket CpuTopologyLevel = "socket"
	// book level. The @books option in SMPConfiguration is used to configure the
	// topology of this level.
	CpuTopologyLevelBook CpuTopologyLevel = "book"
	// drawer level. The @drawers option in SMPConfiguration is used to configure the
	// topology of this level.
	CpuTopologyLevelDrawer CpuTopologyLevel = "drawer"
	// default level. Some architectures will have default topology settings (e.g.,
	// cache topology), and this special level means following the architecture-
	// specific settings.
	CpuTopologyLevelDefault CpuTopologyLevel = "default"
)

// Type of CXL correctable error to inject
//
// Since: 8.0
type CxlCorErrorType string

const (
	// Data ECC error on CXL.cache
	CxlCorErrorTypeCacheDataEcc CxlCorErrorType = "cache-data-ecc"
	// Data ECC error on CXL.mem
	CxlCorErrorTypeMemDataEcc CxlCorErrorType = "mem-data-ecc"
	// Component specific and applicable to 68 byte Flit mode only.
	CxlCorErrorTypeCrcThreshold CxlCorErrorType = "crc-threshold"
	// Retry threshold hit in the Local Retry State Machine, 68B Flits only.
	CxlCorErrorTypeRetryThreshold CxlCorErrorType = "retry-threshold"
	// Received poison from a peer on CXL.cache.
	CxlCorErrorTypeCachePoisonReceived CxlCorErrorType = "cache-poison-received"
	// Received poison from a peer on CXL.mem
	CxlCorErrorTypeMemPoisonReceived CxlCorErrorType = "mem-poison-received"
	// Received error indication from the physical layer.
	CxlCorErrorTypePhysical CxlCorErrorType = "physical"
)

// CXL has a number of separate event logs for different types of
// events.  Each such event log is handled and signaled independently.
//
// Since: 8.1
type CxlEventLog string

const (
	// Information Event Log
	CxlEventLogInformational CxlEventLog = "informational"
	// Warning Event Log
	CxlEventLogWarning CxlEventLog = "warning"
	// Failure Event Log
	CxlEventLogFailure CxlEventLog = "failure"
	// Fatal Event Log
	CxlEventLogFatal CxlEventLog = "fatal"
)

// The policy to use for selecting which extents comprise the released
// capacity, defined in the "Flags" field in Compute Express Link
// (CXL) Specification, Revision 3.1, Table 7-71.
//
// Since: 9.1
type CxlExtentRemovalPolicy string

const (
	// Extents are selected by the device based on tag, with no requirement for
	// contiguous extents.
	CxlExtentRemovalPolicyTagBased CxlExtentRemovalPolicy = "tag-based"
	// Extent list of capacity to release is included in the request payload.
	CxlExtentRemovalPolicyPrescriptive CxlExtentRemovalPolicy = "prescriptive"
)

// The policy to use for selecting which extents comprise the added
// capacity, as defined in Compute Express Link (CXL) Specification,
// Revision 3.1, Table 7-70.
//
// Since: 9.1
type CxlExtentSelectionPolicy string

const (
	// Device is responsible for allocating the requested memory capacity and is free
	// to do this using any combination of supported extents.
	CxlExtentSelectionPolicyFree CxlExtentSelectionPolicy = "free"
	// Device is responsible for allocating the requested memory capacity but must do
	// so as a single contiguous extent.
	CxlExtentSelectionPolicyContiguous CxlExtentSelectionPolicy = "contiguous"
	// The precise set of extents to be allocated is specified by the command. Thus
	// allocation is being managed by the issuer of the allocation command, not the
	// device.
	CxlExtentSelectionPolicyPrescriptive CxlExtentSelectionPolicy = "prescriptive"
	// Capacity has already been allocated to a different host using free, contiguous
	// or prescriptive policy with a known tag. This policy then instructs the device
	// to make the capacity with the specified tag available to an additional host.
	// Capacity is implicit as it matches that already associated with the tag. Note
	// that the extent list (and hence Device Physical Addresses) used are per host, so
	// a device may use different representations on each host. The ordering of the
	// extents provided to each host is indicated to the host using per extent sequence
	// numbers generated by the device. Has a similar meaning for temporal sharing, but
	// in that case there may be only one host involved.
	CxlExtentSelectionPolicyEnableSharedAccess CxlExtentSelectionPolicy = "enable-shared-access"
)

// Type of uncorrectable CXL error to inject.  These errors are
// reported via an AER uncorrectable internal error with additional
// information logged at the CXL device.
//
// Since: 8.0
type CxlUncorErrorType string

const (
	// Data error such as data parity or data ECC error CXL.cache
	CxlUncorErrorTypeCacheDataParity CxlUncorErrorType = "cache-data-parity"
	// Address parity or other errors associated with the address field on CXL.cache
	CxlUncorErrorTypeCacheAddressParity CxlUncorErrorType = "cache-address-parity"
	// Byte enable parity or other byte enable errors on CXL.cache
	CxlUncorErrorTypeCacheBeParity CxlUncorErrorType = "cache-be-parity"
	// ECC error on CXL.cache
	CxlUncorErrorTypeCacheDataEcc CxlUncorErrorType = "cache-data-ecc"
	// Data error such as data parity or data ECC error on CXL.mem
	CxlUncorErrorTypeMemDataParity CxlUncorErrorType = "mem-data-parity"
	// Address parity or other errors associated with the address field on CXL.mem
	CxlUncorErrorTypeMemAddressParity CxlUncorErrorType = "mem-address-parity"
	// Byte enable parity or other byte enable errors on CXL.mem.
	CxlUncorErrorTypeMemBeParity CxlUncorErrorType = "mem-be-parity"
	// Data ECC error on CXL.mem.
	CxlUncorErrorTypeMemDataEcc CxlUncorErrorType = "mem-data-ecc"
	// REINIT threshold hit.
	CxlUncorErrorTypeReinitThreshold CxlUncorErrorType = "reinit-threshold"
	// Received unrecognized encoding.
	CxlUncorErrorTypeRsvdEncoding CxlUncorErrorType = "rsvd-encoding"
	// Received poison from the peer.
	CxlUncorErrorTypePoisonReceived CxlUncorErrorType = "poison-received"
	// Buffer overflows (first 3 bits of header log indicate which)
	CxlUncorErrorTypeReceiverOverflow CxlUncorErrorType = "receiver-overflow"
	// Component specific error
	CxlUncorErrorTypeInternal CxlUncorErrorType = "internal"
	// Integrity and data encryption tx error.
	CxlUncorErrorTypeCxlIdeTx CxlUncorErrorType = "cxl-ide-tx"
	// Integrity and data encryption rx error.
	CxlUncorErrorTypeCxlIdeRx CxlUncorErrorType = "cxl-ide-rx"
)

// An enumeration of data format.
//
// Since: 1.4
type DataFormat string

const (
	// Data is a UTF-8 string (RFC 3629)
	DataFormatUtf8 DataFormat = "utf8"
	// Data is Base64 encoded binary (RFC 3548)
	DataFormatBase64 DataFormat = "base64"
)

// Method used to measure dirty page rate.  Differences between
// available methods are explained in @calc-dirty-rate.
//
// Since: 6.2
type DirtyRateMeasureMode string

const (
	// use page sampling
	DirtyRateMeasureModePageSampling DirtyRateMeasureMode = "page-sampling"
	// use dirty ring
	DirtyRateMeasureModeDirtyRing DirtyRateMeasureMode = "dirty-ring"
	// use dirty bitmap
	DirtyRateMeasureModeDirtyBitmap DirtyRateMeasureMode = "dirty-bitmap"
)

// Dirty page rate measurement status.
//
// Since: 5.2
type DirtyRateStatus string

const (
	// measuring thread has not been started yet
	DirtyRateStatusUnstarted DirtyRateStatus = "unstarted"
	// measuring thread is running
	DirtyRateStatusMeasuring DirtyRateStatus = "measuring"
	// dirty page rate is measured and the results are available
	DirtyRateStatusMeasured DirtyRateStatus = "measured"
)

// Display OpenGL mode.
//
// Since: 3.0
type DisplayGLMode string

const (
	// Disable OpenGL (default).
	DisplayGLModeOff DisplayGLMode = "off"
	// Use OpenGL, pick context type automatically. Would better be named 'auto' but is
	// called 'on' for backward compatibility with bool type.
	DisplayGLModeOn DisplayGLMode = "on"
	// Use OpenGL with Core (desktop) Context.
	DisplayGLModeCore DisplayGLMode = "core"
	// Use OpenGL with ES (embedded systems) Context.
	DisplayGLModeEs DisplayGLMode = "es"
)

// Display protocols which support changing password options.
//
// Since: 7.0
type DisplayProtocol string

const (
	DisplayProtocolVnc   DisplayProtocol = "vnc"
	DisplayProtocolSpice DisplayProtocol = "spice"
)

// Available DisplayReload types.
//
// Since: 6.0
type DisplayReloadType string

const (
	// VNC display
	DisplayReloadTypeVnc DisplayReloadType = "vnc"
)

// Display (user interface) type.
//
// Since: 2.12
type DisplayType string

const (
	// The default user interface, selecting from the first available of gtk, sdl,
	// cocoa, and vnc.
	DisplayTypeDefault DisplayType = "default"
	// No user interface or video output display. The guest will still see an emulated
	// graphics card, but its output will not be displayed to the QEMU user.
	DisplayTypeNone DisplayType = "none"
	// The GTK user interface.
	DisplayTypeGtk DisplayType = "gtk"
	// The SDL user interface.
	DisplayTypeSdl DisplayType = "sdl"
	// No user interface, offload GL operations to a local DRI device. Graphical
	// display need to be paired with VNC or Spice. (Since 3.1)
	DisplayTypeEglHeadless DisplayType = "egl-headless"
	// Display video output via curses. For graphics device models which support a text
	// mode, QEMU can display this output using a curses/ncurses interface. Nothing is
	// displayed when the graphics device is in graphical mode or if the graphics
	// device does not support a text mode. Generally only the VGA device models
	// support text mode.
	DisplayTypeCurses DisplayType = "curses"
	// The Cocoa user interface.
	DisplayTypeCocoa DisplayType = "cocoa"
	// Set up a Spice server and run the default associated application to connect to
	// it. The server will redirect the serial console and QEMU monitors. (Since 4.0)
	DisplayTypeSpiceApp DisplayType = "spice-app"
	// Start a D-Bus service for the display. (Since 7.0)
	DisplayTypeDbus DisplayType = "dbus"
)

// Available DisplayUpdate types.
//
// Since: 7.1
type DisplayUpdateType string

const (
	// VNC display
	DisplayUpdateTypeVnc DisplayUpdateType = "vnc"
)

// An enumeration of guest-memory-dump's format.
//
// Since: 2.0
type DumpGuestMemoryFormat string

const (
	// elf format
	DumpGuestMemoryFormatElf DumpGuestMemoryFormat = "elf"
	// makedumpfile flattened, kdump-compressed format with zlib compression
	DumpGuestMemoryFormatKdumpZlib DumpGuestMemoryFormat = "kdump-zlib"
	// makedumpfile flattened, kdump-compressed format with lzo compression
	DumpGuestMemoryFormatKdumpLzo DumpGuestMemoryFormat = "kdump-lzo"
	// makedumpfile flattened, kdump-compressed format with snappy compression
	DumpGuestMemoryFormatKdumpSnappy DumpGuestMemoryFormat = "kdump-snappy"
	// raw assembled kdump-compressed format with zlib compression (since 8.2)
	DumpGuestMemoryFormatKdumpRawZlib DumpGuestMemoryFormat = "kdump-raw-zlib"
	// raw assembled kdump-compressed format with lzo compression (since 8.2)
	DumpGuestMemoryFormatKdumpRawLzo DumpGuestMemoryFormat = "kdump-raw-lzo"
	// raw assembled kdump-compressed format with snappy compression (since 8.2)
	DumpGuestMemoryFormatKdumpRawSnappy DumpGuestMemoryFormat = "kdump-raw-snappy"
	// Windows full crashdump format, can be used instead of ELF converting (since
	// 2.13)
	DumpGuestMemoryFormatWinDmp DumpGuestMemoryFormat = "win-dmp"
)

// Describe the status of a long-running background guest memory dump.
//
// Since: 2.6
type DumpStatus string

const (
	// no dump-guest-memory has started yet.
	DumpStatusNone DumpStatus = "none"
	// there is one dump running in background.
	DumpStatusActive DumpStatus = "active"
	// the last dump has finished successfully.
	DumpStatusCompleted DumpStatus = "completed"
	// the last dump has failed.
	DumpStatusFailed DumpStatus = "failed"
)

// The eBPF programs that can be gotten with request-ebpf.
//
// Since: 9.0
type EbpfProgramID string

const (
	// Receive side scaling, technology that allows steering traffic between queues by
	// calculation hash. Users may set up indirection table and hash/packet types
	// configurations. Used with virtio-net.
	EbpfProgramIDRss EbpfProgramID = "rss"
)

// An enumeration of Xen event channel port types.
//
// Since: 8.0
type EvtchnPortType string

const (
	// The port is unused.
	EvtchnPortTypeClosed EvtchnPortType = "closed"
	// The port is allocated and ready to be bound.
	EvtchnPortTypeUnbound EvtchnPortType = "unbound"
	// The port is connected as an interdomain interrupt.
	EvtchnPortTypeInterdomain EvtchnPortType = "interdomain"
	// The port is bound to a physical IRQ (PIRQ).
	EvtchnPortTypePirq EvtchnPortType = "pirq"
	// The port is bound to a virtual IRQ (VIRQ).
	EvtchnPortTypeVirq EvtchnPortType = "virq"
	// The post is an inter-processor interrupt (IPI).
	EvtchnPortTypeIpi EvtchnPortType = "ipi"
)

// An enumeration of COLO failover status
//
// Since: 2.8
type FailoverStatus string

const (
	// no failover has ever happened
	FailoverStatusNone FailoverStatus = "none"
	// got failover requirement but not handled
	FailoverStatusRequire FailoverStatus = "require"
	// in the process of doing failover
	FailoverStatusActive FailoverStatus = "active"
	// finish the process of failover
	FailoverStatusCompleted FailoverStatus = "completed"
	// restart the failover process, from 'none' -> 'completed' (Since 2.9)
	FailoverStatusRelaunch FailoverStatus = "relaunch"
)

// Type of Floppy drive to be emulated by the Floppy Disk Controller.
//
// Since: 2.6
type FloppyDriveType string

const (
	// 1.44MB 3.5" drive
	FloppyDriveType144 FloppyDriveType = "144"
	// 2.88MB 3.5" drive
	FloppyDriveType288 FloppyDriveType = "288"
	// 1.2MB 5.25" drive
	FloppyDriveType120 FloppyDriveType = "120"
	// No drive connected
	FloppyDriveTypeNone FloppyDriveType = "none"
	// Automatically determined by inserted media at boot
	FloppyDriveTypeAuto FloppyDriveType = "auto"
)

// Possible allow_other modes for FUSE exports.
//
// Since: 6.1
type FuseExportAllowOther string

const (
	// Do not pass allow_other as a mount option.
	FuseExportAllowOtherOff FuseExportAllowOther = "off"
	// Pass allow_other as a mount option.
	FuseExportAllowOtherOn FuseExportAllowOther = "on"
	// Try mounting with allow_other first, and if that fails, retry without
	// allow_other.
	FuseExportAllowOtherAuto FuseExportAllowOther = "auto"
)

// Key combinations to toggle input-linux between host and guest.
//
// Since: 4.0
type GrabToggleKeys string

const (
	// left and right control key
	GrabToggleKeysCtrlCtrl GrabToggleKeys = "ctrl-ctrl"
	// left and right alt key
	GrabToggleKeysAltAlt GrabToggleKeys = "alt-alt"
	// left and right shift key
	GrabToggleKeysShiftShift GrabToggleKeys = "shift-shift"
	// left and right meta key
	GrabToggleKeysMetaMeta GrabToggleKeys = "meta-meta"
	// scroll lock key
	GrabToggleKeysScrolllock GrabToggleKeys = "scrolllock"
	// either control key and scroll lock key
	GrabToggleKeysCtrlScrolllock GrabToggleKeys = "ctrl-scrolllock"
)

// Since: 9.0
type GranuleMode string

const (
	// granule page size of 4KiB
	GranuleMode4K GranuleMode = "4k"
	// granule page size of 8KiB
	GranuleMode8K GranuleMode = "8k"
	// granule page size of 16KiB
	GranuleMode16K GranuleMode = "16k"
	// granule page size of 64KiB
	GranuleMode64K GranuleMode = "64k"
	// granule matches the host page size
	GranuleModeHost GranuleMode = "host"
)

// An enumeration of the actions taken when guest OS panic is detected
//
// Since: 2.1
type GuestPanicAction string

const (
	// system pauses
	GuestPanicActionPause GuestPanicAction = "pause"
	// system powers off (since 2.8)
	GuestPanicActionPoweroff GuestPanicAction = "poweroff"
	// system continues to run (since 5.0)
	GuestPanicActionRun GuestPanicAction = "run"
)

// An enumeration of the guest panic information types
//
// Since: 2.9
type GuestPanicInformationType string

const (
	// hyper-v guest panic information type
	GuestPanicInformationTypeHyperV GuestPanicInformationType = "hyper-v"
	// s390 guest panic information type (Since: 2.12)
	GuestPanicInformationTypeS390 GuestPanicInformationType = "s390"
)

// Cache associativity in the Memory Side Cache Information Structure
// of HMAT  For more information of @HmatCacheAssociativity, see
// chapter 5.2.27.5: Table 5-147 of ACPI 6.3 spec.
//
// Since: 5.0
type HmatCacheAssociativity string

const (
	// None (no memory side cache in this proximity domain, or cache associativity
	// unknown)
	HmatCacheAssociativityNone HmatCacheAssociativity = "none"
	// Direct Mapped
	HmatCacheAssociativityDirect HmatCacheAssociativity = "direct"
	// Complex Cache Indexing (implementation specific)
	HmatCacheAssociativityComplex HmatCacheAssociativity = "complex"
)

// Cache write policy in the Memory Side Cache Information Structure
// of HMAT  For more information of @HmatCacheWritePolicy, see chapter
// 5.2.27.5: Table 5-147: Field "Cache Attributes" of ACPI 6.3 spec.
//
// Since: 5.0
type HmatCacheWritePolicy string

const (
	// None (no memory side cache in this proximity domain, or cache write policy
	// unknown)
	HmatCacheWritePolicyNone HmatCacheWritePolicy = "none"
	// Write Back (WB)
	HmatCacheWritePolicyWriteBack HmatCacheWritePolicy = "write-back"
	// Write Through (WT)
	HmatCacheWritePolicyWriteThrough HmatCacheWritePolicy = "write-through"
)

// Data type in the System Locality Latency and Bandwidth Information
// Structure of HMAT (Heterogeneous Memory Attribute Table)  For more
// information about @HmatLBDataType, see chapter 5.2.27.4: Table
// 5-146:  Field "Data Type" of ACPI 6.3 spec.
//
// Since: 5.0
type HmatLBDataType string

const (
	// access latency (nanoseconds)
	HmatLBDataTypeAccessLatency HmatLBDataType = "access-latency"
	// read latency (nanoseconds)
	HmatLBDataTypeReadLatency HmatLBDataType = "read-latency"
	// write latency (nanoseconds)
	HmatLBDataTypeWriteLatency HmatLBDataType = "write-latency"
	// access bandwidth (Bytes per second)
	HmatLBDataTypeAccessBandwidth HmatLBDataType = "access-bandwidth"
	// read bandwidth (Bytes per second)
	HmatLBDataTypeReadBandwidth HmatLBDataType = "read-bandwidth"
	// write bandwidth (Bytes per second)
	HmatLBDataTypeWriteBandwidth HmatLBDataType = "write-bandwidth"
)

// The memory hierarchy in the System Locality Latency and Bandwidth
// Information Structure of HMAT (Heterogeneous Memory Attribute
// Table)  For more information about @HmatLBMemoryHierarchy, see
// chapter 5.2.27.4: Table 5-146: Field "Flags" of ACPI 6.3 spec.
//
// Since: 5.0
type HmatLBMemoryHierarchy string

const (
	// the structure represents the memory performance
	HmatLBMemoryHierarchyMemory HmatLBMemoryHierarchy = "memory"
	// first level of memory side cache
	HmatLBMemoryHierarchyFirstLevel HmatLBMemoryHierarchy = "first-level"
	// second level of memory side cache
	HmatLBMemoryHierarchySecondLevel HmatLBMemoryHierarchy = "second-level"
	// third level of memory side cache
	HmatLBMemoryHierarchyThirdLevel HmatLBMemoryHierarchy = "third-level"
)

// Host memory policy types
//
// Since: 2.1
type HostMemPolicy string

const (
	// restore default policy, remove any nondefault policy
	HostMemPolicyDefault HostMemPolicy = "default"
	// set the preferred host nodes for allocation
	HostMemPolicyPreferred HostMemPolicy = "preferred"
	// a strict policy that restricts memory allocation to the host nodes specified
	HostMemPolicyBind HostMemPolicy = "bind"
	// memory allocations are interleaved across the set of host nodes specified
	HostMemPolicyInterleave HostMemPolicy = "interleave"
)

// Set of modifier keys that need to be held for shortcut key actions.
//
// Since: 7.1
type HotKeyMod string

const (
	HotKeyModLctrlLalt       HotKeyMod = "lctrl-lalt"
	HotKeyModLshiftLctrlLalt HotKeyMod = "lshift-lctrl-lalt"
	HotKeyModRctrl           HotKeyMod = "rctrl"
)

// Supported image format types.
//
// Since: 7.1
type ImageFormat string

const (
	// PPM format
	ImageFormatPpm ImageFormat = "ppm"
	// PNG format
	ImageFormatPng ImageFormat = "png"
)

// Since: 1.7
type ImageInfoSpecificKind string

const (
	ImageInfoSpecificKindQcow2 ImageInfoSpecificKind = "qcow2"
	ImageInfoSpecificKindVmdk  ImageInfoSpecificKind = "vmdk"
	// Since 2.7
	ImageInfoSpecificKindLuks ImageInfoSpecificKind = "luks"
	// Since 6.1
	ImageInfoSpecificKindRbd ImageInfoSpecificKind = "rbd"
	// Since 8.0
	ImageInfoSpecificKindFile ImageInfoSpecificKind = "file"
)

// Position axis of a pointer input device (mouse, tablet).
//
// Since: 2.0
type InputAxis string

const (
	InputAxisX InputAxis = "x"
	InputAxisY InputAxis = "y"
)

// Button of a pointer input device (mouse, tablet).
//
// Since: 2.0
type InputButton string

const (
	InputButtonLeft      InputButton = "left"
	InputButtonMiddle    InputButton = "middle"
	InputButtonRight     InputButton = "right"
	InputButtonWheelUp   InputButton = "wheel-up"
	InputButtonWheelDown InputButton = "wheel-down"
	// front side button of a 5-button mouse (since 2.9)
	InputButtonSide InputButton = "side"
	// rear side button of a 5-button mouse (since 2.9)
	InputButtonExtra      InputButton = "extra"
	InputButtonWheelLeft  InputButton = "wheel-left"
	InputButtonWheelRight InputButton = "wheel-right"
	// screen contact on a multi-touch device (since 8.1)
	InputButtonTouch InputButton = "touch"
)

// Since: 2.0
type InputEventKind string

const (
	// a keyboard input event
	InputEventKindKey InputEventKind = "key"
	// a pointer button input event
	InputEventKindBtn InputEventKind = "btn"
	// a relative pointer motion input event
	InputEventKindRel InputEventKind = "rel"
	// an absolute pointer motion input event
	InputEventKindAbs InputEventKind = "abs"
	// a multi-touch input event
	InputEventKindMtt InputEventKind = "mtt"
)

// Type of a multi-touch event.
//
// Since: 8.1
type InputMultiTouchType string

const (
	// A new touch event sequence has just started.
	InputMultiTouchTypeBegin InputMultiTouchType = "begin"
	// A touch event sequence has been updated.
	InputMultiTouchTypeUpdate InputMultiTouchType = "update"
	// A touch event sequence has finished.
	InputMultiTouchTypeEnd InputMultiTouchType = "end"
	// A touch event sequence has been canceled.
	InputMultiTouchTypeCancel InputMultiTouchType = "cancel"
	// Absolute position data.
	InputMultiTouchTypeData InputMultiTouchType = "data"
)

// An enumeration of the I/O operation types
//
// Since: 2.1
type IoOperationType string

const (
	// read operation
	IoOperationTypeRead IoOperationType = "read"
	// write operation
	IoOperationTypeWrite IoOperationType = "write"
)

// An enumeration of header digests supported by libiscsi
//
// Since: 2.9
type IscsiHeaderDigest string

const (
	IscsiHeaderDigestCrc32C     IscsiHeaderDigest = "crc32c"
	IscsiHeaderDigestNone       IscsiHeaderDigest = "none"
	IscsiHeaderDigestCrc32CNone IscsiHeaderDigest = "crc32c-none"
	IscsiHeaderDigestNoneCrc32C IscsiHeaderDigest = "none-crc32c"
)

// An enumeration of libiscsi transport types
//
// Since: 2.9
type IscsiTransport string

const (
	IscsiTransportTcp  IscsiTransport = "tcp"
	IscsiTransportIser IscsiTransport = "iser"
)

// The four primitive and two structured types according to RFC 8259
// section 1, plus 'int' (split off 'number'), plus the obvious top
// type 'value'.
//
// Since: 2.5
type JSONType string

const (
	// JSON string
	JSONTypeString JSONType = "string"
	// JSON number
	JSONTypeNumber JSONType = "number"
	// JSON number that is an integer
	JSONTypeInt JSONType = "int"
	// literal ``false`` or ``true``
	JSONTypeBoolean JSONType = "boolean"
	// literal ``null``
	JSONTypeNull JSONType = "null"
	// JSON object
	JSONTypeObject JSONType = "object"
	// JSON array
	JSONTypeArray JSONType = "array"
	// any JSON value
	JSONTypeValue JSONType = "value"
)

// Indicates the present state of a given job in its lifetime.
//
// Since: 2.12
type JobStatus string

const (
	// Erroneous, default state. Should not ever be visible.
	JobStatusUndefined JobStatus = "undefined"
	// The job has been created, but not yet started.
	JobStatusCreated JobStatus = "created"
	// The job is currently running.
	JobStatusRunning JobStatus = "running"
	// The job is running, but paused. The pause may be requested by either the QMP
	// user or by internal processes.
	JobStatusPaused JobStatus = "paused"
	// The job is running, but is ready for the user to signal completion. This is used
	// for long-running jobs like mirror that are designed to run indefinitely.
	JobStatusReady JobStatus = "ready"
	// The job is ready, but paused. This is nearly identical to @paused. The job may
	// return to @ready or otherwise be canceled.
	JobStatusStandby JobStatus = "standby"
	// The job is waiting for other jobs in the transaction to converge to the waiting
	// state. This status will likely not be visible for the last job in a transaction.
	JobStatusWaiting JobStatus = "waiting"
	// The job has finished its work, but has finalization steps that it needs to make
	// prior to completing. These changes will require manual intervention via @job-
	// finalize if auto-finalize was set to false. These pending changes may still
	// fail.
	JobStatusPending JobStatus = "pending"
	// The job is in the process of being aborted, and will finish with an error. The
	// job will afterwards report that it is @concluded. This status may not be visible
	// to the management process.
	JobStatusAborting JobStatus = "aborting"
	// The job has finished all work. If auto-dismiss was set to false, the job will
	// remain in the query list until it is dismissed via @job-dismiss.
	JobStatusConcluded JobStatus = "concluded"
	// The job is in the process of being dismantled. This state should not ever be
	// visible externally.
	JobStatusNull JobStatus = "null"
)

// Type of a background job.
//
// Since: 1.7
type JobType string

const (
	// block commit job type, see "block-commit"
	JobTypeCommit JobType = "commit"
	// block stream job type, see "block-stream"
	JobTypeStream JobType = "stream"
	// drive mirror job type, see "drive-mirror"
	JobTypeMirror JobType = "mirror"
	// drive backup job type, see "drive-backup"
	JobTypeBackup JobType = "backup"
	// image creation job type, see "blockdev-create" (since 3.0)
	JobTypeCreate JobType = "create"
	// image options amend job type, see "x-blockdev-amend" (since 5.1)
	JobTypeAmend JobType = "amend"
	// snapshot load job type, see "snapshot-load" (since 6.0)
	JobTypeSnapshotLoad JobType = "snapshot-load"
	// snapshot save job type, see "snapshot-save" (since 6.0)
	JobTypeSnapshotSave JobType = "snapshot-save"
	// snapshot delete job type, see "snapshot-delete" (since 6.0)
	JobTypeSnapshotDelete JobType = "snapshot-delete"
)

// Represents command verbs that can be applied to a job.
//
// Since: 2.12
type JobVerb string

const (
	// see @job-cancel
	JobVerbCancel JobVerb = "cancel"
	// see @job-pause
	JobVerbPause JobVerb = "pause"
	// see @job-resume
	JobVerbResume JobVerb = "resume"
	// see @block-job-set-speed
	JobVerbSetSpeed JobVerb = "set-speed"
	// see @job-complete
	JobVerbComplete JobVerb = "complete"
	// see @job-dismiss
	JobVerbDismiss JobVerb = "dismiss"
	// see @job-finalize
	JobVerbFinalize JobVerb = "finalize"
	// see @block-job-change (since 8.2)
	JobVerbChange JobVerb = "change"
)

// Since: 1.3
type KeyValueKind string

const (
	KeyValueKindNumber KeyValueKind = "number"
	KeyValueKindQcode  KeyValueKind = "qcode"
)

// Policy for handling lost ticks in timer devices.  Ticks end up
// getting lost when, for example, the guest is paused.
//
// Since: 2.0
type LostTickPolicy string

const (
	// throw away the missed ticks and continue with future injection normally. The
	// guest OS will see the timer jump ahead by a potentially quite significant amount
	// all at once, as if the intervening chunk of time had simply not existed;
	// needless to say, such a sudden jump can easily confuse a guest OS which is not
	// specifically prepared to deal with it. Assuming the guest OS can deal correctly
	// with the time jump, the time in the guest and in the host should now match.
	LostTickPolicyDiscard LostTickPolicy = "discard"
	// continue to deliver ticks at the normal rate. The guest OS will not notice
	// anything is amiss, as from its point of view time will have continued to flow
	// normally. The time in the guest should now be behind the time in the host by
	// exactly the amount of time during which ticks have been missed.
	LostTickPolicyDelay LostTickPolicy = "delay"
	// deliver ticks at a higher rate to catch up with the missed ticks. The guest OS
	// will not notice anything is amiss, as from its point of view time will have
	// continued to flow normally. Once the timer has managed to catch up with all the
	// missing ticks, the time in the guest and in the host should match.
	LostTickPolicySlew LostTickPolicy = "slew"
)

// Since: 2.1
type MemoryDeviceInfoKind string

const (
	MemoryDeviceInfoKindDimm MemoryDeviceInfoKind = "dimm"
	// since 2.12
	MemoryDeviceInfoKindNvdimm MemoryDeviceInfoKind = "nvdimm"
	// since 4.1
	MemoryDeviceInfoKindVirtioPmem MemoryDeviceInfoKind = "virtio-pmem"
	// since 5.1
	MemoryDeviceInfoKindVirtioMem MemoryDeviceInfoKind = "virtio-mem"
	// since 6.2.
	MemoryDeviceInfoKindSgxEpc MemoryDeviceInfoKind = "sgx-epc"
	// since 8.2.
	MemoryDeviceInfoKindHvBalloon MemoryDeviceInfoKind = "hv-balloon"
)

// Actions taken by QEMU in response to a hardware memory failure.
//
// Since: 5.2
type MemoryFailureAction string

const (
	// the memory failure could be ignored. This will only be the case for action-
	// optional failures.
	MemoryFailureActionIgnore MemoryFailureAction = "ignore"
	// memory failure occurred in guest memory, the guest enabled MCE handling
	// mechanism, and QEMU could inject the MCE into the guest successfully.
	MemoryFailureActionInject MemoryFailureAction = "inject"
	// the failure is unrecoverable. This occurs for action-required failures if the
	// recipient is the hypervisor; QEMU will exit.
	MemoryFailureActionFatal MemoryFailureAction = "fatal"
	// the failure is unrecoverable but confined to the guest. This occurs if the
	// recipient is a guest guest which is not ready to handle memory failures.
	MemoryFailureActionReset MemoryFailureAction = "reset"
)

// Hardware memory failure occurs, handled by recipient.
//
// Since: 5.2
type MemoryFailureRecipient string

const (
	// memory failure at QEMU process address space. (none guest memory, but used by
	// QEMU itself).
	MemoryFailureRecipientHypervisor MemoryFailureRecipient = "hypervisor"
	// memory failure at guest memory,
	MemoryFailureRecipientGuest MemoryFailureRecipient = "guest"
)

type MigMode string

const (
	// the original form of migration. (since 8.2)
	MigModeNormal MigMode = "normal"
	// The migrate command stops the VM and saves state to the URI. After quitting
	// QEMU, the user resumes by running QEMU -incoming. This mode allows the user to
	// quit QEMU, optionally update and reboot the OS, and restart QEMU. If the user
	// reboots, the URI must persist across the reboot, such as by using a file. Unlike
	// normal mode, the use of certain local storage options does not block the
	// migration, but the user must not modify the contents of guest block devices
	// between the quit and restart. This mode supports VFIO devices provided the user
	// first puts the guest in the suspended runstate, such as by issuing guest-
	// suspend-ram to the QEMU guest agent. Best performance is achieved when the
	// memory backend is shared and the @x-ignore-shared migration capability is set,
	// but this is not required. Further, if the user reboots before restarting such a
	// configuration, the shared memory must persist across the reboot, such as by
	// backing it with a dax device. @cpr-reboot may not be used with postcopy,
	// background-snapshot, or COLO. (since 8.2)
	MigModeCprReboot MigMode = "cpr-reboot"
	// This mode allows the user to transfer a guest to a new QEMU instance on the same
	// host with minimal guest pause time by preserving guest RAM in place. Devices and
	// their pinned pages will also be preserved in a future QEMU release. The user
	// starts new QEMU on the same host as old QEMU, with command-line arguments to
	// create the same machine, plus the -incoming option for the main migration
	// channel, like normal live migration. In addition, the user adds a second
	// -incoming option with channel type "cpr". This CPR channel must support file
	// descriptor transfer with SCM_RIGHTS, i.e. it must be a UNIX domain socket. To
	// initiate CPR, the user issues a migrate command to old QEMU, adding a second
	// migration channel of type "cpr" in the channels argument. Old QEMU stops the VM,
	// saves state to the migration channels, and enters the postmigrate state.
	// Execution resumes in new QEMU. New QEMU reads the CPR channel before opening a
	// monitor, hence the CPR channel cannot be specified in the list of channels for a
	// migrate-incoming command. It may only be specified on the command line. The main
	// channel address cannot be a file type, and for an inet socket, the port cannot
	// be 0 (meaning dynamically choose a port). Memory-backend objects must have the
	// share=on attribute, but memory-backend-epc is not supported. The VM must be
	// started with the '-machine aux-ram-share=on' option. When using -incoming defer,
	// you must issue the migrate command to old QEMU before issuing any monitor
	// commands to new QEMU. However, new QEMU does not open and read the migration
	// stream until you issue the migrate incoming command. (since 10.0)
	MigModeCprTransfer MigMode = "cpr-transfer"
)

// The migration stream transport mechanisms.
//
// Since: 8.2
type MigrationAddressType string

const (
	// Migrate via socket.
	MigrationAddressTypeSocket MigrationAddressType = "socket"
	// Direct the migration stream to another process.
	MigrationAddressTypeExec MigrationAddressType = "exec"
	// Migrate via RDMA.
	MigrationAddressTypeRdma MigrationAddressType = "rdma"
	// Direct the migration stream to a file.
	MigrationAddressTypeFile MigrationAddressType = "file"
)

// Migration capabilities enumeration
//
// Since: 1.2
type MigrationCapability string

const (
	// Migration supports xbzrle (Xor Based Zero Run Length Encoding). This feature
	// allows us to minimize migration traffic for certain work loads, by sending
	// compressed difference of the pages
	MigrationCapabilityXbzrle MigrationCapability = "xbzrle"
	// Controls whether or not the entire VM memory footprint is mlock()'d on demand or
	// all at once. Refer to docs/rdma.txt for usage. Disabled by default. (since 2.0)
	MigrationCapabilityRdmaPinAll MigrationCapability = "rdma-pin-all"
	// If enabled, QEMU will automatically throttle down the guest to speed up
	// convergence of RAM migration. (since 1.6)
	MigrationCapabilityAutoConverge MigrationCapability = "auto-converge"
	// During storage migration encode blocks of zeroes efficiently. This essentially
	// saves 1MB of zeroes per block on the wire. Enabling requires source and target
	// VM to support this feature. To enable it is sufficient to enable the capability
	// on the source VM. The feature is disabled by default. (since 1.6)
	MigrationCapabilityZeroBlocks MigrationCapability = "zero-blocks"
	// generate events for each migration state change (since 2.4)
	MigrationCapabilityEvents MigrationCapability = "events"
	// Start executing on the migration target before all of RAM has been migrated,
	// pulling the remaining pages along as needed. The capacity must have the same
	// setting on both source and target or migration will not even start. NOTE: If the
	// migration fails during postcopy the VM will fail. (since 2.6)
	MigrationCapabilityPostcopyRam MigrationCapability = "postcopy-ram"
	// If enabled, migration will never end, and the state of the VM on the primary
	// side will be migrated continuously to the VM on secondary side, this process is
	// called COarse-Grain LOck Stepping (COLO) for Non-stop Service. (since 2.8)
	MigrationCapabilityXColo MigrationCapability = "x-colo"
	// if enabled, qemu will free the migrated ram pages on the source during postcopy-
	// ram migration. (since 2.9)
	MigrationCapabilityReleaseRam MigrationCapability = "release-ram"
	// If enabled, migration will use the return path even for precopy. (since 2.10)
	MigrationCapabilityReturnPath MigrationCapability = "return-path"
	// Pause outgoing migration before serialising device state and before disabling
	// block IO (since 2.11)
	MigrationCapabilityPauseBeforeSwitchover MigrationCapability = "pause-before-switchover"
	// Use more than one fd for migration (since 4.0)
	MigrationCapabilityMultifd MigrationCapability = "multifd"
	// If enabled, QEMU will migrate named dirty bitmaps. (since 2.12)
	MigrationCapabilityDirtyBitmaps MigrationCapability = "dirty-bitmaps"
	// Calculate downtime for postcopy live migration (since 3.0)
	MigrationCapabilityPostcopyBlocktime MigrationCapability = "postcopy-blocktime"
	// If enabled, the destination will not activate block devices (and thus take
	// locks) immediately at the end of migration. (since 3.0)
	MigrationCapabilityLateBlockActivate MigrationCapability = "late-block-activate"
	// If enabled, QEMU will not migrate shared memory that is accessible on the
	// destination machine. (since 4.0)
	MigrationCapabilityXIgnoreShared MigrationCapability = "x-ignore-shared"
	// Send the UUID of the source to allow the destination to ensure it is the same.
	// (since 4.2)
	MigrationCapabilityValidateUuid MigrationCapability = "validate-uuid"
	// If enabled, the migration stream will be a snapshot of the VM exactly at the
	// point when the migration procedure starts. The VM RAM is saved with running VM.
	// (since 6.0)
	MigrationCapabilityBackgroundSnapshot MigrationCapability = "background-snapshot"
	// Controls behavior on sending memory pages on migration. When true, enables a
	// zero-copy mechanism for sending memory pages, if host supports it. Requires that
	// QEMU be permitted to use locked memory for guest RAM pages. (since 7.1)
	MigrationCapabilityZeroCopySend MigrationCapability = "zero-copy-send"
	// If enabled, the migration process will allow postcopy requests to preempt
	// precopy stream, so postcopy requests will be handled faster. This is a
	// performance feature and should not affect the correctness of postcopy migration.
	// (since 7.1)
	MigrationCapabilityPostcopyPreempt MigrationCapability = "postcopy-preempt"
	// If enabled, migration will not stop the source VM and complete the migration
	// until an ACK is received from the destination that it's OK to do so. Exactly
	// when this ACK is sent depends on the migrated devices that use this feature. For
	// example, a device can use it to make sure some of its data is sent and loaded in
	// the destination before doing switchover. This can reduce downtime if devices
	// that support this capability are present. 'return-path' capability must be
	// enabled to use it. (since 8.1)
	MigrationCapabilitySwitchoverAck MigrationCapability = "switchover-ack"
	// If enabled, migration will throttle vCPUs as needed to keep their dirty page
	// rate within @vcpu-dirty-limit. This can improve responsiveness of large guests
	// during live migration, and can result in more stable read performance. Requires
	// KVM with accelerator property "dirty-ring-size" set. (Since 8.1)
	MigrationCapabilityDirtyLimit MigrationCapability = "dirty-limit"
	// Migrate using fixed offsets in the migration file for each RAM page. Requires a
	// migration URI that supports seeking, such as a file. (since 9.0)
	MigrationCapabilityMappedRam MigrationCapability = "mapped-ram"
)

// The migration channel-type request options.
//
// Since: 8.1
type MigrationChannelType string

const (
	// Main outbound migration channel.
	MigrationChannelTypeMain MigrationChannelType = "main"
	// Checkpoint and restart state channel.
	MigrationChannelTypeCpr MigrationChannelType = "cpr"
)

// Migration parameters enumeration
//
// Since: 2.4
type MigrationParameter string

const (
	// Initial delay (in milliseconds) before sending the first announce (Since 4.0)
	MigrationParameterAnnounceInitial MigrationParameter = "announce-initial"
	// Maximum delay (in milliseconds) between packets in the announcement (Since 4.0)
	MigrationParameterAnnounceMax MigrationParameter = "announce-max"
	// Number of self-announce packets sent after migration (Since 4.0)
	MigrationParameterAnnounceRounds MigrationParameter = "announce-rounds"
	// Increase in delay (in milliseconds) between subsequent packets in the
	// announcement (Since 4.0)
	MigrationParameterAnnounceStep MigrationParameter = "announce-step"
	// The ratio of bytes_dirty_period and bytes_xfer_period to trigger throttling. It
	// is expressed as percentage. The default value is 50. (Since 5.0)
	MigrationParameterThrottleTriggerThreshold MigrationParameter = "throttle-trigger-threshold"
	// Initial percentage of time guest cpus are throttled when migration auto-converge
	// is activated. The default value is 20. (Since 2.7)
	MigrationParameterCpuThrottleInitial MigrationParameter = "cpu-throttle-initial"
	// throttle percentage increase each time auto-converge detects that migration is
	// not making progress. The default value is 10. (Since 2.7)
	MigrationParameterCpuThrottleIncrement MigrationParameter = "cpu-throttle-increment"
	// Make CPU throttling slower at tail stage At the tail stage of throttling, the
	// Guest is very sensitive to CPU percentage while the @cpu-throttle -increment is
	// excessive usually at tail stage. If this parameter is true, we will compute the
	// ideal CPU percentage used by the Guest, which may exactly make the dirty rate
	// match the dirty rate threshold. Then we will choose a smaller throttle increment
	// between the one specified by @cpu-throttle-increment and the one generated by
	// ideal CPU percentage. Therefore, it is compatible to traditional throttling,
	// meanwhile the throttle increment won't be excessive at tail stage. The default
	// value is false. (Since 5.1)
	MigrationParameterCpuThrottleTailslow MigrationParameter = "cpu-throttle-tailslow"
	// ID of the 'tls-creds' object that provides credentials for establishing a TLS
	// connection over the migration data channel. On the outgoing side of the
	// migration, the credentials must be for a 'client' endpoint, while for the
	// incoming side the credentials must be for a 'server' endpoint. Setting this to a
	// non-empty string enables TLS for all migrations. An empty string means that QEMU
	// will use plain text mode for migration, rather than TLS. (Since 2.7)
	MigrationParameterTlsCreds MigrationParameter = "tls-creds"
	// migration target's hostname for validating the server's x509 certificate
	// identity. If empty, QEMU will use the hostname from the migration URI, if any. A
	// non-empty value is required when using x509 based TLS credentials and the
	// migration URI does not include a hostname, such as fd: or exec: based migration.
	// (Since 2.7) Note: empty value works only since 2.9.
	MigrationParameterTlsHostname MigrationParameter = "tls-hostname"
	// ID of the 'authz' object subclass that provides access control checking of the
	// TLS x509 certificate distinguished name. This object is only resolved at time of
	// use, so can be deleted and recreated on the fly while the migration server is
	// active. If missing, it will default to denying access (Since 4.0)
	MigrationParameterTlsAuthz MigrationParameter = "tls-authz"
	// maximum speed for migration, in bytes per second. (Since 2.8)
	MigrationParameterMaxBandwidth MigrationParameter = "max-bandwidth"
	// to set the available bandwidth that migration can use during switchover phase.
	// NOTE! This does not limit the bandwidth during switchover, but only for
	// calculations when making decisions to switchover. By default, this value is
	// zero, which means QEMU will estimate the bandwidth automatically. This can be
	// set when the estimated value is not accurate, while the user is able to
	// guarantee such bandwidth is available when switching over. When specified
	// correctly, this can make the switchover decision much more accurate. (Since 8.2)
	MigrationParameterAvailSwitchoverBandwidth MigrationParameter = "avail-switchover-bandwidth"
	// set maximum tolerated downtime for migration. maximum downtime in milliseconds
	// (Since 2.8)
	MigrationParameterDowntimeLimit MigrationParameter = "downtime-limit"
	// The delay time (in ms) between two COLO checkpoints in periodic mode. (Since
	// 2.8)
	MigrationParameterXCheckpointDelay MigrationParameter = "x-checkpoint-delay"
	// Number of channels used to migrate data in parallel. This is the same number
	// that the number of sockets used for migration. The default value is 2 (since
	// 4.0)
	MigrationParameterMultifdChannels MigrationParameter = "multifd-channels"
	// cache size to be used by XBZRLE migration. It needs to be a multiple of the
	// target page size and a power of 2 (Since 2.11)
	MigrationParameterXbzrleCacheSize MigrationParameter = "xbzrle-cache-size"
	// Background transfer bandwidth during postcopy. Defaults to 0 (unlimited). In
	// bytes per second. (Since 3.0)
	MigrationParameterMaxPostcopyBandwidth MigrationParameter = "max-postcopy-bandwidth"
	// maximum cpu throttle percentage. Defaults to 99. (Since 3.1)
	MigrationParameterMaxCpuThrottle MigrationParameter = "max-cpu-throttle"
	// Which compression method to use. Defaults to none. (Since 5.0)
	MigrationParameterMultifdCompression MigrationParameter = "multifd-compression"
	// Set the compression level to be used in live migration, the compression level is
	// an integer between 0 and 9, where 0 means no compression, 1 means the best
	// compression speed, and 9 means best compression ratio which will consume more
	// CPU. Defaults to 1. (Since 5.0)
	MigrationParameterMultifdZlibLevel MigrationParameter = "multifd-zlib-level"
	// Set the compression level to be used in live migration, the compression level is
	// an integer between 0 and 20, where 0 means no compression, 1 means the best
	// compression speed, and 20 means best compression ratio which will consume more
	// CPU. Defaults to 1. (Since 5.0)
	MigrationParameterMultifdZstdLevel MigrationParameter = "multifd-zstd-level"
	// Set the compression level to be used in live migration. The level is an integer
	// between 1 and 9, where 1 means the best compression speed, and 9 means the best
	// compression ratio which will consume more CPU. Defaults to 1. (Since 9.2)
	MigrationParameterMultifdQatzipLevel MigrationParameter = "multifd-qatzip-level"
	// Maps block nodes and bitmaps on them to aliases for the purpose of dirty bitmap
	// migration. Such aliases may for example be the corresponding names on the
	// opposite site. The mapping must be one-to-one, but not necessarily complete: On
	// the source, unmapped bitmaps and all bitmaps on unmapped nodes will be ignored.
	// On the destination, encountering an unmapped alias in the incoming migration
	// stream will result in a report, and all further bitmap migration data will then
	// be discarded. Note that the destination does not know about bitmaps it does not
	// receive, so there is no limitation or requirement regarding the number of
	// bitmaps received, or how they are named, or on which nodes they are placed. By
	// default (when this parameter has never been set), bitmap names are mapped to
	// themselves. Nodes are mapped to their block device name if there is one, and to
	// their node name otherwise. (Since 5.2)
	MigrationParameterBlockBitmapMapping MigrationParameter = "block-bitmap-mapping"
	// Periodic time (in milliseconds) of dirty limit during live migration. Should be
	// in the range 1 to 1000ms. Defaults to 1000ms. (Since 8.1)
	MigrationParameterXVcpuDirtyLimitPeriod MigrationParameter = "x-vcpu-dirty-limit-period"
	// Dirtyrate limit (MB/s) during live migration. Defaults to 1. (Since 8.1)
	MigrationParameterVcpuDirtyLimit MigrationParameter = "vcpu-dirty-limit"
	// Migration mode. See description in @MigMode. Default is 'normal'. (Since 8.2)
	MigrationParameterMode MigrationParameter = "mode"
	// Whether and how to detect zero pages. See description in @ZeroPageDetection.
	// Default is 'multifd'. (since 9.0)
	MigrationParameterZeroPageDetection MigrationParameter = "zero-page-detection"
	// Open migration files with O_DIRECT when possible. This only has effect if the
	// @mapped-ram capability is enabled. (Since 9.1)
	MigrationParameterDirectIo MigrationParameter = "direct-io"
)

// An enumeration of migration status.
//
// Since: 2.3
type MigrationStatus string

const (
	// no migration has ever happened.
	MigrationStatusNone MigrationStatus = "none"
	// migration process has been initiated.
	MigrationStatusSetup MigrationStatus = "setup"
	// in the process of cancelling migration.
	MigrationStatusCancelling MigrationStatus = "cancelling"
	// cancelling migration is finished.
	MigrationStatusCancelled MigrationStatus = "cancelled"
	// in the process of doing migration.
	MigrationStatusActive MigrationStatus = "active"
	// like active, but now in postcopy mode. (since 2.5)
	MigrationStatusPostcopyActive MigrationStatus = "postcopy-active"
	// during postcopy but paused. (since 3.0)
	MigrationStatusPostcopyPaused MigrationStatus = "postcopy-paused"
	// setup phase for a postcopy recovery process, preparing for a recovery phase to
	// start. (since 9.1)
	MigrationStatusPostcopyRecoverSetup MigrationStatus = "postcopy-recover-setup"
	// trying to recover from a paused postcopy. (since 3.0)
	MigrationStatusPostcopyRecover MigrationStatus = "postcopy-recover"
	// migration is finished.
	MigrationStatusCompleted MigrationStatus = "completed"
	// some error occurred during migration process.
	MigrationStatusFailed MigrationStatus = "failed"
	// VM is in the process of fault tolerance, VM can not get into this state unless
	// colo capability is enabled for migration. (since 2.8)
	MigrationStatusColo MigrationStatus = "colo"
	// Paused before device serialisation. (since 2.11)
	MigrationStatusPreSwitchover MigrationStatus = "pre-switchover"
	// During device serialisation (also known as switchover phase). Before 9.2, this
	// is only used when (1) in precopy, and (2) when pre-switchover capability is
	// enabled. After 10.0, this state will always be present for every migration
	// procedure as the switchover phase. (since 2.11)
	MigrationStatusDevice MigrationStatus = "device"
	// wait for device unplug request by guest OS to be completed. (since 4.2)
	MigrationStatusWaitUnplug MigrationStatus = "wait-unplug"
)

// An enumeration whose values tell the mirror block job when to
// trigger writes to the target.
//
// Since: 3.0
type MirrorCopyMode string

const (
	// copy data in background only.
	MirrorCopyModeBackground MirrorCopyMode = "background"
	// when data is written to the source, write it (synchronously) to the target as
	// well. In addition, data is copied in background just like in @background mode.
	MirrorCopyModeWriteBlocking MirrorCopyMode = "write-blocking"
)

// An enumeration of possible behaviors for the initial
// synchronization phase of storage mirroring.
//
// Since: 1.3
type MirrorSyncMode string

const (
	// copies data in the topmost image to the destination
	MirrorSyncModeTop MirrorSyncMode = "top"
	// copies data from all images to the destination
	MirrorSyncModeFull MirrorSyncMode = "full"
	// only copy data written from now on
	MirrorSyncModeNone MirrorSyncMode = "none"
	// only copy data described by the dirty bitmap. (since: 2.4)
	MirrorSyncModeIncremental MirrorSyncMode = "incremental"
	// only copy data described by the dirty bitmap. (since: 4.2) Behavior on
	// completion is determined by the BitmapSyncMode.
	MirrorSyncModeBitmap MirrorSyncMode = "bitmap"
)

// An enumeration of monitor modes.
//
// Since: 5.0
type MonitorMode string

const (
	// HMP monitor (human-oriented command line interface)
	MonitorModeReadline MonitorMode = "readline"
	// QMP monitor (JSON-based machine interface)
	MonitorModeControl MonitorMode = "control"
)

// An enumeration of multifd compression methods.
//
// Since: 5.0
type MultiFDCompression string

const (
	// no compression.
	MultiFDCompressionNone MultiFDCompression = "none"
	// use zlib compression method.
	MultiFDCompressionZlib MultiFDCompression = "zlib"
	// use zstd compression method.
	MultiFDCompressionZstd MultiFDCompression = "zstd"
	// use qatzip compression method. (Since 9.2)
	MultiFDCompressionQatzip MultiFDCompression = "qatzip"
	// use qpl compression method. Query Processing Library(qpl) is based on the
	// deflate compression algorithm and use the Intel In-Memory Analytics
	// Accelerator(IAA) accelerated compression and decompression. (Since 9.1)
	MultiFDCompressionQpl MultiFDCompression = "qpl"
	// use UADK library compression method. (Since 9.1)
	MultiFDCompressionUadk MultiFDCompression = "uadk"
)

// An enumeration of NFS transport types
//
// Since: 2.9
type NFSTransport string

const (
	// TCP transport
	NFSTransportInet NFSTransport = "inet"
)

// Available netdev drivers.
//
// Since: 2.7
type NetClientDriver string

const (
	NetClientDriverNone NetClientDriver = "none"
	NetClientDriverNic  NetClientDriver = "nic"
	NetClientDriverUser NetClientDriver = "user"
	NetClientDriverTap  NetClientDriver = "tap"
	// since 2.1
	NetClientDriverL2Tpv3 NetClientDriver = "l2tpv3"
	NetClientDriverSocket NetClientDriver = "socket"
	// since 7.2
	NetClientDriverStream NetClientDriver = "stream"
	// since 7.2
	NetClientDriverDgram     NetClientDriver = "dgram"
	NetClientDriverVde       NetClientDriver = "vde"
	NetClientDriverBridge    NetClientDriver = "bridge"
	NetClientDriverHubport   NetClientDriver = "hubport"
	NetClientDriverNetmap    NetClientDriver = "netmap"
	NetClientDriverVhostUser NetClientDriver = "vhost-user"
	// since 5.1
	NetClientDriverVhostVdpa NetClientDriver = "vhost-vdpa"
	// since 8.2
	NetClientDriverAfXdp NetClientDriver = "af-xdp"
	// since 7.1
	NetClientDriverVmnetHost NetClientDriver = "vmnet-host"
	// since 7.1
	NetClientDriverVmnetShared NetClientDriver = "vmnet-shared"
	// since 7.1
	NetClientDriverVmnetBridged NetClientDriver = "vmnet-bridged"
)

// Indicates whether a netfilter is attached to a netdev's transmit
// queue or receive queue or both.
//
// Since: 2.5
type NetFilterDirection string

const (
	// the filter is attached both to the receive and the transmit queue of the netdev
	// (default).
	NetFilterDirectionAll NetFilterDirection = "all"
	// the filter is attached to the receive queue of the netdev, where it will receive
	// packets sent to the netdev.
	NetFilterDirectionRx NetFilterDirection = "rx"
	// the filter is attached to the transmit queue of the netdev, where it will
	// receive packets sent by the netdev.
	NetFilterDirectionTx NetFilterDirection = "tx"
)

// Indicates where to insert a netfilter relative to a given other
// filter.
//
// Since: 5.0
type NetfilterInsert string

const (
	// insert before the specified filter
	NetfilterInsertBefore NetfilterInsert = "before"
	// insert behind the specified filter
	NetfilterInsertBehind NetfilterInsert = "behind"
)

// The network address family
//
// Since: 2.1
type NetworkAddressFamily string

const (
	// IPV4 family
	NetworkAddressFamilyIpv4 NetworkAddressFamily = "ipv4"
	// IPV6 family
	NetworkAddressFamilyIpv6 NetworkAddressFamily = "ipv6"
	// unix socket
	NetworkAddressFamilyUnix NetworkAddressFamily = "unix"
	// vsock family (since 2.8)
	NetworkAddressFamilyVsock NetworkAddressFamily = "vsock"
	// otherwise
	NetworkAddressFamilyUnknown NetworkAddressFamily = "unknown"
)

// An enumeration that tells QEMU how to set the backing file path in
// a new image file.
//
// Since: 1.1
type NewImageMode string

const (
	// QEMU should look for an existing image file.
	NewImageModeExisting NewImageMode = "existing"
	// QEMU should create a new image with absolute paths for the backing file. If
	// there is no backing file available, the new image will not be backed either.
	NewImageModeAbsolutePaths NewImageMode = "absolute-paths"
)

// An enumeration of the options specified when enabling notify VM
// exit
//
// Since: 7.2
type NotifyVmexitOption string

const (
	// enable the feature, do nothing and continue if the notify VM exit happens.
	NotifyVmexitOptionRun NotifyVmexitOption = "run"
	// enable the feature, raise a internal error if the notify VM exit happens.
	NotifyVmexitOptionInternalError NotifyVmexitOption = "internal-error"
	// disable the feature.
	NotifyVmexitOptionDisable NotifyVmexitOption = "disable"
)

// Since: 2.1
type NumaOptionsType string

const (
	// NUMA nodes configuration
	NumaOptionsTypeNode NumaOptionsType = "node"
	// NUMA distance configuration (since 2.10)
	NumaOptionsTypeDist NumaOptionsType = "dist"
	// property based CPU(s) to node mapping (Since: 2.10)
	NumaOptionsTypeCpu NumaOptionsType = "cpu"
	// memory latency and bandwidth information (Since: 5.0)
	NumaOptionsTypeHmatLb NumaOptionsType = "hmat-lb"
	// memory side cache information (Since: 5.0)
	NumaOptionsTypeHmatCache NumaOptionsType = "hmat-cache"
)

// Since: 6.0
type ObjectType string

const (
	ObjectTypeAcpiGenericInitiator    ObjectType = "acpi-generic-initiator"
	ObjectTypeAcpiGenericPort         ObjectType = "acpi-generic-port"
	ObjectTypeAuthzList               ObjectType = "authz-list"
	ObjectTypeAuthzListfile           ObjectType = "authz-listfile"
	ObjectTypeAuthzPam                ObjectType = "authz-pam"
	ObjectTypeAuthzSimple             ObjectType = "authz-simple"
	ObjectTypeCanBus                  ObjectType = "can-bus"
	ObjectTypeCanHostSocketcan        ObjectType = "can-host-socketcan"
	ObjectTypeColoCompare             ObjectType = "colo-compare"
	ObjectTypeCryptodevBackend        ObjectType = "cryptodev-backend"
	ObjectTypeCryptodevBackendBuiltin ObjectType = "cryptodev-backend-builtin"
	ObjectTypeCryptodevBackendLkcf    ObjectType = "cryptodev-backend-lkcf"
	ObjectTypeCryptodevVhostUser      ObjectType = "cryptodev-vhost-user"
	ObjectTypeDbusVmstate             ObjectType = "dbus-vmstate"
	ObjectTypeFilterBuffer            ObjectType = "filter-buffer"
	ObjectTypeFilterDump              ObjectType = "filter-dump"
	ObjectTypeFilterMirror            ObjectType = "filter-mirror"
	ObjectTypeFilterRedirector        ObjectType = "filter-redirector"
	ObjectTypeFilterReplay            ObjectType = "filter-replay"
	ObjectTypeFilterRewriter          ObjectType = "filter-rewriter"
	ObjectTypeInputBarrier            ObjectType = "input-barrier"
	ObjectTypeInputLinux              ObjectType = "input-linux"
	ObjectTypeIommufd                 ObjectType = "iommufd"
	ObjectTypeIothread                ObjectType = "iothread"
	ObjectTypeMainLoop                ObjectType = "main-loop"
	ObjectTypeMemoryBackendEpc        ObjectType = "memory-backend-epc"
	ObjectTypeMemoryBackendFile       ObjectType = "memory-backend-file"
	ObjectTypeMemoryBackendMemfd      ObjectType = "memory-backend-memfd"
	ObjectTypeMemoryBackendRam        ObjectType = "memory-backend-ram"
	ObjectTypeMemoryBackendShm        ObjectType = "memory-backend-shm"
	ObjectTypePefGuest                ObjectType = "pef-guest"
	ObjectTypePrManagerHelper         ObjectType = "pr-manager-helper"
	ObjectTypeQtest                   ObjectType = "qtest"
	ObjectTypeRngBuiltin              ObjectType = "rng-builtin"
	ObjectTypeRngEgd                  ObjectType = "rng-egd"
	ObjectTypeRngRandom               ObjectType = "rng-random"
	ObjectTypeSecret                  ObjectType = "secret"
	ObjectTypeSecret_Keyring          ObjectType = "secret_keyring"
	ObjectTypeSevGuest                ObjectType = "sev-guest"
	ObjectTypeSevSnpGuest             ObjectType = "sev-snp-guest"
	ObjectTypeThreadContext           ObjectType = "thread-context"
	ObjectTypeS390PvGuest             ObjectType = "s390-pv-guest"
	ObjectTypeThrottleGroup           ObjectType = "throttle-group"
	ObjectTypeTlsCredsAnon            ObjectType = "tls-creds-anon"
	ObjectTypeTlsCredsPsk             ObjectType = "tls-creds-psk"
	ObjectTypeTlsCredsX509            ObjectType = "tls-creds-x509"
	ObjectTypeTlsCipherSuites         ObjectType = "tls-cipher-suites"
	ObjectTypeXRemoteObject           ObjectType = "x-remote-object"
	ObjectTypeXVfioUserServer         ObjectType = "x-vfio-user-server"
)

// An enumeration of options for specifying a PCI BAR
//
// Since: 2.12
type OffAutoPCIBAR string

const (
	// The specified feature is disabled
	OffAutoPCIBAROff OffAutoPCIBAR = "off"
	// The PCI BAR for the feature is automatically selected
	OffAutoPCIBARAuto OffAutoPCIBAR = "auto"
	// PCI BAR0 is used for the feature
	OffAutoPCIBARBar0 OffAutoPCIBAR = "bar0"
	// PCI BAR1 is used for the feature
	OffAutoPCIBARBar1 OffAutoPCIBAR = "bar1"
	// PCI BAR2 is used for the feature
	OffAutoPCIBARBar2 OffAutoPCIBAR = "bar2"
	// PCI BAR3 is used for the feature
	OffAutoPCIBARBar3 OffAutoPCIBAR = "bar3"
	// PCI BAR4 is used for the feature
	OffAutoPCIBARBar4 OffAutoPCIBAR = "bar4"
	// PCI BAR5 is used for the feature
	OffAutoPCIBARBar5 OffAutoPCIBAR = "bar5"
)

// An enumeration of possible behaviors for copy-before-write
// operation failures.
//
// Since: 7.1
type OnCbwError string

const (
	// report the error to the guest. This way, the guest will not be able to overwrite
	// areas that cannot be backed up, so the backup process remains valid.
	OnCbwErrorBreakGuestWrite OnCbwError = "break-guest-write"
	// continue guest write. Doing so will make the provided snapshot state invalid and
	// any backup or export process based on it will finally fail.
	OnCbwErrorBreakSnapshot OnCbwError = "break-snapshot"
)

// An enumeration of three options: on, off, and auto
//
// Since: 2.2
type OnOffAuto string

const (
	// QEMU selects the value between on and off
	OnOffAutoAuto OnOffAuto = "auto"
	// Enabled
	OnOffAutoOn OnOffAuto = "on"
	// Disabled
	OnOffAutoOff OnOffAuto = "off"
)

// An enumeration of three values: on, off, and split
//
// Since: 2.6
type OnOffSplit string

const (
	// Enabled
	OnOffSplitOn OnOffSplit = "on"
	// Disabled
	OnOffSplitOff OnOffSplit = "off"
	// Mixed
	OnOffSplitSplit OnOffSplit = "split"
)

// An enumeration of PCIe link speeds in units of GT/s
//
// Since: 4.0
type PCIELinkSpeed string

const (
	// 2.5GT/s
	PCIELinkSpeed2_5 PCIELinkSpeed = "2_5"
	// 5.0GT/s
	PCIELinkSpeed5 PCIELinkSpeed = "5"
	// 8.0GT/s
	PCIELinkSpeed8 PCIELinkSpeed = "8"
	// 16.0GT/s
	PCIELinkSpeed16 PCIELinkSpeed = "16"
	// 32.0GT/s (since 9.0)
	PCIELinkSpeed32 PCIELinkSpeed = "32"
	// 64.0GT/s (since 9.0)
	PCIELinkSpeed64 PCIELinkSpeed = "64"
)

// An enumeration of PCIe link width
//
// Since: 4.0
type PCIELinkWidth string

const (
	// x1
	PCIELinkWidth1 PCIELinkWidth = "1"
	// x2
	PCIELinkWidth2 PCIELinkWidth = "2"
	// x4
	PCIELinkWidth4 PCIELinkWidth = "4"
	// x8
	PCIELinkWidth8 PCIELinkWidth = "8"
	// x12
	PCIELinkWidth12 PCIELinkWidth = "12"
	// x16
	PCIELinkWidth16 PCIELinkWidth = "16"
	// x32
	PCIELinkWidth32 PCIELinkWidth = "32"
)

// Since: 6.0
type PanicAction string

const (
	// Pause the VM
	PanicActionPause PanicAction = "pause"
	// Shutdown the VM and exit, according to the shutdown action
	PanicActionShutdown PanicAction = "shutdown"
	// Shutdown the VM and exit with nonzero status (since 7.1)
	PanicActionExitFailure PanicAction = "exit-failure"
	// Continue VM execution
	PanicActionNone PanicAction = "none"
)

// Preallocation mode of QEMU image file
//
// Since: 2.2
type PreallocMode string

const (
	// no preallocation
	PreallocModeOff PreallocMode = "off"
	// preallocate only for metadata
	PreallocModeMetadata PreallocMode = "metadata"
	// like @full preallocation but allocate disk space by posix_fallocate() rather
	// than writing data.
	PreallocModeFalloc PreallocMode = "falloc"
	// preallocate all data by writing it to the device to ensure disk space is really
	// available. This data may or may not be zero, depending on the image format and
	// storage. @full preallocation also sets up metadata correctly.
	PreallocModeFull PreallocMode = "full"
)

// The authorization policy match format
//
// Since: 4.0
type QAuthZListFormat string

const (
	// an exact string match
	QAuthZListFormatExact QAuthZListFormat = "exact"
	// string with ? and * shell wildcard support
	QAuthZListFormatGlob QAuthZListFormat = "glob"
)

// The authorization policy result
//
// Since: 4.0
type QAuthZListPolicy string

const (
	// deny access
	QAuthZListPolicyDeny QAuthZListPolicy = "deny"
	// allow access
	QAuthZListPolicyAllow QAuthZListPolicy = "allow"
)

// The supported algorithms for asymmetric encryption ciphers
//
// Since: 7.1
type QCryptoAkCipherAlgo string

const (
	// RSA algorithm
	QCryptoAkCipherAlgoRsa QCryptoAkCipherAlgo = "rsa"
)

// The type of asymmetric keys.
//
// Since: 7.1
type QCryptoAkCipherKeyType string

const (
	// public key
	QCryptoAkCipherKeyTypePublic QCryptoAkCipherKeyType = "public"
	// private key
	QCryptoAkCipherKeyTypePrivate QCryptoAkCipherKeyType = "private"
)

// The supported full disk encryption formats
//
// Since: 2.6
type QCryptoBlockFormat string

const (
	// QCow/QCow2 built-in AES-CBC encryption. Use only for liberating data from old
	// images.
	QCryptoBlockFormatQcow QCryptoBlockFormat = "qcow"
	// LUKS encryption format. Recommended for new images
	QCryptoBlockFormatLuks QCryptoBlockFormat = "luks"
)

// Defines state of keyslots that are affected by the update
//
// Since: 5.1
type QCryptoBlockLUKSKeyslotState string

const (
	// The slots contain the given password and marked as active
	QCryptoBlockLUKSKeyslotStateActive QCryptoBlockLUKSKeyslotState = "active"
	// The slots are erased (contain garbage) and marked as inactive
	QCryptoBlockLUKSKeyslotStateInactive QCryptoBlockLUKSKeyslotState = "inactive"
)

// The supported algorithms for content encryption ciphers
//
// Since: 2.6
type QCryptoCipherAlgo string

const (
	// AES with 128 bit / 16 byte keys
	QCryptoCipherAlgoAes128 QCryptoCipherAlgo = "aes-128"
	// AES with 192 bit / 24 byte keys
	QCryptoCipherAlgoAes192 QCryptoCipherAlgo = "aes-192"
	// AES with 256 bit / 32 byte keys
	QCryptoCipherAlgoAes256 QCryptoCipherAlgo = "aes-256"
	// DES with 56 bit / 8 byte keys. Do not use except in VNC. (since 6.1)
	QCryptoCipherAlgoDes QCryptoCipherAlgo = "des"
	// 3DES(EDE) with 192 bit / 24 byte keys (since 2.9)
	QCryptoCipherAlgo3Des QCryptoCipherAlgo = "3des"
	// Cast5 with 128 bit / 16 byte keys
	QCryptoCipherAlgoCast5128 QCryptoCipherAlgo = "cast5-128"
	// Serpent with 128 bit / 16 byte keys
	QCryptoCipherAlgoSerpent128 QCryptoCipherAlgo = "serpent-128"
	// Serpent with 192 bit / 24 byte keys
	QCryptoCipherAlgoSerpent192 QCryptoCipherAlgo = "serpent-192"
	// Serpent with 256 bit / 32 byte keys
	QCryptoCipherAlgoSerpent256 QCryptoCipherAlgo = "serpent-256"
	// Twofish with 128 bit / 16 byte keys
	QCryptoCipherAlgoTwofish128 QCryptoCipherAlgo = "twofish-128"
	// Twofish with 192 bit / 24 byte keys
	QCryptoCipherAlgoTwofish192 QCryptoCipherAlgo = "twofish-192"
	// Twofish with 256 bit / 32 byte keys
	QCryptoCipherAlgoTwofish256 QCryptoCipherAlgo = "twofish-256"
	// SM4 with 128 bit / 16 byte keys (since 9.0)
	QCryptoCipherAlgoSm4 QCryptoCipherAlgo = "sm4"
)

// The supported modes for content encryption ciphers
//
// Since: 2.6
type QCryptoCipherMode string

const (
	// Electronic Code Book
	QCryptoCipherModeEcb QCryptoCipherMode = "ecb"
	// Cipher Block Chaining
	QCryptoCipherModeCbc QCryptoCipherMode = "cbc"
	// XEX with tweaked code book and ciphertext stealing
	QCryptoCipherModeXts QCryptoCipherMode = "xts"
	// Counter (Since 2.8)
	QCryptoCipherModeCtr QCryptoCipherMode = "ctr"
)

// The supported algorithms for computing content digests
//
// Since: 2.6
type QCryptoHashAlgo string

const (
	// MD5. Should not be used in any new code, legacy compat only
	QCryptoHashAlgoMd5 QCryptoHashAlgo = "md5"
	// SHA-1. Should not be used in any new code, legacy compat only
	QCryptoHashAlgoSha1 QCryptoHashAlgo = "sha1"
	// SHA-224. (since 2.7)
	QCryptoHashAlgoSha224 QCryptoHashAlgo = "sha224"
	// SHA-256. Current recommended strong hash.
	QCryptoHashAlgoSha256 QCryptoHashAlgo = "sha256"
	// SHA-384. (since 2.7)
	QCryptoHashAlgoSha384 QCryptoHashAlgo = "sha384"
	// SHA-512. (since 2.7)
	QCryptoHashAlgoSha512 QCryptoHashAlgo = "sha512"
	// RIPEMD-160. (since 2.7)
	QCryptoHashAlgoRipemd160 QCryptoHashAlgo = "ripemd160"
	// SM3. (since 9.2.0)
	QCryptoHashAlgoSm3 QCryptoHashAlgo = "sm3"
)

// The supported algorithms for generating initialization vectors for
// full disk encryption.  The 'plain' generator should not be used for
// disks with sector numbers larger than 2^32, except where
// compatibility with pre-existing Linux dm-crypt volumes is required.
//
// Since: 2.6
type QCryptoIVGenAlgo string

const (
	// 64-bit sector number truncated to 32-bits
	QCryptoIVGenAlgoPlain QCryptoIVGenAlgo = "plain"
	// 64-bit sector number
	QCryptoIVGenAlgoPlain64 QCryptoIVGenAlgo = "plain64"
	// 64-bit sector number encrypted with a hash of the encryption key
	QCryptoIVGenAlgoEssiv QCryptoIVGenAlgo = "essiv"
)

// The padding algorithm for RSA.
//
// Since: 7.1
type QCryptoRSAPaddingAlgo string

const (
	// no padding used
	QCryptoRSAPaddingAlgoRaw QCryptoRSAPaddingAlgo = "raw"
	// pkcs1#v1.5
	QCryptoRSAPaddingAlgoPkcs1 QCryptoRSAPaddingAlgo = "pkcs1"
)

// The data format that the secret is provided in
//
// Since: 2.6
type QCryptoSecretFormat string

const (
	// raw bytes. When encoded in JSON only valid UTF-8 sequences can be used
	QCryptoSecretFormatRaw QCryptoSecretFormat = "raw"
	// arbitrary base64 encoded binary data
	QCryptoSecretFormatBase64 QCryptoSecretFormat = "base64"
)

// The type of network endpoint that will be using the credentials.
// Most types of credential require different setup / structures
// depending on whether they will be used in a server versus a client.
//
// Since: 2.5
type QCryptoTLSCredsEndpoint string

const (
	// the network endpoint is acting as the client
	QCryptoTLSCredsEndpointClient QCryptoTLSCredsEndpoint = "client"
	// the network endpoint is acting as the server
	QCryptoTLSCredsEndpointServer QCryptoTLSCredsEndpoint = "server"
)

// The supported algorithm types of a crypto device.
//
// Since: 8.0
type QCryptodevBackendAlgoType string

const (
	// symmetric encryption
	QCryptodevBackendAlgoTypeSym QCryptodevBackendAlgoType = "sym"
	// asymmetric Encryption
	QCryptodevBackendAlgoTypeAsym QCryptodevBackendAlgoType = "asym"
)

// The supported service types of a crypto device.
//
// Since: 8.0
type QCryptodevBackendServiceType string

const (
	// Symmetric Key Cipher service
	QCryptodevBackendServiceTypeCipher QCryptodevBackendServiceType = "cipher"
	// Hash service
	QCryptodevBackendServiceTypeHash QCryptodevBackendServiceType = "hash"
	// Message Authentication Codes service
	QCryptodevBackendServiceTypeMac QCryptodevBackendServiceType = "mac"
	// Authenticated Encryption with Associated Data service
	QCryptodevBackendServiceTypeAead QCryptodevBackendServiceType = "aead"
	// Asymmetric Key Cipher service
	QCryptodevBackendServiceTypeAkcipher QCryptodevBackendServiceType = "akcipher"
)

// The crypto device backend type
//
// Since: 8.0
type QCryptodevBackendType string

const (
	// the QEMU builtin support
	QCryptodevBackendTypeBuiltin QCryptodevBackendType = "builtin"
	// vhost-user
	QCryptodevBackendTypeVhostUser QCryptodevBackendType = "vhost-user"
	// Linux kernel cryptographic framework
	QCryptodevBackendTypeLkcf QCryptodevBackendType = "lkcf"
)

// An enumeration of key name.  This is used by the @send-key command.
//
// 'sysrq' was mistakenly added to hack around the fact that the ps2
// driver was not generating correct scancodes sequences when
// 'alt+print' was pressed. This flaw is now fixed and the 'sysrq' key
// serves no further purpose. Any further use of 'sysrq' will be
// transparently changed to 'print', so they are effectively synonyms.
//
// Since: 1.3
type QKeyCode string

const (
	// since 2.0
	QKeyCodeUnmapped      QKeyCode = "unmapped"
	QKeyCodeShift         QKeyCode = "shift"
	QKeyCodeShift_R       QKeyCode = "shift_r"
	QKeyCodeAlt           QKeyCode = "alt"
	QKeyCodeAlt_R         QKeyCode = "alt_r"
	QKeyCodeCtrl          QKeyCode = "ctrl"
	QKeyCodeCtrl_R        QKeyCode = "ctrl_r"
	QKeyCodeMenu          QKeyCode = "menu"
	QKeyCodeEsc           QKeyCode = "esc"
	QKeyCode1             QKeyCode = "1"
	QKeyCode2             QKeyCode = "2"
	QKeyCode3             QKeyCode = "3"
	QKeyCode4             QKeyCode = "4"
	QKeyCode5             QKeyCode = "5"
	QKeyCode6             QKeyCode = "6"
	QKeyCode7             QKeyCode = "7"
	QKeyCode8             QKeyCode = "8"
	QKeyCode9             QKeyCode = "9"
	QKeyCode0             QKeyCode = "0"
	QKeyCodeMinus         QKeyCode = "minus"
	QKeyCodeEqual         QKeyCode = "equal"
	QKeyCodeBackspace     QKeyCode = "backspace"
	QKeyCodeTab           QKeyCode = "tab"
	QKeyCodeQ             QKeyCode = "q"
	QKeyCodeW             QKeyCode = "w"
	QKeyCodeE             QKeyCode = "e"
	QKeyCodeR             QKeyCode = "r"
	QKeyCodeT             QKeyCode = "t"
	QKeyCodeY             QKeyCode = "y"
	QKeyCodeU             QKeyCode = "u"
	QKeyCodeI             QKeyCode = "i"
	QKeyCodeO             QKeyCode = "o"
	QKeyCodeP             QKeyCode = "p"
	QKeyCodeBracket_Left  QKeyCode = "bracket_left"
	QKeyCodeBracket_Right QKeyCode = "bracket_right"
	QKeyCodeRet           QKeyCode = "ret"
	QKeyCodeA             QKeyCode = "a"
	QKeyCodeS             QKeyCode = "s"
	QKeyCodeD             QKeyCode = "d"
	QKeyCodeF             QKeyCode = "f"
	QKeyCodeG             QKeyCode = "g"
	QKeyCodeH             QKeyCode = "h"
	QKeyCodeJ             QKeyCode = "j"
	QKeyCodeK             QKeyCode = "k"
	QKeyCodeL             QKeyCode = "l"
	QKeyCodeSemicolon     QKeyCode = "semicolon"
	QKeyCodeApostrophe    QKeyCode = "apostrophe"
	QKeyCodeGrave_Accent  QKeyCode = "grave_accent"
	QKeyCodeBackslash     QKeyCode = "backslash"
	QKeyCodeZ             QKeyCode = "z"
	QKeyCodeX             QKeyCode = "x"
	QKeyCodeC             QKeyCode = "c"
	QKeyCodeV             QKeyCode = "v"
	QKeyCodeB             QKeyCode = "b"
	QKeyCodeN             QKeyCode = "n"
	QKeyCodeM             QKeyCode = "m"
	QKeyCodeComma         QKeyCode = "comma"
	QKeyCodeDot           QKeyCode = "dot"
	QKeyCodeSlash         QKeyCode = "slash"
	QKeyCodeAsterisk      QKeyCode = "asterisk"
	QKeyCodeSpc           QKeyCode = "spc"
	QKeyCodeCaps_Lock     QKeyCode = "caps_lock"
	QKeyCodeF1            QKeyCode = "f1"
	QKeyCodeF2            QKeyCode = "f2"
	QKeyCodeF3            QKeyCode = "f3"
	QKeyCodeF4            QKeyCode = "f4"
	QKeyCodeF5            QKeyCode = "f5"
	QKeyCodeF6            QKeyCode = "f6"
	QKeyCodeF7            QKeyCode = "f7"
	QKeyCodeF8            QKeyCode = "f8"
	QKeyCodeF9            QKeyCode = "f9"
	QKeyCodeF10           QKeyCode = "f10"
	QKeyCodeNum_Lock      QKeyCode = "num_lock"
	QKeyCodeScroll_Lock   QKeyCode = "scroll_lock"
	QKeyCodeKp_Divide     QKeyCode = "kp_divide"
	QKeyCodeKp_Multiply   QKeyCode = "kp_multiply"
	QKeyCodeKp_Subtract   QKeyCode = "kp_subtract"
	QKeyCodeKp_Add        QKeyCode = "kp_add"
	QKeyCodeKp_Enter      QKeyCode = "kp_enter"
	QKeyCodeKp_Decimal    QKeyCode = "kp_decimal"
	QKeyCodeSysrq         QKeyCode = "sysrq"
	QKeyCodeKp_0          QKeyCode = "kp_0"
	QKeyCodeKp_1          QKeyCode = "kp_1"
	QKeyCodeKp_2          QKeyCode = "kp_2"
	QKeyCodeKp_3          QKeyCode = "kp_3"
	QKeyCodeKp_4          QKeyCode = "kp_4"
	QKeyCodeKp_5          QKeyCode = "kp_5"
	QKeyCodeKp_6          QKeyCode = "kp_6"
	QKeyCodeKp_7          QKeyCode = "kp_7"
	QKeyCodeKp_8          QKeyCode = "kp_8"
	QKeyCodeKp_9          QKeyCode = "kp_9"
	QKeyCodeLess          QKeyCode = "less"
	QKeyCodeF11           QKeyCode = "f11"
	QKeyCodeF12           QKeyCode = "f12"
	QKeyCodePrint         QKeyCode = "print"
	QKeyCodeHome          QKeyCode = "home"
	QKeyCodePgup          QKeyCode = "pgup"
	QKeyCodePgdn          QKeyCode = "pgdn"
	QKeyCodeEnd           QKeyCode = "end"
	QKeyCodeLeft          QKeyCode = "left"
	QKeyCodeUp            QKeyCode = "up"
	QKeyCodeDown          QKeyCode = "down"
	QKeyCodeRight         QKeyCode = "right"
	QKeyCodeInsert        QKeyCode = "insert"
	QKeyCodeDelete        QKeyCode = "delete"
	QKeyCodeStop          QKeyCode = "stop"
	QKeyCodeAgain         QKeyCode = "again"
	QKeyCodeProps         QKeyCode = "props"
	QKeyCodeUndo          QKeyCode = "undo"
	QKeyCodeFront         QKeyCode = "front"
	QKeyCodeCopy          QKeyCode = "copy"
	QKeyCodeOpen          QKeyCode = "open"
	QKeyCodePaste         QKeyCode = "paste"
	QKeyCodeFind          QKeyCode = "find"
	QKeyCodeCut           QKeyCode = "cut"
	QKeyCodeLf            QKeyCode = "lf"
	QKeyCodeHelp          QKeyCode = "help"
	QKeyCodeMeta_L        QKeyCode = "meta_l"
	QKeyCodeMeta_R        QKeyCode = "meta_r"
	QKeyCodeCompose       QKeyCode = "compose"
	// since 2.0
	QKeyCodePause QKeyCode = "pause"
	// since 2.4
	QKeyCodeRo QKeyCode = "ro"
	// since 2.9
	QKeyCodeHiragana QKeyCode = "hiragana"
	// since 2.9
	QKeyCodeHenkan QKeyCode = "henkan"
	// since 2.9
	QKeyCodeYen QKeyCode = "yen"
	// since 2.12
	QKeyCodeMuhenkan QKeyCode = "muhenkan"
	// since 2.12
	QKeyCodeKatakanahiragana QKeyCode = "katakanahiragana"
	// since 2.4
	QKeyCodeKp_Comma QKeyCode = "kp_comma"
	// since 2.6
	QKeyCodeKp_Equals QKeyCode = "kp_equals"
	// since 2.6
	QKeyCodePower QKeyCode = "power"
	// since 2.10
	QKeyCodeSleep QKeyCode = "sleep"
	// since 2.10
	QKeyCodeWake QKeyCode = "wake"
	// since 2.10
	QKeyCodeAudionext QKeyCode = "audionext"
	// since 2.10
	QKeyCodeAudioprev QKeyCode = "audioprev"
	// since 2.10
	QKeyCodeAudiostop QKeyCode = "audiostop"
	// since 2.10
	QKeyCodeAudioplay QKeyCode = "audioplay"
	// since 2.10
	QKeyCodeAudiomute QKeyCode = "audiomute"
	// since 2.10
	QKeyCodeVolumeup QKeyCode = "volumeup"
	// since 2.10
	QKeyCodeVolumedown QKeyCode = "volumedown"
	// since 2.10
	QKeyCodeMediaselect QKeyCode = "mediaselect"
	// since 2.10
	QKeyCodeMail QKeyCode = "mail"
	// since 2.10
	QKeyCodeCalculator QKeyCode = "calculator"
	// since 2.10
	QKeyCodeComputer QKeyCode = "computer"
	// since 2.10
	QKeyCodeAc_Home QKeyCode = "ac_home"
	// since 2.10
	QKeyCodeAc_Back QKeyCode = "ac_back"
	// since 2.10
	QKeyCodeAc_Forward QKeyCode = "ac_forward"
	// since 2.10
	QKeyCodeAc_Refresh QKeyCode = "ac_refresh"
	// since 2.10
	QKeyCodeAc_Bookmarks QKeyCode = "ac_bookmarks"
	// since 6.1
	QKeyCodeLang1 QKeyCode = "lang1"
	// since 6.1
	QKeyCodeLang2 QKeyCode = "lang2"
	// since 8.0
	QKeyCodeF13 QKeyCode = "f13"
	// since 8.0
	QKeyCodeF14 QKeyCode = "f14"
	// since 8.0
	QKeyCodeF15 QKeyCode = "f15"
	// since 8.0
	QKeyCodeF16 QKeyCode = "f16"
	// since 8.0
	QKeyCodeF17 QKeyCode = "f17"
	// since 8.0
	QKeyCodeF18 QKeyCode = "f18"
	// since 8.0
	QKeyCodeF19 QKeyCode = "f19"
	// since 8.0
	QKeyCodeF20 QKeyCode = "f20"
	// since 8.0
	QKeyCodeF21 QKeyCode = "f21"
	// since 8.0
	QKeyCodeF22 QKeyCode = "f22"
	// since 8.0
	QKeyCodeF23 QKeyCode = "f23"
	// since 8.0
	QKeyCodeF24 QKeyCode = "f24"
)

// Enumeration of capabilities to be advertised during initial client
// connection, used for agreeing on particular QMP extension
// behaviors.
//
// Since: 2.12
type QMPCapability string

const (
	// QMP ability to support out-of-band requests. (Please refer to qmp-spec.rst for
	// more information on OOB)
	QMPCapabilityOob QMPCapability = "oob"
)

type QType string

const (
	QTypeNone    QType = "none"
	QTypeQnull   QType = "qnull"
	QTypeQnum    QType = "qnum"
	QTypeQstring QType = "qstring"
	QTypeQdict   QType = "qdict"
	QTypeQlist   QType = "qlist"
	QTypeQbool   QType = "qbool"
)

// QEMU error classes
//
// Since: 1.2
type QapiErrorClass string

const (
	// this is used for errors that don't require a specific error class. This should
	// be the default case for most errors
	QapiErrorClassGenericerror QapiErrorClass = "GenericError"
	// the requested command has not been found
	QapiErrorClassCommandnotfound QapiErrorClass = "CommandNotFound"
	// a device has failed to be become active
	QapiErrorClassDevicenotactive QapiErrorClass = "DeviceNotActive"
	// the requested device has not been found
	QapiErrorClassDevicenotfound QapiErrorClass = "DeviceNotFound"
	// the requested operation can't be fulfilled because a required KVM capability is
	// missing
	QapiErrorClassKvmmissingcap QapiErrorClass = "KVMMissingCap"
)

// An enumeration of the VFIO device migration states.
//
// Since: 9.1
type QapiVfioMigrationState string

const (
	// The device is stopped.
	QapiVfioMigrationStateStop QapiVfioMigrationState = "stop"
	// The device is running.
	QapiVfioMigrationStateRunning QapiVfioMigrationState = "running"
	// The device is stopped and its internal state is available for reading.
	QapiVfioMigrationStateStopCopy QapiVfioMigrationState = "stop-copy"
	// The device is stopped and its internal state is available for writing.
	QapiVfioMigrationStateResuming QapiVfioMigrationState = "resuming"
	// The device is running in the P2P quiescent state.
	QapiVfioMigrationStateRunningP2P QapiVfioMigrationState = "running-p2p"
	// The device is running, tracking its internal state and its internal state is
	// available for reading.
	QapiVfioMigrationStatePreCopy QapiVfioMigrationState = "pre-copy"
	// The device is running in the P2P quiescent state, tracking its internal state
	// and its internal state is available for reading.
	QapiVfioMigrationStatePreCopyP2P QapiVfioMigrationState = "pre-copy-p2p"
)

// An enumeration of flags that a bitmap can report to the user.
//
// Since: 4.0
type Qcow2BitmapInfoFlags string

const (
	// This flag is set by any process actively modifying the qcow2 file, and cleared
	// when the updated bitmap is flushed to the qcow2 image. The presence of this flag
	// in an offline image means that the bitmap was not saved correctly after its last
	// usage, and may contain inconsistent data.
	Qcow2BitmapInfoFlagsInUse Qcow2BitmapInfoFlags = "in-use"
	// The bitmap must reflect all changes of the virtual disk by any application that
	// would write to this qcow2 file.
	Qcow2BitmapInfoFlagsAuto Qcow2BitmapInfoFlags = "auto"
)

// Compression type used in qcow2 image file
//
// Since: 5.1
type Qcow2CompressionType string

const (
	// zlib compression, see <http://zlib.net/>
	Qcow2CompressionTypeZlib Qcow2CompressionType = "zlib"
	// zstd compression, see <http://github.com/facebook/zstd>
	Qcow2CompressionTypeZstd Qcow2CompressionType = "zstd"
)

// General overlap check modes.
//
// Since: 2.9
type Qcow2OverlapCheckMode string

const (
	// Do not perform any checks
	Qcow2OverlapCheckModeNone Qcow2OverlapCheckMode = "none"
	// Perform only checks which can be done in constant time and without reading
	// anything from disk
	Qcow2OverlapCheckModeConstant Qcow2OverlapCheckMode = "constant"
	// Perform only checks which can be done without reading anything from disk
	Qcow2OverlapCheckModeCached Qcow2OverlapCheckMode = "cached"
	// Perform all available overlap checks
	Qcow2OverlapCheckModeAll Qcow2OverlapCheckMode = "all"
)

// An enumeration of the quorum operation types
//
// Since: 2.6
type QuorumOpType string

const (
	// read operation
	QuorumOpTypeRead QuorumOpType = "read"
	// write operation
	QuorumOpTypeWrite QuorumOpType = "write"
	// flush operation
	QuorumOpTypeFlush QuorumOpType = "flush"
)

// An enumeration of quorum read patterns.
//
// Since: 2.9
type QuorumReadPattern string

const (
	// read all the children and do a quorum vote on reads
	QuorumReadPatternQuorum QuorumReadPattern = "quorum"
	// read only from the first child that has not failed
	QuorumReadPatternFifo QuorumReadPattern = "fifo"
)

// Since: 3.0
type RbdAuthMode string

const (
	RbdAuthModeCephx RbdAuthMode = "cephx"
	RbdAuthModeNone  RbdAuthMode = "none"
)

// Since: 6.1
type RbdImageEncryptionFormat string

const (
	RbdImageEncryptionFormatLuks  RbdImageEncryptionFormat = "luks"
	RbdImageEncryptionFormatLuks2 RbdImageEncryptionFormat = "luks2"
	// Used for opening either luks or luks2 (Since 8.0)
	RbdImageEncryptionFormatLuksAny RbdImageEncryptionFormat = "luks-any"
)

// Possible QEMU actions upon guest reboot
//
// Since: 6.0
type RebootAction string

const (
	// Reset the VM
	RebootActionReset RebootAction = "reset"
	// Shutdown the VM and exit, according to the shutdown action
	RebootActionShutdown RebootAction = "shutdown"
)

// Mode of the replay subsystem.
//
// Since: 2.5
type ReplayMode string

const (
	// normal execution mode. Replay or record are not enabled.
	ReplayModeNone ReplayMode = "none"
	// record mode. All non-deterministic data is written into the replay log.
	ReplayModeRecord ReplayMode = "record"
	// replay mode. Non-deterministic data required for system execution is read from
	// the log.
	ReplayModePlay ReplayMode = "play"
)

// An enumeration of replication modes.
//
// Since: 2.9
type ReplicationMode string

const (
	// Primary mode, the vm's state will be sent to secondary QEMU.
	ReplicationModePrimary ReplicationMode = "primary"
	// Secondary mode, receive the vm's state from primary QEMU.
	ReplicationModeSecondary ReplicationMode = "secondary"
)

// An enumeration of port autoneg states.
//
// Since: 2.4
type RockerPortAutoneg string

const (
	// autoneg is off
	RockerPortAutonegOff RockerPortAutoneg = "off"
	// autoneg is on
	RockerPortAutonegOn RockerPortAutoneg = "on"
)

// An enumeration of port duplex states.
//
// Since: 2.4
type RockerPortDuplex string

const (
	// half duplex
	RockerPortDuplexHalf RockerPortDuplex = "half"
	// full duplex
	RockerPortDuplexFull RockerPortDuplex = "full"
)

// An enumeration of VM run states.
type RunState string

const (
	// QEMU is running on a debugger
	RunStateDebug RunState = "debug"
	// guest is paused waiting for an incoming migration. Note that this state does not
	// tell whether the machine will start at the end of the migration. This depends on
	// the command-line -S option and any invocation of 'stop' or 'cont' that has
	// happened since QEMU was started.
	RunStateInmigrate RunState = "inmigrate"
	// An internal error that prevents further guest execution has occurred
	RunStateInternalError RunState = "internal-error"
	// the last IOP has failed and the device is configured to pause on I/O errors
	RunStateIoError RunState = "io-error"
	// guest has been paused via the 'stop' command
	RunStatePaused RunState = "paused"
	// guest is paused following a successful 'migrate'
	RunStatePostmigrate RunState = "postmigrate"
	// QEMU was started with -S and guest has not started
	RunStatePrelaunch RunState = "prelaunch"
	// guest is paused to finish the migration process
	RunStateFinishMigrate RunState = "finish-migrate"
	// guest is paused to restore VM state
	RunStateRestoreVm RunState = "restore-vm"
	// guest is actively running
	RunStateRunning RunState = "running"
	// guest is paused to save the VM state
	RunStateSaveVm RunState = "save-vm"
	// guest is shut down (and -no-shutdown is in use)
	RunStateShutdown RunState = "shutdown"
	// guest is suspended (ACPI S3)
	RunStateSuspended RunState = "suspended"
	// the watchdog action is configured to pause and has been triggered
	RunStateWatchdog RunState = "watchdog"
	// guest has been panicked as a result of guest OS panic
	RunStateGuestPanicked RunState = "guest-panicked"
	// guest is paused to save/restore VM state under colo checkpoint, VM can not get
	// into this state unless colo capability is enabled for migration. (since 2.8)
	RunStateColo RunState = "colo"
)

// Packets receiving state
//
// Since: 1.6
type RxState string

const (
	// filter assigned packets according to the mac-table
	RxStateNormal RxState = "normal"
	// don't receive any assigned packet
	RxStateNone RxState = "none"
	// receive all assigned packets
	RxStateAll RxState = "all"
)

// An enumeration of CPU entitlements that can be assumed by a virtual
// S390 CPU
//
// Since: 8.2
type S390CpuEntitlement string

const (
	S390CpuEntitlementAuto   S390CpuEntitlement = "auto"
	S390CpuEntitlementLow    S390CpuEntitlement = "low"
	S390CpuEntitlementMedium S390CpuEntitlement = "medium"
	S390CpuEntitlementHigh   S390CpuEntitlement = "high"
)

// An enumeration of CPU polarization that can be assumed by a virtual
// S390 CPU
//
// Since: 8.2
type S390CpuPolarization string

const (
	S390CpuPolarizationHorizontal S390CpuPolarization = "horizontal"
	S390CpuPolarizationVertical   S390CpuPolarization = "vertical"
)

// An enumeration of cpu states that can be assumed by a virtual S390
// CPU
//
// Since: 2.12
type S390CpuState string

const (
	S390CpuStateUninitialized S390CpuState = "uninitialized"
	S390CpuStateStopped       S390CpuState = "stopped"
	S390CpuStateCheckStop     S390CpuState = "check-stop"
	S390CpuStateOperating     S390CpuState = "operating"
	S390CpuStateLoad          S390CpuState = "load"
)

// Reason why the CPU is in a crashed state.
//
// Since: 2.12
type S390CrashReason string

const (
	// no crash reason was set
	S390CrashReasonUnknown S390CrashReason = "unknown"
	// the CPU has entered a disabled wait state
	S390CrashReasonDisabledWait S390CrashReason = "disabled-wait"
	// clock comparator or cpu timer interrupt with new PSW enabled for external
	// interrupts
	S390CrashReasonExtintLoop S390CrashReason = "extint-loop"
	// program interrupt with BAD new PSW
	S390CrashReasonPgmintLoop S390CrashReason = "pgmint-loop"
	// operation exception interrupt with invalid code at the program interrupt new PSW
	S390CrashReasonOpintLoop S390CrashReason = "opint-loop"
)

// This is a @SchemaInfo's meta type, i.e. the kind of entity it
// describes.
//
// Since: 2.5
type SchemaMetaType string

const (
	// a predefined type such as 'int' or 'bool'.
	SchemaMetaTypeBuiltin SchemaMetaType = "builtin"
	// an enumeration type
	SchemaMetaTypeEnum SchemaMetaType = "enum"
	// an array type
	SchemaMetaTypeArray SchemaMetaType = "array"
	// an object type (struct or union)
	SchemaMetaTypeObject SchemaMetaType = "object"
	// an alternate type
	SchemaMetaTypeAlternate SchemaMetaType = "alternate"
	// a QMP command
	SchemaMetaTypeCommand SchemaMetaType = "command"
	// a QMP event
	SchemaMetaTypeEvent SchemaMetaType = "event"
)

// An action to take on changing a password on a connection with
// active clients.
//
// Since: 7.0
type SetPasswordAction string

const (
	// maintain existing clients
	SetPasswordActionKeep SetPasswordAction = "keep"
	// fail the command if clients are connected
	SetPasswordActionFail SetPasswordAction = "fail"
	// disconnect existing clients
	SetPasswordActionDisconnect SetPasswordAction = "disconnect"
)

// An enumeration indicating the type of SEV guest being run.
//
// Since: 6.2
type SevGuestType string

const (
	// The guest is a legacy SEV or SEV-ES guest.
	SevGuestTypeSev SevGuestType = "sev"
	// The guest is an SEV-SNP guest.
	SevGuestTypeSevSnp SevGuestType = "sev-snp"
)

// An enumeration of SEV state information used during @query-sev.
//
// Since: 2.12
type SevState string

const (
	// The guest is uninitialized.
	SevStateUninit SevState = "uninit"
	// The guest is currently being launched; plaintext data and register state is
	// being imported.
	SevStateLaunchUpdate SevState = "launch-update"
	// The guest is currently being launched; ciphertext data is being imported.
	SevStateLaunchSecret SevState = "launch-secret"
	// The guest is fully launched or migrated in.
	SevStateRunning SevState = "running"
	// The guest is currently being migrated out to another machine.
	SevStateSendUpdate SevState = "send-update"
	// The guest is currently being migrated from another machine.
	SevStateReceiveUpdate SevState = "receive-update"
)

// Possible QEMU actions upon guest shutdown
//
// Since: 6.0
type ShutdownAction string

const (
	// Shutdown the VM and exit
	ShutdownActionPoweroff ShutdownAction = "poweroff"
	// pause the VM
	ShutdownActionPause ShutdownAction = "pause"
)

// An enumeration of reasons for a Shutdown.
type ShutdownCause string

const (
	// No shutdown request pending
	ShutdownCauseNone ShutdownCause = "none"
	// An error prevents further use of guest
	ShutdownCauseHostError ShutdownCause = "host-error"
	// Reaction to the QMP command 'quit'
	ShutdownCauseHostQmpQuit ShutdownCause = "host-qmp-quit"
	// Reaction to the QMP command 'system_reset'
	ShutdownCauseHostQmpSystemReset ShutdownCause = "host-qmp-system-reset"
	// Reaction to a signal, such as SIGINT
	ShutdownCauseHostSignal ShutdownCause = "host-signal"
	// Reaction to a UI event, like window close
	ShutdownCauseHostUi ShutdownCause = "host-ui"
	// Guest shutdown/suspend request, via ACPI or other hardware-specific means
	ShutdownCauseGuestShutdown ShutdownCause = "guest-shutdown"
	// Guest reset request, and command line turns that into a shutdown
	ShutdownCauseGuestReset ShutdownCause = "guest-reset"
	// Guest panicked, and command line turns that into a shutdown
	ShutdownCauseGuestPanic ShutdownCause = "guest-panic"
	// Partial guest reset that does not trigger QMP events and ignores --no-reboot.
	// This is useful for sanitizing hypercalls on s390 that are used during
	// kexec/kdump/boot
	ShutdownCauseSubsystemReset ShutdownCause = "subsystem-reset"
	// A snapshot is being loaded by the record & replay subsystem. This value is used
	// only within QEMU. It doesn't occur in QMP. (since 7.2)
	ShutdownCauseSnapshotLoad ShutdownCause = "snapshot-load"
)

// Since: 7.0
type SmbiosEntryPointType string

const (
	// SMBIOS version 2.1 (32-bit) Entry Point
	SmbiosEntryPointType32 SmbiosEntryPointType = "32"
	// SMBIOS version 3.0 (64-bit) Entry Point
	SmbiosEntryPointType64 SmbiosEntryPointType = "64"
	// Either 2.x or 3.x SMBIOS version, 2.x if configuration can be described by it
	// and 3.x otherwise (since: 9.0)
	SmbiosEntryPointTypeAuto SmbiosEntryPointType = "auto"
)

// Available SocketAddress types
//
// Since: 2.9
type SocketAddressType string

const (
	// Internet address
	SocketAddressTypeInet SocketAddressType = "inet"
	// Unix domain socket
	SocketAddressTypeUnix SocketAddressType = "unix"
	// VMCI address
	SocketAddressTypeVsock SocketAddressType = "vsock"
	// Socket file descriptor
	SocketAddressTypeFd SocketAddressType = "fd"
)

// An enumeration of Spice mouse states.
//
// Since: 1.1
type SpiceQueryMouseMode string

const (
	// Mouse cursor position is determined by the client.
	SpiceQueryMouseModeClient SpiceQueryMouseMode = "client"
	// Mouse cursor position is determined by the server.
	SpiceQueryMouseModeServer SpiceQueryMouseMode = "server"
	// No information is available about mouse mode used by the spice server.
	SpiceQueryMouseModeUnknown SpiceQueryMouseMode = "unknown"
)

// Since: 2.12
type SshHostKeyCheckHashType string

const (
	// The given hash is an md5 hash
	SshHostKeyCheckHashTypeMd5 SshHostKeyCheckHashType = "md5"
	// The given hash is an sha1 hash
	SshHostKeyCheckHashTypeSha1 SshHostKeyCheckHashType = "sha1"
	// The given hash is an sha256 hash
	SshHostKeyCheckHashTypeSha256 SshHostKeyCheckHashType = "sha256"
)

// Since: 2.12
type SshHostKeyCheckMode string

const (
	// Don't check the host key at all
	SshHostKeyCheckModeNone SshHostKeyCheckMode = "none"
	// Compare the host key with a given hash
	SshHostKeyCheckModeHash SshHostKeyCheckMode = "hash"
	// Check the host key against the known_hosts file
	SshHostKeyCheckModeKnown_Hosts SshHostKeyCheckMode = "known_hosts"
)

// Enumeration of statistics providers.
//
// Since: 7.1
type StatsProvider string

const (
	// since 7.1
	StatsProviderKvm StatsProvider = "kvm"
	// since 8.0
	StatsProviderCryptodev StatsProvider = "cryptodev"
)

// The kinds of objects on which one can request statistics.
//
// Since: 7.1
type StatsTarget string

const (
	// statistics that apply to the entire virtual machine or the entire QEMU process.
	StatsTargetVm StatsTarget = "vm"
	// statistics that apply to a single virtual CPU.
	StatsTargetVcpu StatsTarget = "vcpu"
	// statistics that apply to a crypto device (since 8.0)
	StatsTargetCryptodev StatsTarget = "cryptodev"
)

// Enumeration of statistics types
//
// Since: 7.1
type StatsType string

const (
	// stat is cumulative; value can only increase.
	StatsTypeCumulative StatsType = "cumulative"
	// stat is instantaneous; value can increase or decrease.
	StatsTypeInstant StatsType = "instant"
	// stat is the peak value; value can only increase.
	StatsTypePeak StatsType = "peak"
	// stat is a linear histogram.
	StatsTypeLinearHistogram StatsType = "linear-histogram"
	// stat is a logarithmic histogram, with one bucket for each power of two.
	StatsTypeLog2Histogram StatsType = "log2-histogram"
)

// Enumeration of unit of measurement for statistics
//
// Since: 7.1
type StatsUnit string

const (
	// stat reported in bytes.
	StatsUnitBytes StatsUnit = "bytes"
	// stat reported in seconds.
	StatsUnitSeconds StatsUnit = "seconds"
	// stat reported in clock cycles.
	StatsUnitCycles StatsUnit = "cycles"
	// stat is a boolean value.
	StatsUnitBoolean StatsUnit = "boolean"
)

// The comprehensive enumeration of QEMU system emulation ("softmmu")
// targets.  Run "./configure --help" in the project root directory,
// and look for the \*-softmmu targets near the "--target-list"
// option. The individual target constants are not documented here,
// for the time being.
//
// .. note:: The resulting QMP strings can be appended to the   "qemu-
// system-" prefix to produce the corresponding QEMU   executable
// name. This is true even for "qemu-system-x86_64".
//
// Since: 3.0
type SysEmuTarget string

const (
	SysEmuTargetAarch64 SysEmuTarget = "aarch64"
	SysEmuTargetAlpha   SysEmuTarget = "alpha"
	SysEmuTargetArm     SysEmuTarget = "arm"
	// since 5.1
	SysEmuTargetAvr  SysEmuTarget = "avr"
	SysEmuTargetHppa SysEmuTarget = "hppa"
	SysEmuTargetI386 SysEmuTarget = "i386"
	// since 7.1
	SysEmuTargetLoongarch64  SysEmuTarget = "loongarch64"
	SysEmuTargetM68K         SysEmuTarget = "m68k"
	SysEmuTargetMicroblaze   SysEmuTarget = "microblaze"
	SysEmuTargetMicroblazeel SysEmuTarget = "microblazeel"
	SysEmuTargetMips         SysEmuTarget = "mips"
	SysEmuTargetMips64       SysEmuTarget = "mips64"
	SysEmuTargetMips64El     SysEmuTarget = "mips64el"
	SysEmuTargetMipsel       SysEmuTarget = "mipsel"
	SysEmuTargetOr1K         SysEmuTarget = "or1k"
	SysEmuTargetPpc          SysEmuTarget = "ppc"
	SysEmuTargetPpc64        SysEmuTarget = "ppc64"
	SysEmuTargetRiscv32      SysEmuTarget = "riscv32"
	SysEmuTargetRiscv64      SysEmuTarget = "riscv64"
	// since 5.0
	SysEmuTargetRx       SysEmuTarget = "rx"
	SysEmuTargetS390X    SysEmuTarget = "s390x"
	SysEmuTargetSh4      SysEmuTarget = "sh4"
	SysEmuTargetSh4Eb    SysEmuTarget = "sh4eb"
	SysEmuTargetSparc    SysEmuTarget = "sparc"
	SysEmuTargetSparc64  SysEmuTarget = "sparc64"
	SysEmuTargetTricore  SysEmuTarget = "tricore"
	SysEmuTargetX86_64   SysEmuTarget = "x86_64"
	SysEmuTargetXtensa   SysEmuTarget = "xtensa"
	SysEmuTargetXtensaeb SysEmuTarget = "xtensaeb"
)

// Specifies unit in which time-related value is specified.
//
// Since: 8.2
type TimeUnit string

const (
	// value is in seconds
	TimeUnitSecond TimeUnit = "second"
	// value is in milliseconds
	TimeUnitMillisecond TimeUnit = "millisecond"
)

// An enumeration of TPM models
//
// Since: 1.5
type TpmModel string

const (
	// TPM TIS model
	TpmModelTpmTis TpmModel = "tpm-tis"
	// TPM CRB model (since 2.12)
	TpmModelTpmCrb TpmModel = "tpm-crb"
	// TPM SPAPR model (since 5.0)
	TpmModelTpmSpapr TpmModel = "tpm-spapr"
)

// An enumeration of TPM types
//
// Since: 1.5
type TpmType string

const (
	// TPM passthrough type
	TpmTypePassthrough TpmType = "passthrough"
	// Software Emulator TPM type (since 2.11)
	TpmTypeEmulator TpmType = "emulator"
)

// State of a tracing event.
//
// Since: 2.2
type TraceEventState string

const (
	// The event is statically disabled.
	TraceEventStateUnavailable TraceEventState = "unavailable"
	// The event is dynamically disabled.
	TraceEventStateDisabled TraceEventState = "disabled"
	// The event is dynamically enabled.
	TraceEventStateEnabled TraceEventState = "enabled"
)

// Since: 1.1
type TransactionActionKind string

const (
	// Since 1.6
	TransactionActionKindAbort TransactionActionKind = "abort"
	// Since 2.5
	TransactionActionKindBlockDirtyBitmapAdd TransactionActionKind = "block-dirty-bitmap-add"
	// Since 4.2
	TransactionActionKindBlockDirtyBitmapRemove TransactionActionKind = "block-dirty-bitmap-remove"
	// Since 2.5
	TransactionActionKindBlockDirtyBitmapClear TransactionActionKind = "block-dirty-bitmap-clear"
	// Since 4.0
	TransactionActionKindBlockDirtyBitmapEnable TransactionActionKind = "block-dirty-bitmap-enable"
	// Since 4.0
	TransactionActionKindBlockDirtyBitmapDisable TransactionActionKind = "block-dirty-bitmap-disable"
	// Since 4.0
	TransactionActionKindBlockDirtyBitmapMerge TransactionActionKind = "block-dirty-bitmap-merge"
	// Since 2.3
	TransactionActionKindBlockdevBackup TransactionActionKind = "blockdev-backup"
	// Since 2.5
	TransactionActionKindBlockdevSnapshot TransactionActionKind = "blockdev-snapshot"
	// Since 1.7
	TransactionActionKindBlockdevSnapshotInternalSync TransactionActionKind = "blockdev-snapshot-internal-sync"
	// since 1.1
	TransactionActionKindBlockdevSnapshotSync TransactionActionKind = "blockdev-snapshot-sync"
	// Since 1.6
	TransactionActionKindDriveBackup TransactionActionKind = "drive-backup"
)

// vnc primary authentication method.
//
// Since: 2.3
type VncPrimaryAuth string

const (
	VncPrimaryAuthNone     VncPrimaryAuth = "none"
	VncPrimaryAuthVnc      VncPrimaryAuth = "vnc"
	VncPrimaryAuthRa2      VncPrimaryAuth = "ra2"
	VncPrimaryAuthRa2Ne    VncPrimaryAuth = "ra2ne"
	VncPrimaryAuthTight    VncPrimaryAuth = "tight"
	VncPrimaryAuthUltra    VncPrimaryAuth = "ultra"
	VncPrimaryAuthTls      VncPrimaryAuth = "tls"
	VncPrimaryAuthVencrypt VncPrimaryAuth = "vencrypt"
	VncPrimaryAuthSasl     VncPrimaryAuth = "sasl"
)

// vnc sub authentication method with vencrypt.
//
// Since: 2.3
type VncVencryptSubAuth string

const (
	VncVencryptSubAuthPlain     VncVencryptSubAuth = "plain"
	VncVencryptSubAuthTlsNone   VncVencryptSubAuth = "tls-none"
	VncVencryptSubAuthX509None  VncVencryptSubAuth = "x509-none"
	VncVencryptSubAuthTlsVnc    VncVencryptSubAuth = "tls-vnc"
	VncVencryptSubAuthX509Vnc   VncVencryptSubAuth = "x509-vnc"
	VncVencryptSubAuthTlsPlain  VncVencryptSubAuth = "tls-plain"
	VncVencryptSubAuthX509Plain VncVencryptSubAuth = "x509-plain"
	VncVencryptSubAuthTlsSasl   VncVencryptSubAuth = "tls-sasl"
	VncVencryptSubAuthX509Sasl  VncVencryptSubAuth = "x509-sasl"
)

// An enumeration of the actions taken when the watchdog device's
// timer is expired
//
// Since: 2.1
type WatchdogAction string

const (
	// system resets
	WatchdogActionReset WatchdogAction = "reset"
	// system shutdown, note that it is similar to @powerdown, which tries to set to
	// system status and notify guest
	WatchdogActionShutdown WatchdogAction = "shutdown"
	// system poweroff, the emulator program exits
	WatchdogActionPoweroff WatchdogAction = "poweroff"
	// system pauses, similar to @stop
	WatchdogActionPause WatchdogAction = "pause"
	// system enters debug state
	WatchdogActionDebug WatchdogAction = "debug"
	// nothing is done
	WatchdogActionNone WatchdogAction = "none"
	// a non-maskable interrupt is injected into the first VCPU (all VCPUS on x86)
	// (since 2.4)
	WatchdogActionInjectNmi WatchdogAction = "inject-nmi"
)

// A X86 32-bit register
//
// Since: 1.5
type X86CPURegister32 string

const (
	X86CPURegister32Eax X86CPURegister32 = "EAX"
	X86CPURegister32Ebx X86CPURegister32 = "EBX"
	X86CPURegister32Ecx X86CPURegister32 = "ECX"
	X86CPURegister32Edx X86CPURegister32 = "EDX"
	X86CPURegister32Esp X86CPURegister32 = "ESP"
	X86CPURegister32Ebp X86CPURegister32 = "EBP"
	X86CPURegister32Esi X86CPURegister32 = "ESI"
	X86CPURegister32Edi X86CPURegister32 = "EDI"
)

// Since: 4.0
type XDbgBlockGraphNodeType string

const (
	// corresponds to BlockBackend
	XDbgBlockGraphNodeTypeBlockBackend XDbgBlockGraphNodeType = "block-backend"
	// corresponds to BlockJob
	XDbgBlockGraphNodeTypeBlockJob XDbgBlockGraphNodeType = "block-job"
	// corresponds to BlockDriverState
	XDbgBlockGraphNodeTypeBlockDriver XDbgBlockGraphNodeType = "block-driver"
)

// An enumeration of yank instance types.  See @YankInstance for more
// information.
//
// Since: 6.0
type YankInstanceType string

const (
	YankInstanceTypeBlockNode YankInstanceType = "block-node"
	YankInstanceTypeChardev   YankInstanceType = "chardev"
	YankInstanceTypeMigration YankInstanceType = "migration"
)

// Since: 9.0
type ZeroPageDetection string

const (
	// Do not perform zero page checking.
	ZeroPageDetectionNone ZeroPageDetection = "none"
	// Perform zero page checking in main migration thread.
	ZeroPageDetectionLegacy ZeroPageDetection = "legacy"
	// Perform zero page checking in multifd sender thread if multifd migration is
	// enabled, else in the main migration thread as for @legacy.
	ZeroPageDetectionMultifd ZeroPageDetection = "multifd"
)
