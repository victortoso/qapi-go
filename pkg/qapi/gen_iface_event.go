/*
 * Copyright 2025 Red Hat, Inc.
 * SPDX-License-Identifier: (MIT-0 and GPL-2.0-or-later)
 */

/****************************************************************************
 * THIS CODE HAS BEEN GENERATED. DO NOT CHANGE IT DIRECTLY                  *
 ****************************************************************************/
package qapi

import "time"

type Event interface {
	AcpiDeviceOstEvent(AcpiDeviceOstEvent, time.Time) error
	BalloonChangeEvent(BalloonChangeEvent, time.Time) error
	BlockExportDeletedEvent(BlockExportDeletedEvent, time.Time) error
	BlockImageCorruptedEvent(BlockImageCorruptedEvent, time.Time) error
	BlockIoErrorEvent(BlockIoErrorEvent, time.Time) error
	BlockJobCancelledEvent(BlockJobCancelledEvent, time.Time) error
	BlockJobCompletedEvent(BlockJobCompletedEvent, time.Time) error
	BlockJobErrorEvent(BlockJobErrorEvent, time.Time) error
	BlockJobPendingEvent(BlockJobPendingEvent, time.Time) error
	BlockJobReadyEvent(BlockJobReadyEvent, time.Time) error
	BlockWriteThresholdEvent(BlockWriteThresholdEvent, time.Time) error
	ColoExitEvent(ColoExitEvent, time.Time) error
	CpuPolarizationChangeEvent(CpuPolarizationChangeEvent, time.Time) error
	DeviceDeletedEvent(DeviceDeletedEvent, time.Time) error
	DeviceTrayMovedEvent(DeviceTrayMovedEvent, time.Time) error
	DeviceUnplugGuestErrorEvent(DeviceUnplugGuestErrorEvent, time.Time) error
	DumpCompletedEvent(DumpCompletedEvent, time.Time) error
	FailoverNegotiatedEvent(FailoverNegotiatedEvent, time.Time) error
	GuestCrashloadedEvent(GuestCrashloadedEvent, time.Time) error
	GuestPanickedEvent(GuestPanickedEvent, time.Time) error
	GuestPvshutdownEvent(GuestPvshutdownEvent, time.Time) error
	HvBalloonStatusReportEvent(HvBalloonStatusReportEvent, time.Time) error
	JobStatusChangeEvent(JobStatusChangeEvent, time.Time) error
	MemoryDeviceSizeChangeEvent(MemoryDeviceSizeChangeEvent, time.Time) error
	MemoryFailureEvent(MemoryFailureEvent, time.Time) error
	MigrationEvent(MigrationEvent, time.Time) error
	MigrationPassEvent(MigrationPassEvent, time.Time) error
	NetdevStreamConnectedEvent(NetdevStreamConnectedEvent, time.Time) error
	NetdevStreamDisconnectedEvent(NetdevStreamDisconnectedEvent, time.Time) error
	NicRxFilterChangedEvent(NicRxFilterChangedEvent, time.Time) error
	PowerdownEvent(PowerdownEvent, time.Time) error
	PrManagerStatusChangedEvent(PrManagerStatusChangedEvent, time.Time) error
	QuorumFailureEvent(QuorumFailureEvent, time.Time) error
	QuorumReportBadEvent(QuorumReportBadEvent, time.Time) error
	ResetEvent(ResetEvent, time.Time) error
	ResumeEvent(ResumeEvent, time.Time) error
	RtcChangeEvent(RtcChangeEvent, time.Time) error
	ShutdownEvent(ShutdownEvent, time.Time) error
	SpiceConnectedEvent(SpiceConnectedEvent, time.Time) error
	SpiceDisconnectedEvent(SpiceDisconnectedEvent, time.Time) error
	SpiceInitializedEvent(SpiceInitializedEvent, time.Time) error
	SpiceMigrateCompletedEvent(SpiceMigrateCompletedEvent, time.Time) error
	StopEvent(StopEvent, time.Time) error
	SuspendEvent(SuspendEvent, time.Time) error
	SuspendDiskEvent(SuspendDiskEvent, time.Time) error
	UnplugPrimaryEvent(UnplugPrimaryEvent, time.Time) error
	VfioMigrationEvent(VfioMigrationEvent, time.Time) error
	VfuClientHangupEvent(VfuClientHangupEvent, time.Time) error
	VncConnectedEvent(VncConnectedEvent, time.Time) error
	VncDisconnectedEvent(VncDisconnectedEvent, time.Time) error
	VncInitializedEvent(VncInitializedEvent, time.Time) error
	VserportChangeEvent(VserportChangeEvent, time.Time) error
	WakeupEvent(WakeupEvent, time.Time) error
	WatchdogEvent(WatchdogEvent, time.Time) error
}
