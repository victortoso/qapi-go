/*
 * Copyright 2025 Red Hat, Inc.
 * SPDX-License-Identifier: (MIT-0 and GPL-2.0-or-later)
 */

/****************************************************************************
 * THIS CODE HAS BEEN GENERATED. DO NOT CHANGE IT DIRECTLY                  *
 ****************************************************************************/
package qapi

import (
	"encoding/json"
	"errors"
	"fmt"
)

// Only implemented on Alternate types that can take JSON NULL as value.
//
// This is a helper for the marshalling code. It should return true only when
// the Alternate is empty (no members are set), otherwise it returns false and
// the member set to be Marshalled.
type AbsentAlternate interface {
	ToAnyOrAbsent() (any, bool)
}

// Since: 4.1
type BlockDirtyBitmapOrStr struct {
	// name of the bitmap, attached to the same node as target bitmap.
	Local *string
	// bitmap with specified node
	External *BlockDirtyBitmap
}

func (s BlockDirtyBitmapOrStr) MarshalJSON() ([]byte, error) {
	if s.Local != nil {
		return json.Marshal(s.Local)
	} else if s.External != nil {
		return json.Marshal(s.External)
	}
	return nil, errors.New("BlockDirtyBitmapOrStr has empty fields")
}

func (s *BlockDirtyBitmapOrStr) UnmarshalJSON(data []byte) error {
	// Check for json-null first
	if string(data) == "null" {
		return errors.New(`null not supported for BlockDirtyBitmapOrStr`)
	}
	// Check for string
	{
		s.Local = new(string)
		if err := strictDecode(s.Local, data); err == nil {
			return nil
		}
		s.Local = nil
	}

	// Check for BlockDirtyBitmap
	{
		s.External = new(BlockDirtyBitmap)
		if err := strictDecode(s.External, data); err == nil {
			return nil
		}
		s.External = nil
	}

	return fmt.Errorf("Can't convert to BlockDirtyBitmapOrStr: %s", string(data))
}

// Reference to a block device.
//
// Since: 2.9
type BlockdevRef struct {
	// defines a new block device inline
	Definition *BlockdevOptions
	// references the ID of an existing block device
	Reference *string
}

func (s BlockdevRef) MarshalJSON() ([]byte, error) {
	if s.Definition != nil {
		return json.Marshal(s.Definition)
	} else if s.Reference != nil {
		return json.Marshal(s.Reference)
	}
	return nil, errors.New("BlockdevRef has empty fields")
}

func (s *BlockdevRef) UnmarshalJSON(data []byte) error {
	// Check for json-null first
	if string(data) == "null" {
		return errors.New(`null not supported for BlockdevRef`)
	}
	// Check for BlockdevOptions
	{
		s.Definition = new(BlockdevOptions)
		if err := strictDecode(s.Definition, data); err == nil {
			return nil
		}
		s.Definition = nil
	}

	// Check for string
	{
		s.Reference = new(string)
		if err := strictDecode(s.Reference, data); err == nil {
			return nil
		}
		s.Reference = nil
	}

	return fmt.Errorf("Can't convert to BlockdevRef: %s", string(data))
}

// Reference to a block device.
//
// Since: 2.9
type BlockdevRefOrNull struct {
	// defines a new block device inline
	Definition *BlockdevOptions
	// references the ID of an existing block device. An empty string
	// means that no block device should be referenced. Deprecated;
	// use null instead.
	Reference *string
	// No block device should be referenced (since 2.10)
	IsNull bool
}

func (s *BlockdevRefOrNull) ToAnyOrAbsent() (any, bool) {
	if s != nil {
		if s.IsNull {
			return nil, false
		} else if s.Definition != nil {
			return *s.Definition, false
		} else if s.Reference != nil {
			return *s.Reference, false
		}
	}

	return nil, true
}

func (s BlockdevRefOrNull) MarshalJSON() ([]byte, error) {
	if s.IsNull {
		return []byte("null"), nil
	} else if s.Definition != nil {
		return json.Marshal(s.Definition)
	} else if s.Reference != nil {
		return json.Marshal(s.Reference)
	}
	return []byte("{}"), nil
}

func (s *BlockdevRefOrNull) UnmarshalJSON(data []byte) error {
	// Check for json-null first
	if string(data) == "null" {
		s.IsNull = true
		return nil
	}
	// Check for BlockdevOptions
	{
		s.Definition = new(BlockdevOptions)
		if err := strictDecode(s.Definition, data); err == nil {
			return nil
		}
		s.Definition = nil
	}

	// Check for string
	{
		s.Reference = new(string)
		if err := strictDecode(s.Reference, data); err == nil {
			return nil
		}
		s.Reference = nil
	}

	return fmt.Errorf("Can't convert to BlockdevRefOrNull: %s", string(data))
}

// Specifies which metadata structures should be guarded against
// unintended overwriting.
//
// Since: 2.9
type Qcow2OverlapChecks struct {
	// set of flags for separate specification of each metadata
	// structure type
	Flags *Qcow2OverlapCheckFlags
	// named mode which chooses a specific set of flags
	Mode *Qcow2OverlapCheckMode
}

func (s Qcow2OverlapChecks) MarshalJSON() ([]byte, error) {
	if s.Flags != nil {
		return json.Marshal(s.Flags)
	} else if s.Mode != nil {
		return json.Marshal(s.Mode)
	}
	return nil, errors.New("Qcow2OverlapChecks has empty fields")
}

func (s *Qcow2OverlapChecks) UnmarshalJSON(data []byte) error {
	// Check for json-null first
	if string(data) == "null" {
		return errors.New(`null not supported for Qcow2OverlapChecks`)
	}
	// Check for Qcow2OverlapCheckFlags
	{
		s.Flags = new(Qcow2OverlapCheckFlags)
		if err := strictDecode(s.Flags, data); err == nil {
			return nil
		}
		s.Flags = nil
	}

	// Check for Qcow2OverlapCheckMode
	{
		s.Mode = new(Qcow2OverlapCheckMode)
		if err := strictDecode(s.Mode, data); err == nil {
			return nil
		}
		s.Mode = nil
	}

	return fmt.Errorf("Can't convert to Qcow2OverlapChecks: %s", string(data))
}

// Since: 7.1
type StatsValue struct {
	// single unsigned 64-bit integers.
	Scalar *uint64
	// single boolean value.
	Boolean *bool
	// list of unsigned 64-bit integers (used for histograms).
	List *[]uint64
}

func (s StatsValue) MarshalJSON() ([]byte, error) {
	if s.Scalar != nil {
		return json.Marshal(s.Scalar)
	} else if s.Boolean != nil {
		return json.Marshal(s.Boolean)
	} else if s.List != nil {
		return json.Marshal(s.List)
	}
	return nil, errors.New("StatsValue has empty fields")
}

func (s *StatsValue) UnmarshalJSON(data []byte) error {
	// Check for json-null first
	if string(data) == "null" {
		return errors.New(`null not supported for StatsValue`)
	}
	// Check for uint64
	{
		s.Scalar = new(uint64)
		if err := strictDecode(s.Scalar, data); err == nil {
			return nil
		}
		s.Scalar = nil
	}

	// Check for bool
	{
		s.Boolean = new(bool)
		if err := strictDecode(s.Boolean, data); err == nil {
			return nil
		}
		s.Boolean = nil
	}

	// Check for []uint64
	{
		s.List = new([]uint64)
		if err := strictDecode(s.List, data); err == nil {
			return nil
		}
		s.List = nil
	}

	return fmt.Errorf("Can't convert to StatsValue: %s", string(data))
}

// This is a string value or the explicit lack of a string (null
// pointer in C).  Intended for cases when 'optional absent' already
// has a different meaning.
//
// Since: 2.10
type StrOrNull struct {
	// the string value
	S *string
	// no string value
	IsNull bool
}

func (s *StrOrNull) ToAnyOrAbsent() (any, bool) {
	if s != nil {
		if s.IsNull {
			return nil, false
		} else if s.S != nil {
			return *s.S, false
		}
	}

	return nil, true
}

func (s StrOrNull) MarshalJSON() ([]byte, error) {
	if s.IsNull {
		return []byte("null"), nil
	} else if s.S != nil {
		return json.Marshal(s.S)
	}
	return []byte("{}"), nil
}

func (s *StrOrNull) UnmarshalJSON(data []byte) error {
	// Check for json-null first
	if string(data) == "null" {
		s.IsNull = true
		return nil
	}
	// Check for string
	{
		s.S = new(string)
		if err := strictDecode(s.S, data); err == nil {
			return nil
		}
		s.S = nil
	}

	return fmt.Errorf("Can't convert to StrOrNull: %s", string(data))
}
