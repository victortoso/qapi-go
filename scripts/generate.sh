#!/bin/bash

# The script is meant to be run with `go generate` command
# Some examples executed from the root of this go module
#  1) go generate ./...
#  2) go generate ./pkg/qapi
#  3) QEMU_REPO=$HOME/src/qemu go generate ./pkg/qapi

URL="https://gitlab.com/victortoso/qemu.git"
BRANCH="qapi-golang"

if [[ -z "${QEMU_REPO}" ]]; then
    git clone --depth 1 --branch $BRANCH $URL
    QEMU_REPO="$PWD/qemu"
fi

# TODO: Add command line option to generate Go code only
python3 $QEMU_REPO/scripts/qapi-gen.py -o tmp $QEMU_REPO/qapi/qapi-schema.json
mv tmp/go/* .
rm -rf tmp qemu
