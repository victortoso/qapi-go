# qapi-go

Generated Go data structures based on QEMU's QAPI specification.

## How to generate

There is a script that is called upon `go generate`. As the generator is not
part of QEMU upstream, the script will fetch the development branch of qemu and
generate it from there.

```shell
cd qapi-go
go generate ./...
# In case you have QEMU codebase with the generator applied
QEMU_REPO=/home/you/path/to/qemu go generate ./...
```

## Status

- The code generation between QAPI spec and Go will take place in QEMU
  code base.
  [Version 3][] (10 Jan 2025)
  [Version 2][] (16 Oct 2023)
  [Version 1][] (27 Sep 2023)
  [RFC v2][] (17 Jun 2022)
  [RFC v1][] (02 Apr 2022)

## License

The source code is dual licensed, MIT-0 and GPL-2-or-later due QAPI
documentation that is copied over the source. As the Go compiler strips
the documentation, the resulting license of the binary is MIT-0.
See the last [upstream discution][] (07 Nov 2024) if you want more info.

[RFC v1]: https://lists.gnu.org/archive/html/qemu-devel/2022-04/msg00226.html
[RFC v2]: https://lists.gnu.org/archive/html/qemu-devel/2022-06/msg03105.html
[Version 1]: https://lists.gnu.org/archive/html/qemu-devel/2023-09/msg06734.html
[Version 2]: https://lists.gnu.org/archive/html/qemu-devel/2023-10/msg04785.html
[Version 3]: https://lists.gnu.org/archive/html/qemu-devel/2025-01/msg01530.html
[upstream discution]: https://lists.gnu.org/archive/html/qemu-devel/2024-11/msg01621.html
