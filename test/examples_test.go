package qapi_test

import (
	"embed"
	"encoding/json"
	"io/fs"
	"reflect"
	"sort"
	"strings"
	"testing"

	"gitlab.com/victortoso/qapi-go/pkg/qapi"
)

//go:embed data/examples
var testData embed.FS

type TestExample struct {
	Id     string     `json:"id"`
	Client []TestPeer `json:"client,omitempty"`
	Server []TestPeer `json:"server,omitempty"`
}

type TestPeer struct {
	SequenceOrder uint            `json:"sequence-order"`
	MessageType   string          `json:"message-type"`
	Message       json.RawMessage `json:"message,omitempty"`
}

func EqualJSON(s1, s2 string) bool {
	var o1 interface{}
	var o2 interface{}

	if err := json.Unmarshal([]byte(s1), &o1); err != nil {
		panic(err)
	}
	if err := json.Unmarshal([]byte(s2), &o2); err != nil {
		panic(err)
	}
	return reflect.DeepEqual(o1, o2)
}

func runTestMessage(t *testing.T, id, MessageType string, Message json.RawMessage) {
	msg := qapi.Message{}
	if err := json.Unmarshal(Message, &msg); err != nil {
		panic(err)
	}
	switch MessageType {
	case "command":
		{
			if msg.Execute == "" && msg.ExecOOB == "" {
				t.Fatalf("no command: %s %s", MessageType, id)
			}
		}
	case "event":
		{
			if msg.Event == "" {
				t.Fatalf("no event: %s %s", MessageType, id)
			}
		}
	case "return":
		{
			if msg.Error == nil && msg.Return == nil {
				t.Fatalf("no return: %s %s", MessageType, id)
			}
		}
	default:
		panic("should not be reached")
	}

	if data, err := json.Marshal(msg); err != nil {
		t.Fatalf("%s: marshal: test-id: %s err: %s",
			MessageType, id, err.Error())
	} else if !EqualJSON(string(data), string(Message)) {
		t.Fatalf("%s: compare: test-id: %s\n* test-data:\n%s\n* qapi-result:\n%s\n",
			MessageType, id, string(Message), string(data))
	}
}

func runTest(t *testing.T, e *TestExample) {
	sort.SliceStable(e.Client, func(i, j int) bool {
		return e.Client[i].SequenceOrder < e.Client[j].SequenceOrder
	})
	sort.SliceStable(e.Server, func(i, j int) bool {
		return e.Server[i].SequenceOrder < e.Server[j].SequenceOrder
	})

	c, s := 0, 0 // Indexes for Client and Server
	for next := uint(1); c != len(e.Client) && s != len(e.Server); next++ {
		if c != len(e.Client) && e.Client[c].SequenceOrder == next {
			runTestMessage(t, e.Id, e.Client[c].MessageType, e.Client[c].Message)
			c++
		} else if s != len(e.Server) && e.Server[s].SequenceOrder == next {
			runTestMessage(t, e.Id, e.Server[s].MessageType, e.Server[s].Message)
			s++
		} else {
			t.Fatalf("Should not be reached: test-id: %s\nclient %d of %dserver %d of %d\n",
				e.Id, c, len(e.Client), s, len(e.Server))
		}
	}
}

func TestCommands(t *testing.T) {
	fs.WalkDir(testData, ".", func(path string, entry fs.DirEntry, err error) error {
		if err != nil {
			t.Fatalf("embed: walk error: %s", err.Error())
			return err
		}

		if !strings.HasSuffix(entry.Name(), ".json") {
			return nil
		}

		tmp := struct {
			Examples []TestExample `json:"examples"`
		}{}

		if data, err := testData.ReadFile(path); err != nil {
			t.Fatalf("embed: read error: %s", err.Error())
			return err
		} else if err := json.Unmarshal(data, &tmp); err != nil {
			t.Fatalf("embed: unmarshal error: %s", err.Error())
		}

		for _, example := range tmp.Examples {
			t.Logf("Running: %s", example.Id)
			runTest(t, &example)
		}

		return nil
	})
}
