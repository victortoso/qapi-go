package qapi_test

import (
	"encoding/json"
	"math"
	"strings"
	"testing"

	. "gitlab.com/victortoso/qapi-go/pkg/qapi"
)

type BuiltinTest struct {
	SchemaStr    string
	SchemaNumber float64
	SchemaInt    int64
	SchemaInt8   int8
	SchemaInt16  int16
	SchemaInt32  int32
	SchemaInt64  int64
	SchemaUint8  uint8
	SchemaUint16 uint16
	SchemaUint32 uint32
	SchemaUint64 uint64
	SchemaSize   uint64
	SchemaBool   bool
	SchemaNull   *string
	SchemaAny    any // Interesting results depend on provided JSON Object, See alternates.
	SchemaQType  QType
}

func TestTypesBuiltin(t *testing.T) {
	structToJson := []struct {
		BuiltinTest
		expectedOutput string
	}{
		// Upper limit check
		{
			BuiltinTest: BuiltinTest{
				SchemaStr:    "a string",
				SchemaNumber: math.MaxFloat64,
				SchemaInt:    math.MaxInt64, // int -> int64 is defined in QEMU generator
				SchemaInt8:   math.MaxInt8,
				SchemaInt16:  math.MaxInt16,
				SchemaInt32:  math.MaxInt32,
				SchemaInt64:  math.MaxInt64,
				SchemaUint8:  math.MaxUint8,
				SchemaUint16: math.MaxUint16,
				SchemaUint32: math.MaxUint32,
				SchemaUint64: math.MaxUint64,
				SchemaSize:   math.MaxUint64,
				SchemaBool:   true,
				SchemaNull:   nil,
				SchemaAny:    nil,
				SchemaQType:  QTypeNone,
			},
			expectedOutput: `{"SchemaStr":"a string","SchemaNumber":1.7976931348623157e+308,` +
				`"SchemaInt":9223372036854775807,"SchemaInt8":127,"SchemaInt16":32767,` +
				`"SchemaInt32":2147483647,"SchemaInt64":9223372036854775807,"SchemaUint8":255,` +
				`"SchemaUint16":65535,"SchemaUint32":4294967295,"SchemaUint64":18446744073709551615,` +
				`"SchemaSize":18446744073709551615,"SchemaBool":true,"SchemaNull":null,` +
				`"SchemaAny":null,"SchemaQType":"none"}`,
		},
		// Lower limit check
		{
			BuiltinTest: BuiltinTest{
				SchemaStr:    "another string",
				SchemaNumber: math.SmallestNonzeroFloat64,
				SchemaInt:    math.MinInt64, // int -> int64 is defined in QEMU generator
				SchemaInt8:   math.MinInt8,
				SchemaInt16:  math.MinInt16,
				SchemaInt32:  math.MinInt32,
				SchemaInt64:  math.MinInt64,
				SchemaUint8:  0,
				SchemaUint16: 0,
				SchemaUint32: 0,
				SchemaUint64: 0,
				SchemaSize:   0,
				SchemaBool:   false,
				SchemaNull:   nil,
				SchemaAny:    nil,
				SchemaQType:  QTypeQbool,
			},
			expectedOutput: `{"SchemaStr":"another string","SchemaNumber":5e-324,` +
				`"SchemaInt":-9223372036854775808,"SchemaInt8":-128,"SchemaInt16":-32768,` +
				`"SchemaInt32":-2147483648,"SchemaInt64":-9223372036854775808,"SchemaUint8":0,` +
				`"SchemaUint16":0,"SchemaUint32":0,"SchemaUint64":0,"SchemaSize":0,"SchemaBool":false,` +
				`"SchemaNull":null,"SchemaAny":null,"SchemaQType":"qbool"}`,
		},
	}

	for _, data := range structToJson {
		if b, err := json.Marshal(&data); err != nil {
			panic(err)
		} else if strings.Compare(string(b), data.expectedOutput) != 0 {
			t.Fatalf("(marshal) Bulting types do not match:\nGot:%s\nHas:%s\n", string(b), data.expectedOutput)
		} else {
			s := BuiltinTest{}
			if err := json.Unmarshal(b, &s); err != nil {
				panic(err)
			} else if s != data.BuiltinTest {
				t.Fatalf("(unmarshal) Bulting types do not match:\nGot:%v\nHas:%v\n", s, data.BuiltinTest)
			}
		}
	}
}

func TestTypesStruct(t *testing.T) {
	input := `{"file":"/some/place/my-image","backing":"/some/place/my-backing-file"}`
	s := BlockdevOptionsGenericCOWFormat{}
	if err := json.Unmarshal([]byte(input), &s); err != nil {
		panic(err)
	} else if strings.Compare(*s.File.Reference, "/some/place/my-image") != 0 ||
		strings.Compare(*s.Backing.Reference, "/some/place/my-backing-file") != 0 {
		t.Fatalf("(unmarshal) expected output failed:\nGot:%v\nHas:%s\n", s, input)
	}

	if b, err := json.Marshal(&s); err != nil {
		panic(err)
	} else if !EqualJSON(string(b), input) {
		t.Fatalf("(marshal) expected output failed:\nGot:%v\nHas:%s\n", string(b), input)
	}
}
