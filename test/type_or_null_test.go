package qapi_test

import (
	"encoding/json"
	"errors"
	"reflect"
	"strings"
	"testing"

	. "gitlab.com/victortoso/qapi-go/pkg/qapi"
)

type Optional struct {
	Name  string
	Value *StrOrNull
}

func (s Optional) MarshalJSON() ([]byte, error) {
	m := make(map[string]any)
	m["Name"] = s.Name
	if val, absent := s.Value.ToAnyOrAbsent(); !absent {
		m["Value"] = val
	}
	return json.Marshal(&m)
}

func (s *Optional) UnmarshalJSON(data []byte) error {
	tmp := struct {
		Name  string
		Value StrOrNull
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return errors.New("Failed")
	}
	s.Name = tmp.Name
	s.Value = nil
	if tmp.Value.S != nil || tmp.Value.IsNull {
		s.Value = &tmp.Value
	}
	return nil
}

// Optional in the QAPI context, that means the
// StrOrNull is going to be a pointer 4 possible values:
// - The "string" or null
// - Absent, by not being a string nor a null
// - Nil, which we consider absent
func strOrNullAsOptional(t *testing.T) {
	valueStr := "A value"

	structToJson := []struct {
		Test           Optional
		ExpectedOutput string
	}{
		{
			Test: Optional{
				Name:  "A string",
				Value: &StrOrNull{S: &valueStr},
			},
			ExpectedOutput: `{"Name":"A string","Value":"A value"}`,
		},
		{
			Test: Optional{
				Name:  "JSON Null",
				Value: &StrOrNull{IsNull: true},
			},
			ExpectedOutput: `{"Name":"JSON Null","Value":null}`,
		},
		{
			Test: Optional{
				Name:  "Nothing at all",
				Value: &StrOrNull{IsNull: false},
			},
			ExpectedOutput: `{"Name":"Nothing at all"}`,
		},
		{
			Test: Optional{
				Name:  "Nil",
				Value: nil,
			},
			ExpectedOutput: `{"Name":"Nil"}`,
		},
	}

	for _, data := range structToJson {
		if b, err := json.Marshal(&data.Test); err != nil {
			panic(err)
		} else if strings.Compare(string(b), data.ExpectedOutput) != 0 {
			t.Fatalf("(marshal) Bulting types do not match:\n- We Got --:%s\nShould Have:%s\n",
				string(b), data.ExpectedOutput)
		} else {
			s := Optional{}
			if err := json.Unmarshal(b, &s); err != nil {
				panic(err)
			} else if data.Test.Value != nil && s.Value == nil {
				// Check coner case of absent
				if data.Test.Value.S != nil || data.Test.Value.IsNull == true {
					t.Fatalf("(unmarshal) absent corner case failed.\nGot:%v\nHas:%v\n",
						s.Value, data.Test)
				}
			} else if !reflect.DeepEqual(s.Value, data.Test.Value) {
				t.Fatalf("(unmarshal) Bulting types do not match:\nGot:%v\nHas:%v\n",
					s.Value, data.Test)
			}
		}
	}
}

// Non Optional in the QAPI context, that means the
// StrOrNull is going to have only 2 possible values:
// - The "string" or null
func strOrNullAsNonOptional(t *testing.T) {
	// We don't need to implement Marshal methods
	type NonOptional struct {
		Name  string
		Value StrOrNull
	}

	valueStr := "A value"

	structToJson := []struct {
		Test           NonOptional
		ExpectedOutput string
	}{
		{
			Test: NonOptional{
				Name:  "A string",
				Value: StrOrNull{S: &valueStr},
			},
			ExpectedOutput: `{"Name":"A string","Value":"A value"}`,
		},
		{
			Test: NonOptional{
				Name:  "JSON Null",
				Value: StrOrNull{IsNull: true},
			},
			ExpectedOutput: `{"Name":"JSON Null","Value":null}`,
		},
	}

	for _, data := range structToJson {
		if b, err := json.Marshal(&data.Test); err != nil {
			panic(err)
		} else if strings.Compare(string(b), data.ExpectedOutput) != 0 {
			t.Fatalf("(marshal) Bulting types do not match:\nGot:%s\nHas:%s\n",
				string(b), data.ExpectedOutput)
		} else {
			s := NonOptional{}
			if err := json.Unmarshal(b, &s); err != nil {
				panic(err)
			} else if (s.Value.S != nil && *s.Value.S != *data.Test.Value.S) ||
				s.Value.IsNull != data.Test.Value.IsNull {
				t.Fatalf("(unmarshal) Bulting types do not match:\nGot:%v\nHas:%v\n",
					s.Value, data.Test)
			}
		}
	}
}

// Test types that uses StrOrNull.
func TestStrOrNull(t *testing.T) {
	strOrNullAsOptional(t)
	strOrNullAsNonOptional(t)
}

type BlockdevRefOrNullAlternateType string

const (
	BlockdevRefOrNullTypeIsStruct BlockdevRefOrNullAlternateType = "struct"
	BlockdevRefOrNullTypeIsString BlockdevRefOrNullAlternateType = "string"
	BlockdevRefOrNullTypeIsNull   BlockdevRefOrNullAlternateType = "null"
	BlockdevRefOrNullTypeIsAbsent BlockdevRefOrNullAlternateType = "absent"
)

type OtherOptional struct {
	Name  string
	Value *BlockdevRefOrNull
}

// This is useful for Types that have optional fields of .BlockdevRefOrNull
func BlockdevRefOrNullMarshalHelper(k *BlockdevRefOrNull) BlockdevRefOrNullAlternateType {
	if k != nil {
		if k.IsNull {
			return BlockdevRefOrNullTypeIsNull
		} else if k.Definition != nil {
			return BlockdevRefOrNullTypeIsStruct
		} else if k.Reference != nil {
			return BlockdevRefOrNullTypeIsString
		}
	}
	// Two situations where Value is ignored when optional (ptr)
	// 1. The value is not set (nil)
	// 2. The value is set but BlockdevRefOrNull's
	//    * IsNull is false and;
	//    * Definition and Reference are not set
	//
	// When unmarshal, we always go with (2)
	return BlockdevRefOrNullTypeIsAbsent
}

func (s OtherOptional) MarshalJSON() ([]byte, error) {
	type baseOptional struct {
		Name string
	}
	base := baseOptional{
		Name: s.Name,
	}

	switch BlockdevRefOrNullMarshalHelper(s.Value) {
	case BlockdevRefOrNullTypeIsNull:
		tmp := struct {
			baseOptional
			Value *string
		}{
			baseOptional: base,
			Value:        nil,
		}
		return json.Marshal(&tmp)
	case BlockdevRefOrNullTypeIsString:
		tmp := struct {
			baseOptional
			Value string
		}{
			baseOptional: base,
			Value:        *s.Value.Reference,
		}
		return json.Marshal(&tmp)
	case BlockdevRefOrNullTypeIsStruct:
		tmp := struct {
			baseOptional
			Value BlockdevOptions
		}{
			baseOptional: base,
			Value:        *s.Value.Definition,
		}
		return json.Marshal(&tmp)
	case BlockdevRefOrNullTypeIsAbsent:
		return json.Marshal(&base)
	default:
		return []byte("{}"), errors.New("Failed")
	}
}

func (s *OtherOptional) UnmarshalJSON(data []byte) error {
	tmp := struct {
		Name  string
		Value BlockdevRefOrNull
	}{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return errors.New("Failed")
	}
	s.Name = tmp.Name
	s.Value = &tmp.Value
	return nil
}

// Optional in the QAPI context, that means the
// BlockdevRefOrNull is going to be a pointer 5 possible values:
// - The struct, string or null
// - Absent, by not being a string nor a null
// - Nil, which we consider absent
func blockdevRefOrNullAsOptional(t *testing.T) {
	valueStr := "A value"
	valueTrue := true

	structToJson := []struct {
		Test           OtherOptional
		ExpectedOutput string
	}{
		{
			Test: OtherOptional{
				Name:  "A string",
				Value: &BlockdevRefOrNull{Reference: &valueStr},
			},
			ExpectedOutput: `{"Name":"A string","Value":"A value"}`,
		},
		{
			Test: OtherOptional{
				Name:  "JSON Null",
				Value: &BlockdevRefOrNull{IsNull: true},
			},
			ExpectedOutput: `{"Name":"JSON Null","Value":null}`,
		},
		{
			Test: OtherOptional{
				Name:  "Nothing at all",
				Value: &BlockdevRefOrNull{IsNull: false},
			},
			ExpectedOutput: `{"Name":"Nothing at all"}`,
		},
		{
			Test: OtherOptional{
				Name: "A struct",
				Value: &BlockdevRefOrNull{
					Definition: &BlockdevOptions{
						ReadOnly: &valueTrue,
						File: &BlockdevOptionsFile{
							Filename: "/tmp/mydisk.qcow2",
						},
					},
				},
			},
			ExpectedOutput: `{"Name":"A struct","Value":{"read-only":true,"filename":"/tmp/mydisk.qcow2","driver":"file"}}`,
		},
		{
			Test: OtherOptional{
				Name:  "Nil",
				Value: nil,
			},
			ExpectedOutput: `{"Name":"Nil"}`,
		},
	}

	for _, data := range structToJson {
		if b, err := json.Marshal(&data.Test); err != nil {
			panic(err)
		} else if !EqualJSON(string(b), data.ExpectedOutput) {
			t.Fatalf("(marshal) Bulting types do not match:\n- We Got --:%s\nShould Have:%s\n",
				string(b), data.ExpectedOutput)
		} else {
			s := OtherOptional{}
			if err := json.Unmarshal(b, &s); err != nil {
				panic(err)
			} else if data.Test.Value == nil {
				if s.Value.IsNull || s.Value.Reference != nil || s.Value.Definition != nil {
					t.Fatalf("(unmarshal) Bulting types do not match:\nGot:%v\nHas:%v\n",
						s.Value, data.Test)
				}
			} else if !reflect.DeepEqual(s.Value, data.Test.Value) {
				t.Fatalf("(unmarshal) Bulting types do not match:\nGot:%+v\nShould be:%+v\n",
					s.Value, data.Test.Value)
			}
		}
	}
}

// Non Optional in the QAPI context, that means the
// BlockdevRefOrNull is going to have only 3 possible values:
// - The BockdevOption, "string" or null
func blockdevRefOrNullAsNonOptional(t *testing.T) {
	// We don't need to implement Marshal methods
	type NonOptional struct {
		Name  string
		Value BlockdevRefOrNull
	}

	valueStr := "A value"
	valueTrue := true

	structToJson := []struct {
		Test           NonOptional
		ExpectedOutput string
	}{
		{
			Test: NonOptional{
				Name:  "A string",
				Value: BlockdevRefOrNull{Reference: &valueStr},
			},
			ExpectedOutput: `{"Name":"A string","Value":"A value"}`,
		},
		{
			Test: NonOptional{
				Name:  "JSON Null",
				Value: BlockdevRefOrNull{IsNull: true},
			},
			ExpectedOutput: `{"Name":"JSON Null","Value":null}`,
		},
		{
			Test: NonOptional{
				Name: "A struct",
				Value: BlockdevRefOrNull{
					Definition: &BlockdevOptions{
						ReadOnly: &valueTrue,
						File: &BlockdevOptionsFile{
							Filename: "/tmp/mydisk.qcow2",
						},
					},
				},
			},
			ExpectedOutput: `{"Name":"A struct","Value":{"read-only":true,"filename":"/tmp/mydisk.qcow2","driver":"file"}}`,
		},
	}

	for _, data := range structToJson {
		if b, err := json.Marshal(&data.Test); err != nil {
			panic(err)
		} else if !EqualJSON(string(b), data.ExpectedOutput) {
			t.Fatalf("(marshal) Bulting types do not match:\nGot:%s\nHas:%s\n",
				string(b), data.ExpectedOutput)
		} else {
			s := NonOptional{}
			if err := json.Unmarshal(b, &s); err != nil {
				panic(err)
			} else if (s.Value.Reference != nil && *s.Value.Reference != *data.Test.Value.Reference) ||
				s.Value.IsNull != data.Test.Value.IsNull {
				t.Fatalf("(unmarshal) Bulting types do not match:\nGot:%v\nHas:%v\n",
					s.Value, data.Test)
			}
		}
	}
}

// Test types that uses BlockdevRefOrNull
func TestBlockdevRefOrNull(t *testing.T) {
	blockdevRefOrNullAsOptional(t)
	blockdevRefOrNullAsNonOptional(t)
}
